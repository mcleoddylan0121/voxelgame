//============================================================================
// Name        : voxelgame.cpp
// Author      : bestgame
// Version     :
// Copyright   :
// Description : This is a test
//============================================================================


#include "console.h"
#include "chunk.h"
#include "gameloop.h"
#include "glcontroller.h"
#include "sdlcontroller.h"
#include "worldstate.h"
#include "geometry.h"
#include "resourcemanager.h"
#include "luareg.h"
#include "filesystem.h"
#include "settingsmanager.h"
#include "guistate.h"
#include "vfreetype.h"
#include "luacontext.h"
#include "entitymanager.h"
#include "modulemanager.h"
#include "noisestate.h"

#include <time.h>
#include <cstdlib>

int main( int argc, char **argv ) {
	srand(0); // sooper important, really

	vox::initConsole();

	vox::LuaScript * assetdir = new vox::LuaScript;
	assetdir->init("assetdir");
	vox::mFileManager = new vox::FileManager(assetdir->getArray<std::string>("paths"));
	vox::mModuleManager = new vox::ModuleManager();
	vox::mModuleManager->registerAllModules(assetdir->getArray<std::string>("modules"));
	delete assetdir;

	vox::registerLuaFunctions();
	vox::mLua = new vox::LuaContext();
	vox::mSettings = new vox::SettingsManager(vox::mFileManager->fetchAsset("settings", vox::Asset::Lua, false));
	vox::mSDL = new vox::SDLController();
	vox::mModuleManager->initBaseModule();
	vox::mSDL->makeWindow(vox::mSettings->resWidth, vox::mSettings->resHeight, vox::mSettings->resWidth, vox::mSettings->resHeight, vox::mSettings->fullscreen);
	vox::mSDL->setIcon(vox::mFileManager->fetchAsset("icon", vox::Asset::Texture, false));
	vox::mPlayer = new vox::PlayerManager();
	vox::mRes = new vox::ResourceManager();
	vox::mGL = new vox::GLController();
	vox::mGL->init();
	vox::mFontManager = new vox::FontManager();
	vox::mFontManager->init();
	vox::mGL->CUBEMAP_FIX = vox::mSettings->cubemapFix;

	vox::Palette::init();


	vox::WorldState * test = new vox::WorldState();
	vox::GUIState * test2 = new vox::GUIState();
	vox::mGameLoop.addStateToFront(test2);
	vox::mGameLoop.addStateToFront(test);

	//vox::NoiseState * test3 = new vox::NoiseState("octavetest");
	//vox::mGameLoop.addStateToFront(test3);

	vox::mSDL->clean();
	vox::mModuleManager->initAllModules();
	vox::mSettings->saveSettings();
	vox::mGameLoop.start();

	delete test;
	delete test2;
	delete vox::mGL;
	delete vox::mSDL;
	delete vox::mRes;
	delete vox::mFileManager;
	delete vox::mSettings;
	delete vox::mFontManager;
	delete vox::mLua;
	delete vox::mPlayer;
	delete vox::mModuleManager;
	// delete vox::mFileManager; // This crashes the program lol

	vox::endConsole();

	return 0;
}
