/*
 * animlua.cpp
 *
 *  Created on: Sep 20, 2015
 *      Author: Daniel
 */

#include "animlua.h"
#include "skeleton.h"
#include "resourcemanager.h"
#include "filesystem.h"

namespace vox {

	JointMotionObject::JointMotionObject() : LuaObject({
		lmakevec(jointName, posOnParent, rotAnchor, offset, rotation)
	}) {}

	LuaKeyFrame::LuaKeyFrame() : LuaObject({
		lmakevec(jointMotionList, time)
	}) {}

	LuaAnimation::LuaAnimation() : LuaObject({
		lmakevec(frames, rep)
	}) {}

	LuaSkeleton::LuaSkeleton() : LuaObject({
		lmakevec(jointNames, jointParents, jointMeshes, animNames, animData)
	}) {}


	void setupJoint(std::unordered_map<std::string, Joint*>& jointMap, KeyFrame * base,
			std::string jointName, std::string parent, std::string mesh,
			glm::vec3 posOnParent, glm::vec3 rotAnchor, glm::vec3 offset, glm::quat rotation) {
		jointMap[jointName] = new Joint(mRes->getResource<EntityMesh, Asset::Mesh>(mesh), jointName);
		jointMap[parent]->children.push_back(jointMap[jointName]);
		base->components[jointName].posOnParent = posOnParent;
		base->components[jointName].anchorPos = rotAnchor;
		base->components[jointName].position = offset;
		base->components[jointName].rot = rotation;
	};

	void addSpecialJoints(std::unordered_map<std::string, Joint*>& jointMap, KeyFrame * base) {
		// Add internal left and right hands (this is where equipment goes) only if jointMap contains both a left and right hand
		if (jointMap.find("leftHand") != jointMap.end() && jointMap.find("rightHand") != jointMap.end()) {
			delete jointMap["_leftHand"];	// Just in case some modder defined these. This prevents memory leaks.
			delete jointMap["_rightHand"];

			// Set up left hand
			setupJoint(jointMap, base, "_leftHand", "leftHand", "weapons/hatchet",
					glm::vec3(0.5, 0.5, 0.5),
					glm::vec3(0.4, 1.5/6, 2/4),
					glm::vec3(),
					glm::angleAxis(-PI/2, glm::vec3(0,1,0)) * glm::angleAxis(-PI/2, glm::vec3(1,0,0)) * glm::angleAxis(PI/2, glm::vec3(0,1,0)));


			// Set up right hand
			setupJoint(jointMap, base, "_rightHand", "rightHand", "weapons/hatchet_small",
					glm::vec3(0.5, 0.5, 0.5),
					glm::vec3(0.4, 1.5/6, 2/4),
					glm::vec3(),
					glm::angleAxis(-PI/2, glm::vec3(0,1,0)) * glm::angleAxis(-PI/2, glm::vec3(1,0,0)) * glm::angleAxis(PI/2, glm::vec3(0,1,0)));
		}
	}

	Skeleton* constructSkeleton(LuaSkeleton s) {
		std::unordered_map<std::string, Joint*> jointMap;

		// Populate the jointMap so we can assign parents
		for (unsigned int i = 0; i < s.jointNames.size(); i++) {
			const std::string& name = s.jointNames[i];
			const std::string& meshName = s.jointMeshes[i];

			// If the joint is already defined, don't define it again.
			// This prevents memory leaks
			if (jointMap.find(name) != jointMap.end()) continue;

			jointMap[name] = new Joint(mRes->getResource<EntityMesh, Asset::Mesh>(meshName), name);
		}

		Joint * rootNode = 0;

		// Assign parents
		for (unsigned int i = 0; i < s.jointNames.size(); i++) {
			const std::string& name = s.jointNames[i];
			const std::string& parentName = s.jointParents[i];

			if (parentName == "_root") rootNode = jointMap[name];
			else jointMap[parentName]->addChild(jointMap[name]);
		}

		// Create animations
		std::unordered_map<std::string, Animation*> anims;
		for (unsigned int i = 0; i < s.animNames.size(); i++) {
			const std::string& name = s.animNames[i];
			const LuaAnimation& anim = s.animData[i];

			std::vector<KeyFrame*> frames;
			for (const LuaKeyFrame& frame : anim.frames) {
				std::unordered_map<std::string, KeyFrameComponent> jointMotionMap;
				for (const JointMotionObject& j : frame.jointMotionList)
					jointMotionMap[j.jointName] = KeyFrameComponent{
						glm::vec3(j.posOnParent.x, j.posOnParent.y, j.posOnParent.z),
						glm::vec3(j.rotAnchor.x, j.rotAnchor.y, j.rotAnchor.z),
						glm::vec3(j.offset.x, j.offset.y, j.offset.z),
						glm::quat(j.rotation.w,j.rotation.x,j.rotation.y,j.rotation.z)
					};
				frames.push_back(new KeyFrame(jointMotionMap, frame.time));
			}

			Animation * a = new Animation(frames);
			a->repeat = anim.rep;
			anims[name] = a;
		}
		addSpecialJoints(jointMap, anims["base"]->frames[0]);

		Skeleton * ret = new Skeleton(rootNode, anims["base"]->frames[0]);
		for (auto i : anims)
			ret->addAnimation(i.first, i.second);

		return ret;
	}
}
