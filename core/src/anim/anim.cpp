#include "anim.h"

#include <cmath>
#include "util.h"

namespace vox {

	namespace tween {

		float linear(float x, float initVal, float finalVal) {
			return (finalVal - initVal) * x + initVal;
		}


		// TODO: Optimize that v
		float linearClosestAngle(float x, float initVal, float finalVal) {
			const float pi = 3.1415926;

			if (std::abs(initVal - finalVal) <= pi) {
				// We can go straight to the value
				return linear(x, initVal, finalVal);
			} else {
				// We have to go through 2 PI
				if (initVal > finalVal)
					return wrapAngle((pi * 2 + finalVal - initVal) * x + initVal);
				else
					return wrapAngle((finalVal - pi * 2 - initVal) * x + initVal);
			}
		}

		glm::vec3 linearVec3(float x, glm::vec3 initVal, glm::vec3 finalVal) {
			return (finalVal - initVal) * x + initVal;
		}

		glm::quat linearQuat(float x, glm::quat q1, glm::quat q2) {
			if(x >= 0.999) return q2;
			else if(x <= 0.001) return q1;
			if(glm::length(q1) <= 0 || glm::length(q2) <= 0) return glm::quat(1,0,0,0);
			float c = glm::dot(q1, q2);
			if (c < 0.0f) {
				q1 = -q1;
				c = -c;
			}
			// quaternion to return
			glm::quat qm;
			// if qa=qb or qa=-qb then theta = 0 and we can return qa
			if (abs(c) >= 1.0){
				qm.w = q1.w;
				qm.x = q1.x;
				qm.y = q1.y;
				qm.z = q1.z;
				return qm;
			}
			// Calculate temporary values.
			double halfTheta = acos(c);
			double sinHalfTheta = sqrt(1.0 - c*c);
			// if theta = 180 degrees then result is not fully defined
			// we could rotate around any axis normal to qa or qb
			if (fabs(sinHalfTheta) < 0.001){ // fabs is floating point absolute
				qm.w = (q1.w * 0.5 + q2.w * 0.5);
				qm.x = (q1.x * 0.5 + q2.x * 0.5);
				qm.y = (q1.y * 0.5 + q2.y * 0.5);
				qm.z = (q1.z * 0.5 + q2.z * 0.5);
				return qm;
			}
			double ratioA = sin((1 - x) * halfTheta) / sinHalfTheta;
			double ratioB = sin(x * halfTheta) / sinHalfTheta;
			//calculate Quaternion.
			qm.w = (q1.w * ratioA + q2.w * ratioB);
			qm.x = (q1.x * ratioA + q2.x * ratioB);
			qm.y = (q1.y * ratioA + q2.y * ratioB);
			qm.z = (q1.z * ratioA + q2.z * ratioB);
			return glm::normalize(qm);
		}

	}
}
