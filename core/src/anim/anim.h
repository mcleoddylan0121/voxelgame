#ifndef ANIM_H_
#define ANIM_H_

#include "vglm.h"
#include <functional>

namespace vox {

	template <typename T>
	struct Tween {

		// Note: Initializes values with the default constructor.
		Tween() {
			_curTime = 0;
			_maxTime = 0;
			_out = 0;
			_init = T();
			_final = T();
			_curVal = T();
		}

		// Initializes the tween. You can call this multiple times to restart the tween if you so desire.
		// You do not need to define out. If you do, however, that variable will automatically be updated as long as
		//   the tween hasn't yet finished.
		void init(T initialValue, T finalValue, float maxTime, std::function<T(float, T, T)> updateFunction, T * out = (T*)0) {
			_init = initialValue;
			_curVal = initialValue;
			_final = finalValue;
			_maxTime = maxTime;
			_curTime = 0;
			_out = out;
			if (_out) *_out = _curVal;
			_update = updateFunction;
			_finished = false;
		}

		// Moves the tween along its path. This updates the out variable as long as you defined it in init.
		// To pause the tween, stop calling this function.
		void tick(float delta) {
			if (!_finished) {
				_curTime += delta;

				if (_curTime / _maxTime >= 1) {
					_curVal = _update(1, _init, _final);
					_finished = true;
				} else _curVal = _update(_curTime / _maxTime, _init, _final);

				if (_out) *_out = _curVal;
			}
		}

		T getValue() { return _curVal; }
		T getInitialValue() { return _init; }
		T getFinalValue() { return _final; }
		bool isFinished() { return _finished; }

		void setFinalValue(T t) { _final = t; }
		void setInitialValue(T t) { _init = t; }

	private:
		T _init, _final, _curVal, *_out;
		float _maxTime, _curTime;
		std::function<T(float, T, T)> _update;
		bool _finished = true;
	};

	namespace tween {

		template<typename T> // Accelerate, then ease out cubicly
		T cubicEaseOut(float x, T initVal, T finalVal) {
			x = (x - 1);
			return (finalVal - initVal)*(x*x*x+1) + initVal;
		}

		template<typename T> // for traditional stop-motion animation
		T instant(float x, T init, T final) {
			return final;
		}

		float linear(float x, float initVal, float finalVal);

		// Moves the value the smallest distance around a circle
		float linearClosestAngle(float x, float initVal, float finalVal);

		glm::quat linearQuat(float x, glm::quat initVal, glm::quat finalVal);
		glm::vec3 linearVec3(float x, glm::vec3 initVal, glm::vec3 finalVal);

		template<float (*baseTween) (float, float, float)>
		glm::quat quatTween(float x, glm::quat initVal, glm::quat finalVal) {
			return linearQuat(baseTween(x,0.f,1.f), initVal, finalVal);
		}

		template<typename T>
		T quinticEaseInOut(float t, T b, T c) {
			t *= 2;
			if (t < 1) return (c-b)*T(t*t*t*t*t/2.f) + b;
			t -= 2;
			return (c-b)*T((t*t*t*t*t + 2.f)/2.f) + b;
		};

		template<typename T>
		T cubicEaseInOut(float t, T b, T c) {
			t *= 2;
			if (t < 1) return (c-b)*T(t*t*t/2.f) + b;
			t -= 2;
			return (c-b)*T((t*t*t + 2)/2.f) + b;
		};

		template<typename T>
		T quadraticEaseInOut(float t, T b, T c) {
			t *= 2;
			if (t < 1) return (c-b) / T(t*t/2.f) + b;
			t--;
			return (c-b) * T((t*t + 2.f)/2.f) + b;
		};

		template<typename T>
		T sinEaseInOut(float t, T b, T c) {
			return -(c-b) * T((cos(3.1415926353f*t) - 1.f)/2.f) + b;
		};

	}
}

#endif /* ANIM_H_ */
