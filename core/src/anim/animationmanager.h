/*
 * animationmanager.h
 *
 *  Created on: Dec 13, 2015
 *      Author: Daniel
 */

#ifndef CORE_SRC_ANIM_ANIMATIONMANAGER_H_
#define CORE_SRC_ANIM_ANIMATIONMANAGER_H_

#include <unordered_map>
#include <string>
#include <vector>
#include <queue>

namespace vox {

	struct Skeleton;
	struct Animation;
	struct KeyFrame;

	class AnimationManager {
		friend struct EntityLibrary;
		friend struct Animation;

		Skeleton * _skeleton;
		std::unordered_map<std::string, std::vector<std::string>> _triggerMap;
		std::queue<std::string> _playlist;
		bool _functional = false;
		std::string _lastTrigger = "";

		Animation * _curAnim;
		bool _running = false, _transition = false;
		float speed = 1.f;

		void _setSkeleton(Skeleton * s);
		void _removeSkeleton();

		// Internal Animation Stopping, called from Animation
		void _curAnimFinished();

	public:
		AnimationManager();

		void tick(float delta);

		void trigger(std::string triggerName, bool interrupt);
		void playNext();

		void setSpeed(float speed);

		bool hasNext();
		bool isFunctional();
	};

}



#endif /* CORE_SRC_ANIM_ANIMATIONMANAGER_H_ */
