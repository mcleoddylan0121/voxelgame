/*
 * animationmanager.cpp
 *
 *  Created on: Dec 13, 2015
 *      Author: Daniel
 */

#include "animationmanager.h"
#include "skeleton.h"

namespace vox {

	const std::string DEFAULT_TRIGGER = "IDLE";

	void AnimationManager::_setSkeleton(Skeleton * s) {
		_skeleton = s;
		_functional = true;
		trigger(DEFAULT_TRIGGER, true);
	}

	void AnimationManager::_removeSkeleton() {
		_skeleton = 0;
		_functional = false;
		_running = false;
	}

	void AnimationManager::_curAnimFinished() {
		if (!isFunctional()) return;
		_running = false;
		_transition = true;
		playNext();
	}

	AnimationManager::AnimationManager() {
		_skeleton = 0;
		_curAnim = 0;

		// TODO: Temporarily hard-coded pls don't kill me
		_triggerMap = {
				{"WALK", {"walk"}},
				{"IDLE", {"idle"}},
				{"INAIR", {"inAir"}}
		};
	}

	void AnimationManager::tick(float delta) {
		delta *= speed;

		if (isFunctional() && _running) {
			if (_transition && _curAnim->curTweenTimeLeft > 0) {
				for (auto&& t : _curAnim->tweens)
					t.second.tick(delta);
				_curAnim->curTweenTimeLeft -= delta;
			} else {
				if (_transition) _curAnim->setupTween();
				_transition = false;
				_curAnim->tick(delta);
			}
		}
	}

	void AnimationManager::trigger(std::string triggerName, bool interrupt) {
		if ((_lastTrigger == triggerName && !interrupt) || !isFunctional() || _triggerMap.find(triggerName) == _triggerMap.end()) return;

		// TODO: Add queueing for non-interrupt triggers

		_lastTrigger = triggerName;

		std::vector<std::string> list = _triggerMap[triggerName];
		_playlist = std::queue<std::string>();
		for (auto& i : list)
			_playlist.push(i);

		playNext();
	}

	void AnimationManager::playNext() {
		if (!isFunctional()) return;
		if (!hasNext()) trigger(DEFAULT_TRIGGER, true);

		std::string anim = _playlist.front();
		Animation * a = _skeleton->getAnimation(anim);
		_playlist.pop();

		if (a != nullptr && a->frames.size() != 0) {
			// If we stopped before the end of the last animation, transition into the next one
			if (_running && !_curAnim->done) _transition = true;

			a->host = this;
			a->curFrame = 0;
			a->time = 0;
			_curAnim = a;
			_running = true;

			if (_transition)
				a->setupTween(_skeleton, a->frames[0]);
		} else
			playNext();
	}

	bool AnimationManager::hasNext() { return _playlist.size() > 0; }

	bool AnimationManager::isFunctional() { return _functional; }

	void AnimationManager::setSpeed(float speed) { this->speed = speed; }
}


