/*
 * skeleton.cpp
 *
 *  Created on: Sep 11, 2015
 *      Author: Dylan
 */

#include "skeleton.h"
#include "resourcemanager.h"
#include "animationmanager.h"

// TEMPORARY AF
#include "entitymanager.h"
#include "worldstate.h"
#include "chunk.h"

namespace vox {

	Joint::~Joint() {
		for(auto i:children) delete i;
	}

	void Joint::_render(EntityMeshRenderer* r, Color::HighpColor override) {
		EntityMeshDisplayObject e(mesh->buf, m_rotM * nMatrix, renderSize);
		e.llevOverride = override;
		if (visible) r->render(e);
		for(auto&& i:children)
			i->_render(r, override);
	}

	void Joint::render(EntityMeshRenderer* r) {
		Color::HighpColor override(0);
		VoxelCoordinate middlePos = worldToVoxelCoordinate(worldPos);
		Color::HighpColor llev = Color::HighpColor(EntityLibrary::_m->_parent->map->getLightLevel(middlePos).getRGBValue(),1);

		_render(r, llev);
	}

	void Joint::_tick(glm::mat4 model, glm::vec3 voxelSize) {
		// Translate this so the anchor is at the origin, then rotate, then translate back
		glm::mat4 rotM(1.f);
		rotM = glm::translate(rotM, glm::vec3(mesh->size) * anchorPos * voxelSize);
		rotM = rotM * glm::toMat4(rot);
		rotM = glm::translate(rotM,-glm::vec3(mesh->size) * anchorPos * voxelSize);
		renderSize = voxelSize * glm::vec3(mesh->size);

		glm::vec3 o = voxelSize * (translate - anchorPos * glm::vec3(mesh->size));
		if (parent) o += voxelSize * posOnParent * glm::vec3(parent->mesh->size);

		glm::mat4 mo(1.f);
		mo = glm::translate(mo, o);
		glm::mat4 m = model * mo;
		m_rotM = m * rotM;

		// This never changes, so we should move voxelSize out of the pipeline and into initialization
		nMatrix = glm::scale(voxelSize);

		// Store this so we can calculate the center of the joint
		glm::mat4 rot2M(1.f);
		rot2M = glm::translate(rotM, glm::vec3(mesh->size) * 0.5f * voxelSize);
		rot2M = rot2M * glm::toMat4(rot);

		auto centerMat = m * rot2M * nMatrix;
		worldPos = glm::vec3(centerMat[3]); // Use the w of the first 3 rows, which stores the position

		for(auto&& i:children)
			i->_tick(m_rotM, voxelSize);
	};

	void Joint::tick(glm::vec3 worldPos, glm::vec3 voxelSize, glm::quat rot) {
		// Note that I hard-code the anchor point here because this is the bounding box's anchor point
		//  and I haven't implemented that yet.
		glm::mat4 worldRot(1.f);
		worldRot = glm::translate(worldRot, glm::vec3(mesh->size) * 0.5f * voxelSize);
		worldRot = worldRot * glm::toMat4(rot);
		worldRot = glm::translate(worldRot,-glm::vec3(mesh->size) * 0.5f * voxelSize);

		glm::mat4 worldModel(1.f);
		worldModel = glm::translate(worldModel, worldPos);

		_tick(worldModel * worldRot, voxelSize);
	}


	void Skeleton::_initJoints(Joint* j) {
		joints[j->name] = j;
		for(auto i: j->children)
			_initJoints(i);
	}

	void Skeleton::_setJointPositions(KeyFrame* f) {
		for (auto& k: joints) {
			Joint* j = k.second;

			auto found = f->components.find(j->name);
			if (found != f->components.end()) {
				KeyFrameComponent c = found->second;
				j->anchorPos = c.anchorPos;
				j->posOnParent = c.posOnParent;
				j->rot = c.rot;
				j->translate = c.position;
			}
		}
	}

	Skeleton::Skeleton(Joint* root, KeyFrame* base): root(root) {
		_initJoints(root);
		if (base) _setJointPositions(base);
	}
	void Skeleton::render(EntityMeshRenderer* r) {
		root->render(r);
	}
	void Skeleton::tick(float delta, WorldCoordinate pos, WorldCoordinate voxelSize, glm::quat rot) {
		root->tick(pos, voxelSize, rot);
	}
	Skeleton::~Skeleton() {
		delete root;
		for(auto i:animations) {
			if(i.second) delete i.second;
		}
	}


	// Helper function
	KeyFrameTween makeTween(KeyFrame* fr, KeyFrameComponent* b, Joint* j, float speed) {
		KeyFrameTween t;
		t.position.init(j->translate, b->position, fr->time / speed, tween::sinEaseInOut<glm::vec3>, &(j->translate));
		t.posOnParent.init(j->posOnParent, b->posOnParent, fr->time / speed, tween::sinEaseInOut<glm::vec3>, &(j->posOnParent));
		t.anchorPos.init(j->anchorPos, b->anchorPos, fr->time / speed, tween::sinEaseInOut<glm::vec3>, &(j->anchorPos));
		t.rot.init(j->rot, b->rot, fr->time / speed, tween::quatTween<tween::sinEaseInOut<float>>, &(j->rot));
		return t;
	}

	Animation::Animation(std::vector<KeyFrame*> frames) : frames(frames) {}

	void Animation::setupTween(Skeleton * skel, KeyFrame * next) {
		for (auto&& a: next->components)
			if (skel->joints[a.first])
				tweens[a.first] = makeTween(next, &(next->components[a.first]), skel->joints[a.first], speed);

		curTweenTimeLeft = next->time / speed;
	}

	void Animation::setupTween() {
		setupTween(host->_skeleton, frames[(curFrame+1) % frames.size()]);
	}

	void Animation::tick(float delta) {
		time += delta * speed;

		for (auto& tween: tweens)
			tween.second.tick(delta);

		if (time >= frames[curFrame]->time) {
			time = 0.f;
			curFrame++;

			if (!repeat && curFrame >= frames.size()) {
				curFrame = frames.size() - 1;
				done = true;
				host->_curAnimFinished();
				return;
			}

			curFrame %= frames.size();

			setupTween();
		}
	}

	Animation::~Animation() {
		for(auto i:frames)
			delete i;
	}

	void KeyFrameTween::tick(float delta) {
		position.tick(delta);
		rot.tick(delta);
		posOnParent.tick(delta);
		anchorPos.tick(delta);
	}

	void Skeleton::addAnimation(std::string name, Animation* anim) {
		animations[name] = anim;
	}

	const std::string DEFAULT_ANIMATION = "idle";

	Animation* Skeleton::getAnimation(std::string name) {
		auto a = animations.find(name);
		return (a != animations.end() ? a->second : getAnimation("base"));
	}
}
