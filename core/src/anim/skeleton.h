/*
 * skeleton.h
 *
 *  Created on: Sep 11, 2015
 *      Author: Dylan
 */

#ifndef SRC_ANIM_SKELETON_H_
#define SRC_ANIM_SKELETON_H_

#include "vglm.h"
#include "renderutils.h"
#include "voxelmesh.h"
#include "anim.h"

#include <unordered_map>

namespace vox {

	struct AnimationManager;

	struct Joint {
		EntityMesh* mesh;
		std::string name;

		// The anchor is the position on the mesh where we anchor the model to the position on the parent
		glm::vec3 posOnParent = glm::vec3(0), anchorPos = glm::vec3(0.5), renderSize;
		WorldCoordinate translate = glm::vec3(0), worldPos; // Worldpos is the center of the joint in world coordinates
											 	 	 	    // Updated every joint tick
		glm::quat rot = glm::quat();


		std::vector<Joint*> children;
		Joint* parent = nullptr;


		glm::mat4 m_rotM, nMatrix;
		bool visible = true;

		virtual ~Joint();

		Joint(EntityMesh* mesh, std::string name): mesh(mesh), name(name) {}

		void addChild(Joint* j) { children.push_back(j); j->parent = this; }

		void render(EntityMeshRenderer* r);

		void tick(glm::vec3 worldPos, glm::vec3 voxelSize, glm::quat rot);

	private:

		// Internal helpoer functions
		void _render(EntityMeshRenderer* r, Color::HighpColor override);
		void _tick(glm::mat4 model, glm::vec3 voxelSize = glm::vec3(1/16.f));
	};

	class Animation;
	class KeyFrame;

	struct Skeleton {
		Joint* root;
		std::unordered_map<std::string, Joint*> joints;
		std::unordered_map<std::string, Animation*> animations;

		Skeleton(Joint* root, KeyFrame* base = 0);
		virtual ~Skeleton();

		void render(EntityMeshRenderer* r);
		void tick(float delta, WorldCoordinate pos, WorldCoordinate voxelSize, glm::quat rot);

		void addAnimation(std::string name, Animation* anim);
		Animation* getAnimation(std::string name);

	private:
		void _initJoints(Joint* j);
		void _setJointPositions(KeyFrame* f);
	};

	struct KeyFrameComponent {
		glm::vec3 posOnParent, anchorPos;
		WorldCoordinate position;
		glm::quat rot;

		friend bool operator ==(KeyFrameComponent a, KeyFrameComponent b);
	};

	inline bool operator ==(KeyFrameComponent a, KeyFrameComponent b) {
		return a.posOnParent == b.posOnParent &&
		       a.position    == b.position &&
			   a.anchorPos   == b.anchorPos &&
			   a.rot         == b.rot;
	}

	struct KeyFrameTween {
		Tween<WorldCoordinate> position;
		Tween<glm::quat> rot;
		Tween<glm::vec3> posOnParent, anchorPos;

		void tick(float delta);
	};

	struct KeyFrame {
		std::unordered_map<std::string, KeyFrameComponent> components; // joint name maps to joint's position
		// The time says how long the keyframe takes to go to the next. So if frame 0 has time=3,
		//  then the transition between 0 and 1 takes 3 seconds. The last frame's time is used when
		//  either beginning a new animation, or repeating, in either case, it will be the time
		//  between the last frame, and the first frame of the next loop/animation.
		float time;

		KeyFrame(std::unordered_map<std::string, KeyFrameComponent> components, float time = 1.f): components(components), time(time) {}
	};

	struct Animation {
		unsigned int curFrame = 0;
		float time = 0.f;
		bool repeat = true, done = false;
		float speed = 1.f;
		float curTweenTimeLeft = 0; // Used by Animation Manager to transition

		AnimationManager* host = nullptr;
		std::vector<KeyFrame*> frames;
		std::unordered_map<std::string, KeyFrameTween> tweens;

		Animation(std::vector<KeyFrame*> frames);

		void setupTween(); // Set the tween up with next as curFrame+1
		void setupTween(Skeleton * skel, KeyFrame * next);
		void tick(float delta);

		virtual ~Animation();
	};

}


#endif /* SRC_ANIM_SKELETON_H_ */
