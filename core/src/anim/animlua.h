/*
 * animlua.h
 *
 *  Created on: Sep 20, 2015
 *      Author: Daniel
 */

#ifndef ANIMLUA_H_
#define ANIMLUA_H_

#include "vluautil.h"

namespace vox {

	struct JointMotionObject : public LuaObject {
		std::string jointName;
		lvec3 posOnParent, rotAnchor, offset;
		lvec4 rotation; // quat

		JointMotionObject();
	};

	struct LuaKeyFrame : public LuaObject {
		LuaArray<JointMotionObject> jointMotionList;
		float time;

		LuaKeyFrame();
	};

	struct LuaAnimation : public LuaObject {
		LuaArray<LuaKeyFrame> frames;
		bool rep;

		LuaAnimation();
	};

	struct LuaSkeleton : public LuaObject {
		LuaArray<std::string> jointNames, jointParents, jointMeshes;

		LuaArray<std::string> animNames;
		LuaArray<LuaAnimation> animData;

		LuaSkeleton();
	};

	struct Skeleton;
	Skeleton * constructSkeleton(LuaSkeleton s);
}



#endif /* ANIMLUA_H_ */
