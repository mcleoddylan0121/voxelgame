/*
 * console.h
 *
 * New console updated to include a MessageHandler and MessageTypes
 *
 *  Created on: Mar 22, 2015
 *      Author: Dani
 */

#ifndef CONSOLE_H_
#define CONSOLE_H_

#include <string>

namespace vox {
	enum MessageType {
		INFORMATION, ERROR, DEBUG, WARNING, TEMPORARY, TEMP = TEMPORARY, FATALERROR,
		SDLERROR, GLERROR, GLINFO, INVALIDMESSAGETYPE,
		SDLISSHIT, CHUNKCREATION,
		IOERROR,
		LUAERROR, LUA, LUAIOERROR,
		FONTERROR, FONTINFO, FINAL_MESSAGETYPE_ENTRY
	};

	// MessageType should be any value other than FINAL_MESSAGETYPE_ENTRY. That
	// extra enum value is necessary because C++ has no enum size check. Furthermore,
	// use 0 as the messagetype (value of INFORMATION) to display no MessageType.
	void print(MessageType t, std::string message);
	void println(MessageType t, std::string message);
	void printf(MessageType t, std::string message, ...);
	void printlnf(MessageType t, std::string message, ...);

	// For internal use only
	void printMessageType(MessageType t, char* buf, int& bufIndex);
	void printTimestamp(char* buf, int& bufIndex);

	void initConsole();
	void endConsole();

	// This class handles all of the MessageTypes. Use setMessageOn(type, false) to disable
	// a messagetype from appearing in the console and a value of true to enable it.
	class MessageStateHandler {
	private:
		bool messageOn[FINAL_MESSAGETYPE_ENTRY];
	public:
		MessageStateHandler();
		virtual ~MessageStateHandler();

		bool isMessageOn(MessageType t);
		void setMessageOn(MessageType t, bool b);
	};

	extern MessageStateHandler mMessageStateHandler; // Defined in console.cpp

	#define printLineNumber() printlnf(TEMP,"Line %i in file %s", __LINE__, __FILE__)
}



#endif /* CONSOLE_H_ */
