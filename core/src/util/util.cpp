/*
 * util.cpp
 *
 *  Created on: Apr 12, 2015
 *      Author: Dani
 */

#include "util.h"
#include "geometry.h"

#include <cmath>
#include <stdio.h>
#include <stdlib.h>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <unordered_map>

namespace vox {

	float constrain(float n, float lowerBound, float upperBound) {
		if (n < lowerBound) n = lowerBound;
		else if (n > upperBound) n = upperBound;
		return n;
	}

	float wrapAngle(float n) {
		return n - PI * 2 * floor(n / PI / 2);
	}

	int char4ToIntBE(char * beg) {
		return ((unsigned char)(beg[3]) << 24) + ((unsigned char)(beg[2]) << 16) + ((unsigned char)(beg[1]) << 8) + (unsigned char)(beg[0]);
	}

	int getPacketOffs(char * beg, int numBytes, int packetSize, const char * cmp) {
		for (int i = 0; i < numBytes; i += packetSize) {
			if (memcmp(&beg[i], cmp, packetSize) == 0) return i;
		}
		return -1;
	}

	struct Timestamp {
		std::string weekday, month, day, hr, min, sec, year, monthnum;
		static std::unordered_map<std::string, std::string> weekdays;
		static std::unordered_map<std::string, std::string> months;
		static std::unordered_map<std::string, std::string> monthNums;

		std::string getFullWeekday() { return weekdays [weekday]; }
		std::string getFullMonth  () { return months   [month]; }
		std::string getMonthNumber() { return monthNums[month]; }

		Timestamp(std::string raw = getTimeStamp()) {
			weekday = raw.substr(0, 3);
			month = raw.substr(4, 3);
			day = raw.substr(8, 2);
			hr = raw.substr(11, 2);
			min = raw.substr(14, 2);
			sec = raw.substr(17, 2);
			year = raw.substr(20, 4);
		}
	};

	std::unordered_map<std::string, std::string> Timestamp::weekdays = {
			{"Sun", "Sunday"},
			{"Mon", "Monday"},
			{"Tue", "Tuesday"},
			{"Wed", "Wednesday"},
			{"Thu", "Thursday"},
			{"Fri", "Friday"},
			{"Sat", "Saturday"}
	};

	std::unordered_map<std::string, std::string> Timestamp::months = {
			{"Jan", "January"},
			{"Feb", "February"},
			{"Mar", "March"},
			{"Apr", "April"},
			{"May", "May"},
			{"Jun", "June"},
			{"Jul", "July"},
			{"Aug", "August"},
			{"Sep", "September"},
			{"Oct", "October"},
			{"Nov", "November"},
			{"Dec", "December"}
	};

	std::unordered_map<std::string, std::string> Timestamp::monthNums = {
			{"Jan", "01"},
			{"Feb", "02"},
			{"Mar", "03"},
			{"Apr", "04"},
			{"May", "05"},
			{"Jun", "06"},
			{"Jul", "07"},
			{"Aug", "08"},
			{"Sep", "09"},
			{"Oct", "10"},
			{"Nov", "11"},
			{"Dec", "12"}
	};

	std::string getTimeStamp() {
		time_t rawtime;
		time (&rawtime);
		std::string timestamp = ctime(&rawtime);
		return timestamp.erase(timestamp.length()-1);
	}

	std::string getFileFormattedTimeStamp() {
		Timestamp ts;
		std::string ret = ts.year + '-' + ts.getMonthNumber() + '-' + ts.day + '_' + ts.hr + '-' + ts.min + '-' + ts.sec;
		return ret;
	}

	std::string getDisplayDate() {
		Timestamp ts;
		std::string ret = ts.getFullWeekday() + ", " + ts.getFullMonth() + " " + ts.day + ", " + ts.year;
		return ret;
	}

	std::string getDisplayTime() {
		Timestamp ts;
		std::string ret = ts.hr + ":" + ts.min + ":" + ts.sec;
		return ret;
	}

	void Rand::seed(Rand::Seed seed) {
		std::seed_seq seq(seed.begin(), seed.end());
		rng = std::minstd_rand(seq);
	}

	Rand::Rand() {
		auto sd = std::vector<uint_least32_t>{
			(uint_least32_t)rand(),
			(uint_least32_t)rand(),
			(uint_least32_t)rand(),
			(uint_least32_t)rand(),
			(uint_least32_t)rand()
		};
		seed(Rand::Seed{sd});
	}

	Rand::Rand(Rand::Seed seed) {
		this->seed(seed);
	}

	Rand Rand::useRandSeed(Rand seed) {
		return Rand(seed.getSeedSeq(10));
	}

	Rand::Seed Rand::getSeedSeq(int len) {
		std::vector<uint_least32_t> vec;
		for(int i = 0; i < len; i++)
			vec.push_back((*this)());
		return Seed{vec};
	}

	uint32_t Rand::operator ()() {
		return rng();
	}

	std::uniform_int_distribution<int> Rand::intDist;
	std::uniform_real_distribution<float> Rand::floatDist;

	int Rand::getInt(int min, int max) {
		return intDist(rng, std::uniform_int_distribution<int>::param_type(min, max));
	}

	float Rand::getFloat(float min, float max) {
		return floatDist(rng, std::uniform_real_distribution<float>::param_type(min, max));
	}

	float Rand::getFloat(glm::vec2 minmax) {
		return getFloat(minmax.x, minmax.y);
	}

	bool Rand::flipCoin(float probability) {
		return getFloat(0, 1) < probability;
	}
}
