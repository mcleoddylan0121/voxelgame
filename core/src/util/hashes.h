/*
 * hashes.h
 *
 *  Created on: Dec 31, 2015
 *      Author: Daniel
 */

#ifndef CORE_SRC_UTIL_HASHES_H_
#define CORE_SRC_UTIL_HASHES_H_

namespace vox {

	struct ChunkHash { size_t operator () (const ChunkCoordinate& in) const; };
	struct LoDHash { size_t operator () (const std::pair<ChunkCoordinate, int>& in) const; };

}



#endif /* CORE_SRC_UTIL_HASHES_H_ */
