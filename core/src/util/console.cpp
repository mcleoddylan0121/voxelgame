/*
 * console.cpp
 *
 * New console updated to include a MessageHandler and MessageTypes
 *
 *  Created on: Mar 22, 2015
 *      Author: Dani
 */

#include <cstdio>
#include <string>
#include <stdarg.h>
#include <iterator>
#include <unordered_map>
#include <functional>
#include <type_traits>
#include <fstream>

#include "console.h"
#include "util.h"
#include "sdlcontroller.h"

namespace vox {

	const int MAX_BUFSIZE = 9001;

	// Not a great way of doing it, but a lot better than a switch statement
	std::unordered_map<MessageType, std::string, std::hash<int>> mMessageTypeValues = {

			// This is a map, and not an array, because it makes initialization a lot clearer
			{INFORMATION, 	"" },

			{FATALERROR, 	"FATAL ERROR"},
			{ERROR, 		"ERROR"},
			{DEBUG, 		"DEBUG"},
			{WARNING, 		"WARNING"},
			{TEMPORARY, 	"TEMPORARY"},
			{SDLERROR, 		"SDL ERROR"},
			{GLERROR, 		"OPENGL ERROR"},
			{GLINFO, 		"OPENGL"},
			{IOERROR, 		"I/O ERROR"},
			{CHUNKCREATION, "CHUNK CREATION"},
			{FONTERROR, "FONT ERROR"},
			{FONTINFO, "FONT INFO"},

			{LUA, "LUA"},
			{LUAERROR, "LUA ERROR"},
			{LUAIOERROR, "LUA IO ERROR"},

			{SDLISSHIT,     "SDL IS SHIT"},

			{INVALIDMESSAGETYPE, "INVALID MESSAGE TYPE"},

			{FINAL_MESSAGETYPE_ENTRY, ""}
	};

	MessageStateHandler mMessageStateHandler; // Prepend global variables with "m"


	std::ofstream outfile;
	void initConsole() { outfile.open("log.txt", std::ios_base::app); }
	void endConsole() { outfile.close(); }

	void print(MessageType t, std::string s) {
		printf(t, s);
	}

	void println(MessageType t, std::string s) {
		printf(t, "%s\n", s.c_str());
	}

	void internalprintf(MessageType t, std::string s, va_list args) {
		if (!mMessageStateHandler.isMessageOn(t)) return;

		char buf[MAX_BUFSIZE];
		int ind = 0;
		printTimestamp(buf, ind);
		printMessageType(t, buf, ind);
		vsprintf(buf + ind, s.c_str(), args);

		outfile << buf;
		std::printf(buf);


		if (t == FATALERROR) {
			mSDL->showErrorMessageBox("Fatal Error", std::string(buf));
			exit(-1);
		}
	}

	void printf(MessageType t, std::string s, ...) {
		va_list args;
		va_start (args, s);
		internalprintf(t, s, args);
		va_end (args);
	}

	void printlnf(MessageType t, std::string s, ...) {
		va_list args;
		va_start (args, s);
		internalprintf(t, s + "\n", args);
		va_end (args);
	}

	void printTimestamp(char* buf, int& bufIndex) {
		std::string tm = "["+getTimeStamp()+"] ";
		snprintf(buf + bufIndex, MAX_BUFSIZE - bufIndex, tm.c_str());
		bufIndex += tm.size();
	}

	// The new implementation looks up an std::unordered map. Not great, but not terrible either.
	void printMessageType(MessageType t, char* buf, int& bufIndex) {
		if(t == SDLISSHIT) {
			for(int i = 0; i<100; i++) {
				printlnf(TEMPORARY, "SDL IS SHIT %i", i);
			}
		}
		if(t != INFORMATION) { // If we're only printing information, we don't want the diamond brackets
			auto text = mMessageTypeValues.find(t);
			if(text == mMessageTypeValues.end()) text = mMessageTypeValues.find(INVALIDMESSAGETYPE);
			snprintf(buf + bufIndex, MAX_BUFSIZE - bufIndex, "<%s> ", text->second.c_str());
			bufIndex += text->second.size() + 3;
		}
	}

	// MessageStateHandler, use the corresponding global variable to enable/disable
	// certain message types. For instance, disable "DEBUG" in the consumer copy.
	MessageStateHandler::MessageStateHandler() {
		for (int i = 0; i < FINAL_MESSAGETYPE_ENTRY; i++)
			this->messageOn[i] = true;
	}
	MessageStateHandler::~MessageStateHandler() {}

	bool MessageStateHandler::isMessageOn(MessageType t) {
		if (t == FINAL_MESSAGETYPE_ENTRY) return false;
		return this->messageOn[t];
	}

	void MessageStateHandler::setMessageOn(MessageType t, bool b) {
		if (t == FINAL_MESSAGETYPE_ENTRY) return;
		this->messageOn[t] = b;
	}
}
