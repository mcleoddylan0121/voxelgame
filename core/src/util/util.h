/*
 * util.h
 *
 *  Created on: Apr 12, 2015
 *      Author: Dani
 */

#ifndef UTIL_H_
#define UTIL_H_

#include "console.h"
#include "vglm.h"

#include <cstddef>
#include <functional>
#include <random>
#include <sstream>
#include <string>
#include <cassert>
#include <sstream>
#include <string>

namespace vox {

#define toString( x ) dynamic_cast< std::ostringstream & >( \
        ( std::ostringstream() << std::dec << x ) ).str()

	const float PI = 3.1415926535;

	float constrain(float n, float lowerBound, float upperBound);
	float wrapAngle(float n);

	int char4ToIntBE(char * beg); // Big Endian

	// Returns the byte offset if there is one and -1 if there isn't
	int getPacketOffs(char * beg, int numBytes, int packetSize, const char * cmp);

	inline void voxAssertNoMacro(bool statement, std::string msg, const char* file, const char* fn, int ln) {
		if(!statement) printlnf(ERROR, "Assert failed! %s, in file %s, in function %s, on line %i", msg.c_str(), file, fn, ln);
		assert(statement);
	}

#ifdef RELEASE
#define vassert(s, m) ; // do nothing
#else
#define vassert(s, m)  voxAssertNoMacro(s, m, __FILE__, __FUNCTION__, __LINE__)
#endif


	template<typename T> T abs(T val) { return val > T()? val : -val; }
	template<typename T> bool equals(T a, T b, T error) { return abs(a - b) < error; }
	template<typename T> T sgn(T val) { if (val == 0) return 0; else return val / abs(val); }

	template<typename T> void swapValues(T& a, T& b) {
		T temp = a;
		a = b;
		b = temp;
	}

	std::string getTimeStamp();
	std::string getFileFormattedTimeStamp(); // This one plays nicely with the file system
	std::string getDisplayDate();
	std::string getDisplayTime();

	// RNG to make our lives easier
	class Rand {
	public:
		typedef std::vector<uint_least32_t> Seed;

		Rand(); // Uses random seed.
		Rand(Seed seed);
		uint32_t operator ()();

		static Rand useRandSeed(Rand seed); // use a rand as a seed for a new one

		Seed getSeedSeq(int len);

		int getInt(int min, int max); // returns [min, max]
		float getFloat(float min, float max); // returns [min, max)
		float getFloat(glm::vec2 minmax); // returns [minmax.x, minmax.y)
		bool flipCoin(float probability); // probability of true

	private:
		static std::uniform_int_distribution<int> intDist;
		static std::uniform_real_distribution<float> floatDist;
		std::minstd_rand rng;

		void seed(Seed seed);
	};

	class Accumulator {
	private:
		float elapsed = 0, limit;
	public:
		Accumulator(float limit): limit(limit) {}
		void accumulate(float delta) { elapsed += delta; }
		bool attempt() {
			if(elapsed >= limit) {
				elapsed -= limit;
				return true;
			}
			return false;
		}
	};

	template <typename T>
	struct nullifier {
		nullifier(T* in = nullptr) { _ptr = in; }
		nullifier<T> operator= (T* in) { _ptr = in; return *this; }

		void nullify() { _ptr = nullptr; }
		bool isNull() { return _ptr == nullptr; }
		bool exists() { return !isNull(); }

		bool operator ==(T* rhs) const { return rhs == _ptr; }
		bool operator !=(T* rhs) const { return rhs != _ptr; }

		T* operator->() { return _ptr; }
		T& operator*() { return *_ptr; }

	private:
		T* _ptr = nullptr;
	};
}

#endif /* UTIL_H_ */
