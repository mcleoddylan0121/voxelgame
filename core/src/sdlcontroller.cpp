/*
 * sdlcontroller.cpp
 *
 *  Created on: Mar 22, 2015
 *      Author: Dylan
 */

#include "sdlcontroller.h"
#include "console.h"
#include "gameloop.h"
#include "guimanager.h"
#include "guilua.h"
#include "glcontroller.h"
#include "settingsmanager.h"
#include "filesystem.h"
#include "io.h"

#include <chrono>
#include <string>

namespace vox { // 
	SDLController * mSDL;

	// This will handle any general errors
	bool SDLController::handleError(const char * func, int line) {
		const char * error = SDL_GetError();
		if (*error /* First character */!= '\0') {
			printf(SDLERROR, "In %s on line %i: %s", func, line, error);

			SDL_ClearError();
			return true;
		}

		return false;
	}

	// Create our window, and the GL context
	SDLController::SDLController() {
		viewportWidth = 0;
		viewportHeight = 0;
		windowWidth = 0;
		windowHeight = 0;
		glContext = 0;
		window = 0;

		mouseGrabbed = false;

		// Initialize window
		if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
			printf(SDLERROR, "SDL could not be initialized: %s\n", SDL_GetError());
			return;
		}

		_c = ClockType::now();
		sdlReady = true;
	}

	void SDLController::makeWindow(int viewportWidth, int viewportHeight, int windowWidth, int windowHeight, bool fullScreen) {
		if (window) return;

		this->viewportWidth = viewportWidth;
		this->viewportHeight = viewportHeight;

		// Use the core profile
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

		// Use OpenGL 3.3
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);
		SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);

		// Turn on double buffering
		SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
		SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);

		//Create our window with specified width and height
		window = SDL_CreateWindow("Vura", SDL_WINDOWPOS_CENTERED,
		SDL_WINDOWPOS_CENTERED, windowWidth, windowHeight,
				SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL
						| (fullScreen ? SDL_WINDOW_MAXIMIZED | SDL_WINDOW_BORDERLESS : SDL_WINDOW_RESIZABLE));

		if (fullScreen) {
			SDL_DisplayMode display;
			// Get desktop screen resolution
			if(SDL_GetDesktopDisplayMode(0, &display) == 0) {
				this->windowWidth = display.w;
				this->windowHeight = display.h;
			} else {
				printf(SDLERROR, "Error getting desktop displaymode! \n");
				handleError(__FUNCTION__, __LINE__);
				// In case of error, use provided dimensions
				this->windowWidth  = windowWidth;
				this->windowHeight = windowHeight;
			}
		}
		else {
			// Use provided dimensions
			this->windowWidth = windowWidth;
			this->windowHeight = windowHeight;
		}

		if (!window) // Something very wrong must have occurred
			printf(SDLERROR, "Couldn't create windows: %s\n", SDL_GetError());

		glContext = SDL_GL_CreateContext(window);	// Add OpenGL context

		handleError(__FUNCTION__, __LINE__);
		SDL_GL_MakeCurrent(window, glContext);
		handleError(__FUNCTION__, __LINE__);

		setVSync(mSettings->vsync);
	}

	SDLController::~SDLController() {
		sdlReady = false;
		SDL_GL_DeleteContext(glContext);
		SDL_DestroyWindow(window);
		SDL_Quit();
	}

	void SDLController::clean() {
		SDL_Event event;	// Discard all false events
		while (SDL_PollEvent(&event)) {}
		mouse.dx = mouse.dy = mouse.sdx = mouse.sdy = 0;
		for (unsigned int i = 0; i < sizeof(mouseButtons) / sizeof(unsigned char); i++)
			lastMouseButtons[i] = mouseButtons[i] = false;
	}

	void SDLController::sleep(long ms) {
		SDL_Delay(ms);
	}

	void SDLController::swapBuffer() {
		SDL_GL_SwapWindow(window);
	}

	SDLController::ClockType::time_point SDLController::getTimePoint() {
		return ClockType::now();
	}

	void SDLController::setMouseGrabbed(bool grabbed) {
		int err = SDL_SetRelativeMouseMode(grabbed ? SDL_TRUE : SDL_FALSE);
		clean();
		if (err < 0) {
			// Relative mode not supported
			// TODO: Add workaround here
			return;
		}
		mouseGrabbed = grabbed;
	}

	void SDLController::setVSync(bool on) {
		SDL_GL_SetSwapInterval(on ? 1 : 0);
	}

	void SDLController::setTextMode(bool on) {
		if (on)
			SDL_StartTextInput();
		else
			SDL_StopTextInput();
	}

	bool SDLController::isMouseGrabbed() { return mouseGrabbed; }

	void SDLController::setTitle(const std::string& title) {
		SDL_SetWindowTitle(window, title.c_str());
	}

	void SDLController::setFullscreen(bool b) {
		if (b) {
			SDL_DisplayMode display;
			if(SDL_GetDesktopDisplayMode(0, &display) == 0) {
				windowWidth = display.w;
				windowHeight = display.h;
			}
		} else {
			windowWidth = viewportWidth;
			windowHeight = viewportHeight;
		}

		SDL_SetWindowFullscreen(window, (b ? SDL_WINDOW_FULLSCREEN_DESKTOP : 0));
	}

	void SDLController::setResolution(int width, int height) {
		viewportWidth = width;
		viewportHeight = height;

		SDL_SetWindowSize(window, width, height);
	}

	void SDLController::setIcon(const Asset& asset) {
		SDL_Surface * s = loadSurface(asset);
		SDL_SetWindowIcon(window, s);
		SDL_FreeSurface(s);
	}

	void SDLController::showErrorMessageBox(std::string title, std::string message) {
		SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, title.c_str(), message.c_str(), NULL);
	}

	bool SDLController::handleEvents() {
		SDL_Event event;
		float xScale = viewportWidth / (float)windowWidth;
		float yScale = viewportHeight / (float)windowHeight;

		while (SDL_PollEvent(&event)) {
			switch (event.type) {
			case SDL_QUIT: return false;
			case SDL_KEYDOWN:
				if (event.key.keysym.sym == SDLK_F4 && (event.key.keysym.mod & KMOD_ALT)) return false;
				gui::GUILibrary::_m->keyDown(event.key.keysym.sym);

				if (keys[event.key.keysym.sym] == false) {
					gui::GUILibrary::_m->keyPressed(event.key.keysym.sym);
					keys[event.key.keysym.sym] = true;
				}
				break;
			case SDL_KEYUP:
				keys[event.key.keysym.sym] = false;
				gui::GUILibrary::_m->keyReleased(event.key.keysym.sym);
				break;
			case SDL_MOUSEBUTTONDOWN:
				gui::GUILibrary::_m->mouseDown(glm::ivec2{mouse.x, mouse.y}, event.button.button);

				if (mouseButtons[event.button.button] == false) {
					mouseButtons[event.button.button] = true;
					gui::GUILibrary::_m->mousePressed(glm::ivec2{mouse.x, mouse.y}, event.button.button);
				}
				break;
			case SDL_MOUSEBUTTONUP:
				mouseButtons[event.button.button] = false;
				gui::GUILibrary::_m->mouseReleased(glm::ivec2{mouse.x, mouse.y}, event.button.button);
				break;
			case SDL_MOUSEMOTION:
				mouse.x = event.motion.x * xScale;
				mouse.y = (mSDL->windowHeight - event.motion.y) * yScale;
				mouse.dx += event.motion.xrel;
				mouse.dy += -event.motion.yrel; // Up is a positive change in y now
				gui::GUILibrary::_m->mouseMoved(glm::ivec2{mouse.dx, mouse.dy});
				break;
			case SDL_MOUSEWHEEL:
				mouse.sdx += event.wheel.x;
				mouse.sdy += event.wheel.y;
				break;
			case SDL_WINDOWEVENT:
				if (event.window.event == SDL_WINDOWEVENT_RESIZED) {
					if (event.window.event == SDL_WINDOWEVENT_RESIZED)
						windowWidth = event.window.data1;
						windowHeight = event.window.data2;
				}
				break;
			case SDL_TEXTINPUT:
				gui::GUILibrary::_m->textTyped(std::string(event.text.text));
				break;
			}
		}

		return true;
	}

	const Mouse& SDLController::getMouse() {
		return mouse;
	}

	void SDLController::updateUtilities() {
		mouse.dx = mouse.dy = mouse.sdx = mouse.sdy = 0;
		for (unsigned int i = 0; i < sizeof(mouseButtons) / sizeof(unsigned char); i++)
			lastMouseButtons[i] = mouseButtons[i];
		lastKeys = keys;
	}

	bool SDLController::inputPressed(std::string name) {
		int button = mSettings->getBinding(name);

		// If the button is a mouse button, check that. If not, check the keyboard.
		if (button & MOUSE_BUTTON_MASK) return _mouseButtonPressed(button ^ MOUSE_BUTTON_MASK);
		else return _keyPressed(button);
	}
	bool SDLController::inputTyped(std::string name) {
		int button = mSettings->getBinding(name);

		// If the button is a mouse button, check that. If not, check the keyboard.
		if (button & MOUSE_BUTTON_MASK) return _mouseButtonTyped(button ^ MOUSE_BUTTON_MASK);
		else return _keyTyped(button);
	}

	bool SDLController::_keyPressed(int key) {
		return keys[key];
	}
	bool SDLController::_keyTyped(int key) {
		return (keys[key] && !lastKeys[key]);
	}
	bool SDLController::_mouseButtonPressed(unsigned char m) {
		return mouseButtons[m];
	}
	bool SDLController::_mouseButtonTyped(unsigned char m) {
		return (mouseButtons[m] && !lastMouseButtons[m]);
	}

	bool SDLController::isSDLInitialized() {
		return sdlReady;
	}

	bool SDLController::isWindowMade() {
		return (window);
	}

	// Mutex
	Mutex::~Mutex() {
		if (mSDL->isSDLInitialized() && _mutex)
			SDL_DestroyMutex(_mutex);
	}

	int Mutex::init() {
		if (mSDL->isSDLInitialized()) {
			_mutex = SDL_CreateMutex();
			if (_mutex) return 0;
		} else println(SDLERROR, "Could not initialize mutex: SDL hasn't been initialized.");
		return -1;
	}

	int Mutex::lock() {
		if (mSDL->isSDLInitialized()) {
			return SDL_LockMutex(_mutex);
		} else println(SDLERROR, "Could not lock mutex: SDL hasn't been initialized.");
		return -1;
	}

	int Mutex::unlock() {
		if (mSDL->isSDLInitialized()) {
			return SDL_UnlockMutex(_mutex);
		} else println(SDLERROR, "Could not unlock mutex: SDL hasn't been initialized.");
		return -1;
	}
} /* namespace vox */
