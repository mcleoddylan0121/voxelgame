/*
 * noiseFunctions.cpp
 *
 *  Created on: Apr 19, 2015
 *      Author: Dylan
 */

#include "noise.h"

// Enter at your own risk.
namespace vox {

namespace noise {

	Noise::value_t dot(const int8_t* g, Noise::value_t x, Noise::value_t y) {
		return g[0] * x + g[1] * y;
	}

	int fastFloor(Noise::value_t x) {
		return x > 0 ? (int) x : (int) x - 1;
	}

	/*
	 * SIMPLEX NOISE
	 */
	const Noise::value_t F2 = 0.5 * (sqrt(3.0f) - 1.0);
	const Noise::value_t G2 = (3.0 - sqrt(3.0f)) / 6.0;
	const int8_t grad3[12][3] = {
			{ 1,   1,  0 }, { -1,  1,  0 }, {  1, -1,  0 },
			{-1,  -1,  0 }, {  1,  0,  1 }, { -1,  0,  1 },
			{ 1,   0, -1 }, { -1,  0, -1 }, {  0,  1,  1 },
			{ 0,  -1,  1 }, {  0,  1, -1 }, {  0, -1, -1 }
	};

	Noise::value_t Simplex::genNoise2D(const glm::vec2& pos) {
		value_t n0, n1, n2;
		value_t s = (pos.x + pos.y) * F2;
		int i = fastFloor(pos.x + s);
		int j = fastFloor(pos.y + s);
		value_t t = (i + j) * G2;
		value_t X0 = i - t;
		value_t Y0 = j - t;
		value_t x0 = pos.x - X0;
		value_t y0 = pos.y - Y0;
		int i1, j1;
		if (x0 > y0) {
			i1 = 1;
			j1 = 0;
		} else {
			i1 = 0;
			j1 = 1;
		}
		value_t x1 = x0 - i1 + G2;
		value_t y1 = y0 - j1 + G2;
		value_t x2 = x0 - 1.0 + 2.0 * G2;
		value_t y2 = y0 - 1.0 + 2.0 * G2;
		int ii = i & 255;
		int jj = j & 255;
		int gi0 = permMod12[ii + perm[jj]];
		int gi1 = permMod12[ii + i1 + perm[jj + j1]];
		int gi2 = permMod12[ii + 1 + perm[jj + 1]];
		value_t t0 = 0.5 - x0 * x0 - y0 * y0;
		if (t0 < 0)
			n0 = 0.0;
		else {
			t0 *= t0;
			n0 = t0 * t0 * dot(grad3[gi0], x0, y0);
		}
		value_t t1 = 0.5 - x1 * x1 - y1 * y1;
		if (t1 < 0)
			n1 = 0.0;
		else {
			t1 *= t1;
			n1 = t1 * t1 * dot(grad3[gi1], x1, y1);
		}
		value_t t2 = 0.5 - x2 * x2 - y2 * y2;
		if (t2 < 0)
			n2 = 0.0;
		else {
			t2 *= t2;
			n2 = t2 * t2 * dot(grad3[gi2], x2, y2);
		}
		return (((70.0 * (n0 + n1 + n2)) + 1.f) / 2.f);
	}


	const Noise::value_t F3 = 1.0/3.0;
	const Noise::value_t G3 = 1.0/6.0; // Very nice and simple unskew factor, too

	Noise::value_t dot(const int8_t * g, Noise::value_t x, Noise::value_t y, Noise::value_t z) { return g[0]*x + g[1]*y + g[2]*z; }

	Noise::value_t Simplex::genNoise3D(const glm::vec3& pos) {
		value_t n0, n1, n2, n3; // Noise contributions from the four corners
		// Skew the input space to determine which simplex cell we're in
		value_t s = (pos.x + pos.y + pos.z)*F3; // Very nice and simple skew factor for 3D
		int i = fastFloor(pos.x+s);
		int j = fastFloor(pos.y+s);
		int k = fastFloor(pos.z+s);
		value_t t = (i+j+k)*G3;
		value_t X0 = i-t; // Unskew the cell origin back to (x,y,z) space
		value_t Y0 = j-t;
		value_t Z0 = k-t;
		value_t x0 = pos.x-X0; // The x,y,z distances from the cell origin
		value_t y0 = pos.y-Y0;
		value_t z0 = pos.z-Z0;
		// For the 3D case, the simplex shape is a slightly irregular tetrahedron.
		// Determine which simplex we are in.
		int i1, j1, k1; // Offsets for second corner of simplex in (i,j,k) coords
		int i2, j2, k2; // Offsets for third corner of simplex in (i,j,k) coords
		if(x0>=y0) {
		if(y0>=z0)
		{ i1=1; j1=0; k1=0; i2=1; j2=1; k2=0; } // X Y Z order
		else if(x0>=z0) { i1=1; j1=0; k1=0; i2=1; j2=0; k2=1; } // X Z Y order
		else { i1=0; j1=0; k1=1; i2=1; j2=0; k2=1; } // Z X Y order
		}
		else { // x0<y0
		if(y0<z0) { i1=0; j1=0; k1=1; i2=0; j2=1; k2=1; } // Z Y X order
		else if(x0<z0) { i1=0; j1=1; k1=0; i2=0; j2=1; k2=1; } // Y Z X order
		else { i1=0; j1=1; k1=0; i2=1; j2=1; k2=0; } // Y X Z order
		}
		// A step of (1,0,0) in (i,j,k) means a step of (1-c,-c,-c) in (x,y,z),
		// a step of (0,1,0) in (i,j,k) means a step of (-c,1-c,-c) in (x,y,z), and
		// a step of (0,0,1) in (i,j,k) means a step of (-c,-c,1-c) in (x,y,z), where
		// c = 1/6.
		value_t x1 = x0 - i1 + G3; // Offsets for second corner in (x,y,z) coords
		value_t y1 = y0 - j1 + G3;
		value_t z1 = z0 - k1 + G3;
		value_t x2 = x0 - i2 + 2.0*G3; // Offsets for third corner in (x,y,z) coords
		value_t y2 = y0 - j2 + 2.0*G3;
		value_t z2 = z0 - k2 + 2.0*G3;
		value_t x3 = x0 - 1.0 + 3.0*G3; // Offsets for last corner in (x,y,z) coords
		value_t y3 = y0 - 1.0 + 3.0*G3;
		value_t z3 = z0 - 1.0 + 3.0*G3;
		// Work out the hashed gradient indices of the four simplex corners
		int ii = i & 255;
		int jj = j & 255;
		int kk = k & 255;
		int gi0 = perm[ii+perm[jj+perm[kk]]] % 12;
		int gi1 = perm[ii+i1+perm[jj+j1+perm[kk+k1]]] % 12;
		int gi2 = perm[ii+i2+perm[jj+j2+perm[kk+k2]]] % 12;
		int gi3 = perm[ii+1+perm[jj+1+perm[kk+1]]] % 12;
		// Calculate the contribution from the four corners
		value_t t0 = 0.6 - x0*x0 - y0*y0 - z0*z0;
		if(t0<0) n0 = 0.0;
		else {
			t0 *= t0;
			n0 = t0 * t0 * dot(grad3[gi0], x0, y0, z0);
		}
		value_t t1 = 0.6 - x1*x1 - y1*y1 - z1*z1;
		if(t1<0) n1 = 0.0;
		else {
			t1 *= t1;
			n1 = t1 * t1 * dot(grad3[gi1], x1, y1, z1);
		}
		value_t t2 = 0.6 - x2*x2 - y2*y2 - z2*z2;
		if(t2<0) n2 = 0.0;
		else {
			t2 *= t2;
			n2 = t2 * t2 * dot(grad3[gi2], x2, y2, z2);
		}
		value_t t3 = 0.6 - x3*x3 - y3*y3 - z3*z3;
		if(t3<0) n3 = 0.0;
		else {
			t3 *= t3;
			n3 = t3 * t3 * dot(grad3[gi3], x3, y3, z3);
		}
		// Add contributions from each corner to get the final noise value.
		// The result is scaled to stay just inside [0,1]
		return (((32.0*(n0 + n1 + n2 + n3)) + 1.f) / 2.f);
	}


	/*
	 * OPENSIMPLEX NOISE
	 */

	const Noise::value_t STRETCH_CONSTANT_2D = -0.211324865405187;    //(1/Math.sqrt(2+1)-1)/2;
	const Noise::value_t SQUISH_CONSTANT_2D = 0.366025403784439;      //(Math.sqrt(2+1)-1)/2;
	const Noise::value_t STRETCH_CONSTANT_3D = -1.0 / 6;              //(1/Math.sqrt(3+1)-1)/3;
	const Noise::value_t SQUISH_CONSTANT_3D = 1.0 / 3;                //(Math.sqrt(3+1)-1)/3;
	const Noise::value_t STRETCH_CONSTANT_4D = -0.138196601125011;    //(1/Math.sqrt(4+1)-1)/4;
	const Noise::value_t SQUISH_CONSTANT_4D = 0.309016994374947;      //(Math.sqrt(4+1)-1)/4;

	const Noise::value_t NORM_CONSTANT_2D = 47;
	const Noise::value_t NORM_CONSTANT_3D = 103;
	const Noise::value_t NORM_CONSTANT_4D = 30;

		//Gradients for 2D. They approximate the directions to the
	//vertices of an octagon from the center.
	char gradients2D[] = {
		 5,  2,    2,  5,
		-5,  2,   -2,  5,
		 5, -2,    2, -5,
		-5, -2,   -2, -5,
	};

	//Gradients for 3D. They approximate the directions to the
	//vertices of a rhombicuboctahedron from the center, skewed so
	//that the triangular and square facets can be inscribed inside
	//circles of the same radius.
	char gradients3D[] = {
		-11,  4,  4,     -4,  11,  4,    -4,  4,  11,
		 11,  4,  4,      4,  11,  4,     4,  4,  11,
		-11, -4,  4,     -4, -11,  4,    -4, -4,  11,
		 11, -4,  4,      4, -11,  4,     4, -4,  11,
		-11,  4, -4,     -4,  11, -4,    -4,  4, -11,
		 11,  4, -4,      4,  11, -4,     4,  4, -11,
		-11, -4, -4,     -4, -11, -4,    -4, -4, -11,
		 11, -4, -4,      4, -11, -4,     4, -4, -11,
	};

	//Gradients for 4D. They approximate the directions to the
	//vertices of a disprismatotesseractihexadecachoron from the center,
	//skewed so that the tetrahedral and cubic facets can be inscribed inside
	//spheres of the same radius.
	char gradients4D[] = {
		 3,  1,  1,  1,      1,  3,  1,  1,      1,  1,  3,  1,      1,  1,  1,  3,
		-3,  1,  1,  1,     -1,  3,  1,  1,     -1,  1,  3,  1,     -1,  1,  1,  3,
		 3, -1,  1,  1,      1, -3,  1,  1,      1, -1,  3,  1,      1, -1,  1,  3,
		-3, -1,  1,  1,     -1, -3,  1,  1,     -1, -1,  3,  1,     -1, -1,  1,  3,
		 3,  1, -1,  1,      1,  3, -1,  1,      1,  1, -3,  1,      1,  1, -1,  3,
		-3,  1, -1,  1,     -1,  3, -1,  1,     -1,  1, -3,  1,     -1,  1, -1,  3,
		 3, -1, -1,  1,      1, -3, -1,  1,      1, -1, -3,  1,      1, -1, -1,  3,
		-3, -1, -1,  1,     -1, -3, -1,  1,     -1, -1, -3,  1,     -1, -1, -1,  3,
		 3,  1,  1, -1,      1,  3,  1, -1,      1,  1,  3, -1,      1,  1,  1, -3,
		-3,  1,  1, -1,     -1,  3,  1, -1,     -1,  1,  3, -1,     -1,  1,  1, -3,
		 3, -1,  1, -1,      1, -3,  1, -1,      1, -1,  3, -1,      1, -1,  1, -3,
		-3, -1,  1, -1,     -1, -3,  1, -1,     -1, -1,  3, -1,     -1, -1,  1, -3,
		 3,  1, -1, -1,      1,  3, -1, -1,      1,  1, -3, -1,      1,  1, -1, -3,
		-3,  1, -1, -1,     -1,  3, -1, -1,     -1,  1, -3, -1,     -1,  1, -1, -3,
		 3, -1, -1, -1,      1, -3, -1, -1,      1, -1, -3, -1,      1, -1, -1, -3,
		-3, -1, -1, -1,     -1, -3, -1, -1,     -1, -1, -3, -1,     -1, -1, -1, -3,
	};

	Noise::value_t OpenSimplex::extrapolate(int xsb, int ysb, value_t dx, value_t dy) {
		int index = perm[(perm[xsb & 0xFF] + ysb) & 0xFF] & 0x0E;
		return gradients2D[index] * dx
			+ gradients2D[index + 1] * dy;
	}

	Noise::value_t OpenSimplex::extrapolate(int xsb, int ysb, int zsb, value_t dx, value_t dy, value_t dz) {
		int index = permGradIndex3D[(perm[(perm[xsb & 0xFF] + ysb) & 0xFF] + zsb) & 0xFF];
		return gradients3D[index] * dx
			+ gradients3D[index + 1] * dy
			+ gradients3D[index + 2] * dz;
	}

	Noise::value_t OpenSimplex::extrapolate(int xsb, int ysb, int zsb, int wsb, value_t dx, value_t dy, value_t dz, value_t dw) {
		int index = perm[(perm[(perm[(perm[xsb & 0xFF] + ysb) & 0xFF] + zsb) & 0xFF] + wsb) & 0xFF] & 0xFC;
		return gradients4D[index] * dx
			+ gradients4D[index + 1] * dy
			+ gradients4D[index + 2] * dz
			+ gradients4D[index + 3] * dw;
	}

	Noise::value_t OpenSimplex::genNoise2D(const glm::vec2& pos) {
				//Place input coordinates onto grid.
			value_t stretchOffset = (pos.x + pos.y) * STRETCH_CONSTANT_2D;
			value_t xs = pos.x + stretchOffset;
			value_t ys = pos.y + stretchOffset;

			//Floor to get grid coordinates of rhombus (stretched square) super-cell origin.
			int xsb = fastFloor(xs);
			int ysb = fastFloor(ys);

			//Skew out to get actual coordinates of rhombus origin. We'll need these later.
			value_t squishOffset = (xsb + ysb) * SQUISH_CONSTANT_2D;
			value_t xb = xsb + squishOffset;
			value_t yb = ysb + squishOffset;

			//Compute grid coordinates relative to rhombus origin.
			value_t xins = xs - xsb;
			value_t yins = ys - ysb;

			//Sum those together to get a value that determines which region we're in.
			value_t inSum = xins + yins;

			//Positions relative to origin point.
			value_t dx0 = pos.x - xb;
			value_t dy0 = pos.y - yb;

			//We'll be defining these inside the next block and using them afterwards.
			value_t dx_ext, dy_ext;
			int xsv_ext, ysv_ext;

			value_t value = 0;

			//Contribution (1,0)
			value_t dx1 = dx0 - 1 - SQUISH_CONSTANT_2D;
			value_t dy1 = dy0 - 0 - SQUISH_CONSTANT_2D;
			value_t attn1 = 2 - dx1 * dx1 - dy1 * dy1;
			if (attn1 > 0) {
				attn1 *= attn1;
				value += attn1 * attn1 * extrapolate(xsb + 1, ysb + 0, dx1, dy1);
			}

			//Contribution (0,1)
			value_t dx2 = dx0 - 0 - SQUISH_CONSTANT_2D;
			value_t dy2 = dy0 - 1 - SQUISH_CONSTANT_2D;
			value_t attn2 = 2 - dx2 * dx2 - dy2 * dy2;
			if (attn2 > 0) {
				attn2 *= attn2;
				value += attn2 * attn2 * extrapolate(xsb + 0, ysb + 1, dx2, dy2);
			}

			if (inSum <= 1) { //We're inside the triangle (2-Simplex) at (0,0)
				value_t zins = 1 - inSum;
				if (zins > xins || zins > yins) { //(0,0) is one of the closest two triangular vertices
					if (xins > yins) {
						xsv_ext = xsb + 1;
						ysv_ext = ysb - 1;
						dx_ext = dx0 - 1;
						dy_ext = dy0 + 1;
					} else {
						xsv_ext = xsb - 1;
						ysv_ext = ysb + 1;
						dx_ext = dx0 + 1;
						dy_ext = dy0 - 1;
					}
				} else { //(1,0) and (0,1) are the closest two vertices.
					xsv_ext = xsb + 1;
					ysv_ext = ysb + 1;
					dx_ext = dx0 - 1 - 2 * SQUISH_CONSTANT_2D;
					dy_ext = dy0 - 1 - 2 * SQUISH_CONSTANT_2D;
				}
			} else { //We're inside the triangle (2-Simplex) at (1,1)
				value_t zins = 2 - inSum;
				if (zins < xins || zins < yins) { //(0,0) is one of the closest two triangular vertices
					if (xins > yins) {
						xsv_ext = xsb + 2;
						ysv_ext = ysb + 0;
						dx_ext = dx0 - 2 - 2 * SQUISH_CONSTANT_2D;
						dy_ext = dy0 + 0 - 2 * SQUISH_CONSTANT_2D;
					} else {
						xsv_ext = xsb + 0;
						ysv_ext = ysb + 2;
						dx_ext = dx0 + 0 - 2 * SQUISH_CONSTANT_2D;
						dy_ext = dy0 - 2 - 2 * SQUISH_CONSTANT_2D;
					}
				} else { //(1,0) and (0,1) are the closest two vertices.
					dx_ext = dx0;
					dy_ext = dy0;
					xsv_ext = xsb;
					ysv_ext = ysb;
				}
				xsb += 1;
				ysb += 1;
				dx0 = dx0 - 1 - 2 * SQUISH_CONSTANT_2D;
				dy0 = dy0 - 1 - 2 * SQUISH_CONSTANT_2D;
			}

			//Contribution (0,0) or (1,1)
			value_t attn0 = 2 - dx0 * dx0 - dy0 * dy0;
			if (attn0 > 0) {
				attn0 *= attn0;
				value += attn0 * attn0 * extrapolate(xsb, ysb, dx0, dy0);
			}

			//Extra Vertex
			value_t attn_ext = 2 - dx_ext * dx_ext - dy_ext * dy_ext;
			if (attn_ext > 0) {
				attn_ext *= attn_ext;
				value += attn_ext * attn_ext * extrapolate(xsv_ext, ysv_ext, dx_ext, dy_ext);
			}
			return (((value / NORM_CONSTANT_2D) + 1.f) / 2.f);
		}

		//3D OpenSimplex Noise.
	Noise::value_t OpenSimplex::genNoise3D(const glm::vec3& pos) {
		//Place input coordinates on simplectic honeycomb.
		value_t stretchOffset = (pos.x + pos.y + pos.z) * STRETCH_CONSTANT_3D;
		value_t xs = pos.x + stretchOffset;
		value_t ys = pos.y + stretchOffset;
		value_t zs = pos.z + stretchOffset;

		//Floor to get simplectic honeycomb coordinates of rhombohedron (stretched cube) super-cell origin.
		int xsb = fastFloor(xs);
		int ysb = fastFloor(ys);
		int zsb = fastFloor(zs);

		//Skew out to get actual coordinates of rhombohedron origin. We'll need these later.
		value_t squishOffset = (xsb + ysb + zsb) * SQUISH_CONSTANT_3D;
		value_t xb = xsb + squishOffset;
		value_t yb = ysb + squishOffset;
		value_t zb = zsb + squishOffset;

		//Compute simplectic honeycomb coordinates relative to rhombohedral origin.
		value_t xins = xs - xsb;
		value_t yins = ys - ysb;
		value_t zins = zs - zsb;

		//Sum those together to get a value that determines which region we're in.
		value_t inSum = xins + yins + zins;

		//Positions relative to origin point.
		value_t dx0 = pos.x - xb;
		value_t dy0 = pos.y - yb;
		value_t dz0 = pos.z - zb;

		//We'll be defining these inside the next block and using them afterwards.
		value_t dx_ext0, dy_ext0 = 0, dz_ext0;
		value_t dx_ext1, dy_ext1, dz_ext1;
		int xsv_ext0, ysv_ext0, zsv_ext0;
		int xsv_ext1, ysv_ext1, zsv_ext1;

		value_t value = 0;
		if (inSum <= 1) { //We're inside the tetrahedron (3-Simplex) at (0,0,0)

			//Determine which two of (0,0,1), (0,1,0), (1,0,0) are closest.
			char aPoint = 0x01;
			value_t aScore = xins;
			char bPoint = 0x02;
			value_t bScore = yins;
			if (aScore >= bScore && zins > bScore) {
				bScore = zins;
				bPoint = 0x04;
			} else if (aScore < bScore && zins > aScore) {
				aScore = zins;
				aPoint = 0x04;
			}

			//Now we determine the two lattice points not part of the tetrahedron that may contribute.
			//This depends on the closest two tetrahedral vertices, including (0,0,0)
			value_t wins = 1 - inSum;
			if (wins > aScore || wins > bScore) { //(0,0,0) is one of the closest two tetrahedral vertices.
				char c = (bScore > aScore ? bPoint : aPoint); //Our other closest vertex is the closest out of a and b.

				if ((c & 0x01) == 0) {
					xsv_ext0 = xsb - 1;
					xsv_ext1 = xsb;
					dx_ext0 = dx0 + 1;
					dx_ext1 = dx0;
				} else {
					xsv_ext0 = xsv_ext1 = xsb + 1;
					dx_ext0 = dx_ext1 = dx0 - 1;
				}

				if ((c & 0x02) == 0) {
					ysv_ext0 = ysv_ext1 = ysb;
					dy_ext0 = dy_ext1 = dy0;
					if ((c & 0x01) == 0) {
						ysv_ext1 -= 1;
						dy_ext1 += 1;
					} else {
						ysv_ext0 -= 1;
						dy_ext0 += 1;
					}
				} else {
					ysv_ext0 = ysv_ext1 = ysb + 1;
					dy_ext0 = dy_ext1 = dy0 - 1;
				}

				if ((c & 0x04) == 0) {
					zsv_ext0 = zsb;
					zsv_ext1 = zsb - 1;
					dz_ext0 = dz0;
					dz_ext1 = dz0 + 1;
				} else {
					zsv_ext0 = zsv_ext1 = zsb + 1;
					dz_ext0 = dz_ext1 = dz0 - 1;
				}
			} else { //(0,0,0) is not one of the closest two tetrahedral vertices.
				char c = (char)(aPoint | bPoint); //Our two extra vertices are determined by the closest two.

				if ((c & 0x01) == 0) {
					xsv_ext0 = xsb;
					xsv_ext1 = xsb - 1;
					dx_ext0 = dx0 - 2 * SQUISH_CONSTANT_3D;
					dx_ext1 = dx0 + 1 - SQUISH_CONSTANT_3D;
				} else {
					xsv_ext0 = xsv_ext1 = xsb + 1;
					dx_ext0 = dx0 - 1 - 2 * SQUISH_CONSTANT_3D;
					dx_ext1 = dx0 - 1 - SQUISH_CONSTANT_3D;
				}

				if ((c & 0x02) == 0) {
					ysv_ext0 = ysb;
					ysv_ext1 = ysb - 1;
					dy_ext0 = dy0 - 2 * SQUISH_CONSTANT_3D;
					dy_ext1 = dy0 + 1 - SQUISH_CONSTANT_3D;
				} else {
					ysv_ext0 = ysv_ext1 = ysb + 1;
					dy_ext0 = dy0 - 1 - 2 * SQUISH_CONSTANT_3D;
					dy_ext1 = dy0 - 1 - SQUISH_CONSTANT_3D;
				}

				if ((c & 0x04) == 0) {
					zsv_ext0 = zsb;
					zsv_ext1 = zsb - 1;
					dz_ext0 = dz0 - 2 * SQUISH_CONSTANT_3D;
					dz_ext1 = dz0 + 1 - SQUISH_CONSTANT_3D;
				} else {
					zsv_ext0 = zsv_ext1 = zsb + 1;
					dz_ext0 = dz0 - 1 - 2 * SQUISH_CONSTANT_3D;
					dz_ext1 = dz0 - 1 - SQUISH_CONSTANT_3D;
				}
			}

			//Contribution (0,0,0)
			value_t attn0 = 2 - dx0 * dx0 - dy0 * dy0 - dz0 * dz0;
			if (attn0 > 0) {
				attn0 *= attn0;
				value += attn0 * attn0 * extrapolate(xsb + 0, ysb + 0, zsb + 0, dx0, dy0, dz0);
			}

			//Contribution (1,0,0)
			value_t dx1 = dx0 - 1 - SQUISH_CONSTANT_3D;
			value_t dy1 = dy0 - 0 - SQUISH_CONSTANT_3D;
			value_t dz1 = dz0 - 0 - SQUISH_CONSTANT_3D;
			value_t attn1 = 2 - dx1 * dx1 - dy1 * dy1 - dz1 * dz1;
			if (attn1 > 0) {
				attn1 *= attn1;
				value += attn1 * attn1 * extrapolate(xsb + 1, ysb + 0, zsb + 0, dx1, dy1, dz1);
			}

			//Contribution (0,1,0)
			value_t dx2 = dx0 - 0 - SQUISH_CONSTANT_3D;
			value_t dy2 = dy0 - 1 - SQUISH_CONSTANT_3D;
			value_t dz2 = dz1;
			value_t attn2 = 2 - dx2 * dx2 - dy2 * dy2 - dz2 * dz2;
			if (attn2 > 0) {
				attn2 *= attn2;
				value += attn2 * attn2 * extrapolate(xsb + 0, ysb + 1, zsb + 0, dx2, dy2, dz2);
			}

			//Contribution (0,0,1)
			value_t dx3 = dx2;
			value_t dy3 = dy1;
			value_t dz3 = dz0 - 1 - SQUISH_CONSTANT_3D;
			value_t attn3 = 2 - dx3 * dx3 - dy3 * dy3 - dz3 * dz3;
			if (attn3 > 0) {
				attn3 *= attn3;
				value += attn3 * attn3 * extrapolate(xsb + 0, ysb + 0, zsb + 1, dx3, dy3, dz3);
			}
		} else if (inSum >= 2) { //We're inside the tetrahedron (3-Simplex) at (1,1,1)

			//Determine which two tetrahedral vertices are the closest, out of (1,1,0), (1,0,1), (0,1,1) but not (1,1,1).
			char aPoint = 0x06;
			value_t aScore = xins;
			char bPoint = 0x05;
			value_t bScore = yins;
			if (aScore <= bScore && zins < bScore) {
				bScore = zins;
				bPoint = 0x03;
			} else if (aScore > bScore && zins < aScore) {
				aScore = zins;
				aPoint = 0x03;
			}

			//Now we determine the two lattice points not part of the tetrahedron that may contribute.
			//This depends on the closest two tetrahedral vertices, including (1,1,1)
			value_t wins = 3 - inSum;
			if (wins < aScore || wins < bScore) { //(1,1,1) is one of the closest two tetrahedral vertices.
				char c = (bScore < aScore ? bPoint : aPoint); //Our other closest vertex is the closest out of a and b.

				if ((c & 0x01) != 0) {
					xsv_ext0 = xsb + 2;
					xsv_ext1 = xsb + 1;
					dx_ext0 = dx0 - 2 - 3 * SQUISH_CONSTANT_3D;
					dx_ext1 = dx0 - 1 - 3 * SQUISH_CONSTANT_3D;
				} else {
					xsv_ext0 = xsv_ext1 = xsb;
					dx_ext0 = dx_ext1 = dx0 - 3 * SQUISH_CONSTANT_3D;
				}

				if ((c & 0x02) != 0) {
					ysv_ext0 = ysv_ext1 = ysb + 1;
					dy_ext0 = dy_ext1 = dy0 - 1 - 3 * SQUISH_CONSTANT_3D;
					if ((c & 0x01) != 0) {
						ysv_ext1 += 1;
						dy_ext1 -= 1;
					} else {
						ysv_ext0 += 1;
						dy_ext0 -= 1;
					}
				} else {
					ysv_ext0 = ysv_ext1 = ysb;
					dy_ext0 = dy_ext1 = dy0 - 3 * SQUISH_CONSTANT_3D;
				}

				if ((c & 0x04) != 0) {
					zsv_ext0 = zsb + 1;
					zsv_ext1 = zsb + 2;
					dz_ext0 = dz0 - 1 - 3 * SQUISH_CONSTANT_3D;
					dz_ext1 = dz0 - 2 - 3 * SQUISH_CONSTANT_3D;
				} else {
					zsv_ext0 = zsv_ext1 = zsb;
					dz_ext0 = dz_ext1 = dz0 - 3 * SQUISH_CONSTANT_3D;
				}
			} else { //(1,1,1) is not one of the closest two tetrahedral vertices.
				char c = (char)(aPoint & bPoint); //Our two extra vertices are determined by the closest two.

				if ((c & 0x01) != 0) {
					xsv_ext0 = xsb + 1;
					xsv_ext1 = xsb + 2;
					dx_ext0 = dx0 - 1 - SQUISH_CONSTANT_3D;
					dx_ext1 = dx0 - 2 - 2 * SQUISH_CONSTANT_3D;
				} else {
					xsv_ext0 = xsv_ext1 = xsb;
					dx_ext0 = dx0 - SQUISH_CONSTANT_3D;
					dx_ext1 = dx0 - 2 * SQUISH_CONSTANT_3D;
				}

				if ((c & 0x02) != 0) {
					ysv_ext0 = ysb + 1;
					ysv_ext1 = ysb + 2;
					dy_ext0 = dy0 - 1 - SQUISH_CONSTANT_3D;
					dy_ext1 = dy0 - 2 - 2 * SQUISH_CONSTANT_3D;
				} else {
					ysv_ext0 = ysv_ext1 = ysb;
					dy_ext0 = dy0 - SQUISH_CONSTANT_3D;
					dy_ext1 = dy0 - 2 * SQUISH_CONSTANT_3D;
				}

				if ((c & 0x04) != 0) {
					zsv_ext0 = zsb + 1;
					zsv_ext1 = zsb + 2;
					dz_ext0 = dz0 - 1 - SQUISH_CONSTANT_3D;
					dz_ext1 = dz0 - 2 - 2 * SQUISH_CONSTANT_3D;
				} else {
					zsv_ext0 = zsv_ext1 = zsb;
					dz_ext0 = dz0 - SQUISH_CONSTANT_3D;
					dz_ext1 = dz0 - 2 * SQUISH_CONSTANT_3D;
				}
			}

			//Contribution (1,1,0)
			value_t dx3 = dx0 - 1 - 2 * SQUISH_CONSTANT_3D;
			value_t dy3 = dy0 - 1 - 2 * SQUISH_CONSTANT_3D;
			value_t dz3 = dz0 - 0 - 2 * SQUISH_CONSTANT_3D;
			value_t attn3 = 2 - dx3 * dx3 - dy3 * dy3 - dz3 * dz3;
			if (attn3 > 0) {
				attn3 *= attn3;
				value += attn3 * attn3 * extrapolate(xsb + 1, ysb + 1, zsb + 0, dx3, dy3, dz3);
			}

			//Contribution (1,0,1)
			value_t dx2 = dx3;
			value_t dy2 = dy0 - 0 - 2 * SQUISH_CONSTANT_3D;
			value_t dz2 = dz0 - 1 - 2 * SQUISH_CONSTANT_3D;
			value_t attn2 = 2 - dx2 * dx2 - dy2 * dy2 - dz2 * dz2;
			if (attn2 > 0) {
				attn2 *= attn2;
				value += attn2 * attn2 * extrapolate(xsb + 1, ysb + 0, zsb + 1, dx2, dy2, dz2);
			}

			//Contribution (0,1,1)
			value_t dx1 = dx0 - 0 - 2 * SQUISH_CONSTANT_3D;
			value_t dy1 = dy3;
			value_t dz1 = dz2;
			value_t attn1 = 2 - dx1 * dx1 - dy1 * dy1 - dz1 * dz1;
			if (attn1 > 0) {
				attn1 *= attn1;
				value += attn1 * attn1 * extrapolate(xsb + 0, ysb + 1, zsb + 1, dx1, dy1, dz1);
			}

			//Contribution (1,1,1)
			dx0 = dx0 - 1 - 3 * SQUISH_CONSTANT_3D;
			dy0 = dy0 - 1 - 3 * SQUISH_CONSTANT_3D;
			dz0 = dz0 - 1 - 3 * SQUISH_CONSTANT_3D;
			value_t attn0 = 2 - dx0 * dx0 - dy0 * dy0 - dz0 * dz0;
			if (attn0 > 0) {
				attn0 *= attn0;
				value += attn0 * attn0 * extrapolate(xsb + 1, ysb + 1, zsb + 1, dx0, dy0, dz0);
			}
		} else { //We're inside the octahedron (Rectified 3-Simplex) in between.
			value_t aScore;
			char aPoint;
			bool aIsFurtherSide;
			value_t bScore;
			char bPoint;
			bool bIsFurtherSide;

			//Decide between point (0,0,1) and (1,1,0) as closest
			value_t p1 = xins + yins;
			if (p1 > 1) {
				aScore = p1 - 1;
				aPoint = 0x03;
				aIsFurtherSide = true;
			} else {
				aScore = 1 - p1;
				aPoint = 0x04;
				aIsFurtherSide = false;
			}

			//Decide between point (0,1,0) and (1,0,1) as closest
			value_t p2 = xins + zins;
			if (p2 > 1) {
				bScore = p2 - 1;
				bPoint = 0x05;
				bIsFurtherSide = true;
			} else {
				bScore = 1 - p2;
				bPoint = 0x02;
				bIsFurtherSide = false;
			}

			//The closest out of the two (1,0,0) and (0,1,1) will replace the furthest out of the two decided above, if closer.
			value_t p3 = yins + zins;
			if (p3 > 1) {
				value_t score = p3 - 1;
				if (aScore <= bScore && aScore < score) {
					aScore = score;
					aPoint = 0x06;
					aIsFurtherSide = true;
				} else if (aScore > bScore && bScore < score) {
					bScore = score;
					bPoint = 0x06;
					bIsFurtherSide = true;
				}
			} else {
				value_t score = 1 - p3;
				if (aScore <= bScore && aScore < score) {
					aScore = score;
					aPoint = 0x01;
					aIsFurtherSide = false;
				} else if (aScore > bScore && bScore < score) {
					bScore = score;
					bPoint = 0x01;
					bIsFurtherSide = false;
				}
			}

			//Where each of the two closest points are determines how the extra two vertices are calculated.
			if (aIsFurtherSide == bIsFurtherSide) {
				if (aIsFurtherSide) { //Both closest points on (1,1,1) side

					//One of the two extra points is (1,1,1)
					dx_ext0 = dx0 - 1 - 3 * SQUISH_CONSTANT_3D;
					dy_ext0 = dy0 - 1 - 3 * SQUISH_CONSTANT_3D;
					dz_ext0 = dz0 - 1 - 3 * SQUISH_CONSTANT_3D;
					xsv_ext0 = xsb + 1;
					ysv_ext0 = ysb + 1;
					zsv_ext0 = zsb + 1;

					//Other extra point is based on the shared axis.
					char c = (char)(aPoint & bPoint);
					if ((c & 0x01) != 0) {
						dx_ext1 = dx0 - 2 - 2 * SQUISH_CONSTANT_3D;
						dy_ext1 = dy0 - 2 * SQUISH_CONSTANT_3D;
						dz_ext1 = dz0 - 2 * SQUISH_CONSTANT_3D;
						xsv_ext1 = xsb + 2;
						ysv_ext1 = ysb;
						zsv_ext1 = zsb;
					} else if ((c & 0x02) != 0) {
						dx_ext1 = dx0 - 2 * SQUISH_CONSTANT_3D;
						dy_ext1 = dy0 - 2 - 2 * SQUISH_CONSTANT_3D;
						dz_ext1 = dz0 - 2 * SQUISH_CONSTANT_3D;
						xsv_ext1 = xsb;
						ysv_ext1 = ysb + 2;
						zsv_ext1 = zsb;
					} else {
						dx_ext1 = dx0 - 2 * SQUISH_CONSTANT_3D;
						dy_ext1 = dy0 - 2 * SQUISH_CONSTANT_3D;
						dz_ext1 = dz0 - 2 - 2 * SQUISH_CONSTANT_3D;
						xsv_ext1 = xsb;
						ysv_ext1 = ysb;
						zsv_ext1 = zsb + 2;
					}
				} else {//Both closest points on (0,0,0) side

					//One of the two extra points is (0,0,0)
					dx_ext0 = dx0;
					dy_ext0 = dy0;
					dz_ext0 = dz0;
					xsv_ext0 = xsb;
					ysv_ext0 = ysb;
					zsv_ext0 = zsb;

					//Other extra point is based on the omitted axis.
					char c = (char)(aPoint | bPoint);
					if ((c & 0x01) == 0) {
						dx_ext1 = dx0 + 1 - SQUISH_CONSTANT_3D;
						dy_ext1 = dy0 - 1 - SQUISH_CONSTANT_3D;
						dz_ext1 = dz0 - 1 - SQUISH_CONSTANT_3D;
						xsv_ext1 = xsb - 1;
						ysv_ext1 = ysb + 1;
						zsv_ext1 = zsb + 1;
					} else if ((c & 0x02) == 0) {
						dx_ext1 = dx0 - 1 - SQUISH_CONSTANT_3D;
						dy_ext1 = dy0 + 1 - SQUISH_CONSTANT_3D;
						dz_ext1 = dz0 - 1 - SQUISH_CONSTANT_3D;
						xsv_ext1 = xsb + 1;
						ysv_ext1 = ysb - 1;
						zsv_ext1 = zsb + 1;
					} else {
						dx_ext1 = dx0 - 1 - SQUISH_CONSTANT_3D;
						dy_ext1 = dy0 - 1 - SQUISH_CONSTANT_3D;
						dz_ext1 = dz0 + 1 - SQUISH_CONSTANT_3D;
						xsv_ext1 = xsb + 1;
						ysv_ext1 = ysb + 1;
						zsv_ext1 = zsb - 1;
					}
				}
			} else { //One point on (0,0,0) side, one point on (1,1,1) side
				char c1, c2;
				if (aIsFurtherSide) {
					c1 = aPoint;
					c2 = bPoint;
				} else {
					c1 = bPoint;
					c2 = aPoint;
				}

				//One contribution is a permutation of (1,1,-1)
				if ((c1 & 0x01) == 0) {
					dx_ext0 = dx0 + 1 - SQUISH_CONSTANT_3D;
					dz_ext0 = dz0 - 1 - SQUISH_CONSTANT_3D;
					xsv_ext0 = xsb - 1;
					ysv_ext0 = ysb + 1;
					zsv_ext0 = zsb + 1;
				} else if ((c1 & 0x02) == 0) {
					dx_ext0 = dx0 - 1 - SQUISH_CONSTANT_3D;
					dy_ext0 = dy0 + 1 - SQUISH_CONSTANT_3D;
					dz_ext0 = dz0 - 1 - SQUISH_CONSTANT_3D;
					xsv_ext0 = xsb + 1;
					ysv_ext0 = ysb - 1;
					zsv_ext0 = zsb + 1;
				} else {
					dx_ext0 = dx0 - 1 - SQUISH_CONSTANT_3D;
					dy_ext0 = dy0 - 1 - SQUISH_CONSTANT_3D;
					dz_ext0 = dz0 + 1 - SQUISH_CONSTANT_3D;
					xsv_ext0 = xsb + 1;
					ysv_ext0 = ysb + 1;
					zsv_ext0 = zsb - 1;
				}

				//One contribution is a permutation of (0,0,2)
				dx_ext1 = dx0 - 2 * SQUISH_CONSTANT_3D;
				dy_ext1 = dy0 - 2 * SQUISH_CONSTANT_3D;
				dz_ext1 = dz0 - 2 * SQUISH_CONSTANT_3D;
				xsv_ext1 = xsb;
				ysv_ext1 = ysb;
				zsv_ext1 = zsb;
				if ((c2 & 0x01) != 0) {
					dx_ext1 -= 2;
					xsv_ext1 += 2;
				} else if ((c2 & 0x02) != 0) {
					dy_ext1 -= 2;
					ysv_ext1 += 2;
				} else {
					dz_ext1 -= 2;
					zsv_ext1 += 2;
				}
			}

			//Contribution (1,0,0)
			value_t dx1 = dx0 - 1 - SQUISH_CONSTANT_3D;
			value_t dy1 = dy0 - 0 - SQUISH_CONSTANT_3D;
			value_t dz1 = dz0 - 0 - SQUISH_CONSTANT_3D;
			value_t attn1 = 2 - dx1 * dx1 - dy1 * dy1 - dz1 * dz1;
			if (attn1 > 0) {
				attn1 *= attn1;
				value += attn1 * attn1 * extrapolate(xsb + 1, ysb + 0, zsb + 0, dx1, dy1, dz1);
			}

			//Contribution (0,1,0)
			value_t dx2 = dx0 - 0 - SQUISH_CONSTANT_3D;
			value_t dy2 = dy0 - 1 - SQUISH_CONSTANT_3D;
			value_t dz2 = dz1;
			value_t attn2 = 2 - dx2 * dx2 - dy2 * dy2 - dz2 * dz2;
			if (attn2 > 0) {
				attn2 *= attn2;
				value += attn2 * attn2 * extrapolate(xsb + 0, ysb + 1, zsb + 0, dx2, dy2, dz2);
			}

			//Contribution (0,0,1)
			value_t dx3 = dx2;
			value_t dy3 = dy1;
			value_t dz3 = dz0 - 1 - SQUISH_CONSTANT_3D;
			value_t attn3 = 2 - dx3 * dx3 - dy3 * dy3 - dz3 * dz3;
			if (attn3 > 0) {
				attn3 *= attn3;
				value += attn3 * attn3 * extrapolate(xsb + 0, ysb + 0, zsb + 1, dx3, dy3, dz3);
			}

			//Contribution (1,1,0)
			value_t dx4 = dx0 - 1 - 2 * SQUISH_CONSTANT_3D;
			value_t dy4 = dy0 - 1 - 2 * SQUISH_CONSTANT_3D;
			value_t dz4 = dz0 - 0 - 2 * SQUISH_CONSTANT_3D;
			value_t attn4 = 2 - dx4 * dx4 - dy4 * dy4 - dz4 * dz4;
			if (attn4 > 0) {
				attn4 *= attn4;
				value += attn4 * attn4 * extrapolate(xsb + 1, ysb + 1, zsb + 0, dx4, dy4, dz4);
			}

			//Contribution (1,0,1)
			value_t dx5 = dx4;
			value_t dy5 = dy0 - 0 - 2 * SQUISH_CONSTANT_3D;
			value_t dz5 = dz0 - 1 - 2 * SQUISH_CONSTANT_3D;
			value_t attn5 = 2 - dx5 * dx5 - dy5 * dy5 - dz5 * dz5;
			if (attn5 > 0) {
				attn5 *= attn5;
				value += attn5 * attn5 * extrapolate(xsb + 1, ysb + 0, zsb + 1, dx5, dy5, dz5);
			}

			//Contribution (0,1,1)
			value_t dx6 = dx0 - 0 - 2 * SQUISH_CONSTANT_3D;
			value_t dy6 = dy4;
			value_t dz6 = dz5;
			value_t attn6 = 2 - dx6 * dx6 - dy6 * dy6 - dz6 * dz6;
			if (attn6 > 0) {
				attn6 *= attn6;
				value += attn6 * attn6 * extrapolate(xsb + 0, ysb + 1, zsb + 1, dx6, dy6, dz6);
			}
		}

		//First extra vertex
		value_t attn_ext0 = 2 - dx_ext0 * dx_ext0 - dy_ext0 * dy_ext0 - dz_ext0 * dz_ext0;
		if (attn_ext0 > 0)
		{
			attn_ext0 *= attn_ext0;
			value += attn_ext0 * attn_ext0 * extrapolate(xsv_ext0, ysv_ext0, zsv_ext0, dx_ext0, dy_ext0, dz_ext0);
		}

		//Second extra vertex
		value_t attn_ext1 = 2 - dx_ext1 * dx_ext1 - dy_ext1 * dy_ext1 - dz_ext1 * dz_ext1;
		if (attn_ext1 > 0)
		{
			attn_ext1 *= attn_ext1;
			value += attn_ext1 * attn_ext1 * extrapolate(xsv_ext1, ysv_ext1, zsv_ext1, dx_ext1, dy_ext1, dz_ext1);
		}

		return (((value  / NORM_CONSTANT_3D) + 1.f) / 2.f);
	}



	/*
	 *
	 *     WORLEY NOISE (cellular noise)
	 *
	 *
	 */






} // namespace noise

}



