/*
 * flora.hpp
 *
 *  Created on: Apr 20, 2015
 *      Author: Dani
 */

#ifndef FLORA_H_
#define FLORA_H_

#include "coords.h"

namespace vox {

	class EntityShaderProgram;
	class EntityMeshBuffer;
	class Camera;
	class ChunkMap;

	struct Flora {
		WorldCoordinate pos, voxelSize, size;

		virtual ~Flora() {}
		virtual void sendMeshToGPU() = 0;
	};

	struct Tree : public Flora {
		EntityMeshBuffer *buf;

		virtual ~Tree();
		void sendMeshToGPU();
	};

	Tree * genTree(WorldCoordinate pos);

}

#endif /* FLORA_H_ */
