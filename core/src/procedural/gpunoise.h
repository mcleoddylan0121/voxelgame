/*
 * gpunoise.h
 *
 *  Created on: Jul 15, 2015
 *      Author: Dylan
 */

#ifndef GPUNOISE_H_
#define GPUNOISE_H_

#include <string>

namespace vox {
namespace noise {

	class Noise;

	std::string getGPUNoiseString(std::string fname, int dims, Noise* noise);

}
}



#endif /* GPUNOISE_H_ */
