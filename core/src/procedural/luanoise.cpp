/*
 * luanoise.cpp
 *
 *  Created on: Jul 12, 2015
 *      Author: Dylan
 */

#include "noise.h"
#include "resourcemanager.h"
#include "vlua.h"

#include <functional>
#include <algorithm>

namespace vox {

namespace noise {


#define nName(noise,theName) const std::string noise::name = theName

	nName(DimensionalNoise,"dimensional");

	const std::string Noise::name = "nullnoise";
	const std::string Generator::name = "nullgenerator";
	const std::string Selector::name = "selector";
	const std::string Modifier::name = "nullmodifier";
	const std::string OutputModifier::name = "nulloutputmodifier";
	const std::string BiSelector::name = "biselector";
	const std::string Constrainer::name = "constrainer";
	const std::string Scalar::name = "inputscalar";
	const std::string OutputShifter::name = "outputshifter";
	const std::string Guassianator::name = "gaussianator";
	const std::string DiscreteFit::name = "discretefit";
	const std::string CloudCutoff::name = "cloudcurve";
	const std::string FitCurve::name = "fitcurve";
	const std::string CombinedNoise::name = "combinednoise";
	const std::string Multiplier::name = "multiply";
	const std::string Simplex::name = "simplex";
	const std::string OpenSimplex::name = "opensimplex";
	const std::string Perlin::name = "perlin";
	const std::string EmptyNoise::name = "empty";
	const std::string OctaveNoise::name = "octave";
	const std::string RidgedNoise::name = "ridged";
	const std::string SharedNoise::name = "sharednoise";
	const std::string InputModifier::name = "nullinputmodifier";
	const std::string InputWarper::name = "inputwarper";

	bool remChar(char c) {
		return (c == ' ') || (c == '_');
	}
	std::string stripFormatting(std::string in) {
		in.erase(std::remove_if(in.begin(), in.end(), remChar), in.end()); // strip spaces + underscores
		std::transform(in.begin(), in.end(), in.begin(), tolower); // convert to lowercase
		return in;
	}

	std::unordered_map<std::string, std::function<Generator*(Rand::Seed, Noise::value_t, Noise::value_t)>> baseGens = {
			{Simplex::name, makeGen<Simplex>},
			{OpenSimplex::name, makeGen<OpenSimplex>},
			{Perlin::name, makeGen<Perlin>}
	};

	std::unordered_map<std::string, std::function<float(float)>> fitCurveFunctions = {
			{"linear", [](float f)->float { return f; } },
			{"invert", [](float f)->float { return 1-f; } },
			{"sqrt", [](float f)->float { return sqrt(f); } },
			{"cubiceasein", [](float f)->float { return f*f*f; }},
			{"cubiceaseinout", [](float f)->float {
				f = 2*f;
				return (f < 1? (f*f*f):((f-2)*(f-2)*(f-2))+2)/2;
			}},
			{"quinticeaseinout", [](float f)->float {
				f = 2*f;
				return (f < 1? (f*f*f*f*f):((f-2)*(f-2)*(f-2)*(f-2)*(f-2))+2)/2;
			}},
			{"square", [](float f)->float { return f*f; }},
			{"quadraticeaseinout", [](float t)->float {
				t *= 2;
				return t<1? 0.5*t*t:-0.5*((t-1)*(t-3)-1);
			}}
	};

	std::function<Generator*(Rand::Seed, Noise::value_t, Noise::value_t)> getBaseGen(std::string s) {
		s = stripFormatting(s);
		return baseGens[s];
	}

	std::function<float(float)> getFitCurveFunction(std::string s) {
		s = stripFormatting(s);
		return fitCurveFunctions[s];
	}

	Rand defaultRand;

	struct LuaNoiseTracker {
		Asset asset;
		LuaScript* script;
		std::string varPath;
		Rand* rand;
		std::unordered_map<std::string, NoisePtr>* shared;

		LuaNoiseTracker(LuaScript* script, Asset a, std::string varPath = "", Rand* rand = &defaultRand, std::unordered_map<std::string, NoisePtr>* shared = nullptr):
			asset(a), script(script), varPath(varPath), rand(rand) {
			if(!shared)
				shared = new std::unordered_map<std::string, NoisePtr>();
			this->shared = shared;
		}
		LuaNoiseTracker(): LuaNoiseTracker(nullptr, Asset()) {}
		template<typename T> T get(std::string name, T defVal = T()) {
			return script->get<T>(name, defVal);
		}
		template<typename T> std::vector<T> getArray(std::string name, std::vector<T> defVal = {}) {
			return script->getArray<T>(name, defVal);
		}

		LuaNoiseTracker delve(std::string varName) {
			return LuaNoiseTracker(script, asset, this->varPath + varName + ".", this->rand, this->shared);
		}

		void throwError(std::string name) {
			printlnf(ERROR, "Invalid variable %s in file %s", (varPath + name).c_str(), asset.path.c_str());
			exit(1);
		}

		NoisePtr getShared(std::string name) {
			NoisePtr ret = (*shared)[name];
			return ret;
		}

		void addShared(std::string name, NoisePtr n) {
			(*shared)[name] = n;
		}

		void clean() {
			delete shared;
		}
	};

	std::function<Noise*(LuaNoiseTracker)> getNoiseDecl(std::string);

	LuaNoiseTracker childTable;

	struct LuaNoiseObject: public LuaObject {
		Noise* noise;
		operator Noise*() { return noise; }
		LuaNoiseObject(std::vector<std::shared_ptr<LuaPair>> map = {}): LuaObject(map) {this->noise = nullptr;}
		virtual ~LuaNoiseObject() {}
		void init(LuaScript& ls, const std::string& varName) {
			LuaNoiseTracker t = childTable;
			std::string type = t.get<std::string>("type", "simplex");
			auto decl = getNoiseDecl(type);
			if(!decl) t.throwError("type");
			noise = decl(t);
		}
	};

	LuaNoiseObject def = LuaNoiseObject();

	Noise* goDeeper(LuaNoiseTracker parentTable, std::string childTableName) {
		childTable = parentTable.delve(childTableName);
		return parentTable.get<LuaNoiseObject>(childTableName);
	}

	Noise* getOctaveNoise(LuaNoiseTracker t) {
		float persistence = t.get<float>("persistence", 0.3);
		auto range = t.getArray<Noise::value_t>("range", {0.f, 1.f});
		float largestFeature = t.get<float>("largestfeature", 100.f);
		int max_octaves = t.get<int>("max_octaves", 1000000);
		std::string base = t.get<std::string>("base", "simplex");
		auto make = getBaseGen(base);
		if(!make) t.throwError("base");
		Rand::Seed seed = t.rand->getSeedSeq(10);
		return new OctaveNoise(largestFeature, persistence, seed, max_octaves, range[0], range[1], make);
	}

	Noise* getSelector(LuaNoiseTracker t) {
		return new Selector(goDeeper(t, "first"), goDeeper(t, "second"), goDeeper(t, "selector"));
	}

	Noise* getGuassianator(LuaNoiseTracker t) {
		float sigma = t.get<float>("sigma", 0.2f);
		float shift = t.get<float>("shift", 0.1f);
		auto range = t.getArray<Noise::value_t>("range", {0, 1});
		Noise* noise = goDeeper(t, "noise");
		return new Guassianator(noise, sigma, shift, range[0], range[1]);
	}

	Noise* getOutputShifter(LuaNoiseTracker t) {
		auto range = t.getArray<Noise::value_t>("range", {0, 1});
		return new OutputShifter(goDeeper(t, "noise"), range[0], range[1]);
	}

	Noise* getScalar(LuaNoiseTracker t) {
		std::vector<Noise::value_t> retScale;
		auto scaleArr = t.getArray<float>("scale", {});
		if(scaleArr.size() > 0) {
			for(unsigned int i = 0; i < 5; i++) {
				if(i < scaleArr.size())
					retScale.push_back(scaleArr[i]);
				else
					retScale.push_back(scaleArr.back());
			}
		} else {
			auto scale = t.get<float>("scale", 1);
			for(int i = 0; i < 5; i++)
				retScale.push_back(scale);
		}
		return new Scalar(goDeeper(t, "noise"), retScale);
	}

	Noise* getBiSelector(LuaNoiseTracker t) {
		auto boundary = t.get<Noise::value_t>("boundary", 0.5f);
		auto range = t.getArray<Noise::value_t>("range", {0, 1});
		return new BiSelector(goDeeper(t, "noise"), boundary, range[0], range[1]);
	}

	Noise* getDiscreteFit(LuaNoiseTracker t) {
		auto steps = t.get<int>("steps", 5);
		Noise* ret = new DiscreteFit(goDeeper(t, "noise"), steps);
		//while(true) printlnf(INFORMATION, "%f", ((DiscreteFit*)(ret))->stepsize);
		return ret;
	}

	template<typename T> Noise* makeBase(LuaNoiseTracker t) {
		auto range = t.getArray<Noise::value_t>("range", {0, 1});
		Rand::Seed seed = t.rand->getSeedSeq(10);
		return makeGen<T>(seed, range[0], range[1]);
	}

	Noise* makeEmpty(LuaNoiseTracker t) {
		auto value = t.get<Noise::value_t>("value", 0.f);
		return new EmptyNoise(value);
	}

	Noise* getFitCurve(LuaNoiseTracker t) {
		auto functionName = t.get<std::string>("curve", "linear");
		auto function = getFitCurveFunction(functionName);
		if(!function) t.throwError("curve");
		return new FitCurve(goDeeper(t, "noise"), function, functionName);
	}

	Noise* getConstrainer(LuaNoiseTracker t) {
		auto range = t.getArray<Noise::value_t>("range", {0, 1});
		return new Constrainer(goDeeper(t, "noise"), range[0], range[1]);
	}

	Noise* getMultiplier(LuaNoiseTracker t) {
		return new Multiplier(goDeeper(t, "first"), goDeeper(t, "second"));
	}

	Noise* getCloudCutoff(LuaNoiseTracker t) {
		auto density = t.get<float>("density", 0.3);
		auto sharpness = t.get<float>("sharpness", 0.5);
		return new CloudCutoff(goDeeper(t, "noise"), density, sharpness);
	}

	Noise* getCombinedNoise(LuaNoiseTracker t) {
		auto noises = t.getArray<LuaNoiseObject>("noises");
		std::vector<NoisePtr> casts;
		for(auto n: noises) casts.push_back((NoisePtr)((Noise*)n));
		return new CombinedNoise(casts);
	}

	Noise* getRidged(LuaNoiseTracker t) {
		return new RidgedNoise(goDeeper(t, "noise"));
	}

	Noise* makeShared(LuaNoiseTracker t) {
		std::string nname = t.get<std::string>("name");
		NoisePtr n = t.getShared(nname);
		if(!n) {
			n = goDeeper(t, "noise");
			t.addShared(nname, n);
		}
		return new SharedNoise(n);
	}

	Noise* makeInputWarper(LuaNoiseTracker t) {
		return new InputWarper(goDeeper(t,"noise"),goDeeper(t,"warp"));
	}

	glm::vec2 getMinMax(LuaNoiseTracker t) {
		auto range = t.getArray<Noise::value_t>("range", {0.f, 1.f});
		return glm::vec2(range[0],range[1]);
	}

	Noise* makeDimensionalNoise(LuaNoiseTracker t) {
		glm::vec2 r = getMinMax(t);
		return new DimensionalNoise(r.x,r.y,t.get<int>("dim"));
	}

#define mknspr(nsType,func) {nsType::name,func}

	// TODO: allow for synonyms or something
	std::unordered_map<std::string, std::function<Noise*(LuaNoiseTracker)>> luaNoiseDecls = {
			mknspr(OctaveNoise,      getOctaveNoise),
			mknspr(Selector,         getSelector),
			mknspr(BiSelector,       getBiSelector),
			mknspr(DiscreteFit,      getDiscreteFit),
			mknspr(Guassianator,     getGuassianator),
			mknspr(OutputShifter,    getOutputShifter),
			mknspr(Constrainer,      getConstrainer),
			mknspr(Scalar,           getScalar),
			mknspr(Multiplier,       getMultiplier),
			mknspr(FitCurve,         getFitCurve),
			mknspr(CloudCutoff,      getCloudCutoff),
			mknspr(CombinedNoise,    getCombinedNoise),
			mknspr(Simplex,          makeBase<Simplex>),
			mknspr(OpenSimplex,      makeBase<OpenSimplex>),
			mknspr(Perlin,           makeBase<Perlin>),
			mknspr(RidgedNoise,      getRidged),
			mknspr(EmptyNoise,       makeEmpty),
			mknspr(SharedNoise,      makeShared),
			mknspr(InputWarper,      makeInputWarper),
			mknspr(DimensionalNoise, makeDimensionalNoise)
	};

	std::function<Noise*(LuaNoiseTracker)> getNoiseDecl(std::string s) {
		s = stripFormatting(s);
		return luaNoiseDecls[s];
	}

	Noise* Noise::readFile(std::string filename, Rand& rand, std::string varname) {
		Rand r = Rand::useRandSeed(rand);
		Asset a = mFileManager->fetchAsset(filename, Asset::Noise);
		if (mFileManager->verifyAsset(a, Asset::Noise)) {
			auto script = mRes->getResource<LuaScript, Asset::Noise>(a.name);
			LuaNoiseTracker t = LuaNoiseTracker(script, a, "", &r);
			Noise* n = goDeeper(t, varname);
			t.clean();
			if(!n) {
				printlnf(LUAERROR, "Unknown error when parsing noise! lol!");
				return new EmptyNoise(1);
			}
			return n;
		}
		return new EmptyNoise(1); // We have to return something, right?
	}

}

}


