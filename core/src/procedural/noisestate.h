/*
 * noisestate.h
 *
 *  Created on: Jul 27, 2015
 *      Author: Dylan
 */

#ifndef SRC_PROCEDURAL_NOISESTATE_H_
#define SRC_PROCEDURAL_NOISESTATE_H_

#include "gameloop.h"
#include "program.h"
#include "framebuffer.h"
#include "noise.h"
#include "gpunoise.h"

namespace vox {


	class NoiseState: public GameState {
	public:
		RenderTextureProgram* program; // Just a hack, no texture is actually rendered
		Texture* dummyTexture; // shhhh
		float time = 0;

		NoiseState(std::string noiseName) {
			unsigned char data[4] = {0,0,0,0};
			dummyTexture = createTexture2D({2,2}, data, GL_R8, GL_NEAREST, GL_CLAMP_TO_EDGE, GL_RED, GL_UNSIGNED_BYTE);

			Rand rand({rand()});
			noise::Noise* noise = noise::Noise::readFile(noiseName, rand);
			std::string g = getGPUNoiseString("getNoise", 3, noise);
			program = new RenderTextureProgram("noisetest", false, {}, {"noise_lib"}, {g});
		}

		void tick(float ttime) { time += ttime; }

		void render(FrameBuffer* f) {
			glDisable(GL_DEPTH_TEST);
			mGL->bindFrameBuffer(f);
			mGL->bindProgram(program);
			glUniform1f(program->getUniformLocation("time"), time); // This is a hack, never do this
			program->beginRender();
			mGL->render(program, dummyTexture, {0,0}, {1,1});
			program->endRender();
		}
	};
}


#endif /* SRC_PROCEDURAL_NOISESTATE_H_ */
