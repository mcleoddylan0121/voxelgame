/*
 * gpunoise.cpp
 *
 *  Created on: Jul 15, 2015
 *      Author: Dylan
 */

#include "gpunoise.h"
#include "noise.h"
#include "resourcemanager.h"
#include "renderutils.h"
#include "util.h"

namespace vox {
namespace noise {

	std::string getBaseInputString(std::string basename, int dims, std::vector<float> inputmul, float min, float max) {
		// multiply the multiplier to the input:
		std::string inputstring = "(" + basename + toString(dims) + "D(pos * vec" + toString(dims) + "(";
		for(int i = 0; i < dims - 1; i++) inputstring += toString(inputmul[i]) + ",";
		inputstring += toString(inputmul[dims-1]) + ")) * " + toString(max - min) + " + " + toString(min) + ")";
		return inputstring;
	}

	std::string Noise::getGPUNoiseString(int dims, std::vector<float> inputmul) {
		printlnf(ERROR, "GPU noise string does not exist for given noise!");
		return getBaseInputString(Simplex::name, dims, inputmul, 0, 1); // give it something
	}

	std::string OutputShifter::getGPUNoiseString(int dims, std::vector<float> inputmul) {
		return noise->getGPUNoiseString(dims, inputmul) + " * " + toString(multiply) + " + " + toString(add);
	}

	std::string Constrainer::getGPUNoiseString(int dims, std::vector<float> inputmul) {
		return "clamp(" + noise->getGPUNoiseString(dims, inputmul) + ", " + toString(min) + ", " + toString(max) + ")";
	}

	std::string Multiplier::getGPUNoiseString(int dims, std::vector<float> inputmul) {
		return "((" + a->getGPUNoiseString(dims, inputmul) + ") * (" + b->getGPUNoiseString(dims, inputmul) + ")) ";
	}

	std::string CloudCutoff::getGPUNoiseString(int dims, std::vector<float> inputmul) {
		return "1.0 - (pow(" + toString(sharpness) + ", clamp(" + noise->getGPUNoiseString(dims, inputmul) + " - " + toString(density) + ", 0, 1)))";
	}

	std::string DiscreteFit::getGPUNoiseString(int dims, std::vector<float> inputmul) {
		return "rndMultiple(" + noise->getGPUNoiseString(dims, inputmul) + " - " + toString(min) + ",  " + toString(stepsize) + ") + " + toString(min);
	}

	std::string Scalar::getGPUNoiseString(int dims, std::vector<float> inputmul) {
		for(unsigned int i = 0; i < inputmul.size(); i++) {
			inputmul[i] /= scale5[i];
		}
		return noise->getGPUNoiseString(dims, inputmul);
	}

	std::string CombinedNoise::getGPUNoiseString(int dims, std::vector<float> inputmul) {
		std::string ret = "(";
		for(unsigned int i = 0; i < noises.size() - 1; i++) {
			ret += noises[i]->getGPUNoiseString(dims, inputmul) + " + ";
		}
		ret += noises[noises.size() - 1]->getGPUNoiseString(dims, inputmul) + ")";
		return ret;

		/*value_t total = 0;
		for(NoisePtr n: noises)
			total += n->noise2D(pos);
		return (total / cMax) * (max - min) + min;*/
	}

	std::string FitCurve::getGPUNoiseString(int dims, std::vector<float> inputmul) {
		return "(fitCurve_" + functionName + "((" + noise->getGPUNoiseString(dims, inputmul) + " - " + toString(min) + ")/" + toString(max-min) + ") * " + toString(max-min) + " + " + toString(min) + ")";
	}

	std::string Simplex::getGPUNoiseString(int dims, std::vector<float> inputmul) {     return getBaseInputString("Perlin", dims, inputmul, min, max); }
	std::string Perlin::getGPUNoiseString(int dims, std::vector<float> inputmul) {      return getBaseInputString("Perlin", dims, inputmul, min, max); }
	std::string OpenSimplex::getGPUNoiseString(int dims, std::vector<float> inputmul) { return getBaseInputString("Perlin", dims, inputmul, min, max); }

	std::string RidgedNoise::getGPUNoiseString(int dims, std::vector<float> inputmul) {
		return "((abs(" + noise->getGPUNoiseString(dims, inputmul) + ")) * " + toString(max-min) + " + " + toString(min) + ")";
	}

	std::string getGPUNoiseString(std::string fname, int dims, Noise* noise) {
		return "float " + fname + "(vec" + toString(dims) + " pos) { \n"
				"	return " + noise->getGPUNoiseString(dims, {1,1,1}) + "; \n"
				"}";
	}

}
}
