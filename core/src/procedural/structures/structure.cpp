/*
 * structure.cpp
 *
 *  Created on: Jul 17, 2016
 *      Author: Dylan
 */


#include "structure.h"
#include "luacontext.h"
#include "vluautil.h"


namespace vox {

	const std::string LUA_STRUCTURE_FUNCTION_PREFIX = "";
	const std::string LUA_STRUCTURE_FUNCTION_SUFFIX = ".evaluate";

	void LuaStructureEvaluation::endUse() {
		for(auto* p:patterns) delete p;
		for(auto* f:functors) delete f;
		patterns.clear();
		functors.clear();
		inUse = false;
	}

	void LuaStructureEvaluation::beginUse(Rand& rand) {
		inUse = true;
		this->rand = &rand;
	}

	int LuaStructureEvaluation::addPattern(PatternReturn* patt) {
		int ret = patterns.size();
		patterns.push_back(patt);
		return ret;
	}

	int LuaStructureEvaluation::addFunctor(VoxelIDFunctor* functor) {
		int ret = functors.size();
		functors.push_back(functor);
		return ret;
	}

	std::vector<LuaStructureEvaluation*> luaStructureEvaluations; // indexed via ID

	LuaStructureEvaluation* registerStructureEvaluation(Rand& rand) {
		for(auto* eval:luaStructureEvaluations) {
			if(!eval->inUse) {
				eval->inUse = true;
				eval->beginUse(rand);
				return eval;
			}
		}

		LuaStructureEvaluation* ret = new LuaStructureEvaluation{};
		ret->beginUse(rand);
		ret->ID = luaStructureEvaluations.size();
		luaStructureEvaluations.push_back(ret);
		return ret;
	}

	LuaStructureEvaluation* getStructureEvaluation(int ID) {
		return luaStructureEvaluations[ID];
	}


	PatternReturn* LuaEvaluatedStructure::evaluate(Rand& rand) {
		LuaStructureEvaluation* thisEval = registerStructureEvaluation(rand);
		int ID = thisEval->ID;

		glm::vec3 v3pos = glm::vec3(position);
		lvec3 pos(v3pos);

		thisEval->ret = nullptr;

		mLua->getContext()->callVoidFunction("vox_structure_table.i0" + LUA_STRUCTURE_FUNCTION_SUFFIX, ID, pos);

		thisEval->endUse();



		return thisEval->ret;
	}


	bool patternVoxelIDReplaceErrorCalled = false; // A sufficiently long variable name helps reduce variable name squatting
	VoxelID VoxelIDReplaceFunctor::get(VoxelCoordinate pos) {
		if(!patternVoxelIDReplaceErrorCalled)
			printlnf(ERROR,
				"Called a VoxelID set on a VoxelIDReplaceFunctor! You should be using a VoxelIDFunctor for this task, my friend!"
				" If you're getting a bunch of air voxels for seemingly no reason, this is why. It's really not a difficult fix, you could've already been done by now"
				" if you hadn't wasted your time reading this message."
			);
		patternVoxelIDReplaceErrorCalled = true;
		return 0;
	}




}

