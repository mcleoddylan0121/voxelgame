/*
 * structuremanager.cpp
 *
 *  Created on: Jul 5, 2016
 *      Author: dylan
 */

#include "structuremanager.h"
#include "coords.h"


namespace vox {


	std::vector<VoxelEdit> StructureUnit::evaluate() {
		evaluated = true;

		Rand rand; // TODO: use hash based on world seed for this // TODO: determine this by features
		// ^ TODO: make a nice ol' lua wrapper for this guy
		// Lots of R&D needed for this little Rand, huh?

		std::vector<VoxelEdit> ret;

		for(Structure* s:structures) {
			PatternReturn* edits = s->evaluate(rand);
			for(auto& edit:edits->changes) {
				VoxelCoordinate worldPosition = edit.position + edits->position + s->position;

				ret.push_back(VoxelEdit{edit.id,worldPosition});
			}
		}

		return ret;
	}

	StructureUnit* StructureManager::createStructureUnit(ChunkCoordinate position) {
		StructureUnit* nUnit = new StructureUnit{};
		nUnit->parent = this;
		nUnit->position = position;

		if(rand()%10 == 0) {
			VoxelCoordinate randCoord = VoxelCoordinate(rand()%32,rand()%32,rand()%32) + chunkToVoxelCoordinate(position);

			LuaEvaluatedStructure* theHeckabigboxman = new LuaEvaluatedStructure(randCoord,"big_ol_square");
			nUnit->structures.push_back(theHeckabigboxman);
		}

		units[position] = nUnit;
		return nUnit;

	}

	std::vector<VoxelEdit> StructureManager::evaluateStructureUnit(ChunkCoordinate position) {
		auto ptr = units.find(position);
		StructureUnit* theUnit = nullptr;
		if(ptr == units.end()) {
			theUnit = createStructureUnit(position);
		} else {
			theUnit = ptr->second;
		}
		std::vector<VoxelEdit> ret = theUnit->evaluate();
		return ret;
	}



#define tickQueue(queue)\
	\
for(unsigned int i = 0; i < queue.size(); ++i) {\
	auto j = queue[i];\
	j.elapsedLifetime += delta;\
	if(j.elapsedLifetime > j.LIFETIME)\
		queue.erase(queue.begin() + i);\
}

	void StructureManager::tick(float delta) {

		tickQueue(toEvalQueue);

		for(int i = 0; i < evalsPerTick; ++i) {
			if(toEvalQueue.empty()) break;
			UnfinishedStructureEval ev = toEvalQueue.back();
			toEvalQueue.pop_back();

			evaluateAndDistributeStructureUnit(ev.position);
		}
	}

	void StructureManager::queueStructureUnitAndNeighbors(ChunkCoordinate position) {
		ChunkCoordinate start = position - STRUCTURE_SEARCH_RADIUS;
		ChunkCoordinate end   = position + STRUCTURE_SEARCH_RADIUS;

		ChunkCoordinate curPos;
		for(curPos.z = start.z; curPos.z <= end.z; ++curPos.z)
		for(curPos.y = start.y; curPos.y <= end.y; ++curPos.y)
		for(curPos.x = start.x; curPos.x <= end.x; ++curPos.x) {
			queueStructureUnit(curPos);
		}
	}

	void StructureManager::queueStructureUnit(ChunkCoordinate position) {
		UnfinishedStructureEval ev;
		ev.position = position;
		toEvalQueue.push_back(ev);
	}

	void StructureManager::evaluateAndDistributeStructureUnit(ChunkCoordinate position) {
		std::vector<VoxelEdit> edits = evaluateStructureUnit(position);

		// TODO: Implementation currently temporary! (extremely temporary; doesn't even work on chunk boundaries, and is very, very slow)
		for(auto edit:edits) {
			ChunkCoordinate editChunk = voxelToChunkCoordinate(edit.position);

			auto found = units.find(editChunk);
			if(found == units.end()) continue; // don't distribute this one, i guess? We should have a better way of handling this, eventually
			StructureUnit* containingUnit = found->second;
			containingUnit->edits.push_back({edit.id,voxelToLocalChunkCoordinate(edit.position)});
		}
	}

	bool StructureManager::structureUnitAvailible(ChunkCoordinate position) {
		bool ret = true;

		ChunkCoordinate start = position - STRUCTURE_SEARCH_RADIUS;
		ChunkCoordinate end   = position + STRUCTURE_SEARCH_RADIUS;

		ChunkCoordinate curPos;
		for(curPos.z = start.z; curPos.z <= end.z; ++curPos.z)
		for(curPos.y = start.y; curPos.y <= end.y; ++curPos.y)
		for(curPos.x = start.x; curPos.x <= end.x; ++curPos.x) {
			auto found = units.find(curPos);
			ret &= ( found != units.end() ) && ( found->second->evaluated );
		}

		return ret;
	}

	StructureUnit* StructureManager::getStructureUnit(ChunkCoordinate position) {
		auto found = units.find(position);

		if(found == units.end() || !found->second->evaluated) return nullptr;

		return found->second;
	}


}
