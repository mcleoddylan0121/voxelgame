/*
 * structuremanager.h
 *
 *  Created on: Jul 5, 2016
 *      Author: dylan
 */

#ifndef SRC_PROCEDURAL_STRUCTURES_STRUCTUREMANAGER_H_
#define SRC_PROCEDURAL_STRUCTURES_STRUCTUREMANAGER_H_

#include "chunk.h"
#include "geometry.h"
#include "structure.h"

namespace vox {

	class StructureManager;

	// Holds all of the structures inside this chunk coordinate
	class StructureUnit {
	public:
		StructureManager* parent;

		std::vector<VoxelEdit> edits; // All of the collective changes to this chunk made by structures
		ChunkCoordinate position;

		std::vector<Structure*> structures;
		bool evaluated = false;
		std::vector<VoxelEdit> evaluate();
	};

	struct UnfinishedStructureEval {
		static constexpr float LIFETIME = 5.f;
		float elapsedLifetime = 0.f;
		ChunkCoordinate position;
	};

	class StructureManager {
	public:
		std::vector<UnfinishedStructureEval> toEvalQueue;

		// Should be larger than 1/2 of how big a structure can ever possibly be
		ChunkCoordinate STRUCTURE_SEARCH_RADIUS = ChunkCoordinate(0);

		std::unordered_map<ChunkCoordinate, StructureUnit*, ChunkHash> units;

		void makeStructureUnit(StructureUnit* position);

		StructureUnit* createStructureUnit(ChunkCoordinate position);
		std::vector<VoxelEdit> evaluateStructureUnit(ChunkCoordinate position);

		void queueStructureUnitAndNeighbors(ChunkCoordinate position); // set guy up for loading and his neighbors
		void queueStructureUnit(ChunkCoordinate position); // set this guy up for loading

		int evalsPerTick = 5;

		void tick(float delta);
		void evaluateAndDistributeStructureUnit(ChunkCoordinate position); // eval then push the VoxelEdits to respective units

		bool structureUnitAvailible(ChunkCoordinate position); // asks whether or not him and his neighbors are ready
		StructureUnit* getStructureUnit(ChunkCoordinate position); // nab the unit, cause he's done

	};


}


#endif /* SRC_PROCEDURAL_STRUCTURES_STRUCTUREMANAGER_H_ */
