/*
 * structure.h
 *
 *  Created on: Jul 5, 2016
 *      Author: dylan
 */

#ifndef SRC_PROCEDURAL_STRUCTURES_STRUCTURE_H_
#define SRC_PROCEDURAL_STRUCTURES_STRUCTURE_H_

#include "randomvar.h"
#include "geometry.h"

namespace vox {

	// Not air, but instead nothing. The difference is that air overwrites other voxels, while this simply does nothing.
	constexpr VoxelID NULL_VOXELID = 65535;

	struct VoxelEdit {
		VoxelID id;
		LocalCoordinate position;
	};

	struct PatternReturn {
		std::vector<VoxelEdit> changes;
		VoxelCoordinate position, size;

		PatternReturn* copy() {
			return new PatternReturn{changes,position,size};
		}
	};


	inline VoxelID mixLmao(int mx, VoxelID _a, VoxelID _b) { return mx*_a + _b-mx*_b; } // Fast little way to choose between 2 different voxel IDs (2 instructions)

	/*
	 * Generates a VoxelID based on the position
	 * Useful for integrating noise, randomness, etc.
	 *
	 * For example, a random VoxelID functor could be used to have mossy bricks placed intermittently
	 * Or you could create a checkered tile pattern by having a going position.x + position.z % 2
	 *
	 * Note that these are called with the local position, not the global one (that could change in the future, who knows)
	 */
	class VoxelIDFunctor {
	public:
		virtual VoxelID get(VoxelCoordinate pos) = 0;
		virtual VoxelID get(VoxelCoordinate pos, VoxelID id) { return get(pos); }
		virtual ~VoxelIDFunctor() {}
	};

	/*
	 * Mostly the same as a regular functor, but it's applied to a list of VoxelEdits, having specific behavior based on the VoxelID at each point
	 * Using this we can palette swap .vox files, and also incorporate some more interesting randomized, pattern-based, and noise based changes
	 * Coming up with interesting changes is an exercise left to the reader
	 */
	class VoxelIDReplaceFunctor: public VoxelIDFunctor {
	public:
		virtual VoxelID get(VoxelCoordinate pos, VoxelID id) = 0;
		virtual VoxelID get(VoxelCoordinate pos);
		virtual ~VoxelIDReplaceFunctor() {}
	};



	class Structure {
	public:
		VoxelCoordinate position;
		virtual ~Structure() {}

		Structure(VoxelCoordinate position): position(position) {}
		virtual PatternReturn* evaluate(Rand& rand) = 0;
	};

	// This is probably most structures, aside from basic ones that will be placed a lot, I guess
	class LuaEvaluatedStructure: public Structure {
	public:
		std::string name;
		LuaEvaluatedStructure(VoxelCoordinate position, std::string name): Structure(position), name(name) {}
		virtual PatternReturn* evaluate(Rand& rand);
	};

	struct LuaStructureEvaluation {
		int ID;

		// We delete these guys when we're done
		// They're just temporary to aid with
		std::vector<PatternReturn*> patterns; // indexed
		std::vector<VoxelIDFunctor*> functors; // also indexed

		PatternReturn* ret;

		bool inUse = false;

		Rand* rand; // AGH

		void endUse();
		void beginUse(Rand& rand);

		int addPattern(PatternReturn* patt);
		int addFunctor(VoxelIDFunctor* functor);
	};

	LuaStructureEvaluation* getStructureEvaluation(int ID);

}



#endif /* SRC_PROCEDURAL_STRUCTURES_STRUCTURE_H_ */
