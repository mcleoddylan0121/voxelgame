/*
 * patterns.h
 *
 *  Created on: Jul 17, 2016
 *      Author: Dylan
 */

#ifndef SRC_PROCEDURAL_STRUCTURES_PATTERNS_H_
#define SRC_PROCEDURAL_STRUCTURES_PATTERNS_H_

#include "structure.h"
#include "luacontext.h"
#include "vluautil.h"
#include "luareg.h"

namespace vox {

namespace pat {

	// Every pattern is called via:
	// Rand&, VoxelIDFunctor*, args
	// This just kinda makes our lives easier I guess

	PatternReturn* makeBoxPattern(Rand* rand, VoxelIDFunctor* id, VoxelCoordinate position, VoxelCoordinate size) {
		PatternReturn* ret = new PatternReturn{};
		ret->position = position;
		ret->size = size;
		glm::ivec3 lPos;

		for(lPos.z = 0; lPos.z < size.z; ++lPos.z)
		for(lPos.y = 0; lPos.y < size.y; ++lPos.y)
		for(lPos.x = 0; lPos.x < size.x; ++lPos.x) {
			ret->changes.push_back(VoxelEdit{ id->get(lPos), lPos + position });
		}

		return ret;
	}

#define PATTERN_CREATE_START_DEF(ID) LuaStructureEvaluation* eval = getStructureEvaluation(ID); Rand* rand = eval->rand


	int PATTERN_CREATE_BOX(int ID, int voxelIDFunctor, lvec3 position, lvec3 size) {
		PATTERN_CREATE_START_DEF(ID);
		VoxelIDFunctor* functor = eval->functors[voxelIDFunctor];
		return eval->addPattern(makeBoxPattern(rand, functor, glm::ivec3(glm::vec3(position)), glm::ivec3(glm::vec3(size))));
	}

	int PATTERN_CREATE_BOX_START_END(int ID, int voxelIDFunctor, lvec3 start, lvec3 end) {
		glm::vec3 s(start.x,start.y,start.z);
		glm::vec3 e = glm::vec3(end.x,end.y,end.z);
		glm::vec3 position(glm::min(s,e));
		glm::vec3 size = glm::max(s,e) - position + 1.f;

		//size = glm::max(size, 1.f);

		PATTERN_CREATE_START_DEF(ID);
		VoxelIDFunctor* functor = eval->functors[voxelIDFunctor];
		return eval->addPattern(makeBoxPattern(rand, functor, glm::ivec3(position), glm::ivec3(size)));
	}


	PatternReturn* createRoofFrameDiagonal(Rand* rand, std::vector<VoxelIDFunctor*> functors, VoxelCoordinate position, VoxelCoordinate size,
			int cycleDim, int lengthDim, int dependentDim, int invSlope, bool flipDir) {
		PatternReturn* ret = new PatternReturn{};
		ret->position = position;
		ret->size = size;

		int roofDir = dependentDim == 1? cycleDim:dependentDim;

		VoxelCoordinate curPos;
		for(int l = 0; l < size[lengthDim]; l++) {
			curPos[lengthDim] = position[lengthDim] + l;

			for(int c = 0; c < size[cycleDim]; c++) {
				curPos[cycleDim] = position[cycleDim] + c;

				int s = c / invSlope;

				curPos[dependentDim] = position[dependentDim] + s;

				if(flipDir) curPos[roofDir] = -(curPos[roofDir] - position[roofDir]) + position[roofDir];

				VoxelIDFunctor* curFunctor = functors[c % invSlope];

				ret->changes.push_back( VoxelEdit { curFunctor->get(curPos), curPos } );
			}
		}

		return ret;
	}

	// input: a + 3*b, returns c in range [0,2] such that c doesn't equal either a or b.
	const int greatestTable[9] = {
			0,2,1,
			2,0,0,
			1,0,0
	};

	std::vector<VoxelIDFunctor*> getFunctorArray(LuaStructureEvaluation* eval, LuaArray<int> in) {
		std::vector<VoxelIDFunctor*> ret(in.size());
		for(unsigned int i = 0; i < in.size(); i++) {
			ret[i] = eval->functors[in[i]];
		}
		return ret;
	}

	int PATTERN_CREATE_ROOF_FRAME_DIAGONAL(int ID, LuaArray<int> functors, lvec3 position, lvec3 size, int cycleDim, int lengthDim, int invSlope, bool flipDir) {
		PATTERN_CREATE_START_DEF(ID);
		int dependentDim = greatestTable[cycleDim + 3*lengthDim];
		std::vector<VoxelIDFunctor*> fns = getFunctorArray(eval, functors);
		return eval->addPattern(createRoofFrameDiagonal(rand, fns, glm::ivec3(glm::vec3(position)), glm::ivec3(glm::vec3(size)), cycleDim, lengthDim, dependentDim, invSlope, flipDir));
	}




	// VoxelID functors


#define VOXEL_ID_FUNCTOR_START_DEF(ID) LuaStructureEvaluation* eval = getStructureEvaluation(ID);


	class StaticVoxelIDFunctor: public VoxelIDFunctor {
	public:
		VoxelID id;
		VoxelID get(VoxelCoordinate pos) { return id; }

		StaticVoxelIDFunctor(VoxelID id): id(id) {}
	};

	int VOXEL_ID_FUNCTOR_CREATE_CONSTANT(int ID, int vid) {
		VOXEL_ID_FUNCTOR_START_DEF(ID);
		VoxelIDFunctor* functor = new StaticVoxelIDFunctor(vid);
		return eval->addFunctor(functor);
	}


	// Makes a checkerboard, if you're into that sort of thing
	class CheckerboardFunctor: public VoxelIDFunctor {
	public:
		VoxelID id1, id2;
		VoxelID get(VoxelCoordinate pos) { return mixLmao((pos.x+pos.y+pos.z)%2, id1, id2); }

		CheckerboardFunctor(VoxelID id1, VoxelID id2): id1(id1), id2(id2) {}
	};

	int VOXEL_ID_FUNCTOR_CREATE_CHECKERBOARD(int ID, int vid, int vid2) {
		VOXEL_ID_FUNCTOR_START_DEF(ID);
		VoxelIDFunctor* functor = new CheckerboardFunctor(vid,vid2);
		return eval->addFunctor(functor);
	}


	PatternReturn* mergePatterns(std::vector<PatternReturn*> in) {
		PatternReturn* ret = new PatternReturn{};
		ret->position = VoxelCoordinate(0);
		ret->size = VoxelCoordinate(10);
		for(unsigned int i = 0; i < in.size(); i++) {
			ret->changes.insert(ret->changes.end(), in[i]->changes.begin(), in[i]->changes.end());
		}
		return ret;
	}


	int MERGE_PATTERNS(int ID, LuaArray<int> patterns) {
		LuaStructureEvaluation* eval = getStructureEvaluation(ID);
		std::vector<PatternReturn*> pats;
		for(unsigned int i = 0; i < patterns.size(); i++) pats.push_back(eval->patterns[patterns[i]]);
		return eval->addPattern(mergePatterns(pats));
	}





	void PATTERN_ASSIGN_STRUCTURE_FINAL_PATTERN(int ID, int patternID) {
		LuaStructureEvaluation* eval = getStructureEvaluation(ID);
		PatternReturn* ret = eval->patterns[patternID];
		eval->ret = ret->copy();;
	}






	// VoxelID replacement functors


	// For Palette swapping I guess?
	class StaticVoxelIDReplaceFunctor {
	public:
		VoxelID getID, setID;
		VoxelID get(VoxelCoordinate pos, VoxelID id) { return mixLmao(id == getID,setID,id); } // id == getID? setID:id;

		StaticVoxelIDReplaceFunctor(VoxelID getID, VoxelID setID): getID(getID), setID(setID) {}
	};

}


}


#endif /* SRC_PROCEDURAL_STRUCTURES_PATTERNS_H_ */
