/*
 * randomvar.h
 *
 *  Created on: Jul 6, 2016
 *      Author: dylan
 */

#ifndef SRC_PROCEDURAL_STRUCTURES_RANDOMVAR_H_
#define SRC_PROCEDURAL_STRUCTURES_RANDOMVAR_H_

#include "vglm.h"
#include "util.h"

namespace vox {

	// I'm not entirely sure that templates are really even necessary for this, but w/e
	// The idea is that we can choose different distributions, with a uniform distribution being the default
	// And such distributions can be chosen via lua, via inheritance
	// Should return within [min, max] always, for consistency's sake (however it shouldn't break anything if this rule isn't followed)
	template<typename T>
	struct RandomVariable {
		T min, max;
		virtual T get(Rand* rand) { return min + rand->getFloat(0,1) * max; }
		virtual ~RandomVariable() {}
	};

	// Makes a Gaussian distribution, clamped to [min,max] (as extreme outliers could be rather devastating)
	template<typename T>
	struct RandomGaussian {

	};

	// Random single-variable generator
	#define RandomGenType(type,func)\
	template<>\
	struct RandomVariable<type> {\
		type min, max;\
		virtual type get(Rand* rand) {\
			return rand->func(min,max);\
		}\
		virtual ~RandomVariable<type>() {}\
	}

	// Random GLM generator
	#define RandomGLM(type,sz,func)\
	template<>\
	struct RandomVariable<type> {\
		type min, max;\
		virtual type get(Rand* rand) {\
			type ret(min);\
			for(int i = 0; i < sz; i++) ret[i] = rand->func(min[i],max[i]);\
			return ret;\
		}\
		virtual ~RandomVariable<type>() {}\
	}

	// Defines RandomVariable<type> for all of these types:
	// float -> vec4, int -> vec4

	RandomGenType(float,getFloat);
	RandomGenType(int, getInt);

	RandomGLM(glm::vec2, 2,getFloat);
	RandomGLM(glm::vec3, 3,getFloat);
	RandomGLM(glm::vec4, 4,getFloat);

	RandomGLM(glm::ivec2,2,getInt);
	RandomGLM(glm::ivec3,3,getInt);
	RandomGLM(glm::ivec4,4,getInt);

}



#endif /* SRC_PROCEDURAL_STRUCTURES_RANDOMVAR_H_ */
