#ifndef SIMPLEX_HPP_
#define SIMPLEX_HPP_

#include "util.h"
#include "coords.h"

#include <glm/gtc/noise.hpp>
#include <vector>
#include <cstdint>
#include <cmath>
#include <functional>
#include <tr1/memory>
#include <cmath>

namespace vox {

	class Asset;

namespace noise {

	typedef float value_t;

	struct NoiseRange1D {
		int size;
		float offs, mul;
		value_t* ptr;

		value_t& operator()(int pos) { return ptr[pos]; }
		float getPosition(int pos) { return pos*mul+offs; }
		int getSize() { return size; }
	};

	struct NoiseRange2D {
		glm::ivec2 size;
		glm::vec2 offs, mul;
		value_t* ptr;

		value_t& operator()(int index) { return ptr[index]; }
		glm::vec2 getPosition(int index) { return glm::vec2(index%size.x,index/size.x)*mul+offs; }
		int getSize() { return size.x * size.y; }
	};

	struct NoiseRange3D {
		glm::ivec3 size;
		glm::vec3 offs, mul;
		value_t* ptr;

		value_t& operator()(int index) { return ptr[index]; }
		glm::vec3 getPosition(int index) { return glm::vec3(index%size.x,(index/size.x)%size.y,(index/size.x)/size.y)*mul+offs; }
		int getSize() { return size.x * size.y * size.z; }
	};

// Macros are somehow easier than virtual functions I guess

#define ONE_DIMENSIONAL_NOISE_DIM NoiseRange1D, int, float
#define TWO_DIMENSIONAL_NOISE_DIM NoiseRange2D, glm::ivec2, glm::vec2
#define THREE_DIMENSIONAL_NOISE_DIM NoiseRange3D, glm::ivec3, glm::vec3

#define modifyNoiseRange_expand(range,function,range_t,key_t,position_t) for(int i = 0; i < range.getSize(); i++) function(range(i)); }

#define modifyNoiseRange(range,function,dim) iterateNoiseRange_expand(range,function,dim)

#define defModifyAllDims(function)\
	virtual value_t noise1D(NoiseRange1D* in) {

	/* Terrain Generator base class.
	 *
	 * Note that for a noise to be valid, it must follow these rules:
	 *
	 *		- Calling a function with the same arguments will give the same result every time
	 *  	- Calling any noise should return a value in [min, max]
	 *		- After construction, it is immutable (or at the very least, is not changed)
	 *
	 * Another thing to take note of is the fact that you only need to define the noises you intend to use.
	 * So, in Rotate3D, you, at the very least, only need to define noise3D.
	 */
	class Noise {
	public:

		typedef float value_t;

		value_t min, max;

		// You only have to define the ones you need. Most likely only 2D/3D, but feel free to do the rest.
		virtual value_t noise1D(float pos);
		virtual value_t noise2D(const glm::vec2& pos);
		virtual value_t noise3D(const glm::vec3& pos);
		virtual value_t noise4D(const glm::vec4& pos);
		virtual value_t noise5D(const float* pos);

		// Make sure to delete the pointer when you're done (TODO: use a vector you idiot)
		virtual value_t* range1D(const float pos, const int range, const float step = 1.f);
		virtual value_t* range2D(const glm::vec2& pos, const glm::ivec2& range, const glm::vec2& step = {1, 1});
		virtual value_t* range3D(const glm::vec3& pos, const glm::ivec3& range, const glm::vec3& step = {1, 1, 1});
		virtual value_t* range4D(const glm::vec4& pos, const glm::ivec4& range, const glm::vec4& step = {1, 1, 1, 1});

		// Has science gone too far? I'd have to say no.
		virtual value_t* range5D(const float* pos, const int* range, const float* step);

		virtual ~Noise() {}
		Noise(value_t min, value_t max): min(min), max(max) {}

		static const std::string name;

		static Noise* readFile(std::string filename, Rand& seed, std::string varname = "noise");

		virtual std::string getGPUNoiseString(int dims, std::vector<float> inputmul);

		// SetRange doesn't necessarily have to be _good_, because its only used by one thing. You only really need to care about it if you need it for some reason
		virtual void setRange(value_t min, value_t max) { this->min = min; this->max = max; }
	};

	// Implicit constructor, make our lives easier
	struct NoisePtr: public std::tr1::shared_ptr<Noise> {
	public: NoisePtr(Noise* n = nullptr): std::tr1::shared_ptr<Noise>(n) {}
	};

	/*
	 * A generator follows these rules:
	 *
	 * 		- The genNoiseND functions will generate a noise [0, 1]
	 *
	 */
	class Generator: public Noise {
	protected:
		virtual value_t genNoise2D(const glm::vec2& pos) = 0;
		virtual value_t genNoise3D(const glm::vec3& pos) = 0;
	public:
		Generator(value_t min, value_t max): Noise(min, max) {}
		virtual ~Generator() {}
		virtual inline value_t noise2D(const glm::vec2& pos) final {
			return genNoise2D(pos) * (max - min) + min;
		}
		virtual inline value_t noise3D(const glm::vec3& pos) final {
			return genNoise3D(pos) * (max - min) + min;
		}
		static const std::string name;
		void setRange(value_t min, value_t max) { this->min = min; this->max = max; }
	};

	// The selector noise selects which noise to use.
	// This one is rather complicated, but this is what it is based off of:
	// http://libnoise.sourceforge.net/glossary/index.html#selectormodule
	// A low output of _selector favors a, while a high one favors b
	// The output is the weighted average of a and b based on _selector
	class Selector: public Noise {
	protected:
		NoisePtr a, b;
		NoisePtr _selector;
	public:
		Selector(NoisePtr a, NoisePtr b, NoisePtr _selector);
		~Selector() { }
		value_t noise2D(const glm::vec2& pos);
		value_t noise3D(const glm::vec3& pos);
		static const std::string name;

		// TODO: untested
		void setRange(value_t min, value_t max) {
			this->min = min; this->max = max;
			value_t tmax = fmax(a->max, b->max);
			value_t tmin = fmin(a->min, b->min);
			value_t total = tmax - tmin;
			value_t ratioa = (a->max-a->min) / total;
			a->max /= ratioa; a->min /= ratioa;
			a->setRange(a->min/ratioa, a->max/ratioa);
			value_t ratiob = (b->max-b->min) / total;
			b->setRange(b->min/ratiob, a->max/ratiob);
		}
	};

	// Modifies the noise passed into the constructor. Various modifications are avalible.
	class Modifier: public Noise {
	protected:
		NoisePtr noise;
	public:
		Modifier(NoisePtr noise): Noise(noise->min, noise->max), noise(noise) {}
		Modifier(NoisePtr noise, value_t min, value_t max): Noise(min, max), noise(noise) {}
		virtual ~Modifier() { }
		static const std::string name;
		void setRange(value_t min, value_t max) { this->min = min; this->max = max; noise->setRange(min, max); }
	};

	class OutputModifier: public Modifier {
	protected:
		virtual value_t doStuff(value_t input) = 0;
	public:
		virtual value_t noise1D(float pos) final;
		virtual value_t noise2D(const glm::vec2& pos) final;
		virtual value_t noise3D(const glm::vec3& pos) final;
		virtual value_t noise4D(const glm::vec4& pos) final;
		virtual value_t noise5D(const float* pos) final;
		OutputModifier(NoisePtr noise): Modifier(noise) {}
		OutputModifier(NoisePtr noise, value_t min, value_t max): Modifier(noise, min, max) {}
		virtual ~OutputModifier() { }
		static const std::string name;
		void setRange(value_t min, value_t max) { Modifier::setRange(min, max); }
	};

	// If noise < boundary, return min, if noise > boundary, return max.
	// Be sure that the included noise is [0, 1]
	class BiSelector: public OutputModifier {
	private:
		value_t boundary;
	protected:
		value_t doStuff(value_t input) { return input < boundary? min : max; }
	public:
		BiSelector(NoisePtr noise, value_t boundary, value_t min, value_t max);
		static const std::string name;
		void setRange(value_t min, value_t max) { this->min = min; this->max = max; }
	};

	// Constrains the given noise within [min, max]
	class Constrainer: public OutputModifier {
	protected:
		value_t doStuff(value_t input);
	public:
		Constrainer(NoisePtr noise, value_t min, value_t max): OutputModifier(noise, min, max) {}
		static const std::string name;
		std::string getGPUNoiseString(int dims, std::vector<float> inputmul);
		void setRange(value_t min, value_t max) { this->min = min; this->max = max; noise->setRange(min, max); }
	};

	// Rotates input points around the origin by the given angle. In radians.
	// Also, you should probably not use these. They make my computer cry. ;_;
	class Rotate2D: public Modifier {
	protected:
		float angle;
	public:
		virtual value_t noise2D(const glm::vec2& pos);
		virtual value_t noise3D(const glm::vec3& pos);

		Rotate2D(NoisePtr noise, float angle): Modifier(noise), angle(angle) {}

	};

	class Rotate3D: public Rotate2D {
	protected:
		float ang2;
	public:
		virtual value_t noise3D(const glm::vec3& pos); // noise2D is in Rotate2D.

		Rotate3D(NoisePtr noise, float angle, float ang2): Rotate2D(noise, angle), ang2(ang2) {}
	};

	// "Stretches" the noise by the scaleMode
	class Scalar: public Modifier {
	private:
		float scale1;
		glm::vec2 scale2;
		glm::vec3 scale3;
		glm::vec4 scale4;
		float scale5[5]; // scale makes output wider
	public:
		value_t noise2D(const glm::vec2& pos) { return noise->noise2D(pos / scale2); }
		value_t noise3D(const glm::vec3& pos) { return noise->noise3D(pos / scale3); }
		Scalar(NoisePtr noise, std::vector<float> scaleMod): Modifier(noise) {
			for(int i = 0; i < 5; i++) scale5[i] = scaleMod[i];
			scale1 = scale5[0];
			scale2 = glm::vec2(scale5[0], scale5[1]);
			scale3 = glm::vec3(scale5[0], scale5[1], scale5[2]);
			scale4 = glm::vec4(scale5[0], scale5[1], scale5[2], scale5[3]);
		}
		Scalar(NoisePtr noise, float s): Scalar(noise, {s,s,s,s,s}) {}
		static const std::string name;
		std::string getGPUNoiseString(int dims, std::vector<float> inputmul);

		void setRange(value_t min, value_t max) { this->min = min; this->max = max; noise->setRange(min, max); }
	};

	// I'm not 100% sure why I need this class
	class SharedNoise: public Noise {
	private:
		NoisePtr noise;
	public:
		SharedNoise(NoisePtr n): Noise(n->min, n->max), noise(n) {}
		value_t noise1D(const float pos) { return noise->noise1D(pos); }
		value_t noise2D(const glm::vec2& pos) { return noise->noise2D(pos); }
		value_t noise3D(const glm::vec3& pos) { return noise->noise3D(pos); }
		value_t noise4D(const glm::vec4& pos) { return noise->noise4D(pos); }

		static const std::string name;
	};

	// Shift, or stretch the output of a noise into the given range
	class OutputShifter: public OutputModifier {
	private:
		value_t add, multiply;
	protected:
		value_t doStuff(value_t val) { return val * multiply + add; }
	public:
		OutputShifter(NoisePtr noise, value_t min, value_t max): OutputModifier(noise, min, max) {
			setRange(min, max);
		}
		static const std::string name;
		std::string getGPUNoiseString(int dims, std::vector<float> inputmul);

		void setRange(value_t min, value_t max) {
			this->min = min; this->max = max;
			value_t diff1 = noise->max - noise->min;
			value_t diff2 = max - min;
			multiply = diff2 / diff1;

			add = min - noise->min * multiply;
		}
	};

	class InputModifier: public Modifier {
	protected:
		virtual float mod1D(const float pos)         { return pos; }
		virtual glm::vec2 mod2D(const glm::vec2 pos) { return pos; }
		virtual glm::vec3 mod3D(const glm::vec3 pos) { return pos; }
		virtual glm::vec4 mod4D(const glm::vec4 pos) { return pos; }
	public:
		virtual value_t noise1D(const float pos) final      { return noise->noise1D(mod1D(pos)); }
		virtual value_t noise2D(const glm::vec2& pos) final { return noise->noise2D(mod2D(pos)); }
		virtual value_t noise3D(const glm::vec3& pos) final { return noise->noise3D(mod3D(pos)); }
		virtual value_t noise4D(const glm::vec4& pos) final { return noise->noise4D(mod4D(pos)); }
		InputModifier(Noise* in): Modifier(in) {}
		static const std::string name;
	};

	class InputWarper: public InputModifier {
	private:
		NoisePtr warp;
	protected:
		virtual glm::vec2 mod2D(const glm::vec2 pos) { return pos + warp->noise2D(pos); }
		virtual glm::vec3 mod3D(const glm::vec3 pos) { return pos + warp->noise3D(pos); }
	public:
		InputWarper(Noise* in, Noise* warp): InputModifier(in), warp(warp) {}
		static const std::string name;
	};

	// Are you missing something in your life? Do you like bells? Do you want your noise functions
	// to do exactly what you want? If you answered yes to any or none of these questions, try
	// the Guassianator today and modify that distribution!
	// http://en.wikipedia.org/wiki/Gaussian_function
	class Guassianator: public OutputModifier {
	private:
		float a, b, cSq;
		value_t min, max;
		value_t doStuff(value_t x) { return constrain(a * exp(-(x-b)*(x-b) / (2 * cSq)), min, max); }

	public:
		// 99.7% of the output will lie 3*sigma away from shift in each direction
		// Shift can also be called the expected output, as it will be the focus of the curve
		Guassianator(NoisePtr noise, float sigma, float shift, value_t min, value_t max): OutputModifier(noise) {
			a = 1/(2.50662827463 * sigma); // 2.50662827463 is sqrt(2*pi)
			b = shift;
			cSq = sigma * sigma;
			this->min = min; this->max = max;
		}
		static const std::string name;

		void setRange(value_t min, value_t max) { this->min = min; this->max = max; }
	};

	// Takes the given noise, and fits a discrete valued curve over it (I.E. it rounds up)
	// Technically, it rounds away from 0, but that shouldn't matter
	class DiscreteFit: public OutputModifier {
	private:
	public:
		value_t stepsize;
		inline static value_t rndMultiple(value_t val, value_t mlt) {
		   return floor(val / mlt) * mlt;
		}
		value_t doStuff(value_t val) { return rndMultiple(val - min, stepsize) + min; }
		DiscreteFit(NoisePtr noise, int steps): OutputModifier(noise) {
			this->stepsize = (max - min)/steps;
		}
		static const std::string name;
		std::string getGPUNoiseString(int dims, std::vector<float> inputmul);

		void setRange(value_t min, value_t max) { this->min = min; this->max = max; noise->setRange(min, max); }
	};

	// Makes a curve that looks sorta like clouds
	class CloudCutoff: public OutputModifier {
	private:
		float density; // 0 = no clouds, 1 = nothing but clouds, though 0 and 1 aren't actually valid
		float sharpness; // 0 = sharp clouds, 1 = fuzzy clouds, though 0 and 1 aren't actually valid
	public:
		value_t doStuff(value_t val) {
			return 1.f - (pow(sharpness, constrain(val - density, 0, 1)));
		}
		CloudCutoff(NoisePtr noise, float density, float sharpness): OutputModifier(noise), density(density), sharpness(sharpness) {}
		static const std::string name;
		std::string getGPUNoiseString(int dims, std::vector<float> inputmul);

		void setRange(value_t min, value_t max) { this->min = min; this->max = max; noise->setRange(min, max); }
	};

	// Map to a given output
	class OutputMapper: public OutputModifier {

	};

	// This is a generalized modifier. Inputs the output of the noise (normalized to [0, 1])
	// into the input of the curve, then re-weights the curve's output (also in [0, 1])
	class FitCurve: public OutputModifier {
	private:
		std::function<value_t (value_t)> curve; // takes [0, 1] as input, outputs [0, 1]
		std::string functionName;
		value_t doStuff(value_t val) { return (curve((val - min) / (max - min)) * (max - min)) + min; }
	public:
		FitCurve(NoisePtr noise, std::function<value_t (value_t)> curve, std::string functionName = "NULL"): OutputModifier(noise), curve(curve), functionName(functionName) {}
		static const std::string name;
		std::string getGPUNoiseString(int dims, std::vector<float> inputmul);

		void setRange(value_t min, value_t max) { this->min = min; this->max = max; noise->setRange(min, max); }
	};

	/*
	 * Takes multiple noises and puts them together. This is extremely
	 * useful for making well-done, highly nuanced noise functions.
	 *
	 * However, be sure to understand how ranges operate in a combined noise.
	 *
	 * 		- For all included noises, the min value MUST be 0, because anything else doesn't actually make sense
	 * 		- An included weight's max acts as it's weight.
	 * 		    ~ For example, if we have 2 noises specified, with maxes 1 and 2, respectively,
	 * 			  then the second noise's output matters twice as much as the first one's when calculating a noise
	 *
	 * 		- The output of this is automatically scaled to the specified min/max
	 */
	class CombinedNoise: public Noise {
	private:
		std::vector<NoisePtr> noises;
		value_t cMax = 0; // combined max
	protected:
		void operator +=(NoisePtr nNoise);
	public:
		virtual value_t noise2D(const glm::vec2& pos);
		virtual value_t noise3D(const glm::vec3& pos);
		virtual ~CombinedNoise();
		CombinedNoise(const std::vector<NoisePtr>& noises = {});
		static const std::string name;
		std::string getGPUNoiseString(int dims, std::vector<float> inputmul);

		void setRange(value_t min, value_t max) { this->min = min; this->max = max; }
	};

	class Multiplier: public Noise {
	private:
		NoisePtr a, b;
	public:
		virtual value_t noise2D(const glm::vec2& pos);
		virtual value_t noise3D(const glm::vec3& pos);
		virtual ~Multiplier() {}
		Multiplier(NoisePtr a, NoisePtr b);
		static const std::string name;
		std::string getGPUNoiseString(int dims, std::vector<float> inputmul);
	};

	// Based on www.itn.liu.se/~stegu/simplexnoise/simplexnoise.pdf.
	// We may not want to include this in the release, because of legal issues.
	class Simplex: public Generator {
	protected:
		uint8_t p[256];
		uint8_t perm[512];
		uint8_t permMod12[512];

		value_t genNoise2D(const glm::vec2& pos) final;
		value_t genNoise3D(const glm::vec3& pos) final;
	public:
		Simplex(Rand::Seed seed, value_t min, value_t max);
		std::string getGPUNoiseString(int dims, std::vector<float> inputmul);
		static const std::string name;
		void setRange(value_t min, value_t max) { this->min = min; this->max = max; }
	};

	// Open source implementation of an O(n^2) noise.
	// Works really well in octaves, with a persistence of ~0.5
	// However, there are some issues with this, because this noise type does not seem to take the full [0, 1] range (?)
	class OpenSimplex: public Generator {
	protected:
		int16_t perm[256];
		int16_t permGradIndex3D[256];

		// TBH, i have no idea how these next 5 functions work. Looks good though.
		value_t extrapolate(int xsb, int ysb, value_t dx, value_t dy);
		value_t extrapolate(int xsb, int ysb, int zsb, value_t dx, value_t dy, value_t dz);
		value_t extrapolate(int xsb, int ysb, int zsb, int wsb, value_t dx, value_t dy, value_t dz, value_t dw);

		value_t genNoise2D(const glm::vec2& pos) final;
		value_t genNoise3D(const glm::vec3& pos) final;
	public:
		OpenSimplex(Rand::Seed seed, value_t min, value_t max);
		std::string getGPUNoiseString(int dims, std::vector<float> inputmul);
		static const std::string name;
		void setRange(value_t min, value_t max) { this->min = min; this->max = max; }
	};

	// TODO: seed
	class Perlin: public Generator {
	protected:
		value_t genNoise2D(const glm::vec2& pos) final { return (glm::perlin(pos) + 1.f) / 2.f; }
		value_t genNoise3D(const glm::vec3& pos) final { return (glm::perlin(pos) + 1.f) / 2.f; }
	public:
		Perlin(Rand::Seed seed, value_t min, value_t max): Generator(min, max) {}
		std::string getGPUNoiseString(int dims, std::vector<float> inputmul);
		static const std::string name;
		void setRange(value_t min, value_t max) { this->min = min; this->max = max; }
	};

	// cellular noise
	/*class Worley: public Generator {
	protected:
		int16_t perm[256];



		value_t genNoise2D(const glm::vec2& pos) final;
		value_t genNoise3D(const glm::vec3& pos) final;
	};*/

	// Returns val, always. Sorta useful, right?
	class EmptyNoise: public Generator {
	protected:
		// These are rather complicated noise functions...
		value_t genNoise2D(const glm::vec2& pos) { return 1; }
		value_t genNoise3D(const glm::vec3& pos) { return 1; }
	public:
		EmptyNoise(value_t val): Generator(val, val) {}
		static const std::string name;
	};

// How to define the noises of the future: by writing crappy macros

#define noiseDef \
	static const std::string name;

#define makeVecNoiseDefs(def) \
	value_t noise2D(const glm::vec2& pos) { def(pos,glm::vec2) } \
	value_t noise3D(const glm::vec3& pos) { def(pos,glm::vec3) } \
	value_t noise4D(const glm::vec4& pos) { def(pos,glm::vec4) }

	class DimensionalNoise: public Noise {
	private:
		int dim;
	public:

		#define DIMENSIONAL_NOISE_RUN_FUNCTION(x,T) return glm::clamp(value_t(x[dim]),min,max);

		makeVecNoiseDefs(DIMENSIONAL_NOISE_RUN_FUNCTION)

		noiseDef
		DimensionalNoise(value_t min, value_t max, int dim): Noise(min,max), dim(dim) {}
	};

	template<typename G> Generator* makeGen(Rand::Seed seed, Noise::value_t min, Noise::value_t max) { return new G(seed, min, max); }

	/* Fun noise. Takes multiple octaves of noise, with decreasing size and amplitude, to make more nuanced curves.
	 * Also goes by the name Fractal noise, because there's detail down to infinity. (in theory)
	 *
	 * This only takes types who inherit Generator, and define the constructor (seed_seq, value_t, value_t)
	 */
	class OctaveNoise: public CombinedNoise {
	private:
		float persistence;
		int num_octs;
	public:
		OctaveNoise(float largest_feature, value_t persistence, Rand::Seed seed, int max_octaves, value_t min = 0.f, value_t max = 1.f,
				std::function<Generator*(Rand::Seed, Noise::value_t, Noise::value_t)> make = makeGen<Simplex>);
		static const std::string name;
	};

	class RidgedNoise: public OutputModifier {
	protected:
		value_t doStuff(value_t in) { return (1.f - fabs(in)) * (max-min) + min; }
	public:
		RidgedNoise(Noise* noise):
			OutputModifier(noise, noise->min, noise->max) { this->noise->setRange(-1, 1); }
		static const std::string name;

		std::string getGPUNoiseString(int dims, std::vector<float> inputmul);
		void setRange(value_t min, value_t max) { this->min = min; this->max = max; }
	};

	template<int nNoises> struct MultiNoiseReturnType {
		MultiNoiseReturnType(Noise::value_t* vals) {
			for(int i = 0; i < nNoises; i++) (*this)[i] = vals[i];
		}
		MultiNoiseReturnType() {}

		Noise::value_t vals[nNoises];
		Noise::value_t& operator[](int index) { return vals[index]; }
	};

	/*
	 * MultiNoises write to multiple output dimensions.
	 * Note that this class makes no assumptions, and has no rules
	 * as to the ranges of the noises inside, since that should be handled by
	 * the contained noises themselves.
	 */
	template<int nNoises> class MultiNoise {
	public:
		typedef MultiNoiseReturnType<nNoises> value_t;

		virtual ~MultiNoise() {}
		virtual	value_t noise2D(const glm::vec2& pos) = 0;
		virtual value_t noise3D(const glm::vec3& pos) = 0;

		//virtual value_t range1D(const float pos, const int range, const float step);
		virtual value_t* range2D(const glm::vec2& pos, const glm::ivec2& range, const glm::vec2& step = {1, 1}) {
			value_t* ret = new value_t[range.x * range.y];
			for(int x = 0; x < range.x; x++)
				for(int y = 0; y < range.y; y++)
					ret[x + y * range.x] = noise2D(pos + glm::vec2(x, y) * step);
			return ret;
		}
		virtual value_t* range3D(const glm::vec3& pos, const glm::ivec3& range, const glm::vec3& step = {1, 1, 1}) {
			value_t* ret = new value_t[range.x * range.y * range.z];
			for(int x = 0; x < range.x; x++)
				for(int y = 0; y < range.y; y++)
					for(int z = 0; z < range.z; z++)
						ret[x + y * range.x + z * range.x * range.y] = noise3D(pos + glm::vec3(x, y, z) * step);
			return ret;
		}
		//virtual value_t range4D(const glm::vec4& pos, const glm::ivec4& range, const glm::vec4& step = {1, 1, 1, 1});
	};

	// In this MultiNoise, every output dimension is mapped to a single input noise. Sort of pointless, huh?
	template<int nNoises> class CopyNoise: public MultiNoise<nNoises> {
	private:
		NoisePtr noise;
	public:
		typedef MultiNoiseReturnType<nNoises> value_t;

		CopyNoise(NoisePtr noise): noise(noise) {}
		~CopyNoise() {}

		value_t noise2D(const glm::vec2& pos) {
			Noise::value_t val = noise->noise2D(pos);
			value_t ret;

			// copy val into all the spots in ret
			for(int i = 0; i < nNoises; i++)
				ret[i] = val;
			return ret;
		}

		value_t noise3D(const glm::vec3& pos) {
			Noise::value_t val = noise->noise3D(pos);
			value_t ret;

			// copy val into all the spots in ret
			for(int i = 0; i < nNoises; i++)
				ret[i] = val;
			return ret;
		}
	};

	// NoiseTuples use a unique noise for each output mapping.
	// TODO: better name
	template<int nNoises> class NoiseTuple: public MultiNoise<nNoises> {
	protected:
		NoisePtr noises[nNoises];
	public:
		typedef MultiNoiseReturnType<nNoises> value_t;

		NoiseTuple(std::vector<NoisePtr> noises) {
			for(int i = 0; i < nNoises; i++)
				this->noises[i] = noises[i];
		}
		~NoiseTuple() {}

		value_t noise2D(const glm::vec2& pos) {
			value_t ret;
			for(int i = 0; i < nNoises; i++)
				ret[i] = noises[i]->noise2D(pos);
			return ret;
		}

		value_t noise3D(const glm::vec3& pos) {
			value_t ret;
			for(int i = 0; i < nNoises; i++)
				ret[i] = noises[i]->noise3D(pos);
			return ret;
		}
	};

} // namespace noise


}
#endif /*SIMPLEX_HPP_*/
