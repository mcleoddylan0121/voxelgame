#include "noise.h"
#include "util.h"
#include "console.h"

#include <cmath>
#include <unordered_map>

namespace vox {

namespace noise {

	Noise::value_t Noise::noise1D(float pos) {
		printlnf(WARNING, "Noise1D for given noise is Undefined!");
		return min;
	}

	Noise::value_t Noise::noise2D(const glm::vec2& pos) {
		printlnf(WARNING, "Noise2D for given noise is Undefined!");
		return min;
	}

	Noise::value_t Noise::noise3D(const glm::vec3& pos) {
		printlnf(WARNING, "Noise3D for given noise is Undefined!");
		return min;
	}

	Noise::value_t Noise::noise4D(const glm::vec4& pos) {
		printlnf(WARNING, "Noise4D for given noise is Undefined!");
		return min;
	}

	Noise::value_t Noise::noise5D(const float* pos) {
		printlnf(WARNING, "Noise5D for given noise is Undefined!");
		return min;
	}

	Noise::value_t* Noise::range1D(const float pos, const int range, const float step) {
		value_t* ret = new value_t[range];
		for(int i = 0; i < range; i++)
			ret[i] = noise1D(pos + i * step);
		return ret;
	}

	Noise::value_t* Noise::range2D(const glm::vec2& pos, const glm::ivec2& range, const glm::vec2& step) {
		value_t* ret = new value_t[range.x * range.y];
		for(int x = 0; x < range.x; x++)
			for(int y = 0; y < range.y; y++)
				ret[x + y * range.x] = noise2D(pos + glm::vec2(x, y) * step);
		return ret;
	}

	Noise::value_t* Noise::range3D(const glm::vec3& pos, const glm::ivec3& range, const glm::vec3& step) {
		value_t* ret = new value_t[range.x * range.y * range.z];
		for(int x = 0; x < range.x; x++)
			for(int y = 0; y < range.y; y++)
				for(int z = 0; z < range.z; z++)
					ret[x + y * range.x + z * range.x * range.y] = noise3D(pos + glm::vec3(x, y, z) * step);
		return ret;
	}

	Noise::value_t* Noise::range4D(const glm::vec4& pos, const glm::ivec4& range, const glm::vec4& step) {
		value_t* ret = new value_t[range.x * range.y * range.z * range.w];
		for(int x = 0; x < range.x; x++)
			for(int y = 0; y < range.y; x++)
				for(int z = 0; z < range.z; y++)
					for(int w = 0; w < range.w; z++)
						ret[x + y * range.x + z * range.x * range.y + w * range.x * range.y * range.z] = noise4D(pos + glm::vec4(x, y, z, w) * step);
		return ret;
	}

	Noise::value_t* Noise::range5D(const float* pos, const int* range, const float* step) {
		value_t* ret = new value_t[range[0] * range[1] * range[2] * range[3] * range[4]];
		for(int x = 0; x < range[0]; x++)
			for(int y = 0; y < range[1]; x++)
				for(int z = 0; z < range[2]; y++)
					for(int w = 0; w < range[3]; z++)
						for(int r = 0; r < range[4]; r++) {
							float lPos[5] = {pos[0] + x, pos[1] + y, pos[2] + z, pos[3] + w, pos[4] + r};
							ret[x + range[0] * (y + range[1] * (z + range[2] * (w + range[3] * r)))] = noise5D(lPos);
						}
		return ret;
	}

	Noise::value_t OutputModifier::noise1D(float pos) {
		return doStuff(noise->noise1D(pos));
	}

	Noise::value_t OutputModifier::noise2D(const glm::vec2& pos) {
		return doStuff(noise->noise2D(pos));
	}

	Noise::value_t OutputModifier::noise3D(const glm::vec3& pos) {
		return doStuff(noise->noise3D(pos));
	}

	Noise::value_t OutputModifier::noise4D(const glm::vec4& pos) {
		return doStuff(noise->noise4D(pos));
	}

	Noise::value_t OutputModifier::noise5D(const float* pos) {
		return doStuff(noise->noise5D(pos));
	}

	Selector::Selector(NoisePtr a, NoisePtr b, NoisePtr _selector):
			Noise(fmin(a->min, b->min), fmax(a->max, b->max)), a(a), b(b), _selector(_selector) {}

	Noise::value_t Rotate2D::noise2D(const glm::vec2& pos) {
		float s = sin(angle);
		float c = cos(angle);
		return noise->noise2D({pos.x * c - pos.y * s, pos.x * s + pos.y * c});
	}

	Noise::value_t Rotate2D::noise3D(const glm::vec3& pos) {
		float s = sin(angle);
		float c = cos(angle);
		return noise->noise3D({pos.x * c - pos.z * s, pos.y, pos.x * s + pos.z * c});
	}

	// TODO: Maybe switch to rotation matrices?
	Noise::value_t Rotate3D::noise3D(const glm::vec3& pos) {
		float rad = glm::distance(pos, {0, 0, 0});
		float theta = pos.z / rad + angle;
		float theta2 = pos.y / rad + ang2;
		glm::vec3 nPos = {rad * sin(theta) * cos(theta2), rad * sin(theta) * cos(theta), rad * cos(theta)};
		return noise->noise3D(nPos);
	}

	BiSelector::BiSelector(NoisePtr noise, value_t boundary, value_t min, value_t max): OutputModifier(noise, min, max), boundary(boundary) {}

	Noise::value_t Selector::noise2D(const glm::vec2& pos) {
		value_t select = _selector->noise2D(pos);
		select = constrain(select, 0, 1);
		return (1 - select) * a->noise2D(pos) + select * b->noise2D(pos);
	}

	Noise::value_t Selector::noise3D(const glm::vec3& pos) {
		value_t select = _selector->noise3D(pos);
		select = constrain(select, 0, 1);
		return (1 - select) * a->noise3D(pos) + select * b->noise3D(pos);
	}

	Noise::value_t Constrainer::doStuff(value_t input) {
		return constrain(input, min, max);
	}

	void CombinedNoise::operator +=(NoisePtr nNoise) {
		noises.push_back(nNoise);
		this->min += nNoise->min;
		this->max += nNoise->max;
	}

	CombinedNoise::~CombinedNoise() {}

	CombinedNoise::CombinedNoise(const std::vector<NoisePtr>& noises): Noise(0,0) {
		for(NoisePtr n: noises)
			(*this) += n;
	}

	Noise::value_t Multiplier::noise2D(const glm::vec2& pos) {
		return (a->noise2D(pos) * b->noise2D(pos));
	}
	Noise::value_t Multiplier::noise3D(const glm::vec3& pos) {
		return (a->noise3D(pos) * b->noise3D(pos));
	}

	Multiplier::Multiplier(NoisePtr a, NoisePtr b): Noise(fmin(fmin(a->min*b->min,a->max*b->max),fmin(a->min*b->max, b->min*a->max)), fmax(fmax(a->min*b->min,a->max*b->max),fmax(a->min*b->max, b->min*a->max))), a(a), b(b) {}

	OctaveNoise::OctaveNoise(float largest_feature, value_t persistence, Rand::Seed seed, int max_octaves, value_t min, value_t max, std::function<Generator*(Rand::Seed, Noise::value_t, Noise::value_t)> make): CombinedNoise() {
		this->persistence = persistence;
		this->num_octs = fmin(ceil(log2(largest_feature)), max_octaves);

		float cMax = 0;
		Rand rand = Rand(seed); // Create an RNG, to make the seeds for the simplices
		for(int i = 0; i < num_octs; i++) {
			value_t amp = pow(persistence, i);
			cMax += amp;
		}
		for (int i = 0; i < num_octs; i++) {
			// Each successive octave has twice the scale, and persistence times the amplitude
			float scale = largest_feature / pow(2, i-1);
			value_t amplitude = (pow(persistence, i) / cMax) * (max - min);

			(*this) += (NoisePtr(new Scalar(NoisePtr(make(rand.getSeedSeq(10), 0.f, amplitude)), scale)));
		}
		if(min != 0.f) {
			(*this) += NoisePtr(new EmptyNoise(min));
		}
	}

	Noise::value_t CombinedNoise::noise2D(const glm::vec2& pos) {
		value_t total = 0;
		for(NoisePtr n: noises)
			total += n->noise2D(pos);
		return total;
	}

	Noise::value_t CombinedNoise::noise3D(const glm::vec3& pos) {
		value_t total = 0;
		for(NoisePtr n: noises)
			total += n->noise3D(pos);
		return total;
	}

	// Used for seeding simplex noise
	const int SWAPS = 750;
	const uint8_t p_supply[256] = { 151, 160, 137, 91, 90, 15, 131, 13, 201, 95, 96, 53, 194, 233, 7,
			225, 140, 36, 103, 30, 69, 142, 8, 99, 37, 240, 21, 10, 23, 190, 6, 148, 247, 120, 234, 75,
			0, 26, 197, 62, 94, 252, 219, 203, 117, 35, 11, 32, 57, 177, 33, 88, 237, 149, 56, 87, 174,
			20, 125, 136, 171, 168, 68, 175, 74, 165, 71, 134, 139, 48, 27, 166, 77, 146, 158, 231, 83,
			111, 229, 122, 60, 211, 133, 230, 220, 105, 92, 41, 55, 46, 245, 40, 244, 102, 143, 54, 65,
			25, 63, 161, 1, 216, 80, 73, 209, 76, 132, 187, 208, 89, 18, 169, 200, 196, 135, 130, 116,
			188, 159, 86, 164, 100, 109, 198, 173, 186, 3, 64, 52, 217, 226, 250, 124, 123, 5, 202, 38,
			147, 118, 126, 255, 82, 85, 212, 207, 206, 59, 227, 47, 16, 58, 17, 182, 189, 28, 42, 223,
			183, 170, 213, 119, 248, 152, 2, 44, 154, 163, 70, 221, 153, 101, 155, 167, 43, 172, 9, 129,
			22, 39, 253, 19, 98, 108, 110, 79, 113, 224, 232, 178, 185, 112, 104, 218, 246, 97, 228,
			251, 34, 242, 193, 238, 210, 144, 12, 191, 179, 162, 241, 81, 51, 145, 235, 249, 14, 239,
			107, 49, 192, 214, 31, 181, 199, 106, 157, 184, 84, 204, 176, 115, 121, 50, 45, 127, 4, 150,
			254, 138, 236, 205, 93, 222, 114, 67, 29, 24, 72, 243, 141, 128, 195, 78, 66, 215, 61, 156,
			180 };

	Simplex::Simplex(Rand::Seed seed, value_t min, value_t max): Generator(min, max) {
		for (int i = 0; i < 256; i++)
			p[i] = p_supply[i];
		Rand rand = Rand(seed);

		// swap values in the permutation list, acting as a seed
		for (int i = 0; i < SWAPS; i++) {
			uint8_t swapFrom = rand();
			uint8_t swapTo = rand();

			uint8_t temp = p[swapFrom];
			p[swapFrom] = p[swapTo];
			p[swapTo] = temp;
		}

		// use the permutation table to create the ones used in the noise function
		for (int i = 0; i < 512; i++) {
			perm[i] = p[i & 255];
			permMod12[i] = (uint8_t) (perm[i] % 12);
		}
	}

	OpenSimplex::OpenSimplex(Rand::Seed seed, value_t min, value_t max): Generator(min, max) {
		short source[256];
		for (short i = 0; i < 256; i++)
			source[i] = i;
		Rand rand = Rand(seed);

		// swap the permutation list, to seed the generator
		for (int i = 255; i >= 0; i--) {
			uint_fast32_t swap = rand();
			int r = (int)((swap + 31) % (i + 1));
			if (r < 0)
				r += (i + 1);
			perm[i] = source[r];
			permGradIndex3D[i] = (short)((perm[i] % (256 / 3)) * 3);
			source[r] = source[i];
		}
	}

} // namespace noise

}

