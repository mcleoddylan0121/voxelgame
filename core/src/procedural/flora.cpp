/*
 * flora.cpp
 *
 *  Created on: Apr 20, 2015
 *      Author: Dani
 */

#include "flora.h"
#include "util.h"
#include "geometry.h"
#include "voxelmesh.h"
#include "sdlcontroller.h"

#include <vector>
#include <unordered_map>

namespace vox {

	struct TreeNode {
		glm::vec3 position;
		std::vector<glm::vec3> attractions;
		bool last = true;

		TreeNode(glm::vec3 pos) : position(pos) {}
	};

	Tree * genTree(WorldCoordinate pos) {
		Rand rng;

		// Constants. Something tells me we should have a FloraConfig class that manages these
		float sizeUnit = 20.f;
		LocalCoordinate rad = LocalCoordinate(sizeUnit, 1.5f * sizeUnit, sizeUnit);
		LocalCoordinate size = rad*2;
		float iterationDistance = 2;
		float radiusOfKill = size.y / rng.getInt(10.f, 20.f);
		float radiusOfInfluence = iterationDistance * 9001;
		int leafRadius = 4;
		int maxIterations = 50;

		const int amtVoxels = 5;
		Palette * voxelMap = new Palette(amtVoxels);
		Voxel lotsaVoxels[amtVoxels] = {
		/* 0. Lifeless Air	*/ Voxel({0, 0, 0, 0}, Voxel::TRANSPARENT | Voxel::IMAGINARY),
		/* 1. Da Trunk		*/ Voxel(Color::u8Color(83, 53, 10), Voxel::SOLID),
		/* 2. Leafy Green	*/ Voxel(Color::u8Color(58, 95, 11), Voxel::SOLID),
		};
		voxelMap->setMultiple(lotsaVoxels, amtVoxels);
		EntityMesh * ret = new EntityMesh(size, voxelMap);

		std::vector<glm::vec3> attractions;

		int nAttractions = rng.getInt(500, 1000);

		for (int i = 0; i < nAttractions; i++) {
			WorldCoordinate r = getPointInSphere(WorldCoordinate(0), 0, sizeUnit * 0.75f, rng);
            attractions.push_back(WorldCoordinate(rad) + WorldCoordinate(r.x, r.y * 0.8f, r.z));
		}
		std::vector<TreeNode> nodes;
		nodes.push_back({{ret->size.x / 2.f, 0, ret->size.z / 2.f}});

		bool moreIterationsPossible = true;
		char failsafe = 0, failI = 0;

		while (moreIterationsPossible && maxIterations --> 0 && failI < 100) {
			if (failsafe == 1) failI++;
			else failI = 0;
			failsafe = 0;

			// Clear all the nodes' attractions before starting to fill them again
			for (unsigned int j = 0; j < nodes.size(); j++)
				nodes[j].attractions.clear();

			// Fill the nodes with their closest attractions
			moreIterationsPossible = false; // If no attractions are close enough, there are no more possible iterations
			for (unsigned int i = 0; i < attractions.size(); i++) {
				// Determine the closest node for each attraction
				float lowestDistance = 100000;
				int nodeIndex = -1;
				for (unsigned int j = 0; j < nodes.size(); j++) {
					float dist = glm::distance(attractions[i], nodes[j].position);
					if (dist < lowestDistance) {
						nodeIndex = j;
						lowestDistance = dist;
					}
				}
				// If it's in the sphere of influence, add it to the closest node's radar
				if (lowestDistance <= radiusOfInfluence && nodeIndex != -1) {
					nodes[nodeIndex].attractions.push_back(attractions[i]);
					moreIterationsPossible = true;
					failsafe |= 1;
				}
			}


			// Grow the tree, note that we can add to the vector during the loop because of push_back
			unsigned int numNodes = nodes.size();
			for (unsigned int j = 0; j < numNodes; j++) {
				if (!nodes[j].attractions.empty()) {
					// Compute the average distance from the node to each of its attractions
					glm::vec3 avgDistance;
					for (unsigned int a = 0; a < nodes[j].attractions.size(); a++)
						avgDistance += ((nodes[j].attractions[a] - nodes[j].position) / glm::length(nodes[j].attractions[a] - nodes[j].position));
					glm::vec3 direction = avgDistance / glm::length(avgDistance);
					nodes.push_back({nodes[j].position + (direction * iterationDistance)});
					nodes[j].last = false;
				}
			}

			// Delete nodes that need to be deleted (within the radiusOfKill)
			for (unsigned int i = 0; i < attractions.size(); i++) {
				// Determine the closest node for each attraction
				float lowestDistance = 100000;
				int nodeIndex = -1;
				for (unsigned int j = 0; j < nodes.size(); j++) {
					float dist = glm::distance(attractions[i], nodes[j].position);
					if (dist < lowestDistance) {
						nodeIndex = j;
						lowestDistance = dist;
					}
				}
				// If it's in the sphere of influence, add it to the closest node's radar
				if (lowestDistance <= radiusOfKill && nodeIndex != -1) {
					// Make leaves
					int lr = leafRadius * rng.getFloat(0.25, 2.0);
					for(int x = -lr; x <= lr; x++) for(int y = -lr; y<= lr; y++) for(int z = -lr; z<= lr; z++) {
						glm::vec3 rPos = glm::vec3(x,y,z);
						glm::vec3 pos = attractions[i] + rPos;
						if(squareLength(rPos) <= lr)
							ret->set(2, LocalCoordinate(constrain(pos.x, 0, size.x-1), constrain(pos.y, 0, size.y-1), constrain(pos.z, 0, size.z-1)));
					}

					// Erase the current attraction then de-increment the iterator because all the elements will be pushed down
					attractions.erase(attractions.begin() + i--);
					failsafe |= 2;
				}
			}
		}

		for (unsigned int j = 0; j < nodes.size(); j++) {
			float trunkRadius = 2 * (1 - nodes[j].position.y / size.y);
			trunkRadius *= trunkRadius;
			int lr = trunkRadius;
			for(int x = -lr; x <= lr; x++) for(int y = -lr; y<= lr; y++) for(int z = -lr; z<= lr; z++) {
				glm::vec3 rpos = glm::vec3(x, y, z);
				if (squareLength(rpos) > trunkRadius * trunkRadius) continue;
				glm::vec3 pos = nodes[j].position + rpos;
				ret->set(1, LocalCoordinate(constrain(pos.x, 0, size.x-1), constrain(pos.y, 0, size.y-1), constrain(pos.z, 0, size.z-1)));
			}
		}

		long ticks = mSDL->getTime<milliseconds>();
		ret->generateMesh();
		ticks = mSDL->getTime<milliseconds>() - ticks;
		printlnf(TEMPORARY, "Time it took to generate the tree (ms): %d", ticks);

		Tree * f = new Tree();
		f->buf = ret->buf;
		f->pos = pos;
		f->voxelSize = WorldCoordinate(1/4.f);
		f->size = (WorldCoordinate)size * f->voxelSize;
		delete ret;

		return f;
	}

	void Tree::sendMeshToGPU() {
		buf->sendToGPU();
	}

	Tree::~Tree() {
		delete buf;
	}
}
