/*
 * thread.h
 *
 *  Created on: Jul 26, 2016
 *      Author: Daniel
 */

#ifndef CORE_SRC_MULTITHREADING_THREAD_H_
#define CORE_SRC_MULTITHREADING_THREAD_H_

#include "system.h"

#include <vector>
#include <string>
#include <atomic>

#ifdef __MINGW32__
#include <mingw.thread.h>
#undef ERROR
#else
#include <thread>
#endif

namespace vox {

	struct Thread {
		virtual ~Thread();

		void start();
		void join();

		bool isRunning();

		// Once a thread finishes executing a task, it sets isWriteSafe to true and both sync and tick to false.
		// The main thread can then update the execList for this Thread and issue a command by setting either tick
		// or sync to true and then setting isWriteSafe to false.
		bool tick = false;
		bool sync = false;
		std::vector<System_t *> * execList;

		// May be called by multiple threads at once.
		std::atomic<bool> isWriteSafe;

		// What the thread runs.
		static int _threadLoop(Thread * t);

	private:
		std::thread * _thread;
		std::atomic<bool> _running;
	};
}



#endif /* CORE_SRC_MULTITHREADING_THREAD_H_ */
