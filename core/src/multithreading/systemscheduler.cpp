/*
 * threadmanager.cpp
 *
 *  Created on: Jul 25, 2016
 *      Author: Daniel
 */

#include "systemscheduler.h"
#include "thread.h"

namespace vox {

	SystemScheduler * mScheduler;


	SystemScheduler::SystemScheduler() {
		_threadPool = new ThreadPool(std::thread::hardware_concurrency() > 0 ? std::thread::hardware_concurrency() : 1);
		printlnf(DEBUG, "Created thread pool with %i threads.", _threadPool->nThreads());
	}

	SystemScheduler::~SystemScheduler() {
		delete _threadPool;
		// Do we delete systems here?
	}

	void SystemScheduler::start() {
		for (;;) {
			if (_newNThreads > 0) _setNThreads();
			if (_rebuild) _rebuildSchedule();

			while (!_threadPool->threadsReady()) { std::this_thread::yield(); }

			_threadPool->executeTick();

			while (!_threadPool->threadsReady()) { std::this_thread::yield(); }

			_threadPool->executeSync();
		}
	}


	void SystemScheduler::setRebuildFlag() {
		_rebuild = true;
	}

	void SystemScheduler::changeNThreads(int newNThreads) {
		_newNThreads = newNThreads;
	}

	void SystemScheduler::_rebuildSchedule() {
		int threadIdx = 0;

		_threadPool->clearSchedule();

		printlnf(DEBUG, "Rebuilding schedule with %i systems.", _systems.size());

		// Distribute system evenly between threads for now (no optimization).
		for (System_t * s : _systems) {
			if (!s->isSleeping()) {
				_threadPool->schedules[threadIdx++]->push_back(s);
				threadIdx %= _threadPool->nThreads();
			}
		}

		_rebuild = false;
	}

	void SystemScheduler::_setNThreads() {
		delete _threadPool;
		_threadPool = new ThreadPool(_newNThreads);
		setRebuildFlag();
		_newNThreads = 0;
	}

	void SystemScheduler::registerSystem(System_t * s) {
		_systems.push_back(s);
		setRebuildFlag();
	}




	ThreadPool::ThreadPool(int nThreads) {
		for (int i = 0; i < nThreads; i++) {
			pool.push_back(new Thread());
			schedules.push_back(new std::vector<System_t*>());
		}

		for (Thread * t : pool)
			t->start();
	}

	ThreadPool::~ThreadPool() {
		for (Thread * t : pool)
			delete t;
		for (std::vector<System_t*> * v : schedules)
			delete v;
	}

	void ThreadPool::clearSchedule() {
		for (std::vector<System_t *> * s : schedules)
			s->clear();
	}

	void ThreadPool::executeTick() {
		for (int i = 0; i < nThreads(); i++) {
			if (schedules[i]->size() > 0) {
				pool[i]->tick = true;
				pool[i]->execList = schedules[i];
				pool[i]->isWriteSafe = false;
			}
		}
	}

	void ThreadPool::executeSync() {
		for (int i = 0; i < nThreads(); i++) {
			if (schedules[i]->size() > 0) {
				pool[i]->sync = true;
				pool[i]->execList = schedules[i];
				pool[i]->isWriteSafe = false;
			}
		}
	}

	bool ThreadPool::threadsReady() {
		for (Thread * t : pool)
			if (!t->isWriteSafe) return false;
		return true;
	}

	int ThreadPool::nThreads() {
		return pool.size();
	}

}
