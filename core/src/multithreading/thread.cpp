/*
 * thread.cpp
 *
 *  Created on: Jul 26, 2016
 *      Author: Daniel
 */

#include "thread.h"
#include "sdlcontroller.h"

namespace vox {

	Thread::~Thread() {
		if (isRunning())
			join();
		delete _thread;
	}

	void Thread::start() {
		if (isRunning()) {
			join();
			delete _thread;
		}

		_thread = new std::thread(Thread::_threadLoop, this);
		_running = true;
	}

	void Thread::join() {
		if (isRunning()) {
			// Set running to false first so the thread knows to stop running.
			_running = false;
			_thread->join();
		} else println(ERROR, "Could not join thread: Thread isn't running.");
	}


	bool Thread::isRunning() {
		return _running;
	}


	// Where the magic happens
	int Thread::_threadLoop(Thread * t) {

		t->isWriteSafe = true;

		while(t->isRunning())
			if (!t->isWriteSafe) {
				for (System_t * s : *t->execList)
					if (t->tick) s->_tick(mSDL->getTime<milliseconds>() / 1000.f);
					else s->_synchronize();

				t->sync = t->tick = false;
				t->isWriteSafe = true;
			} else std::this_thread::yield();

		return 0;
	}
}
