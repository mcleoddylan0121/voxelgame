/*
 * threadmanager.h
 *
 *  Created on: Jul 25, 2016
 *      Author: Daniel
 */

#ifndef CORE_SRC_MULTITHREADING_SYSTEMSCHEDULER_H_
#define CORE_SRC_MULTITHREADING_SYSTEMSCHEDULER_H_

#include "console.h"

#include <atomic>
#include <vector>

namespace vox {

	struct Thread;
	struct System_t;
	struct ThreadPool;


	struct SystemScheduler {

		SystemScheduler();
		virtual ~SystemScheduler();

		// Starts the main loop
		void start();

		// Sets a flag to rebuild the schedule before the next tick/sync cycle.
		void setRebuildFlag();

		// Sets a flag to change the number of threads before the next tick/sync cycle.
		void changeNThreads(int newNThreads);

		// System will begin work next cycle.
		void registerSystem(System_t * s);

	private:
		// Make sure to call this when it's safe to write to threads. (Call setRebuildFlag instead).
		void _rebuildSchedule();

		// This one too (it deletes everything).
		void _setNThreads();


		// May be called by multiple threads at once.
		std::atomic<bool> _rebuild;		// True if the flag is one, false if not
		std::atomic<int> _newNThreads;	// The number of new threads or 0 if the flag is off

		ThreadPool * _threadPool;
		std::vector<System_t*> _systems;
	};

	extern SystemScheduler * mScheduler;


	struct ThreadPool {
		std::vector<Thread*> pool;
		std::vector<std::vector<System_t*>*> schedules;

		// Note the constructor starts the threads.
		ThreadPool(int nThreads);
		virtual ~ThreadPool();

		void clearSchedule();

		void executeTick();
		void executeSync();

		bool threadsReady();

		int nThreads();
	};

}

#endif /* CORE_SRC_MULTITHREADING_SYSTEMSCHEDULER_H_ */
