/*
 * system.h
 *
 *  Created on: Jul 26, 2016
 *      Author: Daniel
 */

#ifndef CORE_SRC_MULTITHREADING_SYSTEM_H_
#define CORE_SRC_MULTITHREADING_SYSTEM_H_

#include "console.h"

#include <atomic>

namespace vox {

	struct Thread;

	template <typename Data>
		struct SystemDataController {
			SystemDataController() {
				_data = new Data();
			}

			virtual ~SystemDataController() {
				delete _data;
			}

			Data& operator*() {
				return *(this->operator->());
			}

			Data* operator->() {
#ifdef THREAD_DEBUG
				if (_syncing) println(DEBUG, "System data access during synchronization.");
#endif
				return _data;
			}

		private:
#ifdef THREAD_DEBUG
			bool _syncing = false;
#endif
			Data * _data;
		};


	// Changes how the scheduler handles the system's tick.
	enum TickMode {
		Async,	// Multiple ticks per cycle. Must be thread-safe, because ticks can be called concurrently.
		Repeat, // Multiple ticks per cycle but in a sequential order. Doesn't have to be thread-safe.
		Single  // Only one tick per cycle.
	};



	struct System_t {
		friend struct Thread;

		virtual ~System_t() {}

		// Delta is the time between this system's last call and this one.
		// Different systems will have different values for delta.
		virtual void tick(float delta) = 0;

		// This function is the only time a system may make modifications to its global data.
		// Note: ticking should not rely on synchronization (it should be possible to tick multiple times for each sync).
		virtual void synchronize() = 0;

		// Implemented by System: this returns what you put in the second template argument.
		virtual TickMode getTickMode() = 0;

		// Sleep when there's nothing left to do (for subsystems). Sleeping systems don't tick / synchronize.
		// Changes to the sleeping state of a system takes effect until the next cycle so this is thread-safe.
		void sleep();

		// Wakes a system from sleep. It'll start its work on the next tick.
		void wake();

		// If this returns true, walk on your tip-toes.
		bool isSleeping();

#ifdef THREAD_DEBUG
		virtual void setSync(bool on) = 0;
#endif


	private:
		// Dat backend tho
		void _tick(float curTime);
		void _synchronize();

		std::atomic<bool> _sleeping;

		float _lastTime = -1;


		// Profiling of the average time it took for the last x function calls (x = inf for now)
		float _avgTickTime = 0, _avgSyncTime = 0;
		long _nTicks = 0, _nSyncs = 0;
	};


	// Inherit this one, not System_t
	template <typename Data, TickMode tickMode>
	struct System : public System_t {
#ifdef THREAD_DEBUG
		void setSync(bool on) { data._syncing = on; }
#endif

		// You should only be able to modify this data on its own system's synchronize call.
		const SystemDataController<Data> getData() { return data; }

		TickMode getTickMode() { return tickMode; }

		SystemDataController<Data> data;
	};
}

#endif /* CORE_SRC_MULTITHREADING_SYSTEM_H_ */
