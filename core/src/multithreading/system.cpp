/*
 * system.cpp
 *
 *  Created on: Jul 26, 2016
 *      Author: Daniel
 */

#include "system.h"
#include "sdlcontroller.h"
#include "systemscheduler.h"

namespace vox {

	void System_t::_tick(float curTime) {
		auto tp = mSDL->getTimePoint();

		float delta = 0;
		if (_lastTime > 0)
			delta = curTime - _lastTime;
		_lastTime = curTime;

		tick(delta);

		// Putting this ++ in the statement following may produce undefined behavior (source: StackOverflow).
		++_nTicks;
		_avgTickTime = (_avgTickTime * _nTicks + mSDL->getTime<milliseconds>(tp) / 1000.f) / float(_nTicks);
	}

	void System_t::_synchronize() {
		auto tp = mSDL->getTimePoint();

		synchronize();

		// Putting this ++ in the statement following may produce undefined behavior (source: StackOverflow).
		++_nSyncs;
		_avgSyncTime = (_avgSyncTime * _nSyncs + mSDL->getTime<milliseconds>(tp) / 1000.f) / float(_nSyncs);
	}

	void System_t::sleep() {
		_sleeping = true;
		mScheduler->setRebuildFlag();
	}

	void System_t::wake() {
		_sleeping = false;
		mScheduler->setRebuildFlag();
	}

	bool System_t::isSleeping() {
		return _sleeping;
	}

}
