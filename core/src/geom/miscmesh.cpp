/*
 * miscmesh.cpp
 *
 *  Created on: Jul 27, 2015
 *      Author: Dylan
 */

#include "miscmesh.h"
#include "renderutils.h"

namespace vox {

	SpatialMeshBuffer* genSphere(int iterations, bool invert) {
		std::vector<glm::vec3> i1;
		std::vector<Triangle<int>> s1;

		// create 12 vertices of a icosahedron
		float t = (1.0 + sqrt(5.0)) / 2.0;

		i1.push_back(glm::vec3(-1,  t,  0));
		i1.push_back(glm::vec3( 1,  t,  0));
		i1.push_back(glm::vec3(-1, -t,  0));
		i1.push_back(glm::vec3( 1, -t,  0));

		i1.push_back(glm::vec3( 0, -1,  t));
		i1.push_back(glm::vec3( 0,  1,  t));
		i1.push_back(glm::vec3( 0, -1, -t));
		i1.push_back(glm::vec3( 0,  1, -t));

		i1.push_back(glm::vec3( t,  0, -1));
		i1.push_back(glm::vec3( t,  0,  1));
		i1.push_back(glm::vec3(-t,  0, -1));
		i1.push_back(glm::vec3(-t,  0,  1));


		// 5 faces around point 0
		s1.push_back(Triangle<int>(0, 11, 5));
		s1.push_back(Triangle<int>(0, 5, 1));
		s1.push_back(Triangle<int>(0, 1, 7));
		s1.push_back(Triangle<int>(0, 7, 10));
		s1.push_back(Triangle<int>(0, 10, 11));

		// 5 adjacent faces
		s1.push_back(Triangle<int>(1, 5, 9));
		s1.push_back(Triangle<int>(5, 11, 4));
		s1.push_back(Triangle<int>(11, 10, 2));
		s1.push_back(Triangle<int>(10, 7, 6));
		s1.push_back(Triangle<int>(7, 1, 8));

		// 5 faces around point 3
		s1.push_back(Triangle<int>(3, 9, 4));
		s1.push_back(Triangle<int>(3, 4, 2));
		s1.push_back(Triangle<int>(3, 2, 6));
		s1.push_back(Triangle<int>(3, 6, 8));
		s1.push_back(Triangle<int>(3, 8, 9));

		// 5 adjacent faces
		s1.push_back(Triangle<int>(4, 9, 5));
		s1.push_back(Triangle<int>(2, 4, 11));
		s1.push_back(Triangle<int>(6, 2, 10));
		s1.push_back(Triangle<int>(8, 6, 7));
		s1.push_back(Triangle<int>(9, 8, 1));

		// refine triangles
		for (int i = 0; i < iterations; i++) {
		std::vector<Triangle<int>> s2;
			for(auto tri: s1) {
			  // replace triangle by 4 triangles
			  glm::vec3 a = (i1[tri[0]] + i1[tri[1]])/2.f;
			  glm::vec3 b = (i1[tri[1]] + i1[tri[2]])/2.f;
			  glm::vec3 c = (i1[tri[2]] + i1[tri[0]])/2.f;
			  i1.push_back(a); i1.push_back(b); i1.push_back(c);
			  unsigned int inds[3] = {i1.size() - 3, i1.size() - 2, i1.size() - 1};

			  s2.push_back(Triangle<int>(tri[0], inds[0], inds[2]));
			  s2.push_back(Triangle<int>(tri[1], inds[1], inds[0]));
			  s2.push_back(Triangle<int>(tri[2], inds[2], inds[1]));
			  s2.push_back(Triangle<int>(inds[0], inds[1], inds[2]));
			}
			s1 = s2;
		}

		for(unsigned int i = 0; i < i1.size(); i++) i1[i] = glm::normalize(i1[i]);
		if(invert)
			for(unsigned int i = 0; i < s1.size(); i++) {
				s1[i] = Triangle<int>(s1[i][2], s1[i][1], s1[i][0]);
			}

		ArrayBuffer<Triangle<int>>* inds = new ArrayBuffer<Triangle<int>>(ELEMENT_ARRAY, STATIC);
		inds->resizeArray(s1.size());
		for(unsigned int i = 0; i < s1.size(); i++) inds->set(i, s1[i]);
		inds->generateBuffer();

		ArrayBuffer<glm::vec3>* verts = new ArrayBuffer<glm::vec3>(ARRAY, STATIC);
		verts->resizeArray(i1.size());
		for(unsigned int i = 0; i < i1.size(); i++) verts->set(i, i1[i]);
		verts->generateBuffer();

		return new SpatialMeshBuffer(verts, inds);
	}

	SpatialMeshBuffer* genDome(int iterations, bool invert, float cutoffHeight) {
		SpatialMeshBuffer* ret = genSphere(iterations, invert);
		ArrayBuffer<Triangle<int>>* inds = (ArrayBuffer<Triangle<int>>*)ret->inds;
		ArrayBuffer<glm::vec3>* verts = (ArrayBuffer<glm::vec3>*)ret->verts;
		const glm::vec3* vs = verts->getData();
		const Triangle<int>* is = inds->getData();

		std::vector<Triangle<int>> nInds;

		for(unsigned int i = 0; i < inds->size; i++) {
			glm::vec3 tris[3] = {vs[is[i][0]], vs[is[i][1]], vs[is[i][2]]};
			if(tris[0].y >= cutoffHeight || tris[1].y >= cutoffHeight || tris[2].y >= cutoffHeight)
				nInds.push_back(inds->getData()[i]);
		}

		inds->resizeArray(nInds.size());
		for(unsigned int i = 0; i < nInds.size(); i++) {
			inds->set(i, nInds[i]);
		}
		inds->streamData();
		return ret;
	}


	TexturedMeshBuffer* TexturedMeshBuffer::createBillboard(Texture* tex, glm::vec2 UVstart, glm::vec2 UVsize) {
		if(UVsize == glm::vec2(-1,-1))
			UVsize = glm::vec2(1,1);
		else
			UVsize = UVsize / glm::vec2(tex->size);

		UVstart = UVstart / glm::vec2(tex->size);

		StackBuffer<TexturedMeshVertex>* verts = new StackBuffer<TexturedMeshVertex>(ARRAY, STATIC);
		float h = 1.f/2.f;
		verts->push({
			{{-h,-h,0},UVstart}, {{h,-h,0},UVstart+glm::vec2(UVsize.x,0)},{{h,h,0}, UVstart+UVsize}, {{-h,h,0}, {UVstart+glm::vec2(0,UVsize.y)}}
		});

		verts->generateBuffer();

		StackBuffer<Triangle<int>>* inds = new StackBuffer<Triangle<int>>(ELEMENT_ARRAY, STATIC);
		inds->push({{0,1,2},{2,3,0}});
		inds->generateBuffer();
		return new TexturedMeshBuffer(verts, inds);
	}


}



