/*
 * coords.h
 *
 *  Created on: Jun 10, 2015
 *      Author: Dani
 */

#ifndef COORDS_H_
#define COORDS_H_

#include "vglm.h"

namespace vox {

	typedef glm::ivec3 LocalCoordinate; // Modelspace Coordinates
	typedef glm::u8vec3 gpuLocalCoordinate; // Modelspace Coordinates that we send to the GPU

	typedef glm::vec3 WorldCoordinate;	 // Worldspace Coordinates
	typedef glm::ivec3 ChunkCoordinate;
	typedef glm::ivec3 VoxelCoordinate;  // WorldSpace Coordinates snapped to the voxel grid

	ChunkCoordinate worldToChunkCoordinate(const WorldCoordinate& c); // Returns the position of the chunk c is in.
	ChunkCoordinate voxelToChunkCoordinate(const VoxelCoordinate& c);
	VoxelCoordinate worldToVoxelCoordinate(const WorldCoordinate& c);
	WorldCoordinate voxelToWorldCoordinate(const VoxelCoordinate& c);
	WorldCoordinate chunkToWorldCoordinate(const ChunkCoordinate& c); // Returns the chunk's position in world space.
	VoxelCoordinate chunkToVoxelCoordinate(const ChunkCoordinate& c);
	LocalCoordinate worldToLocalChunkCoordinate(const WorldCoordinate& c);
	LocalCoordinate voxelToLocalChunkCoordinate(const VoxelCoordinate& c);

	struct ivec3sort { bool operator () (const glm::ivec3& a, const glm::ivec3& b) const; };
}



#endif /* COORDS_H_ */
