/*
 * shapes.cpp
 *
 *  Created on: Jun 26, 2015
 *      Author: Daniel
 */

#include "shapes.h"
#include "geometry.h"

#include <algorithm>
#include <limits>

namespace vox {
	Volume::Volume() : Volume(glm::vec3(0), glm::vec3(0)) {}
	Volume::Volume(WorldCoordinate pos, WorldCoordinate size) : Volume(pos, size, glm::quat_cast(glm::mat3x3())) {}
	Volume::Volume(WorldCoordinate pos, WorldCoordinate size, glm::quat rot) : Volume(pos, size, rot, glm::vec3(1/2.f)) {}
	Volume::Volume(WorldCoordinate pos, WorldCoordinate size, glm::quat rot, glm::vec3 rotAnchor) :
			pos(pos), size(size), rot(rot), rotAnchor(rotAnchor) {}


	WorldCoordinate Volume::getBottomCorner() const {
		return pos - (size / 2.f);
	}

	WorldCoordinate Volume::getBottomCenter() const {
		return WorldCoordinate(pos.x, pos.y - size.y / 2.f, pos.z);
	}

	WorldCoordinate Volume::getTopCorner() const {
		return pos + (size / 2.f);
	}

	WorldCoordinate Volume::getTopQuartile() const {
		return pos + glm::vec3(0, size.y * 0.25f, 0);
	}

	Volume Volume::getTranslation(WorldCoordinate trans) const {
		return {pos + trans, size};
	}

	Volume Volume::getBottomCornerChange(WorldCoordinate newCorner) const {
		return {newCorner + (size / 2.f), size};
	}

	Volume Volume::getComponentChange(int index, float n) const {
		WorldCoordinate newPos = pos;
		newPos[index] = n;
		return {newPos, size};
	}

	std::vector<glm::vec3> ds = {
			{ 1, 1, 1},
			{-1, 1, 1},
			{-1, 1,-1},
			{ 1, 1,-1}
	};

	std::pair<WorldCoordinate, WorldCoordinate> getAABB(const Volume v) {
		glm::vec3 newd;
		for (glm::vec3& n : ds)
			newd = glm::max(newd, glm::abs(v.rot * ((v.size / 2.f) * n)));

		return std::make_pair(v.pos - newd, v.pos + newd);
	}

	Volume Volume::gridAlligned() const {
		Volume ret = {this->pos, this->size, this->rot};
		WorldCoordinate p_1, p_2;
		if (rot.w > .9999f) {
			p_1 = ret.getBottomCorner();
			p_2 = ret.getTopCorner();
		} else {
			auto a = getAABB(*this);
			p_1 = a.first;
			p_2 = a.second;
		}
		p_1 = glm::floor(p_1);
		p_2 = glm::ceil(p_2);
		ret.size = p_2 - p_1;
		ret.pos = p_1 + ret.size / 2.f;
		ret.rot = glm::quat_cast(glm::mat3x3());
		return ret;
	}

	// Returns true if a sphere circumscribed around the volumes intersect (false = guaranteed no collision)
	bool circumscribePass(Volume v1, Volume v2) {
		float r1 = glm::length(v1.size/2.f);
		float r2 = glm::length(v2.size/2.f);
		float dist = glm::distance(v1.pos, v2.pos);
		return (dist <= r2 + r1);
	}

	// Returns true if a sphere inscribed in the volumes intersects (true = guaranteed collision)
	bool inscribedPass(Volume v1, Volume v2) {
		// The abs of the vector going from the center to one of the corners
		glm::vec3 comp1 = glm::abs(v1.rot * (v1.size/2.f));
		glm::vec3 comp2 = glm::abs(v2.rot * (v2.size/2.f));

		// Get sphere with the smallest radius
		float r1 = vecMin(comp1);
		float r2 = vecMin(comp2);
		float dist = glm::distance(v1.pos, v2.pos);

		return (dist <= r2 + r1);
	}

	glm::vec3 axisnorms[3] = {
			{1, 0, 0},
			{0, 1, 0},
			{0, 0, 1}
	};

	std::vector<glm::vec3> Volume::OBB_getAxes() const {
		auto ret = std::vector<glm::vec3>(3);
		for (unsigned int i = 0; i < 3; i++) {
			ret[i] = glm::normalize(OBB_rotate(axisnorms[i]) - OBB_getCenter());
		}
		return ret;
	}
	glm::vec3 Volume::OBB_getExtents() const { return size/2.f; }
	glm::vec3 Volume::OBB_getCenter() const {
		if (rotAnchor == glm::vec3(1/2.f))
			return glm::vec3(0);
		else return OBB_rotate(glm::vec3(0));
	}
	glm::vec3 Volume::OBB_rotate(glm::vec3 p) const {
		return rot * (OBB_getExtents() * (p - 2.f * (rotAnchor - 1/2.f)));
	}

	std::vector<Plane> Volume::OBB_toPlanes() const {
		auto ret = std::vector<Plane>(6);
		auto axes = OBB_getAxes();
		auto extents = OBB_getExtents();

		for (int i = 0; i < 3; i++) {
			ret[2*i+0] = Plane( axes[i],  extents[i] * axes[i]);
			ret[2*i+1] = Plane(-axes[i], -extents[i] * axes[i]);
		}

		return ret;
	}

	int s0[3] = {2, 2, 1};
	int s1[3] = {1, 0, 0};
	int s2[3] = {1, 2, 0};
	int s3[3] = {2, 0, 1};

	// Returns true if the two volumes are intersecting (warning: slow)
	bool separatingAxisPass(Volume v1, Volume v2) {
		glm::mat3x3 c;
		glm::vec3 a = v1.OBB_getExtents(), b = v2.OBB_getExtents();
		auto A = v1.OBB_getAxes(), B = v2.OBB_getAxes();
		glm::vec3 D = v2.pos - v1.pos;

		// Test A0 through A2
		for (unsigned int i = 0; i < 3; i++) {
			float R_0 = a[i];
			float R_1 = 0;
			for (unsigned int j = 0; j < 3; j++)
				R_1 += b[j] * (c[i][j] = std::abs(glm::dot(A[i], B[j]))); // Populate c as we go along
			float R = std::abs(glm::dot(A[i], D));

			if (R > (R_0 + R_1)) return false;
		}

		// Test B0 through B2
		for (unsigned int j= 0; j < 3; j++) {
			float R_0 = 0;
			float R_1 = b[j];
			for (unsigned int i = 0; i < 3; i++)
				R_0 += a[i] * (c[i][j]); // c is already populated
			float R = std::abs(glm::dot(B[j], D));

			if (R > (R_0 + R_1)) return false;
		}

		// Test the 9 cross product axes
		for (unsigned int i = 0; i < 3; i++)
		for (unsigned int j = 0; j < 3; j++) {
			float R_0 = a[s1[i]] * c[s0[i]][j] + a[s0[i]] * c[s1[i]][j];
			float R_1 = b[s1[j]] * c[i][s0[j]] + b[s0[j]] * c[i][s1[j]];
			float R = std::abs(c[s2[i]][j] * glm::dot(A[s3[i]], D) - c[s3[i]][j] * glm::dot(A[s2[i]], D));

			if (R > (R_0 + R_1)) return false;
		}

		return true;
	}


	bool Volume::intersects(Volume v) const {
		// If both volumes are axis aligned, do standard intersection
		if (rot.w > .9999f && v.rot.w > .9999f)
			return
			  !(getTopCorner().x <= v.getBottomCorner().x ||
				getBottomCorner().x >= v.getTopCorner().x ||
				getTopCorner().y <= v.getBottomCorner().y ||
				getBottomCorner().y >= v.getTopCorner().y ||
				getTopCorner().z <= v.getBottomCorner().z ||
				getBottomCorner().z >= v.getTopCorner().z);
		else { // If not AA use a 3-pass combination algorithm
			if (!circumscribePass(*this, v)) return false;
			if (inscribedPass(*this, v)) return true;
			return separatingAxisPass(*this, v);
		}
	}


	RayVolumeReport noRayVolumeIntersection = {false};

	RayVolumeReport Volume::intersects(Ray r) const {
		float t_near = std::numeric_limits<float>::min();
		float t_far = r.rad;

		auto planes = OBB_toPlanes();
		r.origin -= pos; // Convert the ray to local space.

		for (Plane p : planes) {
			float v_n = glm::dot(p.normal, r.origin) + p.d;
			float v_d = glm::dot(p.normal, r.dir);

			if (std::abs(v_d) < 0.0001) { // Account for float precision errors
				if (v_n > 0) return noRayVolumeIntersection;
				else continue;
			}

			float t = -v_n / v_d;

			if (v_d > 0) { // Back-facing plane
				if (t < 0) return noRayVolumeIntersection;
				if (t < t_far) t_far = t;
			} else // Front-facing plane
				if (t > t_near) t_near = t;

			if (t_near > t_far) return noRayVolumeIntersection;
		}

		// Woo intersection
		r.origin += pos; // Revert our change
		WorldCoordinate start = r.origin + t_near * r.dir;
		WorldCoordinate end = r.origin + t_far * r.dir;
		WorldCoordinate normalChecker = start - pos;

		if (t_near < 0) { // Origin is inside of the OBB.
			start = r.origin;
			normalChecker = end - pos; // Use the exit rather than the entrance point
		}

		for (Plane p : planes)
			if (p.intersects(normalChecker)) return {true, start, end, p.normal, glm::distance(r.origin, start)};

		return {true, start, end, WorldCoordinate(0, 1, 0), glm::distance(r.origin, start)};
	}


	Plane::Plane() {
		normal = {0, 1, 0};
		d = -1;
	}

	Plane::Plane(WorldCoordinate normal, WorldCoordinate origin) {
		this->normal = normal;
		d = glm::dot(-origin, normal);
	}

	Plane::Plane(WorldCoordinate p1, WorldCoordinate p2, WorldCoordinate p3) {
		normal = glm::normalize(glm::cross(p1 - p2, p1 - p3));
		d = -glm::dot(normal, p1);
	}

	float Plane::distance(WorldCoordinate p) const {
		return glm::dot(normal, p) + d;
	}

	bool Plane::intersects(WorldCoordinate p) const {
		float ans = glm::dot(normal, p) + d;
		return (ans < 0.0001 && ans > -0.0001); // Floating-point shenanigains.
	}
}
