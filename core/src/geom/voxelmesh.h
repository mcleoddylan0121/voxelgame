/*
 * voxelmesh.h
 *
 *  Created on: Jul 27, 2015
 *      Author: Dylan
 */

#ifndef SRC_GEOM_VOXELMESH_H_
#define SRC_GEOM_VOXELMESH_H_

#include "geometry.h"
#include "noise.h"
#include "glcontroller.h"
#include "glbuffer.h"
#include "shapes.h"
#include "liquidmanager.h"
#include "uvmapping.h"
#include "meshing.h"
#include <set>

namespace vox {

	inline LiquidID voxelToLiquidID(VoxelID id) {
		return (LiquidID) (id-8192)>>4; // discard liquid signifier bit (-8192) and discard level (>>4)
	}

	inline VoxelID liquidToVoxelID(LiquidID id, unsigned int level = 0) {
		level %= 16; // sanity check
		return (id << 4) + 8192 + level; // shift ID by 4 to allow room for level (<<4), add liquid signifier bit (+8192), add height/level (+level)
	}

	// Stores a vertex normal. I'm not really sure why I'm commenting this, it's pretty self-explanatory
	class Normal {
	private:
		glm::i8vec3 _normal; // Normalized between [-1, 1], once again to save space. We may switch to a 2_10_10_10_REV in the future, because 10 bits are better than 8.
	public:
		Normal(glm::vec3 normal);
		Normal(): Normal(0.f,1.f,0.f) {}
		Normal(float x, float y, float z): Normal(glm::vec3(x,y,z)) {}
	};

	struct AmbientOcclusion {
		GLubyte _value;
		AmbientOcclusion(bool firstSide, bool secondSide, bool corner);
		AmbientOcclusion() {_value = 0;}
		~AmbientOcclusion() {}
	};

	struct VertexLighting {
		glm::u8vec3 _value;
		VertexLighting(glm::vec3 value = {0,0,0}) { _value = glm::u8vec3(value*255.f); }
		VertexLighting(VoxelID fwdID, VoxelID s1ID, VoxelID s2ID, VoxelID cnrID,
				   const Voxel& fwdV, const Voxel& s1V, const Voxel& s2V, const Voxel& cnrV);

		float magnitude(); // get the strength of the light
	};

	// A raw vertex (for a voxel) to be sent to the graphics card
	struct ChunkVertex {
		gpuLocalCoordinate position;
		Color::GPUColor color;
		AmbientOcclusion occlusion;
		Normal normal;
		VoxelLight vlight;

		ChunkVertex() = default;
		~ChunkVertex() = default;
	};

	struct CubeMarchVertex {
		glm::vec3 position;
		Color::GPUColor color;
		AmbientOcclusion occlusion;
		Normal normal;
		VoxelLight vlight;

		CubeMarchVertex() = default;
		~CubeMarchVertex() = default;
	};

	struct WaterVertex {
		gpuLocalCoordinate position; // This is most likely necessary.
		Normal normal; // Is this even necessary?
		glm::u16vec2 UV; // UV coordinates of our texture, in the case that we are texturing
		Color::GPUColor color; // Used as tint, if we're texturing. Not completely useless.
		uint8_t height; // normalized unsigned byte, used for shallow puddles and stuff
		AmbientOcclusion occlusion;
		Normal tangent; // tangent + bitangent used for normal mapping
		Normal bitangent;
		VoxelLight vlight;
	};

	// Stores mesh data for rendered voxel meshes on the GPU
	class VoxelMeshBuffer {
	public:
		unsigned int elements = 0;
		LocalCoordinate size;
		VoxelMeshBuffer(LocalCoordinate size): size(size) {}

		typedef StackBuffer<GLuint> indexBufferType;
		static GLuint currentIndex;

		// This is static, because every voxel mesh uses the same pattern for indices, {0,1,2,2,3,0}
		static indexBufferType* indexBuffer;
		void checkForIndexBufferUpdate();
		virtual void calculateElements() = 0;
		virtual ~VoxelMeshBuffer() {}
		glm::mat4 getModelMatrix(glm::vec3 size, glm::vec3 pos, glm::quat rot, glm::vec3 anchor = glm::vec3(1/2.f));
	};

	/*
	 * Stores voxels, to save you from the effort.
	 *
	 * The access operators do automatic bounds-checking for you, and return air if you go outside the range.
	 * If you wish to store voxels outside the size, but not draw them, then set the padding to however many layers of padding you want.
	 * If you want to access voxels stored in the negative layers, then you'll have to set offset to be negative, since local coordinates are unsigned.
	 *
	 * Also features automatic mesh generation, though we may de-couple the stored mesh from this class in the future.
	 */
	class VoxelMesh {
	public:
		const int NORTH = 0, SOUTH = 1, EAST = 2, WEST = 3, UP = 4, DOWN = 5;
		inline Voxel operator [] (const LocalCoordinate& position) const { return get(position); }

		void remove(const LocalCoordinate& position);
		void set(VoxelID v, const LocalCoordinate& position);
		Voxel get(const LocalCoordinate& position) const;
		VoxelID getVoxelID(const LocalCoordinate& position) const;

		void unsafeRemove(const LocalCoordinate& position);
		void unsafeSet(VoxelID v, const LocalCoordinate& position);
		Voxel unsafeGet(const LocalCoordinate& position) const;

		void updatePosition(const LocalCoordinate& position);
		std::set<LocalCoordinate, ivec3sort> locsToUpdate;

		VoxelMesh(LocalCoordinate size, int padding,
			Palette* mapping,
			noise::MultiNoise<1>* colorNoise
		);
		virtual ~VoxelMesh();

		bool visible = true;

		bool inPaddedRange(const LocalCoordinate& pos) const;
		bool inRange      (const LocalCoordinate& pos) const;

		LocalCoordinate size;
		int padding; // amount of voxels of padding in every bordering direction. Useful to determine exposed faces.

		Palette* voxelMap;

		// This is the noise that determines the colors of drawn voxels
		// Entity meshes own this, while chunk meshes do not (it belongs to the chunkmap)
		noise::MultiNoise<1>* colorNoise;
		VoxelID const* getVoxelArray() const { return voxels; }
	protected:
		virtual VoxelID* pGet(const LocalCoordinate& position) const;
		VoxelID* voxels;
	};

	// This is a voxelmesh that will have it's data stored on the GPU, into a GPUbuffer at some point
	class RenderedVoxelMesh: public VoxelMesh {
	public:
		virtual void getVertices(const LocalCoordinate& pos);
		virtual void deleteVertices(const LocalCoordinate& position) = 0;

		virtual void createVertices(const LocalCoordinate& position, Voxel v) = 0;
		virtual void createVertices_INITIAL(const LocalCoordinate& position, Voxel v) = 0;

		virtual void generateMesh();
		RenderedVoxelMesh(LocalCoordinate size, int padding,
			Palette* mapping,
			noise::MultiNoise<1>* colorNoise
		): VoxelMesh(size,padding,mapping,colorNoise) {}

	};

	// very much *not* a voxelmesh buffer, this resides on the CPU. Can be converted into an entity mesh buffer (to be implemented), used for collision detection and terrain palette voxels.
	template<typename VertexType> class CPUVoxelMeshBuffer {
		std::vector<Triangle<VertexType>> vertices;
	};

	struct TerrainPaletteMeshVoxel {
		bool exists = false;
		Color color;
		glm::vec3 light;
	};

	struct TerrainPaletteUV {
		glm::i16vec2 _uv;
		TerrainPaletteUV(glm::i16vec2 _uv = glm::i16vec2(0)): _uv(_uv) {}
	};

	union packedGPUNormalHelper {
		int i32;
		struct {
			int x:10;
			int y:10;
			int z:10;
			int w:2;
		} i32f3;
	};

	struct gpu10_10_10_2 {
		GLuint data = 0;

		// warning! This assumes our data is already 10-bits!
		static gpu10_10_10_2 gpuPackedUnsigned(glm::ivec3 in) {
			gpu10_10_10_2 ret;
			glm::uvec3 un(in);
			ret.data = un.x | un.y<<10 | un.z<<20;
			return ret;
		}

		static gpu10_10_10_2 gpuPackedUnsigned(glm::ivec3 in, unsigned int _2Bit) {
			gpu10_10_10_2 ret;
			glm::uvec3 un(in);
			ret.data = un.x | un.y<<10 | un.z<<20 | _2Bit<<30;
			return ret;
		}

		static gpu10_10_10_2 gpuPackedUnsigned(glm::uvec3 in, unsigned int _2Bit) {
			gpu10_10_10_2 ret;
			ret.data = in.x | in.y<<10 | in.z<<20 | _2Bit<<30;
			return ret;
		}

		static gpu10_10_10_2 gpuPackedNormal(glm::vec3 in) {
			gpu10_10_10_2 ret;
			packedGPUNormalHelper packed;
			in *= 511.f;
			packed.i32f3.x = (int) in.x;
			packed.i32f3.y = (int) in.y;
			packed.i32f3.z = (int) in.z;
			ret.data = packed.i32;
			return ret;
		}
	};

	struct TerrainPaletteVertexPhase1 {
		LocalCoordinate position;
		int normalID;
		TerrainPaletteUV uv;
	};

	struct TerrainPaletteVertex {
		gpu10_10_10_2 position; // the 2 encodes normal ID LSBs
		gpu10_10_10_2 uv; // 2 encodes normal ID MSBs, and one of the 10's encodes the occlusion value

		TerrainPaletteVertex(const TerrainPaletteVertex& other) = default;

		TerrainPaletteVertex() = default;
		~TerrainPaletteVertex() = default;
	};

	inline TerrainPaletteVertex packTerrainPaletteVertex(LocalCoordinate position, int normalID, TerrainPaletteUV uv, float occlusion) {
		TerrainPaletteVertex ret;

		int nLSB = normalID%4;
		int nMSB = (normalID/4);

		int occlusionPackedValue = occlusion * 1023.f;
		glm::ivec3 uvPackedValue(uv._uv.x, uv._uv.y, occlusionPackedValue);

		ret.position = gpu10_10_10_2::gpuPackedUnsigned(position, nLSB);
		ret.uv = gpu10_10_10_2::gpuPackedUnsigned(uvPackedValue, nMSB);

		return ret;
	}

	struct TerrainPaletteMeshBuffer {
		std::vector<TerrainPaletteVertexPhase1> verts_unindexed;
		std::vector<TerrainPaletteVertexPhase1> verts;
		std::vector<uint32_t> inds;
	};

	// This has no buffer and is not designed to be rendered individually, instead stores data to be used for chunk meshing
	// However, the CPU buffer can be converted into an EntityMeshBuffer.
	// This will eventually be the only way of creating entity mesh buffers, as it gives more control over collision detection and allows for more efficient meshing
	class TerrainPaletteVoxelMesh {
	public:
		std::vector<SubVoxel> voxels;
		int padding = 1;
		LocalCoordinate size;

		int ID;
		bool exists = true;

		std::vector<PaletteVoxelTexturePolygon> gMesh;
		TerrainPaletteMeshBuffer* buf;

		void generateMesh();

		TerrainPaletteVoxelMesh(): size(1) { exists = false; ID = 0; buf = nullptr; }

		TerrainPaletteVoxelMesh(LocalCoordinate size, std::vector<SubVoxel> data); // padding added automatically

		TerrainPaletteVoxelMesh(LocalCoordinate size, std::vector<SubVoxel> data, int pad);

		virtual ~TerrainPaletteVoxelMesh() { delete buf; }
	};

	constexpr int invSubVoxelSize = 12;

	struct VoxelSurroundings {
		VoxelID north, south, east, west, up, down;
	};

	struct TerrainPaletteVoxel {
		virtual TerrainPaletteVoxelMesh* getMesh(VoxelSurroundings surrounding, Palette* voxelMap) = 0;
		virtual std::vector<TerrainPaletteVoxelMesh*> getAllMeshes() = 0;

		virtual ~TerrainPaletteVoxel() {}
	};

	// Nine times out of ten, We will not be using this, as it has no support for occlusion/edge texturing. However it's nice for like, prototyping i guess.
	struct BasicTerrainPaletteVoxel: public TerrainPaletteVoxel {
		TerrainPaletteVoxelMesh* theMesh;
		TerrainPaletteVoxelMesh* getMesh(VoxelSurroundings surrounding, Palette* voxelMap) { return theMesh; }
		std::vector<TerrainPaletteVoxelMesh*> getAllMeshes() { return {theMesh}; }
	};

	// TODO: implement
	int getSurroundingBits(VoxelSurroundings in, Palette* voxelMap, int mask);

	struct TiledTerrainPaletteVoxel: public TerrainPaletteVoxel {
		std::vector<TerrainPaletteVoxelMesh*> meshes; // 64 different possible meshes
		TerrainPaletteVoxelMesh* getMesh(VoxelSurroundings surrounding, Palette* voxelMap) { return meshes[getSurroundingBits(surrounding, voxelMap, Voxel::TRANSPARENT)]; }
		std::vector<TerrainPaletteVoxelMesh*> getAllMeshes() { return meshes; }
	};

	template<typename T>
	inline void fillVector(std::vector<T>& in, int startIndex, T start, int size) {
		for(int i = 0; i < size; i++) {
			in[startIndex + i] = start + i;
		}
	}

	inline void setMinorAndMajorPadding(std::vector<int>& in, bool minor, bool major, int sampleVectorEnd) {
		// If a direction is true, it is solid and we fill the padding
		// Else, it is not solid and we pad with air

		if(minor) in[0] = in[1]; // solid
		else      in[0] = -1;    // air

		if(major) in[sampleVectorEnd] = in[sampleVectorEnd-1];
		else      in[sampleVectorEnd] = -1;

	}

	inline TerrainPaletteVoxelMesh* getVoxelMeshFromCubeAndSurroundings(TerrainPaletteVoxelMesh* bigOlCube, bool north, bool south, bool east, bool west, bool up, bool down) {
		LocalCoordinate sizePpad(18);
		LocalCoordinate size(16);
		std::vector<SubVoxel> data(sizePpad.x*sizePpad.y*sizePpad.z);

		// this says which planes we sample along each axis
		std::vector< std::vector<int> > samples;

		// where we look to see how to sample
		bool majorDirs[3] = { east, up, north };
		bool minorDirs[3] = { west, down, south };

		for(int i = 0; i < 3; i++) {
			std::vector<int> thisSample(sizePpad[i]);
			bool majorDir = majorDirs[i];
			bool minorDir = minorDirs[i];

			const int sampleLength = size[i];
			const int hSampleLength = size[i]/2;

			const int minorSampleStart = 1;
			const int midSampleStart = 1+size[i];
			const int majorSampleStart = 1+2*size[i];

			const int majorSampleStart_half2 = 1+2*size[i] + size[i]/2;

			const int sampleVectorEnd = sizePpad[i]-1;

			if(!majorDir && !minorDir) { // split the corners
				fillVector<int>(thisSample, 1, minorSampleStart, hSampleLength);
				fillVector<int>(thisSample, 1+hSampleLength, majorSampleStart_half2, hSampleLength);

			} else if(majorDir && minorDir) { // sample the center
				fillVector<int>(thisSample, 1, midSampleStart, sampleLength);
			} else if(majorDir) { // sample minor dir corner
				fillVector<int>(thisSample, 1, minorSampleStart, sampleLength);
			} else { // sample major dir corner
				fillVector<int>(thisSample, 1, majorSampleStart, sampleLength);
			}
			setMinorAndMajorPadding(thisSample, minorDir, majorDir, sampleVectorEnd);

			samples.push_back(thisSample);
		}

		LocalCoordinate totalSizePpad = bigOlCube->size + 2;

		// Now that we know where to sample, we perform our samples and fill up our voxel vector

		LocalCoordinate curPos;
		for(curPos.x = 0; curPos.x < sizePpad.x; ++curPos.x)
		for(curPos.y = 0; curPos.y < sizePpad.y; ++curPos.y)
		for(curPos.z = 0; curPos.z < sizePpad.z; ++curPos.z) {
			const LocalCoordinate sample(samples[0][curPos.x],samples[1][curPos.y],samples[2][curPos.z]);
			const int dataIndex =   curPos.x + sizePpad.x      * (curPos.y + sizePpad.y      * curPos.z);
			const int sampleIndex = sample.x + totalSizePpad.x * (sample.y + totalSizePpad.y * sample.z);
			if(sample.x == -1 || sample.y == -1 || sample.z == -1) {
				data[dataIndex] = SubVoxel{}; // air
			} else {
				data[dataIndex] = bigOlCube->voxels[sampleIndex];
			}
		}

		TerrainPaletteVoxelMesh* ret = new TerrainPaletteVoxelMesh(size, data, 1);
		return ret;
	}

	// give a 48x48x48 cube, and this gives the corresponding TiledTerrainPaletteVoxel
	inline TiledTerrainPaletteVoxel* generateTiledTerrainPaletteVoxel(TerrainPaletteVoxelMesh* bigOlCube) {
		TiledTerrainPaletteVoxel* ret = new TiledTerrainPaletteVoxel{};

		// loop through all directions being enabled/disabled
		std::vector<bool> loop = {true,false};

		int index = 0;
		for(bool down:loop)
		for(bool up:loop)
		for(bool west:loop)
		for(bool east:loop)
		for(bool south:loop)
		for(bool north:loop) {
			TerrainPaletteVoxelMesh* m = getVoxelMeshFromCubeAndSurroundings(bigOlCube, north, south, east, west, up, down);
			ret->meshes.push_back(m);
		}

		return ret;
	}

	class TerrainPalette {
	public:
		int cur = 0;
		TerrainPaletteVoxel** _voxels;
		PaletteVoxelUVMapper* uvMapper;
		TerrainPaletteVoxelMesh* get(int ID, VoxelSurroundings surr, Palette* voxelMap) { return _voxels[ID]->getMesh(surr, voxelMap); }
		TerrainPalette(int size) {
			_voxels = new TerrainPaletteVoxel*[size];
			uvMapper = new PaletteVoxelUVMapper{};
		}
		void addVoxel(TerrainPaletteVoxel* toAdd) { _voxels[cur++] = toAdd; }
		void makeUVMap() {
			std::vector<UVMapComponent<PaletteVoxelUVMapData>*> comps;

			for(int k = 0; k < cur; k++) {
				std::vector<TerrainPaletteVoxelMesh*> ms = _voxels[k]->getAllMeshes();
				for(auto* m:ms)
				for(PaletteVoxelTexturePolygon& poly:m->gMesh) {
					UVMapComponent<PaletteVoxelUVMapData>* retComp = new UVMapComponent<PaletteVoxelUVMapData>{};
					retComp->size = poly.size;
					retComp->exists = std::vector<bool>(retComp->size.x * retComp->size.y);
					retComp->data = std::vector<PaletteVoxelUVMapData>(retComp->exists.size());
					for(unsigned int i = 0; i < retComp->data.size(); ++i) {
						retComp->exists[i] = poly.textureData[i].exists;
						retComp->data[i].color = poly.textureData[i].color.toGPUColor();
						retComp->data[i].light = glm::u8vec4(poly.textureData[i].lighting*255.f,1);
					}
					comps.push_back(retComp);
					poly.uvMapComp = retComp;
				}
			}
			uvMapper->_mapper.components = comps;
			uvMapper->construct();

			for(int k = 0; k < cur; k++) {
				std::vector<TerrainPaletteVoxelMesh*> ms = _voxels[k]->getAllMeshes();
				for(auto* m:ms) {
					for(PaletteVoxelTexturePolygon& poly:m->gMesh) {
						for(Quad<GreedyMeshVertex>& q:poly.components) {
							for(int i = 0; i < 4; i++) {
								q[i].polygonPosition += poly.uvMapComp->pos;
							}
						}
					}
					m->generateMesh();
				}
			}

		}
	};



	struct TerrainPaletteVoxelPointer {
		uint32_t startIndex;
		uint32_t size;
	};

	// stores vertices that are referenced by index with some other supporting information
	struct CPUGridVoxelVertex {
		LocalCoordinate position;
		int normalID;
	};

	/*
	 *
	 * HERE BE STATIC LIGHTING
	 * I DONT EVEN KNOW MAN, THIS IS TOO DIFFICULT TO WRAP MY HEAD AROUND RIGHT NOW
	 * I KINDA WANNA PLAY DWARF FORTRESS, MAYBE I'LL DO THIS LATER?
	 * TODO TODO TODO TODO TODO LMAO
	// aligned with the gpu index buffer
	struct CPUGridVoxelIndex {
		uint32_t index;
		int offsID; // is this in offset position 0, 1, 2, or 3?
	};

	// Not to be used for collision, instead handles things that are per-grid-vertex, like lighting
	struct CPUGridVoxelMesh {
		std::vector<CPUGridVoxelVertex> vertices;
		std::map<CPUGridVoxelVertex, uint32_t> vertMap;
		std::vector<uint32_t> indices; // should line up with the chunkIndexBuffer
	};
	*/

	// This one is intended for collision
	struct CPUGridVoxelCollisionMeshUnit {
		static const int SIZE;
		std::vector<VoxelCoordinate> solidVoxels; // loop through these guys and check if you intersect. If you do, you're colliding.
	};

	class CPUGridVoxelCollisionMesh {
	private:
		CPUGridVoxelCollisionMeshUnit* getUnitOverSize(glm::ivec3 position); // in localCoordinate / SIZE
	public:
		static const int PER_CHUNK; // Chunk::SIZE / CPUGridVoxelCollisionMeshUnit::SIZE (TODO: don't hardcode this maybe?)
		CPUGridVoxelCollisionMeshUnit units[8*8*8];
		CPUGridVoxelCollisionMeshUnit* getUnit(LocalCoordinate position); // in localCoordinates

		std::vector<CPUGridVoxelCollisionMeshUnit*> getUnits(LocalCoordinate start, LocalCoordinate size);
	};

	class ChunkMeshBuffer: public VoxelMeshBuffer {
	public:
		typedef StackBuffer<TerrainPaletteVertex> BufferType;
		typedef StackBuffer<uint32_t> IndexBufferType;

		typedef MappedBuffer<unsigned int, Quad<WaterVertex>> WaterBufferType;
		typedef MappedBuffer<unsigned int, bool> TriangulationBufferType;
		typedef ArrayBuffer<VertexLighting> LightBufferType;

		BufferType* buffer;
		IndexBufferType* chunkIndexBuffer;
		TriangulationBufferType* triBuffer;
		LightBufferType* lightBuffer;



		// CPUGridVoxelMesh gridMesh;

		CPUGridVoxelCollisionMesh* gridVoxelCollisionMesh;

		std::map<glm::u8vec3, TerrainPaletteVoxelPointer, VertSort<glm::u8vec3>> cpuPointerArray;

		WaterBufferType* wBuffer;
		TriangulationBufferType* wTriBuffer;
		LightBufferType* wLightBuffer;




		typedef MappedBuffer<unsigned int, Triangle<CubeMarchVertex>> CubeMarchBufferType;
		CubeMarchBufferType* cubeMarchBuffer;

		void sendToGPU() {
			buffer->generateBuffer(); chunkIndexBuffer->generateBuffer();
			lightBuffer->generateBuffer(); wBuffer->start();
			wLightBuffer->generateBuffer(); cubeMarchBuffer->generateBuffer();
		}
		void streamLightData() { lightBuffer->streamData(); wLightBuffer->streamData(); }
		~ChunkMeshBuffer() {
			delete buffer; delete chunkIndexBuffer;
			delete triBuffer; delete lightBuffer;
			delete wBuffer; delete wTriBuffer;
			delete wLightBuffer; delete cubeMarchBuffer;
		}
		ChunkMeshBuffer(LocalCoordinate size);
		void calculateElements();

		bool isEmpty() { return chunkIndexBuffer->size > 0; }

		void removeVoxel(glm::u8vec3 position) {
			auto found = cpuPointerArray.find(position);
			if(found == cpuPointerArray.end()) return;
			TerrainPaletteVoxelPointer ptr = found->second;

			// It does what it says on the tin
			std::vector<uint32_t> zeros(ptr.size);
			for(uint32_t i = 0; i < ptr.size; i++) zeros[i] = 0;

			chunkIndexBuffer->modifyRegion(ptr.startIndex,zeros);
		}
	};

	class EntityMeshBuffer: public VoxelMeshBuffer {
	public:
		typedef StackBuffer<TerrainPaletteVertex> BufferType;
		typedef StackBuffer<uint32_t> IndexBufferType;
		BufferType* buffer;
		IndexBufferType* entityIndexBuffer;

		TerrainPalette* globalNotGlobalPalette;	// Should use a global palette, does not right now. TODO: figure out the logistics of that

		void sendToGPU() { buffer->generateBuffer(); entityIndexBuffer->generateBuffer(); }
		void render(EntityShaderProgram* program, const Camera& viewCam, WorldCoordinate position, WorldCoordinate scale, float rot, ChunkMap* parentMap);
		void renderShadows(ShadowShaderProgram* program, const OrthoCamera& sunCam, WorldCoordinate position, WorldCoordinate scale, float rot);

		~EntityMeshBuffer() { delete buffer; delete entityIndexBuffer; }
		EntityMeshBuffer(LocalCoordinate size);
		void calculateElements();
	};

	class EntityMesh: public RenderedVoxelMesh {
	public:
		static EntityMesh* allocate(const Asset&);

		EntityMesh(LocalCoordinate size, Palette* mapping,
				noise::MultiNoise<1>* colorNoise = new noise::CopyNoise<1>(
					new noise::EmptyNoise(0.4)
				), int padding = 0);
		virtual ~EntityMesh();
		void createVertices(const LocalCoordinate& position, Voxel v);
		void createVertices_INITIAL(const LocalCoordinate& position, Voxel v);
		void deleteVertices(const LocalCoordinate& position);

		void updateMesh();
		virtual void generateMesh();
		EntityMeshBuffer* buf;
	};

	class ChunkMesh: public RenderedVoxelMesh {
	public:
		static const int GRIDVOXEL_SIZE;

		ChunkMesh(LocalCoordinate size, Palette* mapping,
				noise::MultiNoise<1>* colorNoise, WorldCoordinate worldPos,
				LiquidManager* liquidManager, TerrainPalette* actualPalette);

		LightLevel getLightLevel(VoxelID id, Voxel v);
		WorldCoordinate worldPos;
		LiquidManager* liquidManager;
		TerrainPalette* actualPalette;
		void createVertices(const LocalCoordinate& position, Voxel v);
		void createVertices_INITIAL(const LocalCoordinate& position, Voxel v);
		void deleteVertices(const LocalCoordinate& position);

		void updateLightBuffer();

		virtual void generateMesh();

		ChunkMeshBuffer* buf;

		unsigned int curIndex = 0;

		virtual ~ChunkMesh();
		void deleteBuffer() { delete buf; }

		void getVertices(const LocalCoordinate& position, bool intial);
		void updateMesh();

		void fastSetVoxel(glm::i8vec3 position, VoxelID v);
		VoxelID fastGetVoxelID(glm::i8vec3 position);
	};


}



#endif /* SRC_GEOM_VOXELMESH_H_ */
