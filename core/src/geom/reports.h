/*
 * reports.h
 *
 *  Created on: Jan 1, 2016
 *      Author: Daniel
 */

#ifndef CORE_SRC_GEOM_REPORTS_H_
#define CORE_SRC_GEOM_REPORTS_H_

#include "coords.h"

namespace vox {

	struct RayVolumeReport {
		bool intersects;
		WorldCoordinate start, end, normal;
		float distance;
	};


	struct Entity;
	struct EntityRaycastReport {
		bool found;
		float distanceTravelled;
		WorldCoordinate endPoint;
		glm::vec3 normal;
		Entity * e;
	};
}



#endif /* CORE_SRC_GEOM_REPORTS_H_ */
