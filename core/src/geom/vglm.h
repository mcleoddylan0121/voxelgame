/*
 * vglm.h
 *
 *  Created on: May 24, 2015
 *      Author: Dani
 */

#ifndef VGLM_H_
#define VGLM_H_

#define GLM_FORCE_RADIANS

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/random.hpp>
#include <glm/gtx/quaternion.hpp>

#ifdef __custom__
#include <gl/glew.h>
#include <gl/gl.h>
#else
#include <GL/glew.h>
#include <GL/gl.h>
#endif

#endif /* VGLM_H_ */
