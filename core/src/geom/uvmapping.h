/*
 * uvmapping.h
 *
 *  Created on: Jul 2, 2016
 *      Author: dylan
 */

#ifndef SRC_GEOM_UVMAPPING_H_
#define SRC_GEOM_UVMAPPING_H_
#include "vglm.h"
#include "geometry.h"
#include "renderutils.h"

namespace vox {


	template<typename TextureData_T>
	struct UVMapComponent {
		glm::ivec2 size;
		std::vector<bool> exists;
		std::vector<TextureData_T> data;

		glm::ivec2 pos = glm::ivec2(0);
	};

	template<typename TextureData_T>
	class UVMapper {
	public:
		typedef UVMapComponent<TextureData_T> CompType;
		std::vector<CompType*> components;
		void addComponent(CompType* comp) { components->push_back(comp); }

		glm::ivec2 size;

		std::vector<bool> mask;
		std::vector<TextureData_T> data;

		UVMapper(glm::ivec2 size = glm::ivec2(512)): size(size) {
			mask = std::vector<bool>(size.x*size.y);
			data = std::vector<TextureData_T>(size.x*size.y);
		}

		void pack() {
			for(int i = 0; i < (int)mask.size(); i++) mask[i] = false;

			// TODO: Beware: Extremely slow algorithm!!! (fix it you fool)
			for(CompType* comp:components) {
				int x,y;
				for(y = 0; y < size.y-comp->size.y; y+=8)
				for(x = 0; x < size.x-comp->size.x; x+=8) {

					bool maskCollide = false;
					for(int y2 = 0; y2 < comp->size.y; y2++)
					for(int x2 = 0; x2 < comp->size.x; x2++) {
						maskCollide |= (comp->exists[x2+y2*comp->size.x] && mask[(x+x2) + (y+y2)*size.x]);
					}

					if(maskCollide) continue;

					goto UVMapLoopEnd1;
				}

				printlnf(FATALERROR, "UV map too small! Line %i in file %s", __LINE__, __FILE__);
				exit(1);

				UVMapLoopEnd1:;

				comp->pos = glm::ivec2(x,y);

				for(int y2 = 0; y2 < comp->size.y; y2++)
				for(int x2 = 0; x2 < comp->size.x; x2++) {
					bool compExists = comp->exists[x2+y2*comp->size.x];
					bool maskExists = mask[(x+x2) + (y+y2)*size.x];
					mask[(x+x2) + (y+y2)*size.x] = maskExists | compExists;
					if(compExists) data[(x+x2) + (y+y2)*size.x] = comp->data  [x2+y2*comp->size.x];
				}

			}
		}

	};


	class PaletteVoxelUVMapper {
	public:
		UVMapper<PaletteVoxelUVMapData> _mapper;
		Texture *colorTex, *lightTex;

		void construct() {
			_mapper.pack();
			std::vector<glm::i8vec4> lightData(_mapper.size.x*_mapper.size.y);
			std::vector<glm::i8vec4> colorData(_mapper.size.x*_mapper.size.y);
			for(unsigned int i = 0; i < lightData.size(); i++) {
				lightData[i] = _mapper.data[i].light;
				colorData[i] = _mapper.data[i].color;
				colorData[i].a = 255;
				lightData[i].a = 255;
			}
			colorTex = createTexture2D(_mapper.size,colorData.data(),GL_RGBA8,GL_NEAREST,GL_REPEAT,GL_RGBA,GL_UNSIGNED_BYTE);
			lightTex = createTexture2D(_mapper.size,lightData.data(),GL_RGBA8,GL_NEAREST,GL_REPEAT,GL_RGBA,GL_UNSIGNED_BYTE);
		}

		~PaletteVoxelUVMapper() { delete colorTex; delete lightTex; }
	};


}




#endif /* SRC_GEOM_UVMAPPING_H_ */
