/*
 * miscmesh.h
 *
 *  Created on: Jul 27, 2015
 *      Author: Dylan
 */

#ifndef SRC_GEOM_MISCMESH_H_
#define SRC_GEOM_MISCMESH_H_

#include "geometry.h"
#include "shapes.h"
#include "glbuffer.h"

namespace vox {

	// for your average, run of the mill objects
	struct SpatialMeshBuffer {
		GLBuffer<glm::vec3>* verts;
		GLBuffer<Triangle<int>>* inds;
		SpatialMeshBuffer(GLBuffer<glm::vec3>* verts, GLBuffer<Triangle<int>>* inds): verts(verts), inds(inds) {}
		virtual ~SpatialMeshBuffer() { delete verts; delete inds; }
	};

	template<typename T> struct GLBuffer;
	SpatialMeshBuffer* genSphere(int iterations, bool invert);
	SpatialMeshBuffer* genDome(int iterations, bool invert, float cutoffHeight);

	struct TexturedMeshVertex {
		glm::vec3 pos;
		glm::vec2 UV;
	};

	struct TexturedMeshBuffer {
		GLBuffer<TexturedMeshVertex>* verts;
		GLBuffer<Triangle<int>>* inds;
		TexturedMeshBuffer(GLBuffer<TexturedMeshVertex>* verts, GLBuffer<Triangle<int>>* inds): verts(verts), inds(inds) {}
		virtual ~TexturedMeshBuffer() { delete verts; delete inds; }

		static TexturedMeshBuffer* createBillboard(Texture* tex, glm::vec2 UVstart = {0,0}, glm::vec2 UVsize = {-1,-1});
	};

}



#endif /* SRC_GEOM_MISCMESH_H_ */
