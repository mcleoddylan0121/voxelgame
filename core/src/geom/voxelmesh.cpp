/*
 * voxelmesh.cpp
 *
 *  Created on: Jul 27, 2015
 *      Author: Dylan
 */

#include "voxelmesh.h"
#include "resourcemanager.h"
#include "glbuffer.h"
#include "sdlcontroller.h"
#include "io.h"
#include "liquidmanager.h"
#include "voxelmeshlookups.h"

#include "meshing.h"

#include "chunk.h"

namespace vox {

	const int ChunkMesh::GRIDVOXEL_SIZE = 16;

	VertexLighting::VertexLighting(
			   VoxelID fwdID, VoxelID s1ID, VoxelID s2ID, VoxelID cnrID,
			   const Voxel& fwdV, const Voxel& s1V, const Voxel& s2V, const Voxel& cnrV) {
		glm::vec3 fwd = glm::vec3(getLightLevelfromVoxelAndID(fwdID, fwdV)._level)/(LightLevel::MAX);
		glm::vec3 s1 = glm::vec3(getLightLevelfromVoxelAndID(s1ID,  s1V)._level)/(LightLevel::MAX);
		glm::vec3 s2 = glm::vec3(getLightLevelfromVoxelAndID(s2ID,  s2V)._level)/(LightLevel::MAX);
		glm::vec3 cnr = glm::vec3(getLightLevelfromVoxelAndID(cnrID, cnrV)._level)/(LightLevel::MAX);

		bool cnrIsVisible = s1V.is(Voxel::IMAGINARY) || s2V.is(Voxel::IMAGINARY);

		int facesVisible = 1 +
			(int) s1V.is(Voxel::IMAGINARY | Voxel::LIGHT_SOURCE) +
			(int) s2V.is(Voxel::IMAGINARY | Voxel::LIGHT_SOURCE) +
			cnrIsVisible * (int) cnrV.is(Voxel::IMAGINARY | Voxel::LIGHT_SOURCE);

		_value = glm::u8vec3((fwd + s1 + s2 + (cnr * (float) cnrIsVisible)) * (255.f / facesVisible));
	}

	float VertexLighting::magnitude() { return _value.x + _value.y + _value.z; }

	VoxelMesh::~VoxelMesh() {
		delete[] voxels;
	}

	EntityMesh::~EntityMesh() {
		delete colorNoise;
		if(voxelMap && voxelMap != errorPalette)
			delete voxelMap;
	}

	ChunkMesh::~ChunkMesh() {}

	VoxelMesh::VoxelMesh(LocalCoordinate size, int padding, Palette * mapping, noise::MultiNoise<1>* colorNoise): colorNoise(colorNoise) {
		if (mapping) { this->voxelMap = mapping; }
		else { mapping = errorPalette; }

		this->padding = padding;
		this->size = size;
		voxels = new VoxelID[(size.x + 2*padding) * (size.y + 2*padding) * (size.z + 2*padding)];
	}

	EntityMesh::EntityMesh(LocalCoordinate size, Palette* mapping, noise::MultiNoise<1>* colorNoise, int padding):
			RenderedVoxelMesh(size, padding, mapping, colorNoise) {
		for(int i = 0; i < (size.x + 2*padding) * (size.y + 2*padding) * (size.z + 2*padding); i++)
			voxels[i] = 0;
		buf = new EntityMeshBuffer(size);
	}

	EntityMeshBuffer::EntityMeshBuffer(LocalCoordinate size): VoxelMeshBuffer(size) {
		buffer = new BufferType(ARRAY, STATIC);
		entityIndexBuffer = new IndexBufferType(ELEMENT_ARRAY, STATIC);
		globalNotGlobalPalette = nullptr;
	}

	ChunkMesh::ChunkMesh(LocalCoordinate size, Palette * mapping, noise::MultiNoise<1>* colorNoise, WorldCoordinate worldPos, LiquidManager* liquidManager,
			TerrainPalette* actualPalette):
			RenderedVoxelMesh(size, 1, mapping, colorNoise), worldPos(worldPos), liquidManager(liquidManager), actualPalette(actualPalette) {
		for(int i = 0; i < (size.x + 2*padding) * (size.y + 2*padding) * (size.z + 2*padding); i++)
			voxels[i] = 0;

		buf = new ChunkMeshBuffer(size);
	}

	ChunkMeshBuffer::ChunkMeshBuffer(LocalCoordinate size): VoxelMeshBuffer(size) {
		buffer = new BufferType(ARRAY, DYNAMIC);
		chunkIndexBuffer = new IndexBufferType(ELEMENT_ARRAY, DYNAMIC);
		lightBuffer = new LightBufferType(ARRAY, DYNAMIC);
		triBuffer = new TriangulationBufferType(ARRAY, DYNAMIC);
		wBuffer = new WaterBufferType(ARRAY, DYNAMIC);
		wLightBuffer = new LightBufferType(ARRAY, DYNAMIC);
		wTriBuffer = new TriangulationBufferType(ARRAY, DYNAMIC);
		cubeMarchBuffer = new CubeMarchBufferType(ARRAY, DYNAMIC);
		gridVoxelCollisionMesh = new CPUGridVoxelCollisionMesh{};
	}

	Voxel VoxelMesh::get(const LocalCoordinate& position) const {
		return voxelMap->get(getVoxelID(position));
	}

	VoxelID VoxelMesh::getVoxelID(const LocalCoordinate& position) const {
		return inPaddedRange(position)? *pGet(position) : 0;
	}

	VoxelID* VoxelMesh::pGet(const LocalCoordinate& position) const {
		return &voxels[(position.x + padding) 	 + (position.y + padding) * (size.x + 2*padding) 		+ (position.z + padding) * (size.x + 2*padding) * (size.y + 2*padding)];
	}

	void VoxelMesh::set(VoxelID v, const LocalCoordinate& position) {
		if(inPaddedRange(position))
			*pGet(position) = v;
	}

	void VoxelMesh::remove(const LocalCoordinate& position) {
		set(0, position);
	}

	void VoxelMesh::unsafeSet(VoxelID v, const LocalCoordinate& position) {
		*pGet(position) = v;
	}

	void VoxelMesh::unsafeRemove(const LocalCoordinate& position) {
		unsafeSet(0, position);
	}

	Voxel VoxelMesh::unsafeGet(const LocalCoordinate& position) const {
		return voxelMap->get(*pGet(position));
	}

	std::chrono::high_resolution_clock::time_point beginTime;

	float total = 0, timesStamped = 0;

	void RenderedVoxelMesh::generateMesh() {

		// beginTime = mSDL->getTimePoint();

		for(int z = 0; z < size.z; z++) for(int y = 0; y < size.y; y++) for(int x = 0; x < size.x; x++) {
			LocalCoordinate pos = {x, y, z};
			Voxel v = unsafeGet(pos);
			if(!v.is(Voxel::IMAGINARY))
				createVertices_INITIAL(pos, v);
		}

		// total += mSDL->getTime<nanoseconds>(beginTime) / 1000000000.f;
		// ++timesStamped;

		// printlnf(TEMP,"current: %f average: %f",mSDL->getTime<nanoseconds>(beginTime) / 1000000000.f, total/timesStamped);

	}

	void chunkmeshMarchCubes(ChunkMesh* ch, LocalCoordinate position, Voxel vx);

	void ChunkMesh::fastSetVoxel(glm::i8vec3 position, VoxelID v) {
		voxels[(position.x + padding) 	 + (position.y + padding) * (size.x + 2*padding) 		+ (position.z + padding) * (size.x + 2*padding) * (size.y + 2*padding)] = v;
	}

	VoxelID ChunkMesh::fastGetVoxelID(glm::i8vec3 position) {
		return voxels[(position.x + padding) 	 + (position.y + padding) * (size.x + 2*padding) 		+ (position.z + padding) * (size.x + 2*padding) * (size.y + 2*padding)];
	}

	void VoxelMeshBuffer::checkForIndexBufferUpdate() {
		if(indexBuffer->size < elements) {
			while(indexBuffer->size < elements) {
				indexBuffer->push({currentIndex, currentIndex + 1, currentIndex + 2, currentIndex + 2, currentIndex + 3, currentIndex});
				currentIndex += 4;
			}
			// Updating can be expensive, static buffers exist to minimize frequent updating.
			// only update after we've made all necessary changes.
			indexBuffer->updateBuffer();
		}
	}

	void ChunkMeshBuffer::calculateElements() {
		elements = chunkIndexBuffer->size;
		if(elements == 0) return;
		// checkForIndexBufferUpdate();
	}

	void EntityMeshBuffer::calculateElements() {
		elements = entityIndexBuffer->size;
		if(elements == 0) return;
		checkForIndexBufferUpdate();
	}

	bool VoxelMesh::inRange(const LocalCoordinate& pos) const {
		return pos.x < size.x && pos.y < size.y && pos.z < size.z
			&& pos.x >= 0     && pos.y >= 0     && pos.z >= 0;
	}

	bool VoxelMesh::inPaddedRange(const LocalCoordinate& pos) const {
		return pos.x < size.x + padding && pos.y < size.y + padding && pos.z < size.z + padding
			&& pos.x >= -padding        && pos.y >= -padding        && pos.z >= -padding;
	}

	void VoxelMesh::updatePosition(const LocalCoordinate& pos) {
		if(inPaddedRange(pos)) {
			for(int x = -1; x <= 1; x++) for(int y = -1; y <= 1; y++) for(int z = -1; z <= 1; z++) {
				if(inRange(pos + LocalCoordinate(x,y,z))) {
					locsToUpdate.insert(pos + LocalCoordinate(x,y,z));
				}
			}
		}
	}

	glm::mat4 VoxelMeshBuffer::getModelMatrix(glm::vec3 size, glm::vec3 pos, glm::quat rot, glm::vec3 anchor) {
		glm::mat4 model(1.0);
		model = glm::translate(model, pos);
		model = glm::scale(model, size / glm::vec3(this->size));

		glm::mat4 rotM(1.0);
		rotM = glm::translate(rotM, glm::vec3(this->size) * anchor);
		rotM = glm::scale(rotM, 	glm::vec3(this->size) / size);
		rotM = rotM * glm::toMat4(rot);
		rotM = glm::scale(rotM, 	size / glm::vec3(this->size));
		rotM = glm::translate(rotM,-glm::vec3(this->size) * anchor);
		model *= rotM;

		return model;
	}

	void EntityMesh::updateMesh() {

	}

	void ChunkMesh::updateMesh() {
		/*
		int sz = locsToUpdate.size();
		if(sz > 30) // This number can be fine-tuned, but it's probably not relevant anyway
			buf->buffer->pause(); // pause it, so we can update the entire buffer at once

		int i = 0;
		while(!locsToUpdate.empty() && i < 4096) {
			i++;
			auto it = locsToUpdate.begin();
			LocalCoordinate loc = *it;
			locsToUpdate.erase(it);
			getVertices(loc);
		}

		if(sz > 30 && locsToUpdate.empty()) {
			buf->buffer->streamData();
			buf->buffer->started = true; // unpause it, so automatic streaming continues
		}*/

		if(locsToUpdate.empty()) return;

		while(!locsToUpdate.empty()) {
			// i++;
			auto it = locsToUpdate.begin();
			LocalCoordinate loc = *it;
			locsToUpdate.erase(it);
			buf->removeVoxel(glm::u8vec3(loc));
			getVertices(loc, false);
		}
		buf->buffer->updateBuffer();
		buf->chunkIndexBuffer->updateBuffer();
	}

	EntityMesh* EntityMesh::allocate(const Asset& ent) {
		mFileManager->verifyAsset(ent, Asset::Mesh);
		return loadEntityVox(ent);
	}

	AmbientOcclusion::AmbientOcclusion(bool firstSide, bool secondSide, bool corner) {
		_value = 2 - ((firstSide || secondSide)? ((int) firstSide + (int) secondSide) : (int) corner);
	}

	// This function looks complex, but i digress. It is only confusing.
	void RenderedVoxelMesh::getVertices(const LocalCoordinate& position) {
		Voxel v = get(position);
		if(!v.is(Voxel::IMAGINARY))
			 createVertices(position, v);
		else deleteVertices(position);
	}

	void ChunkMesh::deleteVertices(const LocalCoordinate& position) {
		for(int dir = 0; dir < 6; dir++) {
			// Although there is air here, there may have not been in the past. That means we should remove the faces from the buffer.
			unsigned int key = (((unsigned int)position.x) << 24) + (((unsigned int)position.y) << 16) + ((unsigned int)position.z << 8) + dir;
			// buf->buffer->remove(key);
			buf->triBuffer->remove(key);
		}
	}

	void EntityMesh::deleteVertices(const LocalCoordinate& position) {
		for(int dir = 0; dir < 6; dir++) {
			// Although there is air here, there may have not been in the past. That means we should remove the faces from the buffer.
			unsigned int key = (((unsigned int)position.x) << 24) + (((unsigned int)position.y) << 16) +  (((unsigned int)position.z) << 8) + dir;
			//buf->buffer->remove(key);
		}
	}

	template<typename T> void someLightUpdateHelperFunction(MappedBuffer<unsigned int, Quad<T>>* buf, MappedBuffer<unsigned int, bool>* triBuf, ArrayBuffer<VertexLighting>* light, ChunkMesh* m) {
		// TODO: use indexed algorithm to improve this function's runtime

		light->resizeArray(buf->size * 4);
		auto map = buf->getMap();
		auto triMap = triBuf->getMap();
		auto triIt = triMap->begin();
		for(auto it = map->begin(); it != map->end(); it++, triIt++) {
			int index = it->second;
			int dir = it->first & 255;
			bool tri = *(triBuf->getFromIndex(triIt->second));
			const Quad<T>& q = *(buf->getFromIndex(index));
			LocalCoordinate position = LocalCoordinate(q[0].position) - faces[dir][3*(int)tri];

			VoxelID forwardID = m->getVoxelID(position + faceOffsets[dir]);
			Voxel forwardV = m->voxelMap->get(forwardID);

			for(int i = 0; i < 4; i++) {
				VoxelID firstSideID = m->getVoxelID(position + voxelsBorderingVertex[dir][i][0]);
				VoxelID secondSideID = m->getVoxelID(position + voxelsBorderingVertex[dir][i][1]);
				VoxelID cornerID = m->getVoxelID(position + cornerVoxel[dir][i]);

				Voxel firstSideV = m->voxelMap->get(firstSideID);
				Voxel secondSideV = m->voxelMap->get(secondSideID);
				Voxel cornerV = m->voxelMap->get(cornerID);

				VertexLighting vLight =
						VertexLighting(	forwardID, firstSideID, secondSideID, cornerID,
				                        forwardV,  firstSideV,  secondSideV,  cornerV);

				light->set(	4*index + ((i + (int) tri)%4), vLight);
			}
		}
	}

	void ChunkMesh::updateLightBuffer() {
		//someLightUpdateHelperFunction<ChunkVertex>(buf->buffer, buf->triBuffer, buf->lightBuffer, this);
		//someLightUpdateHelperFunction<WaterVertex>(buf->wBuffer, buf->wTriBuffer, buf->wLightBuffer, this);
	}

	void getVoxelAO(VoxelMesh* v, LocalCoordinate position, int dir, AmbientOcclusion* out) {
		for(int i = 0; i < 4; i++) {
			Voxel firstSide = v->get(position + voxelsBorderingVertex[dir][i][0]);
			Voxel secondSide = v->get(position + voxelsBorderingVertex[dir][i][1]);
			Voxel corner = v->get(position + cornerVoxel[dir][i]);
			out[i] = AmbientOcclusion(
					!firstSide.is(Voxel::IMAGINARY | Voxel::LIQUID),
					!secondSide.is(Voxel::IMAGINARY | Voxel::LIQUID),
					!corner.is(Voxel::IMAGINARY | Voxel::LIQUID)
				);
		}
	}

	Color getVertexColor(Voxel v, noise::MultiNoise<1>* noise, LocalCoordinate position, LocalCoordinate vertexPos, WorldCoordinate worldPos = WorldCoordinate(0)) {
		noise::MultiNoise<1>::value_t noiseColor = noise->noise3D(glm::vec3(position + vertexPos) + worldPos);
		return v.color + YELLOW * noiseColor[0];
	}

	/*
		im not sure why this exists?
	*/
	glm::vec3 VertexInterp(LocalCoordinate p1, LocalCoordinate p2) {
		return glm::mix(glm::vec3(p1), glm::vec3(p2), 0.5);
	}

	std::vector<Triangle<glm::vec3>> marchingCubes(VoxelMesh* v, LocalCoordinate position) {
		bool grid[8] = {0,0,0,0,0,0,0,0};

		for(int i = 0; i < 8; i++) {
			grid[i] |= !(v->get(position + corners[i]).is(Voxel::TRANSPARENT | Voxel::IMAGINARY | Voxel::LIQUID));
		}

		/*for(int i = 0; i < 6; i++) {
			if(!(v->get(position + faceOffsets[i]).is(Voxel::TRANSPARENT | Voxel::IMAGINARY | Voxel::LIQUID))) {
				for(int j = 0; j < 4; j++) {
					grid[cornerObstructions[i][j]] = true;
				}
			}
		}*/

		int cubeindex = 0;
		if (grid[0]) cubeindex |= 1;
		if (grid[1]) cubeindex |= 2;
		if (grid[2]) cubeindex |= 4;
		if (grid[3]) cubeindex |= 8;
		if (grid[4]) cubeindex |= 16;
		if (grid[5]) cubeindex |= 32;
		if (grid[6]) cubeindex |= 64;
		if (grid[7]) cubeindex |= 128;

		/* Cube is entirely in/out of the surface */
		if (edgeTable[cubeindex] == 0)
		  return {};

		glm::vec3 vertlist[12];

		/* Find the vertices where the surface intersects the cube */
		if (edgeTable[cubeindex] & 1)
		  vertlist[0] = VertexInterp(position + corners[0],position + corners[1]);
		if (edgeTable[cubeindex] & 2)
		  vertlist[1] = VertexInterp(position + corners[1],position + corners[2]);
		if (edgeTable[cubeindex] & 4)
		  vertlist[2] = VertexInterp(position + corners[2],position + corners[3]);
		if (edgeTable[cubeindex] & 8)
		  vertlist[3] = VertexInterp(position + corners[3],position + corners[0]);
		if (edgeTable[cubeindex] & 16)
		  vertlist[4] = VertexInterp(position + corners[4],position + corners[5]);
		if (edgeTable[cubeindex] & 32)
		  vertlist[5] = VertexInterp(position + corners[5],position + corners[6]);
		if (edgeTable[cubeindex] & 64)
		  vertlist[6] = VertexInterp(position + corners[6],position + corners[7]);
		if (edgeTable[cubeindex] & 128)
		  vertlist[7] = VertexInterp(position + corners[7],position + corners[4]);
		if (edgeTable[cubeindex] & 256)
		  vertlist[8] = VertexInterp(position + corners[0],position + corners[4]);
		if (edgeTable[cubeindex] & 512)
		  vertlist[9] = VertexInterp(position + corners[1],position + corners[5]);
		if (edgeTable[cubeindex] & 1024)
		  vertlist[10] = VertexInterp(position + corners[2],position + corners[6]);
		if (edgeTable[cubeindex] & 2048)
		  vertlist[11] = VertexInterp(position + corners[3],position + corners[7]);

		/* Create the triangle */
		std::vector<Triangle<glm::vec3>> ret;
		for (int i=0;triTable[cubeindex][i]!=-1;i+=3) {
			ret.push_back(Triangle<glm::vec3>(vertlist[triTable[cubeindex][i]], vertlist[triTable[cubeindex][i+1]], vertlist[triTable[cubeindex][i+2]]));
		}

		return ret;
	}


	// Construct greedy mesh with chunk data
	/*
	void ChunkMesh::generateMesh() {
		glm::ivec3 sizePpad = this->size + 2*padding;
		std::vector<SubVoxel> voxxels = std::vector<SubVoxel>(sizePpad.x*sizePpad.y*sizePpad.z);
		std::vector<bool> voxxxels = std::vector<bool>(sizePpad.x*sizePpad.y*sizePpad.z);
		bool hasFull = false, hasEmpty = false;
		for(unsigned int i = 0; i < voxxels.size(); i++) {
			Voxel voxe = voxelMap->get(this->voxels[i]);
			voxxels[i].exists = !(voxe.is(Voxel::IMAGINARY));
			voxxxels[i] = voxxels[i].exists;

			hasFull |= voxxxels[i];
			hasEmpty |= !(voxxxels[i]);

			voxxels[i].color = voxe.color;
			voxxels[i].lighting = glm::vec3(0);
		}

		if(!(hasFull && hasEmpty)) return; // there is no mesh here

		std::vector<PaletteVoxelTexturePolygon> gMesh = constructGreedyMesh(voxxels,voxxxels,size,padding);

		Color::GPUColor wh = BROWN.toGPUColor();

		int key = 0;
		for(auto poly:gMesh) {
			for(auto q:poly.components) {
				Quad<ChunkVertex> retQ;
				for(int i = 0; i < 4; i++) {
					Color::GPUColor col = wh;
					Normal normal = normals[q[i].direction];
					gpuLocalCoordinate coord(q[i].position);
					VoxelLight llev = VoxelLight(glm::vec3(0));
					retQ[i] = ChunkVertex{coord, col, AmbientOcclusion(), normal, llev};
				}
				buf->buffer->add(key++,retQ);
			}
		}
	}*/

	void chunkmeshMarchCubes(ChunkMesh* ch, LocalCoordinate position, Voxel vx) {
		if(!vx.is(Voxel::IMAGINARY)) return;
		std::vector<Triangle<glm::vec3>> tris = marchingCubes(ch, position);

		std::vector<Triangle<CubeMarchVertex>> out;
		unsigned int ind = 0;
		for(auto i:tris) {
			++ind;
			Triangle<CubeMarchVertex> v;
			glm::vec3 normal = glm::normalize(glm::cross(i[0]-i[1], i[0]-i[2]));
			Color::GPUColor col = Color::mix({ch->get(LocalCoordinate(i[0])).color,ch->get(LocalCoordinate(i[1])).color,ch->get(LocalCoordinate(i[2])).color}).toGPUColor();

			for(int j = 0; j < 3; j++) {
				v[j].position = i[j];
				v[j].color = col;
				v[j].occlusion = AmbientOcclusion();
				v[j].normal = Normal(normal);
			}
			unsigned int key = (((unsigned int)position.x) << 24) + (((unsigned int)position.y) << 16) + ((unsigned int)position.z << 8) + ind;
			ch->buf->cubeMarchBuffer->add(key, v);
		}
	}

	void chunkMeshRemCubeMarch(ChunkMesh* ch, LocalCoordinate position) {
		for(int i = 0; i < 5; i++) {
			unsigned int key = (((unsigned int)position.x) << 24) + (((unsigned int)position.y) << 16) + ((unsigned int)position.z << 8) + i;
			ch->buf->cubeMarchBuffer->remove(key);
		}
	}

	glm::u8vec2 liquidUV[4] = {{0,0}, {0,1}, {1,1}, {1,0}};

	VoxelSurroundings getSurroundingVoxels(LocalCoordinate p, const VoxelMesh* mesh) {
		const VoxelID* const voxels = mesh->getVoxelArray();

		// padding = 1

		// all of the different coordinate values we'll be using (we add one due to padding)
		const int pV[9] = {
			p.x,  (p.x+1), (p.x+2),
			p.y,  (p.y+1), (p.y+2),
			p.z,  (p.z+1), (p.z+2)
		};

		// const int8_t size = 32;
		const int sizePlusTwoTimesPadding = 34;

		// xC is just pV

		const int yC[3] = { // y contribution
				pV[3]*sizePlusTwoTimesPadding,
				pV[4]*sizePlusTwoTimesPadding,
				pV[5]*sizePlusTwoTimesPadding
		};

		const int zC[3] = { // z contribution
				pV[6]*sizePlusTwoTimesPadding*sizePlusTwoTimesPadding,
				pV[7]*sizePlusTwoTimesPadding*sizePlusTwoTimesPadding,
				pV[8]*sizePlusTwoTimesPadding*sizePlusTwoTimesPadding,
		};

		// contributions for default values (x+y,x+z,y+z), saves on one addition
		const int xPy = pV[1]+yC[1],
				  xPz = pV[1]+zC[1],
				  yPz = yC[1]+zC[1];

		return { // nearby voxel IDs
			voxels[xPy+zC[2]], voxels[xPy+zC[0]],
			voxels[yPz+pV[2]], voxels[yPz+pV[0]],
			voxels[xPz+yC[2]], voxels[xPz+yC[0]]
		};
	}

	int getSurroundingBits(VoxelSurroundings in, Palette* voxelMap, int mask) {
		return (voxelMap->get(in.north).is(mask))         | ((voxelMap->get(in.south).is(mask)) << 1)
		     | ((voxelMap->get(in.east).is(mask)) << 2)   | ((voxelMap->get(in.west).is(mask)) << 3)
		     | ((voxelMap->get(in.up).is(mask)) << 4)     | ((voxelMap->get(in.down).is(mask)) << 5);
	}

	bool voxelCompletelySurrounded(char surroundingVoxels) {
		return surroundingVoxels == 0x00; // 0b00000000
	}

	bool faceNotObstructed(char surroundingVoxels, int direction) {
		return surroundingVoxels & (1<<direction);
	}

	unsigned int makeFaceKey(LocalCoordinate position, int dir) {
		return (((unsigned int)position.x) << 24) + (((unsigned int)position.y) << 16) + ((unsigned int)position.z << 8) + dir;
	}

	template<bool INITIAL> void createChunkMeshVertices(ChunkMesh* ch, const LocalCoordinate& position, Voxel v, LiquidManager* lmanager) {
		/*VoxelSurroundings surroundingVoxels = getSurroundingVoxels(position, ch,v.is(Voxel::LIQUID),false);

		bool liquid = v.is(Voxel::LIQUID);

		if(INITIAL && voxelCompletelySurrounded(surroundingVoxels)) return; // no vertices here, we're surrounded

		if(liquid) { // add this to the liquid buffer

			char memb = 0;
			Color::GPUColor memc[8];

			glm::vec2 modPos = glm::vec2(position.x%4, position.z%4);
			Quad<glm::u16vec2> UV = ch->liquidManager->getLiquidVertexData(voxelToLiquidID(v.id), modPos/4.f, glm::vec2(0.25));

			for(int dir = 0; dir < 6; dir++) {
				unsigned int key = makeFaceKey(position,dir);

				if(faceNotObstructed(surroundingVoxels,dir)) { // Check if the bordering voxel can be seen through
					Quad<WaterVertex> face;
					AmbientOcclusion occlusion[4];
					getVoxelAO(ch, position - LocalCoordinate(0,1,0), dir, occlusion);

					bool tri = occlusion[0]._value + occlusion[2]._value
							 < occlusion[1]._value + occlusion[3]._value;

					for(int i = 0; i < 4; i++) {
						if(!(memb & 1<<vertexIDs[dir][i])) { // check if the memoized value has been created
							memb |= 1<<vertexIDs[dir][i];
							//LocalCoordinate vertexPos = faces[dir][i];
							// The color that the noise outputted for this vertex
							Color vColor = v.color;
							memc[vertexIDs[dir][i]] = vColor.toGPUColor();
						}
						// Finally, create the vertex
						face[(i + (int)tri)%4] = WaterVertex{gpuLocalCoordinate(position + faces[dir][i]), normals[dir], UV[i], memc[vertexIDs[dir][i]], (uint8_t)v.meta, occlusion[i], tangents[dir], bitangents[dir], v.vlight};
					}
					// add our new face to the buffer
					ch->buf->wBuffer->add(key, face);
					ch->buf->wTriBuffer->add(key, tri);
				} else if (!INITIAL) {
					// The bordering voxel may have overridden this face, in which case, remove it from the buffer
					ch->buf->wBuffer->remove(key);
					ch->buf->wTriBuffer->remove(key);
					// ch->buf->buffer->remove(key);
					ch->buf->triBuffer->remove(key);
				}
			}

		} else {
			char memb = 0;
			Color::GPUColor memc[8];

			Color::GPUColor col = BROWN.toGPUColor();

			for(int dir = 0; dir < 6; dir++) {
				unsigned int key = makeFaceKey(position,dir);

				if(faceNotObstructed(surroundingVoxels, dir)) { // Check if the bordering voxel can be seen through
					Quad<ChunkVertex> face;

					AmbientOcclusion occlusion[4];
					getVoxelAO(ch, position, dir, occlusion);

					bool tri = occlusion[0]._value + occlusion[2]._value
							 < occlusion[1]._value + occlusion[3]._value;

					for(int i = 0; i < 4; i++) {
						if(!(memb & 1<<vertexIDs[dir][i])) { // check if the memoized value has been created
							memb |= 1<<vertexIDs[dir][i];
							LocalCoordinate vertexPos = faces[dir][i];
							// The color that the noise outputted for this vertex
							Color vColor = getVertexColor(v, ch->colorNoise, position, vertexPos, ch->worldPos);
							memc[vertexIDs[dir][i]] = vColor.toGPUColor();
						}
						// Finally, create the vertex
						face[(i + (int)tri)%4] = ChunkVertex{gpuLocalCoordinate(position + faces[dir][i]), col, occlusion[i], normals[dir], v.vlight};
					}
					// add our new face to the buffer
					//ch->buf->buffer->add(key, face);
					ch->buf->triBuffer->add(key, tri);
				} else if(!INITIAL) {
					// The bordering voxel may have overridden this face, in which case, remove it from the buffer
					// ch->buf->buffer->remove(key);
					ch->buf->triBuffer->remove(key);
					ch->buf->wBuffer->remove(key);
					ch->buf->wTriBuffer->remove(key);
				}
			}
		}*/
	}

	void ChunkMesh::createVertices(const LocalCoordinate& position, Voxel v) {
		createChunkMeshVertices<false>(this, position, v, liquidManager);
	}
	void ChunkMesh::createVertices_INITIAL(const LocalCoordinate& position, Voxel v) {
		createChunkMeshVertices<true>(this, position, v, liquidManager);
	}

	template<bool INITIAL> void createEntityMeshVertices(EntityMesh* e, const LocalCoordinate& position, Voxel v) {
		/*unsigned char memb = 0;
		Color::GPUColor memc[8];
		for(int dir = 0; dir < 6; dir++) {
			Voxel bordering = e->get(position + faceOffsets[dir]);
			unsigned int key = makeFaceKey(position,dir);

			if(bordering.is(Voxel::TRANSPARENT | Voxel::IMAGINARY)) { // Check if the bordering voxel can be seen through
				//Quad<EntityVertex> face;

				AmbientOcclusion occlusion[4];
				getVoxelAO(e, position, dir, occlusion);

				bool tri = occlusion[0]._value + occlusion[2]._value
						 < occlusion[1]._value + occlusion[3]._value;

				for(int i = 0; i < 4; i++) {
					if(!(memb & 1<<vertexIDs[dir][i])) {
						memb |= 1<<vertexIDs[dir][i];
						LocalCoordinate vertexPos = faces[dir][i];
						Color vColor = getVertexColor(v, e->colorNoise, position, vertexPos);
						memc[vertexIDs[dir][i]] = vColor.toGPUColor();
					}
					// Finally, create the vertex
					//face[(i + (int)tri)%4] = EntityVertex{gpuLocalCoordinate(position + faces[dir][i]), memc[vertexIDs[dir][i]], occlusion[i], normals[dir], v.vlight};
				}
				// add our new face to the buffer
				//e->buf->buffer->add(key, face);
			} else if(!INITIAL) {
				// The bordering voxel may have overridden this face, in which case, remove it from the buffer
				//e->buf->buffer->remove(key);
			}
		}*/
	}

	void EntityMesh::createVertices(const LocalCoordinate& position, Voxel v) {
		createEntityMeshVertices<false>(this, position, v);
	}
	void EntityMesh::createVertices_INITIAL(const LocalCoordinate& position, Voxel v) {
		createEntityMeshVertices<true>(this, position, v);
	}

	void EntityMesh::generateMesh() {
		buf->globalNotGlobalPalette = new TerrainPalette(1); // size = 1, as this is the only thing in the palette

		std::vector<SubVoxel> thisData(size.x*size.y*size.z);
		for(unsigned int i = 0; i < thisData.size(); i++) {
			SubVoxel subVox;
			Voxel thisVox = voxelMap->get(getVoxelArray()[i]);
			subVox.color = thisVox.color;
			subVox.exists = !(thisVox.is(Voxel::IMAGINARY));
			subVox.lighting = glm::vec3(thisVox.vlight._value)/127.f;
			thisData[i] = subVox;
		}

		TerrainPaletteVoxelMesh* tMesh = new TerrainPaletteVoxelMesh(size, thisData);
		BasicTerrainPaletteVoxel* tPVoxel = new BasicTerrainPaletteVoxel{};
		tPVoxel->theMesh = tMesh;
		buf->globalNotGlobalPalette->addVoxel(tPVoxel);

		buf->globalNotGlobalPalette->makeUVMap();

		TerrainPaletteMeshBuffer* _buf = tMesh->buf;
		for(auto v:_buf->verts) {
			LocalCoordinate nPos = v.position;
			TerrainPaletteUV uv(v.uv._uv);
			TerrainPaletteVertex nVert = packTerrainPaletteVertex(nPos, v.normalID, uv, 0.f);
			buf->buffer->push(nVert);
		}
		for(auto ind:_buf->inds) {
			buf->entityIndexBuffer->push(ind);
		}
	}







	// Greedy meshing function -- independent from other voxel meshes' generateMesh function
	void TerrainPaletteVoxelMesh::generateMesh() {
		std::vector<TerrainPaletteVertexPhase1> verts;

		int help[6] = {0,1,2,2,3,0};
		for(auto poly:gMesh) {
			for(auto q:poly.components) {
				for(int i = 0; i < 6; i++) {
					//gpu10_10_10_2 normal = gpu10_10_10_2::gpuPackedNormal(normals[q[help[i]].direction]);
					int normalID = q[help[i]].direction;
					LocalCoordinate coord(q[help[i]].position);
					TerrainPaletteUV uv(  glm::i16vec2(q[help[i]].polygonPosition)  );
					TerrainPaletteVertex nVert = packTerrainPaletteVertex(coord, normalID, uv, 0.f);
					verts.push_back({coord,normalID,uv});
				}
			}
		}

		std::vector<TerrainPaletteVertexPhase1> verts_indexed;
		std::vector<uint32_t> inds;
		makeIndexBuffer(verts,verts_indexed,inds);

		buf = new TerrainPaletteMeshBuffer{verts,verts_indexed,inds};
	}

	void ChunkMesh::generateMesh() {

		// set null index value (index 0 corresponds to garbage)
		curIndex = 1;
		buf->buffer->push(TerrainPaletteVertex{
			gpu10_10_10_2::gpuPackedUnsigned(glm::uvec3{0,0,0},0),
			gpu10_10_10_2::gpuPackedUnsigned(glm::uvec3{0,0,0},0)
		});

		for(int x = 0; x < size.x; x++) {
			for(int y = 0; y < size.y; y++) {
				for(int z = 0; z < size.z; z++) {
					LocalCoordinate pos(x,y,z);
					getVertices(pos, true);
				}
			}
		}

	}

	TerrainPaletteVoxelMesh::TerrainPaletteVoxelMesh(LocalCoordinate size, std::vector<SubVoxel> data): size(size) {
		LocalCoordinate sizePpad(size+2*padding);
		voxels = std::vector<SubVoxel>(sizePpad.x*sizePpad.y*sizePpad.z);
		for(int z = 1; z < sizePpad.z-1; ++z)
		for(int y = 1; y < sizePpad.y-1; ++y)
		for(int x = 1; x < sizePpad.x-1; ++x) {
			voxels[x+sizePpad.x*(y+sizePpad.y*z)] = data[(x-1)+size.x*( (y-1) + size.y*(z-1) )];
		}
		buf = nullptr;
		ID = 0;

		std::vector<bool> bVoxels(voxels.size());
		for(unsigned int i = 0; i < bVoxels.size(); i++) bVoxels[i] = voxels[i].exists;

		gMesh = constructGreedyMesh(voxels,bVoxels,size,padding);
	}


	TerrainPaletteVoxelMesh::TerrainPaletteVoxelMesh(LocalCoordinate size, std::vector<SubVoxel> data, int pad): size(size), voxels(data), padding(pad) {
		buf = nullptr;
		ID = 0;

		std::vector<bool> bVoxels(voxels.size());
		for(unsigned int i = 0; i < bVoxels.size(); i++) bVoxels[i] = voxels[i].exists;

		gMesh = constructGreedyMesh(voxels,bVoxels,size,padding);
	}



	void ChunkMesh::getVertices(const LocalCoordinate& pos, bool initial) {
		// Voxel v = get(position);

		//chunkMeshRemCubeMarch(this, position);
		//chunkmeshMarchCubes(this,position,v); // very necessary! :3

		// if(!v.is(Voxel::IMAGINARY)) {
		// 	createVertices(position, v);
		// }
		// else deleteVertices(position);

		VoxelSurroundings surround = getSurroundingVoxels(pos,this);
		char surrBits = getSurroundingBits(surround, voxelMap, Voxel::TRANSPARENT);

		VoxelCoordinate voxelWorldPos = worldToVoxelCoordinate(worldPos) + pos;

		if(surrBits == 0x0) return;

		LocalCoordinate posMul(pos*GRIDVOXEL_SIZE);
		TerrainPaletteVoxelMesh* mesh = actualPalette->get( getVoxelID(pos), surround, voxelMap );
		if(!mesh->exists) return;
		TerrainPaletteMeshBuffer* _buf = mesh->buf;

		buf->cpuPointerArray[glm::u8vec3(pos)] = TerrainPaletteVoxelPointer{buf->chunkIndexBuffer->size,_buf->inds.size()};
		for(auto ind:_buf->inds) {
			buf->chunkIndexBuffer->push(ind+curIndex);
		}

		for(auto v:_buf->verts) {
			LocalCoordinate nPos = v.position+posMul;
			TerrainPaletteVertex nVert = packTerrainPaletteVertex(nPos, v.normalID, v.uv._uv, 0.f);
			buf->buffer->push(nVert);
		}
		int numVerts = _buf->verts.size();
		curIndex += numVerts;

		// update collision units
		if(!initial) {
			char surroundSolid = getSurroundingBits(surround, voxelMap, Voxel::SOLID);

			if(surroundSolid != 63) {
				Voxel thisVoxel = unsafeGet(pos);
				if(!thisVoxel.is(Voxel::SOLID)) {

					auto un = buf->gridVoxelCollisionMesh->getUnit(pos);

					for(unsigned int i = 0; i < un->solidVoxels.size(); ++i) {
						if(un->solidVoxels[i] == voxelWorldPos) {
							un->solidVoxels.erase(un->solidVoxels.begin() + i);
						}
					}

				}
			}
		}

		Voxel thisVoxel = unsafeGet(pos);
		if(thisVoxel.is(Voxel::SOLID)) {
			auto un = buf->gridVoxelCollisionMesh->getUnit(pos);
			un->solidVoxels.push_back(voxelWorldPos);
		}

	}

	const int CPUGridVoxelCollisionMeshUnit::SIZE = 4;
	const int CPUGridVoxelCollisionMesh::PER_CHUNK = 8;

	CPUGridVoxelCollisionMeshUnit* CPUGridVoxelCollisionMesh::getUnitOverSize(glm::ivec3 position) {
		return units + (position.x + PER_CHUNK * (position.y + PER_CHUNK * position.z) );
	}

	CPUGridVoxelCollisionMeshUnit* CPUGridVoxelCollisionMesh::getUnit(LocalCoordinate position) {
		return getUnitOverSize( position / CPUGridVoxelCollisionMeshUnit::SIZE );
	}

	std::vector<CPUGridVoxelCollisionMeshUnit*> CPUGridVoxelCollisionMesh::getUnits(LocalCoordinate start, LocalCoordinate size) {
		LocalCoordinate end = start + size;
		end = glm::min(end, LocalCoordinate(Chunk::SIZE-1));
		start = glm::max(start, LocalCoordinate(0));

		glm::ivec3 startOverSize = start / CPUGridVoxelCollisionMeshUnit::SIZE;
		glm::ivec3 endOverSize = end / CPUGridVoxelCollisionMeshUnit::SIZE;

		std::vector<CPUGridVoxelCollisionMeshUnit*> ret;
		glm::ivec3 curPos;
		for(curPos.z = startOverSize.z; curPos.z <= endOverSize.z; curPos.z++)
		for(curPos.y = startOverSize.y; curPos.y <= endOverSize.y; curPos.y++)
		for(curPos.x = startOverSize.x; curPos.x <= endOverSize.x; curPos.x++) {
			ret.push_back(getUnitOverSize(curPos));
		}
		return ret;
	}

}

