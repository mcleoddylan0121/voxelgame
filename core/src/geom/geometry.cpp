/*
 * geometry.cpp
 *
 *  Created on: Mar 23, 2015
 *      Author: Dylan
 */

#include "geometry.h"
#include "console.h"
#include "chunk.h"
#include "util.h"
#include "io.h"
#include "filesystem.h"
#include "vlua.h"
#include "vluautil.h"
#include "resourcemanager.h"

#include <map>
#include <cstring>
#include <cmath>
#include <algorithm>
#include <cstdint>
#include <vector>


namespace vox {

	Palette* errorPalette;

	VoxelMeshBuffer::indexBufferType* VoxelMeshBuffer::indexBuffer;

	const float LightLevel::MAX = Chunk::SIZE - 1;
	const int LightLevel::I_MAX = LightLevel::MAX;

	Normal::Normal(glm::vec3 normal) {
		glm::vec3 normalizedInput = glm::normalize(normal);
		_normal = glm::i8vec3(normalizedInput.x * 127, normalizedInput.y * 127, normalizedInput.z * 127);
	}

	Voxel::Voxel(Color color, char attribs, unsigned int meta, LightLevel llevel, VoxelLight vlight, std::string name, VoxelID id):
			color(color), light(llevel), flags(attribs), meta(meta), vlight(vlight), name(name), id(id) { }
	Voxel::~Voxel() {}
	Voxel::Voxel(const Voxel& v): Voxel(v.color, v.flags, v.meta, v.light, v.vlight, v.name, v.id) {}

	float rad(glm::vec3 scale) {
		return sqrt(scale.x * scale.x + scale.y * scale.y + scale.z * scale.z) / 2.f;
	}

	float distance(WorldCoordinate a, WorldCoordinate b) {
		return sqrt(squareDistance(a, b));
	}
	float squareDistance(WorldCoordinate a, WorldCoordinate b) {
		return (a.x-b.x) * (a.x-b.x) + (a.y-b.y) * (a.y-b.y) + (a.z-b.z) * (a.z-b.z);
	}
	float manhattanDistance(WorldCoordinate a, WorldCoordinate b) {
		return abs(a.x - b.x) + abs(a.y - b.y) + abs(a.z - b.z);
	}
	float squareLength(WorldCoordinate w) {
		return w.x * w.x + w.y * w.y + w.z * w.z;
	}
	float vecMin(WorldCoordinate a) {
		return std::min(a.x, std::min(a.y, a.z));
	}
	WorldCoordinate vecReplace(WorldCoordinate a, float in, float out) {
		for (int i = 0; i < 3; i++)
			if (a[i] == in) a[i] = out;
		return a;
	}
	WorldCoordinate vecReplaceExclusive(WorldCoordinate a, float in, float out) {
		for (int i = 0; i < 3; i++)
			if (a[i] != in) a[i] = out;
		return a;
	}

	glm::vec3 directionToEulerAngles(glm::vec3 dir, glm::vec3 up) {
		dir = glm::normalize(dir);
		up = glm::normalize(up);
		glm::vec3 X = glm::normalize(glm::cross(dir, up));
		glm::vec3 Y = -glm::normalize(glm::cross(dir, X));
		float xy = sqrt(dir.x * dir.x + dir.y * dir.y);
	    if (xy < FLT_EPSILON) {
	    	return glm::vec3(
					atan2(Y.x*dir.y - Y.y*dir.x, X.x * dir.y - X.y * dir.x),
					atan2(xy, dir.z),
					atan2(-dir.x, dir.y)
	        );
	    }
	    else {
	    	return glm::vec3(0, dir.z > 0? 0:PI, -atan2(X.y, X.x));
	    }
	}

	WorldCoordinate getPointInSphere(const WorldCoordinate& pos, float minrad, float maxrad, Rand& rng) {
		float maxsqr = maxrad * maxrad;
		float minsqr = minrad * minrad;
		while(true) {
			WorldCoordinate tPos = {rng.getFloat(-maxrad, maxrad), rng.getFloat(-maxrad, maxrad), rng.getFloat(-maxrad, maxrad)};
			float distsqr = squareLength(tPos);
			if(distsqr < maxsqr && distsqr > minsqr) return pos + tPos;
		}
		return {0,0,0}; // stahp wining c++ i dont liek it
	}

	WorldCoordinate getPointOnSphere(const WorldCoordinate& pos, float rad, Rand& rng) {
		WorldCoordinate ang = {rng.getFloat(-1,1), rng.getFloat(-1,1), rng.getFloat(-1,1) };
		return pos + glm::normalize(ang) * rad;
	}

	WorldCoordinate getPointInSpheroid(const WorldCoordinate& pos, const WorldCoordinate& rad, Rand& rng) {
		while(true) {
			WorldCoordinate tPos = {rng.getFloat(-rad.x, rad.x), rng.getFloat(-rad.y, rad.y), rng.getFloat(-rad.z, rad.z)};
			float distsqr = tPos.x * tPos.x / (rad.x * rad.x) + tPos.y * tPos.y / (rad.y * rad.y) + tPos.z * tPos.z / (rad.z * rad.z);
			if(distsqr < 1) return pos + tPos;
		}
		return {0,0,0}; // stahp wining c++ i dont liek it
	}

	WorldCoordinate getPointOnSpheroid(const WorldCoordinate& pos, const WorldCoordinate& rad, Rand& rng) {
		WorldCoordinate ang = {rng.getFloat(-1,1), rng.getFloat(-1,1), rng.getFloat(-1,1) };
		return pos + glm::normalize(ang) * rad;
	}

	Palette::Palette(size_t size): size(size) {
		_map = new Voxel[size];
		_map[0] = Voxel(BLACK, Voxel::IMAGINARY);
	}

	Palette::~Palette() {
		delete[] _map;
	}

	void Palette::set(Voxel v, VoxelID id) { _map[id] = v; }
	void Palette::setMultiple(Voxel* v, int len, int offs) {
		for (int i = 0; i < len; i++)
			set(v[i], i + offs);
	}

	int Palette::getSize() { return size; }

	Voxel Palette::get(VoxelID id) const { return id < size? _map[id]: _map[0]; }

	void Palette::init() {
		errorPalette = new Palette(2);
		Voxel air = Voxel({0,0,0,0}, Voxel::IMAGINARY | Voxel::TRANSPARENT);
		Voxel pink = Voxel({255.f/255.f,105.f/255.f,180.f/255.f});
		pink.set(Voxel::SOLID);

		errorPalette->set(air, 0);
		errorPalette->set(pink, 1);
	}

	glm::mat4 makeBillboardMatrix(glm::mat4 MV) {
		glm::mat4 ret(MV);
		ret[0] = {1,0,0,MV[0][3]};
		ret[1] = {0,1,0,MV[1][3]};
		ret[2] = {0,0,1,MV[2][3]};
		return ret;
	}

	// why does this need to exist
	struct LuaVoxel: public LuaObject {
		Voxel v;
		int ID;
		LuaVoxel(std::vector<std::shared_ptr<LuaPair>> map = {}): LuaObject(map) { ID = 0; }
		virtual ~LuaVoxel() {}

		void init(LuaScript& ls, const std::string& varName) {
			ID = ls.get("ID", 0);
			int transparent = ((int)ls.get<bool>("flags.transparent", false)) * Voxel::TRANSPARENT;
			int solid = ((int)ls.get<bool>("flags.solid", true)) * Voxel::SOLID;
			int imaginary = ((int)ls.get<bool>("flags.imaginary", false)) * Voxel::IMAGINARY;
			int lightSource = ((int)ls.get<bool>("flags.lightSource")) * Voxel::LIGHT_SOURCE;
			int liquid = ((int)ls.get<bool>("flags.liquid")) * Voxel::LIQUID;
			unsigned int flags = transparent | solid | imaginary | liquid | lightSource;

			lvec4 color = ls.get<lvec4>("color", lvec4(glm::vec4(1,1,1,1)));
			lvec4 lcolor = ls.get<lvec4>("lightcolor", lvec4(glm::vec4(0,0,0,1)));

			std::string name = ls.get<std::string>("name", "unnamed voxel");
			Color ucolor = Color(color);
			glm::vec3 lcolora = {lcolor.x, lcolor.y, lcolor.z};
			LightLevel llev = LightLevel::fromRGBValue(lcolora);
			int meta = ls.get<int>("meta", 0);

			lvec4 vlcolor = ls.get<lvec4>("voxellight", lvec4(glm::vec4(0,0,0,1)));
			glm::vec3 vlcolora = {vlcolor.x, vlcolor.y, vlcolor.z};
			VoxelLight vl(vlcolora);

			v = Voxel(ucolor, flags, meta, llev, vl, name, ID);
		}
	};

	Palette* Palette::allocate(const Asset& a) {
		LuaScript* script = mRes->getResource<LuaScript, Asset::Palette>(a.name);
		std::vector<LuaVoxel> vox = script->getArray<LuaVoxel>("cpplist");
		int len = 0;
		for(auto& i: vox) {
			len = std::max(i.ID+1, len);
		}
		Palette* ret = new Palette(len);
		for(int i = 0; i < len; i++) {
			ret->set(Voxel(), i);
		}
		for(auto i:vox) {
			ret->set(i.v, i.ID);
		}
		return ret;
	}

}

