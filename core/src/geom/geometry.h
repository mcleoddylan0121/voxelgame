/*
 * geometry.h
 *
 *  Created on: Mar 23, 2015
 *      Author: Dylan
 */

#ifndef GEOMETRY_H_
#define GEOMETRY_H_

#include "colormath.h"
#include "coords.h"

namespace vox {

	typedef uint16_t VoxelID;

	struct Asset;

	class Program; class Camera; class ChunkShaderProgram; class EntityShaderProgram;
	class ChunkMap; class OrthoCamera;

	WorldCoordinate getPointInSphere(const WorldCoordinate& pos, float minrad, float maxrad, Rand& rng);
	WorldCoordinate getPointOnSphere(const WorldCoordinate& pos, float rad, Rand& rng);

	WorldCoordinate getPointInSpheroid(const WorldCoordinate& pos, const WorldCoordinate& rad, Rand& rng);
	WorldCoordinate getPointOnSpheroid(const WorldCoordinate& pos, const WorldCoordinate& rad, Rand& rng);

	float rad(glm::vec3 scale);
	inline float sum(glm::vec3 v) { return v.x + v.y + v.z; }
	float distance(WorldCoordinate a, WorldCoordinate b); // Returns the square root of squareDistance
	float squareDistance(WorldCoordinate a, WorldCoordinate b); // Returns the distance between two points squared
	float manhattanDistance(WorldCoordinate a, WorldCoordinate b); // Use this distance for comparative stuffs. It's fast!
	float vecMin(WorldCoordinate a); // Finds the minimum of all the components of a
	WorldCoordinate vecReplace(WorldCoordinate a, float in, float out); // Replaces all instances of in with out
	WorldCoordinate vecReplaceExclusive(WorldCoordinate a, float notIn, float out); // Replaces all instances not "notIn" with out

	glm::vec3 directionToEulerAngles(glm::vec3 dir, glm::vec3 up);

	float squareLength(WorldCoordinate val);

	inline void decomposeMatrix(glm::mat4 in, glm::vec3& pos) {
		pos.x = in[3][0];
		pos.y = in[3][1];
		pos.z = in[3][2];
	}

	struct LightLevel {
		glm::i8vec3 _level;
		static const float MAX;
		static const int I_MAX;
		inline LightLevel(glm::i8vec3 level): _level(level) {}

		inline VoxelID getVoxelID() const { return (1 << 15) + (_level.x << 10) + (_level.y << 5) + _level.z; }
		inline operator VoxelID() const { return getVoxelID(); }

		// I don't recommend you use this, except when dealing with color constants
		static inline LightLevel fromLABColor(Color color) {
			return fromRGBValue(glm::vec3(color.toGPUColor()) / 255.f);
		}

		static inline LightLevel fromVoxelID(VoxelID v) {
			return LightLevel( // if the MSD isn't one, then this is solid
				glm::i8vec3((v >> 10) % 32, (v >> 5) % 32, v % 32) * (int8_t) ((v & (1<<15)) >> 15)
			);
		}

		static inline LightLevel fromRGBValue(glm::vec3 rgb) {
			rgb = glm::clamp(rgb, glm::vec3(0), glm::vec3(1));
			return LightLevel(glm::i8vec3(rgb * MAX));
		}

		inline glm::vec3 getRGBValue() const  {
			return glm::vec3(_level) / MAX;
		}
	};

	// constexpr because i'm lazy
	constexpr float voxelLightMax = 4; // things can have a voxel light of up to +- glm::vec3(4)
	struct VoxelLight {
		glm::i8vec3 _value;
		VoxelLight(glm::vec3 value = {0,0,0}): _value(glm::clamp(value,glm::vec3(-1),glm::vec3(1))*(127.f/voxelLightMax)) {}
		VoxelLight(const VoxelLight& other): _value(other._value) {}
		void operator =(const VoxelLight& other) { _value = other._value; }
	};

	struct Voxel {
		Color color;
		LightLevel light; // 0, unless this is a lamp (why isn't this a lamp? what do you have against lamps?)

		uint32_t flags; // Using our own implementation because bitsets are limited in use
		uint32_t meta;
		VoxelLight vlight; // does not spread, unlike light levels, added to light level. Entity/liquid-enabled.
		std::string name;

		VoxelID id;

		enum Flag: char {
			TRANSPARENT = 1, // This voxel can be seen through
			SOLID = 2,  	 // Entities can collide with this voxel
			IMAGINARY = 4,   // This voxel does not have any drawn faces
			LIGHT_SOURCE = 8,// This voxel's base light level > 0
			LIQUID = 16
		};

		bool is(char bits) const { return flags & bits; }
		void set(char bits) { flags |= bits; }

		Voxel(Color color = BLACK, char attribs = 0, unsigned int meta = 0, LightLevel llevel = LightLevel({0,0,0}), VoxelLight vlight = VoxelLight({0,0,0}), std::string name = "", VoxelID id = 0);
		Voxel(const Voxel& v);
		~Voxel();

		// Lazy enum
		static const int NORTH_FACE = 0, SOUTH_FACE = 1, EAST_FACE = 2, WEST_FACE = 3, UP_FACE = 4, DOWN_FACE = 5;

	};

	// Includes color and lighting, may contain more data in the future
	// This data will be put into textures for shading
	struct SubVoxel {
		bool exists = false;
		Color color = WHITE;
		glm::vec3 lighting = glm::vec3(0);
	};

	class Palette {
	protected:
		size_t size;
	public:
		Voxel* _map;

		Palette(size_t size);
		virtual ~Palette();

		void set(Voxel v, VoxelID id);
		void setMultiple(Voxel* v, int len, int offs=0); // Set many voxels!
		Voxel get(VoxelID id) const;

		int getSize();

		static void init();

		static Palette* allocate(const Asset& a);
	};

	extern Palette* errorPalette;

	inline LightLevel getLightLevelfromVoxelAndID(VoxelID id, const Voxel& v) {
		return LightLevel(v.light._level + LightLevel::fromVoxelID(id)._level);
	}

	bool isInsideViewFrustrum(glm::mat4 MVP);

	glm::mat4 makeBillboardMatrix(glm::mat4 MV);


	struct PaletteVoxelUVMapData {
		glm::u8vec4 light, color;
	};

}


#endif /* GEOMETRY_H_ */
