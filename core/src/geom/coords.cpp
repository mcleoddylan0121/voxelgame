/*
 * coords.cpp
 *
 *  Created on: Jun 10, 2015
 *      Author: Dani
 */

#include "coords.h"
#include "chunk.h"

#include <cstring>

namespace vox {

	ChunkCoordinate worldToChunkCoordinate(const WorldCoordinate& c) {
		return ChunkCoordinate(floor(c.x / Chunk::SIZE), floor(c.y / Chunk::SIZE), floor(c.z / Chunk::SIZE));
	}
	ChunkCoordinate voxelToChunkCoordinate(const VoxelCoordinate& c) {
		return worldToChunkCoordinate(voxelToWorldCoordinate(c));
	}
	WorldCoordinate chunkToWorldCoordinate(const ChunkCoordinate& c) { return WorldCoordinate(c.x * Chunk::SIZE, c.y * Chunk::SIZE, c.z * Chunk::SIZE); }
	LocalCoordinate worldToLocalChunkCoordinate(const WorldCoordinate& c) {
		return voxelToLocalChunkCoordinate(worldToVoxelCoordinate(c));
	}

	VoxelCoordinate worldToVoxelCoordinate(const WorldCoordinate& c) {
		return VoxelCoordinate(glm::floor(c));
	}
	WorldCoordinate voxelToWorldCoordinate(const VoxelCoordinate& c) {
		return WorldCoordinate(c);
	}
	VoxelCoordinate chunkToVoxelCoordinate(const ChunkCoordinate& c) {
		return c * Chunk::SIZE;
	}
	LocalCoordinate voxelToLocalChunkCoordinate(const VoxelCoordinate& c) {
		return c - voxelToChunkCoordinate(c) * Chunk::SIZE;
	}

	bool ivec3sort::operator () (const glm::ivec3& a, const glm::ivec3& b) const {
		return memcmp((void*)&a, (void*)&b, sizeof(glm::ivec3)) > 0;
	}

}
