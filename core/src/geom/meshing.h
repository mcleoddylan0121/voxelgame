/*
 * meshing.h
 *
 *  Created on: Jun 29, 2016
 *      Author: dylan
 */

// Everything here happens in a header file because it will be templatized
// And because it is only ever included by source files

#ifndef SRC_GEOM_MESHING_H_
#define SRC_GEOM_MESHING_H_
#include "geometry.h"
#include "console.h"
#include "uvmapping.h"
#include <algorithm>
#include <vector>
#include <cstring>
#include <map>

namespace vox {
	/* Monotone meshing: outputs triangles, probably not a good idea overall (WIP) */
	/*
	struct MonotoneVertex {
		glm::ivec3 pos;
		bool operator == (MonotoneVertex other) { return pos == other.pos; }
		bool operator != (MonotoneVertex other) { return !(this == other); }
	};

	struct MonotoneLine {
		MonotoneVertex first, second;
	};

	// I'm gonna be honest. I have no idea what a monotone polygon is.
	struct MonotonePolygon {
		std::vector<MonotoneLine> left, right;
		void closeOff(MonotoneVertex v) {
			left. push_back({left. back().first,v});
			right.push_back({right.back().first,v});
		}

		void mergeRun(MonotoneVertex v, MonotoneVertex u_l, MonotoneVertex u_r) {
			MonotoneVertex l = left.back().first, r = right.back().first;
			if(l != u_l) {

			}
		}
	};*/

	template<typename T>
	bool voxelAccess(std::vector<T>& voxels, glm::ivec3 pos, glm::ivec3 sizePpad, int pad) {
		pos += pad;
		return voxels[pos.x + sizePpad.x * (pos.y + sizePpad.y * pos.z)].exists;
	}

	template<typename T>
	T voxelAccess2(std::vector<T>& voxels, glm::ivec3 pos, glm::ivec3 sizePpad, int pad) {
		pos += pad;
		return voxels[pos.x + sizePpad.x * (pos.y + sizePpad.y * pos.z)];
	}

	const int greedyMeshNormals[6] = {
			3,2,5,4,1,0
	};

	struct GreedyMeshVertex {
		glm::ivec3 position;
		int direction;

		glm::ivec2 polygonPosition; // position of this vertex on the parent PaletteVoxelTexturePolygon, becomes position on texture atlas
	};

	struct PaletteVoxelTexturePolygon {
		glm::ivec3 position;
		glm::ivec2 size;

		UVMapComponent<PaletteVoxelUVMapData>* uvMapComp; // The position of this on the UV map

		// Used to aid in construction
		glm::ivec2 planeStart = glm::ivec2(256), planeEnd = glm::ivec2(0);

		char maskValue;

		// The data of all the sub-voxels contained by this polygon
		std::vector<SubVoxel> textureData;

		std::vector<Quad<GreedyMeshVertex>> components; // All of the quads that make up this polygon (TODO: make triangles)
	};

	// This is a bit expensive, sorry about that friend
	inline void DFS(PaletteVoxelTexturePolygon* ret, std::vector<char>* mask, glm::ivec2 size, glm::ivec2 pos, char initVal) {

		// continue search
		if((*mask)[pos.x+pos.y*size.x] == initVal) {
			(*mask)[pos.x+pos.y*size.x] = 0; // Set this as 0 so we don't traverse back on ourselves

			ret->textureData[pos.x + pos.y*size.x].exists = true; // Set this as existing, because we just found it

			// if this exceeds any of the previous dimensions, we expand the polygon
			if(pos.x < ret->planeStart.x) ret->planeStart.x = pos.x;
			if(pos.y < ret->planeStart.y) ret->planeStart.y = pos.y;

			if(pos.x > ret->planeEnd.x)   ret->planeEnd.x =   pos.x;
			if(pos.y > ret->planeEnd.y)   ret->planeEnd.y =   pos.y;

			// run DFS on all of our neighbors
			if(pos.x > 0) DFS(ret,mask,size,glm::ivec2(pos.x-1,pos.y),initVal);
			if(pos.x < size.x-1) DFS(ret,mask,size,glm::ivec2(pos.x+1,pos.y),initVal);

			if(pos.y > 0) DFS(ret,mask,size,glm::ivec2(pos.x,pos.y-1),initVal);
			if(pos.y < size.y-1) DFS(ret,mask,size,glm::ivec2(pos.x,pos.y+1),initVal);
		}

	}

	// find all of the continuous polygons (parts of the mask with the same, nonzero value
	inline std::vector<PaletteVoxelTexturePolygon> getConnectivity(std::vector<char> mask, glm::ivec2 size) {
		std::vector<PaletteVoxelTexturePolygon> ret;

		for(int x = 0; x < size.x; x++)
		for(int y = 0; y < size.y; y++) {
			glm::ivec2 pos(x,y);

			char maskVal = mask[x+y*size.x];
			if(maskVal == 0) continue; // empty

			PaletteVoxelTexturePolygon thisPoly;
			thisPoly.textureData = std::vector<SubVoxel>(size.x*size.y);
			thisPoly.maskValue = maskVal;

			DFS(&thisPoly, &mask, size, pos, maskVal);

			thisPoly.size = thisPoly.planeEnd-thisPoly.planeStart+glm::ivec2(1);

			// Remap the data from the size of the plane to the size of the polygon
			std::vector<SubVoxel> nData = std::vector<SubVoxel>(thisPoly.size.x * thisPoly.size.y);

			for(int xx = 0; xx < thisPoly.size.x; xx++)
			for(int yy = 0; yy < thisPoly.size.y; yy++) {
				nData[xx+yy*thisPoly.size.x] = thisPoly.textureData[(xx+thisPoly.planeStart.x)+(yy+thisPoly.planeStart.y)*size.x];
			}

			thisPoly.textureData.clear();
			thisPoly.textureData = nData;

			ret.push_back(thisPoly);
		}

		return ret;
	}

	inline std::vector<PaletteVoxelTexturePolygon> makePaletteVoxelTexturePolygons(std::vector<char> mask, glm::ivec2 sizeX,
			int& d, int& u, int& v, glm::ivec3& x, glm::ivec3& q, std::vector<SubVoxel> voxels, glm::ivec3 sizePpad, int pad) {
		std::vector<PaletteVoxelTexturePolygon> ret = getConnectivity(mask,sizeX);

		for(auto &poly:ret) {
			int i,j,w,h,k;
			int n = 0;


			int nrml = (poly.maskValue & 2)>>1;
			int nm = greedyMeshNormals[d*2+nrml];

			std::vector<bool> mmask(poly.size.y*poly.size.x);

			for(unsigned int kkk = 0; kkk < mmask.size(); ++kkk) {
				mmask[kkk] = poly.textureData[kkk].exists;
			}

			for(j = 0; j < poly.size.y; j++)
			for(i = 0; i < poly.size.x;) {

				if(mmask[n]) {

					// Trigger warning: Loop with no body; goto

					// Compute width
					for(w=1; mmask[n+w] && i+w<poly.size.x; ++w);

					// Compute height
					for(h=1; j+h<poly.size.y; ++h) {
						for(k=0; k<w; ++k) {
							if(!(mmask[n+k+h*poly.size.x])) goto endGreedyMeshHeightComp;
						}
					}endGreedyMeshHeightComp:;

					// Add quad
					x[u] = i+poly.planeStart.x; x[v] = j+poly.planeStart.y;
					glm::ivec3 du(0); du[u] = w;
					glm::ivec3 dv(0); dv[v] = h;

					if(nrml) std::swap(du,dv); // gives the face a different chirality

					glm::ivec2 uv = glm::ivec2(i,j);
					glm::ivec2 di(w,0);
					glm::ivec2 dj(0,h);

					if(nrml) std::swap(di,dj);

					poly.components.push_back(Quad<GreedyMeshVertex>{
						{x,       nm, uv},
						{x+dv,    nm, uv+dj},
						{x+du+dv, nm, uv+di+dj},
						{x+du,    nm, uv+di}
					});

					for(int l=0;l<h;++l)
					for(k=0;k<w;++k) {
						mmask[n+k+l*poly.size.x] = false;
					}

					i += w; n += w;

				} else {
					++i; ++n;
				}

			}



			for(x[v]=poly.planeStart.y; x[v]<=poly.planeEnd.y; ++x[v])
			for(x[u]=poly.planeStart.x; x[u]<=poly.planeEnd.x; ++x[u]) {
				SubVoxel voxe;
				if(nrml) voxe = voxelAccess2<SubVoxel>(voxels,x-q,sizePpad,pad);
				else     voxe = voxelAccess2<SubVoxel>(voxels,x,sizePpad,pad);
				glm::ivec2 polyPos = glm::ivec2(x[u],x[v])-poly.planeStart;

				if(poly.textureData[polyPos.x+polyPos.y*poly.size.x].exists) {
					// Color::GPUColor cl = voxe.color.toGPUColor();
					// printlnf(TEMP, "%i %i %i %i", cl.r,cl.b,cl.g,cl.a);
					poly.textureData[polyPos.x+polyPos.y*poly.size.x] = voxe;
				}
			}

		}

		return ret;
	}


	/* Greedy meshing: outputs quads, and is pretty good
	 *
	 * Word of warning: padding MUST be at least one
	 *
	 */
	inline std::vector<PaletteVoxelTexturePolygon> constructGreedyMesh(std::vector<SubVoxel> voxels, std::vector<bool> bVoxels, glm::ivec3 size, int pad) {
		if(pad < 1) {
			printlnf(FATALERROR, "Unable to construct greedy mesh with given padding! To create a greedy mesh, the padding attribute must be set to one or higher");
			exit(1); // this is a pretty fatal error
		}
		glm::ivec3 sizePpad = size+2*pad;

		std::vector<PaletteVoxelTexturePolygon> ret;

		// I apologize for the mess, but you can't argue with results, man.
		for(int d = 0; d < 3; d++) {
			int u = (d+1)%3,
				v = (d+2)%3;
			glm::ivec3 x(0), q(0);
			std::vector<char> mask(size[u]*size[v]);
			q[d] = 1;
			for(x[d]=-1; x[d]<size[d]; ) {
				int n = 0;
				// Compute mask (see which voxels are exposed)
				bool hasMask = false;
				for(x[v]=0; x[v]<size[v]; ++x[v])
				for(x[u]=0; x[u]<size[u]; ++x[u]) {
					bool _1st = voxelAccess2<bool>(bVoxels,x,sizePpad,pad),
					     _2nd = voxelAccess2<bool>(bVoxels,x+q,sizePpad,pad);
					mask[n++] = (char)(_1st != _2nd) * ((((char)_1st) << 1) + 1);
					hasMask |= _1st != _2nd;
				}

				++x[d];

				if(hasMask) {
					std::vector<PaletteVoxelTexturePolygon> app = makePaletteVoxelTexturePolygons(mask,glm::ivec2(size[u],size[v]),d,u,v,x,q,voxels,sizePpad,pad);
					ret.insert(ret.begin(),app.begin(),app.end());
				}
			}
		}

		return ret;
	}

	template<typename Vertex_T> struct VertSort {
		bool operator()(const Vertex_T& lhs, const Vertex_T& rhs) {
			return memcmp(reinterpret_cast<const void*>(&lhs),reinterpret_cast<const void*>(&rhs),sizeof(Vertex_T)) < 0;
		}
	};

	template<typename Vertex_T, typename Index_T>
	void makeIndexBuffer(const std::vector<Vertex_T>& in,
						 std::vector<Vertex_T>& outVerts,
						 std::vector<Index_T>& outInds) {
		std::map<Vertex_T, Index_T, VertSort<Vertex_T>> soFar;
		int curIndex = 0;
		for(auto v:in) {
			auto found = soFar.find(v);
			if(found == soFar.end()) {
				soFar.emplace(v,curIndex);
				outInds.push_back(curIndex);
				outVerts.push_back(v);
				++curIndex;
			} else {
				outInds.push_back(found->second);
			}
		}
	}

}


#endif /* SRC_GEOM_MESHING_H_ */
