/*
 * shapes.h
 *
 *  Created on: Jun 26, 2015
 *      Author: Daniel
 */

#ifndef SHAPES_H_
#define SHAPES_H_

#include "coords.h"
#include "reports.h"

#include <vector>

namespace vox {

	// A ray can be modeled by the function r = dir * t + origin
	//  where dir and origin are in R^3 and t is in R^1.
	// Rad is the maximum t allowed and thus the length of the ray.
	struct Ray {
		WorldCoordinate origin, dir; // Note: please normalize dir.
		float rad; // Essentially t_max
	};

	// A plane can be modeled by the function normal * (t - origin) = 0
	//  where * denotes dot product and t, normal, and origin are all in R^3.
	// Note this is stored as <a, b, c> = normal and d = normal * (-origin).
	//  Thus, the plane is modeled by <a, b, c> * t + d = 0
	struct Plane {
		WorldCoordinate normal;
		float d;

		// Constructs a plane from the normal {0, 1, 0} and the origin {0, 0, 0}
		Plane();

		// Construct a plane from a normal and an origin. This method the fastest.
		Plane(WorldCoordinate normal, WorldCoordinate origin);

		// Construct a plane from 3 points. P1 serves as the origin.
		Plane(WorldCoordinate p1, WorldCoordinate p2, WorldCoordinate p3);

		// Finds the perpendicular distance from the point to the plane.
		float distance(WorldCoordinate p) const;

		// Checks whether a point lies on the plane
		bool intersects(WorldCoordinate p) const;
	};

	// A rectangular prism
	struct Volume {
		WorldCoordinate pos,  // Pos is the center
						size;
		glm::quat 		rot;
		glm::vec3		rotAnchor; // [0,1]

		Volume();
		Volume(WorldCoordinate pos, WorldCoordinate size);
		Volume(WorldCoordinate pos, WorldCoordinate size, glm::quat rot);
		Volume(WorldCoordinate pos, WorldCoordinate size, glm::quat rot, glm::vec3 rotAnchor);

		// These only work for AABB's
		WorldCoordinate getBottomCorner() const; // Bottom-Left-Back
		WorldCoordinate getTopCorner() const; // Top-Right-Front
		WorldCoordinate getBottomCenter() const;
		WorldCoordinate getTopQuartile() const;

		Volume getTranslation(WorldCoordinate trans) const;
		Volume getBottomCornerChange(WorldCoordinate newCorner) const;

		Volume gridAlligned() const;

		// For OBB's (these are in local space)
		std::vector<glm::vec3> OBB_getAxes() const; // The vector returned is constant and size 3
		std::vector<Plane> OBB_toPlanes() const; // Contains 6 planes.
		glm::vec3 OBB_getExtents() const;
		glm::vec3 OBB_getCenter() const;
		glm::vec3 OBB_rotate(glm::vec3 point) const; // Point must be [-1, 1]

		// DO NOT USE THIS
		Volume getComponentChange(int index, float n) const;

		bool intersects(Volume v) const;
		RayVolumeReport intersects(Ray r) const;
	};

	template<typename T, int size> struct Poly {
		T data[size];
		Poly(std::vector<T> v = std::vector<T>(0)) { for(int i = 0; i < (int)v.size() && i < size; i++) data[i] = v[i]; }
		T& operator [] (size_t index) { return data[index]; }
		T const& operator [] (size_t index) const { return data[index]; }
	};
	template<typename T> struct Line: public Poly<T, 2> { Line(T a = T(), T b = T()) { this->data[0] = a; this->data[1] = b; }};
	template<typename T> struct Triangle: public Poly<T, 3> { Triangle(T a = T(), T b = T(), T c = T()) { this->data[0] = a; this->data[1] = b; this->data[2] = c; }};
	template<typename T> struct Quad: public Poly<T, 4> { Quad(T a = T(), T b = T(), T c = T(), T d = T()) { this->data[0] = a; this->data[1] = b; this->data[2] = c; this->data[3] = d; }};

}

#endif /* SHAPES_H_ */
