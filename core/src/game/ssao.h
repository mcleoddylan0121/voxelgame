/*
 * ssao.h
 *
 *  Created on: Jun 19, 2016
 *      Author: Dylan
 */

#ifndef SRC_GAME_SSAO_H_
#define SRC_GAME_SSAO_H_

#include "framebuffer.h"
#include "program.h"

namespace vox {

	class BlurHandler;

	struct SSAOShaderProgram: public Program {
		GLint tMats;
		GLint gBufferTexture, normalTexture, texNoise, IP, samples, scales, randomSamples;

		SSAOShaderProgram(const std::string& fileName, bool geometryShader = false, std::vector<std::string> macros = {},
				std::vector<std::string> libs = {}, std::vector<std::string> inject = {});
	};

	class SSAOHandler {
	private:
		SSAOShaderProgram* shader;

		ColorBuffer *ssaoOutput;
		bool enableSSAO = true;

		std::vector<glm::vec3> kernel; // all of the positions we sample on each SSAO hemisphere
		std::vector<float> scaleArray; // all of the ranges that we multiply the kernel by
		std::vector<glm::vec3> rotationTexture;
		std::vector<float> randomSampleTexture;

		Texture *gpuRotationTexture, *gpuRandomSampleTexture;

		glm::ivec2 resolution;

		BlurHandler* blurrer;

		Texture *nullSSAO; // completely white

		int kernelSamples, rotationTextureResolution, scaleTextureResolution; // kernelSamples should match the value in the shader
	public:
		// If resolution is empty, then the screen resolution is used (which is probably for the best)
		SSAOHandler(glm::ivec2 resolution = {-1,-1}, bool enableSSAO = true, int kernelSamples = 23, int rotationTextureResolution = 5, int scaleTextureResolution = 5);

		Texture* runSSAO(DeferredRenderer* in, CameraBase* camera);
	};

}

#endif /* SRC_GAME_SSAO_H_ */
