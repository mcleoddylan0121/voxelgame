/*
 * skybox.h
 *
 *  Created on: Jul 11, 2015
 *      Author: Dylan
 */

#ifndef SKYBOX_H_
#define SKYBOX_H_

#include "coords.h"
#include "miscmesh.h"
#include "particle.h"
#include <algorithm>

namespace vox {

	enum daynight { DAY, NIGHT }; // bool 2.0

	namespace noise {
		class Noise;
	}

	struct SunLight {
		glm::vec3 invDirection;
		float power;
		glm::vec3 color;
		float compensation;
	};

	struct SkyboxSnapShot {
		float time;
		int dayOrNight;
		glm::vec3 sunInverseDir;
		glm::quat sunrot;

		SkyboxSnapShot(float time = 0, int dayOrNight = DAY, glm::vec3 sunInverseDir = glm::vec3(0,1,0), glm::quat sunrot= glm::quat(1,0,0,0)):
			time(time), dayOrNight(dayOrNight), sunInverseDir(sunInverseDir), sunrot(sunrot) {}
	};

	struct FogUniforms {
		glm::vec4 color;
		float density;

		template<typename Program_t> void upload(Program_t* p) {
			glUniform4fv(p->fogColor, 1, &(color[0]));
			glUniform1f(p->fogDensity, density);
		}
	};



	struct RenderToSkyShaderProgram;
	struct RenderSkyShaderProgram;
	template<typename T> struct ArrayBuffer;
	template<typename T> struct StackBuffer;
	struct Texture;
	struct CloudShaderProgram;
	struct Camera;
	struct CubeMapColorBuffer;
	struct SkyObjectShaderProgram;
	struct TexturedMeshBuffer;
	struct RenderTextureProgram;
	class TexturedMeshShaderProgram;

	class MeshSkyLighter;

	class Sky {
	public:
		Sky();
		~Sky();

		RenderToSkyShaderProgram* skyRenderTargetShader;

		RenderSkyShaderProgram* skyShader_exposure;
		RenderSkyShaderProgram* skyShader_noExposure;
		RenderSkyShaderProgram* starShader;

		RenderTextureProgram* renderCloudsToScreen;

		TexturedMeshShaderProgram* billboardShader;

		CubeMapColorBuffer* skyBoxes[3];
		SkyboxSnapShot skyboxSnapShots[3]; // the information behind our past 2, and upcoming skybox renders

		glm::quat sunRot;
		glm::vec3 sunInverseDir;
		SunLight sun;
		ArrayBuffer<SunLight>* sunData;

		noise::Noise* cloudNoise;
		StackBuffer<glm::vec3>* cloudLayerBuffer;
		CloudShaderProgram* cloudShader;

		// For rendering the skybox in halves
		StackBuffer<glm::vec3>* cloudLayerHalfBuffers[6][2];
		StackBuffer<glm::vec3>* atmHalfBuffers[2];

		StackBuffer<glm::vec3>* renderTextureBuffer;

		Texture* moonTex;
		Texture* sunLensFlare;

		Texture* cloudParticleAtlas; // for particle-based cloud rendering, stores the different particles used
		CloudParticleManager* cloudParticleManager; // seperate from worldstate particle manager, because there's no way these two sets of particles will concievably interact in any meaningful way.

		TexturedMeshBuffer* lensflareBuf;

		CubeMap* starCubemap;

		FogUniforms fogUniforms;

		MeshSkyLighter* meshSkyLighter;

		float time;
		float timeOfDay; // from 0 to 1, percent completion of whatever sky event is going on (day, night, blood moon, etc)

		bool downsampleClouds;
		ColorBuffer* downsampledCloudBuffer;

		int curLayer = 0;

		void calculateSkyLight();
		void renderToSkyBox(int boxNumber, int face, int half);
		void renderCloudsToSkyBox(int boxNumber, int face, int half);
		void renderTextureToScreen(TexturedMeshBuffer* mesh, Texture* tex, glm::quat rot, glm::vec3 scale, const Camera& cam, int layer, glm::vec4 color);
		void renderMoonAndStars(const Camera& cam, std::function<glm::vec3(glm::vec3,float)> exposureFunction);
		void renderSunLensFlare(const Camera& cam, std::function<glm::vec3(glm::vec3,float)> exposureFunction);
		void renderCubeMap(const Camera& cam, glm::quat rot, RenderSkyShaderProgram* p, int layer, glm::vec4 color, Texture* t1, Texture* t2 = nullptr);

		void createCloudParticles(std::function<glm::vec3(glm::vec3,float)> exposureFunction); // create the cloud particles and all of that good fun (done in the renderTo step)
		void renderCloudParticles(const Camera& cam, float time); // render the cloud particles to the screen (last step, since they're the closest part of the sky)

		void prepareForRender(const Camera& cam);
		void tick(float delta);
		void render(const Camera& cam, bool exposure);

		static void mulTimeRate(float mul);
		static void setAtmosValue(int index, float val);
	};

}



#endif /* SKYBOX_H_ */
