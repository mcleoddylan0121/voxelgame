/*
 * chunk.h
 *
 *  Created on: Mar 25, 2015
 *      Author: Dylan
 */

#ifndef CHUNK_H_
#define CHUNK_H_

#include "sdlcontroller.h"
#include "camera.h"
#include "renderutils.h"
#include "geometry.h"
#include "voxelmesh.h"
#include "hashes.h"

#include <unordered_map>
#include <vector>
#include <stack>
#include <queue>
#include <utility>

namespace vox {
	class ChunkMap;
	class Flora;
	class WorldState;
	class StructureManager;
	class StructureUnit;

	struct CPUGridVoxelCollisionMeshUnit;

	struct QueuedLight {
		glm::i8vec3 pos;
		char intensity;

		QueuedLight(glm::i8vec3 pos, char intensity):
			pos(pos), intensity(intensity) {}
	};

	struct QueuedLightRemoval {
		glm::i8vec3 pos;
		char oldIntensity;

		QueuedLightRemoval(glm::i8vec3 pos, char oldIntensity):
			pos(pos), oldIntensity(oldIntensity) {}
	};

	class Chunk {
	public:

		ChunkCoordinate position;
		Volume volume;
		ChunkMap* parent;

		static const int SIZE; // Chunks are SIZE*SIZE*SIZE
		static const int LIFE;
		Chunk (ChunkCoordinate position, Palette * mapping, noise::MultiNoise<1>* meshColorNoise, ChunkMap* parent);
		~Chunk();

		bool isVoxelAt(const LocalCoordinate& position);

		void setVoxel(LocalCoordinate position, const VoxelID value);
		void sendMeshToGPU();

		void tick(float ttime);
		void resetLifeCounter();
		bool checkForDeletion();

		ChunkMesh* mesh = nullptr;
		std::vector<Flora*> flora;

		Voxel get(LocalCoordinate position);
		VoxelID getVoxelID(LocalCoordinate& position);

		bool contains(const VoxelCoordinate& v);
		bool containsInPaddedRange(const VoxelCoordinate& v);
		std::vector<Chunk*> getChunksThatContainPoint(glm::i8vec3 point);
		Chunk* getChunkThatPointBelongsTo(glm::i8vec3 point);

		void resolveLighting(int i);

		bool updateLighting = false;

		std::queue<QueuedLight> lightsToAdd[3]; // one for each color
		std::queue<QueuedLightRemoval> lightsToRemove[3];
	protected:
		void setLightLevel(glm::i8vec3 pos, LightLevel lev);

		float lifeCounter = LIFE;
	};

	struct ChunkData {
		Chunk * ch;
		ChunkCoordinate pos;
		StructureUnit* structureUnit;

		virtual ~ChunkData() {}
	};

	class BiomeMap {
	public:
		typedef glm::vec3 BiomeCoordinate; // Currently {height, moisture, heat}

		BiomeMap();
		virtual ~BiomeMap();

		Color interpolate(BiomeCoordinate b); // Use only normalized values

	private:
		Color * map; // height + moisture * heightSections + heat * moistureSections * heightSections
		BiomeCoordinate mapSize; // The amount of sections on each size the map is split up into

		Color getColor(BiomeCoordinate b);
		void setColor(Color c, BiomeCoordinate b);
		BiomeCoordinate toIndex(BiomeCoordinate b); // Ensures that the input is composed of integers
	};

	class ChunkGenerator {
	private:
		long _seed;

	public:
		noise::Noise *terrainNoise, *terrainMoisture, *terrainHeight;
		noise::MultiNoise<1>* tempBiomeNoise;
		ChunkMap* parent;
		ChunkGenerator(long seed, ChunkMap* parent);
		virtual ~ChunkGenerator();

		VoxelID generateVoxel(WorldCoordinate pos);
		Chunk * generateChunk(ChunkCoordinate& pos, Palette * mapping, StructureUnit* structureUnit);
	};

	struct ChunkSorter {
		WorldCoordinate playerPos;
		Camera playerCam;

		// Sort functor
		bool operator() (ChunkData * a, ChunkData * b);

	};

	class ChunkLoader {
	private:
		std::stack<ChunkData*> _chunksFinished;
		std::vector<ChunkData*> _chunksCreation;
		Mutex _chunkCreationLock, _chunkFinishedLock, _dataAccessLock;
		ChunkSorter _lastSorter;
		SDLThread<ChunkLoader> * _thread;
		ChunkGenerator * _gen;
		ChunkMap * _parent;
		bool _running = false;
		bool _sort = false;

		static int _chunkCreationLoop(void * data);

		ChunkData * _popCreation();
		void _pushCreation(ChunkData * cd);
		ChunkData * _popFinished();
		void _pushFinished(ChunkData * cd);

		void _sortCreation();

	public:
		virtual ~ChunkLoader();

		void loadChunk(ChunkCoordinate& pos);
		void destroyChunkData(ChunkData * cd);

		// Batch chunk loading
		void startBatch();
		void batchLoad(ChunkCoordinate& pos);
		void endBatch();

		ChunkData * next();
		bool hasNext();

		void start(ChunkGenerator * gen, ChunkMap * parent, bool sort = false);
		void stop();

		void updateChunkSorter(const ChunkSorter& c);
	};

	// for raycasts in a chunk map
	struct VoxelRaycastReport {
		float distanceTravelled;
		WorldCoordinate endPoint;
		glm::vec3 normal;
		bool found;
	};
	bool isValidRaycast(const WorldCoordinate& pos, const WorldCoordinate& dir, float rad);

	// depth sorts chunks before rendering them, excludes empty chunks, does frustum culling
	class ChunkDepthSorter {
	private:
		void _sort();
		ChunkMap* parent;
		std::vector<Chunk*> depthSortedChunks;

	public:
		void prepareForRender();

		friend class ChunkMap;
		friend class GLAnalytics;
	};

	class ChunkMap {
	private:
		static const unsigned int NUM_CHUNKLOADERS = 2;

		unsigned int curLoader = 0;
		ChunkLoader loaders[NUM_CHUNKLOADERS];
		Chunk * tempChunk;
		ChunkGenerator * gen;

		std::vector<ChunkCoordinate> chunkStructuresToLoad;
		std::vector<ChunkCoordinate> chunkLoadBuffer;

		void setLightForAddition(VoxelCoordinate pos, LightLevel lev);
		void setLightForRemoval(VoxelCoordinate pos);

	public:
		StructureManager* structures;

		ChunkDepthSorter _depthSort;

		std::unordered_map<ChunkCoordinate, Chunk*, ChunkHash> chunks;

		WorldState* _parent;
		Palette * voxelMap;
		TerrainPalette * actualPalette;

		bool isLoadedYet(const Chunk* ch);

		void init(WorldState* parent);
		virtual ~ChunkMap();

		Chunk* getChunk(ChunkCoordinate& position);
		void setChunk(ChunkCoordinate& position, Chunk* someChunk);
		void createChunk(ChunkCoordinate& position);

		Voxel getVoxel(VoxelCoordinate& pos);
		VoxelID getVoxelID(VoxelCoordinate& pos);
		void removeVoxel(VoxelCoordinate& pos);
		void setVoxel(VoxelID id, VoxelCoordinate& pos);

		float getTerrainHeightAt(glm::vec2 pos);

		// For collision detection, find out how far out the line will extend before it hits a solid voxel
		VoxelRaycastReport getRaycast(const WorldCoordinate& pos, const WorldCoordinate& dir, float rad);

		inline void loadInRadius(ChunkCoordinate position, int radius);
		void  loadInRadius(ChunkCoordinate position, ChunkCoordinate radii);

		void tick(float ttime);

		std::unordered_map<ChunkCoordinate, Chunk*, ChunkHash>::iterator begin();
		std::unordered_map<ChunkCoordinate, Chunk*, ChunkHash>::iterator end();

		void prepareForRender();

		void renderChunks(ChunkMeshRenderer* render);
		void renderEntities(EntityMeshRenderer* render);
		void renderChunkVolumes(VolumeRenderer* render);

		void setLight(VoxelCoordinate pos, LightLevel value);
		void removeLight(VoxelCoordinate pos);

		std::vector<CPUGridVoxelCollisionMeshUnit*> getCollisionUnits(VoxelCoordinate start, VoxelCoordinate size);


		LightLevel getLightLevel(VoxelCoordinate pos);
	};

	const glm::ivec3 CHUNK_RENDER_RADIUS = {6, 5, 6};
		inline bool inRenderRadius(WorldCoordinate cam, WorldCoordinate ch, ChunkCoordinate rad) {
			ChunkCoordinate ccam = worldToChunkCoordinate(cam);
			ChunkCoordinate cch = worldToChunkCoordinate(ch);
			return  glm::distance2(ccam.x, cch.x) < rad.x * rad.x &&
					glm::distance2(ccam.y, cch.y) < rad.y * rad.y &&
					glm::distance2(ccam.z, cch.z) < rad.z * rad.z;
		}
}



#endif /* GAMEMAP_H_ */
