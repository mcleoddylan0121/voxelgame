/*
 * entityphysics.h
 *
 *  Created on: Sep 2, 2015
 *      Author: Daniel
 */

#ifndef ENTITYPHYSICS_H_
#define ENTITYPHYSICS_H_

#include "coords.h"
#include "shapes.h"
#include "anim.h"

namespace vox {

	struct Entity;


	struct MotionObject {
		WorldCoordinate velocity, acceleration, delta_d;

		bool onGround;

		// Sets delta_d as ��pos
		void tick(float delta);
		void stop();

		// Call this after tick!
		// Can change delta_d
		void handleTerrainCollisions(Entity* e);
	};

	struct BoundingVolume {
		friend struct MotionObject;

		WorldCoordinate pos;
		glm::quat rot;

		// For collision with the chunkmap
		Volume withTerrain();

		// For collision with entities
		Volume withEntities();

		// Bounds the EntityRender
		Volume visual();

		void setTerrainVolume(Volume v);
		void setEntityVolume(Volume v);
		void setVisualVolume(Volume v);

	private:
		// Using pos here as the offset and size as the size
		Volume _terrain, _entities, _visual;
	};



	struct EntityTweens {
		Tween<glm::quat> rotTween, headTween;
		Tween<float> rampTween;
		Entity* e;

		void init(Entity* e);
		void tick(float delta);
	};

	void swingTwistDecomposition(const glm::quat& rotation,
							 	 const glm::vec3& direction,
								 	 glm::quat& swing,
									 glm::quat& twist);
}



#endif /* ENTITYPHYSICS_H_ */
