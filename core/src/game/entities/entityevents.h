/*
 * entityevents.h
 *
 *  Created on: Sep 17, 2015
 *      Author: Daniel
 */

#ifndef ENTITYEVENTS_H_
#define ENTITYEVENTS_H_

#include "vluautil.h"

namespace vox {

	struct Entity;

	struct EventManager {
		Entity* parent;

		void init(Entity* p);

		void collide(Entity * other);
	};
}



#endif /* ENTITYEVENTS_H_ */
