/*
 * entitylua.h
 *
 *  Created on: Aug 31, 2015
 *      Author: Daniel
 */

#ifndef ENTITYLUA_H_
#define ENTITYLUA_H_

#include "entityutil.h"
#include "vluautil.h"
#include "animlua.h"

namespace vox {
	struct EntityManager;

	struct EntityLibrary {
		// -------Global functions---------
		static void setPlayer(EntityID id);
		static void renderDebug(bool b);


		// --------Local (Per entity) functions--------
		static EntityID createEntity(std::string address);
		static void remove(EntityID id);

		static lvec3 getPos(EntityID id);
		static lvec3 getVel(EntityID id);
		static lvec3 getAcc(EntityID id);
		static lvec3 getSize(EntityID id);

		static void setStat(EntityID id, std::string name, float value);
		static void setMesh(EntityID id, std::string meshName, float voxelSize);
		static void setSkeleton(EntityID id, LuaSkeleton ls, float voxelSize);
		static void setAnimation(EntityID id, std::string anim, bool interrupt);
		static void setAnimSpeed(EntityID id, float speed);
		static void setPos(EntityID id, lvec3 pos, bool path);
		static void setVel(EntityID id, lvec3 vel, bool path);
		static void setAcc(EntityID id, lvec3 acc, bool path);

		static void setNoClip(EntityID id, bool b);
		static void setSolid(EntityID id, bool b);
		static void setUntouchable(EntityID id, bool b);

		static void setTerrainVolume(EntityID id, lvec3 offs, lvec3 size);

		static EntityManager * _m;
	};

}

#endif /* ENTITYLUA_H_ */
