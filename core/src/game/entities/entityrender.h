/*
 * entityrender.h
 *
 *  Created on: Sep 2, 2015
 *      Author: Daniel
 */

#ifndef ENTITYRENDER_H_
#define ENTITYRENDER_H_

#include "renderutils.h"

namespace vox {

	struct BoundingVolume;
	struct EntityMesh;
	struct Skeleton;

	struct EntityRender {
		enum Type {
			Mesh, Skeletal
		};

		virtual ~EntityRender() {}
		virtual void render(EntityMeshRenderer * r, BoundingVolume b) = 0;
		virtual void tick(float delta, BoundingVolume b);
		virtual Type type() = 0;
	};

	struct MeshRender : public EntityRender {
		EntityMesh * mesh;
		float voxelSize;

		void render(EntityMeshRenderer * r, BoundingVolume b);
		WorldCoordinate getSize();

		EntityRender::Type type() { return Mesh; }

		// TODO: destructor unload mesh
		virtual ~MeshRender() {}
	};

	struct SkeletalRender : public EntityRender {
		Skeleton * skeleton;
		float voxelSize;

		void render(EntityMeshRenderer * r, BoundingVolume b);
		void tick(float delta, BoundingVolume b);

		EntityRender::Type type() { return Skeletal; }

		virtual ~SkeletalRender();
	};
}



#endif /* ENTITYRENDER_H_ */
