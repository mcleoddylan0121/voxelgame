/*
 * entitylua.cpp
 *
 *  Created on: Aug 31, 2015
 *      Author: Daniel
 */

#include "entitylua.h"
#include "entitymanager.h"
#include "luacontext.h"
#include "console.h"
#include "entityrender.h"
#include "filesystem.h"
#include "voxelmesh.h"
#include "resourcemanager.h"
#include "geometry.h"
#include "skeleton.h"
#include "animationmanager.h"

#include <algorithm>

#define _chk() EntityManager::iterator e = _m->getEntity(id); if (!e.exists()) return;
#define _chk2(x) EntityManager::iterator e = _m->getEntity(id); if (!e.exists()) return x;

namespace vox {
	EntityManager * EntityLibrary:: _m;

	// Global functions
	void EntityLibrary::setPlayer(EntityID id) {
		_chk();
		mPlayer->setPlayer(e);
	}

	void EntityLibrary::renderDebug(bool b) {
		_m->_renderDebug = b;
	}

	// Per entity functions
	EntityID EntityLibrary::createEntity(std::string address) {
		Entity * e = new Entity(_m->_parent, address, ++_m->_curID); // ++curID because 0 is not a valid ID
		_m->_entities[e->_id] = e;
		_m->_toRender.push_back(e);
		_m->_toTick.push_back(e);
		return e->_id;
	}

	lvec3 EntityLibrary::getPos(EntityID id) { lvec3 a; a.init(glm::vec3(0)); _chk2(a); a.init(e->bb.pos); return a; }
	lvec3 EntityLibrary::getVel(EntityID id) { lvec3 a; a.init(glm::vec3(0)); _chk2(a); a.init(e->motion.velocity); return a; }
	lvec3 EntityLibrary::getAcc(EntityID id) { lvec3 a; a.init(glm::vec3(0)); _chk2(a); a.init(e->motion.acceleration); return a; }
	lvec3 EntityLibrary::getSize(EntityID id) { lvec3 a; a.init(glm::vec3(0)); _chk2(a); a.init(e->bb.visual().size); return a; }

	void EntityLibrary::setStat(EntityID id, std::string name, float value) {
		_chk();
		e->_stats[name] = value;
	}

	void EntityLibrary::setMesh(EntityID id, std::string meshName, float voxelSize) {
		_chk();
		delete e->_render;
		MeshRender * r = new MeshRender();
		r->mesh = mRes->getResource<EntityMesh, Asset::Mesh>(meshName);
		r->voxelSize = voxelSize;
		e->anims->_removeSkeleton();
		e->_render = r;
		e->_renderInitialized = true;

		Volume v = {glm::vec3(0), r->getSize()};
		e->bb.setEntityVolume(v);
		e->bb.setTerrainVolume(v);
		e->bb.setVisualVolume(v);
	}
	void EntityLibrary::setSkeleton(EntityID id, LuaSkeleton ls, float voxelSize) {
		_chk();
		delete e->_render;
		SkeletalRender * s = new SkeletalRender();
		s->skeleton = constructSkeleton(ls);
		s->voxelSize = voxelSize;
		e->anims->_setSkeleton(s->skeleton);
		e->_render = s;
		e->_renderInitialized = true;

		Volume v = {glm::vec3(0), glm::vec3(s->skeleton->root->mesh->size) * voxelSize};
		e->bb.setEntityVolume(v);
		e->bb.setTerrainVolume({glm::vec3(0), glm::vec3(vecMin(v.size))});
		e->bb.setVisualVolume(v);
	}
	void EntityLibrary::setAnimation(EntityID id, std::string anim, bool interrupt) {
		_chk();
		e->anims->trigger(anim, interrupt);
	}
	void EntityLibrary::setAnimSpeed(EntityID id, float speed) {
		_chk();
		e->anims->setSpeed(speed);
	}
	void EntityLibrary::setPos(EntityID id, lvec3 pos, bool path) {
		_chk();
		e->_posPath = path;
		if (!path)
			e->setPos((glm::vec3)pos);
	}
	void EntityLibrary::setVel(EntityID id, lvec3 vel, bool path) {
		_chk();
		e->_velPath = path;
		if (!path)
			e->motion.velocity = vel;
	}
	void EntityLibrary::setAcc(EntityID id, lvec3 acc, bool path) {
		_chk();
		e->_accPath = path;
		if (!path)
			e->motion.acceleration = acc;
	}

	void EntityLibrary::setSolid(EntityID id, bool b) { _chk(); e->solid = b; }
	void EntityLibrary::setNoClip(EntityID id, bool b) {
		_chk();
		auto i = std::find(_m->_cWithTerrain.begin(), _m->_cWithTerrain.end(), (Entity*)e);
		if (i != _m->_cWithTerrain.end()) _m->_cWithTerrain.erase(i);

		if (!b) _m->_cWithTerrain.push_back(e);
	}
	void EntityLibrary::setUntouchable(EntityID id, bool b) {
		_chk();
		auto i = std::find(_m->_cWithEntities.begin(), _m->_cWithEntities.end(), (Entity*)e);
		if (i != _m->_cWithEntities.end()) _m->_cWithEntities.erase(i);

		if (!b) _m->_cWithEntities.push_back(e);
	}

	void EntityLibrary::setTerrainVolume(EntityID id, lvec3 offs, lvec3 size) {
		_chk();
		Volume v = {(glm::vec3)(offs), (glm::vec3)(size)};
		e->bb.setTerrainVolume(v);
	}

	void EntityLibrary::remove(EntityID id) {
		_m->_toRemove.push_back(id);
	}
}
