/*
 * entity_temp.cpp
 *
 *  Created on: Aug 30, 2015
 *      Author: Daniel
 */

#include "entity.h"
#include "worldstate.h"
#include "skeleton.h"
#include "entityphysics.h"
#include "animationmanager.h"

namespace vox {

	Entity::Entity(WorldState * p, std::string address, EntityID id) : parent(p), _id(id), _address(address) {
		solid = false;
		_render = 0;
		events.init(this);
		tweens.init(this);
		anims = new AnimationManager();
	}
	Entity::~Entity() { delete _render; }

	void Entity::tick(float delta) {
		anims->tick(delta);

		motion.tick(delta);
		setPos(bb.pos + motion.delta_d);
		tweens.tick(delta);

		// For collisions to happen once, not once per tick
		_lastCollisions = _currentCollisions;
		_currentCollisions.clear();
	}

	void Entity::render(EntityMeshRenderer * r) {
		if (_renderInitialized)
			_render->render(r, bb);
	}

	void Entity::setPos(WorldCoordinate pos) {
		bb.pos = pos;
	}
	void Entity::setRot(glm::quat rot) {
		bb.rot = rot;
	}


	float swingClamp = PI * 5 / 12;
	float twistClamp = PI / 6;
	float lookTweenDur = 0.1;

	void Entity::lookAt(glm::vec3 p) {
		if (hasSkeleton()) {
			auto a = getSkeleton();
			auto q = glm::inverse(bb.rot) * glm::rotate(a->animations["base"]->frames[0]->components["head"].rot
				* glm::inverse(glm::toQuat(glm::lookAt(bb.pos, p, glm::vec3(0, 1, 0)))), PI/2, glm::vec3(0, 1, 0));
			glm::quat swing, twist;
			swingTwistDecomposition(q, glm::vec3(0, 0, 1), swing, twist);
			float swingA = glm::angle(swing);
			float twistA = glm::angle(twist);
			if (swingA > swingClamp) swing = glm::slerp(glm::quat(), swing, swingClamp / swingA);
			if (twistA > twistClamp) twist = glm::slerp(glm::quat(), twist, twistClamp / twistA);
			tweens.headTween.init(a->joints["head"]->rot, swing, lookTweenDur, tween::linearQuat, &a->joints["head"]->rot);
		}
	}

	WorldCoordinate Entity::getPos() {
		return bb.pos;
	}
	glm::quat Entity::getRot() {
		return bb.rot;
	}

	Skeleton * Entity::getSkeleton() {
		return ((SkeletalRender*)_render)->skeleton;
	}

	bool Entity::hasHead() {
		if (!hasSkeleton()) return false;

		auto a = getSkeleton();
		return a->joints.find("head") != a->joints.end();
	}

	bool Entity::hasSkeleton() {
		return _render->type() == EntityRender::Skeletal;
	}

	void Entity::trigger(std::string triggerName) {
		anims->trigger(triggerName, false);
	}
	void Entity::triggerInterrupt(std::string triggerName) {
		anims->trigger(triggerName, true);
	}

	EntityID Entity::id() { return _id; }
}
