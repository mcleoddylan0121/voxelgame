/*
 * entity_temp.cpp
 *
 *  Created on: Aug 30, 2015
 *      Author: Daniel
 */

#ifndef ENTITY_H_
#define ENTITY_H_

#include "entityutil.h"
#include "entityrender.h"
#include "entitylua.h"
#include "entityphysics.h"
#include "entityevents.h"

#include <string>
#include <set>
#include <unordered_map>

namespace vox {

	struct WorldState;
	struct EntityManager;
	struct PlayerManager;
	struct Skeleton;
	struct AnimationManager;

	struct Entity {
		friend struct EntityLibrary;
		friend struct PlayerManager;
		friend struct EntityManager;
		friend struct EventManager;

		Entity(WorldState * parent, std::string address, EntityID id);
		virtual ~Entity();

		void tick(float delta);
		void render(EntityMeshRenderer * r);

		void setPos(WorldCoordinate p);
		void setRot(glm::quat rot);


		// For animations
		void trigger(std::string triggerName);				// Does not re-trigger if trigger currently active
		void triggerInterrupt(std::string triggerName);		// Triggers again no matter what trigger is active

		// The entity looks at p if and only if it has a skeleton and that skeleton has a head.
		void lookAt(WorldCoordinate p);

		WorldCoordinate getPos();
		glm::quat getRot();

		EntityID id();

		Skeleton * getSkeleton();

		bool hasHead();
		bool hasSkeleton();

		BoundingVolume bb;
		MotionObject motion;
		EntityTweens tweens;
		EventManager events;

		WorldState * parent;
		bool solid;

	private:
		EntityID _id;

		bool _posPath = false, _velPath = false, _accPath = false;

		EntityRender * _render;
		bool _renderInitialized = false;

		AnimationManager * anims;

		// Lua stuffs
		std::string _address;
		std::unordered_map<std::string, float> _stats;

		std::set<EntityID> _currentCollisions, _lastCollisions;
	};

}


#endif /* ENTITY_H_ */
