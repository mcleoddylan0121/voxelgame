/*
 * entityevents.cpp
 *
 *  Created on: Sep 17, 2015
 *      Author: Daniel
 */

#include "entityevents.h"
#include "entity.h"
#include "luacontext.h"

namespace vox {

	void EventManager::init(Entity * p) { parent = p; }

	void EventManager::collide(Entity * other) {
		mLua->getContext()->callVoidFunction(parent->_address + "." + "_collide", parent->_address, other->_address);
	}

}
