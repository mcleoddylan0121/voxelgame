/*
 * entityalgo.cpp
 *
 *  Created on: Sep 16, 2015
 *      Author: Daniel
 */

#include "entitymanager.h"
#include "chunk.h"
#include "reports.h"
#include "shapes.h"

#include <list>
#include <limits>

namespace vox {

	typedef ChunkCoordinate ChunkSector;

	// Number of ChunkSectors in one chunk
	const int nSectors = 8;

	ChunkCoordinate toChunkCoordinate(ChunkSector c) {
		int conv = Chunk::SIZE / nSectors;
		return (c / (int)conv);
	}

	ChunkSector toChunkSector(WorldCoordinate c) {
		return ChunkSector(glm::floor(c / (float)nSectors));
	}

#define entity_bb_loop	Volume v = e->bb.withEntities().gridAlligned();\
						ChunkSector a = toChunkSector(v.getBottomCorner()),\
									b = toChunkSector(v.getTopCorner());\
						for (int z = a.z; z <= b.z; z++)\
						for (int y = a.y; y <= b.y; y++)\
						for (int x = a.x; x <= b.x; x++)

	void EntityManager::_collideEntities(std::vector<Entity*>& v) {
		// We're using lists here because we want constant time insert and constant time deletion and we
		//  aren't doing direct accesses so linked lists are perfect.
		std::unordered_map<ChunkSector, std::list<Entity*>, ChunkHash> grid;

		// Set up our grid by adding each entity to all the grid sectors they occupy
		for (Entity* e : v) { // Note, these braces are necessary because of how macro expansion works
			entity_bb_loop
				grid[ChunkSector(x, y, z)].push_back(e);
		}

		// Copy the grid here because we're destroying our copy. We use the copy in raycasts.
		_grid = grid;

		// Find an entity, iterate over all grid spaces it occupies, and remove it from those grid spaces
		for (Entity* e : v) { // Note, these braces are necessary because of how macro expansion works
			std::set<EntityID> e_checked; // Make sure we don't collide with the same entity twice

			entity_bb_loop {
				auto& gridEntities = grid[ChunkSector(x, y, z)];
				std::list<Entity*>::iterator e_iterator; // So we can erase it later

				// We use an iterator here so we can save the iterator corresponding to the current entity
				for (auto i = gridEntities.begin(); i != gridEntities.end(); i++)
					if (*i == e)
						e_iterator = i;
					else if (e_checked.find((*i)->id()) == e_checked.end()) { // If we haven't checked against this entity yet
						e_checked.insert((*i)->id());
						_handleCollision(e, *i);
					}
				gridEntities.erase(e_iterator);
			}
		}
	}

	void EntityManager::_handleCollision(Entity* e1, Entity* e2) {
		if (!e1->bb.withEntities().intersects(e2->bb.withEntities())) return;

		e1->_currentCollisions.insert(e2->id());
		e2->_currentCollisions.insert(e1->id());

		// Here, we only need to check one of the entity's last collisions because the id is added to both
		//  so we choose the smallest one to binary search over
		if (e1->_lastCollisions.size() < e2->_lastCollisions.size() ?
				e1->_lastCollisions.find(e2->id()) == e1->_lastCollisions.end() :
				e2->_lastCollisions.find(e1->id()) == e2->_lastCollisions.end()) {
			// If we did NOT collide last tick
			e1->events.collide(e2);
			e2->events.collide(e1);
		}
	}


	// RAYCASTS
	EntityRaycastReport EntityManager::getRaycast(WorldCoordinate pos, WorldCoordinate dir, float rad, std::vector<Entity*> ignoreList) {
		if (!isValidRaycast(pos, dir, rad)) return {false};

		// Get the grid we created last tick.
		std::unordered_map<ChunkSector, std::list<Entity*>, ChunkHash> grid = _grid;

		float conv = Chunk::SIZE / nSectors; // Units per sector

		WorldCoordinate normDir = glm::normalize(dir);
		WorldCoordinate sign = glm::sign(normDir);
		WorldCoordinate biSign = glm::floor((sign + WorldCoordinate(1)) / 2.f) * conv; // Sign used for rounding. Components are 0 if - or conv if +

		float dist = 0.f;
		WorldCoordinate curPos = pos;
		while (dist <= rad) {
			WorldCoordinate prox = WorldCoordinate(toChunkSector(curPos + biSign)) * conv - curPos; // Proximity to the chunkSector boundary
			WorldCoordinate params = glm::abs(prox / vecReplace(normDir, 0, 0.001f)); // Replace 0 to account for DBZ errors
			float t = vecMin(params); // Minimized distance (find the minimum parameterization to a chunkSector boundary)
			dist += t;
			curPos = curPos + normDir * t + sign * 0.001f; // Overshoot it by just a bit to account for float errors
			ChunkSector curSector = toChunkSector(curPos);

			if (grid.find(curSector) != grid.end()) {
				// Second test: check if we actually hit an entity.
				float minDist = std::numeric_limits<float>::max();
				Entity* minEnt = 0;
				RayVolumeReport minReport;

				for (Entity* e : grid[curSector]) {
					RayVolumeReport r = e->bb.visual().intersects({pos, normDir, rad});
					if (r.intersects && r.distance < minDist) {
						bool ignoreThisEntity = false;
						for (Entity * ignore : ignoreList)
							if ((ignoreThisEntity = (e == ignore)))
								break;
						if (!ignoreThisEntity) {
							minReport = r;
							minEnt = e;
							minDist = r.distance;
						}
					}
				}

				if (minDist != std::numeric_limits<float>::max())
					return {
						true, // found
						minDist, // Distance traveled
						minReport.start, // End point
						minReport.normal, // Normal
						minEnt	// The entity
					};
			}
		}

		return {false, dist, curPos, glm::vec3(), 0};
	}
}
