/*
 * entityrender.cpp
 *
 *  Created on: Sep 2, 2015
 *      Author: Daniel
 */

#include "entityrender.h"
#include "entityphysics.h"
#include "voxelmesh.h"
#include "renderutils.h"
#include "skeleton.h"

namespace vox {

	void EntityRender::tick(float delta, BoundingVolume b) {}

	void MeshRender::render(EntityMeshRenderer * r, BoundingVolume b) {
		renderMesh<EntityMeshRenderer, EntityMeshBuffer>(r, mesh->buf, b.visual().getBottomCorner(), b.visual().size, b.rot);
	}
	WorldCoordinate MeshRender::getSize() {
		return glm::vec3(mesh->size) * voxelSize;
	}


	void SkeletalRender::render(EntityMeshRenderer * r, BoundingVolume b) {
		skeleton->render(r);
	}
	SkeletalRender::~SkeletalRender() {
		delete skeleton;
	}
	void SkeletalRender::tick(float delta, BoundingVolume b) {
		skeleton->tick(delta, b.visual().getBottomCorner(), WorldCoordinate(voxelSize), b.rot);
	}
}


