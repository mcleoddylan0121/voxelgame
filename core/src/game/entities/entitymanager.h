/*
 * entitymanager.h
 *
 *  Created on: May 24, 2015
 *      Author: Dani
 */

#ifndef ENTITYMANAGER_H_
#define ENTITYMANAGER_H_

#include "entity.h"
#include "renderutils.h"
#include "camera.h"
#include "anim.h"
#include "hashes.h"
#include "util.h"

#include <set>
#include <list>

namespace vox {

	class WorldState;
	class EntityLibrary;
	class OrthoCamera;
	class EntityManager;

	struct PlayerManager {
		friend struct EntityManager;

		// Camera stuffs
		float camDist;
		float rot = 0, height;
		bool firstPerson = false;

		Camera cam;
		Entity * player;

		nullifier<Entity> selectedEntity, hoveredEntity;

		PlayerManager();
		virtual ~PlayerManager();

		void tick(float delta);
		void setPlayer(Entity* e);

		WorldCoordinate screenToWorldSpace(glm::vec2 point_screenspace, float depth = 0);

	private:
		void _handleCameraTweens(float delta);
		void _handleBlockPlacement(float delta);
		void _updateCamCenter(WorldCoordinate pos);
		void _handleSelection(float delta);

		EntityManager * _entityManager;

		// TODO Change this to an actual default
		Entity _defaultPlayer = Entity(0, "", 0);

		Tween<float> _camDistTween;
		Tween<float> _camAngleTween;
		Tween<float> _camHeightTween;
		Tween<glm::vec3> _camCenterTween;

		Tween<float> _playerSpeedTween;

		// Constants
		static float _camCenterTweenTime;
		static float _camAngleTweenTime;
		static float _camDistTweenTime;
		static float camDistSpd;
		static float fovSpd;
		static float spdDecayTime;
	};

	extern PlayerManager * mPlayer;

	struct EntityRaycastReport;

	// --------- ENTITY MANAGER -------------

	class EntityManager {
		friend class EntityLibrary;
	public:
		struct iterator {
			friend class EntityManager;
			Entity * operator->() { return e; }
			Entity operator*() { return *e; }
			operator Entity*() { return e; }

			bool exists() { return found; }

		private:
			bool found;
			Entity * e;
		};

		EntityManager();
		void init(WorldState * parent);
		virtual ~EntityManager();

		void tick(float delta);
		void render(EntityMeshRenderer* render);
		void renderVolumes(VolumeRenderer* render);
		void renderDropShadows(DropShadowRenderer* render);

		EntityRaycastReport getRaycast(WorldCoordinate pos, WorldCoordinate dir, float rad, std::vector<Entity*> ignoreList = {});

		iterator getEntity(EntityID id);
		int countEntities();

		WorldState * _parent;

	private:
		void _collideEntities(std::vector<Entity*>& e);
		void _handleCollision(Entity* e1, Entity* e2);

		// Entity id's start at 1, so 0 is an invalid ID
		std::unordered_map<EntityID, Entity*> _entities;
		std::vector<Entity*> _toRender, _toTick;
		std::vector<Entity*> _cWithTerrain, _cWithEntities;
		std::vector<EntityID> _toRemove;

		// For collision detection.
		std::unordered_map<ChunkCoordinate, std::list<Entity*>, ChunkHash> _grid;

		EntityID _curID = 0;

	public: // TODO: TEMPORARY!!!
		bool _renderDebug = false;
	};

}


#endif /* ENTITYMANAGER_H_ */
