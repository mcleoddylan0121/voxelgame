/*
 * entitymanager.cpp
 *
 *  Created on: May 24, 2015
 *      Author: Dani
 */

#include "entitymanager.h"
#include "resourcemanager.h"
#include "worldstate.h"
#include "sdlcontroller.h"
#include "entitylua.h"
#include "geometry.h"
#include "shapes.h"
#include "voxelmesh.h"
#include "util.h"
#include "settingsmanager.h"
#include "chunk.h"
#include "luacontext.h"
#include "skeleton.h"

#include <algorithm>

namespace vox {
	PlayerManager * mPlayer;

	float PlayerManager::_camCenterTweenTime = 0;
	float PlayerManager::_camAngleTweenTime = .025f;
	float PlayerManager::_camDistTweenTime = .25f;
	float PlayerManager::camDistSpd = 20;
	float PlayerManager::fovSpd = 5;
	float PlayerManager::spdDecayTime = 0.3f;

	// ------PLAYER MANAGER--------
	PlayerManager::PlayerManager() {
		height = PI / 9.0;
		camDist = 8;

		_entityManager = 0;

		player = &_defaultPlayer;

		_playerSpeedTween.init(0, 0, 1, tween::linear);
		_camDistTween.init(camDist, camDist, _camDistTweenTime, tween::cubicEaseInOut<float>, &camDist);

		cam.pos = glm::vec3(0) + camDist;
		cam.lookAt = glm::vec3(0);
		cam.up = glm::vec3(0, 1, 0);
		cam.aspectRatio = ((float)mSDL->windowWidth) / mSDL->windowHeight;
	}

	PlayerManager::~PlayerManager() {
	}

	void PlayerManager::setPlayer(Entity * p) {
		player = p;
	}

	void PlayerManager::_updateCamCenter(WorldCoordinate pos) {
		cam.lookAt = pos;
		//if (_camCenterTween.getFinalValue() != pos)
		//	_camCenterTween.init(cam.lookAt, pos, _camCenterTweenTime / (glm::length(pos - cam.lookAt) + 1) / (glm::length(pos - cam.lookAt) + 1), tween::linearVec3, &cam.lookAt);
	}

	void PlayerManager::tick(float delta) {
		WorldCoordinate camCenter = (firstPerson && player->hasHead()) ? player->getSkeleton()->joints["head"]->worldPos : player->bb.visual().getTopQuartile();

		_updateCamCenter(camCenter);
		//_camCenterTween.tick(delta);
		cam.pos = cam.lookAt + glm::vec3(camDist * cos(rot) * cos(height), camDist * sin(height), camDist * sin(rot) * cos(height));
		WorldCoordinate realPos = camCenter + glm::vec3(camDist * cos(rot) * cos(height), camDist * sin(height), camDist * sin(rot) * cos(height));

//		if (renderPlayer) {
//			auto camRaycast = map->getRaycast(camCenter, cam.pos - camCenter, glm::length(cam.pos - camCenter));
//			if (camRaycast.found()) cam.pos = camRaycast.endPoint + camRaycast.normal * 0.5f;
//		}
		cam.update();

		glm::vec2 tvel;

		float trot = 0; int nrot = 0;
		float theta = atan2(player->bb.visual().getTopQuartile().z - realPos.z, player->bb.visual().getTopQuartile().x - realPos.x);

		if (mSDL->isMouseGrabbed()) {
			if(mSDL->inputPressed("Move Forward")) trot += 0, nrot++, tvel.x += cos(theta), tvel.y += sin(theta);
			if(mSDL->inputPressed("Move Backward")) trot += M_PI, nrot++, tvel.x -= cos(theta), tvel.y -= sin(theta);
			if(mSDL->inputPressed("Move Left")) trot += M_PI / 2, nrot++, tvel.x += sin(theta), tvel.y -= cos(theta);
			if(mSDL->inputPressed("Move Right")) trot = -trot, trot += -M_PI / 2, nrot++, tvel.x -= sin(theta), tvel.y += cos(theta);
			if(mSDL->inputPressed("Jump") && mPlayer->player->motion.onGround) {
				if (mPlayer->player->motion.acceleration.y < 0) {
					float h = mPlayer->player->_stats["jumpHeight"];
					mPlayer->player->motion.velocity.y = sqrt(abs(-2 * mPlayer->player->motion.acceleration.y * h)) * 1.01f;
				}
			}
			_handleCameraTweens(delta);
			_handleBlockPlacement(delta);
		}

		_handleSelection(delta);

		if (nrot > 0)
			_playerSpeedTween.init(player->_stats["moveSpeed"], 0, spdDecayTime, tween::linear);
		else tvel.x = player->motion.velocity.x, tvel.y = player->motion.velocity.z;
		_playerSpeedTween.tick(delta);
		float spd = _playerSpeedTween.getValue();

		// Set magnitude of velocity vector (speed) to spd
		if (glm::length(tvel) != 0) tvel = glm::normalize(tvel);
		player->motion.velocity.x = tvel.x * spd;
		player->motion.velocity.z = tvel.y * spd;
	}

	void PlayerManager::_handleCameraTweens(float delta) {
		Mouse m = mSDL->getMouse();

		float rotSpd = mSettings->mouseSensitivity;
		float heightSpd = mSettings->mouseSensitivity;

		float tempCamDist = _camDistTween.getFinalValue();

		if(mSDL->inputPressed("FOV Modifier")) {
			cam.fovY -= m.sdy * delta * fovSpd;
			cam.fovY = constrain(cam.fovY, M_PI * 1.0 / 4.0, M_PI * 4.0/6.0);
		} else {
			int aval = abs(m.sdy);
			int sg = sgn(m.sdy);
			for (int i = 0; i < aval; i++)
				tempCamDist -= sg * camDistSpd * delta * tempCamDist;
		}
		tempCamDist = constrain(tempCamDist, 0.5, 32);
		if(abs(tempCamDist - camDist) > 0.01)
			_camDistTween.init(camDist, tempCamDist, _camDistTweenTime, tween::cubicEaseOut<float>, &camDist);

		_camHeightTween.init(
				height,
				constrain(_camHeightTween.getFinalValue() - heightSpd * delta * m.dy, (float) -(M_PI * 9995 / 20000), (float) (M_PI * 9995 / 20000)),
				_camAngleTweenTime,
				tween::linear,
				&height);
		_camAngleTween.init(rot, _camAngleTween.getFinalValue() + rotSpd * delta * m.dx, _camAngleTweenTime, tween::linear, &rot);

		_camHeightTween.tick(delta);
		_camAngleTween.tick(delta);
		_camDistTween.tick(delta);
		firstPerson = camDist <= 1;
	}

	void PlayerManager::_handleSelection(float delta) {
		glm::vec2 mousePos;
		std::vector<Entity*> ignoreList;

		if (mSDL->isMouseGrabbed()) {
			mousePos = {mSDL->windowWidth / 2.f, mSDL->windowHeight / 2.f};
			ignoreList.push_back(player);
		} else {
			Mouse m = mSDL->getMouse();
			mousePos = {m.x, m.y};
		}
		auto report = _entityManager->getRaycast(cam.pos, screenToWorldSpace(mousePos, 0.1) - cam.pos, 500, ignoreList);

		if (report.found && (!mSDL->isMouseGrabbed() || report.e != player)) {
			if (mSDL->inputPressed("Primary Fire"))
					selectedEntity = report.e;
			hoveredEntity = report.e;
		} else hoveredEntity.nullify();

		if (mSDL->inputPressed("Secondary Fire"))
			selectedEntity.nullify();
	}


	float placeLimiter = 0.0;
	const float placeDelay = 0.2;

	VoxelID bType = 1;

	void PlayerManager::_handleBlockPlacement(float delta) {
		if (mSDL->inputTyped("Cycle Block"))
			bType = (bType%13) + 1;

		WorldCoordinate viewDirection = cam.lookAt - cam.pos + WorldCoordinate(0.001, 0.001, 0.001);
		float PLACE_DISTANCE = 500;
		placeLimiter += delta;

		if(placeLimiter > placeDelay) {
			int t = -1;

			if(mSDL->inputPressed("Remove Block"))
				t = 0;

			if(mSDL->inputPressed("Place Block"))
				t = bType;

			if(t != -1) {
				auto raycast = player->parent->map->getRaycast(cam.lookAt, viewDirection, PLACE_DISTANCE);
				if (raycast.found) {
					float rad = mSettings->placeRadius;
					//if(!(playerEntity->bb.getVisualVolume().intersects(Volume{raycast.endPoint - rad, WorldCoordinate(2*rad)})) || t == 0)
					for(int x = -rad; x <= rad; x++) for(int y = -rad; y <= rad; y++) for(int z = -rad; z <= rad; z++) {
						float rad1 = x*x + y*y + z*z;
						if(rad1 < rad * rad) {
							VoxelCoordinate vPos = worldToVoxelCoordinate(raycast.endPoint + WorldCoordinate(x,y,z) + (t==0 ? WorldCoordinate(0) : raycast.normal));
							player->parent->map->setVoxel(t, vPos);
						}
					}
					placeLimiter = 0;
				}
			}
		}
	}

	WorldCoordinate PlayerManager::screenToWorldSpace(glm::vec2 p, float depth) {
		// TODO: Check if we need Window or Viewport here, remember to replace above too

		float x = 2 * p.x / mSDL->windowWidth - 1;
		float y = 2 * p.y / mSDL->windowHeight - 1;

		glm::vec4 ret(x, y, depth, 1);
		ret = glm::inverse(cam.viewProjection) * ret;
		ret /= ret.w;

		return {ret.x, ret.y, ret.z};
	}


	// ------ENTITY MANAGER--------

	EntityManager::EntityManager() {
		EntityLibrary::_m = this;
		mPlayer->_entityManager = this;
		_parent = 0;
	}

	EntityManager::~EntityManager() {
		for (auto i = _entities.begin(); i != _entities.end(); i++)
			delete i->second;
	}

	void EntityManager::init(WorldState * parent) {
		_parent = parent;
	}

	void findErase(Entity* e, std::vector<Entity*>& vec) {
		auto iter = std::find(vec.begin(), vec.end(), e);
		if (iter != vec.end())
			vec.erase(iter);
	}

	glm::vec3 callLuaPath(std::string func, int id, float delta) {
		return mLua->getContext()->callFunction<lvec3>("vox_entity_pathify", func, id, delta);
	}

	void EntityManager::tick(float delta) {
		// Delete entities at the beginning of the loop because whatever
		for (auto i = _toRemove.rbegin(); i != _toRemove.rend(); i++) {
			auto iter = getEntity(*i);
			if (iter.found) {
				Entity* e = iter;
				if (mPlayer->selectedEntity == e) mPlayer->selectedEntity.nullify();
				if (mPlayer->hoveredEntity == e) mPlayer->hoveredEntity.nullify();
				findErase(e, _toRender);
				findErase(e, _toTick);
				findErase(e, _cWithEntities);
				findErase(e, _cWithTerrain);
				_entities.erase(*i);
				delete e;
			}
			_toRemove.pop_back();
		}

		// Tick first to move the entity
		for (Entity* e : _toTick)
			e->tick(delta);

		// Then check if it collided with anything
		for (Entity* e : _cWithTerrain)
			e->motion.handleTerrainCollisions(e);

		// Then check if it collided with any entities
		_collideEntities(_cWithEntities);

		// Tick lua paths right after collision.
		for (Entity* e: _toTick) {
			if (e->_posPath) e->setPos(callLuaPath("_posPath", e->_id, delta));
			if (e->_velPath) e->motion.velocity = callLuaPath("_velPath", e->_id, delta);
			if (e->_accPath) e->motion.acceleration = callLuaPath("_accPath", e->_id, delta);
		}

		// Now update the entities' skeletons
		for (Entity* e : _toTick) {
			float spd = glm::length(glm::vec2(e->motion.velocity.x, e->motion.velocity.z));
			if (e->hasSkeleton()) {
				if (e->motion.onGround) {
					if (spd < 0.0001) e->trigger("IDLE"), spd = 1;
					else e->trigger("WALK");
				} else
					e->trigger("INAIR");
			}
			e->_render->tick(delta, e->bb);
		}

		// Then set the velocity of the player for the next tick
		mPlayer->tick(delta);
	}

	void EntityManager::render(EntityMeshRenderer* r) {
		for (Entity* e : _toRender) {
			if (e != mPlayer->player || !mPlayer->firstPerson)
				e->render(r);
		}
	}

	void EntityManager::renderDropShadows(DropShadowRenderer* r) {
		for (Entity* e : _toRender) {
			Volume a = e->bb.visual();
			r->render(DropShadowDisplayObject(&a, e->motion.onGround));
		}
	}

	// Volume colors
	Color::GPUColor _visualColor = RED.toGPUColor();
	Color::GPUColor _entityColor = BLUE.toGPUColor();
	Color::GPUColor _terrainColor = GREEN.toGPUColor();
	Color::GPUColor _terGridColor = (YELLOW).toGPUColor();
	Color::GPUColor _entGridColor = (RED + BLUE).toGPUColor();


	void EntityManager::renderVolumes(VolumeRenderer* render) {
		if (_renderDebug)
			for (Entity* e : _toRender) {
				Volume a = e->bb.visual(), b = e->bb.withEntities(), c = e->bb.withTerrain(), d = e->bb.withTerrain().gridAlligned(), f = e->bb.withEntities().gridAlligned();
				render->render(VolumeDisplayObject(&a, Color::HighpColor(_visualColor)/255.f));
				render->render(VolumeDisplayObject(&b, Color::HighpColor(_entityColor)/255.f));
				render->render(VolumeDisplayObject(&c, Color::HighpColor(_terrainColor)/255.f));
				render->render(VolumeDisplayObject(&d, Color::HighpColor(_terGridColor)/255.f));
				render->render(VolumeDisplayObject(&f, Color::HighpColor(_entGridColor)/255.f));
			}

		if (mPlayer->selectedEntity.exists()) {
			Volume a = mPlayer->selectedEntity->bb.visual();
			render->render(VolumeDisplayObject(&a, Color::HighpColor(_entityColor)/255.f));
		}

		if (mPlayer->hoveredEntity.exists()) {
			Volume a = mPlayer->hoveredEntity->bb.visual();
			a.size += WorldCoordinate(0.1);
			render->render(VolumeDisplayObject(&a, Color::HighpColor(_visualColor)/255.f));
		}
	}

	EntityManager::iterator EntityManager::getEntity(EntityID id) {
		auto it = _entities.find(id);
		iterator ret;
		if (it == _entities.end()) {
			ret.e = 0;
			ret.found = false;
		} else {
			ret.e = it->second;
			ret.found = true;
		}
		return ret;
	}

	int EntityManager::countEntities() {
		return _entities.size();
	}
}
