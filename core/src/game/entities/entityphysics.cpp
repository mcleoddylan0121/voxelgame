/*
 * entityphysics.cpp
 *
 *  Created on: Sep 2, 2015
 *      Author: Daniel
 */

#include "entityphysics.h"
#include "console.h"
#include "entity.h"
#include "util.h"
#include "geometry.h"
#include "worldstate.h"
#include "chunk.h"

#include <cmath>

namespace vox {

	Volume BoundingVolume::withTerrain() {
		Volume v = _terrain.getTranslation(pos);
		v.rot = glm::quat_cast(glm::mat3x3());
		return v;
	}

	Volume BoundingVolume::withEntities() {
		Volume v = _entities.getTranslation(pos);
		v.rot = rot;
		return v;
	}

	Volume BoundingVolume::visual() {
		Volume v = _visual.getTranslation(pos);
		v.rot = rot;
		return v;
	}

	void BoundingVolume::setTerrainVolume(Volume v) { _terrain = v; };
	void BoundingVolume::setEntityVolume(Volume v) { _entities = v; };
	void BoundingVolume::setVisualVolume(Volume v) { _visual = v; };


	// MOTION OBJECT

	void MotionObject::tick(float delta) {
		delta_d = acceleration * 0.5f * delta * delta + velocity * delta;
		velocity += delta * acceleration;
	}

	void MotionObject::stop() {
		velocity = glm::vec3();
	}

	// ENTITY TWEENS

	/**
	   Decompose the rotation on to 2 parts.
	   1. Twist - rotation around the "direction" vector
	   2. Swing - rotation around axis that is perpendicular to "direction" vector
	   The rotation can be composed back by
	   rotation = swing * twist

	   has singularity in case of swing_rotation close to 180 degrees rotation.
	   if the input quaternion is of non-unit length, the outputs are non-unit as well
	   otherwise, outputs are both unit
	*/
	void swingTwistDecomposition(const glm::quat& rotation,
	                             const glm::vec3& direction,
	                                   glm::quat& swing,
	                                   glm::quat& twist) {
	    glm::vec3 ra( rotation.x, rotation.y, rotation.z ); // rotation axis
	    glm::vec3 p = glm::dot(ra,direction) * glm::normalize(direction); // return projection v1 on to v2  (parallel component)
	    twist = glm::normalize(glm::quat(rotation.w, p.x, p.y, p.z));
	    swing = rotation * glm::conjugate(twist);
	}

	void EntityTweens::tick(float delta) {
		if(abs(e->motion.velocity.x) > 0 || abs(e->motion.velocity.z) > 0) {

			float rotTweenTime = 0.15f;
			float normRot = -atan2(e->motion.velocity.z, e->motion.velocity.x);
			glm::quat normRotQuat = glm::angleAxis(normRot, glm::vec3(0,1,0)); // this accounts for rotation perpendicular to the y axis
			glm::quat swing;
			glm::quat twist;
			swingTwistDecomposition(e->getRot(), glm::vec3(0,1,0), swing, twist);
			glm::quat diff = twist * glm::inverse(normRotQuat);
			float s = sgn(diff.y);
			float dist = s * constrain((1-pow(glm::dot(twist,normRotQuat),2)) * 0.2, 0, 0.0225);
			if(!std::isfinite(dist)) dist = 0;
			// This makes the entity lean when they are turning while moving
			glm::quat lean = glm::angleAxis(dist * PI / 2, glm::normalize(e->motion.velocity));
			//glm::quat orientation = rotTween->getFinalValue();
			e->setRot(lean * e->getRot());
			e->setRot(glm::normalize(e->getRot()));

			rotTween.init(e->getRot(), glm::normalize(normRotQuat), rotTweenTime, tween::linearQuat, &e->bb.rot);
		}

		rotTween.tick(delta);
		headTween.tick(delta);
		rampTween.tick(delta);
	}

	void EntityTweens::init(Entity* e) { this->e = e; }



	// COLLISION DETECTION

	struct VoxelCollisionData {
		WorldCoordinate pos; // The position of the voxel
		Voxel v;	 // The voxel we collided with (if this is nullptr, we didn't collide with anything)
	};

	VoxelCollisionData isCollidingWithVoxel(Entity * e, Volume normBox, WorldCoordinate v, bool multiDir) {
		if(!true) {
			normBox.size += glm::abs(v) / 1.1f;
			normBox.pos += v / 2.2f;
		} else {
			// TODO: Change this to a raycast?
			normBox.pos += v;
		}

		// This is here to make sure that we iterate in the proper direction of the Entity's movement
		WorldCoordinate sign = vecReplace(glm::sign(v), 0, 1);
		WorldCoordinate biSign = glm::floor((sign + WorldCoordinate(1)) / 2.f); // Sign used for rounding. Components are 0 if - or 1 if +
		VoxelCoordinate start = worldToVoxelCoordinate(normBox.pos - (normBox.size / 2.f) * sign + 1.f - biSign);
		VoxelCoordinate end = worldToVoxelCoordinate(normBox.pos + (normBox.size / 2.f) * sign + biSign + sign);

		VoxelCollisionData ans;
		VoxelCoordinate pos;

		WorldCoordinate a = glm::abs(v);
		int component = a.x > a.y? 0:1;
		component = a[component] > a.z? component:2;

		if (false) // TODO Change to true

		{   // Collision phase 1 V2 (probably only should be used for big guys, but you know, whatever)

			std::vector<CPUGridVoxelCollisionMeshUnit*> collisions = e->parent->map->getCollisionUnits(worldToVoxelCoordinate(normBox.getBottomCorner()),worldToVoxelCoordinate(normBox.size)+1);

			float bestCost = 100000.f;
			bool everFound = false;

			for(auto unit:collisions) {
				for(auto voxel:unit->solidVoxels) {
					WorldCoordinate wPos = voxelToWorldCoordinate(voxel);
					if(!normBox.intersects(Volume{wPos + WorldCoordinate(0.5), WorldCoordinate(1)})) continue;
					WorldCoordinate diff = wPos - normBox.pos;
					float cost = diff[component] * sign[component];
					if(cost < bestCost) {
						ans.pos = wPos + 0.5f;
						bestCost = cost;
					}
					everFound |= true;
				}
			}

			if(everFound) {
				VoxelCoordinate coord = worldToVoxelCoordinate(ans.pos);
				ans.v = e->parent->map->getVoxel(coord);
				return ans;
			}
			else
				return VoxelCollisionData{};
		}

		else

		{   // Collision phase 1 V1 (probably should only be used for little guys, but you know, whatever)
			for (pos.z = start.z; pos.z != end.z; pos.z += sign.z) {
				for (pos.y = start.y; pos.y != end.y; pos.y += sign.y) {
					for (pos.x = start.x; pos.x != end.x; pos.x += sign.x) {
						Voxel v = e->parent->map->getVoxel(pos);

						// Only pass the collision check if the voxel is solid
						if (!v.is(Voxel::SOLID) || !normBox.intersects(Volume{voxelToWorldCoordinate(pos) + WorldCoordinate(0.5), WorldCoordinate(1)})) continue;

						ans.v = v;
						ans.pos = voxelToWorldCoordinate(pos) + 0.5f;
						return ans;
					}
				}
			}
		}



		return ans;
	}

	float rampTime = 0.2;

	void MotionObject::handleTerrainCollisions(Entity * e) {
		Volume tvol = e->bb.withTerrain();
		tvol.pos -= delta_d;

		auto ramptween = [&](float change){e->tweens.rampTween.init(e->tweens.rampTween.getValue() - change, 0, rampTime, tween::cubicEaseOut<float>, &e->bb._visual.pos.y);};

		// Check collision in each individual axis we travel, because it's easier
		if (delta_d.y) {
			VoxelCollisionData ss = isCollidingWithVoxel(e, tvol, {0, delta_d.y, 0}, false);
			onGround = false;
			if (ss.v.is(Voxel::SOLID)) {
				if (delta_d.y < 0) tvol = tvol.getComponentChange(1, ss.pos.y + 0.5f + tvol.size.y / 2.f), velocity.y = 0, onGround = true;
				else tvol = tvol.getComponentChange(1, ss.pos.y - tvol.size.y / 2.f - 0.5f), velocity.y = 0;
			} else tvol = tvol.getTranslation({0, delta_d.y, 0});
		}
		if (delta_d.x) {
			VoxelCollisionData ss = isCollidingWithVoxel(e, tvol, {delta_d.x, 0, 0}, false);
			if (ss.v.is(Voxel::SOLID)) {
				float change = std::floor(tvol.getBottomCorner().y+1) - tvol.getBottomCorner().y;
				VoxelCollisionData rampSS = isCollidingWithVoxel(e, tvol, {delta_d.x, change , 0}, true);

				if (!rampSS.v.is(Voxel::SOLID)) {
					tvol = tvol.getTranslation({delta_d.x, change, 0});
					ramptween(change);
				} else {
					if (delta_d.x < 0)	tvol = tvol.getComponentChange(0, ss.pos.x + 0.5f + tvol.size.x / 2.f);
					else tvol = tvol.getComponentChange(0, ss.pos.x - tvol.size.x / 2.f - 0.5f);
					velocity.x = 0;
				}
			} else tvol = tvol.getTranslation({delta_d.x, 0, 0});
		}
		if (delta_d.z) {
			VoxelCollisionData ss = isCollidingWithVoxel(e, tvol, {0, 0, delta_d.z}, false);
			if (ss.v.is(Voxel::SOLID)) {
				float change = std::floor(tvol.getBottomCorner().y+1) - tvol.getBottomCorner().y;
				VoxelCollisionData rampSS = isCollidingWithVoxel(e, tvol, {0, change, delta_d.z}, true);

				if (!rampSS.v.is(Voxel::SOLID)) {
					tvol = tvol.getTranslation({0, change, delta_d.z});
					ramptween(change);
				} else {
					if (delta_d.z < 0)	tvol = tvol.getComponentChange(2, ss.pos.z + 0.5f + tvol.size.z / 2.f);
					else tvol = tvol.getComponentChange(2, ss.pos.z - tvol.size.z / 2.f - 0.5f);
					velocity.z = 0;
				}
			} else tvol = tvol.getTranslation({0, 0, delta_d.z});
		}

		delta_d = tvol.pos - e->bb.pos;
		e->setPos(tvol.pos); // Push ourselves back in the case we're colliding with a voxel
	}
}
