/*
 * sky.cpp
 *
 *  Created on: Jul 11, 2015
 *      Author: Dylan
 */

#include "sky.h"
#include "glcontroller.h"
#include "camera.h"
#include "framebuffer.h"
#include "glbuffer.h"
#include "worldstate.h"
#include "console.h"
#include "sky.h"
#include "sdlcontroller.h"
#include "colormath.h"
#include "resourcemanager.h"
#include "noise.h"
#include "settingsmanager.h"
#include "gpunoise.h"
#include "program.h"
#include "voxelmesh.h"
#include "miscmesh.h"
#include "anim.h"
#include "meshskylight.h"

#include <cmath>
#include <vector>

namespace vox {

	FogUniforms dayFogUniforms{glm::vec4(155.0/255.0, 226.0/255.0, 255.0/255.0, 1.0), 0.014f},
	            nightFogUniforms{glm::vec4(0.0/255.0, 0.0/255.0, 0.0/255.0,1.0), 0.017f};

	glm::vec4 someConst(1); // necessary.

	glm::vec3 expose(glm::vec3 rgb, float fct = 1.f) {
		return glm::vec3(1)-(glm::exp(-rgb*fct));
	}

	glm::vec3 noExpose(glm::vec3 rgb, float fct = 1.f) {
		return rgb;
	}

	struct CloudUniforms {
		float phaseMod;
		float phaseMul;
		float phaseAdd;
		glm::vec3 brightnessMul;
		float maxAlpha;
		float particleAlpha;

		void upload(CloudShaderProgram* p) {
			glUniform1f(p->phaseMod, phaseMod);
			glUniform1f(p->phaseMul, phaseMul);
			glUniform1f(p->phaseAdd, phaseAdd);
			glUniform3fv(p->brightnessMul, 1, &brightnessMul[0]);
			glUniform1f(p->maxAlpha, maxAlpha);
		}
	} dayCloudUniforms{0.9,2.0,1.0,{1,1,1},1.0,1.0}, nightCloudUniforms{0.8, 0.03, 0, {0.25,0.25,0.3}, 1.0,0.5};//, nightCloudUniforms{0.8, 0.03, 0, {1,1,1.75}, 1.0,0.4};

	struct CloudUniformTween {
		Tween<float> phaseMod, phaseMul, phaseAdd, maxAlpha, particleAlpha;
		Tween<glm::vec3> brightnessMul;

		void tick(float delta) {
			phaseMod.tick(delta);
			phaseMul.tick(delta);
			phaseAdd.tick(delta);
			maxAlpha.tick(delta);
			brightnessMul.tick(delta);
			particleAlpha.tick(delta);
		}

		void init(CloudUniforms* toTween, CloudUniforms start, CloudUniforms end, float time) {
			phaseMod.init(start.phaseMod,end.phaseMod,time,tween::cubicEaseInOut<float>,&(toTween->phaseMod));
			phaseMul.init(start.phaseMul,end.phaseMul,time,tween::cubicEaseInOut<float>,&(toTween->phaseMul));
			phaseAdd.init(start.phaseAdd,end.phaseAdd,time,tween::cubicEaseInOut<float>,&(toTween->phaseAdd));
			maxAlpha.init(start.maxAlpha,end.maxAlpha,time,tween::cubicEaseInOut<float>,&(toTween->maxAlpha));
			particleAlpha.init(start.particleAlpha,end.particleAlpha,time,tween::cubicEaseInOut<float>,&(toTween->particleAlpha));
			brightnessMul.init(start.brightnessMul,end.brightnessMul,time,tween::cubicEaseInOut<glm::vec3>,&(toTween->brightnessMul));
		}
	};

	struct FogUniformTween {
		Tween<glm::vec4> color;
		Tween<float> density;

		void tick(float delta) {
			color.tick(delta);
			density.tick(delta);
		}

		void init(FogUniforms* toTween, FogUniforms start, FogUniforms end, float time) {
			color.init(start.color,end.color,time,tween::quinticEaseInOut<glm::vec4>,&(toTween->color));
			density.init(start.density,end.density,time,tween::quinticEaseInOut<float>,&(toTween->density));
		}
	};

	CloudUniformTween cloudTween;
	CloudUniforms cloudUniforms = dayCloudUniforms;

	FogUniformTween fogTween;

	std::vector<GLint*> skyboxFineTune;
	glm::quat cubemapFaceRotations[6];

	struct AtmosValueSetEvent {
		int index;
		float value;
	};
	std::vector<AtmosValueSetEvent> sevents;

	void Sky::setAtmosValue(int index, float val) {
		sevents.push_back(AtmosValueSetEvent{index, val});
	}

	float dayLength = 720.0;
	float startTime = 360.0;
	float minutesPerSecond = 1.0;

	void Sky::mulTimeRate(float fac) {
		minutesPerSecond *= fac;
	}

	Sky::Sky() {
		time = startTime;

		meshSkyLighter = new MeshSkyLighter{};

		cloudParticleAtlas = mRes->getResource<Texture, Asset::Texture>("cloudparticles");
		cloudParticleManager = new InstancedCloudParticleManager(cloudParticleAtlas);

		std::vector<std::string> skyRenderTargetShaderDefines;
		std::vector<std::string> skyShaderDefines;
		if(mGL->CUBEMAP_FIX) {
			skyShaderDefines.push_back("CUBEMAP_FIX");
		}
		skyRenderTargetShader = new RenderToSkyShaderProgram("skyshader_rendertosky", false, skyRenderTargetShaderDefines);

		renderCloudsToScreen = new RenderTextureProgram("screenshader", false, {"CLOUDS_TO_SCREEN"});

		skyShader_noExposure = new RenderSkyShaderProgram("skyshader_rendersky", false, skyShaderDefines);
		skyShaderDefines.push_back("RESOLVE_HDR");
		skyShader_exposure = new RenderSkyShaderProgram("skyshader_rendersky", false, skyShaderDefines);
		starShader = new RenderSkyShaderProgram("skyshader_rendersky", false, {"STARS"});
		Rand cloudRand({rand(), rand(), rand()});
		cloudNoise = noise::Noise::readFile("clouds", cloudRand);

		moonTex = mRes->getResource<Texture, Asset::Texture>("moon");
		sunLensFlare = mRes->getResource<Texture, Asset::Texture>("lensflare_sun");
		starCubemap = mRes->getResource<CubeMap, Asset::CubeMap>("stars");

		lensflareBuf = TexturedMeshBuffer::createBillboard(sunLensFlare);

		billboardShader = new TexturedMeshShaderProgram("texturemesh", false, {"SKY"});

		downsampleClouds = mSettings->downsampleClouds;

		if(downsampleClouds) {
			downsampledCloudBuffer = new ColorBuffer({mSDL->viewportWidth/2, mSDL->viewportHeight/2}, GL_RGBA8, false);
		}

		std::vector<glm::quat> tempRotationVector;
		if(mGL->CUBEMAP_FIX) {
			tempRotationVector = {
					glm::angleAxis((float)0, glm::vec3(0,0,1)) * glm::angleAxis((float)PI, glm::vec3(0,1,0)),
					glm::angleAxis((float)PI/2, glm::vec3(0,0,1)),
					glm::angleAxis((float)PI/2, glm::vec3(0,1,0)) * glm::angleAxis((float)3*PI/2, glm::vec3(1,0,0)),
					glm::angleAxis((float)PI, glm::vec3(0,1,0)) * glm::angleAxis((float)PI/2, glm::vec3(1,0,0)),
					glm::angleAxis((float)3*PI/2, glm::vec3(1,0,0)) * glm::angleAxis((float)3*PI/2, glm::vec3(0,1,0)),
					glm::angleAxis((float)0, glm::vec3(1,0,0)) * glm::angleAxis((float)PI/2, glm::vec3(0,1,0))
			};
		} else {
			tempRotationVector = {
					glm::angleAxis((float)PI, glm::vec3(0,0,1)) * glm::angleAxis((float)PI, glm::vec3(0,1,0)),
					glm::angleAxis((float)PI, glm::vec3(0,0,1)),
					glm::angleAxis((float)PI/2, glm::vec3(0,1,0)) * glm::angleAxis((float)3*PI/2, glm::vec3(1,0,0)),
					glm::angleAxis((float)PI/2, glm::vec3(0,1,0)) * glm::angleAxis((float)PI/2, glm::vec3(1,0,0)),
					glm::angleAxis((float)PI, glm::vec3(1,0,0)) * glm::angleAxis((float)3*PI/2, glm::vec3(0,1,0)),
					glm::angleAxis((float)PI, glm::vec3(1,0,0)) * glm::angleAxis((float)PI/2, glm::vec3(0,1,0))
			};
		}
		for(int i = 0; i < 6; i++) cubemapFaceRotations[i] = tempRotationVector[i];

		int res = std::max(mSDL->viewportWidth * 19.f / 52.f - 184.f / 13.f, 128.f); // Note: Used to be a setting. Warning: Magic numbers.
		skyboxFineTune = {
				&skyRenderTargetShader->cameraHeight, &skyRenderTargetShader->rayleighBrightness, &skyRenderTargetShader->mieBrightness, &skyRenderTargetShader->sunBrightness,
				&skyRenderTargetShader->intensity, &skyRenderTargetShader->rayleighStrength, &skyRenderTargetShader->mieStrength,
				&skyRenderTargetShader->rayleighCollectionPower, &skyRenderTargetShader->mieCollectionPower, &skyRenderTargetShader->scatterStrength,
				&skyRenderTargetShader->mieDistribution, &skyRenderTargetShader->surfaceHeight
		};
		for(int i = 0; i < 3; i++) skyBoxes[i] = new CubeMapColorBuffer({res,res});

		float cloudHeight = 1.0;
		float cloudStretchDist = 100; // how far the clouds stretch (feel free to make this absurdly high)
		cloudLayerBuffer = new StackBuffer<glm::vec3>(ARRAY, STATIC);
		cloudLayerBuffer->push({
			{-cloudStretchDist, cloudHeight, -cloudStretchDist},
			{ cloudStretchDist, cloudHeight, -cloudStretchDist},
			{ cloudStretchDist, cloudHeight,  cloudStretchDist},
			{ cloudStretchDist, cloudHeight,  cloudStretchDist},
			{-cloudStretchDist, cloudHeight,  cloudStretchDist},
			{-cloudStretchDist, cloudHeight, -cloudStretchDist}
		});
		cloudLayerBuffer->generateBuffer();

		cloudShader = new CloudShaderProgram("clouds", false, {"DAY"}, {"noise_lib"}, {noise::getGPUNoiseString("noise", 3, cloudNoise)});

		atmHalfBuffers[0] = new StackBuffer<glm::vec3>(ARRAY, STATIC);
		atmHalfBuffers[0]->push({{-1,0,0},{1,0,0},{1,1,0},{-1,1,0}});
		atmHalfBuffers[0]->generateBuffer();
		atmHalfBuffers[1] = new StackBuffer<glm::vec3>(ARRAY, STATIC);
		atmHalfBuffers[1]->push({{-1,-1,0},{1,-1,0},{1,0,0},{-1,0,0}});
		atmHalfBuffers[1]->generateBuffer();

		renderTextureBuffer = new StackBuffer<glm::vec3>(ARRAY, STATIC);
		renderTextureBuffer->push({{-1,1,-1}, {1,1,-1}, {1,1,1}, {-1,1,1}});
		renderTextureBuffer->generateBuffer();

		for(int dir = 0; dir < 3; dir++) {
			bool XorY = dir <= 1; // x and y use the same buffer, z uses a different one
			// If this goes in the x direction, we split in halves of Z.
			// If this goes in the z direction, we split in halves of X.
			// If this goes in the y direction, we can split either way.
			for(int half = 0; half < 2; half++) {
				for(int neg = 0; neg < 2; neg++) {
					cloudLayerHalfBuffers[dir*2 + neg][half] = new StackBuffer<glm::vec3>(ARRAY, STATIC);
					cloudLayerHalfBuffers[dir*2 + neg][half]->push({
						glm::vec3(-cloudStretchDist, cloudHeight, -cloudStretchDist) * (XorY? glm::vec3(1,1,1-half): glm::vec3(1-half,1,1)),
						glm::vec3( cloudStretchDist, cloudHeight, -cloudStretchDist) * (XorY? glm::vec3(1,1,1-half): glm::vec3(half,1,1)),
						glm::vec3( cloudStretchDist, cloudHeight,  cloudStretchDist) * (XorY? glm::vec3(1,1,half): glm::vec3(half,1,1)),
						glm::vec3(-cloudStretchDist, cloudHeight,  cloudStretchDist) * (XorY? glm::vec3(1,1,half): glm::vec3(1-half,1,1))
					});
					cloudLayerHalfBuffers[dir*2 + neg][half]->generateBuffer();
				}
			}
		}

		// Do our initial skybox renders
		for(int bn = 0; bn < 2; bn++) {
			for(int fa = 0; fa < 6; fa++) {
				renderToSkyBox(bn, fa, 0);
				renderToSkyBox(bn, fa, 1);
				renderCloudsToSkyBox(bn, fa, 0);
				renderCloudsToSkyBox(bn, fa, 1);
			}
		}

		std::function<glm::vec3(glm::vec3,float)> exposureFunction = expose; // whether or not we want to expose our constants
		createCloudParticles(expose);
	}


	Sky::~Sky() {
		delete skyRenderTargetShader;
		delete skyShader_exposure;
		delete starShader;
		delete skyShader_noExposure;
		for(int i = 0; i < 3; i++) delete skyBoxes[i];
	}

	glm::vec3 lookUp[] = {{1,0,0}, {-1,0,0}, {0,1,0}, {0,-1,0}, {0,0,1}, {0,0,-1}}; // i dont remember what this does

	void Sky::renderToSkyBox(int boxNumber, int face, int half) {
		glDisable(GL_DEPTH_TEST);
		mGL->bindFrameBuffer(skyBoxes[boxNumber]);
		skyBoxes[boxNumber]->selectFace(face);

		SkyboxSnapShot s = skyboxSnapShots[boxNumber];
		mGL->bindProgram(skyRenderTargetShader);
		glUniform3fv(skyRenderTargetShader->faceDirection, 1, &lookUp[face][0]);
		glUniform1i(skyRenderTargetShader->layer, face);
		glUniform3fv(skyRenderTargetShader->sunPos, 1, &s.sunInverseDir[0]);

		glm::mat4 model(1.0);
		model *= glm::toMat4(glm::angleAxis((float)3*PI/2, glm::vec3(0,1,0)));
		//model *= glm::toMat4(glm::inverse(s.sunrot));
		model *= glm::toMat4(cubemapFaceRotations[face]);
		glUniformMatrix4fv(skyRenderTargetShader->model, 1, GL_FALSE, &model[0][0]);


		skyRenderTargetShader->beginRender();
		atmHalfBuffers[half]->bindBuffer();
		glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE,0,(void*) 0);
		VoxelMeshBuffer::indexBuffer->bindBuffer();

		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*) 0);
		skyRenderTargetShader->endRender();
		glEnable(GL_DEPTH_TEST);
	}
	int dayOrNight;

	void Sky::calculateSkyLight() {
		float dTime = fmod(time, dayLength) / dayLength;
		float sTime = fmod(time, 100 * dayLength) / (100 * dayLength);
		float sTimeMoon = fmod(time, 50 * dayLength) / (50 * dayLength);
		float sunsetNumber = 0.17;
		float sunsetdropoff = 0.04;
		float sunTemp = 5000;
		float moonTemp = 35000;

		timeOfDay = dTime;

		float invsunsetdropoff = 1.f/sunsetdropoff;
		float pow;

		dayOrNight = fmod(time, 2*dayLength) < dayLength? DAY: NIGHT;

		if(dTime > sunsetdropoff && dTime < 1.0 - sunsetdropoff) {
			pow = 1.f;
		} else {
			float t;
			if(dTime < sunsetdropoff) t = invsunsetdropoff * dTime;
			else t = (invsunsetdropoff * (1 - dTime));
			pow = (-cos(t * PI) + 1) / 2.f;
		}

		sTime = (sTimeMoon = -0.33);
		float sAngle = (1+2*sunsetNumber)*PI*dTime - sunsetNumber*PI;
		if(dayOrNight == DAY) { // daytime
			sunInverseDir = {cos(sAngle) * cos(2 * PI * (sTime + 0.33)), sin(sAngle), cos(sAngle) * sin(2 * PI * (sTime + 0.33))};

			sunInverseDir += glm::vec3(0,0.15,0.15);
			sunInverseDir = glm::normalize(sunInverseDir);

			glm::vec3 sunColor = glm::normalize( glm::normalize(Color::RGBFromTemperature(sunTemp)) + glm::vec3(50) ); // dilute the sun's actual color (it's atmospheric scattering, okay?)

			sun = SunLight{sunInverseDir, pow * 2.2f, sunColor, 0.5};
			sunRot = glm::angleAxis((float)(-2*PI*(0.33+sTime)), glm::vec3(0,1,0));
		} else { // nighttime
			sunInverseDir = {cos(sAngle) * cos(2 * PI * (sTimeMoon + 0.33)), sin(sAngle), cos(sAngle) * sin(2 * PI * (sTimeMoon + 0.33))};

			sunInverseDir += glm::vec3(0,0.15,0.15);
			sunInverseDir = glm::normalize(sunInverseDir);

			sun = SunLight{sunInverseDir, pow * 1.3f, glm::normalize(Color::RGBFromTemperature(moonTemp)), 0.5};
			sunRot = glm::angleAxis((float)(-2*PI*(0.33+sTimeMoon)), glm::vec3(0,1,0));
		}
		sunRot *= glm::quat(glm::vec3(0, PI/2, sAngle + 3*PI/2.0));

		sunInverseDir = glm::normalize(sunInverseDir);
	}

	float t = 0;
	float u = 0;

	glm::mat4 faceRotationsCloud[6] = {
		glm::lookAt(glm::vec3(0.f), {-1,0,0}, {0,1,0}),
		glm::lookAt(glm::vec3(0.f), {1,0,0}, {0,0,-1}),
		glm::lookAt(glm::vec3(0.f), {0,1,0}, {0,0,-1}),
		glm::lookAt(glm::vec3(0.f), {0,-1,0}, {-1,0,0}),
		glm::lookAt(glm::vec3(0.f), {0,0,-1}, {-1,0,0}),
		glm::lookAt(glm::vec3(0.f), {0,0,1}, {0,1,0}),
	};

	glm::mat4 faceRotationsCloudnofix[6] = {
		glm::lookAt(glm::vec3(0.f), {-1,0,0}, {0,-1,0}),
		glm::lookAt(glm::vec3(0.f), {1,0,0}, {0,-1,0}),
		glm::lookAt(glm::vec3(0.f), {0,1,0}, {0,0,-1}),
		glm::lookAt(glm::vec3(0.f), {0,-1,0}, {0,0,1}),
		glm::lookAt(glm::vec3(0.f), {0,0,-1}, {0,-1,0}),
		glm::lookAt(glm::vec3(0.f), {0,0,1}, {0,-1,0}),
	};

	void Sky::renderCloudsToSkyBox(int boxNumber, int face, int half) {
		mGL->bindFrameBuffer(skyBoxes[boxNumber]);
		skyBoxes[boxNumber]->selectFace(face);

		SkyboxSnapShot s = skyboxSnapShots[boxNumber];

		CloudShaderProgram* sdr = cloudShader;
		mGL->bindProgram(sdr);

		cloudUniforms.upload(sdr);

		glm::mat4 model(1.0);
		glm::vec3 pos(0.01);
		pos[face/2] = 1.0;
		model *= glm::toMat4(glm::angleAxis((float)PI, glm::vec3(0,1,0)));
		//model *= glm::toMat4(glm::inverse(s.sunrot));

		glm::mat4 view = mGL->CUBEMAP_FIX?
				faceRotationsCloud[face]:
				faceRotationsCloudnofix[face];
		glm::mat4 vp = glm::perspective<float>(PI/2,1.f,0.01f,1000.f) * view;
		glUniformMatrix4fv(sdr->M, 1, GL_FALSE, &model[0][0]);
		glUniformMatrix4fv(sdr->VP, 1, GL_FALSE, &vp[0][0]);
		glUniform1i(sdr->face, face);
		glUniform1f(sdr->worldTime, s.time);
		glm::vec3 d = {-s.sunInverseDir.x, s.sunInverseDir.y, s.sunInverseDir.z};
		d = glm::normalize(d);

		glUniform3fv(sdr->sundir, 1, &d[0]);

		sdr->beginRender();
		cloudLayerHalfBuffers[face][half]->bindBuffer();
		glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE,0,(void*) 0);
		VoxelMeshBuffer::indexBuffer->bindBuffer();

		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*) 0);
		sdr->endRender();
	}

	int sti = 0;


	void Sky::renderTextureToScreen(TexturedMeshBuffer* mesh, Texture* tex, glm::quat rot, glm::vec3 scale, const Camera& cam, int layer, glm::vec4 color) {
		glDisable(GL_CULL_FACE);
		mGL->bindTexture(tex, 0);

		glUniform1i(billboardShader->tex, 0);
		float depth = 1.f - layer / 10000.f;
		glUniform1f(billboardShader->depth, depth);
		glUniform4fv(billboardShader->color, 1, &color[0]);

		glm::mat4 model(1.f);
		model = glm::translate(model, cam.pos);
		model *= glm::toMat4(glm::angleAxis((float)3*PI/2, glm::vec3(0,1,0)));
		model *= glm::toMat4(rot);
		model *= glm::toMat4(glm::angleAxis((float)PI, glm::vec3(0,0,1)));
		model *= glm::toMat4(glm::angleAxis((float)3*PI/2, glm::vec3(1,0,0)));
		model *= glm::toMat4(glm::angleAxis((float)PI/2, glm::vec3(0,0,1)));
		model = glm::translate(model, glm::vec3(0,0,-1));
		model = glm::scale(model, scale);
		glm::mat4 mvp = cam.getMVP(model);
		glUniformMatrix4fv(billboardShader->MVP, 1, GL_FALSE, &mvp[0][0]);

		mesh->verts->bindBuffer();
		glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE,sizeof(TexturedMeshVertex),(void*) offsetof(TexturedMeshVertex, pos));
		glVertexAttribPointer(1,2,GL_FLOAT,GL_FALSE,sizeof(TexturedMeshVertex),(void*) offsetof(TexturedMeshVertex, UV));
		mesh->inds->bindBuffer();

		glDrawElements(GL_TRIANGLES, mesh->inds->size*3, GL_UNSIGNED_INT, (void*)0);
	}

	void Sky::renderCubeMap(const Camera& cam, glm::quat rot, RenderSkyShaderProgram* p, int layer, glm::vec4 color, Texture* t1, Texture* t2) {
		glm::mat4 skyMatrix(1.0);
		skyMatrix = glm::translate(skyMatrix, cam.pos);
		skyMatrix *= glm::toMat4(rot);
		skyMatrix = cam.getMVP(skyMatrix);
		glUniformMatrix4fv(p->MVP, 1, GL_FALSE, &skyMatrix[0][0]);
		glUniform4fv(p->color, 1, &color[0]);

		glUniform1i(p->tex, 0);
		glUniform1i(p->tex2, 1);
		float depth = 1.f - layer / 10000.f;
		glUniform1f(p->depth, depth);

		mGL->bindTexture(t1, 0);
		if(t2) mGL->bindTexture(t2, 1);
		mGL->skyBoxBuffer->bindBuffer();
		glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE,0,0);
		mGL->skyBoxTexLayerBuffer->bindBuffer();
		glVertexAttribPointer(1,1,GL_FLOAT,GL_FALSE,0,0);
		glDrawArrays(GL_TRIANGLES, 0, mGL->skyBoxBuffer->getMaxSize());
	}


	void Sky::createCloudParticles(std::function<glm::vec3(glm::vec3,float)> exposureFunction) {
		int c = 36; // temporary test helpy thing

		std::vector<glm::vec2> points;
		glm::ivec2 curPos;
		for(curPos.x = -c; curPos.x <= c; ++curPos.x)
		for(curPos.y = -c; curPos.y <= c; ++curPos.y) {
			float scale = 15000.f;
			scale /= c;
			glm::vec2 pos = glm::vec2(curPos) * scale;
			points.push_back(pos);
		}

		Rand tempRand;

		float cloudLayerHeight = 7000;
		float heightDeviation = 2000;
		float radiusOfEarth = 20000;

		// generate random points on sphere, and put cloud particles there based on noise
		// TODO: use more consistent random sampling
		for(unsigned int i = 0; i < points.size(); i++) {
			float ht = cloudLayerHeight + tempRand.getFloat(-heightDeviation, heightDeviation);

			glm::vec3 worldPos(points[i].x, ht, points[i].y);

			worldPos = glm::normalize(worldPos) * 15000.f;

			//if(worldPos.y < -1000.f) continue;

			//worldPos.y = 3000.f;

			glm::vec2 pos = glm::vec2(worldPos.x,worldPos.z) / 4000.f;
			float distWeight = glm::min(1.f,1.f / glm::length(pos / 2.f));

			if(tempRand.flipCoin(cloudNoise->noise3D({pos.x,time,pos.y}) * distWeight)) {
				cloudParticleManager->storeForRender(CloudParticleDisplayObject(worldPos, tempRand.getInt(0,2)));
			}
		}

		printlnf(TEMPORARY, "%i Particles Created.", cloudParticleManager->cloudParticles.size());
	}

	void Sky::renderCloudParticles(const Camera& cam, float time) {
		glDepthMask(GL_FALSE);
		glDisable(GL_CULL_FACE);

		mGL->bindProgram(((InstancedCloudParticleManager*) cloudParticleManager)->billboardShader);
		glm::vec4 color = glm::vec4(1,1,1,cloudUniforms.particleAlpha);
		glUniform4fv( ((InstancedCloudParticleManager*) cloudParticleManager)->billboardShader->globalColor, 1, &color[0]);
		cloudParticleManager->render(cam);

		glEnable(GL_CULL_FACE);
		glDepthMask(GL_TRUE);
	}



	void Sky::renderMoonAndStars(const Camera& cam, std::function<glm::vec3(glm::vec3,float)> exposureFunction) {
		mGL->bindProgram(starShader);
		starShader->beginRender();
		renderCubeMap(cam, glm::angleAxis((float)3*PI/2, glm::vec3(0,1,0))*glm::inverse(sunRot), starShader, curLayer++, glm::vec4(exposureFunction(glm::vec3(someConst),1),1), starCubemap);
		starShader->endRender();
		mGL->bindProgram(billboardShader);
		billboardShader->beginRender();
		renderTextureToScreen(lensflareBuf, moonTex, glm::inverse(sunRot), glm::vec3(0.25), cam, curLayer++, glm::vec4(exposureFunction(glm::vec3(someConst),1),1));
		billboardShader->endRender();
	}

	void Sky::renderSunLensFlare(const Camera& cam, std::function<glm::vec3(glm::vec3,float)> exposureFunction) {
		mGL->bindProgram(billboardShader);
		float closeness2Sunset;
		float sunsetdropoff = 0.4;
		float invsunsetdropoff = 1.f/sunsetdropoff;

		dayOrNight = fmod(time, 2*dayLength) < dayLength? DAY: NIGHT;

		if(timeOfDay > sunsetdropoff && timeOfDay < 1.0 - sunsetdropoff) {
			closeness2Sunset = 1.f;
		} else {
			float t;
			if(timeOfDay < sunsetdropoff) t = invsunsetdropoff * timeOfDay;
			else t = (invsunsetdropoff * (1 - timeOfDay));
			closeness2Sunset = (-cos(t * PI) + 1) / 2.f;
		}
		billboardShader->beginRender();
		renderTextureToScreen(lensflareBuf, sunLensFlare, glm::inverse(sunRot), glm::vec3(closeness2Sunset), cam, curLayer++, someConst);
		billboardShader->endRender();
	}

	Accumulator skyAccumulator(1.f/60.f);

	void Sky::prepareForRender(const Camera& cam) {
		cloudParticleManager->prepareForRender(cam.pos);

		if(downsampleClouds) {
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			mGL->bindFrameBuffer(downsampledCloudBuffer);
			downsampledCloudBuffer->clear();
			renderCloudParticles(cam, time);
		}


		if(!skyAccumulator.attempt()) return; // we only want to do this 60 times per second

		sti = (sti+1)%(24);

		// And you could have it all
		// My empire of code

		if(sti / 12 < 1) { // ticks [0,12): render to the skybox
			int face = sti / 2;
			if(skyboxSnapShots[2].dayOrNight == DAY) {
				if(sti % 2 == 0) {
					renderToSkyBox(2, face, 0);
				} else {
					renderToSkyBox(2, face, 1);
				}
			}
			else {
				if(sti % 2 == 0) {
					mGL->bindFrameBuffer(skyBoxes[2]);
					skyBoxes[2]->selectFace(face);
					skyBoxes[2]->clear();
				}
			}
		} else if(sti / 12 < 2) { // ticks [12,24): render clouds
			//int face = (sti-12) / 2;
			if(sti % 2 == 0) {
				// renderCloudsToSkyBox(2, face, 0);
			} else {
				// renderCloudsToSkyBox(2, face, 1);
			}
		}

		if(sti == 4 * 6 - 1) { // we finished the render, shuffle around the skyboxes to render our new skybox. Also begin rendering to the next.
			auto t = skyBoxes[0];
			skyBoxes[0] = skyBoxes[1];
			skyBoxes[1] = skyBoxes[2];
			skyBoxes[2] = t;
			skyboxSnapShots[0] = skyboxSnapShots[1];
			skyboxSnapShots[1] = skyboxSnapShots[2];
			skyboxSnapShots[1].time = time;
			skyboxSnapShots[2] = SkyboxSnapShot(
					time, // We don't care about the worldTime until we're done rendering
					dayOrNight,
					sunInverseDir,
					sunRot
			);
		}
	}

	int curTweenPhase = NIGHT;

	void Sky::tick(float delta) {
		skyAccumulator.accumulate(delta);
		time += delta * minutesPerSecond;

		if((fmod(time,dayLength)) > dayLength/2.f && dayOrNight != curTweenPhase) { // init the next tween for cloud uniforms, and for the fog color
			curTweenPhase = dayOrNight;
			CloudUniforms current = dayOrNight == NIGHT? nightCloudUniforms:dayCloudUniforms;
			CloudUniforms next =    dayOrNight == DAY?   nightCloudUniforms:dayCloudUniforms;
			cloudTween.init(&cloudUniforms, current, next, dayLength);

			FogUniforms currentf = dayOrNight == NIGHT? nightFogUniforms:dayFogUniforms;
			FogUniforms nextf =    dayOrNight == DAY?   nightFogUniforms:dayFogUniforms;
			fogTween.init(&fogUniforms, currentf, nextf, dayLength);
		}

		meshSkyLighter->sun = this->sun;

		cloudTween.tick(delta * minutesPerSecond);
		fogTween.tick(delta * minutesPerSecond);

		calculateSkyLight();

		mGL->bindProgram(skyRenderTargetShader);

		for(auto&& sevent: sevents) {
			glUniform1f(*skyboxFineTune[sevent.index], sevent.value);
		} sevents.clear();
	}

	void Sky::render(const Camera& cam, bool exposure) {
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		std::function<glm::vec3(glm::vec3,float)> exposureFunction = exposure? noExpose : expose; // whether or not we want to expose our constants
		RenderSkyShaderProgram* sdr = exposure? skyShader_exposure : skyShader_noExposure;

		curLayer = 0;
		if(dayOrNight == NIGHT) renderMoonAndStars(cam, exposureFunction);


		mGL->bindProgram(sdr);

		float timeDiff = skyboxSnapShots[1].time - skyboxSnapShots[0].time;
		if(abs(timeDiff) < 0.001) {
			timeDiff = 0;
		} else {
			float nTime = time - timeDiff;
			timeDiff = (nTime - skyboxSnapShots[0].time) / timeDiff;
		}

		glUniform1f(sdr->worldTimeDiff, timeDiff);

		sdr->beginRender();
		renderCubeMap(cam, glm::quat(1,0,0,0), sdr, curLayer++, someConst, skyBoxes[0]->target, skyBoxes[1]->target);
		sdr->endRender();

		if(downsampleClouds) {
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			mGL->bindProgram(renderCloudsToScreen);
			renderCloudsToScreen->beginRender();
			mGL->render(renderCloudsToScreen, downsampledCloudBuffer->target, glm::vec2(0), glm::vec2(1), 0.9991, 0, glm::vec4(cloudUniforms.brightnessMul,1));
			renderCloudsToScreen->endRender();
		} else {
			renderCloudParticles(cam, time);
		}
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		//if(skyboxSnapShots[2].dayOrNight == DAY)
		//	renderSunLensFlare(cam); // lens flare goes on top of clouds
	}

	struct LensFlareElement {
		float distance;
		float size;
	};

	struct LensFlare {
		std::vector<LensFlareElement> elements;
		float maxSize;
		float scale;
	};

	/*void renderLensFlares(LensFlare flare, glm::vec2 l, glm::vec2 c, glm::vec2 screenSize, Camera& cam) {

		float     maxflaredist, flaredist, flaremaxsize, flarescale;
		float     alpha;    // Piece parameters;
		int     i;

		glm::vec2 p, size;

		// Compute how far off-center the flare source is.
		maxflaredist = glm::length(c);
		flaredist = glm::distance(c,l);
		flaredist = (maxflaredist - flaredist);
		flaremaxsize = (screenSize.y * flare.maxSize);
		flarescale = (screenSize.y * flare.scale);

		// Destination is opposite side of centre from source
		glm::vec2 d = c + (c-l);

		// Render each element.
		for (i = 0; i < flare.elements.size(); ++i) {
			LensFlareElement element = flare.elements[i];

			// Position is interpolated along line between start and destination.
			glm::vec2 p = l*(1.f - element.distance) + d*element.distance;

			// Piece size are 0 to 1; flare size is proportion of
			// screen width; scale by flaredist/maxflaredist.
			glm::vec2 size;
			size.x = (flaredist*flarescale*element.size)/maxflaredist;

			// Width gets clamped, to allows the off-axis flares
			// to keep a good size without letting the elements get
			// too big when centered.
			if (size.x > flaremaxsize) {
				size.x = flaremaxsize;
			}

			// Flare elements are square (round) so height is just
			// width scaled by aspect ratio.
			size.y = size.x / cam.aspectRatio;

			//drawQuad( px - width/2, py - height/2, width, height, element->texture, argb );
		}
	}*/

}



