/*
 * worldstaterender.cpp
 *
 *  Created on: Jul 18, 2015
 *      Author: Dylan
 */

#include "worldstate.h"
#include "shadowmap.h"
#include "framebuffer.h"
#include "glbuffer.h"
#include "settingsmanager.h"
#include "program.h"
#include "io.h"
#include "flora.h"
#include "chunk.h"
#include "sky.h"
#include "resourcemanager.h"
#include "shapes.h"
#include "skeleton.h"
#include "effectmanager.h"
#include "worldstaterender.h"
#include "entitymanager.h"
#include "particle.h"
#include "ssao.h"
#include "meshskylight.h"
#include "blur.h"
#include "distortscreen.h"
#include "supercooleffects.h"

namespace vox {

	bool useDeferredPipeline;
	bool useBloom;

	int TILES = 24;

	struct WorldStateRenderChunkMesh: public ChunkMeshRenderer {
		WorldStateRenderHandler* parent;
		WorldStateRenderChunkMesh(WorldStateRenderHandler* parent): parent(parent) {}
		void render(ChunkMeshDisplayObject ch) {
			parent->renderChunkMesh(ch);
		}
		~WorldStateRenderChunkMesh() {}
	};

	struct WorldStateRenderEntityMesh: public EntityMeshRenderer {
		WorldStateRenderHandler* parent;
		WorldStateRenderEntityMesh(WorldStateRenderHandler* parent): parent(parent) {}
		void render(EntityMeshDisplayObject en) {
			parent->renderEntityMesh(en);
		}
		~WorldStateRenderEntityMesh() {}
	};

	// Renders a volume
	struct DebugVolumeRender: public VolumeRenderer {
		WorldStateRenderHandler* parent;
		DebugVolumeRender(WorldStateRenderHandler* parent): parent(parent) {}

		void render(VolumeDisplayObject v) {
			parent->renderVolume(v);
		}
	};

	// Renders a sillhouette outline around the entity
	struct SillhouetteEntityRender: public EntityMeshRenderer {
		WorldStateRenderHandler* parent;
		glm::vec4 color = {1,1,0,0};
		float thickness = 10; // in pixels
		SillhouetteEntityRender(WorldStateRenderHandler* parent): parent(parent) {}

		void render(EntityMeshDisplayObject en) {
			parent->renderEntityMeshSillhouette(en, color);
		}
	};

	struct EntityShadowRenderer: public DropShadowRenderer {
		WorldStateRenderHandler* parent;
		EntityShadowRenderer(WorldStateRenderHandler* parent): parent(parent) {}

		void render(DropShadowDisplayObject v) {
			parent->renderDropShadow(v);
		}
	};

	struct WorldStateRenderWater: public ChunkMeshRenderer {
		WorldStateRenderHandler* parent;
		WorldStateRenderWater(WorldStateRenderHandler* parent): parent(parent) {}

		void render(ChunkMeshDisplayObject ch) {
			parent->renderChunkWater(ch);
		}
	};

	struct WorldStateRenderDynamicLights: public DynamicLightRenderer {
		WorldStateRenderHandler* parent;
		WorldStateRenderDynamicLights(WorldStateRenderHandler* parent): parent(parent) {}

		void render(DynamicLightDisplayObject d) {
			parent->dynamicLightsRenderedThisTick.push_back(d);
		}
	};

	struct WorldStateRenderEffects : public EffectRenderer {
		WorldStateRenderHandler * parent;
		WorldStateRenderEffects(WorldStateRenderHandler * parent) : parent(parent) {}

		void render(BillboardDisplayObject b) {
			parent->particleManager->storeForRender(b);
		}
		void render(DynamicLightDisplayObject d) {
			parent->dynamicLightRenderer->render(d);
		}
	};

	struct WorldStateRenderChunkWireFrame: public ChunkMeshRenderer {
		WorldStateRenderHandler* parent;
		WorldStateRenderChunkWireFrame(WorldStateRenderHandler* parent): parent(parent) {}

		void render(ChunkMeshDisplayObject d) {
			parent->renderWireFrame(d);
		}
	};

	// I'm lazy, so let's just put this here :3
	Texture1D* createOcclusionCurve() {
		float GPUOcclusionData[101];

		for(int i = 0; i < 101; i++) {
			float val = i / 100.f;
			GPUOcclusionData[i] = pow(val, 0.45) * .425 + .575;
		}
		GPUOcclusionData[100] = 1;
		Texture1D* ret = createTexture1D(101, GPUOcclusionData, GL_R16F, GL_LINEAR, GL_CLAMP_TO_EDGE, GL_RED, GL_FLOAT);
		return ret;
	}

	Texture* getDitherSampler() {
		unsigned char data[64]= {
			0, 32,  8, 40,  2, 34, 10, 42,
			48, 16, 56, 24, 50, 18, 58, 26,
			12, 44,  4, 36, 14, 46,  6, 38,
			60, 28, 52, 20, 62, 30, 54, 22,
			3, 35, 11, 43,  1, 33,  9, 41,
			51, 19, 59, 27, 49, 17, 57, 25,
			15, 47,  7, 39, 13, 45,  5, 37,
			63, 31, 55, 23, 61, 29, 53, 21
		};
		for(auto& d:data) d*=4; // We want bytes in the range of 0-255, so that we don't need to do any multiplication in the shader.
		return createTexture2D({8,8}, data, GL_R8, GL_NEAREST, GL_REPEAT, GL_RED, GL_UNSIGNED_BYTE);
	}

	const int skells = 1;

	Texture1D* occlusionCurve;
	Texture* ditherSampler;
	Texture* billboardAtlas;
	Texture* shadowTex;
	TexturedMeshBuffer* dropShadowBuf;

	void WorldStateRenderHandler::tick(float delta) {
		sky->tick(delta);
		effectManager->tick(delta);
		liquidManager->tick(delta);
	}

	WorldStateRenderHandler::~WorldStateRenderHandler() {
		delete cShader;
		delete eShader;
		delete dShader;
		delete shadowMap;
		delete dynamicLights;
		delete dynamicLightTileData;
		delete screenTiles;
		//if(useDeferredPipeline)
		//	delete deferredRender;
		delete effectManager;
		delete sky;
	}
	
	bool useMSAA = true;

	WorldStateRenderHandler::WorldStateRenderHandler(WorldState* parent): parent(parent) {
		int liquidRenderType = liquidRenderingTypeMap["textured"]; // Note: Used to be a setting.
		liquidManager = new LiquidManager(liquidRenderType);
		effectManager = new EffectManager();
		effectManager->init(parent);
		billboardAtlas = mRes->getResource<Texture, Asset::Texture>("particles");
		particleManager = new InstancedParticleManager(billboardAtlas);
		shadowTex = mRes->getResource<Texture, Asset::Texture>("shadow2");

		ssaoHandler = new SSAOHandler({mSDL->viewportWidth,mSDL->viewportHeight},false);

		StackBuffer<TexturedMeshVertex>* verts = new StackBuffer<TexturedMeshVertex>(ARRAY, STATIC);
		float h = 1.f/2.f;
		verts->push({
			TexturedMeshVertex{{-h,0,-h},{0,0}}, TexturedMeshVertex{{-h,0,h},{1,0}}, TexturedMeshVertex{{h,0,h}, {1,1}}, TexturedMeshVertex{{h,0,-h}, {0,1}}
		});

		forwardPass_preblit = nullptr;//new ColorBuffer({mSDL->viewportWidth,mSDL->viewportHeight},GL_RGBA8,true,GL_LINEAR,GL_LINEAR,true,16);
		forwardPass_postblit = nullptr;//new ColorBuffer({mSDL->viewportWidth,mSDL->viewportHeight},GL_RGBA8,true,GL_LINEAR,GL_LINEAR,false,1);

		verts->generateBuffer();

		StackBuffer<Triangle<int>>* inds = new StackBuffer<Triangle<int>>(ELEMENT_ARRAY, STATIC);
		inds->push({{0,1,2},{2,3,0}});
		inds->generateBuffer();

		dropShadowBuf = new TexturedMeshBuffer(verts, inds);

		useDeferredPipeline = true;
		useBloom = useDeferredPipeline && mSettings->bloom;

		if(useBloom) {
			glm::vec2 bloomSize(mSDL->viewportWidth, mSDL->viewportHeight);
			bloomSize /= 2; // 1/2 the framebuffer size == 4x performance
			bloomBlurrer = new BlurHandler(bloomSize, GL_RGBA16F, 4, true);
		}

		std::vector<std::string> dShaderDefines;
		std::vector<std::string> cShaderDefines, eShaderDefines = {};
		std::vector<std::string> lShaderDefines = {"INCLUDE_DYNAMIC_LIGHT_HANDLING", liquidRenderingTypeDefines[liquidRenderType]};

		enableShadowmap = false; // Note: Used to be a setting.
		if(!enableShadowmap) {
			dShaderDefines.push_back("NO_SHADOWMAP");
			cShaderDefines.push_back("NO_SHADOWMAP");
			eShaderDefines.push_back("NO_SHADOWMAP");
			lShaderDefines.push_back("NO_SHADOWMAP");
		}
		dShaderDefines.push_back("INCLUDE_DYNAMIC_LIGHT_HANDLING");
		if(useBloom) {
			dShaderDefines.push_back("USE_BLOOM");
		}
		if(useDeferredPipeline) {
			cShaderDefines.push_back("DEFERRED");
			eShaderDefines.push_back("DEFERRED");
		} else {
			cShaderDefines.push_back("INCLUDE_DYNAMIC_LIGHT_HANDLING");
			eShaderDefines.push_back("INCLUDE_DYNAMIC_LIGHT_HANDLING");
		}

		cShader = new ChunkShaderProgram("chunks", false, cShaderDefines, {"stdlib"}, {}, true);
		cShaderDefines.push_back("FORWARD");

		cShader_forwardPass = new ChunkShaderProgram("chunks", false, cShaderDefines, {"stdlib"}, {}, false);

		eShader = new EntityShaderProgram("entities", false, eShaderDefines, {"stdlib"}, {}, useDeferredPipeline);
		lShader = new LiquidShaderProgram("fancywater", false, lShaderDefines, {"stdlib"}, {});
		dShader = new DeferredShaderProgram("deferred", false, dShaderDefines, {"stdlib"});
		coloredShader = new ColoredShaderProgram("colored");
		billboardShader = new TexturedMeshShaderProgram("texturemesh");
		dropShadowShader = new TexturedMeshShaderProgram("texturemesh");

		entityRenderer = new WorldStateRenderEntityMesh(this);
		chunkRenderer = new WorldStateRenderChunkMesh(this);
		debugVolumeRenderer = new DebugVolumeRender(this);
		entityBoxRenderer = new SillhouetteEntityRender(this);
		entitySillhouetteRenderer = new SillhouetteEntityRender(this);
		liquidRenderer = new WorldStateRenderWater(this);
		dropShadowRenderer = new EntityShadowRenderer(this);
		effectRenderer = new WorldStateRenderEffects(this);
		dynamicLightRenderer = new WorldStateRenderDynamicLights(this);
		chunkWireFrameRenderer = new WorldStateRenderChunkWireFrame(this);

		sky = new Sky();

		((SillhouetteEntityRender*)entitySillhouetteRenderer)->color = {1,0.6,0,1};

		occlusionCurve = createOcclusionCurve();
		ditherSampler = getDitherSampler();
		mGL->bindProgram(eShader);
		glUniform1i(eShader->occlusionCurve, 0);
		glUniform1i(eShader->ditherSampler, 1);
		mGL->bindProgram(cShader);
		glUniform1i(cShader->occlusionCurve, 0);
		glUniform1i(cShader->ditherSampler, 1);
		mGL->bindProgram(cShader_forwardPass);
		glUniform1i(cShader_forwardPass->occlusionCurve, 0);
		glUniform1i(cShader_forwardPass->ditherSampler, 1);
		mGL->bindProgram(lShader);
		glUniform1i(lShader->occlusionCurve, 0);
		glUniform1i(lShader->ditherSampler, 1);

		int MSAALevel = mSettings->antiAliasing;
		useMSAA = (MSAALevel > 1);
		if(useDeferredPipeline) {
			deferredRenderPreblit = new DeferredRenderer({mSDL->viewportWidth, mSDL->viewportHeight}, useMSAA, MSAALevel);
			
			// no need to blit if we aren't using anti-aliasing, as the blitting is just there to resolve multisampled framebuffer
			if(useMSAA)
				deferredRenderPostblit = new DeferredRenderer({mSDL->viewportWidth, mSDL->viewportHeight}, false);
			else
				deferredRenderPostblit = nullptr; // no blitting is done in this case, so there is not post blit. Instead we let the pre-blit buffer stand-in for it
				
			skyScreenbuffer = new ColorBuffer({mSDL->viewportWidth, mSDL->viewportHeight});
		}
		else {
			deferredRenderPreblit = nullptr;
			deferredRenderPostblit = nullptr;
			skyScreenbuffer = nullptr;
		}

		if(enableShadowmap)
			shadowMap = new ShadowMap(this);
		else shadowMap = nullptr;

		dynamicLights = new ArrayBuffer<DynamicLight>(UNIFORM, STREAM, MAX_ENTITY_LIGHTS, 2, true);
		dynamicLightTileData = new ArrayBuffer<Tile>(UNIFORM, STREAM, TILES * TILES, 2, true);
		screenTiles = new ArrayBuffer<ScreenTileCoord>(ARRAY, STATIC, TILES * TILES * 6, 2, true);
		depthBiasMVPs = new ArrayBuffer<glm::mat4>(UNIFORM, STREAM, shadowCascades, 2, true);
		sunData = new ArrayBuffer<SunLight>(UNIFORM, STREAM, 1, 2, true);

		for(unsigned int y = 0; (int) y < TILES; y++) for(unsigned int x = 0; (int) x < TILES; x++) {
			screenTiles->set((x + y * TILES) * 6,		{{x / (float) TILES, y / (float) TILES, 0},				(x + y * TILES)});
			screenTiles->set((x + y * TILES) * 6 + 1,	{{(x + 1) / (float) TILES, y / (float) TILES, 0},		(x + y * TILES)});
			screenTiles->set((x + y * TILES) * 6 + 2,	{{(x + 1) / (float) TILES, (y + 1) / (float) TILES, 0},	(x + y * TILES)});
			screenTiles->set((x + y * TILES) * 6 + 3,	{{(x + 1) / (float) TILES, (y + 1) / (float) TILES, 0},	(x + y * TILES)});
			screenTiles->set((x + y * TILES) * 6 + 4,	{{x / (float) TILES, (y + 1) / (float) TILES, 0},		(x + y * TILES)});
			screenTiles->set((x + y * TILES) * 6 + 5,	{{x / (float) TILES, y / (float) TILES, 0},				(x + y * TILES)});
		}

		std::vector<Program*> someShaders = {cShader, dShader, eShader}; // used for uniform block bindings for lighting

		// This isn't exactly good design, but that doesn't matter too much here
#define bindBlocks(x)\
		mGL->bindProgram(x);\
		glUniformBlockBinding(x->name, x->lightData, 0);\
		glUniformBlockBinding(x->name, x->tileData, 1);\
		glUniformBlockBinding(x->name, x->sun, 2);\
		glUniformBlockBinding(x->name, x->depthBiasMVP, 3);

		bindBlocks(cShader);
		bindBlocks(eShader);
		bindBlocks(dShader);

		screenTiles->	generateBuffer();
		dynamicLights->generateBuffer();
		glBindBufferRange(GL_UNIFORM_BUFFER, 0, dynamicLights->ID, 0, sizeof(DynamicLight) * dynamicLights->getMaxSize()); // Bind the created Uniform Buffer to the Buffer Index
		dynamicLightTileData->generateBuffer();
		glBindBufferRange(GL_UNIFORM_BUFFER, 1, dynamicLightTileData->ID,  0, sizeof(Tile) * dynamicLightTileData->getMaxSize());
		sunData->generateBuffer();
		glBindBufferRange(GL_UNIFORM_BUFFER, 2, sunData->ID,  0, sizeof(SunLight) * sunData->getMaxSize());
		depthBiasMVPs->generateBuffer();
		glBindBufferRange(GL_UNIFORM_BUFFER, 3, depthBiasMVPs->ID,  0, sizeof(glm::mat4) * depthBiasMVPs->getMaxSize());

		glUniform1i(dShader->diffuseTex, 0);
		glUniform1i(dShader->gBufferTex, 1);
		glUniform1i(dShader->normalTex, 2);
		glUniform1i(dShader->lightTex, 3);
		glUniform1i(dShader->shadowMap, 4);

		testEffect = new RollingWaveDistortionEffectHandler();
		distortionHandler = new ScreenDistortionHandler({mSDL->viewportWidth,mSDL->viewportHeight});
		screenBuffer = new ColorBuffer({mSDL->viewportWidth,mSDL->viewportHeight});
	}

	// TODO: put this on seperate thread
	void WorldStateRenderHandler::calculateEntityLightData() {
		int count = 0;
		dynamicLights->resizeArray(dynamicLightsRenderedThisTick.size());
		for(auto lit = dynamicLightsRenderedThisTick.begin(); lit != dynamicLightsRenderedThisTick.end() && count < MAX_ENTITY_LIGHTS; lit++, count++) {
			DynamicLight l;
			l.brightness = lit->brightness;
			l.compensation = lit->compensation;
			l.color = glm::vec3(lit->color.x, lit->color.y, lit->color.z);
			l.position = lit->pos;
			l.radius = lit->radius;
			if(l.radius <= 0) l.radius = 1; // prevent errors
			if(l.compensation < 0)
				l.compensation = 0.18*log(l.radius) + 0.017;
			l.compensation = constrain(l.compensation, 0, 1);

			dynamicLights->set(count, l);
		}

		dynamicLights->streamData();
		for(int x = 0; x < TILES; x++) for(int y = 0; y < TILES; y++) {
			Tile t;
			t.lightsPer = 0;
			dynamicLightTileData->set(x + y * TILES, t);
		}

		auto arr = dynamicLights->getData();
		auto tiles = dynamicLightTileData->getData();

		// TODO: optimize this lmao
		for(unsigned int i = 0; i < dynamicLights->size; i++) {

			if(arr[i].radius < glm::distance(arr[i].position, mPlayer->cam.pos)) {
				DynamicLight l = arr[i];
				WorldCoordinate lightworldpos = l.position;
				lightworldpos -= glm::normalize(lightworldpos - mPlayer->cam.pos) * l.radius;

				glm::vec4 screenpos = mPlayer->cam.viewProjection * glm::vec4(lightworldpos,1);

				if(l.radius / screenpos.w <= 0) continue;

				glm::vec2 pos = glm::vec2(screenpos.x, screenpos.y) / screenpos.w;
				pos = (pos+glm::vec2(1.f))/2.f;
				pos = pos * (float) TILES;

				float lightRad = l.radius / screenpos.w / mPlayer->cam.tangent;
				lightRad = (lightRad * (float) TILES / 2.0 + 1.0);
				lightRad = fmax(lightRad, 1.0);
				int intRad = (int)lightRad;
				float radsqr = lightRad * lightRad;
				float xradsqr = ceil(radsqr * mPlayer->cam.aspectRatio);
				glm::ivec2 ipos = glm::ivec2(pos);
				glm::ivec2 start = ipos - intRad;
				glm::ivec2 end = ipos + intRad;
				for(int x = std::max(start.x, 0); x <= glm::min(end.x, TILES); x++) for(int y = std::max(start.y, 0); y <= std::min(end.y, TILES); y++) {
					glm::ivec2 npos = glm::ivec2(x,y);
					if((x - ipos.x) * (x - ipos.x) / xradsqr + (y - ipos.y) * (y - ipos.y) / radsqr > 1) continue;
					if((npos.y >= TILES) || (npos.y < 0) || (npos.x >= TILES) || (npos.x < 0)) continue;
					unsigned int key = npos.x + npos.y * TILES;
					Tile t = tiles[key];
					if((int) t.lightsPer < MAX_ENTITY_LIGHTS_PER_TILE) {
						t.lightsPer++;
						t.lightIDs[t.lightsPer - 1] = i;
						dynamicLightTileData->set(key, t);
					}
				}
			} else {
				for(int x = 0; x < TILES; x++) for(int y = 0; y < TILES; y++) {
					glm::ivec2 npos = glm::ivec2(x,y);
					unsigned int key = npos.x + npos.y * TILES;
					Tile t = tiles[key];
					if((int) t.lightsPer < MAX_ENTITY_LIGHTS_PER_TILE) {
						t.lightsPer++;
						t.lightIDs[t.lightsPer - 1] = i;
						dynamicLightTileData->set(key, t);
					}
				}
			}
		}
		dynamicLightTileData->streamData();
		dynamicLightsRenderedThisTick.clear();
	}

	glm::mat4 biasMatrix (
		0.5, 0.0, 0.0, 0.0,
		0.0, 0.5, 0.0, 0.0,
		0.0, 0.0, 0.5, 0.0,
		0.5, 0.5, 0.5, 1.0
	);

	Color::GPUColor voxelLookAtColor = Color::GPUColor(255,255,255,200);

	void renderVoxelOutline(VoxelRaycastReport r, VolumeRenderer* render, Color::GPUColor col = Color::GPUColor(255,255,255,255)) {
		if(!r.found) return;
		WorldCoordinate pos = r.endPoint;
		pos -= r.normal * 0.1f;
		pos = voxelToWorldCoordinate(worldToVoxelCoordinate(pos)) + WorldCoordinate(0.5) + r.normal * 0.01f;
		Volume v(pos, WorldCoordinate(1), glm::quat(1,0,0,0), glm::vec3(0.5));
		render->render(VolumeDisplayObject(&v, Color::HighpColor(col)/255.f));
	}

	void renderVoxelLookingAt(WorldState* w, VolumeRenderer* render) {
		VoxelRaycastReport r = w->map->getRaycast(mPlayer->cam.pos, glm::normalize(mPlayer->cam.lookAt-mPlayer->cam.pos + WorldCoordinate(.0001)), 100.f);
		renderVoxelOutline(r, render, voxelLookAtColor);
	}



	// bind a shader for worldstate rendering, upload relevant uniforms
	template<typename Program_T> void
	EZBeginWorldStateShaderRender(WorldStateRenderHandler* renderHandler, Program_T* shader) {
		mGL->bindProgram(shader);
		renderHandler->sky->fogUniforms.upload(shader);

		glUniform1f(shader->getUniformLocation("worldTime"), renderHandler->sky->time);
		glm::vec3 pPos = mPlayer->player->bb.pos;
		glUniform3fv(shader->getUniformLocation("playerPos"), 1, &pPos[0]);

		mGL->bindTexture(renderHandler->parent->map->actualPalette->uvMapper->colorTex,2,shader->voxelColorTex);
		mGL->bindTexture(renderHandler->parent->map->actualPalette->uvMapper->lightTex,3,shader->voxelLightTex);
		uploadCameraMatrices(shader, &(mPlayer->cam));
		shader->beginRender();
	}

	void renderWorldStateVolumes(WorldStateRenderHandler* renderHandler, glm::vec2 fboPixelSize) {
		mGL->bindProgram(renderHandler->coloredShader);
		renderHandler->coloredShader->beginRender();

		glLineWidth(fmax(fboPixelSize.x, fboPixelSize.y));
		renderHandler->parent->entityManager->renderVolumes(renderHandler->debugVolumeRenderer); // Render entity volumes

		if(renderHandler->parent->entityManager->_renderDebug) { // Only if debug rendering is enabled
			glLineWidth(2.f*fmax(fboPixelSize.x, fboPixelSize.y));
			glPolygonMode( GL_FRONT_AND_BACK, GL_LINE ); // enable wireframe rendering
			renderHandler->parent->map->renderChunks(renderHandler->chunkWireFrameRenderer); // Render wireframes around chunks
			glPolygonMode( GL_FRONT_AND_BACK, GL_FILL ); // disable wireframe rendering

			glLineWidth(3.f*fmax(fboPixelSize.x, fboPixelSize.y));
			renderHandler->parent->map->renderChunkVolumes(renderHandler->debugVolumeRenderer); // Render bounding boxes around chunks
		}
		glLineWidth(4.f*fmax(fboPixelSize.x, fboPixelSize.y));
		renderVoxelLookingAt(renderHandler->parent, renderHandler->debugVolumeRenderer); // render the voxel the player is looking at
		renderHandler->coloredShader->endRender();
	}

	void WorldStateRenderHandler::initLiquidRendering() {
		liquidManager->renderToFrameBuffer(); // render liquids to their framebuffer
	}

	void WorldStateRenderHandler::renderLiquids() {
		mGL->bindProgram(lShader);
		sky->fogUniforms.upload(lShader);

		// bind these textures again because liquids use them
		mGL->bindTexture(occlusionCurve, 0);
		mGL->bindTexture(ditherSampler, 1);
		glUniform1i(lShader->occlusionCurve, 0);
		glUniform1i(lShader->ditherSampler, 1);

		liquidManager->beginRendering(2,3, lShader); // bind to texture binding locations 2 and 3 for liquids

		glUniform3fv(lShader->camPos, 1, &(mPlayer->cam.pos[0]));

		lShader->beginRender();
		parent->map->renderChunks(liquidRenderer);
		lShader->endRender();
	}

	struct VRPerspective {
		ColorBuffer* buf;
		glm::vec3 offset;
	};

	VRPerspective eyes[2] = { VRPerspective{}, VRPerspective{} };

	bool VRInitialized = false;

	void WorldStateRenderHandler::render(FrameBuffer* fbo) {
		bool useVR = mSettings->enableVR;
		if(!useVR) {
			renderPerspective(fbo);
			return;
		}

		if(!VRInitialized) {
			glm::ivec2 halfResolution(mSDL->viewportWidth/2,mSDL->viewportHeight);
			VRInitialized = true;

			eyes[0] = VRPerspective{ new ColorBuffer(halfResolution),glm::vec3(0)};
			eyes[1] = VRPerspective{ new ColorBuffer(halfResolution),glm::vec3(0)};
		}

		glm::vec3 forward = glm::normalize(mPlayer->cam.pos-mPlayer->cam.lookAt);
		glm::vec3 up = glm::normalize(mPlayer->cam.up);
		glm::vec3 cross = glm::normalize(glm::cross(up,forward));

		eyes[0].offset = cross*0.2f;
		eyes[1].offset = -cross*0.2f;

		for(int i = 0; i < 2; i++) {
			mPlayer->cam.pos += eyes[i].offset;
			mPlayer->cam.lookAt += eyes[i].offset;
			renderPerspective(eyes[i].buf);
			mPlayer->cam.lookAt -= eyes[i].offset;
			mPlayer->cam.pos -= eyes[i].offset;
		}

		mGL->bindFrameBuffer(fbo);
		glClear(GL_DEPTH_BUFFER_BIT);

		mGL->bindProgram(mGL->screenProgram);
		mGL->screenProgram->beginRender();
		mGL->render(mGL->screenProgram, eyes[0].buf->target, glm::vec2(0,0), glm::vec2(0.5,1));
		mGL->render(mGL->screenProgram, eyes[1].buf->target, glm::vec2(0.5,0), glm::vec2(0.5,1));
		mGL->screenProgram->endRender();
	}

	// It's OK to cry.
	void WorldStateRenderHandler::renderPerspective(FrameBuffer* fbo) {
		mGL->bindFrameBuffer(fbo);
		glClear(GL_DEPTH_BUFFER_BIT);

		mGL->bindFrameBuffer(screenBuffer);
		screenBuffer->clear(glm::vec4(0,0,0,1));

		FrameBuffer* screen = screenBuffer;

		initLiquidRendering();

		effectManager->render(effectRenderer);

		// Set the camera's aspect ratio to that of the target framebuffer's
		mPlayer->cam.aspectRatio = ((float) screen->size.x) / screen->size.y;

		mPlayer->cam.update(); // Set all relevant matrices/etc. for the player's camera

		// Prepare particles for rendering
		particleManager->prepareForRender(mPlayer->cam.pos);

		// Prepare the sky for some intense rendering
		sky->prepareForRender(mPlayer->cam);

		// Update the position of the sun
		sunData->set(0, sky->sun);
		sunData->streamData();

		// Render to the shadowmap, if it's enabled
		if(enableShadowmap) shadowMap->renderTo();

		calculateEntityLightData(); // Calculate and store all the necessary data for shading dynamic lights

		// This is the guy we render the scene to
		FrameBuffer* curFrame;
		// If we're using forward rendering, we can render straight to the target, otherwise we need a middleman
		if(useDeferredPipeline) curFrame = deferredRenderPreblit;
		else curFrame = screen;

		// Bind and clear our current framebuffer for rendering (Normally we would clear only the depth buffer, but this is the first rendering stage, so we wipe the entire thing)
		mGL->bindFrameBuffer(curFrame);
		curFrame->clear();

		// If we're forward rendering, then we render the skybox first. Otherwise, we'll want to hold onto the skybox for later use.
		if(!useDeferredPipeline) {
			sky->render(mPlayer->cam, true); // exposure=true, as this buffer is _not_ HDR
			glEnable(GL_CULL_FACE);
		}

		// These are used in the world shaders, so we bind them here
		mGL->bindTexture(occlusionCurve, 0); // This specifies how we occlude voxels
		mGL->bindTexture(ditherSampler, 1);  // This specifies the dither matrix (used to eliminate color banding)

		// Begin entity rendering
		EZBeginWorldStateShaderRender<EntityShaderProgram>(this, eShader);
		parent->map->renderEntities(entityRenderer);
		parent->entityManager->render(entityRenderer);
		particleManager->renderMeshes(entityRenderer);
		eShader->endRender();
		// End entity rendering

		// Begin chunk rendering
		parent->map->prepareForRender();
		EZBeginWorldStateShaderRender<ChunkShaderProgram>(this, cShader);
		parent->map->renderChunks(chunkRenderer);
		cShader->endRender();
		// End chunk rendering

		if(useDeferredPipeline) { // we're only writing to the color buffer here, in the case of deferred rendering, so we temporarily disable the other ones
			deferredRenderPreblit->setDrawBuffers({DeferredRenderer::COLOR});
		}

		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glDepthMask(GL_FALSE);
		// TODO: move this back such that it's rendering to the window
		glDepthFunc(GL_LEQUAL);
		// Render volumes to the window
		// renderWorldStateVolumes(this, fbo->pixelSize);

		// Disable depth writes, so shadows don't z-fight
		glDepthMask(GL_FALSE);

		// This allows us to use a simple additive shadow texture (white = no shadow, black = full shadow
		// and allows us to have order-independent rendering, no matter the texture, so we don't have to worry about depth writes
		glBlendFunc(GL_ZERO, GL_SRC_COLOR);

		// Begin Drop Shadow Rendering
		mGL->bindProgram(dropShadowShader);
		// Bind Drop shadow texture
		glUniform1i(dropShadowShader->tex, 0);
		mGL->bindTexture(shadowTex, 0);

		dropShadowShader->beginRender();
		parent->entityManager->renderDropShadows(dropShadowRenderer);
		dropShadowShader->endRender();
		// End Drop Shadow Rendering

		// Re-enable default settings
		glDepthMask(GL_TRUE);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		if(useDeferredPipeline) { // re-enable default (all) render targets
			deferredRenderPreblit->setDrawBuffers({DeferredRenderer::COLOR, DeferredRenderer::NORMALS, DeferredRenderer::LIGHT, DeferredRenderer::G_BUFFER});
		}

		// Render Entity outlines and so forth (we save this for later if doing deferred rendering)
		if(!useDeferredPipeline) renderWorldStateVolumes(this, fbo->pixelSize);

		// Do the deferred postprocessing step
		if(useDeferredPipeline) {
		
			DeferredRenderer* deferredSampleFramebuffer; // This is our deferredRenderer that we sample from. (Varies depending on antialiasing settings)

			// Downsample the anti-aliased framebuffer, if applicable
			if(useMSAA) {
				mGL->sampleFramebuffer(deferredRenderPreblit, deferredRenderPostblit, GL_COLOR_BUFFER_BIT, GL_LINEAR, DeferredRenderer::COLOR_ATTACH,    DeferredRenderer::COLOR_ATTACH);
				mGL->sampleFramebuffer(deferredRenderPreblit, deferredRenderPostblit, GL_COLOR_BUFFER_BIT, GL_LINEAR, DeferredRenderer::NORMAL_ATTACH,   DeferredRenderer::NORMAL_ATTACH);
				mGL->sampleFramebuffer(deferredRenderPreblit, deferredRenderPostblit, GL_COLOR_BUFFER_BIT, GL_LINEAR, DeferredRenderer::LIGHT_ATTACH,    DeferredRenderer::LIGHT_ATTACH);
				mGL->sampleFramebuffer(deferredRenderPreblit, deferredRenderPostblit, GL_COLOR_BUFFER_BIT, GL_LINEAR, DeferredRenderer::G_BUFFER_ATTACH, DeferredRenderer::G_BUFFER_ATTACH);
				mGL->sampleFramebuffer(deferredRenderPreblit, deferredRenderPostblit, GL_DEPTH_BUFFER_BIT, GL_NEAREST, GL_NONE, GL_NONE);
				deferredSampleFramebuffer = deferredRenderPostblit;
			} else {
				deferredSampleFramebuffer = deferredRenderPreblit;
				mGL->bindTexture(occlusionCurve, 0); // This specifies how we occlude voxels
				mGL->bindTexture(ditherSampler, 1);  // This specifies the dither matrix (used to eliminate color banding)
			}

			glDisable(GL_DEPTH_TEST);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

			Texture* ssaoOutput = ssaoHandler->runSSAO(deferredSampleFramebuffer, &(mPlayer->cam));

			// render the sky to a seperate framebuffer for later use
			glEnable(GL_DEPTH_TEST);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			mGL->bindFrameBuffer(skyScreenbuffer);
			skyScreenbuffer->clear();
			sky->render(mPlayer->cam, true); // exposure=true, as this buffer is _not_ HDR

			ColorBuffer* bloomRet = nullptr;


			// Begin bloom rendering, if enabled
			if(useBloom) {

				// render the sky to the bloom buffer where there is no terrain
				glEnable(GL_DEPTH_TEST);
				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
				mGL->bindFrameBuffer(deferredSampleFramebuffer);
				deferredSampleFramebuffer->setDrawBuffers({DeferredRenderer::LIGHT});

				// TODO: merge this with the other sky render
				sky->render(mPlayer->cam, false); // exposure=false, as light buffer is HDR

				// Begin Blurring
				bloomRet = bloomBlurrer->blur(deferredSampleFramebuffer,DeferredRenderer::LIGHT);
				// End Blurring
				mGL->bindFrameBuffer(deferredSampleFramebuffer);
				deferredSampleFramebuffer->setDrawBuffers({DeferredRenderer::COLOR, DeferredRenderer::NORMALS, DeferredRenderer::LIGHT, DeferredRenderer::G_BUFFER});
			}   // End bloom rendering

			// Bind the target directly because we finally want to render to it
			mGL->bindFrameBuffer(screen);

			// Begin deferred shading
			mGL->bindProgram(dShader);
			sky->fogUniforms.upload(dShader);

			// Set up shadowmap if enabled
			if(enableShadowmap) {
				// Set up the uniform buffer for each cascade
				for(int i = 0; i < shadowCascades; i++)
					depthBiasMVPs->set(i, biasMatrix * shadowMap->sunCams[i].viewProjection);
				depthBiasMVPs->streamData(); // Stream the uniform buffer
				mGL->bindTexture(&(shadowMap->framebuffer->target[0]), 4); // Bind the textures
			}

			// Bind relevent uniform matrices
			glm::mat4 invVP = glm::inverse(mPlayer->cam.viewProjection);
			glUniformMatrix4fv(dShader->IVP, 1, GL_FALSE, &invVP[0][0]);
			glUniform3fv(dShader->cameraPos, 1, &mPlayer->cam.pos[0]);

			// Begin texture binding
			for(int i = 0; i < 4; i++) { mGL->bindTexture(&(deferredSampleFramebuffer->target[i]), i); }

			mGL->bindTextures({{bloomRet->target,5,dShader->bloomTex},{skyScreenbuffer->target,6,dShader->skyTex}, {ssaoOutput,7,dShader->ssaoTex}});
			// End texture Binding

			glBlendFunc(GL_ONE, GL_ZERO); // overwrite, so we don't have to clear

			// Render the screen tiles (for tiled deferred shading)
			dShader->beginRender();
			screenTiles->bindBuffer();
			glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE, sizeof(ScreenTileCoord),(void*)offsetof(ScreenTileCoord, coord));
			glVertexAttribIPointer(1,1,GL_UNSIGNED_INT,	 sizeof(ScreenTileCoord),(void*)offsetof(ScreenTileCoord, index));
			glDrawArrays(GL_TRIANGLES, 0, screenTiles->size);
			dShader->endRender();

			/*
			glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

			glDisable(GL_LINE_SMOOTH);
			glDisable(GL_LINE_WIDTH);
			glLineWidth(1.f);
			glPolygonMode( GL_FRONT_AND_BACK, GL_LINE ); // enable wireframe rendering
			if(true) {
				// mGL->sampleFramebuffer(deferredSampleFramebuffer, forwardPass_preblit, GL_DEPTH_BUFFER_BIT, GL_NEAREST, GL_NONE, GL_NONE);
				mGL->bindFrameBuffer(forwardPass_preblit);

				glDrawBuffer(GL_COLOR_ATTACHMENT0);

				glClearColor(0,0,0,0);
				glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

				mGL->bindTexture(occlusionCurve, 0); // This specifies how we occlude voxels
				mGL->bindTexture(ditherSampler, 1);  // This specifies the dither matrix (used to eliminate color banding)

				// Begin chunk rendering
				parent->map->prepareForRender();
				ChunkShaderProgram* tm = cShader;
				cShader = cShader_forwardPass;
				EZBeginWorldStateShaderRender<ChunkShaderProgram>(this, cShader_forwardPass);
				parent->map->renderChunks(chunkRenderer);
				cShader->endRender();
				cShader = tm;
				// End chunk rendering

				mGL->sampleFramebuffer(forwardPass_preblit, forwardPass_postblit, GL_COLOR_BUFFER_BIT, GL_LINEAR, GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT0);
				mGL->sampleFramebuffer(forwardPass_preblit, forwardPass_postblit, GL_DEPTH_BUFFER_BIT, GL_NEAREST, GL_NONE, GL_NONE);
			}
			glPolygonMode( GL_FRONT_AND_BACK, GL_FILL ); // disable wireframe rendering

			glBlendFunc(GL_ONE,GL_ONE_MINUS_SRC_ALPHA);

			mGL->bindFrameBuffer(screen);
			glDisable(GL_DEPTH_TEST);
			mGL->bindProgram(mGL->screenProgram);
			mGL->screenProgram->beginRender();
			//mGL->render(mGL->screenProgram,forwardPass_postblit->target);
			mGL->screenProgram->endRender();
			glEnable(GL_DEPTH_TEST);*/

			// End deferred shading

			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); // Re-enable default settings

			distortionHandler->clearDistortions();
			testEffect->render(distortionHandler,deferredSampleFramebuffer, sky->time);
		} // End deferred post-processing step



		// If doing deferred rendering, this is the step where we render all transparent components (cannot be deferred)
		// If not, then we're just rendering them here for convenience

		glEnable(GL_DEPTH_TEST);
		glDisable(GL_CULL_FACE); // We want both sides of transparent things to be visible
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); // Set Default blending, just to be safe
		mGL->bindFrameBuffer(screen); // Bind the target directly, as there's no deferred sorcery that will be going on here

		renderLiquids(); // render liquids
		particleManager->renderBillboards(mPlayer->cam); // render particles

		// Re-enable default settings
		glEnable(GL_CULL_FACE);

		distortionHandler->renderDistortions(screenBuffer,fbo);

	}

	glm::vec3 mvnormals[6] = {
			{0,0,1}, {0,0,-1},
			{1,0,0}, {-1,0,0},
			{0,1,0}, {0,-1,0}
	};

	template<typename Program_T>
	void uploadWorldStateModelInfo(WorldStateRenderHandler* ws, glm::mat4 model, Program_T* program) {

		ws->sky->meshSkyLighter->lightMesh(model,program);

		glm::vec3 mvnormalvals[6];
		glm::mat4 normalMat = glm::transpose(glm::inverse(mPlayer->cam.getMV(model)));
		for(int i = 0; i <  6; i++) {
			glm::vec4 v4N = normalMat * glm::vec4(mvnormals[i],0);
			mvnormalvals[i] = glm::vec3(v4N.x,v4N.y,v4N.z);
			mvnormalvals[i] = glm::normalize(mvnormalvals[i]);
		}
		glUniform3fv(program->normalVectors,6,&mvnormalvals[0][0]);

	}

	void WorldStateRenderHandler::renderChunkMesh(ChunkMeshDisplayObject ch) {
		ch.buf->calculateElements();
		WorldCoordinate position;
		decomposeMatrix(ch.model,position);

		glm::mat4 IM = glm::inverse(ch.model);
		glUniformMatrix4fv(cShader->getUniformLocation("IM"), 1, GL_FALSE, &(IM[0][0]));

		uploadWorldStateModelInfo(this,ch.model,cShader);

		mGL->render(ch.buf, cShader, &mPlayer->cam, ch.model);
	}

	void WorldStateRenderHandler::renderWireFrame(ChunkMeshDisplayObject ch) {
		ch.buf->calculateElements();

		WorldCoordinate position;
		decomposeMatrix(ch.model,position);

		if(ch.buf->elements == 0 || !inRenderRadius(mPlayer->player->bb.visual().getBottomCorner(), position, CHUNK_RENDER_RADIUS)) return;

		uploadCameraModelMatrices(coloredShader, &(mPlayer->cam), ch.model);
		glUniform4f(coloredShader->color, 0, 0, 0, 0.3);

		ch.buf->buffer->bindBuffer();
		glVertexAttribPointer(0, 4, GL_UNSIGNED_INT_2_10_10_10_REV, GL_FALSE, sizeof(TerrainPaletteVertex), (GLvoid*) offsetof(TerrainPaletteVertex, position));

		ch.buf->chunkIndexBuffer->bindBuffer();

		glDrawElements(GL_TRIANGLES, ch.buf->elements, GL_UNSIGNED_INT, (GLvoid*) 0);
	}

	bool inLoDRenderRadius(WorldCoordinate cam, WorldCoordinate ch, ChunkCoordinate srad, ChunkCoordinate rad, int lod) {
		for(int l = 2; l < lod; l*= 2)
			srad += rad * l;
		return !inRenderRadius(cam, ch, srad) && inRenderRadius(cam, ch, srad + rad * lod);
	}

	bool interpolateEntityLightLevel = false;

	void WorldStateRenderHandler::renderEntityMesh(EntityMeshDisplayObject en) {
		en.buf->calculateElements();
		WorldCoordinate position;
		decomposeMatrix(en.model,position);

		mGL->bindTexture(en.buf->globalNotGlobalPalette->uvMapper->colorTex,2);
		mGL->bindTexture(en.buf->globalNotGlobalPalette->uvMapper->lightTex,3);

		if(!mPlayer->cam.containsSphere(position, rad(en.scale*2.f)) || en.buf->elements == 0) return;

		const float p1 = 0.05;
		const float p2 = 1.0 - p1*2;
		if(interpolateEntityLightLevel) {

			glm::vec3 cornerLights[8];
			for(int x = 0; x < 2; x++) for(int y = 0; y < 2; y++) for(int z = 0; z < 2; z++) {
				VoxelCoordinate cornerPos = worldToVoxelCoordinate(
						position + en.scale * WorldCoordinate(x*p2 + p1, y*p2 + p1, z*p2 + p1)
				);
				cornerLights[x + y * 2 + z * 4] = parent->map->getLightLevel(cornerPos).getRGBValue();
			}

			glUniform3fv(eShader->cornerLights, 8, &((*cornerLights)[0]));
		} else {

			if(!(en.llevOverride.x < -std::numeric_limits<float>::epsilon()))
				glUniform3fv(eShader->lightLevel, 1, &(en.llevOverride[0]));

			else {
				glm::vec3 llev;
				VoxelCoordinate middlePos = worldToVoxelCoordinate(
						position + en.scale * WorldCoordinate(0.5,0.5,0.5)
				);
				llev = parent->map->getLightLevel(middlePos).getRGBValue();

				glUniform3fv(eShader->lightLevel, 1, &(llev[0]));
			}
		}

		uploadWorldStateModelInfo(this,en.model,eShader);

		WorldCoordinate size = WorldCoordinate(en.buf->size);
		glUniform3fv(eShader->size, 1, &size[0]);
		mGL->render(en.buf, eShader, &mPlayer->cam, en.model);
	}


	void WorldStateRenderHandler::renderVolume(VolumeDisplayObject v) {
		if(!mPlayer->cam.containsSphere(v.volume->pos, rad(v.volume->size))) return;

		glm::mat4 model(1.0);
		model = glm::translate(model, v.volume->getBottomCorner());
		model = glm::scale(model, v.volume->size);

		glm::mat4 rotM(1.0);
		rotM = glm::translate(rotM, glm::vec3(1/2.f));
		rotM = glm::scale(rotM, 	glm::vec3(1) / v.volume->size);
		rotM = rotM * glm::toMat4(v.volume->rot);
		rotM = glm::scale(rotM, 	v.volume->size / glm::vec3(1));
		rotM = glm::translate(rotM, glm::vec3(-1/2.f));
		model *= rotM;

		uploadCameraModelMatrices(coloredShader, &(mPlayer->cam), model);

		glm::vec4 convCol = glm::vec4(v.color); // convert to highp color
		glUniform4fv(coloredShader->color, 1, &convCol[0]);

		mGL->cubeWireframe->bindBuffer();
		// Vertex buffer
		glVertexAttribPointer(0, 3, GL_UNSIGNED_BYTE, GL_FALSE, sizeof(glm::u8vec3), 0);

		glDrawArrays(GL_LINES, 0, mGL->cubeWireframe->size*2);
	}

	void WorldStateRenderHandler::renderEntityMeshSillhouette(EntityMeshDisplayObject en, Color::HighpColor color) {
		en.buf->calculateElements();
		WorldCoordinate position;
		decomposeMatrix(en.model,position);
		if(!mPlayer->cam.containsSphere(position + en.scale/2.f, rad(en.scale)) || en.buf->elements == 0) return;

		glm::mat4 mvp = mPlayer->cam.getMVP(en.model);

		glUniformMatrix4fv(coloredShader->MVP, 1, GL_FALSE, &mvp[0][0]);
		glUniform4fv(coloredShader->color, 1, &color[0]);

		en.buf->buffer->bindBuffer();
		// Vertex buffer
		//glVertexAttribPointer(0, 3, GL_UNSIGNED_BYTE, GL_FALSE, sizeof(EntityVertex), (void*)(offsetof(EntityVertex,position)));

		 // Draw the triangles !
		VoxelMeshBuffer::indexBuffer->bindBuffer();
		glDrawElements(GL_TRIANGLES, en.buf->elements, GL_UNSIGNED_INT, (void*) 0);
	}

	void WorldStateRenderHandler::renderChunkWater(ChunkMeshDisplayObject ch) {
		ch.buf->calculateElements();
		WorldCoordinate position;
		decomposeMatrix(ch.model,position);
		if(!mPlayer->cam.containsSphere(position + ch.scale/2.f, rad(ch.scale)) || ch.buf->elements == 0 || !inRenderRadius(mPlayer->player->bb.pos, position, CHUNK_RENDER_RADIUS)) return;

		uploadCameraModelMatrices(lShader, &(mPlayer->cam), ch.model);

		ch.buf->wBuffer->bindBuffer();

		glVertexAttribPointer(0, 3, GL_UNSIGNED_BYTE, GL_FALSE, sizeof(WaterVertex), (GLvoid*) offsetof(WaterVertex,position));
		glVertexAttribPointer(1, 3, GL_BYTE, 		  GL_TRUE, 	sizeof(WaterVertex), (GLvoid*) offsetof(WaterVertex,normal));
		glVertexAttribPointer(2, 2, GL_UNSIGNED_SHORT,GL_TRUE,  sizeof(WaterVertex), (GLvoid*) offsetof(WaterVertex,UV));
		glVertexAttribPointer(3, 4, GL_UNSIGNED_BYTE, GL_TRUE,  sizeof(WaterVertex), (GLvoid*) offsetof(WaterVertex,color));
		glVertexAttribPointer(4, 1, GL_UNSIGNED_BYTE, GL_TRUE, 	sizeof(WaterVertex), (GLvoid*) offsetof(WaterVertex,height));
		glVertexAttribPointer(5, 1, GL_UNSIGNED_BYTE, GL_FALSE, sizeof(WaterVertex), (GLvoid*) offsetof(WaterVertex,occlusion));
		glVertexAttribPointer(6, 3, GL_UNSIGNED_BYTE, GL_TRUE,  sizeof(WaterVertex), (GLvoid*) offsetof(WaterVertex,tangent));
		glVertexAttribPointer(7, 3, GL_UNSIGNED_BYTE, GL_TRUE,  sizeof(WaterVertex), (GLvoid*) offsetof(WaterVertex,bitangent));
		glVertexAttribPointer(9, 3, GL_BYTE, GL_TRUE,          sizeof(WaterVertex), (GLvoid*) offsetof(WaterVertex,vlight));

		ch.buf->wLightBuffer->bindBuffer();
		glVertexAttribIPointer(8, 3, GL_UNSIGNED_BYTE, 0,0);

		VoxelMeshBuffer::indexBuffer->bindBuffer();

		glDrawElements(GL_TRIANGLES, ch.buf->wBuffer->size * 6, GL_UNSIGNED_INT, (GLvoid*) 0);
	}

	void WorldStateRenderHandler::renderChunkCubeMarch(ChunkMeshDisplayObject ch) {
		ch.buf->calculateElements();
		WorldCoordinate position;
		decomposeMatrix(ch.model,position);
		if(!mPlayer->cam.containsSphere(position + ch.scale/2.f, rad(ch.scale)) || ch.buf->elements == 0 ||
				!inRenderRadius(mPlayer->player->bb.visual().getBottomCorner(), position, CHUNK_RENDER_RADIUS)) return;

		uploadCameraModelMatrices(cShader, &(mPlayer->cam), ch.model);

		ch.buf->cubeMarchBuffer->bindBuffer();

		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE,         sizeof(CubeMarchVertex), (GLvoid*) offsetof(CubeMarchVertex,position));
		glVertexAttribPointer(1, 4, GL_UNSIGNED_BYTE, GL_TRUE,  sizeof(CubeMarchVertex), (GLvoid*) offsetof(CubeMarchVertex,color));
		glVertexAttribPointer(2, 1, GL_UNSIGNED_BYTE, GL_FALSE, sizeof(CubeMarchVertex), (GLvoid*) offsetof(CubeMarchVertex,occlusion));
		glVertexAttribPointer(3, 3, GL_BYTE, 		  GL_TRUE, 	sizeof(CubeMarchVertex), (GLvoid*) offsetof(CubeMarchVertex, normal));
		glVertexAttribPointer(5, 3, GL_BYTE, GL_TRUE,           sizeof(CubeMarchVertex), (GLvoid*) offsetof(CubeMarchVertex,vlight));

		ch.buf->lightBuffer->bindBuffer();

		glVertexAttribPointer(4, 3, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(VertexLighting), (GLvoid*) 0);

		VoxelMeshBuffer::indexBuffer->bindBuffer();

		glDrawArrays(GL_TRIANGLES, 0, ch.buf->cubeMarchBuffer->size * 3);
	}

	// lookdir is -cam.lookdir
	glm::mat4 billboardMatrix(glm::vec3 pos, glm::vec3 lookDir, glm::vec3 camUp, float rot) {
		return glm::inverse(glm::lookAt(pos, pos-lookDir, camUp)) * glm::rotate(rot, glm::vec3(0,0,1));
	}

	glm::mat4 rotationMatrix(glm::vec3 normal, glm::vec3 desiredNormal) {
		glm::vec3 axis = glm::normalize(glm::cross(normal, desiredNormal));
		float angle = acos(glm::dot(normal, desiredNormal) / (glm::length(normal) * glm::length(desiredNormal)));
		return glm::toMat4(glm::angleAxis(angle, axis));
	}

	void WorldStateRenderHandler::renderDropShadow(DropShadowDisplayObject d) {
		/*if(onGround) {
			WorldCoordinate scale2 = WorldCoordinate(fmin(v.size.x,v.size.z)) * 1.5f;
			glm::mat4 model(1.f);
			model = glm::translate(model, v.getBottomCenter() + WorldCoordinate(0.f,0.025f,0.f));
			model = glm::scale(model, scale2);

			glm::mat4 mvp = mPlayer->cam.getMVP(model);

			glUniformMatrix4fv(dropShadowShader->MVP, 1, GL_FALSE, &mvp[0][0]);

			dropShadowBuf->verts->bindBuffer();
			glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE,sizeof(TexturedMeshVertex),(void*) offsetof(TexturedMeshVertex, pos));
			glVertexAttribPointer(1,2,GL_FLOAT,GL_FALSE,sizeof(TexturedMeshVertex),(void*) offsetof(TexturedMeshVertex, UV));
			dropShadowBuf->inds->bindBuffer();

			glDrawElements(GL_TRIANGLES, dropShadowBuf->inds->size*3, GL_UNSIGNED_INT, (void*)0);

		} else {*/
			VoxelRaycastReport ray = parent->map->getRaycast(d.volume->pos, glm::normalize(WorldCoordinate(0.001,-1,0.001)), 10.f);
			if(ray.found) {
				WorldCoordinate scale2 = WorldCoordinate(fmin(d.volume->size.x,d.volume->size.z)) * 1.5f;
				glm::mat4 model(1.f);
				model = glm::translate(model, ray.endPoint + WorldCoordinate(0.f,0.025f,0.f));
				model = glm::scale(model, scale2);

				uploadCameraModelMatrices(dropShadowShader, &(mPlayer->cam), model);

				dropShadowBuf->verts->bindBuffer();
				glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE,sizeof(TexturedMeshVertex),(void*) offsetof(TexturedMeshVertex, pos));
				glVertexAttribPointer(1,2,GL_FLOAT,GL_FALSE,sizeof(TexturedMeshVertex),(void*) offsetof(TexturedMeshVertex, UV));
				dropShadowBuf->inds->bindBuffer();

				glDrawElements(GL_TRIANGLES, dropShadowBuf->inds->size*3, GL_UNSIGNED_INT, (void*)0);
			}
		// }
	}

}
