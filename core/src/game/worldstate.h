/*
 * worldstate.h
 *
 *  Created on: Mar 22, 2015
 *      Author: Dani
 */

#ifndef WORLDSTATE_H_
#define WORLDSTATE_H_
#include "gameloop.h"

namespace vox {

	class WorldStateRenderHandler;
	class EntityManager;
	class ChunkMap;

	class WorldState : public GameState {
	public:
		WorldStateRenderHandler* renderHandler;
		ChunkMap* map;
		EntityManager* entityManager;

		WorldState();
		virtual ~WorldState();

		void render(FrameBuffer* fbo);
		void tick(float delta);
	};

	extern WorldState* mWorldState;

}

#endif /* WORLDSTATE_H_ */
