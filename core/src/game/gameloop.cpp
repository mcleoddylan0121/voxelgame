/*
 * gameloop.cpp
 *
 *  Created on: Mar 22, 2015
 *      Author: Dani
 */

#include "gameloop.h"
#include "sdlcontroller.h"
#include "glcontroller.h"
#include "framebuffer.h"
#include "vfreetype.h"
#include <algorithm>
#include <sstream>

namespace vox {
	GameLoop mGameLoop;

	GameLoop::GameLoop() {
		running = false;
		fps = 0;
		frame = 0;
		lastTime = 0;
		accumulator = 0;
		frameTime = 0;
	}

	GameLoop::~GameLoop() {}

	void GameLoop::start() {
		running = true;
		lastTime = mSDL->getTime<milliseconds>();

		loop();
	}

	void GameLoop::loop() {
		while (running) {
			long curTime = mSDL->getTime<milliseconds>();
			int delta = curTime - lastTime;
			lastTime = curTime;
			accumulator += delta;

			bool screenshot = false;

			while (accumulator > DELTA_LIMIT_LOWER) {
				int ttime = std::min((long long) DELTA_LIMIT_UPPER, accumulator);
				accumulator -= ttime;

				// Events
				if (!mSDL->handleEvents()) {
					this->kill();
					break;
				}

				if(mSDL->inputTyped("Take Screenshot")) screenshot = true;

				// Tick
				for (auto curState : curStates)
					curState->tick(ttime / 1000.f);
				mSDL->updateUtilities();
			}

			mFontManager->tick();

			// Render
			mGL->renderGameStates(mGL->windowFBO);
			mSDL->swapBuffer();

			if(screenshot) {
				mGL->takeScreenShot();
			}

			// Remove deleted GameStates
			if (trashStates.size()) {
				for (auto trash : trashStates) {
					curStates.remove(trash);
				}
				trashStates.clear();
			}


			// Calculate frames per second
			if (frameTime > 1000) {
				frameTime = 0;
				fps = frame;
				frame = 0;
			}
			aaaaTempDelta.push_back(delta);
			frameTime += delta;
			frame++;
		}
	}

	void GameLoop::kill() { running = false; }
	bool GameLoop::isKill() { return !this->running; }

	void GameLoop::addStateToFront(GameState * s) {
		curStates.push_front(s);
	}
	void GameLoop::addStateToBack(GameState * s) {
		curStates.push_back(s);
	}
	void GameLoop::removeState(GameState * s) {
		trashStates.push_back(s);
	}

	long GameLoop::getTimeInMillis() {
		return lastTime;
	}

	int GameLoop::getFPS() {
		return fps;
	}

	std::list<GameState *> GameLoop::getGameStates() {
		return curStates;
	}
}
