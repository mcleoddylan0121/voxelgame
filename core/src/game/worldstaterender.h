#ifndef WORLDSTATERENDER_H_
#define WORLDSTATERENDER_H_

#include "coords.h"
#include "renderutils.h"
#include "ssao.h"

namespace vox {

	class ChunkMap;

	const int MAX_ENTITY_LIGHTS = 256;
	const int MAX_ENTITY_LIGHTS_PER_TILE = 47;

	struct Tile {
		unsigned char lightsPer = 0;
		unsigned char lightIDs[MAX_ENTITY_LIGHTS_PER_TILE];
	};
	struct ScreenTileCoord {
		glm::vec3 coord;
		unsigned int index;
	};

	class BlurHandler;
	class SunLight;
	class Sky;
	class DeferredRenderer;
	class ChunkShaderProgram;
	class EntityShaderProgram;
	class DeferredShaderProgram;
	class ColoredShaderProgram;
	class LiquidShaderProgram;
	class TexturedMeshShaderProgram;
	class ParticleManager;
	class RollingWaveDistortionEffectHandler;
	class ScreenDistortionHandler;

	struct EffectRenderer;
	struct EffectManager;

	template<typename T> class ArrayBuffer;
	class ShadowMap;

	struct DynamicLight {
		glm::vec3 position=glm::vec3(1);
		float radius=1;
		glm::vec3 color=glm::vec3(1);
		float compensation=0;
		float brightness=1;
		float pad1=0, pad2=0, pad3=0;
	};

    // handles all rendering for the worldstate
    class WorldStateRenderHandler {
    public:
        WorldState* parent;
        SSAOHandler* ssaoHandler;

		WorldStateRenderHandler(WorldState* parent);
		virtual ~WorldStateRenderHandler();

		void render(FrameBuffer* fbo);

		void initLiquidRendering(); // get ready to render liquids
		void renderLiquids(); // render all liquids to the current bound framebuffer


		void tick(float delta);

		void calculateEntityLightData();

		void renderChunkMesh(ChunkMeshDisplayObject);
		void renderChunkWater(ChunkMeshDisplayObject);
		void renderWireFrame(ChunkMeshDisplayObject);
		void renderChunkCubeMarch(ChunkMeshDisplayObject);
		void renderEntityMesh(EntityMeshDisplayObject);
		void renderVolume(VolumeDisplayObject);
		void renderEntityMeshSillhouette(EntityMeshDisplayObject, Color::HighpColor);
		void renderDropShadow(DropShadowDisplayObject);

		void renderPerspective(FrameBuffer* fbo);

		bool enableShadowmap;

		ChunkShaderProgram *cShader, *cShader_forwardPass;
		EntityShaderProgram* eShader;
		DeferredShaderProgram* dShader;
		ColoredShaderProgram* coloredShader;
		LiquidShaderProgram* lShader;
		TexturedMeshShaderProgram* billboardShader;
		TexturedMeshShaderProgram* dropShadowShader;

		BlurHandler* bloomBlurrer;

		EntityMeshRenderer* entityRenderer;
		EffectRenderer* effectRenderer;
		ChunkMeshRenderer* chunkRenderer;
		EntityMeshRenderer *entityBoxRenderer, *entitySillhouetteRenderer;
		VolumeRenderer* debugVolumeRenderer;
		ChunkMeshRenderer* liquidRenderer;
		DropShadowRenderer* dropShadowRenderer;
		DynamicLightRenderer* dynamicLightRenderer;
		ChunkMeshRenderer* chunkWireFrameRenderer;

		DeferredRenderer* deferredRenderPreblit;  // render to this
		DeferredRenderer* deferredRenderPostblit; // sample this

		ColorBuffer *forwardPass_preblit, *forwardPass_postblit;

		EffectManager* effectManager;
        ParticleManager* particleManager;
        LiquidManager* liquidManager;

		std::vector<DynamicLightDisplayObject> dynamicLightsRenderedThisTick;
		ArrayBuffer<DynamicLight>* dynamicLights;
		ArrayBuffer<Tile>* dynamicLightTileData;
		ArrayBuffer<ScreenTileCoord>* screenTiles;
		ArrayBuffer<SunLight>* sunData;
		ArrayBuffer<glm::mat4>* depthBiasMVPs;

		Sky* sky;
		ShadowMap* shadowMap;

		ColorBuffer* skyScreenbuffer; // Sky rendered in cam perspective this tick


		RollingWaveDistortionEffectHandler* testEffect;
		ScreenDistortionHandler* distortionHandler;

		ColorBuffer* screenBuffer;

    };

} // namespace vox

#endif
