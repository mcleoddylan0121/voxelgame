/*
 * ssao.cpp
 *
 *  Created on: Jun 19, 2016
 *      Author: Dylan
 */

#include "ssao.h"
#include "sdlcontroller.h"
#include "program.h"
#include "glcontroller.h"
#include "glbuffer.h"
#include "console.h"
#include "blur.h"

namespace vox {

	// SSAO implementation from http://www.learnopengl.com/#!Advanced-Lighting/SSAO

	float lerp(float a, float b, float f) {
		return a + f * (b - a);
	}

	Rand ssaoRand;

	std::vector<glm::vec3> generateCustomVoxelSSAOKernel(int samples, int rings) {
		float z = 0.04f;
		std::vector<glm::vec3> ret;
		for(int i = 0; i < rings; i++) for(int j = 0; j < samples; j++) {
			float rad = (i+1)/(float)rings;
			float rads = (2*M_PI) * (j/(float)samples);
			ret.push_back(glm::vec3(rad*cos(rads),rad*sin(rads),z));
		}
		return ret;
	}

	std::vector<glm::vec3> generateSSAOKernel(int numSamples, float sampleRadius = 0.85f) {
		std::vector<glm::vec3> ret;

		for (int i = 0; i < numSamples; ++i) {
		    glm::vec3 sample(
		    	ssaoRand.getFloat(-1,1),
				ssaoRand.getFloat(-1,1),
				ssaoRand.getFloat(0.15,1) // this is a hemisphere, so this guy is cut in half (and a little more)
		    );
		    sample = glm::normalize(sample) * sampleRadius;
		    ret.push_back(sample);
		 }

		return ret;
	}

	std::vector<float> generateScaleArray(int numSamples) {
		std::vector<float> ret;

		for(int i = 0; i < numSamples; ++i) {
			float sample = 1.f;
		    sample *= ssaoRand.getFloat(0,1);
		    float scale = float(i) / numSamples;
		    scale = lerp(0.1f, 1.0f, scale * scale);
		    sample *= scale;
		    ret.push_back(sample);
		}

		return ret;
	}

	std::vector<glm::vec3> generateRotationTexture(int resolution) {
		std::vector<glm::vec3> ssaoNoise;
		for (int i = 0; i < resolution*resolution; i++)
		{
			glm::vec3 noise(
				ssaoRand.getFloat(0,1) * 2.0 - 1.0,
				ssaoRand.getFloat(0,1) * 2.0 - 1.0,
				0.0f);
			ssaoNoise.push_back(noise);
		}
		return ssaoNoise;
	}

	std::vector<float> generateRandomSampleTexture(int resolution, int numSamples) {
		std::vector<float> ssaoNoise;
		for (int i = 0; i < resolution*resolution; i++)
		{
			float noise(ssaoRand.getInt(0,numSamples-1) / 256.f);
			ssaoNoise.push_back(noise);
		}
		return ssaoNoise;
	}

	SSAOHandler::SSAOHandler(glm::ivec2 resolution, bool enableSSAO, int kernelSamples, int rotationTextureResolution, int scaleTextureResolution):
		enableSSAO(enableSSAO),
		kernelSamples(kernelSamples), rotationTextureResolution(rotationTextureResolution), scaleTextureResolution(scaleTextureResolution) {

		if(resolution.x <= 0 || resolution.y <= 0) {
			this->resolution = glm::ivec2(mSDL->viewportWidth, mSDL->viewportHeight);
		}

		this->resolution = resolution;

		kernel = generateSSAOKernel(kernelSamples);
		scaleArray = generateScaleArray(kernelSamples);
		rotationTexture = generateRotationTexture(rotationTextureResolution);
		randomSampleTexture = generateRandomSampleTexture(scaleTextureResolution, kernelSamples);

		std::vector<float> nullSSAOBuf = {1,1,1,1}; // 2x2 white pixels (1x1 probably works too, but w/e)
		nullSSAO = createTexture2D({2,2},&(nullSSAOBuf[0]),GL_R8,GL_NEAREST,GL_NEAREST,GL_RED,GL_FLOAT);

		if(enableSSAO) {
			ssaoOutput = new ColorBuffer(resolution,GL_R8,true);
			shader = new SSAOShaderProgram("ssao", false, {}, {"stdlib"});
			blurrer = new BlurHandler(resolution, GL_RED, 1, false, "blur/ssaoblur");
			gpuRotationTexture = createTexture2D(glm::ivec2(rotationTextureResolution), &(rotationTexture[0]),GL_RGB16F,GL_NEAREST,GL_REPEAT,GL_RGB,GL_FLOAT);
			gpuRandomSampleTexture = createTexture2D(glm::ivec2(scaleTextureResolution), &(randomSampleTexture[0]), GL_R8, GL_NEAREST, GL_REPEAT, GL_RED, GL_FLOAT);
		} else {
			ssaoOutput = nullptr;
			shader = nullptr;
			gpuRotationTexture = nullptr;
			blurrer = nullptr;
		}
	}

	Texture* SSAOHandler::runSSAO(DeferredRenderer* in, CameraBase* camera) {
		if(!enableSSAO) return nullSSAO; // TODO: return a black texture, not nullptr (idiot)

		mGL->bindFrameBuffer(ssaoOutput);

		glDisable(GL_CULL_FACE);
		glDisable(GL_DEPTH_TEST);
		glBlendFunc(GL_ONE,GL_ZERO);

		mGL->bindProgram(shader);
		shader->beginRender();

		mGL->bindTextures({
			TextureBinding{&(in->target[DeferredRenderer::G_BUFFER]),0,shader->gBufferTexture},
			TextureBinding{&(in->target[DeferredRenderer::NORMALS]),1,shader->normalTexture},
			TextureBinding{gpuRotationTexture,2,shader->texNoise},
			TextureBinding{gpuRandomSampleTexture,3,shader->randomSamples}
		});

		glm::mat4 invP = glm::inverse(camera->projection);
		glUniformMatrix4fv(shader->IP, 1, GL_FALSE, &invP[0][0]);

		glUniform3fv(shader->samples, kernelSamples, &(kernel[0][0]));
		glUniform1fv(shader->scales, kernelSamples, &(scaleArray[0]));

		uploadCameraMatrices(shader,camera);

		mGL->screenBuffer->bindBuffer();
		glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE,0,(void*) 0);

		glDrawArrays(GL_TRIANGLES, 0, 6);

		shader->endRender();

		glEnable(GL_CULL_FACE);
		glEnable(GL_DEPTH_TEST);

		blurrer->blur(ssaoOutput);

		return ssaoOutput->target;
	}

	SSAOShaderProgram::SSAOShaderProgram(const std::string& fileName, bool geometryShader, std::vector<std::string> macros, std::vector<std::string> libs, std::vector<std::string> inject):
		Program({"vertexPosition"}, fileName,
			{
				tMatDef,
				mkpr(gBufferTexture),
				mkpr(normalTexture),
				mkpr(texNoise),
				mkpr(IP),
				mkpr(samples),
				mkpr(scales),
				mkpr(randomSamples)

			}, {}, {}, geometryShader, macros, libs, inject) {}
}


