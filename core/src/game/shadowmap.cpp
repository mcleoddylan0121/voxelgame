/*
 * shadowmap.cpp
 *
 *  Created on: Jul 11, 2015
 *      Author: Dylan
 */

#include "shadowmap.h"
#include "glcontroller.h"
#include "framebuffer.h"
#include "program.h"
#include "worldstate.h"
#include "settingsmanager.h"
#include "sky.h"
#include "geometry.h"
#include "chunk.h"
#include "entitymanager.h"
#include "worldstaterender.h"

namespace vox {

	struct RenderChunkMeshShadows: public ChunkMeshRenderer {
		ShadowMap* parent;
		RenderChunkMeshShadows(ShadowMap* parent): parent(parent) {}
		void render(ChunkMeshDisplayObject ch) {
			parent->renderChunkMeshShadows(ch);
		}
		~RenderChunkMeshShadows() {}
	};

	struct RenderEntityMeshShadows: public EntityMeshRenderer {
		ShadowMap* parent;
		RenderEntityMeshShadows(ShadowMap* parent): parent(parent) {}
		void render(EntityMeshDisplayObject en) {
			parent->renderEntityMeshShadows(en);
		}
		~RenderEntityMeshShadows() {}
	};

	ShadowMap::ShadowMap(WorldStateRenderHandler* parent) {
		_parent = parent;
		shader = new ShadowShaderProgram("shadows", false);

		int shadowmapResolution = 1024; // Note: this was a setting.
		framebuffer = new CascadedShadowMap({shadowmapResolution, shadowmapResolution}, shadowCascades);

		chunkShadowRenderer = new RenderChunkMeshShadows(this);
		entityShadowRenderer = new RenderEntityMeshShadows(this);
		sunGeometryVPs = new ArrayBuffer<glm::mat4>(UNIFORM, STREAM, shadowCascades, 2, true);

		mGL->bindProgram(shader);

		sunGeometryVPs->generateBuffer();
		glUniformBlockBinding(shader->name, shader->sunVPs, 4);
		glBindBufferRange(GL_UNIFORM_BUFFER, 4, sunGeometryVPs->ID, 0, sizeof(glm::mat4) * sunGeometryVPs->getMaxSize());

		sunCams = new OrthoCamera[shadowCascades];
		for(int i = 0; i < shadowCascades; i++)
			sunCams[i] = OrthoCamera(-shadowCascadeSizes[i], shadowCascadeSizes[i], -shadowCascadeSizes[i], shadowCascadeSizes[i], -shadowCascadeSizes[i], shadowCascadeSizes[i], {0,0,0}, {1,1,1}, {0,1,0});
	}

	ShadowMap::~ShadowMap() {
		delete shader;
		delete framebuffer;
		delete chunkShadowRenderer;
		delete entityShadowRenderer;
		delete sunGeometryVPs;
		delete[] sunCams;
	}

	void ShadowMap::renderTo() {
		float cascadeOffset = 0;
		// calculate shadow cascade view projections
		for(int i = 0; i < shadowCascades; i++) { // loop from 0 to shadowCascades
			float cascadeMinSize = cascadeOffset; // use the equals operator to set one value to another
			float cascadeMaxSize = cascadeMinSize + 2*shadowCascadeSizes[i]; // use a combination of the equals, plus, times, and bracket operators to accomplish the desired result

			std::vector<glm::vec3> corners = mPlayer->cam.getCorners(cascadeMinSize, cascadeMaxSize); // use a black box to get some mysterious values. spoopy.
			glm::vec3 centroid(0);
			for(auto v: corners) centroid += v;
			centroid /= 8.f;

			sunCams[i].pos = centroid + _parent->sky->sun.invDirection * 1.f;
			sunCams[i].lookAt = centroid;
			sunCams[i].up = glm::vec3(0, 1, 0);

			glm::mat4 view = glm::lookAt(sunCams[i].pos, sunCams[i].lookAt, sunCams[i].up);
			glm::vec4 mins(view * glm::vec4(corners[0], 1)), maxes(view * glm::vec4(corners[0], 1));

			for (auto v: corners) {
				glm::vec4 l = view * glm::vec4(v, 1.f);
				if (l.x > maxes.x) 		maxes.x = l.x;
				else if (l.x < mins.x) 	mins.x = l.x;
				if (l.y > maxes.y)		maxes.y = l.y;
				else if (l.y < mins.y)	mins.y = l.y;
				if (l.z > maxes.z)		maxes.z = l.z;
				else if (l.z < mins.z)	mins.z = l.z;
			}

			sunCams[i].setOrtho(mins.x, maxes.x, mins.y, maxes.y, mins.z, maxes.z + 10.f);

			sunCams[i].update();


			sunGeometryVPs->set(i, sunCams[i].viewProjection);

			cascadeOffset += 2*shadowCascadeSizes[i];
		}

		sunGeometryVPs->streamData();

		mGL->bindFrameBuffer(framebuffer);
		mGL->bindProgram(shader);

		glUniform3fv(shader->cameraPos, 1, &mPlayer->cam.pos[0]);

		mGL->bindTexture(&framebuffer->target[0], 0);

		for(int i = 0; i < shadowCascades; i++) {
			shader->beginRender();
			framebuffer->selectCascade(i);
			curCascade = i;
			framebuffer->clear();
			_parent->parent->entityManager->render(entityShadowRenderer);
			_parent->parent->map->renderChunks(chunkShadowRenderer);
			shader->endRender();
			cascadeSampleTime++;
		}
	}

	void ShadowMap::renderChunkMeshShadows(ChunkMeshDisplayObject ch) {
		ch.buf->calculateElements();

		if(ch.buf->elements <= 0) return;
		WorldCoordinate position;
		decomposeMatrix(ch.model,position);
		float r = rad(ch.scale);
		/*bool fCull = false;
		for(CameraBase& c: sunCams)
			fCull |= c.containsSphere(position + scale/2.f, r);
		if(!fCull) return;*/
		if(!sunCams[curCascade].containsSphere(position + ch.scale/2.f, r)) return;

		mGL->renderShadows(ch.buf, shader, &(sunCams[curCascade]), ch.model);
	}

	void ShadowMap::renderEntityMeshShadows(EntityMeshDisplayObject en) {
		en.buf->calculateElements();

		if(en.buf->elements <= 0) return;
		WorldCoordinate position;
		decomposeMatrix(en.model,position);
		float r = rad(en.scale);
		/*bool fCull = false;
		for(CameraBase& c: sunCams)
			fCull |= c.containsSphere(position + scale/2.f, r);
		if(!fCull) return;*/
		if(!sunCams[curCascade].containsSphere(position + en.scale/2.f, r)) return;

		mGL->renderShadows(en.buf, shader, &(sunCams[curCascade]), en.model);
	}

}
