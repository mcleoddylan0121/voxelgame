/*
 * gameloop.h
 *
 *  Created on: March 22, 2014
 *      Author: Dani
 */

#ifndef GAMELOOP_H_
#define GAMELOOP_H_

#include <list>
#include <vector>

namespace vox {

	class FrameBuffer;
	// Inherit this class and get stating!
	class GameState {
	private:
	protected:
	public:
		virtual ~GameState() {}

		virtual void render(FrameBuffer* fbo) = 0;
		virtual void tick(float delta) = 0;
	};

	class GameLoop {
	private:
		std::list<GameState *> curStates;
		std::list<GameState *> trashStates;
		bool running;

		// FPS and delta calculation
		int fps, frame, frameTime;
		long long lastTime, accumulator;
		const int DELTA_LIMIT_UPPER = 1000 / 20,
				  DELTA_LIMIT_LOWER = 1000 / 90; // FPS cap

		void loop();

	public:

		GameLoop();
		virtual ~GameLoop();

		void start();

		void kill();
		bool isKill();

		int getFPS();

		void addStateToFront(GameState * s);
		void addStateToBack(GameState * s);
		void removeState(GameState * s);

		long getTimeInMillis(); // Thread safe version of mSDL->getTimeInMillis, updated every render cycle

		std::list<GameState *> getGameStates();

		friend class FPSAnalytics;

		std::vector<float> aaaaTempDelta;
	};


	extern GameLoop mGameLoop;
}

#endif /* GAMELOOP_H_ */
