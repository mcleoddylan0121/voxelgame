/*
 * shadowmap.h
 *
 *  Created on: Jul 11, 2015
 *      Author: Dylan
 */

#ifndef SHADOWMAP_H_
#define SHADOWMAP_H_

#include "coords.h"
#include "renderutils.h"

namespace vox {

	const int shadowCascades = 3;
	const float shadowCascadeSizes[shadowCascades] = {
			8, 16, 32
	};

	class WorldStateRenderHandler;
	struct ShadowShaderProgram;
	struct CascadedShadowMap;
	struct OrthoCamera;
	template<typename T> struct ArrayBuffer;
	struct ChunkMeshBuffer;
	struct EntityMeshBuffer;


	class ShadowMap {
	public:
		ShadowMap(WorldStateRenderHandler* parent);
		~ShadowMap();
		int curCascade = 0;
		ShadowShaderProgram* shader;
		CascadedShadowMap* framebuffer;
		OrthoCamera* sunCams;
		ArrayBuffer<glm::mat4>* sunGeometryVPs;

		EntityMeshRenderer* entityShadowRenderer;
		ChunkMeshRenderer* chunkShadowRenderer;

		WorldStateRenderHandler* _parent;

		int cascadeSampleTime = 0;

		void renderTo();

		void renderChunkMeshShadows(ChunkMeshDisplayObject ch);
		void renderEntityMeshShadows(EntityMeshDisplayObject en);
	};

}



#endif /* SHADOWMAP_H_ */
