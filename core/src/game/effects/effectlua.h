/*
 * effectslua.h
 *
 *  Created on: Oct 19, 2015
 *      Author: Daniel
 */

#ifndef CORE_SRC_GAME_ENTITIES_EFFECTSLUA_H_
#define CORE_SRC_GAME_ENTITIES_EFFECTSLUA_H_

#include "effectutil.h"
#include "vluautil.h"

namespace vox {

	struct EffectManager;

	struct EffectLibrary {
		static EffectID create();
		static void startStop(EffectID, bool start);

		static void setDurations(EffectID id, float duration, float lingeringDuration);
		static void setTimeLeft(EffectID id, float newTime);

		static float getTimeLeft(EffectID id);
		static lvec3 getPos(EffectID id);

		static void remove(EffectID id);
		static void addLightComp(EffectID id, lvec4 color, float intensity, float rad, float compensation);
		static void addSpriteComp(EffectID id, std::string texName, lvec2 texOffs, lvec2 texSize, float rot, lvec2 scale, lvec4 col);
		static void addParticleEmitterComp(
				EffectID id, EffectID particle,
				lvec4 scale, lvec2 particleCD, lvec2 particleLife,
				bool rotate,
				lvec2 theta, lvec2 phi, lvec2 rho,
				lvec3 acceleration
				);

		// Temporary until I make paths
		static void setPos(EffectID id, lvec3 pos);

		static EffectManager * _m;
	};


}

#endif /* CORE_SRC_GAME_ENTITIES_EFFECTSLUA_H_ */
