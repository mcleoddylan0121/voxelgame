/*
 * effectslua.cpp
 *
 *  Created on: Oct 19, 2015
 *      Author: Daniel
 */

#include "effect.h"
#include "effectlua.h"
#include "effectmanager.h"
#include "resourcemanager.h"
#include "miscmesh.h"

#define _chk() EffectManager::iterator e = _m->getEffect(id); if (!e.exists()) return;
#define _chk2(x) EffectManager::iterator e = _m->getEffect(id); if (!e.exists()) return x;

namespace vox {

	EffectManager * EffectLibrary::_m;

	EffectID EffectLibrary::create() {
		Effect * e = new Effect();
		EffectID id = ++_m->_curID; // ++curID because 0 is not a valid ID
		e->id = id;
		_m->_effects[id] = e;
		return id;
	}

	void EffectLibrary::startStop(EffectID id, bool start) {
		_chk();
		if (start) e->start();
		else e->stop();
	}

	void EffectLibrary::setDurations(EffectID id, float dur, float lingDur) {
		_chk();
		e->duration = dur;
		e->lingeringDuration = lingDur;
	}

	void EffectLibrary::setTimeLeft(EffectID id, float timeLeft) {
		_chk();
		e->timeLeft = timeLeft;
	}

	float EffectLibrary::getTimeLeft(EffectID id) {
		_chk2(0);
		return e->timeLeft;
	}

	lvec3 EffectLibrary::getPos(EffectID id) {
		_chk2(lvec3());
		lvec3 ans;
		ans.init(e->pos);
		return ans;
	}

	void EffectLibrary::remove(EffectID id) {
		_chk();
		_m->removeEffect(id);
	}

	void EffectLibrary::addLightComp(EffectID id, lvec4 color, float intensity, float rad, float compensation) {
		_chk();
		auto a = new LightComponent();
		a->c = Color::HighpColor(color);
		a->intensity = intensity;
		a->rad = rad;
		a->compensation = compensation;
		e->components.push_back(a);
	}

	void EffectLibrary::addSpriteComp(EffectID id, std::string texName, lvec2 texOffs, lvec2 texSize, float rot, lvec2 scale, lvec4 col) {
		_chk();
		auto a = new SpriteComponent;
		a->c = Color::HighpColor(col);
		a->rot = rot;

		auto tex = mRes->getResource<Texture, Asset::Texture>(texName);
		if (texSize.x < 0) texSize.x = tex->size.x;
		if (texSize.y < 0) texSize.y = tex->size.y;
		a->tex = tex;
		a->UVstart = texOffs;
		a->UVsize = texSize;
		a->size = glm::vec2(texSize) * (glm::vec2)scale;

		e->components.push_back(a);
	}



	void EffectLibrary::addParticleEmitterComp(
				EffectID id, EffectID particle,
				lvec4 scale, lvec2 particleCD, lvec2 particleLife,
				bool rotate,
				lvec2 theta, lvec2 phi, lvec2 rho,
				lvec3 acceleration
				) {
		_chk();
		EffectManager::iterator p = _m->getEffect(particle);
		if (!p.exists()) return;

		auto a = new ParticleEmitterComponent();

		// Steal the components from the particle effect, as it's going to be deleted right after this
		for (unsigned int i = 0; i < p->components.size(); i++) {
			a->particleTemplate.push_back(p->components[i]);
			p->components[i] = 0;
		}

		_m->_effects.erase(_m->_effects.find(particle));

		a->acceleration = acceleration;
		a->scale = scale;
		a->particleLife = particleLife;
		a->particleCD = particleCD;
		a->phi = phi;
		a->rho = rho;
		a->theta = theta;
		a->rotate = rotate;

		e->components.push_back(a);
	}

	void EffectLibrary::setPos(EffectID id, lvec3 pos) {
		_chk();
		e->pos = WorldCoordinate(pos);
	}

}
