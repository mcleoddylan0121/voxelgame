/*
 * effectutil.h
 *
 *  Created on: Oct 25, 2015
 *      Author: Dani
 */

#ifndef CORE_SRC_GAME_EFFECTS_EFFECTUTIL_H_
#define CORE_SRC_GAME_EFFECTS_EFFECTUTIL_H_

#include "renderutils.h"

namespace vox {
	typedef int EffectID;

	struct EffectRenderer : public BillboardRenderer, public DynamicLightRenderer {};
}


#endif /* CORE_SRC_GAME_EFFECTS_EFFECTUTIL_H_ */
