/*
 * effectsmanager.cpp
 *
 *  Created on: Oct 19, 2015
 *      Author: Daniel
 */

#include "effect.h"
#include "effectlua.h"
#include "effectmanager.h"
#include "worldstate.h"

namespace vox {

	Effect* EffectManager::iterator::operator->() { return e; }
	EffectManager::iterator::operator Effect *() { return e; }
	bool EffectManager::iterator::exists() { return found; }


	EffectManager::EffectManager() {
		EffectLibrary::_m = this;
		_parent = 0;
	}

	EffectManager::~EffectManager() {
		for (auto i = _effects.begin(); i != _effects.end(); i++)
			delete i->second;
	}

	void EffectManager::init(WorldState * parent) {
		_parent = parent;
	}

	void EffectManager::tick(float delta) {
		for (auto i = _toRemove.rbegin(); i != _toRemove.rend(); i++) {
			_effects.erase(*i);
			_toRemove.pop_back();
		}

		for (auto i : _effects)
			i.second->tick(delta);
	}

	void EffectManager::render(EffectRenderer* r) {
		for (auto i : _effects)
			i.second->render(r);
	}

	void EffectManager::removeEffect(EffectID id) {
		_toRemove.push_back(id);
	}

	EffectManager::iterator EffectManager::getEffect(EffectID id) {
		auto it = _effects.find(id);
		iterator ret;
		if (it == _effects.end()) {
			ret.e = 0;
			ret.found = false;
		} else {
			ret.e = it->second;
			ret.found = true;
		}
		return ret;
	}

}
