/*
 * effects.cpp
 *
 *  Created on: Oct 19, 2015
 *      Author: Daniel
 */

#include "effect.h"
#include "effectlua.h"
#include "effectmanager.h"

namespace vox {

	// Effect
	void Effect::tick(float delta) {
		if (started && timeLeft <= 0) {
			if (lingerTime <= 0 && lingeringDuration >= 0)
				EffectLibrary::_m->removeEffect(id);
			else lingerTime -= delta;
		} else {
			timeLeft -= delta;
			for (auto i : components)
				i->tick(delta);
		}
	}

	void Effect::render(EffectRenderer * r) {
		for (auto i : components)
			i->render(r, pos);
	}

	void Effect::start() {
		timeLeft = duration;
		lingerTime = lingeringDuration;
		started = true;
	}

	void Effect::stop() {
		started = false;
	}

	Effect::~Effect() {
		for (auto i : components)
			delete i;
	}

	// Components

	EffectComponent::~EffectComponent() {}

	void LightComponent::render(EffectRenderer * r, glm::vec3 pos) {
		DynamicLightRenderer * a = r;
		a->render(DynamicLightDisplayObject(pos, rad, c, intensity, compensation));
	}

	LightComponent* LightComponent::getInstance() {
		auto a = new LightComponent();
		a->c = c;
		a->intensity = intensity;
		a->rad = rad;
		a->compensation = compensation;
		return a;
	}

	void SpriteComponent::render(EffectRenderer * r, glm::vec3 pos) {
		BillboardRenderer * a = r;
		a->render(BillboardDisplayObject(tex, pos, size, UVstart, UVsize, rot, c));
	}

	SpriteComponent* SpriteComponent::getInstance() {
		auto a = new SpriteComponent();
		a->tex = tex;
		a->UVstart = UVstart;
		a->UVsize = UVsize;
		a->c = c;
		a->rot = rot;
		a->size = size;
		return a;
	}


	EffectParticle::~EffectParticle() {
		for (auto i : components)
			delete i;
	}

	ParticleEmitterComponent::~ParticleEmitterComponent() {
		for (auto i : particleTemplate)
			delete i;
		for (auto i : particles)
			delete i;
	}

	void ParticleEmitterComponent::render(EffectRenderer * r, glm::vec3 pos) {
		for (EffectParticle* p : particles)
			for (auto i : p->components)
				i->render(r, pos + p->pos);
	}

	void ParticleEmitterComponent::tick(float delta) {
		// Iterate backward here so we can delete particles as we go without invalidating iterators
		for (auto p = particles.rbegin(); p != particles.rend(); p++) {
			(*p)->timeLeft -= delta;
			if ((*p)->timeLeft <= 0) {
				particles.erase(std::next(p).base());
				continue;
			}

			(*p)->m.tick(delta);
			(*p)->pos += (*p)->m.delta_d;
			for (EffectComponent* ec : (*p)->components)
				ec->tick(delta);
		}

		if (timeLeft <= 0) {
			timeLeft = rng.getFloat(this->particleCD);
			spawnParticle();
		}

		timeLeft -= delta;
	}

	void ParticleEmitterComponent::spawnParticle() {
		EffectParticle* p = new EffectParticle();

		for (EffectComponent* ec : particleTemplate) {
			EffectComponent* a = ec->getInstance();
			a->size *= glm::vec2(rng.getFloat(scale[0], scale[2]), rng.getFloat(scale[1], scale[3]));
			(*p).components.push_back(a);
		}

		(*p).pos = WorldCoordinate(0);
		(*p).timeLeft = rng.getFloat(this->particleLife);

		(*p).m.acceleration = acceleration;
		float theta = rng.getFloat(this->theta);
		float phi = rng.getFloat(this->phi);
		float rho = rng.getFloat(this->rho);
		(*p).m.velocity = WorldCoordinate(rho * cos(theta) * sin(phi), rho * cos(phi), rho * sin(theta) * sin(phi));

		particles.push_back(p);
	}

	ParticleEmitterComponent* ParticleEmitterComponent::getInstance() {
		auto a = new ParticleEmitterComponent();

		for (EffectComponent* ec : particleTemplate)
			a->particleTemplate.push_back(ec->getInstance());

		a->acceleration = acceleration;
		a->scale = scale;
		a->particleLife = particleLife;
		a->particleCD = particleCD;
		a->phi = phi;
		a->rho = rho;
		a->theta = theta;
		a->rotate = rotate;

		return a;
	}
}
