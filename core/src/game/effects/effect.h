/*
 * effects.h
 *
 *  Created on: Oct 19, 2015
 *      Author: Daniel
 */

#ifndef CORE_SRC_GAME_ENTITIES_EFFECTS_H_
#define CORE_SRC_GAME_ENTITIES_EFFECTS_H_

#include "coords.h"
#include "effectutil.h"
#include "colormath.h"
#include "entityphysics.h"
#include "util.h"

#include <vector>

namespace vox {

	struct EffectRenderer;
	struct EffectComponent;

	struct Effect {
		float duration, lingeringDuration, timeLeft = 0, lingerTime = 0;
		bool started = false;
		WorldCoordinate pos;
		EffectID id;

		std::vector<EffectComponent *> components;

		void tick(float delta);
		void render(EffectRenderer * r);

		void start();
		void stop();

		virtual ~Effect();
	};

	struct EffectComponent {
		glm::vec2 size = glm::vec2(1, 1);

		virtual ~EffectComponent();
		virtual void render(EffectRenderer * r, glm::vec3 pos) = 0;
		virtual EffectComponent* getInstance() = 0;
		virtual void tick(float delta) {};
	};

	struct LightComponent : public EffectComponent {
		Color::HighpColor c;
		float intensity;
		float rad;
		float compensation;

		void render(EffectRenderer * r, glm::vec3 pos);
		LightComponent* getInstance();
	};

	struct SpriteComponent : public EffectComponent {
		Color::HighpColor c;
		float rot;

		Texture* tex;
		glm::ivec2 UVstart, UVsize;

		void render(EffectRenderer * r, glm::vec3 pos);
		SpriteComponent* getInstance();
	};

	struct EffectParticle {
		float timeLeft;
		WorldCoordinate pos;
		MotionObject m;

		std::vector<EffectComponent*> components;

		virtual ~EffectParticle();
	};

	struct ParticleEmitterComponent : public EffectComponent {
		glm::vec4 scale;
		glm::vec2 particleCD, particleLife;
		bool rotate;
		glm::vec2 theta, phi, rho;
		glm::vec3 acceleration;

		Rand rng;
		std::vector<EffectComponent*> particleTemplate;
		std::vector<EffectParticle*> particles;

		float timeLeft = 0; // to next particle spawn

		~ParticleEmitterComponent();

		void render(EffectRenderer * r, glm::vec3 pos);
		void tick(float delta);

		ParticleEmitterComponent* getInstance();

	private:
		void spawnParticle();
	};
}


#endif /* CORE_SRC_GAME_ENTITIES_EFFECTS_H_ */
