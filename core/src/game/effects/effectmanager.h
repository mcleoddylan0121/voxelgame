/*
 * effectsmanager.h
 *
 *  Created on: Oct 19, 2015
 *      Author: Daniel
 */

#ifndef CORE_SRC_GAME_ENTITIES_EFFECTSMANAGER_H_
#define CORE_SRC_GAME_ENTITIES_EFFECTSMANAGER_H_

#include "effectutil.h"
#include "renderutils.h"

#include <unordered_map>

namespace vox {

	struct Effect;
	struct EffectLibrary;
	struct WorldState;
	struct EffectRenderer;

	struct EffectManager {
		friend struct EffectLibrary;

		struct iterator {
			friend struct EffectManager;
			Effect * operator->();
			operator Effect*();

			bool exists();

		private:
			bool found;
			Effect * e;
		};

		EffectManager();
		void init(WorldState * parent);
		virtual ~EffectManager();

		void tick(float delta);
		void render(EffectRenderer* render);

		iterator getEffect(EffectID id);
		void removeEffect(EffectID id);

	private:
		// Entity id's start at 1, so 0 is an invalid ID
		std::unordered_map<EffectID, Effect*> _effects;
		std::vector<EffectID> _toRemove;

		EffectID _curID = 0;

		WorldState * _parent;
	};

}


#endif /* CORE_SRC_GAME_ENTITIES_EFFECTSMANAGER_H_ */
