/*
 * chunk.cpp
 *
 *  Created on: Mar 25, 2015
 *      Author: Dylan
 */

#include "chunk.h"
#include "worldstate.h"
#include "console.h"
#include "flora.h"
#include "resourcemanager.h"
#include "renderutils.h"
#include "entitymanager.h"
#include "geometry.h"
#include "worldstaterender.h"
#include "structuremanager.h"

#include <ctime>
#include <cstdlib>
#include <algorithm>

namespace vox {
	const int Chunk::SIZE = 32;
	const int Chunk::LIFE = 5; // The time a chunk lives scales negatively with distance to player


	ChunkGenerator::ChunkGenerator(long seed, ChunkMap* parent): parent(parent) {
		Rand r = Rand(Rand({seed}).getSeedSeq(10));
		_seed = seed;

		terrainHeight = noise::Noise::readFile("not_cubeworld", r, "height");
		terrainMoisture = noise::Noise::readFile("not_cubeworld", r, "moisture");
		tempBiomeNoise = new noise::CopyNoise<1> (noise::Noise::readFile("not_cubeworld", r, "biome"));

		terrainNoise = new noise::CombinedNoise( {
			new noise::OctaveNoise(200.f, 0.3f, r.getSeedSeq(5), 1000000, 0, 1, noise::makeGen<noise::OpenSimplex>)
		});
	}

	ChunkGenerator::~ChunkGenerator() {
		delete terrainNoise;
		delete terrainMoisture;
		delete terrainHeight;
		delete tempBiomeNoise;
	}

	glm::ivec3 glmMax(glm::ivec3 a, glm::ivec3 b) {
		return {std::max(a.x, b.x), std::max(a.y, b.y), std::max(a.z, b.z)};
	}

	LightLevel sunlight = LightLevel::fromLABColor(BLACK);

	Chunk * ChunkGenerator::generateChunk(ChunkCoordinate& pos, Palette * mapping, StructureUnit* structureUnit) {
		Chunk * ch = new Chunk(pos, mapping, tempBiomeNoise, parent);
		WorldCoordinate worldPos = chunkToWorldCoordinate(pos);

		ChunkHash hash;
		Rand floraRand = Rand({(unsigned long)hash(pos), (unsigned long)_seed, (unsigned long)(hash(pos) | _seed)});
		float torchDistribution = 0.001f, flowerDistribution = 0.009f;

		int aaaaaaa = 41;
		glm::vec3 noiseStart = worldPos - glm::vec3(2);
		glm::ivec3 noiseSize = glm::ivec3(aaaaaaa);
		float* noise = terrainHeight->range3D(noiseStart, noiseSize);
		for(int z = -1; z <= Chunk::SIZE; z++) for(int x = -1; x <= Chunk::SIZE; x++) for(int y = -1; y <= Chunk::SIZE; y++) {
			// float noise = 	 terrainNoise->   noise2D({pos.x * Chunk::SIZE + x, pos.z * Chunk::SIZE + z});
			// float moisture = 	   terrainMoisture->noise2D({pos.x * Chunk::SIZE + x, pos.z * Chunk::SIZE + z});

			float noiseValue = noise[ (x+2) + (y+3)*aaaaaaa + (z+2)*aaaaaaa*aaaaaaa];
			float noiseValuePlusOne = noise[ (x+2) + (y+4)*aaaaaaa + (z+2)*aaaaaaa*aaaaaaa];
			float noiseValuePlusThree = noise[ (x+2) + (y+6)*aaaaaaa + (z+2)*aaaaaaa*aaaaaaa];
			float noiseValueMinusOne = noise[ (x+2) + (y+2)*aaaaaaa + (z+2)*aaaaaaa*aaaaaaa];
			float threshold = 0.f;
			bool solid = noiseValue>threshold;
			bool solidGround = noiseValueMinusOne > threshold;
			VoxelID id = solid?(noiseValuePlusOne>threshold?(noiseValuePlusThree>threshold?3:2):1):0;

			// We can place flora here
			if(!solid && solidGround) {

				if(floraRand.flipCoin(torchDistribution)) {
					id = 4;
				} else if(floraRand.flipCoin(flowerDistribution)) {
					id = 5 + rand()%5;
				}

			}

			//VoxelID id = solid?1:0;
			LocalCoordinate vCoord(x,y,z);
			ch->mesh->unsafeSet(
				id,
				vCoord
			);
		}
		delete[] noise;


		if(structureUnit) {

			for(VoxelEdit e:structureUnit->edits) {
				ch->mesh->unsafeSet(e.id,e.position);
			}
		}


		ch->mesh->generateMesh();

		return ch;
	}

	BiomeMap::BiomeMap() {
		mapSize = {3, 3, 3};
		map = new Color[(int)(mapSize.x * mapSize.y * mapSize.z)];
		for (int height = 0; height < 3; height++) {

		}
	}

	BiomeMap::~BiomeMap() {
		delete[] map;
	}

	Color BiomeMap::interpolate(BiomeCoordinate b) {
		return getColor(toIndex(b * mapSize));
	}

	Color BiomeMap::getColor(BiomeCoordinate b) {
		return map[(int)(b.x + b.y * mapSize.x + b.z * mapSize.x * mapSize.y)];
	}

	void BiomeMap::setColor(Color c, BiomeCoordinate b) {
		map[(int)(b.x + b.y * mapSize.x + b.z * mapSize.x * mapSize.y)] = c;
	}

	BiomeMap::BiomeCoordinate BiomeMap::toIndex(BiomeCoordinate b) { return glm::floor(b); }

	bool ChunkMap::isLoadedYet(const Chunk* ch) {
		return ch && ch != tempChunk && ch->mesh;
	}

	void ChunkMap::init(WorldState* parent) {
		structures = new StructureManager{};
		_depthSort.parent = this;
		_parent = parent;
		voxelMap = mRes->getResource<Palette, Asset::Palette>("palette");
		for(int i = 4; i < 10; i++) {
			voxelMap->_map->flags |= Voxel::TRANSPARENT;
		}

		int numVox = 9;

		actualPalette = new TerrainPalette(numVox+1);
		std::string files[] = {"grass","dirt","stone","torch","red flower", "blue flower", "purple flower", "yellow flower", "sunflower"};


		BasicTerrainPaletteVoxel* air = new BasicTerrainPaletteVoxel{};
		air->theMesh = new TerrainPaletteVoxelMesh();
		actualPalette->addVoxel(air);


		for(int i = 0; i < 2; i++){ // load up chad.vox
			EntityMesh* tempMesh = mRes->getResource<EntityMesh,Asset::Mesh>("voxelmodels/chad");
			std::vector<SubVoxel> tempVoxels(tempMesh->size.x*tempMesh->size.y*tempMesh->size.z);
			for(unsigned int j = 0; j < tempVoxels.size(); j++) {
				Voxel voxe = tempMesh->voxelMap->get(tempMesh->getVoxelArray()[j]);
				SubVoxel vocce;
				vocce.exists = !(voxe.is(Voxel::IMAGINARY));
				vocce.color = voxe.color;
				vocce.lighting = glm::vec3(0);

				if( glm::distance(glm::vec4(vocce.color.toGPUColor()), glm::vec4(225,191,23,255)) < 30 )
					vocce.lighting = glm::vec3(0.6,0.6,0.2);

				tempVoxels[j] = vocce;
			}

			TerrainPaletteVoxelMesh* someCoolMesh = new TerrainPaletteVoxelMesh(tempMesh->size,tempVoxels);

			TiledTerrainPaletteVoxel* thePaletteVoxel = generateTiledTerrainPaletteVoxel(someCoolMesh);
			actualPalette->addVoxel(thePaletteVoxel);
		}


		for(int i = 2; i < numVox; i++) {
			EntityMesh* tempMesh = mRes->getResource<EntityMesh,Asset::Mesh>("voxelmodels/"+files[i]);
			std::vector<SubVoxel> tempVoxels(tempMesh->size.x*tempMesh->size.y*tempMesh->size.z);
			for(unsigned int j = 0; j < tempVoxels.size(); j++) {
				Voxel voxe = tempMesh->voxelMap->get(tempMesh->getVoxelArray()[j]);
				SubVoxel vocce;
				vocce.exists = !(voxe.is(Voxel::IMAGINARY));
				vocce.color = voxe.color;
				vocce.lighting = glm::vec3(0);

				if( glm::distance(glm::vec4(vocce.color.toGPUColor()), glm::vec4(225,191,23,255)) < 30 )
					vocce.lighting = glm::vec3(0.6,0.6,0.2);

				tempVoxels[j] = vocce;
			}

			BasicTerrainPaletteVoxel* thePaletteVoxel = new BasicTerrainPaletteVoxel{};
			thePaletteVoxel->theMesh = new TerrainPaletteVoxelMesh(tempMesh->size,tempVoxels);
			thePaletteVoxel->theMesh->ID = i+1;
			actualPalette->addVoxel(thePaletteVoxel);
		}

		actualPalette->makeUVMap();

		gen = new ChunkGenerator(rand(), this);
		ChunkCoordinate pos = {0, 50, 0};
		tempChunk = gen->generateChunk(pos, voxelMap, nullptr);
		tempChunk->sendMeshToGPU();

		for (unsigned int i = 0; i < NUM_CHUNKLOADERS; i++)
			loaders[i].start(gen, this, true);
	}

	ChunkMap::~ChunkMap() {
		for (unsigned int i = 0; i < NUM_CHUNKLOADERS; i++)
			loaders[i].stop();

		auto it = begin();
		while (it != end()) {
			if (isLoadedYet(it->second))
				delete it->second;
			it++;
		}
		chunks.clear();

		delete tempChunk;
		delete gen;
	}

	void ChunkMap::setChunk(ChunkCoordinate& position, Chunk* someChunk) {
		chunks[position] = someChunk;
	}

	Chunk* ChunkMap::getChunk(ChunkCoordinate& position) {
		Chunk* ch = chunks[position];
		return ch;
	}

	Voxel ChunkMap::getVoxel(VoxelCoordinate& pos) {
		ChunkCoordinate cpos = voxelToChunkCoordinate(pos);
		Chunk * c = getChunk(cpos);
		if (isLoadedYet(c) && c->mesh) {
			LocalCoordinate clocal = voxelToLocalChunkCoordinate(pos);
			return c->get(clocal);
		}
		else
			return voxelMap->get(0); // pffft... whatever
	}

	void ChunkMap::removeVoxel(VoxelCoordinate& pos) {
		setVoxel(0, pos);
	}

	void ChunkMap::setVoxel(VoxelID vox, VoxelCoordinate& pos) {
		ChunkCoordinate cpos = voxelToChunkCoordinate(pos);
		LocalCoordinate clocal = voxelToLocalChunkCoordinate(pos);
		Chunk* ch = getChunk(cpos);

		if(!isLoadedYet(ch)) return; // if the chunk containing this isn't loaded, we probably shouldn't place it
		if(ch->getVoxelID(clocal) == vox) return; // don't want to beat a dead horse, because this is already here

		setLight(pos, getLightLevelfromVoxelAndID(vox, voxelMap->get(vox)));

		std::vector<Chunk*> chs = ch->getChunksThatContainPoint(glm::i8vec3(clocal));
		for(Chunk* c: chs) {
			if(isLoadedYet(c)) {
				LocalCoordinate nClocal = clocal - Chunk::SIZE * (c->position - cpos);
				c->mesh->set(vox, nClocal);
				c->mesh->updatePosition(nClocal);
			}

		}
	}

	void Chunk::setLightLevel(glm::i8vec3 pos, LightLevel lev) {
		mesh->fastSetVoxel(pos, lev.getVoxelID());
		updateLighting = true;
	}

	Chunk::Chunk(ChunkCoordinate position, Palette * mapping, noise::MultiNoise<1>* meshColorNoise, ChunkMap* parent):
			position(position), parent(parent) {
		mesh = new ChunkMesh({(unsigned char)SIZE, (unsigned char)SIZE, (unsigned char)SIZE}, mapping, meshColorNoise, chunkToWorldCoordinate(position),
				parent->_parent->renderHandler->liquidManager, parent->actualPalette);
		lifeCounter = LIFE;
	}

	Chunk::~Chunk() {
		mesh->deleteBuffer();
		delete mesh;
		for (Flora * f: flora)
			delete f;
	}

	void Chunk::tick(float ttime) {
		lifeCounter -= std::exp((worldToChunkCoordinate(mPlayer->player->getPos()) - position).length()) * ttime;
		if(updateLighting) {
			mesh->updateLightBuffer();
			mesh->buf->streamLightData();
			updateLighting = false;
		}
	}
	void Chunk::resetLifeCounter() { lifeCounter = Chunk::LIFE; }
	bool Chunk::checkForDeletion() { return lifeCounter <= 0; }

	void ChunkMap::loadInRadius(ChunkCoordinate position, int radius) { loadInRadius(position, ChunkCoordinate(radius, radius, radius)); }
	void ChunkMap::loadInRadius(ChunkCoordinate position, ChunkCoordinate radii) {
		for(int x = position.x - radii.x; x <= position.x + radii.x; x++)
			for(int y = position.y - radii.y; y <= position.y + radii.y; y++)
				for(int z = position.z - radii.z; z <= position.z + radii.z; z++) {
					ChunkCoordinate pos = {x,y,z};
					Chunk * ch = getChunk(pos);
					if(!ch) {
						createChunk(pos);
					}
					else
						ch->resetLifeCounter();
				}
	}

	std::unordered_map<ChunkCoordinate, Chunk*, ChunkHash>::iterator ChunkMap::begin() { return chunks.begin(); }
	std::unordered_map<ChunkCoordinate, Chunk*, ChunkHash>::iterator ChunkMap::end()   { return chunks.end();   }



	std::chrono::high_resolution_clock::time_point beginTime2;

	float total2 = 0, timesStamped2 = 0;

	void ChunkMap::tick(float ttime) {

		structures->tick(ttime);

		for(int i = chunkStructuresToLoad.size()-1; i>=0; --i) {
			ChunkCoordinate coord = chunkStructuresToLoad[i];

			if(!structures->structureUnitAvailible(coord)) continue;

			chunkLoadBuffer.push_back(coord);

			chunkStructuresToLoad.erase(chunkStructuresToLoad.begin()+i);
		}


		if (!chunkLoadBuffer.empty()) {
			for (unsigned int i = 0; i < NUM_CHUNKLOADERS; i++)
				loaders[i].startBatch();
			for (unsigned int i = 0; i < chunkLoadBuffer.size(); i++) {
				loaders[curLoader++].loadChunk(chunkLoadBuffer[i]);
				curLoader %= NUM_CHUNKLOADERS;
			}
			for (unsigned int i = 0; i < NUM_CHUNKLOADERS; i++) {
				loaders[i].endBatch();
				loaders[i].updateChunkSorter({mPlayer->player->bb.pos, mPlayer->cam});
			}
		}
		chunkLoadBuffer.clear();

		auto it = begin();
		while (it != end()) {
			Chunk* ch = it->second;
			if (isLoadedYet(ch)) { // does it exist?
				if (ch->checkForDeletion()) {
					delete ch;
					chunks.erase(it);    // erase it from the map
					it = begin();
					continue;
				}
			}
			it++;
		}

		it = begin();
		while (it != end()) {
			Chunk* ch = it->second;
			if (isLoadedYet(ch)) {
				for(int i = 0; i < 3; i++)
					if(ch->lightsToAdd[i].size() > 0 || ch->lightsToRemove[i].size() > 0) {
						ch->resolveLighting(i);
						goto end; //sorry
					}
			}
			it++;
		} end:

		int updatedLocs = 0;
		it = begin();
		while (it != end()) {
			Chunk* ch = it->second;
			if (isLoadedYet(ch)) {
				updatedLocs += ch->mesh->locsToUpdate.size();
				ch->mesh->updateMesh();
				if(updatedLocs > 400)
					break;
			}
			it++;
		}

		it = begin();
		while (it != end()) {
			Chunk* ch = it->second;
			if (isLoadedYet(ch))
				ch->tick(ttime);
			it++;
		}

		for (unsigned int i = 0; i < NUM_CHUNKLOADERS; i++) {
			while (loaders[i].hasNext()) {
				ChunkData * cd = loaders[i].next();
				auto found = chunks.find(cd->pos);
				if (found != end() && found->second == tempChunk) { // did we find the chunk, and does it exist?
					found->second = cd->ch;

					// Create the meshes on the GLThread (the main thread) instead of the generation threads
					cd->ch->sendMeshToGPU();

					// TODO: at some point, make this happen
					// for(int i = 0; i < 3; i++)
					//	cd->ch->resolveLighting(i);

					// cd->ch->mesh->updateLightBuffer();
					cd->ch->mesh->buf->streamLightData();
					for (Flora * f: cd->ch->flora)
						f->sendMeshToGPU();
				} else delete cd->ch;

				loaders[i].destroyChunkData(cd);
			}
		}
	}


	float ChunkMap::getTerrainHeightAt(glm::vec2 pos) {
		return floor(gen->terrainHeight->noise2D(pos));
	}

	struct ls {
		bool operator ()(const Chunk* a, const Chunk* b) {
			float adist = squareDistance(chunkToWorldCoordinate(a->position), mPlayer->cam.pos);
			float bdist = squareDistance(chunkToWorldCoordinate(b->position), mPlayer->cam.pos);
			return adist < bdist;
		}
	};

	void ChunkMap::prepareForRender() {
		_depthSort.prepareForRender();
	}

	void ChunkMap::renderChunks(ChunkMeshRenderer* render) {
		for(auto ch: _depthSort.depthSortedChunks)
			renderMesh<ChunkMeshRenderer, ChunkMeshBuffer>(render, ch->mesh->buf,
					glm::vec3(ch->position.x, ch->position.y, ch->position.z) * (float) Chunk::SIZE, WorldCoordinate(Chunk::SIZE)/(float)ChunkMesh::GRIDVOXEL_SIZE);
	}

	void ChunkMap::renderChunkVolumes(VolumeRenderer* render) {
		for(auto it = begin(); it != end(); it++) {
			Chunk* ch = it->second;
			if(isLoadedYet(ch))
				render->render(VolumeDisplayObject(&(ch->volume), Color::HighpColor(0,0,0,1)));
		}
	}

	void ChunkMap::renderEntities(EntityMeshRenderer* render) {
		/*for(auto it = begin(); it != end(); it++) {
			Chunk* ch = it->second;
			if (isLoadedYet(ch)) {
				for (Flora * f: ch->flora) {
					//f->render(p, cam, this);
				}
			}
		}*/
	}

	bool Chunk::isVoxelAt(const LocalCoordinate& postition) {
		return !(get(position).is(Voxel::IMAGINARY));
	}

	bool isValidRaycast(const WorldCoordinate& pos, const WorldCoordinate& dir, float rad) {
		if (glm::length(dir) == 0) return false;
		return true;
	}

	VoxelRaycastReport ChunkMap::getRaycast(const WorldCoordinate& pos, const WorldCoordinate& dir, float rad) {
		if (!isValidRaycast(pos, dir, rad)) return {0, glm::vec3(), glm::vec3(), false};

		WorldCoordinate normDir = glm::normalize(dir);
		WorldCoordinate sign = glm::sign(normDir);
		WorldCoordinate biSign = glm::floor((sign + WorldCoordinate(1)) / 2.f); // Sign used for rounding. Components are 0 if - or 1 if +

		float dist = 0.f;
		WorldCoordinate curPos = pos;
		while (dist <= rad) {
			WorldCoordinate prox = WorldCoordinate(worldToVoxelCoordinate(curPos + biSign)) - curPos; // Proximity to the voxel boundary
			WorldCoordinate params = glm::abs(prox / vecReplace(normDir, 0, 0.001f)); // Replace 0 to account for DBZ errors
			float t = vecMin(params); // Minimized distance (find the minimum parameterization to a voxel boundary)
			dist += t;
			curPos = curPos + normDir * t + sign * 0.001f; // Overshoot it by just a bit to account for float errors
			VoxelCoordinate curVox = worldToVoxelCoordinate(curPos);

			if (getVoxel(curVox).is(Voxel::SOLID)) {
				// We found it, boys!
				return {
						dist, // Distance traveled
						curPos, // End point
						(vecReplaceExclusive(params, t, 0) / t) * -sign,
						true
				};
			}
		}

		return {dist, curPos, glm::vec3(), false};
	}

	void Chunk::sendMeshToGPU() {
		mesh->buf->sendToGPU();
	}

	void ChunkMap::createChunk(ChunkCoordinate& position) {
		if (getChunk(position)) return;

		setChunk(position,tempChunk);

		chunkStructuresToLoad.push_back(position);
		structures->queueStructureUnitAndNeighbors(position);
	}

	void ChunkMap::removeLight(VoxelCoordinate pos) {
		setLightForRemoval(pos);
		setLight(pos, LightLevel({0,0,0}));
	}

	void ChunkMap::setLightForAddition(VoxelCoordinate pos, LightLevel lev) {
		ChunkCoordinate cpos = voxelToChunkCoordinate(pos);
		for(int i = 0; i < 3; i++)
			getChunk(cpos)->lightsToAdd[i].emplace(glm::i8vec3(voxelToLocalChunkCoordinate(pos)), lev._level[i]);
	}

	void ChunkMap::setLightForRemoval(VoxelCoordinate pos) {
		ChunkCoordinate cpos = voxelToChunkCoordinate(pos);
		for(int i = 0; i < 3; i++)
			getChunk(cpos)->lightsToRemove[i].emplace(glm::i8vec3(voxelToLocalChunkCoordinate(pos)), getLightLevel(pos)._level[i]);
	}

	std::vector<CPUGridVoxelCollisionMeshUnit*> ChunkMap::getCollisionUnits(VoxelCoordinate start, VoxelCoordinate size) {
		std::vector<CPUGridVoxelCollisionMeshUnit*> ret;

		ChunkCoordinate s = voxelToChunkCoordinate(start);
		ChunkCoordinate e = voxelToChunkCoordinate(start+size);
		ChunkCoordinate diff = e-s;

		ChunkCoordinate poss;

		LocalCoordinate localPosStart = voxelToLocalChunkCoordinate(start);

		for(poss.z = 0; poss.z <= diff.z; ++poss.z)
		for(poss.y = 0; poss.y <= diff.y; ++poss.y)
		for(poss.x = 0; poss.x <= diff.x; ++poss.x) {
			ChunkCoordinate p2 = poss+s;
			Chunk* thisChunk = getChunk(p2);

			if(!isLoadedYet(thisChunk)) continue;

			VoxelCoordinate cStart = chunkToVoxelCoordinate(p2);
			LocalCoordinate oneFirstMate = voxelToLocalChunkCoordinate(start);
			LocalCoordinate inChunk = oneFirstMate - oneFirstMate * glm::min(poss,LocalCoordinate(1));

			LocalCoordinate sizeLeft = size - (chunkToVoxelCoordinate(poss) - localPosStart * glm::min(poss,LocalCoordinate(1)));

			std::vector<CPUGridVoxelCollisionMeshUnit*> thisUnit = thisChunk->mesh->buf->gridVoxelCollisionMesh->getUnits(inChunk,sizeLeft);

			ret.insert(ret.end(), thisUnit.begin(), thisUnit.end());
		}

		return ret;
	}

	bool Chunk::contains(const VoxelCoordinate& v) {
		return voxelToChunkCoordinate(v) == v;
	}

	VoxelCoordinate containsOffsets[27] = {
			{-1, -1, -1}, {-1, -1, 0}, {-1, -1, 1}, {-1, 0, -1}, {-1, 0, 0}, {-1, 0, 1}, {-1, 1, -1}, {-1, 1, 0}, {-1, 1, 1},
			{0, -1, -1},  {0, -1, 0},  {0, -1, 1},  {0, 0, -1},   {0, 0, 1},  {0, 1, -1},  {0, 1, 0},  {0, 1, 1},
			{1, -1, -1},  {1, -1, 0},  {1, -1, 1},  {1, 0, -1},  {1, 0, 0},  {1, 0, 1},  {1, 1, -1},  {1, 1, 0},  {1, 1, 1}
	};

	bool Chunk::containsInPaddedRange(const VoxelCoordinate& v) {
		bool contain = false;
		for(int i = 0; i < 27; i++) {
			contain |= contains(v + containsOffsets[i]); // try moving v every which way :3
		}
		return contain;
	}

	void ChunkMap::setLight(VoxelCoordinate pos, LightLevel value) {
		setLightForRemoval(pos);
		setLightForAddition(pos, value);
		ChunkCoordinate cpos = voxelToChunkCoordinate(pos);
		LocalCoordinate clocal = voxelToLocalChunkCoordinate(pos);

		int TEMP = 1;
		for(int x = -TEMP; x <= TEMP; x++) for(int y = -TEMP; y <= TEMP; y++) for(int z = -TEMP; z <= TEMP; z++) {
			ChunkCoordinate nCPos = cpos + ChunkCoordinate(x,y,z);
			Chunk* ch = getChunk(nCPos);
			if(isLoadedYet(ch)) {
				LocalCoordinate nClocal = clocal - LocalCoordinate(x,y,z) * Chunk::SIZE;
				ch->mesh->set(value, nClocal);
			}
		}
	}

	LightLevel ChunkMap::getLightLevel(VoxelCoordinate pos) {
		VoxelID id = getVoxelID(pos);
		return getLightLevelfromVoxelAndID(id, voxelMap->get(id));
	}

	void Chunk::setVoxel(LocalCoordinate position, const VoxelID val) {
		mesh->set(val, position);
	}

	Voxel Chunk::get(LocalCoordinate position) {
		return mesh->get(position);
	}

	VoxelID ChunkMap::getVoxelID(VoxelCoordinate& pos) {
		ChunkCoordinate cpos = voxelToChunkCoordinate(pos);
		Chunk * c = getChunk(cpos);
		if (isLoadedYet(c) && c->mesh) {
			LocalCoordinate clocal = voxelToLocalChunkCoordinate(pos);
			return c->getVoxelID(clocal);
		}
		else
			return 0; // pffft... whatever
	}

	VoxelID Chunk::getVoxelID(LocalCoordinate& pos) {
		return mesh->getVoxelID(pos);
	}

	const glm::i8vec3 propagationDirections[] = {
			{0,0,1}, {0,0,-1}, // North,South
			{1,0,0}, {-1,0,0}, // East , West
			{0,1,0}, {0,-1,0},  // Up   , Down
	};

	const std::vector<ChunkCoordinate> offs[27] = {
			{ {0, 0, -1}, {0, -1, 0}, {0, -1, -1}, {-1, 0, 0}, {-1, 0, -1}, {-1, -1, 0}, {-1, -1, -1} },
			{ {0, -1, 0}, {-1, 0, 0}, {-1, -1, 0} },
			{ {0, 0, 1}, {0, -1, 0}, {0, -1, 1}, {-1, 0, 0}, {-1, 0, 1}, {-1, -1, 0}, {-1, -1, 1} },
			{ {0, 0, -1}, {-1, 0, 0}, {-1, 0, -1} },
			{ {-1, 0, 0} },
			{ {0, 0, 1}, {-1, 0, 0}, {-1, 0, 1} },
			{ {0, 0, -1}, {0, 1, 0}, {0, 1, -1}, {-1, 0, 0}, {-1, 0, -1}, {-1, 1, 0}, {-1, 1, -1} },
			{ {0, 1, 0}, {-1, 0, 0}, {-1, 1, 0} },
			{ {0, 0, 1}, {0, 1, 0}, {0, 1, 1}, {-1, 0, 0}, {-1, 0, 1}, {-1, 1, 0}, {-1, 1, 1} },
			{ {0, 0, -1}, {0, -1, 0}, {0, -1, -1} },
			{ {0, -1, 0} },
			{ {0, 0, 1}, {0, -1, 0}, {0, -1, 1} },
			{ {0, 0, -1} },
			{},
			{ {0, 0, 1} },
			{ {0, 0, -1}, {0, 1, 0}, {0, 1, -1} },
			{ {0, 1, 0} },
			{ {0, 0, 1}, {0, 1, 0}, {0, 1, 1} },
			{ {0, 0, -1}, {0, -1, 0}, {0, -1, -1}, {1, 0, 0}, {1, 0, -1}, {1, -1, 0}, {1, -1, -1} },
			{ {0, -1, 0}, {1, 0, 0}, {1, -1, 0} },
			{ {0, 0, 1}, {0, -1, 0}, {0, -1, 1}, {1, 0, 0}, {1, 0, 1}, {1, -1, 0}, {1, -1, 1} },
			{ {0, 0, -1}, {1, 0, 0}, {1, 0, -1} },
			{ {1, 0, 0} },
			{ {0, 0, 1}, {1, 0, 0}, {1, 0, 1} },
			{ {0, 0, -1}, {0, 1, 0}, {0, 1, -1}, {1, 0, 0}, {1, 0, -1}, {1, 1, 0}, {1, 1, -1} },
			{ {0, 1, 0}, {1, 0, 0}, {1, 1, 0} },
			{ {0, 0, 1}, {0, 1, 0}, {0, 1, 1}, {1, 0, 0}, {1, 0, 1}, {1, 1, 0}, {1, 1, 1} }
	};

	std::vector<Chunk*> Chunk::getChunksThatContainPoint(glm::i8vec3 point) {
		int_fast8_t t =
			(-1*(int_fast8_t)(point.z <= 0) + 1*(int_fast8_t)(point.z >= SIZE - 1)) + 3*(
			 -1*(int_fast8_t)(point.y <= 0) + 1*(int_fast8_t)(point.y >= SIZE - 1) + 3*(
			 -1*(int_fast8_t)(point.x <= 0) + 1*(int_fast8_t)(point.x >= SIZE - 1)
			)
			) + 13;
		const std::vector<ChunkCoordinate>& off = offs[t];
		std::vector<Chunk*> ret(off.size() + 1);
		ret[0] = this;
		for(unsigned int i = 0; i < off.size(); i++) {
			ChunkCoordinate cpos = position + off[i];
			ret[i + 1] = parent->getChunk(cpos);
		}
		return ret;
	}

	Chunk* Chunk::getChunkThatPointBelongsTo(glm::i8vec3 point) {
		ChunkCoordinate cpos = {
			-1*(int_fast8_t)(point.x == -1) + 1*(int_fast8_t)(point.x == SIZE),
			-1*(int_fast8_t)(point.y == -1) + 1*(int_fast8_t)(point.y == SIZE),
			-1*(int_fast8_t)(point.z == -1) + 1*(int_fast8_t)(point.z == SIZE)
		};
		if(cpos == ChunkCoordinate(0,0,0)) return this;
		ChunkCoordinate c2 = position + cpos;
		return parent->getChunk(c2);
	}

	void Chunk::resolveLighting(int i) {
		while(!lightsToRemove[i].empty()) {
			QueuedLightRemoval lite = lightsToRemove[i].front();
			lightsToRemove[i].pop();
			updateLighting = true;

			for(unsigned int j = 0; j < 6; j++) {
				glm::i8vec3 vCoord = lite.pos + propagationDirections[j];
				VoxelID vID = mesh->fastGetVoxelID(vCoord);

				Chunk* host = getChunkThatPointBelongsTo(vCoord);
				if(!parent->isLoadedYet(host)) continue;
				std::vector<Chunk*> chs = getChunksThatContainPoint(vCoord);
				glm::i8vec3 coordInChunk = vCoord - glm::i8vec3(SIZE * (host->position - position));


				Voxel v = mesh->voxelMap->get(vID);
				if ((v.is(Voxel::IMAGINARY))) {
					LightLevel atPos = getLightLevelfromVoxelAndID(vID, v);
					if(atPos._level[i] > 0 && atPos._level[i] < lite.oldIntensity) {
						if(atPos._level[i] != 0) { // it's already 0, who cares

							host->lightsToRemove[i].emplace(coordInChunk, atPos._level[i]);

							atPos._level[i] = 0;
							for(unsigned int i = 0; i < chs.size(); i++) {
								if(parent->isLoadedYet(chs[i])) {
									glm::i8vec3 coordInChunk2 = vCoord - glm::i8vec3(SIZE * (chs[i]->position - position));
									chs[i]->setLightLevel(coordInChunk2, atPos);
								}
							}
						}
					} else if (atPos._level[i] >= lite.oldIntensity) {
						host->lightsToAdd[i].emplace(coordInChunk, atPos._level[i]);
					}
				} else if((v.is(Voxel::LIGHT_SOURCE))) {
					host->lightsToAdd[i].emplace(coordInChunk, v.light._level[i]);
				}
			}
		}

		while(!lightsToAdd[i].empty()) {
			QueuedLight lite = lightsToAdd[i].front();
			lightsToAdd[i].pop();

			for(unsigned int j = 0; j < 6; j++) {
				glm::i8vec3 vCoord = lite.pos + propagationDirections[j];
				VoxelID vID = mesh->fastGetVoxelID(vCoord);

				Chunk* host = getChunkThatPointBelongsTo(vCoord);
				if(!parent->isLoadedYet(host)) continue;
				std::vector<Chunk*> chs = getChunksThatContainPoint(vCoord);
				glm::i8vec3 coordInChunk = vCoord - glm::i8vec3(SIZE * (host->position - position));

				if ((mesh->voxelMap->get(vID).is(Voxel::IMAGINARY))) {
					LightLevel atPos = getLightLevelfromVoxelAndID(vID, mesh->voxelMap->get(vID));
					if(atPos._level[i] < lite.intensity - 1) {

						atPos._level[i] = lite.intensity - 1;
						for(unsigned int i = 0; i < chs.size(); i++) {
							if(parent->isLoadedYet(chs[i])) {
								glm::i8vec3 coordInChunk2 = vCoord - glm::i8vec3(SIZE * (chs[i]->position - position));
								chs[i]->setLightLevel(coordInChunk2, atPos);
							}
						}

						// Emplace new node to queue. (could use push as well)
						host->lightsToAdd[i].emplace(coordInChunk, atPos._level[i]);
					}
				}
			}
		}
	}

	// ChunkLoader
	ChunkLoader::~ChunkLoader() {
		if (_running) stop();
		while (!_chunksFinished.empty())
			delete _popFinished();
		while (!_chunksCreation.empty())
			delete _popCreation();
	}

	void ChunkLoader::start(ChunkGenerator * gen, ChunkMap * parent, bool sort) {
		_chunkCreationLock.init();
		_chunkFinishedLock.init();
		_dataAccessLock.init();

		_sort = sort;
		_gen = gen;
		_parent = parent;
		_running = true;
		_thread = new SDLThread<ChunkLoader>(ChunkLoader::_chunkCreationLoop, "Chunk Loader", this);
		_thread->start();
	}
	void ChunkLoader::stop() {
		_chunkCreationLock.unlock();
		_chunkFinishedLock.unlock();
		_dataAccessLock.unlock();

		_running = false;
		_thread->join();
		delete _thread;
	}

	void ChunkLoader::loadChunk(ChunkCoordinate& pos) {
		ChunkData * cd = new ChunkData();
		cd->pos = pos;
		cd->structureUnit = _parent->structures->getStructureUnit(pos);
		_pushCreation(cd);
	}

	void ChunkLoader::destroyChunkData(ChunkData * cd) {
		delete cd;
	}

	ChunkData * ChunkLoader::next() {
		return _popFinished();
	}
	bool ChunkLoader::hasNext() {
		return !_chunksFinished.empty();
	}

	void ChunkLoader::startBatch() {
		_chunkCreationLock.lock();
	}
	void ChunkLoader::endBatch() {
		_chunkCreationLock.unlock();
	}
	void ChunkLoader::batchLoad(ChunkCoordinate& pos) {
		ChunkData * cd = new ChunkData();
		cd->pos = pos;
		_chunksCreation.push_back(cd);
	}

	ChunkData * ChunkLoader::_popFinished() {
		_chunkFinishedLock.lock();
		ChunkData * ret = _chunksFinished.top();
		_chunksFinished.pop();
		_chunkFinishedLock.unlock();
		return ret;
	}
	ChunkData * ChunkLoader::_popCreation() {
		_chunkCreationLock.lock();
		ChunkData * ret = _chunksCreation.front();
		_chunksCreation.erase(_chunksCreation.begin());
		_chunkCreationLock.unlock();
		return ret;
	}
	void ChunkLoader::_pushFinished(ChunkData * cd) {
		_chunkFinishedLock.lock();
		_chunksFinished.push(cd);
		_chunkFinishedLock.unlock();
	}
	void ChunkLoader::_pushCreation(ChunkData * cd) {
		_chunkCreationLock.lock();
		_chunksCreation.push_back(cd);
		_chunkCreationLock.unlock();
	}

	void ChunkLoader::_sortCreation() {
		if (!_sort) return;

		_dataAccessLock.lock();
		ChunkSorter c = _lastSorter;
		_dataAccessLock.unlock();

		_chunkCreationLock.lock();
		std::stable_sort(_chunksCreation.begin(), _chunksCreation.end(), c);
		_chunkCreationLock.unlock();
	}

	int ChunkLoader::_chunkCreationLoop(void * data) {
		ChunkLoader * loader = (ChunkLoader*) data;

		// We have to use mGameLoop's lower precision lasttime because SDL_GetTicks
		// may or not be thread safe depending on the operating system
		long lastTime = mGameLoop.getTimeInMillis();

		bool throttle = true;

		while (loader->_running) {
			loader->_chunkCreationLock.lock();
			bool var = !loader->_chunksCreation.empty();
			loader->_chunkCreationLock.unlock();
			if (var && (throttle ? mGameLoop.getTimeInMillis() - lastTime > 0 : true)) {

				throttle = false;
				lastTime = mGameLoop.getTimeInMillis();

				loader->_sortCreation();

				ChunkData * cd = loader->_popCreation();

				if (!loader->_parent->getChunk(cd->pos)) {
					loader->destroyChunkData(cd);
					continue;
				}
				cd->ch = loader->_gen->generateChunk(cd->pos, loader->_parent->voxelMap, cd->structureUnit);


				loader->_pushFinished(cd);
			} else mSDL->sleep(500);
		}

		return 0;
	}

	void ChunkLoader::updateChunkSorter(const ChunkSorter & c) {
		_dataAccessLock.lock();
		_lastSorter = c;
		_dataAccessLock.unlock();
	}

	int req_rad = 3;

	bool ChunkSorter::operator()(ChunkData * a, ChunkData * b) {
		return (manhattanDistance(chunkToWorldCoordinate(a->pos), playerPos) + ((float)(!playerCam.containsSphere(chunkToWorldCoordinate(a->pos) + Chunk::SIZE / 2.f, (req_rad + .5f) * Chunk::SIZE)) * 65536))
				< (manhattanDistance(chunkToWorldCoordinate(b->pos), playerPos) + ((float)(!playerCam.containsSphere(chunkToWorldCoordinate(b->pos) + Chunk::SIZE / 2.f, (req_rad + .5f) * Chunk::SIZE)) * 65536));
	}











	size_t ChunkHash::operator ()(const ChunkCoordinate& in) const {
		uint64_t key = ((uint64_t) in.x) + (((uint64_t) in.y) << 24) + ((uint64_t) in.z << 32);
		// Take the nearby points and make them far apart
		key = (~key) + (key << 18);
		key = key ^ (key >> 31);
		key = key * 21;
		key = key ^ (key >> 11);
		key = key + (key << 6);
		key = key ^ (key >> 22);
		// Scramble it up some more, weed out the collisions with some magic numbers
		key *= 		  in.x * 1327144003;
		key ^= ~key * in.y * 486187739;
		key ^= ~key * in.z * 1610612741;
		return key;
	}

	ChunkHash chhash;
	size_t LoDHash::operator ()(const std::pair<ChunkCoordinate, int>& in) const {
		return chhash(in.first) + in.second; // laziness
	}



	void ChunkDepthSorter::_sort() {

		depthSortedChunks.clear();

		for(auto it = parent->begin(); it != parent->end(); it++) {
			Chunk* ch = it->second;
			if(parent->isLoadedYet(ch)) {

				WorldCoordinate center = chunkToWorldCoordinate(ch->position) + glm::vec3(Chunk::SIZE/2.f);
				if (
						ch->mesh->buf->isEmpty() &&
						mPlayer->cam.containsSphere(center, rad(glm::vec3(Chunk::SIZE))) &&
						inRenderRadius(mPlayer->player->bb.pos, chunkToWorldCoordinate(ch->position), CHUNK_RENDER_RADIUS)
				) {
					depthSortedChunks.push_back(ch);
				}

			}
		}

		ls sorter;
		std::stable_sort(depthSortedChunks.begin(), depthSortedChunks.end(), sorter);

	}

	void ChunkDepthSorter::prepareForRender() {
		_sort();
	}

}




