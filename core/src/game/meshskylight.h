/*
 * meshskylight.h
 *
 *  Created on: Jul 2, 2016
 *      Author: dylan
 */

#ifndef SRC_GAME_MESHSKYLIGHT_H_
#define SRC_GAME_MESHSKYLIGHT_H_

#include "vglm.h"

namespace vox {

	class MeshSkyLighter {
	public:
		glm::vec3 meshNormals[6] = {
				{0,0,1}, {0,0,-1}, // North,South
				{1,0,0}, {-1,0,0}, // East , West
				{0,1,0}, {0,-1,0}  // Up   , Down
		};

		SunLight sun;

		template<typename Program_T> void lightMesh(glm::mat4 model, Program_T* program) {

			glm::vec3 array[6];

			glm::vec3 invDir = glm::normalize(sun.invDirection);

			for(int i = 0; i < 6; i++) {
				glm::vec4 worldNormal = (glm::transpose(glm::inverse(model)) * glm::vec4(meshNormals[i],0));
				glm::vec3 N(worldNormal.x,worldNormal.y,worldNormal.z);
				N = glm::normalize(N);

				float cosTheta = glm::dot( N,invDir ) * 0.5f + 0.5f;
				cosTheta = pow(cosTheta, 2);
				glm::vec3 value = sun.color * sun.power * glm::mix(sun.compensation, 1.f, cosTheta);
				array[i] = value;
			}

			glUniform3fv(program->normalLighting, 6, &array[0][0]);

		}
	};

}


#endif /* SRC_GAME_MESHSKYLIGHT_H_ */
