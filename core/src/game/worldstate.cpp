/*
 * worldstate.cpp
 *
 *  Created on: Mar 22, 2015
 *      Author: Dani
 */

#include "worldstate.h"
#include "chunk.h"
#include "settingsmanager.h"
#include "resourcemanager.h"
#include "worldstaterender.h"
#include "entitymanager.h"
#include "framebuffer.h"

namespace vox {

	WorldState* mWorldState;

	WorldState::WorldState() {
		renderHandler = new WorldStateRenderHandler(this);
		entityManager = new EntityManager();
		entityManager->init(this);

		mSDL->setMouseGrabbed(true);

		map = new ChunkMap();
		map->init(this);

		mWorldState = this;

		println(DEBUG, "WorldState Created!");
	}

	WorldState::~WorldState() {
		delete map;
		delete entityManager;
	}

	void WorldState::render(FrameBuffer* fbo) {
		renderHandler->render(fbo);
	}

	void WorldState::tick(float delta) {
		entityManager->tick(delta);
		renderHandler->tick(delta);

		map->loadInRadius(worldToChunkCoordinate(mPlayer->cam.lookAt), {6,2,6});
		map->tick(delta);
		//map->loadLoDRadius(worldToChunkCoordinate(playerEntity->getPos()));
	}

}
