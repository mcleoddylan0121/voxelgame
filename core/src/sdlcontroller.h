/*
 * sdlcontroller.h
 *
 *  Created on: Mar 22, 2015
 *      Author: Dylan
 */

#ifndef SDLCONTROLLER_H_
#define SDLCONTROLLER_H_

#include "console.h"

#ifdef __custom__
#include <SDL.h>
#else
#include <SDL2/SDL.h>
#endif

#include <map>
#include <string>
#include <chrono>


namespace vox {
	struct Asset;

	struct Mouse {
		int x, y, dx, dy,
			sdx, sdy; // Scroll Wheel
	};

	// Time units
	typedef std::chrono::hours hours;
	typedef std::chrono::minutes minutes;
	typedef std::chrono::seconds seconds;
	typedef std::chrono::milliseconds milliseconds;
	typedef std::chrono::microseconds microseconds;
	typedef std::chrono::nanoseconds nanoseconds;

	class SDLController;
	extern SDLController * mSDL;

	class SDLController {
	private:
		typedef std::chrono::steady_clock ClockType;

		// Note, SDL uses the masks 0 and 1<<30. Be careful 1<<29 doesn't collide with anything.
		int MOUSE_BUTTON_MASK = 1<<29;

		SDL_Window* window;
		SDL_GLContext glContext;
		std::map<int,bool> keys, lastKeys; // Why is this a map?
		bool mouseButtons[256], lastMouseButtons[256]; // a num from 1 to 255
		bool mouseGrabbed;
		bool sdlReady = false;
		Mouse mouse;
		ClockType::time_point _c;

		// lol I stole this from STL
		template<typename _Tp> struct __is_duration : std::false_type {};
		template<typename _Rep, typename _Period> struct __is_duration<std::chrono::duration<_Rep, _Period>> : std::true_type {};

		// These are internal functions. Use inputPressed and Typed instead.
		bool _keyPressed(int);
		bool _keyTyped(int);
		bool _mouseButtonPressed(unsigned char);
		bool _mouseButtonTyped(unsigned char);

	public:
		int windowWidth, windowHeight; // The actual width and height of the window
		int viewportWidth, viewportHeight; // The window's screen resolution


		SDLController();
		virtual ~SDLController();

		/*  If fullscreen is set to true, then windowWidth and windowHeight are overridden */
		void makeWindow(int viewportWidth, int viewportHeight, int windowWidth, int windowHeight, bool fullScreen);

		void clean(); // Cleans all input

		template <typename T>
		typename std::enable_if<__is_duration<T>::value, typename T::rep>::type getTime(ClockType::time_point startPoint = mSDL->_c) {
			return std::chrono::duration_cast<T>(ClockType::now() - startPoint).count();
		}

		ClockType::time_point getTimePoint();

		void sleep(long ms);

		void swapBuffer();

		bool handleEvents();
		bool inputPressed(std::string name);
		bool inputTyped(std::string name);

		void setMouseGrabbed(bool grabbed);
		void setTextMode(bool on);	// Setting this on will trigger SDL_TEXTEVENTs. Use this for text boxes.
		void setVSync(bool on);
		void setFullscreen(bool b);
		void setResolution(int w, int h);
		void setIcon(const Asset& asset);

		bool isMouseGrabbed();

		void setTitle(const std::string& title);

		void showErrorMessageBox(std::string title, std::string message);

		const Mouse& getMouse();
		void updateUtilities(); // Updates the mouse and keyboard configuration, use once a tick

		bool isSDLInitialized();
		bool isWindowMade();

		bool handleError(const char * func, int line);
	};

	template <typename data_t>
	class SDLThread {
	private:
		SDL_Thread * _thread;
		std::string _name;
		int (*_function)(void *);
		bool _running = false;
		data_t * _data;

	public:
		SDLThread(int (*funcs)(void *), std::string name, data_t * data) {
			_thread = 0;
			_function = funcs;
			_name = name;
			_data = data;
		}
		virtual ~SDLThread() {
			if (isRunning())
				join();
		}

		int start() {
			if (mSDL->isSDLInitialized()) {
				_thread = SDL_CreateThread(_function, _name.c_str(), (void*) _data);
				_running = true;
			} else
				printf(SDLERROR, "Could not start thread \"%s\": SDL hasn't been initialized.\n", _name.c_str());
			return -1;
		}
		int join() {
			if (mSDL->isSDLInitialized())
				if (isRunning()) {
					_running = false;
					int ret;
					SDL_WaitThread(_thread, &ret);
					return ret;
				} else printf(SDLERROR, "Could not join thread \"%s\": Thread isn't running.\n", _name.c_str());
			else printf(SDLERROR, "Could not join thread \"%s\": SDL hasn't been initialized.\n", _name.c_str());
			return -1;
		}

		bool isRunning() { return _running; }
		data_t * getData() { return _data; }
	};

	class Mutex {
	private:
		SDL_mutex * _mutex;

	public:
		virtual ~Mutex();

		int init();

		int lock();
		int unlock();
	};

} /* namespace vox */

#endif /* SDLCONTROLLER_H_ */
