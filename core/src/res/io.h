/*
 * io.h
 *
 *  Created on: Mar 23, 2015
 *      Author: Dylan
 */

#ifndef IO_H_
#define IO_H_

#include "glcontroller.h"

#include <string>

#ifdef __custom__
#include <SOIL.h>
#else
#include <SOIL/SOIL.h>
#endif

struct SDL_Surface;

namespace vox {
	struct Texture;
	struct VoxelMesh;
	struct EntityMesh;
	struct Asset;

	// This only checks if the Asset exists, not the type
	std::string loadTextFile(const Asset& asset);
	std::string loadTextFile(std::string path); // For use without the asset system

	// Asset must be of type Texture (is this used anywhere?)
	Texture* loadTexture(const Asset& asset);
	CubeMap* loadCubeMap(const Asset& a);

	// Note: Use this only for SDL-specific stuff like the window icon.
	SDL_Surface * loadSurface(const Asset& asset);

	// The assets must be vertex/frag shaders respectively
	GLuint loadShaders(const Asset& vertexShader, const Asset& fragmentShader, const Asset& geometryShader,
			std::vector<std::string> fragShaderOutputs, std::vector<std::string> vAttribs,
			std::vector<std::string> macros, std::vector<Asset> libs, std::vector<std::string> inject);

	// The asset must be of type Mesh
	EntityMesh* loadEntityVox(const Asset& asset);

	void saveImage(void* pixels, int width, int height, std::string path);
	void saveTextFile(std::string path, std::string file);
}



#endif /* IO_H_ */
