/*
 * iolua.h
 *
 *  Created on: Aug 3, 2015
 *      Author: Daniel
 */

#ifndef IOLUA_H_
#define IOLUA_H_

#include "vlua.h"
#include "renderutils.h"

#include <string>

namespace vox {

	struct TexParams : public LuaObject {
		int TEXTURE_MAG_FILTER = GL_NEAREST,
			TEXTURE_MIN_FILTER = GL_NEAREST,
			TEXTURE_WRAP_S = GL_REPEAT,
			TEXTURE_WRAP_T = GL_REPEAT,
			TEXTURE_WRAP_R = GL_REPEAT;
		bool GEN_MIPMAPS = false;

		TexParams(std::string name);
	};

	// Gets the path to a lua file. Specify only the name without extensions.
	std::string getLuaPath(std::string moduleName, std::string name);
}

#endif /* IOLUA_H_ */
