/*
 * resourcemanager.h
 *
 *  Created on: May 23, 2015
 *      Author: Dani
 */

#ifndef RESOURCEMANAGER_H_
#define RESOURCEMANAGER_H_

#include "filesystem.h"
#include "console.h"

#include <string>
#include <unordered_map>
#include <tuple>

namespace vox {

	// TODO:
	//    - Asynchronous allocation

	template<typename T>
	struct isAllocatable {
	private:
		typedef char _true[1];
		typedef char _false[2];

		template<typename C> static _true&	_test(decltype(&C::allocate));
		template<typename C> static _false&	_test(...);

	public:
		enum { value = sizeof(_test<T>(0)) == sizeof(_true) };
	};

	struct ResourceManager;
	extern ResourceManager * mRes;

	class ResourceManager {
	public:

		// If the asset type is specified, it will load the asset if it isn't already
		template<typename T, Asset::Type A>
		T* getResource(const std::string& name) {
			static_assert(isAllocatable<T>::value, "Tried to get an unallocatable resource. Define the allocate function.");

			if (_isAllocated<T, A>(name))
				return std::get<0>(_getAllocator<T, A>()[name]);
			else if (A != Asset::Unspecified)
				return _allocate<T, A>(mFileManager->fetchAsset(name, A));

			printlnf(ERROR, "Tried to get a resource not yet allocated: %s", name.c_str());
			return 0;
		}

		template<typename T, Asset::Type A>
		bool isLoaded(const std::string& name) {
			return _isAllocated<T, A>(name);
		}

		template<typename T, Asset::Type A>
		void deleteResource(const std::string& name) {
			_deallocate<T, A>(name);
		}

		template<typename T, Asset::Type A>
		void reloadAll() {
			std::unordered_map<std::string, std::tuple<T*, Asset>> tempMap = _getAllocator<T, A>();
			for (auto i = tempMap.begin(); i != tempMap.end(); i++) {
				_deallocate<T, A>(i->first);
				_allocate<T, A>(std::get<1>(i->second));
			}
		}

	private:
		template<typename T, Asset::Type A>
		struct _resourceAllocator {
			static std::unordered_map<std::string, std::tuple<T*, Asset>> data;
		};

		template<typename T, Asset::Type A>
		std::unordered_map<std::string, std::tuple<T*, Asset>>& _getAllocator() { return _resourceAllocator<T, A>::data; }

		template<typename T, Asset::Type A>
		T* _allocate(const Asset& asset) {
			T* alloc = T::allocate(asset);
			_getAllocator<T, A>()[asset.name] = std::make_tuple(alloc, asset);
			return alloc;
		}

		template<typename T, Asset::Type A>
		void _deallocate(const std::string& name) { delete std::get<0>(_getAllocator<T, A>()[name]); _getAllocator<T, A>().erase(name); }

		template<typename T, Asset::Type A>
		bool _isAllocated(const std::string& name) {
			return _getAllocator<T, A>().count(name) > 0;
		}
	};

	template<typename T, Asset::Type A>
	struct AllocatorTemplate {
		static inline T* get(const std::string& name) {
			return mRes->getResource<T, A>(name);
		}
	};

	template<typename T, Asset::Type A>
	typename std::unordered_map<std::string, std::tuple<T*, Asset>> ResourceManager::_resourceAllocator<T, A>::data;
}

#endif /* RESOURCEMANAGER_H_ */
