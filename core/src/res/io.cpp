/*
 * io.cpp
 *
 *  Created on: Mar 23, 2015
 *      Author: Dylan
 */

#include "io.h"
#include "console.h"
#include "util.h"
#include "geometry.h"
#include "filesystem.h"
#include "voxelmesh.h"
#include "iolua.h"
#include "sdlcontroller.h"

#include "glcontroller.h"
#include "glanalytics.h"

#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <cmath>

namespace vox {
	std::string loadTextFile(const Asset& asset) {
		if (asset.exists)
			return loadTextFile(asset.path);
		else return  "";
	}
	std::string loadTextFile(std::string path) {
		std::ifstream fin;							// Create a file in stream
		fin.open(path);							 	// Open the file

		if (!fin.is_open()) { 						// If there was an error opening the file
			vox::printf(IOERROR, "Cannot read file, \"%s\"", path.c_str());
			return "";
		}

		std::string temp;							// Store the data in here
		std::string line;							// Temporary variable to store lines

		while (!fin.eof()) {						// While we haven't reached the end of the file
			std::getline(fin, line);				// Get the next line in the file and store it in line
			temp.append(line + "\n");				// Append line to our data
		}

		fin.close();					// Close the stream so the file can be opened by other programs

		return temp;
	}

	void saveTextFile(std::string path, std::string text) {
		std::ofstream ofs;
		ofs.open(path);

		if (!ofs.is_open()) {
			printf(IOERROR, "Cannot save file, \"%s\"");
			return;
		}

		ofs << text;
		ofs.close();
	}

	EntityMesh * loadEntityVox(const Asset& asset) {
		if (!mFileManager->verifyAsset(asset, Asset::Mesh)) return 0;

		std::ifstream ifs(asset.path.c_str(), std::ios::binary|std::ios::ate);
		std::ifstream::pos_type pos = ifs.tellg();

		std::vector<char> data(pos);

		ifs.seekg(0, std::ios::beg);
		ifs.read(&data[0], pos);

		const int packetSize = 4;

		// Make sure this is the right file format
		const char * str = "VOX ";
		if (memcmp(&data[0], str, packetSize * sizeof(char)) != 0) {
			printlnf(IOERROR, "Cannot read %s: Not a valid Vox file. Could not read \"VOX \" attribute.", asset.path.c_str());
			return 0;
		}

		// Check version
		unsigned int vers = char4ToIntBE(&data[packetSize]);
		if (vers < 150) {
			printlnf(IOERROR, "Cannot read %s: Version outdated (%d), use version 150 or higher.", asset.path.c_str(), vers);
			return 0;
		}

		// Get the size
		int sizeOffs = getPacketOffs(&data[0], data.size(), packetSize, "SIZE");
		if (sizeOffs < 0) {
			printlnf(IOERROR, "Cannot read %s: Not a valid Vox file. Could not read \"SIZE\" attribute.", asset.path.c_str());
			return 0;
		}
		LocalCoordinate size = {data[sizeOffs + 4 * packetSize], data[sizeOffs + 5 * packetSize], data[sizeOffs + 3 * packetSize]};

		// Yay palettes
		Palette * palette = new Palette(257);
		Voxel air = Voxel({0, 0, 0, 0}, Voxel::IMAGINARY);
		palette->set(air, 0);
		int paletteOffs = getPacketOffs(&data[0], data.size(), packetSize, "RGBA");
		if (sizeOffs < 0) {
			// TODO: use default palette
			for (int i = 1; i < 256; i++) {
				palette->set(Voxel({1,0,1,1}), i);
			}
		} else {
			for (int i = 0; i < 256; i++) {
				int j = paletteOffs + 3 * packetSize + i * packetSize;
				Voxel t = Voxel(Color::u8Color(data[j], (unsigned char)data[j+1], (unsigned char)data[j+2], (unsigned char)data[j+3]));
				palette->set(t, i+1);
			}
		}

		// Now finally load the voxels
		int voxelOffs = getPacketOffs(&data[0], data.size(), packetSize, "XYZI");
		if (voxelOffs < 0) {
			printlnf(IOERROR, "Cannot read %s: Not a valid Vox file. Could not read \"XYZI\" attribute.", asset.path.c_str());
			return 0;
		}
		EntityMesh * mesh = new EntityMesh(size, palette);
		int voxelsToRead = char4ToIntBE(&data[voxelOffs + 3 * packetSize]);
		for (int i = 0; i < voxelsToRead; ++i) {
			int j = voxelOffs + (4 + i) * packetSize;
			LocalCoordinate t = {size.x - data[j+1] - 1, data[j+2], size.z - data[j] - 1};
			mesh->set((unsigned char)data[j + 3], t);
		}

		mesh->generateMesh();

		mesh->buf->sendToGPU();

		return mesh;
	}

	GLenum channelLookup[5] = {0, GL_RED, GL_RG, GL_RGB, GL_RGBA};

	Texture* loadTexture(const Asset& asset) {
		if (!mFileManager->verifyAsset(asset, Asset::Texture)) return 0;

		int w, h, channels;

		glm::u8vec4 *data = reinterpret_cast<glm::u8vec4*>(SOIL_load_image(asset.path.c_str(), &w, &h, &channels, SOIL_LOAD_AUTO));

		if (!data) {
			vox::printf(IOERROR, "Couldn't load image %s.\n", asset.path.c_str());
			exit(1);
		}

		// flip the texture, because reasons
		for(int y = 0; y < h / 2; y++) {
			int index1 = y * w;
			int index2 = (h - y - 1) * w;
			for(int x = 0; x < w; x++) {
				swapValues(data[x + index1], data[x + index2]);
			}
		}

		TexParams tp = TexParams(asset.name);
		Texture* retTex = new Texture();
		glGenTextures(1, &retTex->tex);
		mGL->bindTexture(retTex, 0);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, w, h, 0, channelLookup[channels], GL_UNSIGNED_BYTE, data);
		if (tp.GEN_MIPMAPS) {
			glGenerateMipmap(GL_TEXTURE_2D);
			mGL->analytics.registerMemory((16 * w * h) / 3);
		} else {
			mGL->analytics.registerMemory(4 * w * h);
		}

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, tp.TEXTURE_MAG_FILTER);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, tp.TEXTURE_MIN_FILTER);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, tp.TEXTURE_WRAP_S);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, tp.TEXTURE_WRAP_T);

		SOIL_free_image_data(reinterpret_cast<unsigned char*>(data));
		mGL->unbindTexture(retTex, 0);

		if (!retTex->tex)
			printf(IOERROR, "Couldn't load image: %s.\n", asset.path.c_str());

		retTex->size = {w,h};

		return retTex;
	}

	SDL_Surface * loadSurface(const Asset& asset) {
		if (!mFileManager->verifyAsset(asset, Asset::Texture)) return 0;

		int w, h, channels;
		unsigned char * data = SOIL_load_image(asset.path.c_str(), &w, &h, &channels, SOIL_LOAD_AUTO);

		if (!data) {
			vox::printf(IOERROR, "Couldn't load image %s.\n", asset.path.c_str());
			exit(1);
		}

		// Swap shit for whatever reason
		for (int i = 0; i < w * h; i++) {
			unsigned char tmp = data[i * channels + 1];
			data[i * channels + 1] = data[i * channels + 2];
			data[i * channels + 2] = tmp;
			if (channels == 4) {
				unsigned char tmp = data[i * channels + 0];
				data[i * channels + 0] = data[i * channels + 3];
				data[i * channels + 3] = tmp;
			}
		}

		// Remember to free this surface!
		return SDL_CreateRGBSurfaceFrom(data, w, h, channels * 8, w * channels,
				0xFF000000, 0x00FF0000, 0x0000FF00, channels == 4 ? 0x000000FF : 0);
	}

	/*

	Texture2DArray* loadTexture2DArray(const Asset& a) {
		if (!mFileManager->verifyAsset(a, Asset::Texture2DArray)) return 0;

		LuaScript* s = mRes->getResource<LuaScript, Asset::Texture2DArray>(a.name);
		TexParams tp = TexParams(a.name);
		std::vector<std::string> textures = s->getArray<std::string>("textures");

		int w1,h1;
		Texture2DArray* retTex = new Texture2DArray();

		for(int i = 0; i < textures.size(); i++) {

			int w, h, channels;

			Asset textureAsset = mFileManager->fetchAsset(textures[i], Asset::Texture, true);

			glm::u8vec4 *data = reinterpret_cast<glm::u8vec4*>(SOIL_load_image(textureAsset.path.c_str(), &w, &h, &channels,SOIL_LOAD_AUTO));

			if(i!=0) {
				if(w1 != w || h1 != h) {
					vox::printf(IOERROR, "Couldn't load image %s.\n", textureAsset.path.c_str());
					exit(1);

				}
			} else {
				glGenTextures(1, &retTex->tex);
				glBindTexture(GL_TEXTURE_2D_ARRAY, retTex->tex);
				glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_RGBA16F,
							 w1,h1,textures.size(),
							 GL_FALSE, GL_RGBA, GL_FLOAT, NULL );
			}

			if (!data) {
				vox::printf(IOERROR, "Couldn't load image %s.\n", textureAsset.path.c_str());
				exit(1);
			}

			// flip the texture, because reasons
			for(int y = 0; y < h / 2; y++) {
				int index1 = y * w;
				int index2 = (h - y - 1) * w;
				for(int x = 0; x < w; x++) {
					swapValues(data[x + index1], data[x + index2]);
				}
			}

			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, w, h, 0, channelLookup[channels], GL_UNSIGNED_BYTE, data);
			if (tp.GEN_MIPMAPS) glGenerateMipmap(GL_TEXTURE_2D);
		}

		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, tp.TEXTURE_MAG_FILTER);
		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, tp.TEXTURE_MIN_FILTER);
		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, tp.TEXTURE_WRAP_S);
		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, tp.TEXTURE_WRAP_T);
		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_R, tp.TEXTURE_WRAP_R);

		retTex->size = {w1,h1};
		retTex->textures = textures.size();

		return retTex;
	}

	*/



	CubeMap* loadCubeMap(const Asset& a) {
		if (!mFileManager->verifyAsset(a, Asset::CubeMap)) return 0;

		int w, h, channels;

		unsigned char *data = SOIL_load_image(a.path.c_str(), &w, &h, &channels, SOIL_LOAD_AUTO);

		if (!data)
			vox::printf(IOERROR, "Couldn't load image %s.\n", a.path.c_str());

		int cubeH = h / 6;
		int stride = w * (h / 6) * 4;

		TexParams tp = TexParams(a.name);
		CubeMap* retTex = new CubeMap();
		glGenTextures(1, &retTex->tex);
		mGL->bindTexture(retTex, 0);

		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, tp.TEXTURE_MAG_FILTER);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, tp.TEXTURE_MIN_FILTER);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, tp.TEXTURE_WRAP_S);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, tp.TEXTURE_WRAP_T);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, tp.TEXTURE_WRAP_R);

		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, GL_RGBA8, w, cubeH, 0, channelLookup[channels], GL_UNSIGNED_BYTE, data);
		if (tp.GEN_MIPMAPS) glGenerateMipmap(GL_TEXTURE_CUBE_MAP);
		glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, GL_RGBA8, w, cubeH, 0, channelLookup[channels], GL_UNSIGNED_BYTE, data + stride);
		if (tp.GEN_MIPMAPS) glGenerateMipmap(GL_TEXTURE_CUBE_MAP);
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, GL_RGBA8, w, cubeH, 0, channelLookup[channels], GL_UNSIGNED_BYTE, data + 2 * stride);
		if (tp.GEN_MIPMAPS) glGenerateMipmap(GL_TEXTURE_CUBE_MAP);
		glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, GL_RGBA8, w, cubeH, 0, channelLookup[channels], GL_UNSIGNED_BYTE, data + 3 * stride);
		if (tp.GEN_MIPMAPS) glGenerateMipmap(GL_TEXTURE_CUBE_MAP);
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, GL_RGBA8, w, cubeH, 0, channelLookup[channels], GL_UNSIGNED_BYTE, data + 4 * stride);
		if (tp.GEN_MIPMAPS) glGenerateMipmap(GL_TEXTURE_CUBE_MAP);
		glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, GL_RGBA8, w, cubeH, 0, channelLookup[channels], GL_UNSIGNED_BYTE, data + 5 * stride);
		if (tp.GEN_MIPMAPS) glGenerateMipmap(GL_TEXTURE_CUBE_MAP);

		if (tp.GEN_MIPMAPS) {
			glGenerateMipmap(GL_TEXTURE_2D);
			mGL->analytics.registerMemory(16/3 * w * h * 6);
		} else {
			mGL->analytics.registerMemory(4 * w * h * 6);
		}

		SOIL_free_image_data(data);

		if (!retTex->tex)
			printf(IOERROR, "Couldn't load image: %s.\n", a.path.c_str());

		retTex->size = {w,cubeH};

		return retTex;
	}

	void crashAndBurn(std::string shader, int crawlerPos) {
		printlnf(GLERROR, "at character %i, failed to parse %s", crawlerPos, shader.c_str());
		exit(1);
	}

	bool compare(char a, char b) {
		return (a == b) || (b == '*');
	}

	void parseVersionDeclaration(const std::string& s, std::string& versDeclaration, int& crawlLength) {
		bool inComment = false; // If we're in a comment
		bool inBlockComment = false; // If we're in a block comment
		bool lastCharacterWasAsterisk = false;
		bool beginningComment = false; // If we're past a forward slash
		versDeclaration ="____________";
		unsigned int declCounter = 0;
		const std::string expected = "#version ***";
		unsigned int i;
		for(i = 0; i < s.length(); i++) {
			if(declCounter == expected.length()) break;
			else if(declCounter == 0) { // We still haven't hit the version declaration, so keep looking at comments
				if(inComment) {
					if(s[i] == '\n') inComment = false; // we're on a new line, so the comment ends here
				} else if(inBlockComment) {
					if(lastCharacterWasAsterisk) {
						if(s[i] == '/')
							inBlockComment = false;
						lastCharacterWasAsterisk = false;
					} else {
						if(s[i] == '*')
							lastCharacterWasAsterisk = true;
					}
				} else if(beginningComment) {
					beginningComment = false;
					if(s[i] == '*')
						inBlockComment = true;
					else if(s[i] == '/')
						inComment = true;
					else {
						// Crash and burn, baby
						crashAndBurn(s, i);
					}
				} else if(s[i] == '\n')
					continue;
				else { // this isn't a comment character, so the code starts here
					if(compare(s[i], expected[declCounter])) {
						versDeclaration[declCounter++] = s[i];
					} else if(s[i] == '/') {
						beginningComment = true;
					} else crashAndBurn(s, i);
				}
			} else { // we're inside the version declaration
				if(compare(s[i], expected[declCounter])) {
					versDeclaration[declCounter++] = s[i];
				} else crashAndBurn(s, i);
			}
		}
		for(unsigned int k = 0; k < expected.size(); k++) {
			if(!compare(versDeclaration[k], expected[k]))
				crashAndBurn(s, i);
		}
		versDeclaration = versDeclaration + '\n';
		crawlLength = i;
	}

	GLuint loadShaders(const Asset& vertexShader, const Asset& fragmentShader, const Asset& geometryShader,
			std::vector<std::string> fragShaderOutputs, std::vector<std::string> vAttribs,
			std::vector<std::string> macros, std::vector<Asset> libs, std::vector<std::string> inject) {
		if (!mFileManager->verifyAsset(vertexShader, Asset::VertexShader) ||
			!mFileManager->verifyAsset(fragmentShader, Asset::FragmentShader)) return 0;

		GLuint vertShader = glCreateShader(GL_VERTEX_SHADER);
		GLuint fragShader = glCreateShader(GL_FRAGMENT_SHADER);
		GLuint geomShader = 0;
		if(geometryShader.exists)
			geomShader = glCreateShader(GL_GEOMETRY_SHADER);

		bool shouldICrash = false;

		std::string totMacros = "";
		for(auto s: macros) {
			totMacros += "#define " + s + "\n";
		}
		for(auto l:libs) {
			if(!mFileManager->verifyAsset(l, Asset::ShaderLibrary)) return 0;
			std::string s = vox::loadTextFile(l);
			totMacros += s + "\n";
		}
		for(auto s:inject) {
			totMacros += s + "\n";
		}

		const char * macroSrc = totMacros.c_str();

		std::string vertexShaderVersionInfo;
		int crawlLength;
		std::string vertexShaderStr = vox::loadTextFile(vertexShader);	// Read the text of our vertex shader
		parseVersionDeclaration(vertexShaderStr, vertexShaderVersionInfo, crawlLength);
		vertexShaderStr = vertexShaderStr.substr(crawlLength);
		const char * vertexShaderSrc = vertexShaderStr.c_str();// We have to create a temporary string before this
		const char * vertexVersSrc = vertexShaderVersionInfo.c_str(); // because if we don't, the vertex_shader and fragment_shader data would be destroyed
		const char* vShaderStrings[3] = {vertexVersSrc, macroSrc, vertexShaderSrc};

		// and we'd have an empty char array

		GLint result = GL_FALSE;
		int logLength;

		// Compile vertex shader
		vox::printlnf(GLINFO, "Compiling vertex shader: %s", vertexShader.path.c_str());
		glShaderSource(vertShader, 3, vShaderStrings, 0);
		glCompileShader(vertShader);

		// Check for errors in vertex shader
		glGetShaderiv(vertShader, GL_COMPILE_STATUS, &result);
		glGetShaderiv(vertShader, GL_INFO_LOG_LENGTH, &logLength);
		if (logLength > 1) {
			char* error = new char[logLength];
			glGetShaderInfoLog(vertShader, logLength, 0, error);
			vox::println(GLINFO, error);
			delete[] error;
		}
		if (result != GL_TRUE) {
			vox::println(GLERROR, "Failed to compile vertex shader.");
			shouldICrash = true;
		} else
			vox::println(GLINFO, "Vertex shader compiled successfully");


		std::string fragmentShaderVersionInfo;
		std::string fragmentShaderStr = vox::loadTextFile(fragmentShader);	// Read the text of our vertex shader
		parseVersionDeclaration(fragmentShaderStr, fragmentShaderVersionInfo, crawlLength);
		fragmentShaderStr = fragmentShaderStr.substr(crawlLength);
		const char * fragmentShaderSrc = fragmentShaderStr.c_str();
		const char * fragmentVersSrc = fragmentShaderVersionInfo.c_str();
		const char* fShaderStrings[3] = {fragmentVersSrc, macroSrc, fragmentShaderSrc};

		// Compile fragment shader
		vox::printlnf(GLINFO, "Compiling fragment shader: %s", fragmentShader.path.c_str());
		glShaderSource(fragShader, 3, fShaderStrings, 0);
		glCompileShader(fragShader);

		// Check for errors in fragment shader
		glGetShaderiv(fragShader, GL_COMPILE_STATUS, &result);
		glGetShaderiv(fragShader, GL_INFO_LOG_LENGTH, &logLength);
		if (logLength > 1) {
			char* error = new char[logLength];
			glGetShaderInfoLog(fragShader, logLength, 0, error);
			vox::println(GLERROR, error);
			delete[] error;
		}
		if (result != GL_TRUE) {
			vox::println(GLERROR, "Failed to compile fragment shader.");
			shouldICrash = true;
		} else
			vox::println(GLINFO, "Fragment shader compiled successfully");

		if(geometryShader.exists) {
			std::string geometryShaderVersionInfo;
			std::string geometryShaderStr = vox::loadTextFile(geometryShader);	// Read the text of our vertex shader
			parseVersionDeclaration(geometryShaderStr, geometryShaderVersionInfo, crawlLength);
			geometryShaderStr = geometryShaderStr.substr(crawlLength);
			const char * geometryShaderSrc = geometryShaderStr.c_str();
			const char * geometryVersSrc = geometryShaderVersionInfo.c_str();
			const char* gShaderStrings[3] = {geometryVersSrc, macroSrc, geometryShaderSrc};

			// Compile geometry shader
			vox::printlnf(GLINFO, "Compiling geometry shader: %s", geometryShader.path.c_str());
			glShaderSource(geomShader, 3, gShaderStrings, 0);
			glCompileShader(geomShader);

			// Check for errors in geometry shader
			glGetShaderiv(geomShader, GL_COMPILE_STATUS, &result);
			glGetShaderiv(geomShader, GL_INFO_LOG_LENGTH, &logLength);
			if (logLength > 1) {
				char* error = new char[logLength];
				glGetShaderInfoLog(geomShader, logLength, 0, error);
				vox::println(GLERROR, error);
				delete[] error;
			}
			if (result != GL_TRUE) {
				vox::println(GLERROR, "Failed to compile geometry shader.");
				shouldICrash = true;
			} else
				vox::println(GLINFO, "Geometry shader compiled successfully");
		}

		// Link the shaders into a program
		vox::println(GLINFO, "Linking...");
		GLuint program = glCreateProgram();
		glAttachShader(program, vertShader);
		glAttachShader(program, fragShader);
		if(geometryShader.exists)
			glAttachShader(program, geomShader);

		for(unsigned int i = 0; i < vAttribs.size(); i++) {
			glBindAttribLocation(program, i, vAttribs[i].c_str());
			// this is equal to doing layout(location = i) in T vAttrib[i]
		}
		for(unsigned int i = 0; i < fragShaderOutputs.size(); i++)
			glBindFragDataLocation(program, i, fragShaderOutputs[i].c_str());


		glLinkProgram(program);

		// Check for errors in the linkage
		glGetProgramiv(program, GL_COMPILE_STATUS, &result);
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logLength);
		if (logLength > 1) {
			char* error = new char[logLength];
			glGetProgramInfoLog(program, logLength, 0, error);
			vox::println(GLERROR, error);
			delete[] error;
		}
		if (result != GL_TRUE) {
			vox::println(GLERROR, "Failed to link program "  + vertexShader.name + ".");
			shouldICrash = true;
		}else
			vox::println(GLINFO, "Program " + vertexShader.name + " linked successfully.");

		// Clean up shaders
		glDetachShader(program, vertShader);
		glDetachShader(program, fragShader);
		if(geometryShader.exists)
			glDetachShader(program, geomShader);

		if(shouldICrash) exit(1);

		return program;
	}

	void saveImage(void* pixels, int width, int height, std::string path) {
		int err = SOIL_save_image (
			path.c_str(),
			SOIL_SAVE_TYPE_BMP,
			width, height, 4,
			(unsigned char*) pixels
		);

		if(!err) printlnf(IOERROR, "Saving image %s failed!", path.c_str());
	}
}



