/*
 * modulemanager.h
 *
 *  Created on: Jun 16, 2016
 *      Author: Daniel
 */

#ifndef CORE_SRC_RES_MODULEMANAGER_H_
#define CORE_SRC_RES_MODULEMANAGER_H_

#include <unordered_map>
#include <string>
#include <vector>

namespace vox {
	class ModuleManager {
		std::unordered_map<std::string, std::string> moduleFolders;

	public:
		void registerModule(std::string moduleName, std::string rootFolder);
		void registerAllModules(std::vector<std::string> pathList);

		void initAllModules();
		void initBaseModule();

		std::string getPrefix(std::string moduleName);
	};

	extern ModuleManager * mModuleManager;
}



#endif /* CORE_SRC_RES_MODULEMANAGER_H_ */
