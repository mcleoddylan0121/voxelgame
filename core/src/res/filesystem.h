/*
 * filesystem.h
 *
 *  Created on: Jun 21, 2015
 *      Author: Daniel
 */

#ifndef FILESYSTEM_H_
#define FILESYSTEM_H_

#include <string>
#include <vector>
#include <unordered_map>

namespace vox {

	class FileManager;

	struct Asset {
		friend class FileManager;

		enum Type : unsigned int {
			TexParams, Lua,										// Lua
			Mesh, Texture, CubeMap,	Texture2DArray,				// Graphics
			FragmentShader, VertexShader, GeometryShader, ShaderLibrary,		// OpenGL
			Font, 												// Text Rendering
			Noise,												// Procedural Generation
			Palette,											// Chunk palette
			Unspecified
		};


		bool exists;			// If this is false, don't try to load this asset!
								//  verifyAsset will spew an error already, so just
								//  load the default stuff if the file doesn't exist.

		std::string path;		// A valid path such as "/assets/entities/pooperscooper.lua"
								//  or "C:/Users/Dylan/Desktop/forpoopyfiles/pooperscooper.lua"

		std::string name;		// The name of the file without any extensions or directories
								//  such as "pooperscooper"

		Type type;				// The file is guaranteed to be this type if the file exists

		Asset();				// Creates a nonexistent asset
		Asset(const Asset&);	// Copy Contructor
	private:
		Asset(const bool&, const std::string&, const std::string&, const Type&);
	};

	class FileManager {
	public:
		FileManager(const std::vector<std::string>& paths);
		virtual ~FileManager();

		// The name is without any directories or file extensions
		Asset fetchAsset(const std::string& name, Asset::Type type, bool fatal = true);

		// Prints an error if the asset doesn't exist or is not of the proper type.
		// Returns true if the asset is valid and false if not.
		// Use the macro!
		bool verifyAssetNoMacro(const Asset& asset, Asset::Type correctType, const char * file, int line);
#define verifyAsset(x, y) verifyAssetNoMacro(x, y, __FILE__, __LINE__)

	private:
		std::unordered_map<Asset::Type, std::vector<std::string>, std::hash<unsigned int>> _ext;
		std::unordered_map<Asset::Type, std::vector<std::string>, std::hash<unsigned int>> _pref;

		std::vector<std::string> _paths;
	};

	extern FileManager * mFileManager;

}

#endif /* FILESYSTEM_H_ */
