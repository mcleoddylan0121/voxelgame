/*
 * filesystem.cpp
 *
 *  Created on: Jun 21, 2015
 *      Author: Daniel
 */

#include "filesystem.h"
#include "console.h"
#include "sdlcontroller.h"

#include <fstream>

namespace vox {
	FileManager * mFileManager;

	FileManager::FileManager(const std::vector<std::string>& paths) : _paths(paths) {
		_ext[Asset::Mesh]				= {".vox"};
		_ext[Asset::FragmentShader]		= {".fs"};
		_ext[Asset::VertexShader] 		= {".vs"};
		_ext[Asset::GeometryShader]		= {".gs"};
		_ext[Asset::ShaderLibrary]		= {".glsl"};
		_ext[Asset::Font]				= {".ttf"};
		_ext[Asset::Texture] 			= {".png"};
		_ext[Asset::CubeMap]			= {".png"};
		_ext[Asset::TexParams]			= {".lua", ".texparams"};
		_ext[Asset::Noise]				= {".lua", ".noise"};
		_ext[Asset::Palette]			= {".lua"};
		_ext[Asset::Lua]				= {".lua"};

		_pref[Asset::Mesh]				= {"meshes/"};
		_pref[Asset::FragmentShader]	= {"shaders/"};
		_pref[Asset::VertexShader] 		= {"shaders/"};
		_pref[Asset::GeometryShader]	= {"shaders/"};
		_pref[Asset::ShaderLibrary]		= {"shaders/"};
		_pref[Asset::Font]				= {"fonts/"};
		_pref[Asset::Texture] 			= {"textures/"};
		_pref[Asset::CubeMap]			= {"textures/"};
		_pref[Asset::TexParams]			= {"textures/"};
		_pref[Asset::Noise]				= {"noises/"};
		_pref[Asset::Palette]			= {"voxels/"};
		_pref[Asset::Lua]				= {""};

		for (std::string& p : _paths)
			if (p.back() != '/' && p.back() != '\\') p.append("/");
	}
	FileManager::~FileManager() {}

	Asset FileManager::fetchAsset(const std::string& name, Asset::Type type, bool fatal) {
		for (unsigned int i = 0; i < _paths.size(); i++) {
			for (unsigned int j = 0; j < _ext[type].size(); j++) {
				for (unsigned int k = 0; k < _pref[type].size(); k++) {
					std::string fullPath = _paths[i] + _pref[type][k] + name + _ext[type][j];
					std::ifstream filestream;
					filestream.open(fullPath);
					if (filestream.is_open()) {
						filestream.close();
						return Asset(true, fullPath, name, type);
					}
				}
			}
		}

		if (fatal) {
			printlnf(IOERROR, "Fetch failed: %s (%d)", name.c_str(), (int)type);
			mSDL->showErrorMessageBox("IO Error", std::string("Could not find file: ") + std::string(name));
		}

		return Asset(false, _paths[0] + _pref[type][0] + name + _ext[type][0], name, type);
	}

	bool FileManager::verifyAssetNoMacro(const Asset& asset, Asset::Type corr, const char * func, int line) {
		if (!asset.exists)
			printlnf(IOERROR, "Specified asset doesn't exist: %s (type %u) [in file %s on line %i]", asset.name.c_str(), asset.type, func, line);
		else if (asset.type != corr)
			printlnf(IOERROR, "Specified asset is not the correct type: %s (%u, not %u) [in file %s on line %i]", asset.name.c_str(), asset.type, corr, func, line);
		else return true;
		return false;
	}

	Asset::Asset(const bool& b, const std::string& s1, const std::string& s2, const Type& t)
		: exists(b), path(s1), name(s2), type(t) {}

	Asset::Asset() : Asset(false, "", "", Asset::Unspecified) {}

	Asset::Asset(const Asset& a) : Asset(a.exists, a.path, a.name, a.type) {}
}
