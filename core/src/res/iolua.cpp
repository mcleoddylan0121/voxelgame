/*
 * iolua.cpp
 *
 *  Created on: Aug 3, 2015
 *      Author: Daniel
 */

#include "iolua.h"
#include "filesystem.h"
#include "modulemanager.h"
#include "luacontext.h"

namespace vox {
	TexParams::TexParams(std::string name) : LuaObject({
		lmake(TEXTURE_MAG_FILTER, "TEXTURE_MAG_FILTER", TEXTURE_MAG_FILTER),
		lmake(TEXTURE_MIN_FILTER, "TEXTURE_MIN_FILTER", TEXTURE_MIN_FILTER),
		lmake(TEXTURE_WRAP_S, "TEXTURE_WRAP_S", TEXTURE_WRAP_S),
		lmake(TEXTURE_WRAP_T, "TEXTURE_WRAP_T", TEXTURE_WRAP_T),
		lmake(TEXTURE_WRAP_R, "TEXTURE_WRAP_R", TEXTURE_WRAP_R),
		lmakem(GEN_MIPMAPS)
	}) {
		Asset a = mFileManager->fetchAsset(name, Asset::TexParams, false);
		if (a.exists) {
			LuaScript * s = mLua->makeScript(a, "_tex");
			mLua->loadFile(a, "_tex");
			init(*s, "");
			delete s;
		}
	}

	std::string getLuaPath(std::string moduleName, std::string name) {
		auto file = mFileManager->fetchAsset(mModuleManager->getPrefix(moduleName) + "lua/" + name, Asset::Lua, false);
		mFileManager->verifyAsset(file, Asset::Lua);
		return file.path;
	}
}
