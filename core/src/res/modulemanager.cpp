/*
 * modulemanager.cpp
 *
 *  Created on: Jun 16, 2016
 *      Author: Daniel
 */

#include "modulemanager.h"
#include "filesystem.h"
#include "luacontext.h"
#include "vlua.h"

namespace vox {

	ModuleManager * mModuleManager;

	void ModuleManager::registerModule(std::string moduleName, std::string rootFolder) {
		moduleFolders[moduleName] = rootFolder;
	}

	void ModuleManager::registerAllModules(std::vector<std::string> pathList) {
		for (std::string path : pathList) {
			LuaScript * tmp = new LuaScript(mFileManager->fetchAsset(path + "modulelist", Asset::Lua, true).path);
			std::vector<std::string> modules = tmp->getArray<std::string>("modules");
			delete tmp;

			for (std::string module : modules) {
				std::string modulePath = path + module + "/";

				if (moduleFolders.find(module) != moduleFolders.end())
					printlnf(MessageType::LUAIOERROR, "Cannot register module \"%s\" (%s) because its name is the same as another already registered (%s)",
							module.c_str(), modulePath.c_str(), moduleFolders[module].c_str());
				else registerModule(module, modulePath);
			}
		}
	}

	void ModuleManager::initAllModules() {
		for (auto modulePath : moduleFolders) {
			if (modulePath.first == "base") continue;

			auto file = mFileManager->fetchAsset(modulePath.second + "lua/init", Asset::Lua, false);
			if (file.exists)
				mLua->loadFile(file, modulePath.first);
		}
	}

	void ModuleManager::initBaseModule() {
		auto file = mFileManager->fetchAsset(moduleFolders["base"] + "lua/init", Asset::Lua, false);
		mLua->loadFile(file, "base");
	}

	std::string ModuleManager::getPrefix(std::string moduleName) {
		auto i = moduleFolders.find(moduleName);

		if (i == moduleFolders.end()) {
			printlnf(LUAIOERROR, "Could not find module %s. Was it registered?", moduleName.c_str());
			return "";
		}

		return i->second;
	}
}
