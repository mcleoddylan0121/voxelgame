/*
 * component.h
 *
 *  Created on: Jul 9, 2015
 *      Author: Daniel
 */

#ifndef COMPONENT_H_
#define COMPONENT_H_

#include "guiutil.h"
#include "renderutils.h"
#include "guirender.h"
#include "events.h"

#include <vector>
#include <tuple>

namespace vox {

	struct TextField;
	struct Font;

namespace gui {

	struct GUILibrary;

	struct Component {
		friend struct GUILibrary;
		friend struct GUIManager;

		Component(std::string address, GUIManager * parent, FrameFactory factory);
		virtual ~Component();

		Rectangle getBoundingBox(Point p);

		void setSize(Point size);
		void setVisible(bool visible);
		void setHue(glm::vec4 hue);
		void setFont(Font * f);
		void setText(std::string text);

		Point getSize();
		bool isVisible();

		void updateSkin();

	private:
		GUIRenderObject * _frameRender = nullptr;
		FrameFactory _factory;

		TextField * _textField = nullptr;
		Font * _font = nullptr;
		std::string _text = "";
		Point _textOffs = {0, 0};

		Point _size;
		glm::vec2 _scale = {1, 1};
		glm::vec4 _hue = {1, 1, 1, 1};
		std::string _address = "";

		bool _visible = true;
		bool _interactable = true;

		GUIManager * _parent = nullptr;
		GUIRenderObject * _img = nullptr;

		std::vector<EventListener> _listeners;
		std::vector<ComponentID> _children;
	};

} // namespace gui

} // namespace vox

#endif /* CONTAINER_H_ */
