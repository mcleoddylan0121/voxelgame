/*
 * events.h
 *
 *  Created on: Jul 14, 2015
 *      Author: Daniel
 */

#ifndef EVENTS_H_
#define EVENTS_H_

#include "guiutil.h"
#include "vluautil.h"

#include <string>
#include <unordered_map>

namespace vox {

namespace gui {

	struct MouseEvent : public LuaObject {
		lvec2 absolutePos, relativePos;
		int button;

		MouseEvent();
	};

	struct MouseMotionEvent : public LuaObject {
		lvec2 delta, absolutePos, relativePos;

		MouseMotionEvent();
	};

	struct KeyEvent : public LuaObject {
		int keyCode;

		KeyEvent();
	};

	struct TextTypedEvent : public LuaObject {
		std::string text;

		TextTypedEvent();
	};

	struct EventListener {
		std::string address;
		std::unordered_map<std::string, bool> functionExists;
	};
}
}




#endif /* EVENTS_H_ */
