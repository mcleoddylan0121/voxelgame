/*
 * guiutil.cpp
 *
 *  Created on: Jul 9, 2015
 *      Author: Daniel
 */

#include "guiutil.h"
#include "guirender.h"

namespace vox {

namespace gui {

	BorderWeight::BorderWeight() : LuaObject({
		lmakem(top), lmakem(right), lmakem(bottom), lmakem(left)
	}) {}
	CornerWeight::CornerWeight() : LuaObject({
		lmakem(topLeft), lmakem(topRight), lmakem(bottomRight), lmakem(bottomLeft)
	}) {}
	AtlasEntry::AtlasEntry() : LuaObject({
		lmakem(pos), lmakem(size)
	}) {}
	AtlasEntry::operator TextureAtlasEntry() {
		return {pos, size};
	}
	FrameAtlas::FrameAtlas() : LuaObject({
		lmakem(texName),
		lmakem(topLeft), lmakem(top), lmakem(topRight), lmakem(right), lmakem(bottomRight), lmakem(bottom), lmakem(bottomLeft), lmakem(left)
	}) {}
	FrameFactory::FrameFactory() : LuaObject({
		lmakem(atlas),
		lmakem(borders),
		lmakem(corners)
	}) {}

	GUIRenderObject * FrameFactory::make(Point size) {
		return makeFrame(atlas, size, borders, corners);
	}

	Rectangle FrameFactory::getBounds(Point p, Point size) {
		return Rectangle(p, size);
	}

	Point FrameFactory::getArea() {
		return {borders.left + borders.right, borders.top + borders.bottom};
	}

	Point FrameFactory::getOffs() {
		return {borders.left, borders.bottom};
	}

	Rectangle::Rectangle(Point pos, Point size) : x(pos.x), y(pos.y), w(size.x), h(size.y) {}

	Rectangle::Rectangle(int x, int y, int w, int h) : x(x), y(y), w(w), h(h) {}

	bool Rectangle::intersects(Rectangle other) {
		return
		  !(x + w <= other.x	   ||
			x >= other.x + other.w ||
			y + h <= other.y 	   ||
			y >= other.y + other.h);
	}

	bool Rectangle::contains(Point p) {
		return !(p.x >= x + w ||
				 p.x <= x	  ||
				 p.y >= y + h ||
			     p.y <= y);
	}
} // namespace gui

} // namespace vox

