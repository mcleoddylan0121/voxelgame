/*
 * container.cpp
 *
 *  Created on: Jul 9, 2015
 *      Author: Daniel
 */

#include "component.h"
#include "resourcemanager.h"
#include "guimanager.h"
#include "vfreetype.h"
#include "fontrender.h"

namespace vox {

namespace gui {

	Component::Component(std::string address, GUIManager * parent, FrameFactory factory) : _factory(factory), _address(address), _parent(parent) {
		updateSkin();
		setFont(parent->defaultFont);
	}

	Component::~Component() {
		delete _frameRender;
		delete _img;
		delete _textField;
	}

	void Component::updateSkin() {
		delete _img;
		_img = _parent->makeComponentRenderObject(_address, "backTex");
		_img->hue = _hue;

		setSize(_size);
	}

	void Component::setSize(Point p) {
		this->_size = p;
		this->_scale = glm::vec2(p - _factory.getArea()) / glm::vec2(this->_img->size);

		delete _frameRender;
		_frameRender = _factory.make(p);
		_frameRender->hue = _hue;
	}

	Rectangle Component::getBoundingBox(Point pos) {
		return _factory.getBounds(pos, _size);
	}

	void Component::setVisible(bool vis) { this->_visible = vis; }

	Point Component::getSize() { return this->_size; }
	bool Component::isVisible() { return this->_visible; }
	void Component::setHue(glm::vec4 hue) {
		_hue = hue;
		if (_img != nullptr) _img->hue =_hue;
		if (_frameRender != nullptr) _frameRender->hue =_hue;
	}
	void Component::setFont(Font * f) {
		_font = f;
		_textField = new TextField(1000);
		setText(_text);
	}
	void Component::setText(std::string text) {
		_textField->setText(text, _font, {16,16}, {1,1,1,1});
	}

} // namespace gui

} // namespace vox

