/*
 * events.cpp
 *
 *  Created on: Jul 14, 2015
 *      Author: Daniel
 */

#include "events.h"

namespace vox {

namespace gui {

	MouseEvent::MouseEvent() : LuaObject({
		lmakem(absolutePos),
		lmakem(relativePos),
		lmakem(button)
	}) {}

	MouseMotionEvent::MouseMotionEvent() : LuaObject({
		lmakem(delta),
		lmakem(absolutePos),
		lmakem(relativePos)
	}) {}

	KeyEvent::KeyEvent() : LuaObject({
		lmake(keyCode, "keyCode")
	}) {}

	TextTypedEvent::TextTypedEvent() : LuaObject({
		lmakem(text)
	}) {}

}

}
