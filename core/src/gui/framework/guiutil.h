/*
 * guiutil.h
 *
 *  Created on: Jul 9, 2015
 *      Author: Daniel
 */

#ifndef GUIUTIL_H_
#define GUIUTIL_H_

#include "vglm.h"
#include "vluautil.h"

namespace vox {
	struct TextureAtlasEntry;
	struct GUIRenderObject;

namespace gui {

	typedef glm::ivec2 Point;
	struct GUIManager;
	typedef int ComponentID;

	struct Rectangle {
		int x, y, w, h;

		Rectangle(Point pos, Point size);
		Rectangle(int x, int y, int w, int h);

		bool intersects(Rectangle other);
		bool contains(Point p);

		Point getPos();
		Point getSize();
	};


	struct BorderWeight : public LuaObject {
		int top, right, bottom, left;
		BorderWeight();
	};
	struct CornerWeight : public LuaObject {
		lvec2 topLeft, topRight, bottomRight, bottomLeft;
		CornerWeight();
	};
	struct AtlasEntry : public LuaObject {
		lvec2 pos, size;
		AtlasEntry();

		operator TextureAtlasEntry();
	};
	struct FrameAtlas : public LuaObject {
		std::string texName;
		AtlasEntry topLeft, top, topRight, right, bottomRight, bottom, bottomLeft, left;
		FrameAtlas();
	};
	struct FrameFactory : public LuaObject {
		FrameAtlas atlas;
		BorderWeight borders;
		CornerWeight corners;

		FrameFactory();

		GUIRenderObject * make(Point size);
		Rectangle getBounds(Point pos, Point size);

		Point getOffs();
		Point getArea();
	};

} // namespace gui

} // namespace vox

#endif /* GUIUTIL_H_ */
