/*
 * guilua.cpp
 *
 *  Created on: Jul 14, 2015
 *      Author: Daniel
 */

#include "guilua.h"
#include "guimanager.h"
#include "component.h"
#include "events.h"
#include "sdlcontroller.h"
#include "gameloop.h"

#include "vfreetype.h"
#include "fontrender.h"

#include <algorithm>
#include <limits>

namespace vox {

namespace gui {
	GUIManager * GUILibrary::_m;

	ComponentID GUILibrary::createComponent(std::string address, lvec2 size) {
		Component * c = new Component(address, _m, _m->getFrameFactory(address));
		c->setSize(size);

		// Plz no h8; just appreci8.
		for (ComponentID i = 0; i < std::numeric_limits<ComponentID>::max(); i++) {
			auto it = _m->_frames.find(i);
			if (it == _m->_frames.end()) {
				Point p = Point(mSDL->viewportWidth, mSDL->viewportHeight) / 2 - c->getSize() / 2;
				ContainedComponent cc;
				cc.comp = c;
				cc.pos = p;
				_m->_frames[i] = cc;
				return i;
			}
		}

		return -1;
	}

	void GUILibrary::setPos(ComponentID i, lvec2 pos) {
		_m->getFrame(i).pos = pos;
	}

	void GUILibrary::removeParents(ComponentID i) {
		auto& c = _m->getFrame(i);

		if (c.parent >= 0) {
			auto& last_parent = _m->getFrame(c.parent).comp->_children;
			auto it = std::find(last_parent.begin(), last_parent.end(), i);
			if (it != last_parent.end()) last_parent.erase(it);
		} else if (c.parent == ContainedComponent::GLOBAL) {
			auto it = std::find(_m->_frameOrder.begin(), _m->_frameOrder.end(), i);
			if (it != _m->_frameOrder.end()) _m->_frameOrder.erase(it);
		}
	}

	void GUILibrary::setParent(ComponentID i, ComponentID parent) {
		auto& p = _m->getFrame(parent);
		auto& c = _m->getFrame(i);

		removeParents(i);

		p.comp->_children.push_back(i);
		c.parent = parent;
	}

	void GUILibrary::makeWindow(ComponentID i) {
		removeParents(i);
		_m->getFrame(i).parent = ContainedComponent::GLOBAL;
		_m->_frameOrder.push_back(i);
	}

	void GUILibrary::setVisible(ComponentID i, bool b) {
		_m->getFrame(i).comp->setVisible(b);
	}

	void GUILibrary::setInteractable(ComponentID i, bool b) {
		_m->getFrame(i).comp->_interactable = b;
	}

	void GUILibrary::setSize(ComponentID i, lvec2 size) {
		_m->getFrame(i).comp->setSize((glm::ivec2)(glm::vec2)size);
	}

	void GUILibrary::addListener(ComponentID i, std::string e) {
		_m->getFrame(i).comp->_listeners.push_back(_makeListener(e));
	}

	void GUILibrary::bringToFront(ComponentID i) {
		_m->bringToFront(i);
	}

	void GUILibrary::setTextMode(bool b) {
		mSDL->setTextMode(b);
	}

	std::vector<std::string> _funcs = {
			".mousePressed",
			".mouseReleased",
			".mouseDown",
			".mouseMoved",
			".mouseExited",
			".mouseEntered",
			".keyPressed",
			".keyReleased",
			".keyDown",
			".textTyped",
			".focusGained",
			".focusLost"
	};

	EventListener GUILibrary::_makeListener(std::string e) {
		EventListener ret;
		ret.address = e;
		for (std::string& s : _funcs)
			ret.functionExists[s] = _m->_handlerScript->variableExists(e + s);
		return ret;
	}

	void GUILibrary::addGlobalListener(std::string e) {
		_m->_globalListeners.push_back(_makeListener(e));
	}

	void GUILibrary::setMouseGrabbed(bool b) {
		mSDL->setMouseGrabbed(b);
	}

	void GUILibrary::exitGame() {
		mGameLoop.kill();
	}

	void GUILibrary::updateSkin(ComponentID i) {
		auto& f = _m->getFrame(i).comp;
		f->_factory = _m->getFrameFactory(f->_address);
		f->updateSkin();
	}

	void GUILibrary::setHue(ComponentID i, lvec4 hue) {
		_m->getFrame(i).comp->setHue(hue);
	}

	void GUILibrary::setText(ComponentID i, std::string text) {
		_m->getFrame(i).comp->setText(text);
	}
	void GUILibrary::setTextPos(ComponentID i, lvec2 pos) {
		_m->getFrame(i).comp->_textOffs = pos;
	}
	lvec2 GUILibrary::getTextSize(ComponentID i) {
		auto f = _m->getFrame(i).comp->_textField;
		lvec2 doob;
		doob.init(f->scale * (f->renderObject->size - glm::vec2(0, f->renderObject->originOffsetY)));
		return doob;
	}
}

}
