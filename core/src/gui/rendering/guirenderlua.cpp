/*
 * guirenderlua.cpp
 *
 *  Created on: Jul 13, 2015
 *      Author: Daniel
 */

#include "guirenderlua.h"
#include "guirender.h"

namespace vox {

namespace gui {
	LuaQuadInfo::LuaQuadInfo() : LuaObject({
		lmake(type, "type"),
		lmake(texName, "texName"),
		lmake(size, "size"),
		lmake(tile, "tile"),
		lmake(tilesInEachDirection, "tilesInEachDirection"),
		lmake(sprite, "sprite")
	}) {}

	GUIRenderObject * LuaQuadInfo::make() {
		LuaQuadInfo::Type type = (LuaQuadInfo::Type)this->type;

		switch (type) {
		case Quad:
			return makeQuad(texName, glm::ivec2((glm::vec2)size), glm::bvec2((glm::vec2)tile));
		case QuadFromAtlas:
			return makeQuadFromAtlas(texName, (TextureAtlasEntry)sprite, glm::ivec2((glm::vec2)size), glm::bvec2((glm::vec2)tile));
		case TiledQuad:
			return makeTiledQuad(texName, (TextureAtlasEntry)sprite, tilesInEachDirection, glm::ivec2((glm::vec2)size));
		}

		return 0;
	}


	LuaQuadInfo luaMakeQuad(std::string texName, lvec2 onScreenSize, lvec2 tile) {
		LuaQuadInfo ret;

		ret.type = (int)LuaQuadInfo::Quad;

		if (onScreenSize.isCreated()) ret.size = (glm::vec2)onScreenSize;
		else ret.size = ON_SCREEN_SIZE_SAME_AS_SPRITE_SIZE;

		if (tile.isCreated()) ret.tile = (glm::vec2)tile;
		else ret.tile = {false, false};

		ret.texName = texName;

		return ret;
	}

	LuaQuadInfo luaMakeQuadFromAtlas(std::string texName, AtlasEntry sprite, lvec2 onScreenSize, lvec2 tile) {
		LuaQuadInfo ret;

		ret.type = (int)LuaQuadInfo::QuadFromAtlas;

		if (onScreenSize.isCreated()) ret.size = (glm::vec2)onScreenSize;
		else ret.size = ON_SCREEN_SIZE_SAME_AS_SPRITE_SIZE;

		if (tile.isCreated()) ret.tile = (glm::vec2)tile;
		else ret.tile = {false, false};

		ret.texName = texName;
		ret.sprite.pos = (glm::vec2)sprite.pos;
		ret.sprite.size = (glm::vec2)sprite.size;

		return ret;
	}

	LuaQuadInfo luaMakeTiledQuad(std::string texName, AtlasEntry sprite, lvec2 tilesInEachDirection, lvec2 tileSize) {
		LuaQuadInfo ret;

		ret.type = (int)LuaQuadInfo::TiledQuad;

		if (tileSize.isCreated()) ret.size = (glm::vec2)tileSize;
		else ret.size = ON_SCREEN_SIZE_SAME_AS_SPRITE_SIZE;

		ret.texName = texName;
		ret.sprite.pos = (glm::vec2)sprite.pos;
		ret.sprite.size = (glm::vec2)sprite.size;
		ret.tilesInEachDirection = (glm::vec2)tilesInEachDirection;

		return ret;
	}
}

}


