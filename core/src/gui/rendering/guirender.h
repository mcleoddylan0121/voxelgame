/*
 * guirender.h
 *
 *  Created on: Jul 10, 2015
 *      Author: Dylan
 */

#ifndef GUIRENDER_H_
#define GUIRENDER_H_

#include "coords.h"
#include "renderutils.h"

#include <vector>
#include <utility>

namespace vox {

	class GUIShaderProgram; class TextShaderProgram;
	template<typename T> struct GLBuffer;
	template<typename T> struct StackBuffer;
	class FrameBuffer;

	struct GUIVertex {
		glm::i16vec2 pos;
		glm::i16vec2 UV;
		glm::i16vec2 bottomleft;
		glm::i16vec2 ref;
		glm::i16vec2 textureSize;
		glm::bvec2 repeat;
		glm::i16vec2 repeatSize; // if repeat is disabled, ignore this
	};

	// Owns the buffer but not the texture!
	struct GUIRenderObject {
		Texture* tex;
		GLBuffer<GUIVertex>* buf;
		glm::ivec2 size;
		glm::vec4 hue = {1,1,1,1};

		virtual ~GUIRenderObject();
	};

	struct GUIDisplayObject {
		GUIRenderObject* renderObj;
		glm::ivec2 pos;
		glm::vec2 stretch;

		GUIDisplayObject(GUIRenderObject* renderObj, glm::ivec2 pos, glm::vec2 stretch):
			renderObj(renderObj), pos(pos), stretch(stretch) {}
	};

	typedef Renderer<GUIDisplayObject> GUIRenderer;

	void pushQuad(StackBuffer<GUIVertex>* out, glm::ivec2 bottomLeft, glm::ivec2 size, glm::ivec2 ref, glm::ivec2 texSize, glm::bvec2 repeat, glm::ivec2 screenRepeatSize = {0,0});
	void getFrameVertices(StackBuffer<GUIVertex>* out, glm::ivec2 bottomLeft, glm::ivec2 size, std::vector<int> edgeWidths, std::vector<glm::ivec2> cornerSizes, std::vector<TextureAtlasEntry> textures);

	struct FontRenderObject;

	struct GUIRenderInstance {
		GUIDisplayObject displayObj;
		int layer;
	};

	struct FontRenderInstance {
		TextDisplayObject displayObj;
		int layer;
	};

	class GUIRenderHelper: public TextRenderer, public GUIRenderer {
	public:
		GUIRenderHelper(GUIShaderProgram* guiShader, TextShaderProgram* textShader):
			guiShader(guiShader), textShader(textShader) {}
		virtual ~GUIRenderHelper();

		GUIShaderProgram* guiShader;
		TextShaderProgram* textShader;
		std::vector<GUIRenderInstance> guiQueue;
		std::vector<FontRenderInstance> textQueue;
		int curlayer = 9999;
		glm::vec4 colorMult = {1,1,1,1};
		void render(FrameBuffer* fbo);
		void addToQueue(GUIDisplayObject);
		void addToQueue(TextDisplayObject);

		void render(GUIDisplayObject);
		void render(TextDisplayObject);
	};

	// Here because fuck idk
	namespace gui {
		struct QuadAtlas {
			std::string texName;
			TextureAtlasEntry sprite;
		};

		struct BorderWeight;
		struct CornerWeight;
		struct FrameAtlas;

		GUIRenderObject * makeFrame(FrameAtlas atlas, glm::ivec2 size, BorderWeight borders, CornerWeight corners);

		const glm::ivec2 ON_SCREEN_SIZE_SAME_AS_SPRITE_SIZE = {-1, -1};

		GUIRenderObject * makeQuad(std::string texName);
		GUIRenderObject * makeQuad(std::string texName, glm::ivec2 onScreenSize, glm::bvec2 tile = {false, false});

		GUIRenderObject * makeQuadFromAtlas(std::string texName, TextureAtlasEntry sprite, glm::ivec2 onScreenSize = ON_SCREEN_SIZE_SAME_AS_SPRITE_SIZE, glm::bvec2 tile = {false, false});

		GUIRenderObject * makeTiledQuad(std::string texName, glm::vec2 tilesInEachDirection, glm::ivec2 tileSize = ON_SCREEN_SIZE_SAME_AS_SPRITE_SIZE);
		GUIRenderObject * makeTiledQuad(std::string texName, TextureAtlasEntry sprite, glm::vec2 tilesInEachDirection, glm::ivec2 tileSize = ON_SCREEN_SIZE_SAME_AS_SPRITE_SIZE);
	}
}



#endif /* GUIRENDER_H_ */
