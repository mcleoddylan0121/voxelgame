/*
 * guirender.cpp
 *
 *  Created on: Jul 10, 2015
 *      Author: Dylan
 */

#include "guirender.h"
#include "geometry.h"
#include "glcontroller.h"
#include "resourcemanager.h"
#include "guiutil.h"
#include "program.h"
#include "glbuffer.h"
#include "voxelmesh.h"
#include "framebuffer.h"
#include "fontrender.h"
#include "sdlcontroller.h"

namespace vox {

	void pushQuad(StackBuffer<GUIVertex>* out, glm::ivec2 bottomLeft, glm::ivec2 size, glm::ivec2 ref, glm::ivec2 texSize, glm::bvec2 repeat, glm::ivec2 screenRepeatSize) {
		if(screenRepeatSize == glm::ivec2(0,0)) screenRepeatSize = texSize;
		out->push({
			GUIVertex{ bottomLeft, 							ref, 							bottomLeft, ref, texSize, repeat, screenRepeatSize },
			GUIVertex{ bottomLeft + glm::ivec2(size.x, 0), 	ref + glm::ivec2(texSize.x, 0), bottomLeft, ref, texSize, repeat, screenRepeatSize },
			GUIVertex{ bottomLeft + size,					ref + texSize,					bottomLeft, ref, texSize, repeat, screenRepeatSize },
			GUIVertex{ bottomLeft + glm::ivec2(0, size.y), 	ref + glm::ivec2(0, texSize.y), bottomLeft, ref, texSize, repeat, screenRepeatSize }
		});
	}

	void getFrameVertices(StackBuffer<GUIVertex>* out, glm::ivec2 bottomLeft, glm::ivec2 size, std::vector<int> edgeWidths, std::vector<glm::ivec2> cornerSizes, std::vector<TextureAtlasEntry> textures) {
		pushQuad(out, glm::ivec2(cornerSizes[0].x, 0) + bottomLeft, 						glm::ivec2(size.x - cornerSizes[0].x - cornerSizes[1].x, edgeWidths[0]), 	textures[0].pos, textures[0].size, glm::bvec2(true, false));
		pushQuad(out, glm::ivec2(cornerSizes[2].x, size.y - edgeWidths[1]) + bottomLeft,	glm::ivec2(size.x - cornerSizes[2].x - cornerSizes[3].x, edgeWidths[1]), 	textures[1].pos, textures[1].size, glm::bvec2(true, false));
		pushQuad(out, glm::ivec2(0,cornerSizes[0].y) + bottomLeft, 							glm::ivec2(edgeWidths[2], size.y - cornerSizes[0].y - cornerSizes[2].y), 	textures[2].pos, textures[2].size, glm::bvec2(false, true));
		pushQuad(out, glm::ivec2(size.x - edgeWidths[3], cornerSizes[1].y) + bottomLeft, 	glm::ivec2(edgeWidths[3], size.y - cornerSizes[1].y - cornerSizes[3].y), 	textures[3].pos, textures[3].size, glm::bvec2(false, true));

		pushQuad(out, bottomLeft,	 											cornerSizes[0], textures[4].pos, textures[4].size, glm::bvec2(false,false));
		pushQuad(out, glm::ivec2(size.x - cornerSizes[1].x,0) + bottomLeft, 	cornerSizes[1], textures[5].pos, textures[5].size, glm::bvec2(false,false));
		pushQuad(out, glm::ivec2(0,size.y - cornerSizes[2].y) + bottomLeft, 	cornerSizes[2], textures[6].pos, textures[6].size, glm::bvec2(false,false));
		pushQuad(out, size - cornerSizes[3] + bottomLeft, 						cornerSizes[3], textures[7].pos, textures[7].size, glm::bvec2(false,false));
	}

	GUIRenderObject::~GUIRenderObject() { delete buf; }

	void GUIRenderHelper::render(FrameBuffer* fbo) {

		mGL->bindProgram(guiShader);
		glm::vec2 screenSize = glm::vec2(fbo->size) / fbo->pixelSize;
		glUniform2fv(guiShader->screenSize, 1, &screenSize[0]);
		guiShader->beginRender();
		for(auto inst:guiQueue) {
			if(!inst.displayObj.renderObj) continue;
			mGL->bindTexture(inst.displayObj.renderObj->tex, 0);

			glm::vec2 fpos(inst.displayObj.pos);
			glm::vec2 tsize(inst.displayObj.renderObj->tex->size);
			glUniform2fv(guiShader->offset, 1, &fpos[0]);
			glUniform2fv(guiShader->stretch, 1, &inst.displayObj.stretch[0]);
			glUniform2fv(guiShader->textureAtlasSize, 1, &tsize[0]);
			glUniform1i(guiShader->textureAtlas, 0);
			glm::vec4 uniformHue = colorMult * inst.displayObj.renderObj->hue;
			glUniform4fv(guiShader->hue, 1, &uniformHue[0]);
			float flayer = inst.layer;
			glUniform1f(guiShader->layer, flayer);

			inst.displayObj.renderObj->buf->bindBuffer();
			glVertexAttribPointer(0,2,GL_SHORT,GL_FALSE,sizeof(GUIVertex),(void*) offsetof(GUIVertex, pos));
			glVertexAttribPointer(1,2,GL_SHORT,GL_FALSE,sizeof(GUIVertex),(void*) offsetof(GUIVertex, UV));
			glVertexAttribPointer(2,2,GL_SHORT,GL_FALSE,sizeof(GUIVertex),(void*) offsetof(GUIVertex, bottomleft));
			glVertexAttribPointer(3,2,GL_SHORT,GL_FALSE,sizeof(GUIVertex),(void*) offsetof(GUIVertex, ref));
			glVertexAttribPointer(4,2,GL_SHORT,GL_FALSE,sizeof(GUIVertex),(void*) offsetof(GUIVertex, textureSize));
			glVertexAttribPointer(5,2,GL_UNSIGNED_BYTE, GL_FALSE,sizeof(GUIVertex),(void*) offsetof(GUIVertex, repeat));
			glVertexAttribPointer(6,2,GL_SHORT,GL_FALSE,sizeof(GUIVertex),(void*) offsetof(GUIVertex, repeatSize));

			VoxelMeshBuffer::indexBuffer->bindBuffer();
			glDrawElements(GL_TRIANGLES, inst.displayObj.renderObj->buf->size * 6 / 4, GL_UNSIGNED_INT, (GLvoid*) 0);
		}
		guiShader->endRender();

		mGL->bindProgram(textShader);
		glUniform2fv(textShader->screenSize, 1, &screenSize[0]);
		textShader->beginRender();
		for(auto inst:textQueue) {
			if(inst.displayObj.fontRender)
				mGL->renderText(textShader, inst.displayObj.fontRender, inst.displayObj.pos, inst.displayObj.size, inst.displayObj.color * colorMult, inst.layer);
		}
		textShader->endRender();

		curlayer = 9999;
		guiQueue.clear();
		textQueue.clear();
	}

	void GUIRenderHelper::addToQueue(GUIDisplayObject g) {
		guiQueue.push_back(GUIRenderInstance{g,curlayer--});
	}
	void GUIRenderHelper::addToQueue(TextDisplayObject t) {
		textQueue.push_back(FontRenderInstance{t,curlayer--});
	}

	void GUIRenderHelper::render(GUIDisplayObject g) { addToQueue(g); }
	void GUIRenderHelper::render(TextDisplayObject t) { addToQueue(t); }

	GUIRenderHelper::~GUIRenderHelper() { delete guiShader; delete textShader; }

namespace gui {

	TextureAtlasEntry _f_a(AtlasEntry a, int size) {
		return {{a.pos.x, size - a.pos.y}, (glm::ivec2)a.size};
	}

	std::vector<TextureAtlasEntry> _f_b(const FrameAtlas& a, int s) {
		return { _f_a(a.bottom, s), _f_a(a.top, s), _f_a(a.left, s), _f_a(a.right, s),
				_f_a(a.bottomLeft, s), _f_a(a.bottomRight, s), _f_a(a.topLeft, s), _f_a(a.topRight, s) };
	}

	GUIRenderObject * makeFrame(FrameAtlas atlas, glm::ivec2 size, BorderWeight borders, CornerWeight corners) {
		Texture * tex = TEX_ALLOC::get(atlas.texName);
		StackBuffer<GUIVertex> * buf = new StackBuffer<GUIVertex>(ARRAY, DYNAMIC);

		GUIRenderObject * obj = new GUIRenderObject();
		obj->tex = tex;
		obj->buf = buf;
		obj->size = glm::ivec2(size.x, size.y);

		getFrameVertices(
				buf,																						// The buffer to modify
				glm::ivec2(0, 0),																			// The offset when rendering the frame
				obj->size,																					// The size of the frame to wrap around the parameters
				{ borders.bottom, borders.top, borders.left, borders.right },								// The size of the edges
				{ corners.bottomLeft, corners.bottomRight, corners.topLeft, corners.topRight },				// The size of the corners
				{ atlas.bottom, atlas.top, atlas.left, atlas.right, atlas.bottomLeft, atlas.bottomRight, atlas.topLeft, atlas.topRight }); // The sprite positions
		buf->generateBuffer();

		return obj;
	}


	GUIRenderObject * makeQuad(std::string texName) {
		Texture * tex = TEX_ALLOC::get(texName);
		return makeQuad(texName, tex->size);
	}

	GUIRenderObject * makeQuad(std::string texName, glm::ivec2 onScreenSize, glm::bvec2 tile) {
		Texture * tex = TEX_ALLOC::get(texName);
		return makeQuadFromAtlas(texName, {{0, 0}, tex->size}, onScreenSize, tile);
	}

	GUIRenderObject * makeQuadFromAtlas(std::string texName, TextureAtlasEntry sprite, glm::ivec2 onScreenSize, glm::bvec2 tile) {
		if (onScreenSize == ON_SCREEN_SIZE_SAME_AS_SPRITE_SIZE) onScreenSize = sprite.size;

		Texture * tex = TEX_ALLOC::get(texName); // Calls mRes->getResource<Texture, Asset::texture>() btw
		StackBuffer<GUIVertex> * buf = new StackBuffer<GUIVertex>(ARRAY, DYNAMIC);

		pushQuad(buf, {0, 0}, onScreenSize, sprite.pos, sprite.size, tile, onScreenSize);

		buf->generateBuffer();

		GUIRenderObject * obj = new GUIRenderObject();
		obj->tex = tex;
		obj->buf = buf;
		obj->size = onScreenSize;

		return obj;
	}

	GUIRenderObject * makeTiledQuad(std::string texName, glm::vec2 tilesInEachDirection, glm::ivec2 tileSize) {
		Texture * tex = TEX_ALLOC::get(texName);
		return makeTiledQuad(texName, {{0, 0}, tex->size}, tilesInEachDirection, tileSize);
	}

	GUIRenderObject * makeTiledQuad(std::string texName, TextureAtlasEntry sprite, glm::vec2 tilesInEachDirection, glm::ivec2 tileSize) {
		if (tileSize == ON_SCREEN_SIZE_SAME_AS_SPRITE_SIZE) tileSize = sprite.size;

		Texture * tex = TEX_ALLOC::get(texName);
		StackBuffer<GUIVertex> * buf = new StackBuffer<GUIVertex>(ARRAY, DYNAMIC);

		pushQuad(buf, {0, 0}, glm::ivec2(tilesInEachDirection * glm::vec2(tileSize)), sprite.pos, sprite.size, tileSize);

		buf->generateBuffer();

		GUIRenderObject * obj = new GUIRenderObject();
		obj->tex = tex;
		obj->buf = buf;
		obj->size = glm::ivec2(tilesInEachDirection * glm::vec2(tileSize));

		return obj;
	}

}
}


