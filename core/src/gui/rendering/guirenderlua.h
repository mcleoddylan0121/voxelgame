/*
 * guirenderlua.h
 *
 *  Created on: Jul 13, 2015
 *      Author: Daniel
 */

#ifndef GUIRENDERLUA_H_
#define GUIRENDERLUA_H_

#include "vluautil.h"
#include "guiutil.h"

namespace vox {

	struct GUIRenderObject;
	struct TextureAtlasEntry;

namespace gui {
	struct LuaQuadInfo : public LuaObject {
		enum Type : int {
			Quad, QuadFromAtlas, TiledQuad
		};

		std::string texName;
		int type;
		lvec2 size, tile, tilesInEachDirection;
		AtlasEntry sprite;

		LuaQuadInfo();

		GUIRenderObject * make();
	};


	LuaQuadInfo luaMakeQuad(std::string texName, lvec2 onScreenSize, lvec2 tile);
	LuaQuadInfo luaMakeQuadFromAtlas(std::string texName, AtlasEntry sprite, lvec2 onScreenSize, lvec2 tile);
	LuaQuadInfo luaMakeTiledQuad(std::string texName, AtlasEntry sprite, lvec2 tilesInEachDirection, lvec2 tileSize);

}

}

#endif /* GUIRENDERLUA_H_ */
