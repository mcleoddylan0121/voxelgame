
#include "guistate.h"
#include "guirender.h"
#include "glcontroller.h"
#include "program.h"
#include "resourcemanager.h"
#include "guiutil.h"
#include "component.h"
#include "sdlcontroller.h"
#include "guimanager.h"
#include "console.h"
#include "glbuffer.h"
#include "framebuffer.h"

#include "vfreetype.h"
#include "fontrender.h"
#include "worldstate.h"
#include "entitymanager.h"

#include "util.h"

#include <algorithm>
#include <cmath>

namespace vox {

	TextField *FPSCounter, *FPSCounter2;

	FontRenderObject* atlas; // used for debugging purposes

	Font* consolas;

	GUIState::GUIState() {
		fpsAnalytics = new FPSAnalytics();
		GUIShaderProgram* program = new GUIShaderProgram("guishader");
		TextShaderProgram* textProgram = new TextShaderProgram("font");

		renderHelper = new GUIRenderHelper(program, textProgram);

		_guiManager = new gui::GUIManager();

		consolas = mFontManager->makeFont("consolas");
		consolas->textShadow = true;
		consolas->shadowOffset = {2,1};

		FPSCounter = new TextField(800);
		FPSCounter2 = new TextField(mSDL->viewportWidth - 4);

		StackBuffer<Quad<FontVertex>>* buf = new StackBuffer<Quad<FontVertex>>(ARRAY, STATIC);

		atlas = new FontRenderObject();
		atlas->tex = mFontManager->loadedChars->framebuffer->target;
		atlas->vertexBuffer = buf;

		glm::i16vec2 ppos2 = {0,0};
		glm::i16vec2 texPos = {0,0};
		glm::i16vec2 size(mFontManager->loadedChars->framebuffer->size);
		glm::u8vec4 color = {255,255,255,255};

		buf->push(Quad<FontVertex>(
				{ppos2, {texPos.x, texPos.y + size.y}, color}, {{ppos2.x + size.x, ppos2.y}, texPos + size, color},
				{ppos2 + size, {texPos.x + size.x, texPos.y}, color}, {{ppos2.x, ppos2.y + size.y}, texPos, color}
		));
		buf->generateBuffer();
	}

	GUIState::~GUIState() {
		delete _guiManager;
		delete renderHelper;
	}

	WorldStateGLAnalytics wsAnalytics;
	Accumulator updateAnalytics(0.1f);

	void GUIState::render(FrameBuffer* target) {
		glEnable(GL_DEPTH_TEST);
		mGL->bindFrameBuffer(target);
		glClear(GL_DEPTH_BUFFER_BIT); // clear the depth buffer

		_guiManager->render(renderHelper);


		// TODO: move analytics display to a dedicated GUI component
		if(updateAnalytics.attempt()) {
			wsAnalytics = mGL->analytics.getWorldStateAnalytics(mWorldState->renderHandler);
			fpsAnalytics->recalculate();
		}

		float fps = mGameLoop.getFPS();

		// Recent FPS is probably outdated
		std::string recentFPS =  "FPS: " + toString(fps);
		std::string recentFT =   "Frame Time: " + toString(1000.f/fps).substr(0,5) + "ms";

		// Here come the advanced profiler metrics
		std::string averageFPS = "Average FPS: " + toString(fpsAnalytics->averageFPS);
		std::string averageFT =  "Average Frame Time: " + toString(fpsAnalytics->averageFrameTime).substr(0,5) + "ms";
		std::string bottom10FPS ="Worst 10% FPS: " + toString(fpsAnalytics->bottomTenAverageFPS);
		std::string bottom10FT = "Worst 10% Frame Time: " + toString(fpsAnalytics->bottomTenAverageFrameTime).substr(0,5) + "ms";
		std::string worstFT =    "Worst Frame Time: " + toString(fpsAnalytics->worstFrameTime).substr(0,5) + "ms";
		std::string worstFTEver ="Worst Frame Time Ever: " + toString(fpsAnalytics->worstFrameTimeEver).substr(0,5) + "ms";

		// May be slightly incorrect
		std::string vramString = "Graphics Memory Usage: " + toString(mGL->analytics.getMemoryUsage()/1024/1024) + "MB";

		// Only counts the number of triangles rendered, may be slightly incorrect
		std::string triangles =  "Triangles: " + toString(wsAnalytics.chunkTriangles + wsAnalytics.waterTriangles + wsAnalytics.entityTriangles + wsAnalytics.lodTriangles + wsAnalytics.numParticles * 2);

		// Air chunks are chunks with no vertices
		std::string airChunks =   "Air Chunks: " + toString(wsAnalytics.numChunks - wsAnalytics.nonEmptyChunks);
		std::string filledChunks ="Filled Chunks: " + toString(wsAnalytics.nonEmptyChunks);
		std::string renderedChunks = "Rendered Chunks: " + toString(wsAnalytics.renderedChunks);

		std::string entities =   "Entities: " + toString(wsAnalytics.numEntities);
		std::string joints =     "Joints: " + toString(wsAnalytics.numJoints);
		std::string particles =  "Particles: " + toString(wsAnalytics.numParticles);

		glm::vec3 pPos = mPlayer->player->bb.visual().getBottomCenter();
		pPos = glm::floor(pPos * 10.f) / 10.f;
		std::string playerPos = "Position: ("+toString(pPos.x)+","+toString(pPos.y)+","+toString(pPos.z)+")";

		// Optimally should never go above 1000
		std::string drawCalls =  "Draw Calls: " + toString(wsAnalytics.drawCalls);

		// If we have over 1k draw calls, there's something very wrong happening (this would eat fps)
		if(wsAnalytics.drawCalls > 1000) drawCalls += "\nToo many draw calls! Tell Dylan";

		std::string tot = "";
		std::string tot2 = "";
		std::string left[] = {
				recentFPS, recentFT, averageFPS, averageFT, bottom10FPS, bottom10FT, worstFT, worstFTEver, playerPos
		};

		std::string right[] = {
				vramString, triangles, airChunks, filledChunks, renderedChunks, entities, joints, particles, drawCalls
		};
		for(std::string s:left) tot += s + "\n";
		for(std::string s:right) tot2 += s + "\n";

		FPSCounter->setText(tot, consolas, {18,18}, {1,1,1,1});
		FPSCounter->setTopLeft({8,mSDL->viewportHeight}); // put in top left corner of screen

		FPSCounter2->setText(tot2, consolas, {18,18}, {1,1,1,1}, RIGHT);
		FPSCounter2->setTopLeft({0, mSDL->viewportHeight});

		if(analyticsEnabled) {
			FPSCounter->render(renderHelper);
			FPSCounter2->render(renderHelper);
		}

		// Renders the entire font atlas over the screen
		// mGL->renderText(testSSShader, atlas, {0,0}, {1,1}, glm::vec4(1,1,1,1));

		renderHelper->render(target);
	}

	void GUIState::tick(float delta) { updateAnalytics.accumulate(delta); fpsAnalytics->tick(delta); }

	template<typename T> T average(std::vector<T> in) {
		T size = (T) in.size();
		T total = T(0);
		for(int i = 0; i < in.size(); i++) {
			total += in[i];
		}
		total /= size;
		return total;
	}

	void FPSAnalytics::recalculate() {
		if((int) lastFewFrameTimes.size() < numFramesToStore) return; // THERE IS YET INSUFFICIENT DATA FOR A MEANINGFUL ANSWER
		else if((int) lastFewFrameTimes.size() > numFramesToStore) {
			lastFewFrameTimes.erase(lastFewFrameTimes.begin(), lastFewFrameTimes.begin()+(lastFewFrameTimes.size()-numFramesToStore));
		}

		std::vector<float> bottom10Percent = lastFewFrameTimes;
		std::sort(bottom10Percent.rbegin(),bottom10Percent.rend());
		bottom10Percent.erase(bottom10Percent.begin()+numFramesToStore/10,bottom10Percent.end());
		averageFrameTime = average(lastFewFrameTimes);
		bottomTenAverageFrameTime = average(bottom10Percent);
		worstFrameTime = bottom10Percent[0];
		worstFrameTimeEver = fmax(worstFrameTime,worstFrameTimeEver);

		averageFPS =          1000.f / averageFrameTime;
		bottomTenAverageFPS = 1000.f / bottomTenAverageFrameTime;
		worstFPS =            1000.f / worstFrameTime;
		worstFPSEver =        1000.f / worstFrameTimeEver;
	}

}
