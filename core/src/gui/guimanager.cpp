/*
 * guimanager.cpp
 *
 *  Created on: Jul 10, 2015
 *      Author: Daniel
 */

#include "guimanager.h"
#include "guirenderlua.h"
#include "resourcemanager.h"
#include "component.h"
#include "vlua.h"
#include "guilua.h"
#include "sdlcontroller.h"
#include "events.h"
#include "vfreetype.h"
#include "fontrender.h"
#include "luacontext.h"

#include <algorithm>

namespace vox {

namespace gui {

	GUIManager::GUIManager() {
		GUILibrary::_m = this;

		defaultFont = mFontManager->makeFont("consolas");
		defaultFont->textShadow = true;
		defaultFont->shadowOffset = {2,1};

		// Load and run the script.
		// Note, we do not use resourceManager here because loading the script requires it to reference itself.
		_handlerScript = mLua->getContext();
	}

	GUIManager::~GUIManager() {
		for (auto& f : _frames)
			delete f.second.comp;
	}

	std::unordered_map<ComponentID, ContainedComponent>::iterator GUIManager::_handleMouseRecurse(std::vector<ComponentID>& v, Point runningPos) {
		if (_grabCount > 0) {
			auto it = _frames.find(_grabbedID);
			if (it->second.comp->isVisible())
				return _frames.find(_grabbedID);
		}
		for (auto i = v.rbegin(); i != v.rend(); i++) {
			if (_frames[*i].comp->isVisible()) {
				auto ret = _frames.find(*i);
				auto goDeeper = _handleMouseRecurse(ret->second.comp->_children, runningPos - ret->second.pos);

				if (goDeeper != _frames.end()) return goDeeper;
				if (ret->second.comp->getBoundingBox(ret->second.pos).contains(runningPos) && ret->second.comp->_interactable) return ret;
			}
		}
		return _frames.end();
	}

	std::unordered_map<ComponentID, ContainedComponent>::iterator GUIManager::_handleMouseEvent(std::string func, Point pos, int button) {
		auto f = _handleMouseRecurse(_frameOrder, pos);
		for (auto& e : _globalListeners) {
			MouseEvent event = makeMouseEvent(pos, pos, button);
			if (e.functionExists[func]) _handlerScript->callVoidFunction(e.address + func, event);
		}

		if (mSDL->isMouseGrabbed()) return _frames.end();

		if (f != _frames.end()) {
			bringToFront(f->first);

			auto r = f;
			Point runningPos = {0, 0};
			do runningPos += r->second.pos;
			while (r->second.parent >= 0 && (r = _frames.find(r->second.parent), true));

			for (auto& e : f->second.comp->_listeners) {
				MouseEvent event = makeMouseEvent(pos, pos - runningPos, button);
				if (e.functionExists[func]) _handlerScript->callVoidFunction(e.address + func, event);
			}
		}

		return f;
	}
	void GUIManager::mousePressed(Point pos, int button) {
		auto it = _handleMouseEvent(".mousePressed", pos, button);
		if (it != _frames.end()) {
			_grabbedID = it->first;
			_grabCount++;
		}
	}
	void GUIManager::mouseReleased(Point pos, int button) {
		_handleMouseEvent(".mouseReleased", pos, button);
		if (_grabCount > 0) _grabCount--;

		auto f = _handleMouseRecurse(_frameOrder, pos);
		ComponentID newID = (f == _frames.end() ? ContainedComponent::UNDEFINED : f->first);
		if (_enteredID != newID) {
			if (_enteredID != ContainedComponent::UNDEFINED) {
				auto f2 = getFrame(_enteredID);
				for (auto& e : f2.comp->_listeners)
					if (e.functionExists[".mouseExited"]) _handlerScript->callVoidFunction(e.address + ".mouseExited");
			}

			if (newID != ContainedComponent::UNDEFINED)
				for (auto& e : f->second.comp->_listeners)
					if (e.functionExists[".mouseEntered"]) _handlerScript->callVoidFunction(e.address + ".mouseEntered");

			_enteredID = newID;
		}
	}
	void GUIManager::mouseDown(Point pos, int button) {
		_handleMouseEvent(".mouseDown", pos, button);
	}

	void GUIManager::_handleMouseMotionEvent(std::string func, Point delta) {
		Mouse m = mSDL->getMouse();
		Point pos = {m.x, m.y};

		auto f = _handleMouseRecurse(_frameOrder, pos);
		for (auto& e : _globalListeners) {
			MouseMotionEvent event = makeMouseMotionEvent(pos, pos, delta);
			if (e.functionExists[func]) _handlerScript->callVoidFunction(e.address + func, event);
		}

		if (mSDL->isMouseGrabbed()) return;

		ComponentID newID = (f == _frames.end() ? ContainedComponent::UNDEFINED : f->first);
		if (_enteredID != newID) {
			if (_enteredID != ContainedComponent::UNDEFINED) {
				auto f2 = getFrame(_enteredID);
				for (auto& e : f2.comp->_listeners)
					if (e.functionExists[".mouseExited"]) _handlerScript->callVoidFunction(e.address + ".mouseExited");
			}

			if (newID != ContainedComponent::UNDEFINED)
				for (auto& e : f->second.comp->_listeners)
					if (e.functionExists[".mouseEntered"]) _handlerScript->callVoidFunction(e.address + ".mouseEntered");

			_enteredID = newID;
		}

		if (f != _frames.end()) {
			auto r = f;
			Point runningPos = {0, 0};
			do runningPos += r->second.pos;
			while (r->second.parent >= 0 && (r = _frames.find(r->second.parent), true));

			for (auto& e : f->second.comp->_listeners) {
				MouseMotionEvent event = makeMouseMotionEvent(pos, pos - runningPos, delta);
				if (e.functionExists[func]) _handlerScript->callVoidFunction(e.address + func, event);
			}
		}
	}
	void GUIManager::mouseMoved(Point delta) {
		_handleMouseMotionEvent(".mouseMoved", delta);
	}

	void GUIManager::_handleKeyEvent(std::string func, int key) {
		auto f = _frames.find(_focusedID);
		for (auto& e : _globalListeners) {
			KeyEvent event = makeKeyEvent(key);
			if (e.functionExists[func]) _handlerScript->callVoidFunction(e.address + func, event);
		}

		if (mSDL->isMouseGrabbed()) return;

		if (f != _frames.end()) {
			for (auto& e : f->second.comp->_listeners) {
				KeyEvent event = makeKeyEvent(key);
				if (e.functionExists[func]) _handlerScript->callVoidFunction(e.address + func, event);
			}
		}

	}
	void GUIManager::keyPressed(int key) {
		_handleKeyEvent(".keyPressed", key);
	}
	void GUIManager::keyReleased(int key) {
		_handleKeyEvent(".keyReleased", key);
	}
	void GUIManager::keyDown(int key) {
		_handleKeyEvent(".keyDown", key);
	}

	void GUIManager::textTyped(std::string text) {
		if (_focusedID < 0) return;

		for (auto& e : _frames[_focusedID].comp->_listeners) {
			TextTypedEvent t;
			t.text = text;
			if (e.functionExists[".textTyped"]) _handlerScript->callVoidFunction(e.address + ".textTyped", t);
		}
	}

	KeyEvent GUIManager::makeKeyEvent(int button) {
		KeyEvent k;
		k.keyCode = button;
		return k;
	}
	MouseEvent GUIManager::makeMouseEvent(Point pos, Point relativePos, int button) {
		MouseEvent m;
		m.relativePos = relativePos;
		m.absolutePos = pos;
		m.button = button;
		return m;
	}
	MouseMotionEvent GUIManager::makeMouseMotionEvent(Point pos, Point relativePos, Point delta) {
		MouseMotionEvent m;
		m.relativePos = relativePos;
		m.absolutePos = pos;
		m.delta = delta;
		return m;
	}


	std::unordered_map<ComponentID, ContainedComponent>::iterator GUIManager::getActiveFrame() {
		for (auto i = _frameOrder.rbegin(); i != _frameOrder.rend(); i++) {
			if (_frames[*i].comp->isVisible() && _frames[*i].comp->_interactable) {
				auto ret = _frames.find(*i);
				bringToFront(*i);
				return ret;
			}
		}
		return _frames.end();
	}
	std::unordered_map<ComponentID, ContainedComponent>::iterator GUIManager::getFirstFrame(Point p) {
		for (auto i = _frameOrder.rbegin(); i != _frameOrder.rend(); i++) {
			if (_frames[*i].comp->getBoundingBox(_frames[*i].pos).contains(p) && _frames[*i].comp->isVisible() && _frames[*i].comp->_interactable) {
				auto ret = _frames.find(*i);
				bringToFront(*i);
				return ret;
			}
		}
		return _frames.end();
	}


	void GUIManager::bringToFront(ComponentID i, bool first) {

		// Call the focus lost/gained events
		bool keepGoing = first;
		if (keepGoing && _frames[i].comp->_interactable) {
			keepGoing = false;
			if (i != _focusedID) {
				if (_focusedID >= 0)
					for (auto& e : _frames[_focusedID].comp->_listeners)
						if (e.functionExists[".focusLost"]) _handlerScript->callVoidFunction(e.address + ".focusLost");
				_focusedID = i;

				for (auto& e : _frames[i].comp->_listeners)
					if (e.functionExists[".focusGained"]) _handlerScript->callVoidFunction(e.address + ".focusGained");
			}
		}

		if (_frameOrder.back() == i) return;

		// Recurse to find the window this comp belongs to
		auto it = std::find(_frameOrder.begin(), _frameOrder.end(), i);
		if (it == _frameOrder.end() && _frames[i].parent >= 0) {
			bringToFront(_frames[i].parent, keepGoing);
			return;
		}
		_grabCount = 0;
		_frameOrder.erase(it);
		_frameOrder.push_back(i);
	}

	void GUIManager::_recursiveRender(GUIRenderHelper * r, std::vector<ComponentID>& comps, Point runningPos) {
		for (auto& f : comps) {
			auto& frame = _frames[f];
			if (!frame.comp->isVisible()) continue;

			Point newRunningPos = runningPos + frame.pos;

			r->render(GUIDisplayObject(frame.comp->_img, newRunningPos + frame.comp->_factory.getOffs(), frame.comp->_scale));
			r->render(GUIDisplayObject(frame.comp->_frameRender, newRunningPos, {1, 1}));
			frame.comp->_textField->setFirstRowBaseline(newRunningPos + frame.comp->_textOffs);
			frame.comp->_textField->render(r);

			_recursiveRender(r, frame.comp->_children, newRunningPos);
		}
	}

	void GUIManager::render(GUIRenderHelper * r) {
		_recursiveRender(r, _frameOrder, {0, 0});
	}

	ContainedComponent& GUIManager::getFrame(ComponentID i) {
		return _frames[i];
	}

	GUIRenderObject * GUIManager::makeComponentRenderObject(std::string componentAddress, std::string name) {
		LuaQuadInfo quad = _handlerScript->get<LuaQuadInfo>(componentAddress + ".skin." + name);
		return quad.make();
	}

	FrameFactory GUIManager::getFrameFactory(std::string componentAddress) {
		return _handlerScript->get<FrameFactory>(componentAddress + ".skin.frameFactory");
	}
} // namespace gui

} // namespace vox

