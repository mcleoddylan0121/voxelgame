/*
 * guimanager.h
 *
 *  Created on: Jul 10, 2015
 *      Author: Daniel
 */

#ifndef GUIMANAGER_H_
#define GUIMANAGER_H_

#include "filesystem.h"
#include "guirender.h"
#include "guiutil.h"
#include "events.h"

#include <string>
#include <vector>

namespace vox {

	struct LuaScript;
	struct Font;
	template<typename T, Asset::Type> struct AllocatorTemplate;

namespace gui {

	struct Component;
	struct GUILibrary;

	struct ContainedComponent {
		enum ParentID : int {
			UNDEFINED = -2,
			GLOBAL = -1
		};

		Point pos;
		Component * comp;
		ComponentID parent = UNDEFINED;
	};

	struct GUIManager {
		friend class GUILibrary;

		GUIManager();
		virtual ~GUIManager();

		void render(GUIRenderHelper * r);

		void mousePressed(Point pos, int button);
		void mouseReleased(Point pos, int button);
		void mouseDown(Point pos, int button);

		void mouseMoved(Point delta);

		void keyPressed(int key);
		void keyReleased(int key);
		void keyDown(int key);

		void textTyped(std::string text);

		ContainedComponent& getFrame(ComponentID i);
		std::unordered_map<ComponentID, ContainedComponent>::iterator getActiveFrame();
		std::unordered_map<ComponentID, ContainedComponent>::iterator getFirstFrame(Point p);
		void bringToFront(ComponentID i, bool first = true);

		GUIRenderObject * makeComponentRenderObject(std::string componentAddress, std::string name);
		FrameFactory getFrameFactory(std::string componentAddress);

		Font * defaultFont = nullptr;

	private:

		KeyEvent makeKeyEvent(int button);
		MouseEvent makeMouseEvent(Point pos, Point relativePos, int button);
		MouseMotionEvent makeMouseMotionEvent(Point pos, Point relativePos, Point delta);

		void _recursiveRender(GUIRenderHelper * r, std::vector<ComponentID>& comps, Point runningPos);

		std::unordered_map<ComponentID, ContainedComponent>::iterator _handleMouseRecurse(std::vector<ComponentID>& comps, Point pos);
		std::unordered_map<ComponentID, ContainedComponent>::iterator _handleMouseEvent(std::string func, Point pos, int button);
		void _handleMouseMotionEvent(std::string func, Point delta);

		void _handleKeyEvent(std::string func, int key);

		int _grabCount = 0;
		ComponentID _grabbedID = ContainedComponent::UNDEFINED;
		ComponentID _enteredID = ContainedComponent::UNDEFINED;
		ComponentID _focusedID = ContainedComponent::UNDEFINED;

		LuaScript * _handlerScript;

		std::unordered_map<ComponentID, ContainedComponent> _frames;
		std::vector<ComponentID> _frameOrder;

		std::vector<EventListener> _globalListeners;
	};

} // namespace gui

} // namespace vox



#endif /* GUIMANAGER_H_ */
