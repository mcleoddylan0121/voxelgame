/*
 * guistate.h
 *
 *  Created on: Jul 10, 2015
 *      Author: Dylan
 */

#ifndef GUISTATE_H_
#define GUISTATE_H_

#include "gameloop.h"
#include <vector>

namespace vox {

	class DefaultGUIRenderer;
	class GUIShaderProgram;
	class DefaultTextRenderer;
	class GUIRenderHelper;

	class FPSAnalytics;

	namespace gui {
		struct GUIManager;
	}

	class GUIState: public GameState {
	public:
		GUIRenderHelper* renderHelper;
		GUIState();
		virtual ~GUIState();
		void tick(float delta);
		void render(FrameBuffer* target);
		bool analyticsEnabled = true;

	private:
		gui::GUIManager * _guiManager;
		FPSAnalytics * fpsAnalytics;
	};

	class FPSAnalytics {
	public:
		void recalculate();
		int numFramesToStore = 500; // how many frames we store
		std::vector<float> lastFewFrameTimes;
		void tick(float delta) {
			lastFewFrameTimes.insert(lastFewFrameTimes.end(),mGameLoop.aaaaTempDelta.begin(),mGameLoop.aaaaTempDelta.end());
			mGameLoop.aaaaTempDelta.clear();
		}
		float averageFPS = 0.f, averageFrameTime = 0.f;
		float bottomTenAverageFPS = 0.f, bottomTenAverageFrameTime = 0.f;
		float worstFrameTime = 0.f, worstFPS = 0.f;
		float worstFrameTimeEver = 0.f, worstFPSEver = 0.f;
	};

} // namespace vox



#endif /* GUISTATE_H_ */
