/*
 * guilua.h
 *
 *  Created on: Jul 14, 2015
 *      Author: Daniel
 */

#ifndef GUILUA_H_
#define GUILUA_H_

#include "vluautil.h"
#include "guiutil.h"

#include <string>

namespace vox {

namespace gui {

	struct GUIManager;
	struct EventListener;

	struct GUILibrary {
		static ComponentID createComponent(std::string address, lvec2 size);
		static void makeWindow(ComponentID i);

		static void setVisible(ComponentID i, bool b);
		static void setSize(ComponentID i, lvec2 size);
		static void addListener(ComponentID i, std::string e);
		static void updateSkin(ComponentID i);
		static void setPos(ComponentID i, lvec2 pos);
		static void setParent(ComponentID i, ComponentID parent);
		static void setHue(ComponentID i, lvec4 hue);
		static void setText(ComponentID i, std::string text);
		static void setTextPos(ComponentID i, lvec2 pos);
		static void setInteractable(ComponentID i, bool b);

		static void setTextMode(bool b);

		static lvec2 getTextSize(ComponentID i);

		static void bringToFront(ComponentID i);

		static void addGlobalListener(std::string e);
		static void setMouseGrabbed(bool b);

		static void exitGame();

		static GUIManager * _m;

	private:
		static void removeParents(ComponentID i);
		static EventListener _makeListener(std::string);
	};
}

}

#endif /* GUILUA_H_ */
