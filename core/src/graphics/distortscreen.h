/*
 * distortscreen.h
 *
 *  Created on: Jul 25, 2016
 *      Author: Dylan
 */

#ifndef SRC_GRAPHICS_DISTORTSCREEN_H_
#define SRC_GRAPHICS_DISTORTSCREEN_H_
#include "vglm.h"
namespace vox {

	class ColorBuffer;
	class RenderTextureProgram;
	class FrameBuffer;

	// Handles screen distortions -- We add distortions to the distortion texture, then it's used to distort the screen
	class ScreenDistortionHandler {
	private:
		ColorBuffer *distortionTextureFrameBuffer;
		RenderTextureProgram* program;
	public:
		void beginDistortionTextureRender(); // bind framebuffer, set blend mode
		void renderDistortions(ColorBuffer* in, FrameBuffer* out);
		void clearDistortions();

		ScreenDistortionHandler(glm::ivec2 size);
	};



}

#endif /* SRC_GRAPHICS_DISTORTSCREEN_H_ */
