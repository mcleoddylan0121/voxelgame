/*
 * particle.cpp
 *
 *  Created on: Sep 7, 2015
 *      Author: Dylan
 */

#include "particle.h"
#include "glcontroller.h"

#include <algorithm>

namespace vox {

	GLBuffer<glm::vec2>* getQuadBuffer() {
		StackBuffer<glm::vec2>* quad = new StackBuffer<glm::vec2>(ARRAY, STATIC);
		quad->push({glm::vec2(0,0), glm::vec2(0,1), glm::vec2(1,0), glm::vec2(1,1)});
		quad->generateBuffer();
		return quad;
	}

	ParticleManager::ParticleManager(Texture* billboardTexture): billboardTexture(billboardTexture) {}

	InstancedParticleManager::InstancedParticleManager(Texture* billboardTexture): ParticleManager(billboardTexture) {
		quadBuffer = getQuadBuffer();
		streamedBillboardBuffer = new ArrayBuffer<InstancedParticleVertex>(ARRAY, STREAM, MAX_PARTICLES, 2, true);
		streamedBillboardBuffer->generateBuffer();
		billboardShader = new InstancedParticleShaderProgram("particles", false, {"INSTANCED"});
	}

	InstancedCloudParticleManager::InstancedCloudParticleManager(Texture* billboardTexture): CloudParticleManager(billboardTexture) {
		quadBuffer = getQuadBuffer();
		streamedBillboardBuffer = new ArrayBuffer<InstancedCloudParticleVertex>(ARRAY, STREAM, MAX_PARTICLES, 2, true);
		streamedBillboardBuffer->generateBuffer();
		billboardShader = new InstancedCloudParticleShaderProgram("particles", false, {"INSTANCED", "CLOUD"});
	}

	void InstancedParticleManager::prepareForRender(WorldCoordinate cameraPos) {
		std::sort(billboards.begin(), billboards.end(), BillboardSort<BillboardDisplayObject>{cameraPos});

		streamedBillboardBuffer->resizeArray(std::min(MAX_PARTICLES, (int)billboards.size()));
		for(int i = 0; i < (int) billboards.size() && i < MAX_PARTICLES; i++) {
			InstancedParticleVertex vert{billboards[i].pos, glm::u16vec2(billboards[i].UVstart*65535.f), glm::u16vec2(billboards[i].UVsize*65535.f), glm::u8vec4(billboards[i].color*255.f), billboards[i].size};
			streamedBillboardBuffer->set(i, vert);
		}
		streamedBillboardBuffer->streamData();

		billboardCount = billboards.size();
		meshCount = meshes.size();
		billboards.clear();
	}

	void InstancedParticleManager::renderBillboards(const Camera& cam) {
		mGL->bindProgram(billboardShader);
		billboardShader->beginRender();

		mGL->bindTexture(billboardTexture, 0);
		glUniform1i(billboardShader->tex, 0);

		uploadCameraMatrices(billboardShader, &cam);


		quadBuffer->bindBuffer();
		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, (GLvoid*)0);

		streamedBillboardBuffer->bindBuffer();
		glVertexAttribPointer(1, 3, GL_FLOAT,          GL_FALSE, sizeof(InstancedParticleVertex), (GLvoid*) offsetof(InstancedParticleVertex, pos));
		glVertexAttribPointer(2, 2, GL_UNSIGNED_SHORT, GL_TRUE,  sizeof(InstancedParticleVertex), (GLvoid*) offsetof(InstancedParticleVertex, UVstart));
		glVertexAttribPointer(3, 2, GL_UNSIGNED_SHORT, GL_TRUE,  sizeof(InstancedParticleVertex), (GLvoid*) offsetof(InstancedParticleVertex, UVsize));
		glVertexAttribPointer(4, 4, GL_UNSIGNED_BYTE,  GL_TRUE,  sizeof(InstancedParticleVertex), (GLvoid*) offsetof(InstancedParticleVertex, color));
		glVertexAttribPointer(5, 2, GL_FLOAT,          GL_FALSE, sizeof(InstancedParticleVertex), (GLvoid*) offsetof(InstancedParticleVertex, scale));

		glVertexAttribDivisor(0, 0); // reuse for every quad
		glVertexAttribDivisor(1, 1);
		glVertexAttribDivisor(2, 1);
		glVertexAttribDivisor(3, 1);
		glVertexAttribDivisor(4, 1);
		glVertexAttribDivisor(5, 1);

		glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0, 4, streamedBillboardBuffer->size);

		glVertexAttribDivisor(0, 0);
		glVertexAttribDivisor(1, 0);
		glVertexAttribDivisor(2, 0);
		glVertexAttribDivisor(3, 0);
		glVertexAttribDivisor(4, 0);
		glVertexAttribDivisor(5, 0);

		billboardShader->endRender();
	}

	void InstancedCloudParticleManager::prepareForRender(WorldCoordinate cameraPos) {
		streamedBillboardBuffer->resizeArray(std::min(MAX_PARTICLES, (int)cloudParticles.size()));
		for(int i = 0; i < (int) cloudParticles.size() && i < MAX_PARTICLES; i++) {
			InstancedCloudParticleVertex vert{cloudParticles[i].pos, (uint8_t) cloudParticles[i].textureIndex};
			streamedBillboardBuffer->set(i, vert);
		}
		streamedBillboardBuffer->streamData();

		// cloudParticles.clear();
	}

	void InstancedCloudParticleManager::render(const Camera& cam) {
		mGL->bindProgram(billboardShader);
		billboardShader->beginRender();

		mGL->bindTexture(billboardTexture, 0);
		glUniform1i(billboardShader->tex, 0);

		uploadCameraMatrices(billboardShader, &cam);

		glUniform4f(billboardShader->color, 1.f, 1.f, 1.f, 0.04f);
		glUniform2f(billboardShader->UVsize, 1.f, 1.f/3.f);
		glUniform2f(billboardShader->UVgap, 0.f, 1.f/3.f);
		glUniform1f(billboardShader->time, 0.f);
		glUniform2f(billboardShader->scale, 2500.f / sqrt(cam.aspectRatio), 2500.f / sqrt(cam.aspectRatio));
		glUniform1f(billboardShader->aspectRatio, cam.aspectRatio);
		glUniform3fv(billboardShader->camPos, 1, &(cam.pos[0]));

		glm::vec2 camLookXZ = glm::vec2(cam.lookAt.x, cam.lookAt.z) - glm::vec2(cam.pos.x, cam.pos.z);
		camLookXZ = glm::normalize(camLookXZ);

		glm::mat2 camLookXZRotationMatrix = // make rotation matrix of camera's lookDir
			glm::mat2(camLookXZ.x, -camLookXZ.y,
			          camLookXZ.y,  camLookXZ.x);

		glUniformMatrix2fv(billboardShader->camLookXZRotationMatrix, 1, GL_FALSE, &(camLookXZRotationMatrix[0][0]));

		quadBuffer->bindBuffer();
		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, (GLvoid*)0);

		streamedBillboardBuffer->bindBuffer();
		glVertexAttribPointer(1, 3, GL_FLOAT,          GL_FALSE, sizeof(InstancedCloudParticleVertex), (GLvoid*) offsetof(InstancedCloudParticleVertex, pos));
		glVertexAttribPointer(2, 1, GL_UNSIGNED_BYTE,  GL_FALSE, sizeof(InstancedCloudParticleVertex), (GLvoid*) offsetof(InstancedCloudParticleVertex, textureIndex));

		glVertexAttribDivisor(0, 0); // reuse for every quad
		glVertexAttribDivisor(1, 1);
		glVertexAttribDivisor(2, 1);

		glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0, 4, streamedBillboardBuffer->size);

		glVertexAttribDivisor(0, 0);
		glVertexAttribDivisor(1, 0);
		glVertexAttribDivisor(2, 0);


		billboardShader->endRender();
	}

	void ParticleManager::renderMeshes(EntityMeshRenderer* render) {
		for(auto&& i:meshes)
			render->render(i);
		meshes.clear();
	}
}
