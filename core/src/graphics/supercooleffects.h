/*
 * supercooleffects.h
 *
 *  Created on: Jul 25, 2016
 *      Author: Dylan
 */

#ifndef SRC_GRAPHICS_SUPERCOOLEFFECTS_H_
#define SRC_GRAPHICS_SUPERCOOLEFFECTS_H_

namespace vox {

	class RenderTextureProgram;
	class ScreenDistortionHandler;
	class DeferredRenderer;

	class RollingWaveDistortionEffectHandler {
	private:
		RenderTextureProgram* shader;
	public:
		void tick(float delta);
		void render(ScreenDistortionHandler* handler, DeferredRenderer* renderer, float worldTime);
		RollingWaveDistortionEffectHandler();
	};

}


#endif /* SRC_GRAPHICS_SUPERCOOLEFFECTS_H_ */
