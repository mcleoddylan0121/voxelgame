/*
 * glanalytics.cpp
 *
 *  Created on: Jan 10, 2016
 *      Author: Dylan
 */

#include "glanalytics.h"
#include "entity.h"
#include "chunk.h"
#include "worldstaterender.h"
#include "worldstate.h"
#include "particle.h"
#include "sky.h"
#include "entitymanager.h"

namespace vox {


	class GLAnalyticsEntityRenderer: public EntityMeshRenderer {
	public:
		GLAnalytics* parent;
		GLAnalyticsEntityRenderer(GLAnalytics* parent): parent(parent) {}

		void render(EntityMeshDisplayObject e) {
			parent->_curEntityMeshTriangles += parent->_getTriangleCount(e.buf);
			parent->_curJoints++;
		}
	};

	GLAnalytics::GLAnalytics() {
		entityAnalyticsRenderer = new GLAnalyticsEntityRenderer(this);
		memCount = 0;
	}

	int GLAnalytics::getTriangleCount(Chunk* ch) {
		return (ch->mesh->buf->chunkIndexBuffer->size)/3;
	}

	int GLAnalytics::getWaterTriangleCount(Chunk* ch) {
		return 2*(ch->mesh->buf->wBuffer->size);
	}

	int GLAnalytics::_getTriangleCount(EntityMeshBuffer* en) {
		return 2*(en->buffer->size);
	}



	// The difficult one
	int GLAnalytics::getTriangleCount(Entity* en) {
		en->render(entityAnalyticsRenderer);

		int temp = _curEntityMeshTriangles;
		_curEntityMeshTriangles = 0;

		_curJoints = 0;

		return temp;
	}

	WorldStateGLAnalytics GLAnalytics::getWorldStateAnalytics(WorldStateRenderHandler* world) {
		WorldStateGLAnalytics ret;

		for(auto it = world->parent->map->begin(); it != world->parent->map->end(); it++) {
			Chunk* ch = it->second;
			if(world->parent->map->isLoadedYet(ch)) {
				ret.numChunks++;
				if(ch->mesh->buf->chunkIndexBuffer->size > 0) ret.nonEmptyChunks++;

			}
		}

		for(auto it = world->parent->map->_depthSort.depthSortedChunks.begin(); it != world->parent->map->_depthSort.depthSortedChunks.end(); it++) {
			Chunk* ch = *it;
			if(world->parent->map->isLoadedYet(ch)) {
				ret.renderedChunks++;
				ret.chunkTriangles += getTriangleCount(ch);
				ret.waterTriangles += getWaterTriangleCount(ch);
			}
		}

		// TODO: Finish entity GL Analytics
		EntityManager* e = world->parent->entityManager;
		e->render(entityAnalyticsRenderer);

		ret.entityTriangles = _curEntityMeshTriangles;
		_curEntityMeshTriangles = 0;
		ret.numJoints = _curJoints;
		_curJoints = 0;
		ret.numEntities = mWorldState->entityManager->countEntities();

		ParticleManager* pmanager1 = world->particleManager;
		CloudParticleManager* pmanager2 = world->sky->cloudParticleManager;

		ret.numParticles = pmanager1->billboardCount + pmanager1->meshCount + pmanager2->cloudParticles.size();

		ret.drawCalls = ret.renderedChunks * 2 + ret.numJoints + ret.numLoD + (int)pmanager1->meshes.size();

		return ret;
	}


}


