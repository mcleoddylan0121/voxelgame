/*
 * blur.cpp
 *
 *  Created on: Jun 23, 2016
 *      Author: Dylan
 */
#include "blur.h"
#include "glcontroller.h"
#include "program.h"
namespace vox {

	std::string numberHelpers[] = {"","float","vec2","vec3","vec4"};

	BlurHandler::BlurHandler(glm::ivec2 size, GLenum colorformat, int colorChannels, bool makeNew, std::string shader): makeNew(makeNew) {
		intermediate = new ColorBuffer(size,colorformat,false);
		if(makeNew) returnBuf = new ColorBuffer(size,colorformat,false);
		else        returnBuf = nullptr;
		channels = colorChannels;

		std::string colorChannelDefine = numberHelpers[channels] + " returnType";

		horiBlur = new RenderTextureProgram(shader, false, {"BLUR_HORIZONTAL", "BLUR"});
		vertBlur = new RenderTextureProgram(shader, false, {"BLUR_VERTICAL", "BLUR"}  );
	}

	void blurHelp(RenderTextureProgram* i, FrameBuffer* f, Texture* t, GLenum drawBuffer) {
		mGL->bindFrameBuffer(f);
		glDrawBuffer(drawBuffer);
		mGL->bindProgram(i);
		i->beginRender();
		mGL->render(i,t);
		i->endRender();
	}

	ColorBuffer* BlurHandler::blur(FrameBuffer* in, int targetComp) {
		glDisable(GL_DEPTH_TEST);
		glBlendFunc(GL_ONE,GL_ZERO);
		FrameBuffer* ret = makeNew? returnBuf:in;
		blurHelp(horiBlur, intermediate, in->target+targetComp, GL_COLOR_ATTACHMENT0);
		blurHelp(vertBlur, ret, intermediate->target, GL_COLOR_ATTACHMENT0);
		glEnable(GL_DEPTH_TEST);
		glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
		return (ColorBuffer*) ret;
	}

}
