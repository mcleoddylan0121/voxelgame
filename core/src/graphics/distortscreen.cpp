/*
 * distortscreen.cpp
 *
 *  Created on: Jul 25, 2016
 *      Author: Dylan
 */

#include "distortscreen.h"
#include "framebuffer.h"
#include "program.h"

namespace vox {

	ScreenDistortionHandler::ScreenDistortionHandler(glm::ivec2 size) {
		distortionTextureFrameBuffer = new ColorBuffer(size, GL_RGBA16F, false);
		program = new RenderTextureProgram("screenshader", false, {"DISTORT"});
	}


	void ScreenDistortionHandler::beginDistortionTextureRender() {
		mGL->bindFrameBuffer(distortionTextureFrameBuffer);
		glBlendFunc(GL_ONE,GL_ONE);
	}

	void ScreenDistortionHandler::renderDistortions(ColorBuffer* in, FrameBuffer* out) {
		glDisable(GL_DEPTH_TEST);

		mGL->bindProgram(program);
		mGL->bindTexture(distortionTextureFrameBuffer->target,1,program->getUniformLocation("distortionTexture"));

		mGL->bindFrameBuffer(out);
		program->beginRender();
		mGL->render(program,in->target);
		program->endRender();

		glEnable(GL_DEPTH_TEST);
	}


	void ScreenDistortionHandler::clearDistortions() {
		mGL->bindFrameBuffer(distortionTextureFrameBuffer);
		distortionTextureFrameBuffer->clear(glm::vec4(0,0,0,1));

	}


}


