/*
 * framebuffer.h
 *
 *  Created on: Jul 16, 2015
 *      Author: Dylan
 */

#ifndef FRAMEBUFFER_H_
#define FRAMEBUFFER_H_

#include "glcontroller.h"
#include <cstdint>
#include <vector>
namespace vox {

	class FrameBuffer {
	private:
		FrameBuffer();
	protected:
		FrameBuffer(glm::ivec2 size, int outputs);
	public:
		glm::ivec2 size;
		glm::vec2 pixelSize = {1,1};
		int outputs;
		glm::vec2 viewportStart, viewportSize;
		GLuint name;
		Texture* target; // Target texture to render to
		virtual ~FrameBuffer();
		virtual void clear(glm::vec4 color = {0,0,0,0});
		static FrameBuffer* getWindow();
		void initTextures();

		int64_t memUsage = 0;
	};

	class ColorBuffer: public FrameBuffer {
	private:
		GLuint depthBuffer;
		bool enableDepthBuffer;
	public:
		ColorBuffer(glm::ivec2 size, GLenum colorformat = GL_RGBA8, bool enableDepthBuffer = true, GLenum minFilter = GL_LINEAR, GLenum magFilter = GL_LINEAR,
				bool multisample = false, int MSAALevel = 1);
		virtual ~ColorBuffer();
	};

	class CubeMapColorBuffer: public FrameBuffer {
	public:
		CubeMapColorBuffer(glm::ivec2 size);
		virtual ~CubeMapColorBuffer();
		void selectFace(int face);
		void clear();
	};

	class DepthBuffer: public FrameBuffer {
	public:
		DepthBuffer(glm::ivec2 size);
		void clear();
		virtual ~DepthBuffer();
	};

	class CascadedShadowMap: public FrameBuffer {
	public:
		int cascades;
		CascadedShadowMap(glm::ivec2 size, int cascades);
		void clear();
		void selectCascade(int cascade);
		virtual ~CascadedShadowMap();
	};

	class DeferredRenderer: public FrameBuffer {
	public:
		GLuint depthBuffer;
		static const int COLOR = 0, G_BUFFER = 1, NORMALS = 2, LIGHT = 3;
		static const int COLOR_ATTACH = GL_COLOR_ATTACHMENT0, LIGHT_ATTACH = GL_COLOR_ATTACHMENT2, NORMAL_ATTACH = GL_COLOR_ATTACHMENT1, G_BUFFER_ATTACH = GL_COLOR_ATTACHMENT3;
		DeferredRenderer(glm::ivec2 size, bool multisample = true, int MSAALevel = 4);
		virtual ~DeferredRenderer() {}
		void clear();

		/*
		 * Use something such as {COLOR, NORMALS, LIGHT}
		 * Order matters! must be the same order as specified in the program!
		 * Do not use DEPTH. that's written to automatically           _         _
		 * Always use normals, because that's used as a stencil buffer  \_(.'/)_/
		 */
		void setDrawBuffers(std::vector<int>);
	};

}



#endif /* FRAMEBUFFER_H_ */
