/*
 * liquidmanager.cpp
 *
 *  Created on: Nov 20, 2015
 *      Author: Dylan
 */
#include "liquidmanager.h"
#include "program.h"
#include "glcontroller.h"
#include "renderutils.h"
#include "framebuffer.h"
#include "glbuffer.h"
#include "util.h"
#include "resourcemanager.h"

namespace vox {

	// ignore us, for we are temporary
	int tttt = 0;
	int tmmmm = 8;

	std::unordered_map<std::string, int> liquidRenderingTypeMap = {
			{ "solid",      SOLID },
			{ "textured",   TEXTURED },
			{ "procedural", PROCEDURAL }
	};

	std::string liquidRenderingTypeDefines[3] = {
			"SOLID", "TEXTURED", "PROCEDURAL"
	};


	LiquidManager::LiquidManager(int renderType): renderType(renderType) {
		liquidVertexBuffer = new StackBuffer<glm::vec2>(ARRAY, STATIC);
		((StackBuffer<glm::vec2>*) liquidVertexBuffer)->push({
			{0,0}, {0,1}, {1,1}, {1,1}, {1,0}, {0,0}
		});
		liquidVertexBuffer->generateBuffer();

		addLiquid
		(0, "water", Color::GPUColor(0,0,255,255),
			{"vec4 colorFunction(float time, vec2 UV) { return Perlin4D(getTiledNoisePos3D(UV, vec2(1), vec2(2), time, 3)) * vec4(0,0.4,0.4,0)+vec4(0,0.6,0.6,0.8); }"}
		);

		if(renderType == PROCEDURAL || renderType == TEXTURED) {
			for(int i = 0; i < 3; i++) {
				frames[i] = new ColorBuffer(glm::ivec2(noiseIndividualTextureSize * noiseTextureSize), GL_RGBA8, false, GL_NEAREST, GL_NEAREST);
			}
			for(int i = 0; i < tmmmm*3; i++) {
				tick(1.f);
				renderToFrameBuffer();
			}
		}
	}

	LiquidManager::~LiquidManager() {
		for(auto it = idTable.begin(); it != idTable.end(); it++) { // loop over all liquids
			delete it->second;
		}
		delete liquidVertexBuffer;
		if(renderType == PROCEDURAL || renderType == TEXTURED) {
			for(int i = 0; i < 3; i++) {
				delete frames[i];
			}
		}

	}

	float time2 = 0; // TODO: C'mon, man, standardize this already...
	Accumulator liquidManagerAccumulator(1.f/60.f);

	void LiquidManager::beginRendering(size_t bind1, size_t bind2, LiquidShaderProgram* p) {
		if(renderType == PROCEDURAL || renderType == TEXTURED) {
			mGL->bindTexture(liquidTextures[0], bind1);
			mGL->bindTexture(liquidTextures[1], bind2);
			glUniform1i(p->tex1, bind1);
			glUniform1i(p->tex2, bind2);

			float timeDiff = snapshots[1].time - snapshots[0].time;
			if(abs(timeDiff) < 0.001) {
				timeDiff = 0;
			} else {
				float nTime = time2 - timeDiff;
				timeDiff = (nTime - snapshots[0].time) / timeDiff;
			}

			glUniform1f(p->timeDiff, timeDiff);
		}
	}

	void LiquidManager::tick(float delta) {
		liquidManagerAccumulator.accumulate(delta); // we only want this to happen 60x/sec
		time2 += delta; // TODO: seriously...
	}

	void LiquidManager::renderLiquidToFrameBuffer(Liquid* l) {
		if(renderType == PROCEDURAL) {
			mGL->bindProgram(l->glslShader);

			glUniform1f(l->glslShader->_time_, snapshots[2].time);
			glUniform2fv(l->glslShader->UVstart, 1, &(l->UV[0]));
			glm::vec2 UVSize = glm::vec2(1.f/(float)(noiseTextureSize));
			glUniform2fv(l->glslShader->UVsize, 1, &(UVSize[0]));

			l->glslShader->beginRender();
			liquidVertexBuffer->bindBuffer();
			glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(glm::vec2), (void*) 0);
			glDrawArrays(GL_TRIANGLES, 0, liquidVertexBuffer->size);
			l->glslShader->endRender();
		} else if(renderType == TEXTURED) {
			mGL->bindProgram(mGL->screenProgram);
			glm::vec2 UVstart = glm::vec2(0, ((float)l->curTextureFrame)/l->numTextureFrames);
			glm::vec2 UVsize = glm::vec2(1, 1.f/l->numTextureFrames);

			l->curTextureFrame = (l->curTextureFrame + 1) % (l->numTextureFrames);

			mGL->screenProgram->beginRender();
			mGL->render(mGL->screenProgram, l->tex, l->UV, glm::vec2(1.f/(float)(noiseTextureSize)), 1, 0, glm::vec4(1), UVstart, UVsize);
			mGL->screenProgram->endRender();
		}
	}

	void LiquidManager::renderToFrameBuffer() {
		if(!liquidManagerAccumulator.attempt()) return;

		if(renderType == PROCEDURAL || renderType == TEXTURED) {

			glDisable(GL_DEPTH_TEST);
			glBlendFunc(GL_ONE, GL_ZERO);
			glDisable(GL_CULL_FACE);
			// we render to the future framebuffer, then we cycle him to the present once he's done
			mGL->bindFrameBuffer(frames[2]);
			if((tttt++)%tmmmm==0) { // only do this once every 'tmmmm' ticks -- extremely temporary
				for(auto it = idTable.begin(); it != idTable.end(); it++) { // loop over all liquids
					renderLiquidToFrameBuffer(it->second);
				}

				// present becomes past, future becomes present, reset the past and set it to the future
				FrameBuffer* temp = frames[0];
				frames[0] = frames[1];
				frames[1] = frames[2];
				frames[2] = temp;
				snapshots[0] = snapshots[1];
				snapshots[1] = snapshots[2];
				snapshots[1].time = time2;
				snapshots[2] = LiquidFrameSnapshot{time2};
				for(int i = 0; i < 3; i++)
					liquidTextures[i] = frames[i]->target;
			}
			glEnable(GL_DEPTH_TEST);
			glEnable(GL_CULL_FACE);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		}
	}

	Quad<glm::u16vec2> LiquidManager::getLiquidVertexData(LiquidID l, glm::vec2 UVstart, glm::vec2 UVsize) {
		Liquid* liq = getLiquid(l);

		glm::u16vec2 st = glm::u16vec2(liq->UV * 65535.f) + glm::u16vec2(65535 / noiseTextureSize * UVstart.x, 65535 / noiseTextureSize * UVstart.y);
		return Quad<glm::u16vec2>(
			st,
			st + glm::u16vec2(0, 65535 / noiseTextureSize * UVsize.y),
			st + glm::u16vec2(65535 / noiseTextureSize * UVsize.x, 65535 / noiseTextureSize * UVsize.y),
			st + glm::u16vec2(65535 / noiseTextureSize * UVsize.x, 0)
		);
	}

	Liquid* LiquidManager::addLiquid(LiquidID ID, std::string name, Color::GPUColor color, std::vector<std::string> glslFunction) {
		RenderToLiquidShaderProgram* shader = new RenderToLiquidShaderProgram("rendertoliquid", false, {}, {"noise_lib"}, glslFunction);
		Texture* tex = mRes->getResource<Texture, Asset::Texture>(name);
		int numTextureFrames = tex->size.y / tex->size.x;
		Liquid* ret = new Liquid(ID, name, color, glslFunction, shader, {0,0}, tex, numTextureFrames);
		nameMap[name] = ret;
		idTable[ID]   = ret;
		return ret;
	}

	// TODO: figure out how to make this function better
	void LiquidManager::removeLiquid(LiquidID ID) {
		delete idTable[ID];
	}

	Liquid::~Liquid() {
		delete glslShader;
	}

}



