/*
 * renderutils.cpp
 *
 *  Created on: Jul 16, 2015
 *      Author: Dylan
 */

#include "renderutils.h"
#include "glcontroller.h"
#include "io.h"
#include "voxelmesh.h"

#include <cmath>

namespace vox {

	Texture* Texture::allocate(const Asset& a) {
		return loadTexture(a);
	}

	CubeMap* CubeMap::allocate(const Asset& a) {
		return loadCubeMap(a);
	}

	Texture* createTexture2D(glm::ivec2 size, void* data, GLenum internalFormat, GLenum sampling, GLenum border, GLenum format, GLenum dataType) {
		Texture* ret = new Texture(0, size);
		glGenTextures(1, &ret->tex);
		mGL->bindTexture(ret, 0);
		glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, size.x, size.y, 0, format, dataType, data);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, sampling);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, sampling);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, border);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, border);
		return ret;
	}

	Texture1D* createTexture1D(int size, void* data, GLenum internalFormat, GLenum sampling, GLenum border, GLenum format, GLenum dataType) {
		Texture1D* ret = new Texture1D(0, {size,1});
		glGenTextures(1, &ret->tex);
		mGL->bindTexture(ret, 0);
		glTexImage1D(GL_TEXTURE_1D, 0, internalFormat, size, 0, format, dataType, data);

		glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, sampling);
		glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, sampling);
		glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, border);
		return ret;
	}

	void TextureAtlas::add(std::string key, TextureAtlasEntry ent) {
		entries.emplace(key, ent);
	}

	void TextureAtlas::add(std::vector<std::pair<std::string, TextureAtlasEntry>> arr){
		for(auto k:arr) add(k.first,k.second);
	}

	TextureAtlasEntry TextureAtlas::get(std::string k) {
		auto f = entries.find(k);
		if(f == entries.end()) {
			printlnf(GLERROR, "Texture Atlas Entry not found!");
			return TextureAtlasEntry{{0,0}, texture->size};
		}
		return f->second;
	}

}



