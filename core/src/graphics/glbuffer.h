/*
 * glbuffer.h
 *
 *  Created on: Jul 16, 2015
 *      Author: Dylan
 */

#ifndef GLBUFFER_H_
#define GLBUFFER_H_

#include "glcontroller.h"
#include "glanalytics.h"

namespace vox {

	enum BufferType: GLenum {
		ARRAY = GL_ARRAY_BUFFER,
		ELEMENT_ARRAY = GL_ELEMENT_ARRAY_BUFFER,
		UNIFORM = GL_UNIFORM_BUFFER,
		READ = GL_COPY_READ_BUFFER,
		WRITE = GL_COPY_WRITE_BUFFER
	};

	enum DrawType: GLenum {
		STATIC = GL_STATIC_DRAW,
		DYNAMIC = GL_DYNAMIC_DRAW,
		STREAM = GL_STREAM_DRAW
	};

	template<typename DataType> class GLBuffer {
	protected:
		int memoryUsage = 0;
	public:
		size_t size = 0; // The number of valid elements
		GLuint ID = 0;
		bool created = false;
		BufferType bufferType;
		DrawType drawType;

		static_assert(std::is_standard_layout<DataType>::value,
					"Invalid DataType specified! "
					"The DataType for this GL Buffer is Non-POD (plain-old data), "
					"use of this DataType will lead to undefined behavior.");

		GLBuffer(BufferType bufferType, DrawType drawType): bufferType(bufferType), drawType(drawType) {}

		virtual ~GLBuffer() { deleteBuffer(); }
		virtual void generateBuffer() = 0;

		virtual void bindBuffer(BufferType bufType) { mGL->bindBuffer(bufType, ID); }
		virtual void bindBuffer() final { bindBuffer(bufferType); }

		virtual void deleteBuffer() final {
			if(created) {
				glDeleteBuffers(1, &ID);
				created = false;

				mGL->analytics.freeMemory(memoryUsage);
				memoryUsage = 0;
			}
		}
	};

	// This guy is just here to support resizing and stuff. You can't actually instantiate him.
	template<typename DataType>
	class GLVector: public GLBuffer<DataType> {
	private:
		DataType* data;
		float resizeFactor;
		size_t maxSize;
		bool noResize;
	private:
		// No data must be sent from CPU to GPU, so this is a surprisingly fast operation
		void copyBuffer(int copySize) {
			GLuint toDelete = this->ID;
			this->ID = 0;

			glGenBuffers(1, &this->ID);
			this->bindBuffer();
			glBufferData(this->bufferType, this->maxSize * sizeof(DataType), NULL, this->drawType);

			mGL->bindBuffer(toDelete, 	READ);
			mGL->bindBuffer(this->ID,	WRITE);
			glCopyBufferSubData(READ, WRITE, 0, 0, copySize * sizeof(DataType));

			glDeleteBuffers(1, &toDelete);
		}

		void upSize(int newSize) {
			int oldSize = maxSize;
			maxSize = newSize;

			DataType* temp = new DataType[newSize];
			for(int i = 0; i < oldSize; i++)
				temp[i] = data[i];
			for(int i = oldSize; i < newSize; i++)
				temp[i] = DataType();
			delete[] data;
			data = temp;

			if(this->created) generateBuffer();
		}

		void downSize(int newSize) {
			maxSize = newSize;

			DataType* temp = new DataType[newSize];
			for(int i = 0; i < newSize; i++)
				temp[i] = data[i];
			delete[] data;
			data = temp;

			if(this->created)  generateBuffer();
		}

	protected:
		GLVector(BufferType bufType, DrawType drawType, size_t maxSize = 32, float resizeFactor = 2, bool noResize = false):
			GLBuffer<DataType>(bufType, drawType), resizeFactor(resizeFactor), maxSize(maxSize), noResize(noResize) {
			data = new DataType[maxSize];
			for(int i = 0; i < maxSize; i++) data[i] = DataType();
		}

		virtual void checkForResize() {
			if(!noResize) {
				while(this->size >= maxSize) upSize(maxSize * resizeFactor + 1);
				while(this->size > 32 && this->size < maxSize / (resizeFactor * resizeFactor)) downSize(maxSize / resizeFactor);
			}
		}

		virtual void set(int index, DataType val) { data[index] = val; }
	public:
		size_t getMaxSize() { return maxSize; }
		virtual ~GLVector() { delete[] data; }
		const DataType* getData() { return &data[0]; }

		virtual void generateBuffer() final {
			this->deleteBuffer();
			glGenBuffers(1, &this->ID);
			this->bindBuffer();
			glBufferData(this->bufferType, this->maxSize * sizeof(DataType), this->getData(), this->drawType);
			this->created = true;

			this->memoryUsage = this->maxSize * sizeof(DataType);
			mGL->analytics.registerMemory(this->memoryUsage);
		}

		virtual void streamData() final {
			this->bindBuffer();
			glBufferData(this->bufferType, this->getMaxSize() * sizeof(DataType), NULL, this->drawType); // Buffer orphaning, a common way to improve streaming perf.
			glBufferSubData(this->bufferType, 0, this->size * sizeof(DataType), this->getData());
		}

		virtual void updatePortion(size_t index, size_t size) final {
			this->bindBuffer();
			glBufferSubData(this->bufferType, index * sizeof(DataType), size * sizeof(DataType), this->getData() + index);
		}

		virtual void copyPortion(size_t readIndex, size_t writeIndex, size_t size) final {
			this->bindBuffer();
			glCopyBufferSubData(this->bufferType, this->bufferType, readIndex * sizeof(DataType), writeIndex * sizeof(DataType), size * sizeof(DataType));
		}
	};



	template<typename DataType>
	class ArrayBuffer: public GLVector<DataType> {
	public:
		ArrayBuffer(BufferType bufferType, DrawType drawType, size_t maxSize = 32, float resizeFactor = 2.f, bool noResize = false): GLVector<DataType>(bufferType, drawType, maxSize, resizeFactor, noResize) {
			this->size = maxSize;
		}

		void set(int index, DataType val) { GLVector<DataType>::set(index, val); }

		void resizeArray(int nSize) {
			this->size = nSize;
			this->checkForResize();
		}
	};



	template<typename DataType> class StackBuffer: public GLVector<DataType> {
	private:
		int minUpdateIndex = 0; // the minimum value reached since last buffer update

		int lastFilledIndex() { return this->size - 1; }
		int firstUnfilledIndex() { return this->size; }
	public:
		StackBuffer(BufferType bufferType, DrawType drawType):
			GLVector<DataType>(bufferType, drawType) {}

		void push(DataType val) {
			int index = firstUnfilledIndex();
			this->size++;
			this->checkForResize();
			this->set(index, val);
		}

		void push(std::vector<DataType> vals) {
			for(int i = 0; i < vals.size(); i++) push(vals[i]);
		}

		void pop() {
			this->size--;
			this->checkForResize();
			if(firstUnfilledIndex() < minUpdateIndex)
				minUpdateIndex = firstUnfilledIndex();
		}

		void updateBuffer() {
			this->updatePortion(minUpdateIndex,firstUnfilledIndex()-minUpdateIndex);
			minUpdateIndex = lastFilledIndex();
		}

		void modifyRegion(size_t start, std::vector<DataType> nData) {
			for(int i = 0; i < (int)nData.size(); i++) {
				GLVector<DataType>::set(start+i,nData[i]);
			}
			this->updatePortion(start,nData.size());
		}
	};

	template<typename KeyType, typename DataType>
	class MappedBuffer: public GLVector<DataType> {
	private:
		std::unordered_map<KeyType, int> indices;
		std::unordered_map<int, KeyType> reverseMap;

		int lastFilledIndex() { return this->size - 1; }
		int firstUnfilledIndex() { return this->size; }

	public:
		// 0 is a valid index, so don't return 0 for out of bounds.
		int getIndex(KeyType key) {
			auto it = indices.find(key);
			return it == indices.end()? -1 : it->second;
		}

		bool started;

		void start() {
			started = true;
			this->generateBuffer();
		}

		void pause() { started = false; }

		void addKeyIndexPair(KeyType key, int index) {
			indices.emplace(key, index);
			reverseMap.emplace(index, key);
		}

		void eraseKeyIndexPair(KeyType key, int index) {
			indices.erase(key);
			reverseMap.erase(index);
		}

		void add(KeyType key, DataType value) {
			int index = getIndex(key);
			if(index == -1) { // Not found, add to the end of the buffer
				index = firstUnfilledIndex();
				addKeyIndexPair(key, index);

				this->size++;
				this->checkForResize();
			}
			this->set(index, value);
			if(started) {
				this->updatePortion(index, 1);
			}
		}

		void remove(KeyType key) {
			int index = getIndex(key);
			if(index == -1) return; // The index is not in the map, so this doesn't exist already
			if(index == lastFilledIndex()) {
				indices.erase(key); // Erase the key from the map, because it no longer exists
				reverseMap.erase(index);
			} else {
				this->set(index, *(this->getData() + lastFilledIndex())); // Overwrite the data with what's at the last
				KeyType lastKey = reverseMap.find(lastFilledIndex())->second;

				eraseKeyIndexPair(key, index); // Erase the key from the map, because it no longer exists there
				eraseKeyIndexPair(lastKey, lastFilledIndex()); // Erase where the last data point was, because we're moving it

				addKeyIndexPair(lastKey, index);

				if(started) {
					this->copyPortion(lastFilledIndex(), index, 1);
				}
			}

			this->size--;
			this->checkForResize();
		}

		MappedBuffer(BufferType bufferType, DrawType drawType, size_t maxSize = 32, float resizeFactor = 2.f, bool noResize = false): GLVector<DataType>(bufferType, drawType, maxSize, resizeFactor, noResize) {
			started = false;
		}

		const std::unordered_map<KeyType, int>* getMap() { return &indices; }
		const DataType* getFromIndex(int index) { return this->getData() + index; }
	};

}


#endif /* GLBUFFER_H_ */
