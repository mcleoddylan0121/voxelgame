/*
 * glanalytics.h
 *
 *  Created on: Jan 10, 2016
 *      Author: Dylan
 */

#ifndef SRC_GRAPHICS_GLANALYTICS_H_
#define SRC_GRAPHICS_GLANALYTICS_H_

#include <cstdint>

#include "renderutils.h"

namespace vox {

	class Entity;
	class Chunk;
	class WorldStateRenderHandler;

	// returned from GLAnalytics::getWorldStateAnalytics
	struct WorldStateGLAnalytics {
		int chunkTriangles = 0, waterTriangles = 0, entityTriangles = 0, lodTriangles = 0, numParticles = 0;
		int numChunks = 0, numEntities = 0, numLoD = 0, numJoints;
		int nonEmptyChunks = 0, renderedChunks = 0;
		int drawCalls = 0;
	};

	class GLAnalytics {
	private:
		int64_t memCount; // In bytes, how much VRAM (approx.) has been used overall

		int _curEntityMeshTriangles = 0, _curJoints = 0;
		int _getTriangleCount(EntityMeshBuffer* en);

		EntityMeshRenderer* entityAnalyticsRenderer;

		friend class GLAnalyticsEntityRenderer;
	public:
		GLAnalytics();

		// TRIANGLE COUNT
		int getTriangleCount(Chunk* ch);
		int getWaterTriangleCount(Chunk* ch);
		int getTriangleCount(Entity* en);

		WorldStateGLAnalytics getWorldStateAnalytics(WorldStateRenderHandler* worldStateRenderHandler);


		// MEMORY (VRAM)
		void registerMemory(int64_t amount) { memCount += amount; }
		void freeMemory(int64_t amount) { memCount -= amount; }
		int64_t getMemoryUsage() { return memCount; } // poll current openGL memory usage
	};

}


#endif /* SRC_GRAPHICS_GLANALYTICS_H_ */
