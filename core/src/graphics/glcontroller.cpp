/*
 * glcontroller.cpp
 *
 *  Created on: Mar 22, 2015
 *      Author: Dylan
 */

#include "glcontroller.h"
#include "console.h"
#include "sdlcontroller.h"
#include "io.h"
#include "geometry.h"
#include "filesystem.h"
#include "framebuffer.h"
#include "program.h"
#include "glbuffer.h"
#include "camera.h"
#include "fontrender.h"
#include "voxelmesh.h"
#include "settingsmanager.h"

#include <math.h>
#include <cstddef>
#include <cmath>
#include <png.h>
#include <stdio.h>

namespace vox {
	GLController * mGL;

	void GLController::bindBuffer(GLenum bufferType, GLuint buf) {
		//if(boundBuffers[bufferType] != buf) {
			glBindBuffer(bufferType, buf);
		//	boundBuffers[bufferType] = buf;
		//}
	}

	glm::vec2 globalViewportTranslate(0);
	glm::vec2 globalViewportScale(1);

	void GLController::bindFrameBuffer(FrameBuffer* fbo) {
		//if(currentFrameBuffer != fbo) {
			currentFrameBuffer = fbo;
			// Render to our framebuffer
			glBindFramebuffer(GL_FRAMEBUFFER, fbo->name);
			if(fbo == windowFBO)
				glViewport(0,0,mSDL->windowWidth,mSDL->windowHeight);
			else
				glViewport(fbo->viewportStart.x + (globalViewportTranslate.x / fbo->size.x),fbo->viewportStart.y + (globalViewportTranslate.y / fbo->size.y),

						   fbo->viewportSize.x * globalViewportScale.x, fbo->viewportSize.y * globalViewportScale.y);
		//}
	}

	void GLController::unbindFrameBuffer() {
		bindFrameBuffer(windowFBO);
	}

	void GLController::render(RenderTextureProgram* p, Texture* t, glm::vec2 pos, glm::vec2 size, float depth, float rot, glm::vec4 color, glm::vec2 UVstart, glm::vec2 UVsize) {
		bindTexture(t, 0);

		glUniform1i(p->texture, 0);
		glUniform2fv(p->size, 1, &size[0]);
		glUniform2fv(p->position, 1, &pos[0]);
		glUniform1f(p->depth, depth);
		glUniform1f(p->rot, rot);
		glUniform4fv(p->color, 1, &color[0]);
		glUniform2fv(p->UVstart, 1, &(UVstart[0]));
		glUniform2fv(p->UVsize, 1, &(UVsize[0]));
		glUniform2f(p->screenSize, (float)currentFrameBuffer->size.x, (float)currentFrameBuffer->size.y);

		screenBuffer->bindBuffer();
		glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE,0,(void*) 0);

		glDrawArrays(GL_TRIANGLES, 0, 6);
	}

	void GLController::renderText(TextShaderProgram* p, FontRenderObject* f, glm::ivec2 pos, glm::vec2 size, glm::vec4 color, int layer) {
		bindTexture(f->tex, 0);

		glUniform1i(p->textureAtlas, 0);
		glm::vec2 ftsize = glm::vec2(f->tex->size);
		glUniform2fv(p->textureSize, 1, &ftsize[0]);
		glUniform2fv(p->size, 1, &size[0]);
		glm::vec2 fpos = glm::vec2(pos);
		glUniform2fv(p->pos, 1, &fpos[0]);
		glUniform4fv(p->color, 1, &color[0]);

		float flayer = layer;
		glUniform1f(p->layer, flayer);

		f->vertexBuffer->bindBuffer();
		glVertexAttribPointer(0,2,GL_SHORT,GL_FALSE,sizeof(FontVertex),(GLvoid*)offsetof(FontVertex, pos));
		glVertexAttribPointer(1,2,GL_SHORT,GL_FALSE,sizeof(FontVertex),(GLvoid*)offsetof(FontVertex, texCoord));
		glVertexAttribPointer(2,4,GL_UNSIGNED_BYTE,GL_TRUE,sizeof(FontVertex),(GLvoid*)offsetof(FontVertex, color));
		VoxelMeshBuffer::indexBuffer->bindBuffer();

		glDrawElements(GL_TRIANGLES, f->vertexBuffer->size * 6, GL_UNSIGNED_INT, (GLvoid*) 0);
	}

	void GLController::bindProgram(Program* p) {
		if(currentProgram != p) {
			glUseProgram(p->name);
			currentProgram = p;
		}
	}

	void GLController::useScreenProgram() {
		bindProgram(screenProgram);
	}

	void GLController::bindTexture(Texture * t, int channel) {
		glActiveTexture(GL_TEXTURE0 + channel);
		glBindTexture(t->DEFAULT_BINDING_LOCATION, t->tex);
	}

	void GLController::bindTexture(Texture* t, int channel, GLint uniform) {
		glActiveTexture(GL_TEXTURE0 + channel);
		glBindTexture(t->DEFAULT_BINDING_LOCATION, t->tex);
		glUniform1i(uniform, channel);
	}

	void GLController::bindTextures(std::vector<TextureBinding> textures) {
		for(TextureBinding t:textures) {
			bindTexture(t.texture,t.channel,t.uniform);
		}
	}

	void GLController::unbindTexture(Texture * t, int channel) {
		glActiveTexture(GL_TEXTURE0 + channel);
		glBindTexture(t->DEFAULT_BINDING_LOCATION, 0);
	}

	bool GLController::handle_error(GLenum error, const char * func, int line) {
		if (error != 0 && error != 1280) { // Don't return opengl INVALID_ENUM errors because they don't mean anything
			printf(GLERROR, "In %s on line %i: %s (%i)\n", func, line, gluErrorString(error),
					error);
			return true;
		}
		return false;
	}

	struct Bitmap {
	    glm::u8vec3 *pixels;
	    size_t width;
	    size_t height;
	};

	glm::u8vec3 pixelAt(Bitmap* b, glm::ivec2 pos) {
		return b->pixels[pos.x + pos.y*b->width];
	}

	int save_png_to_file (Bitmap *bitmap, const char *path) {
	    FILE * fp;
	    png_structp png_ptr = NULL;
	    png_infop info_ptr = NULL;
	    size_t x, y;
	    png_byte ** row_pointers = NULL;
	    /* "status" contains the return value of this function. At first
	       it is set to a value which means 'failure'. When the routine
	       has finished its work, it is set to a value which means
	       'success'. */
	    int status = -1;
	    /* The following number is set by trial and error only. I cannot
	       see where it it is documented in the libpng manual.
	    */
	    int pixel_size = 3;
	    int depth = 8;

	    fp = fopen (path, "wb");
	    if (! fp) {
	        goto fopen_failed;
	    }

	    png_ptr = png_create_write_struct (PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	    if (png_ptr == NULL) {
	        goto png_create_write_struct_failed;
	    }

	    info_ptr = png_create_info_struct (png_ptr);
	    if (info_ptr == NULL) {
	        goto png_create_info_struct_failed;
	    }

	    /* Set up error handling. */

	    if (setjmp (png_jmpbuf (png_ptr))) {
	        goto png_failure;
	    }

	    /* Set image attributes. */

	    png_set_IHDR (png_ptr,
	                  info_ptr,
	                  bitmap->width,
	                  bitmap->height,
	                  depth,
	                  PNG_COLOR_TYPE_RGB,
	                  PNG_INTERLACE_NONE,
	                  PNG_COMPRESSION_TYPE_DEFAULT,
	                  PNG_FILTER_TYPE_DEFAULT);

	    /* Initialize rows of PNG. */

	    row_pointers = (png_byte**) png_malloc (png_ptr, bitmap->height * sizeof (png_byte *));
	    for (y = 0; y < bitmap->height; ++y) {
	        png_byte *row =
	            (png_byte*) png_malloc (png_ptr, sizeof (uint8_t) * bitmap->width * pixel_size);
	        row_pointers[y] = row;
	        for (x = 0; x < bitmap->width; ++x) {
	            glm::u8vec3 pixel = pixelAt (bitmap, {x, y});
	            *row++ = pixel.r;
	            *row++ = pixel.g;
	            *row++ = pixel.b;
	        }
	    }

	    /* Write the image data to "fp". */

	    png_init_io (png_ptr, fp);
	    png_set_rows (png_ptr, info_ptr, row_pointers);
	    png_write_png (png_ptr, info_ptr, PNG_TRANSFORM_IDENTITY, NULL);

	    /* The routine has successfully written the file, so we set
	       "status" to a value which indicates success. */

	    status = 0;

	    for (y = 0; y < bitmap->height; y++) {
	        png_free (png_ptr, row_pointers[y]);
	    }
	    png_free (png_ptr, row_pointers);

		png_failure:
		png_create_info_struct_failed:
		png_destroy_write_struct (&png_ptr, &info_ptr);
		png_create_write_struct_failed:
		fclose (fp);
		fopen_failed:
		return status;
	}

	void GLController::saveFrameBufferOutput(FrameBuffer* buf, const std::string& path) {
		// RGBA = 4 bytes
		GLuint* pixels = new GLuint[buf->size.y * buf->size.x];

		bindFrameBuffer(buf);
		glReadPixels( 0, 0, buf->size.x, buf->size.y, GL_RGBA, GL_UNSIGNED_INT_8_8_8_8_REV, &pixels[0] );

		// I'm lazy, let's flip the image
		for(int y = 0; y < buf->size.y / 2; y++) {
			int index1 = y * buf->size.x;
			int index2 = (buf->size.y - y - 1) * buf->size.x;
			for(int x = 0; x < buf->size.x; x++) {
				swapValues(pixels[x + index1], pixels[x + index2]);
			}
		}

		glm::u8vec3* bmpx = new glm::u8vec3[buf->size.y * buf->size.x];
		Bitmap b = Bitmap{bmpx,(size_t)buf->size.x,(size_t)buf->size.y};
		for(int i = 0; i < buf->size.y * buf->size.x; i++) {
			glm::u8vec4* v = (glm::u8vec4*)(pixels + i);
			bmpx[i] = glm::u8vec3(v->r,v->g,v->b);
			v->a = 255;
		}

		save_png_to_file(&b, path.c_str());

		delete[] pixels;
		delete[] bmpx;
	}

	int scaleFactor = 1;

	void GLController::takeScreenShot() {
		glm::vec2 SCREENSHOT_SIZE = {mSDL->viewportWidth, mSDL->viewportHeight};
		ColorBuffer* screenshot = new ColorBuffer(SCREENSHOT_SIZE * (float)scaleFactor);
		ColorBuffer* AA = new ColorBuffer(SCREENSHOT_SIZE); // we can afford to do some extra anti-aliasing here, so why not?
		screenshot->pixelSize = glm::vec2(scaleFactor) * glm::vec2(AA->size) / glm::vec2(windowFBO->size);
		std::string ts = getFileFormattedTimeStamp();
		std::string path = "screenshots/screenshot_" + ts + ".png";
		screenshot->clear();
		/*for(int x = 0; x < scaleFactor; x++) {
			for(int y = 0; y < scaleFactor; y++) {
				screenshot->viewportStart = SCREENSHOT_SIZE * glm::vec2(x,y) * (float)scaleFactor;
				screenshot->viewportSize = SCREENSHOT_SIZE * (float)scaleFactor;
				globalViewportTranslate = glm::vec2(x,y)/(float)scaleFactor;
				globalViewportScale = glm::vec2(1.f/(float)scaleFactor);
				renderGameStates(screenshot);
			}
		}
		globalViewportTranslate = glm::vec2(0);
		globalViewportScale = glm::vec2(1);*/
		renderGameStates(screenshot);

		handleGLError();
		sampleFramebuffer(screenshot, AA);
		saveFrameBufferOutput(AA, path);
		delete screenshot;
		delete AA;
	}

	void GLController::renderGameStates(FrameBuffer* target) {
		bindFrameBuffer(target);

		for (auto state : mGameLoop.getGameStates()) {
			state->render(currentFrameBuffer);
		}
		handleGLError();
	}

	GLuint VoxelMeshBuffer::currentIndex = 0;

	GLController::GLController() {
		glewExperimental = GL_TRUE;									// OpenGL 3.3 needs this to function
		handle_error(glewInit(), __FUNCTION__, __LINE__);			// Initialize GLEW
		mSDL->handleError(__FUNCTION__, __LINE__);					// Make sure we didn't break anything
		handleGLError();			// It's okay if this returns "invalid enumerant"

		/* Begin various initialization stuff */

		// Sky blue
		glClearColor(130.f/255.f, 200.f/255.f, 255.f/255.f, 1.f);

		glEnable(GL_DEPTH_TEST); // Enable depth test
		glDepthFunc(GL_LEQUAL);  // Accept fragment if it closer to the camera than the former one
		glEnable(GL_TEXTURE_2D);
		glEnable(GL_TEXTURE_2D_ARRAY);
		glEnable(GL_TEXTURE_CUBE_MAP);
		glEnable(GL_BLEND); 	 // Enable blending
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		// Cull triangles which normal is not towards the camera (I.E. Don't show walls we can't see)
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
		glDisable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
		glEnable(GL_MULTISAMPLE);

		glClampColor(GL_CLAMP_READ_COLOR, GL_FALSE);
		glClampColor(GL_CLAMP_VERTEX_COLOR, GL_FALSE);
		glClampColor(GL_CLAMP_FRAGMENT_COLOR, GL_FALSE);

		/* End various initialization stuff */

		handleGLError();

		glGenVertexArrays(1, &vAID);
		glBindVertexArray(vAID);

		colorFormatSizes = {
					{ GL_R,    1},
					{ GL_RED,  1},
					{ GL_RG,   2},
					{ GL_RGB,  3},
					{ GL_RGBA, 4},

					{ GL_R8,    1},
					{ GL_RG8,   2},
					{ GL_RGB8,  3},
					{ GL_RGBA8, 4},

					{ GL_R16,    2},
					{ GL_RG16,   4},
					{ GL_RGB16,  6},
					{ GL_RGBA16, 8},

					{ GL_R16F,    2},
					{ GL_RG16F,   4},
					{ GL_RGB16F,  6},
					{ GL_RGBA16F, 8},

					{ GL_R32F,    4},
					{ GL_RG32F,   8},
					{ GL_RGB32F,  12},
					{ GL_RGBA32F, 16},

					{ GL_R8I,    1},
					{ GL_RG8I,   2},
					{ GL_RGB8I,  3},
					{ GL_RGBA8I, 4},

					{ GL_R16I,    2},
					{ GL_RG16I,   4},
					{ GL_RGB16I,  6},
					{ GL_RGBA16I, 8},

					{ GL_R32I,    4},
					{ GL_RG32I,   8},
					{ GL_RGB32I,  12},
					{ GL_RGBA32I, 16},

					{ GL_R8UI,    1},
					{ GL_RG8UI,   2},
					{ GL_RGB8UI,  3},
					{ GL_RGBA8UI, 4},

					{ GL_R16UI,    2},
					{ GL_RG16UI,   4},
					{ GL_RGB16UI,  6},
					{ GL_RGBA16UI, 8},

					{ GL_R32UI,    4},
					{ GL_RG32UI,   8},
					{ GL_RGB32UI,  12},
					{ GL_RGBA32UI, 16},

					{ GL_R3_G3_B2,    1},
					{ GL_RGB10_A2, 4},
					{ GL_RGB10_A2UI, 4},
					{ GL_R11F_G11F_B10F, 4},
					{ GL_SRGB8_ALPHA8, 4},
					{ GL_DEPTH_COMPONENT16, 2},
					{ GL_DEPTH_COMPONENT24, 3},
					{ GL_DEPTH_COMPONENT32F, 4},
					{ GL_DEPTH24_STENCIL8, 4},
					{ GL_DEPTH32F_STENCIL8, 5}
		};
	}

	int GLController::getColorFormatSize(GLenum format) {
		auto found = colorFormatSizes.find(format);
		if(found == colorFormatSizes.end()) {
			printlnf(GLERROR, "Invalid color format (%i) specified, cannot give memory readout. (Blame Dylan)", format);
			return 0;
		}
		return found->second;
	}

	void GLController::init() {
		VoxelMeshBuffer::indexBuffer = new VoxelMeshBuffer::indexBufferType(ELEMENT_ARRAY, STATIC);
		VoxelMeshBuffer::indexBuffer->generateBuffer();

		screenBuffer = new StackBuffer<glm::vec3>(ARRAY, STATIC);
		screenBuffer->push( {
			{0.f, 0.f, 0.f},
			{1.f, 0.f, 0.f},
			{1.f, 1.f, 0.f},
			{1.f, 1.f, 0.f},
			{0.f, 1.f, 0.f},
			{0.f, 0.f, 0.f}
		});
		screenBuffer->generateBuffer();

		skyBoxBuffer = new StackBuffer<glm::vec3>(ARRAY, STATIC);
		skyBoxTexLayerBuffer = new StackBuffer<float>(ARRAY, STATIC);
		cubeWireframe = new StackBuffer<Line<glm::u8vec3>>(ARRAY, STATIC);
		skyBoxBuffer->push( {
				{-1.0f, 1.0f, 1.0f},{-1.0f,-1.0f, 1.0f},{-1.0f,-1.0f,-1.0f},
				{-1.0f, 1.0f,-1.0f},{-1.0f,-1.0f,-1.0f},{1.0f, 1.0f,-1.0f},
				{1.0f,-1.0f,-1.0f}, {-1.0f,-1.0f,-1.0f},{1.0f,-1.0f, 1.0f},
				{-1.0f,-1.0f,-1.0f},{1.0f,-1.0f,-1.0f}, {1.0f, 1.0f,-1.0f},
				{-1.0f, 1.0f,-1.0f},{-1.0f, 1.0f, 1.0f},{-1.0f,-1.0f,-1.0f},
				{-1.0f,-1.0f,-1.0f},{-1.0f,-1.0f, 1.0f},{1.0f,-1.0f, 1.0f},

				{1.0f,-1.0f, 1.0f}, {-1.0f,-1.0f, 1.0f},{-1.0f, 1.0f, 1.0f},
				{1.0f, 1.0f,-1.0f}, {1.0f,-1.0f,-1.0f}, {1.0f, 1.0f, 1.0f},
				{1.0f,-1.0f, 1.0f}, {1.0f, 1.0f, 1.0f}, {1.0f,-1.0f,-1.0f},
				{-1.0f, 1.0f,-1.0f},{1.0f, 1.0f,-1.0f}, {1.0f, 1.0f, 1.0f},
				{-1.0f, 1.0f, 1.0f},{-1.0f, 1.0f,-1.0f},{1.0f, 1.0f, 1.0f},
				{1.0f,-1.0f, 1.0f}, {-1.0f, 1.0f, 1.0f},{1.0f, 1.0f, 1.0f}
		});
		skyBoxTexLayerBuffer->push({
				1,1,1,	5,5,5,	3,3,3,
				5,5,5,	1,1,1,	3,3,3,
				4,4,4,	0,0,0,	0,0,0,
				2,2,2,	2,2,2,	4,4,4
		});
		cubeWireframe->push({
			{{0,0,0},{1,0,0}}, {{0,0,0},{0,1,0}}, {{0,0,0},{0,0,1}},
			{{1,0,0},{1,1,0}}, {{1,0,0},{1,0,1}},
			{{0,1,0},{1,1,0}}, {{0,1,0},{0,1,1}},
			{{0,0,1},{1,0,1}}, {{0,0,1},{0,1,1}},
			{{1,1,0},{1,1,1}}, {{1,0,1},{1,1,1}}, {{0,1,1},{1,1,1}}
		});
		cubeWireframe->generateBuffer();
		skyBoxBuffer->generateBuffer();
		skyBoxTexLayerBuffer->generateBuffer();
		screenProgram = new RenderTextureProgram("screenshader");

		windowFBO = FrameBuffer::getWindow();
		unbindFrameBuffer();
		useScreenProgram();

		handleGLError();
	}

	GLController::~GLController() {
		delete screenBuffer;
		glDeleteVertexArrays(1, &vAID);
		delete screenProgram;
	}

	void GLController::render(ChunkMeshBuffer* v, ChunkShaderProgram* p, const CameraBase* viewCam, glm::mat4 model) {
		uploadCameraModelMatrices(p,viewCam,model);

		v->buffer->bindBuffer();
		glVertexAttribPointer( 0, 4, GL_UNSIGNED_INT_2_10_10_10_REV, GL_FALSE, sizeof(TerrainPaletteVertex), (GLvoid*) offsetof(TerrainPaletteVertex,position));
		glVertexAttribPointer( 1, 4, GL_UNSIGNED_INT_2_10_10_10_REV, GL_FALSE, sizeof(TerrainPaletteVertex), (GLvoid*) offsetof(TerrainPaletteVertex,uv));

		// v->lightBuffer->bindBuffer();

		// glVertexAttribPointer(4, 3, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(VertexLighting), (GLvoid*) 0);

		v->chunkIndexBuffer->bindBuffer();

		glDrawElements(GL_TRIANGLES, v->elements, GL_UNSIGNED_INT, (GLvoid*) 0);
	}

	void GLController::render(LoDMeshBuffer* v, ChunkLoDShaderProgram* p, const CameraBase* viewCam, glm::mat4 model) {
		uploadCameraModelMatrices(p,viewCam,model);

		v->buffer->bindBuffer();

		/*glVertexAttribPointer(0, 3, GL_UNSIGNED_BYTE, GL_FALSE, sizeof(EntityVertex), (GLvoid*) offsetof(EntityVertex,position));
		glVertexAttribPointer(1, 4, GL_UNSIGNED_BYTE, GL_TRUE,  sizeof(EntityVertex), (GLvoid*) offsetof(EntityVertex,color));
		glVertexAttribPointer(2, 1, GL_UNSIGNED_BYTE, GL_FALSE, sizeof(EntityVertex), (GLvoid*) offsetof(EntityVertex,occlusion));
		glVertexAttribPointer(3, 3, GL_BYTE, 		  GL_TRUE, 	sizeof(EntityVertex), (GLvoid*) offsetof(EntityVertex,normal));
		glVertexAttribPointer(4, 3, GL_BYTE,          GL_TRUE,  sizeof(EntityVertex), (GLvoid*) offsetof(EntityVertex,vlight));
		*/
		VoxelMeshBuffer::indexBuffer->bindBuffer();

		glDrawElements(GL_TRIANGLES, v->elements, GL_UNSIGNED_INT, (GLvoid*) 0);
	}

	void GLController::render(EntityMeshBuffer* v, EntityShaderProgram* p, const CameraBase* viewCam, glm::mat4 model) {
		uploadCameraModelMatrices(p,viewCam,model);

		v->buffer->bindBuffer();
		glVertexAttribPointer( 0, 4, GL_UNSIGNED_INT_2_10_10_10_REV, GL_FALSE, sizeof(TerrainPaletteVertex), (GLvoid*) offsetof(TerrainPaletteVertex,position));
		glVertexAttribPointer( 1, 4, GL_UNSIGNED_INT_2_10_10_10_REV, GL_FALSE, sizeof(TerrainPaletteVertex), (GLvoid*) offsetof(TerrainPaletteVertex,uv));

		v->entityIndexBuffer->bindBuffer();
		glDrawElements(GL_TRIANGLES, v->elements, GL_UNSIGNED_INT, (GLvoid*) 0);
	}

	void GLController::renderShadows(ChunkMeshBuffer* v, ShadowShaderProgram* p, const CameraBase* cam, glm::mat4 model) {
		uploadCameraModelMatrices(p,cam,model);

		v->buffer->bindBuffer();
		// glVertexAttribPointer(0, 3, GL_UNSIGNED_BYTE, GL_FALSE, sizeof(EntityVertex), (void*)(offsetof(EntityVertex,position)));
		VoxelMeshBuffer::indexBuffer->bindBuffer();
		glDrawElements(GL_TRIANGLES, v->elements, GL_UNSIGNED_INT, (void*) 0);
	}

	void GLController::renderShadows(EntityMeshBuffer* v, ShadowShaderProgram* p, const CameraBase* cam, glm::mat4 model) {
		uploadCameraModelMatrices(p,cam,model);

		v->buffer->bindBuffer();
		// glVertexAttribPointer(0, 3, GL_UNSIGNED_BYTE, GL_FALSE, sizeof(EntityVertex), (void*)(offsetof(EntityVertex,position)));
		VoxelMeshBuffer::indexBuffer->bindBuffer();
		glDrawElements(GL_TRIANGLES, v->elements, GL_UNSIGNED_INT, (void*) 0);
	}

	void GLController::sampleFramebuffer(FrameBuffer* source, FrameBuffer* target, GLenum mask, GLenum interpolate, GLenum read, GLenum draw) {
		glBindFramebuffer(GL_READ_FRAMEBUFFER, source->name);
		glReadBuffer(read);
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, target->name);
		glDrawBuffer(draw);
		glBlitFramebuffer (0,0,source->size.x,source->size.y,0,0,target->size.x,target->size.y,mask,interpolate);
	}

} /* namespace vox */
