/*
 * blur.h
 *
 *  Created on: Jun 23, 2016
 *      Author: Dylan
 */

#ifndef SRC_GRAPHICS_BLUR_H_
#define SRC_GRAPHICS_BLUR_H_

#include "framebuffer.h"

namespace vox {

	class BlurHandler {
	private:
		ColorBuffer *intermediate, *returnBuf; // returnBuf is only created if makeNew is enabled, else we blur on top of the old buffer
		int channels;
		bool makeNew;

		RenderTextureProgram *horiBlur, *vertBlur;
	public:
		BlurHandler(glm::ivec2 size, GLenum colorformat = GL_RGBA8, int colorChannels = 4, bool makeNew = false, std::string shader = "screenshader");
		ColorBuffer* blur(FrameBuffer* in, int targetComp = 0);
	};

}


#endif /* SRC_GRAPHICS_BLUR_H_ */
