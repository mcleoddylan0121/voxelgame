/*
 * renderutils.h
 *
 *  Created on: Jul 10, 2015
 *      Author: Daniel
 */

#ifndef RENDERUTILS_H_
#define RENDERUTILS_H_

#include "coords.h"
#include "filesystem.h"
#include "colormath.h"

#include <tuple>

namespace vox {

	struct Volume;
	struct Color;

	template<typename... ArgTypes>
	struct Renderer {
		virtual void render(ArgTypes...) = 0;
		virtual ~Renderer() {}
	};

	template<>
	struct Renderer<void> {
		virtual void render() = 0;
		virtual ~Renderer() {}
	};

	struct ChunkMeshBuffer;
	struct EntityMeshBuffer;
	struct FontRenderObject;
	struct TexturedMeshBuffer;

	typedef EntityMeshBuffer LoDMeshBuffer;

	struct GUIRenderObject;

	struct Texture {
		GLuint tex;
		glm::ivec2 size;
		GLenum DEFAULT_BINDING_LOCATION;
		Texture(GLuint tex = 0, glm::ivec2 size = glm::ivec2(0), GLenum DEFAULT_BINDING_LOCATION = GL_TEXTURE_2D): tex(tex), size(size), DEFAULT_BINDING_LOCATION(DEFAULT_BINDING_LOCATION) {}
		static Texture* allocate(const Asset& a);
	};

	// Renderers are called with display objects so it's easier to add new arguments without breaking everything.
	// And it's cleaner.
	// Not only that, but we can make custom constructors, instead of having to use crappy functions to call renderers with different arguments (ew)


	// Template laziness
	template<typename BufferType>
	struct VoxelMeshDisplayObject {
		BufferType* buf;
		glm::mat4 model;
		WorldCoordinate scale; // make sure this is correct!!!

		Color::HighpColor llevOverride = Color::HighpColor(-1);

		VoxelMeshDisplayObject(BufferType* buf, glm::mat4 model, WorldCoordinate scale):
			buf(buf), model(model), scale(scale) {}
	};

	typedef VoxelMeshDisplayObject<ChunkMeshBuffer> ChunkMeshDisplayObject;
	typedef VoxelMeshDisplayObject<EntityMeshBuffer> EntityMeshDisplayObject;
	typedef VoxelMeshDisplayObject<LoDMeshBuffer> LoDMeshDisplayObject;

	struct VolumeDisplayObject {
		Volume* volume;
		Color::HighpColor color;

		VolumeDisplayObject(Volume* volume, Color::HighpColor color = Color::HighpColor(1,0,0,1)): // default color is red, since that's a standard debug color
			volume(volume), color(color) {}
	};

	// TODO: integrate this with FontRenderObject, since that's it's purpose anyway, right?
	struct TextDisplayObject {
		FontRenderObject* fontRender;
		glm::vec2 pos;
		glm::vec2 size;
		Color::HighpColor color;

		TextDisplayObject(FontRenderObject* fontRender, glm::vec2 pos = glm::vec2(0), glm::vec2 size = glm::vec2(1), Color::HighpColor color = Color::HighpColor(1)):
			fontRender(fontRender), pos(pos), size(size), color(color) {}
	};

	struct BillboardDisplayObject {
		Texture* tex;
		WorldCoordinate pos;
		glm::vec2 size;

		// these are in range of [0,1], but the constructor takes coordinates in the range of [0, tex->size]
		glm::vec2 UVstart;
		glm::vec2 UVsize;

		float rot;
		Color::HighpColor color;

		BillboardDisplayObject(Texture* tex = nullptr, WorldCoordinate pos = WorldCoordinate(0), glm::vec2 size = glm::vec2(1), glm::ivec2 UVstart = glm::vec2(0), glm::ivec2 UVsize = glm::vec2(-1), float rot = 0.f, Color::HighpColor color = Color::HighpColor(1)):
			tex(tex), pos(pos), size(size), rot(rot), color(color) {

			if(UVsize == glm::ivec2(-1) || !tex) {
				this->UVsize = glm::vec2(1);
			}
			else {
				this->UVstart = glm::vec2(UVstart) / glm::vec2(tex->size);
				this->UVsize = glm::vec2(UVsize) / glm::vec2(tex->size);
			}

		}
	};

	struct DynamicLightDisplayObject {
		WorldCoordinate pos;
		float radius;
		Color::HighpColor color;
		float compensation;
		float brightness;

		DynamicLightDisplayObject(WorldCoordinate pos, float radius = 8, Color::HighpColor color = Color::HighpColor(1), float brightness = 1, float compensation = -1):
			pos(pos), radius(radius), color(color), compensation(compensation), brightness(brightness) {}
	};

	struct DropShadowDisplayObject {
		Volume* volume;
		bool onGround = false;

		DropShadowDisplayObject(Volume* volume, bool onGround = false): volume(volume), onGround(onGround) {}
	};

	struct TexturedMeshDisplayObject {
		TexturedMeshBuffer* buf;
		WorldCoordinate pos;
		glm::vec2 size;
		glm::vec3 scale;
		glm::quat rot;
		Color::HighpColor color;

		TexturedMeshDisplayObject(TexturedMeshBuffer* buf, WorldCoordinate pos, glm::vec2 size, glm::vec3 scale, glm::quat rot, Color::HighpColor color):
			buf(buf), pos(pos), size(size), scale(scale), rot(rot), color(color) {}
	};

	struct CloudParticleDisplayObject {
		glm::vec3 pos;
		int textureIndex;
		CloudParticleDisplayObject(glm::vec3 pos, int textureIndex): pos(pos), textureIndex(textureIndex) {}
	};

	typedef Renderer<ChunkMeshDisplayObject> ChunkMeshRenderer;
	typedef Renderer<LoDMeshDisplayObject> LoDChunkMeshRenderer;
	typedef Renderer<EntityMeshDisplayObject> EntityMeshRenderer;
	typedef Renderer<DynamicLightDisplayObject> DynamicLightRenderer;
	typedef Renderer<VolumeDisplayObject> VolumeRenderer;
	typedef Renderer<TextDisplayObject> TextRenderer;
	typedef Renderer<BillboardDisplayObject> BillboardRenderer;
	typedef Renderer<DropShadowDisplayObject> DropShadowRenderer;
	typedef Renderer<TexturedMeshDisplayObject> TexturedMeshRenderer;
	typedef Renderer<CloudParticleDisplayObject> CloudParticleRenderer;


	template<typename Render, typename BufferType> void renderMesh(Render* renderer, BufferType* buffer, WorldCoordinate pos, WorldCoordinate scale, glm::quat rot = glm::quat(1,0,0,0)) {
		glm::mat4 model = buffer->getModelMatrix(scale,pos,rot);
		renderer->render(VoxelMeshDisplayObject<BufferType>(buffer, model, scale));
	}

	struct Texture1D: public Texture {
		Texture1D(GLuint tex = 0, glm::ivec2 size = glm::ivec2(0,1)): Texture(tex, size, GL_TEXTURE_1D) {}
	};

	// A rectangle on the texture atlas
	struct TextureAtlasEntry {
		glm::ivec2 pos;
		glm::ivec2 size;
	};

	Texture* createTexture2D(glm::ivec2 size, void* data, GLenum internalFormat, GLenum sampling, GLenum border, GLenum format, GLenum dataType);
	Texture1D* createTexture1D(int size, void* data, GLenum internalFormat, GLenum sampling, GLenum border, GLenum format, GLenum dataType);

	struct TextureAtlas {
	private:
		std::unordered_map<std::string, TextureAtlasEntry> entries;
	public:
		Texture* texture;
		void add(std::string key, TextureAtlasEntry ent);
		void add(std::vector<std::pair<std::string, TextureAtlasEntry>> arr);
		TextureAtlasEntry get(std::string k);
		static TextureAtlas* allocate(const Asset& a);
	};

	struct Texture2DArray: public Texture {
		int textures;
		Texture2DArray(GLuint tex = 0, glm::ivec2 size = glm::ivec2(0), int textures = 1): Texture(tex, size, GL_TEXTURE_2D_ARRAY), textures(textures) {}
	};

	struct CubeMap: public Texture {
		CubeMap(GLuint tex = 0, glm::ivec2 size = glm::ivec2(0)): Texture(tex, size, GL_TEXTURE_CUBE_MAP) {}
		static CubeMap* allocate(const Asset& a);
	};

	template<typename T, Asset::Type A>
	struct AllocatorTemplate;

	typedef AllocatorTemplate<Texture, Asset::Texture> TEX_ALLOC;
}

#endif /* RENDERUTILS_H_ */
