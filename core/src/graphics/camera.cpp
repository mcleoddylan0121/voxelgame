/*
 * camera.cpp
 *
 *  Created on: Jul 16, 2015
 *      Author: Dylan
 */

#include "camera.h"
#include "shapes.h"

namespace vox {

	Camera::Camera(float fovY, float aspectRatio, float near, float far, glm::vec3 lookAt, glm::vec3 pos, glm::vec3 up):
					CameraBase(lookAt, pos, up), fovY(fovY), aspectRatio(aspectRatio), near(near), far(far) { update(); }

	void CameraBase::translate(float x, float y, float z) {
		pos += glm::vec3(x, y, z);
		lookAt += glm::vec3(x, y, z);
	}

	bool Camera::containsSphere(const glm::vec3& pos, float radius) const {
		for(int i = 0; i < 6; i++)
			if (planes[i].distance(pos) < -radius )
				return false;
		return true;
	}

	std::vector<glm::vec3> Camera::getCorners(float dist1, float dist2) {
		glm::vec3 dir   = glm::normalize(pos - lookAt); // The direction we're facing
		glm::vec3 right = glm::normalize(glm::cross(up, dir)); // Our right
		glm::vec3 up2   = glm::normalize(glm::cross(dir, right)); // Temporary "up", may differ from given up

		// the centers of the near and far planes
		glm::vec3 center1 = pos - dir * dist1;
		glm::vec3 center2 = pos - dir * dist2;

		std::vector<glm::vec3> ret;

		float height1 = dist1 * tangent;
		float height2 = dist2 * tangent;
		float width1 = height1 * aspectRatio;
		float width2 = height2 * aspectRatio;

		ret.push_back(center1 - up2 * height1 - right * width1);
		ret.push_back(center1 - up2 * height1 + right * width1);
		ret.push_back(center1 + up2 * height1 - right * width1);
		ret.push_back(center1 + up2 * height1 + right * width1);
		ret.push_back(center2 - up2 * height2 - right * width2);
		ret.push_back(center2 - up2 * height2 + right * width2);
		ret.push_back(center2 + up2 * height2 - right * width2);
		ret.push_back(center2 + up2 * height2 + right * width2);
		return ret;
	}

	void Camera::update() {
		// Set the V and P matrices
		view = glm::lookAt(pos, lookAt, up);
		projection = glm::perspective(fovY, aspectRatio, near, far);
		viewProjection = projection * view;


		tangent = tan((fovY / 2.f)); // tan(fovY / 2)

		nearHeight = near * tangent; // tan(fovY / 2) = height / distance
		farHeight  = far  * tangent;
		nearWidth  = nearHeight * aspectRatio;
		farWidth   = farHeight  * aspectRatio;

		glm::vec3 dir   = glm::normalize(pos - lookAt); // The direction we're facing
		glm::vec3 right = glm::normalize(glm::cross(up, dir)); // Our right
		glm::vec3 up2   = glm::normalize(glm::cross(dir, right)); // Temporary "up", may differ from given up

		// the centers of the near and far planes
		glm::vec3 nearCenter = pos - dir * near;
		glm::vec3 farCenter  = pos - dir * far;

		// compute the 8 corners of the frustum. We don't divide the heights and widths by 2 it's the height from the center
		corners[nbl] = nearCenter - up2 * nearHeight - right * nearWidth;
		corners[nbr] = nearCenter - up2 * nearHeight + right * nearWidth;
		corners[ntl] = nearCenter + up2 * nearHeight - right * nearWidth;
		corners[ntr] = nearCenter + up2 * nearHeight + right * nearWidth;
		corners[fbl] = farCenter  - up2 * farHeight  - right * farWidth;
		corners[fbr] = farCenter  - up2 * farHeight  + right * farWidth;
		corners[ftl] = farCenter  + up2 * farHeight  - right * farWidth;
		corners[ftr] = farCenter  + up2 * farHeight  + right * farWidth;

		// compute the six planes
		planes[TOP]		= Plane(corners[ntr],corners[ntl],corners[ftl]);
		planes[BOTTOM]	= Plane(corners[nbl],corners[nbr],corners[fbr]);
		planes[LEFT]	= Plane(corners[ntl],corners[nbl],corners[fbl]);
		planes[RIGHT]	= Plane(corners[nbr],corners[ntr],corners[fbr]);
		planes[NEAR]	= Plane(corners[ntl],corners[ntr],corners[nbr]);
		planes[FAR]		= Plane(corners[ftr],corners[ftl],corners[fbl]);
	}

	OrthoCamera::OrthoCamera(float left, float right, float bottom, float top, float near, float far, glm::vec3 lookAt, glm::vec3 pos, glm::vec3 up):
			CameraBase(lookAt, pos, up), left(left), right(right), bottom(bottom), top(top), near(near), far(far) { update(); }

	bool OrthoCamera::containsSphere(const glm::vec3& pos, float radius) const {
		for(int i = 0; i < 4; i++)
			if (planes[i].distance(pos) < -radius )
				return false;
		return true;
	}

	void OrthoCamera::setOrtho(float left, float right, float bottom, float top, float near, float far) {
		this->left = left;
		this->right = right;
		this->bottom = bottom;
		this->top = top;
		this->near = near;
		this->far = far;
	}

	void OrthoCamera::update() {
		// Set the V and P matrices
		view = glm::lookAt(pos, lookAt, up);
		projection = glm::ortho(left, right, bottom, top, near, far);
		if(yRot) {
			glm::mat4 rot(1.0);
			rot = glm::scale(rot, glm::vec3(1.f) /glm::vec3((right - left) / 2.f, (top - bottom) / 2.f, (far - near) / 2.f) );
			rot = glm::rotate(rot, yRot, up);
			rot = glm::scale(rot, glm::vec3((right - left) / 2.f, (top - bottom) / 2.f, (far - near) / 2.f) );
			projection *= rot;
		}
		viewProjection = projection * view;

		glm::vec3 dir   = glm::normalize(pos - lookAt); // The direction we're facing
		glm::vec3 right = glm::normalize(glm::cross(up, dir)); // Our right
		glm::vec3 up2   = glm::normalize(glm::cross(dir, right)); // Temporary "up", may differ from given up

		// the centers of the near and far planes
		glm::vec3 nearCenter = pos - dir * near;
		glm::vec3 farCenter  = pos - dir * far;

		corners[nbl] = nearCenter + up2 * this->bottom + right * this->left;
		corners[nbr] = nearCenter + up2 * this->bottom + right * this->right;
		corners[ntl] = nearCenter + up2 * this->top + right * this->left;
		corners[ntr] = nearCenter + up2 * this->top + right * this->right;
		corners[fbl] = farCenter  + up2 * this->bottom  + right * this->left;
		corners[fbr] = farCenter  + up2 * this->bottom  + right * this->right;
		corners[ftl] = farCenter  + up2 * this->top  + right * this->left;
		corners[ftr] = farCenter  + up2 * this->top  + right * this->right;

		// compute the six planes
		planes[0]	= Plane(corners[ntr],corners[ntl],corners[ftl]);
		planes[1]	= Plane(corners[nbl],corners[nbr],corners[fbr]);
		planes[2]	= Plane(corners[ntl],corners[nbl],corners[fbl]);
		planes[3]	= Plane(corners[nbr],corners[ntr],corners[fbr]);
	}

	glm::mat4 CameraBase::getMVP(glm::mat4 model) const { return viewProjection * model; }
	glm::mat4 CameraBase::getMV (glm::mat4 model) const { return view * model; }
	glm::mat4 CameraBase::getMVPFromMV(glm::mat4 modelView) const { return projection * modelView; }

	glm::vec3 CameraBase::getScreenPos(glm::vec3 in) {
		glm::vec4 ret(in.x,in.y,in.z,1);
		ret = viewProjection * ret;
		return glm::vec3(ret.x,ret.y,ret.z);
	}


}


