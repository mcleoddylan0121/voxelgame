/*
 * camera.h
 *
 *  Created on: Jul 16, 2015
 *      Author: Dylan
 */

#ifndef CAMERA_H_
#define CAMERA_H_

#include "vglm.h"
#include "shapes.h"

#include <vector>

namespace vox {

	struct CameraBase {
		glm::mat4 projection;
		glm::mat4 view;
		glm::mat4 viewProjection;
		glm::vec3 lookAt, pos, up;

		glm::mat4 getMVP(glm::mat4 model) const;
		glm::mat4 getMV (glm::mat4 model) const;
		// Not really useful, but who knows
		glm::mat4 getMVPFromMV(glm::mat4 modelView) const;

		void translate(float x, float y, float z);
		virtual bool containsPoint(const glm::vec3& pos) const { return containsSphere(pos, 0); }
		virtual bool containsSphere(const glm::vec3& pos, float radius) const = 0;

		virtual ~CameraBase() {}
		CameraBase(glm::vec3 lookAt, glm::vec3 pos, glm::vec3 up):
			lookAt(lookAt), pos(pos), up(up) {}

		glm::vec3 getScreenPos(glm::vec3 in);

		enum {
			NEAR = 0, FAR, TOP, BOTTOM, LEFT,
			RIGHT
		};

		//corners
		glm::vec3 corners[8];
		enum { ntl = 0,ntr,nbl,nbr,ftl,ftr,fbl,fbr };
	};

	struct Camera: public CameraBase {
		float fovY, aspectRatio, near, far;

		float tangent, nearWidth, nearHeight, farWidth, farHeight;

		Plane planes[6];

		bool containsSphere(const glm::vec3& pos, float radius) const;

		Camera(float fovY, float aspectRatio, float near, float far,
				glm::vec3 lookAt, glm::vec3 pos, glm::vec3 up);

		Camera() : Camera
			(3.1415f / 2, 4.f/3.f, 0.1f, 1000000.f,
			{1.f,1.f,1.f}, {0.f,0.f,0.f}, {0.f,1.f,0.f}) {}

		void update(); // recalculate view and viewProjection

		std::vector<glm::vec3> getCorners(float dist1, float dist2);
	};

	struct OrthoCamera: public CameraBase {
		float left, right, bottom, top, near, far;

		float yRot = 0;

		Plane planes[4];

		void setOrtho(float left, float right, float bottom, float top, float near, float far);

		OrthoCamera(float left, float right, float bottom, float top, float znear, float zfar,
					glm::vec3 lookAt, glm::vec3 pos, glm::vec3 up);
		OrthoCamera(): OrthoCamera(-400,400,-400,400,-400,400, {1,1,1}, {0,0,0}, {0,1,0}) {}

		void update(); // recalculate view and viewProjection
		void translate(float x, float y, float z);
		virtual bool containsSphere(const glm::vec3& pos, float radius) const;
	};

}



#endif /* CAMERA_H_ */
