/*
 * program.h
 *
 *  Created on: Jul 16, 2015
 *      Author: Dylan
 */

#ifndef PROGRAM_H_
#define PROGRAM_H_

#include "renderutils.h"
#include "camera.h"

#include <map>

namespace vox {

	class Program {
	private:
		std::map<GLint*, std::string> uniformNames;
		std::map<GLint*, std::string> uniformBuffers;
		std::string fileName;
		int vertexAttribs;
		bool geometryShader;
		void createMap(std::initializer_list<std::pair<GLint*, std::string>> uniformNames, std::initializer_list<std::pair<GLint*, std::string>> uniformBuffers);
	protected:
		Program(std::vector<std::string> vertexAttribs, const std::string& fileName, std::initializer_list<std::pair<GLint*, std::string>> uniformNames,
				std::initializer_list<std::pair<GLint*, std::string>> uniformBuffers, std::vector<std::string> fragShaderOutputs,
				bool geometryShader, std::vector<std::string> macros, std::vector<std::string> libs, std::vector<std::string> inject);

		// The name of the shader is without all directories or extensions (eg. "chunks")
		void setShader(const std::string& name, std::vector<std::string> fragShaderOutputs, bool geometryShader,
				std::vector<std::string> macros, std::vector<std::string> libs, std::vector<std::string> inject, std::vector<std::string> vAttribs);
	public:
		GLint name;
		virtual ~Program();
		virtual void beginRender();
		virtual void endRender();

		GLint getUniformLocation(std::string loc) { return glGetUniformLocation(name, loc.c_str()); } // There's not a huge reason to use this, but it's there anyway
	};

	template<typename Program_T> void uploadCameraMatrices(Program_T* program, const CameraBase* camera) {
		glUniformMatrix4fv(program->V,  1, GL_FALSE, &(camera->view[0][0]));
		glUniformMatrix4fv(program->P,  1, GL_FALSE, &(camera->projection[0][0]));
		glUniformMatrix4fv(program->VP, 1, GL_FALSE, &(camera->viewProjection[0][0]));
	}

	template<typename Program_T> void uploadCameraModelMatrices(Program_T* program, const CameraBase* camera, const glm::mat4 M) {
		glm::mat4 MV = camera->getMV(M);
		glm::mat4 MVP = camera->getMVP(M);
		glm::mat4 normalMatrix = glm::transpose(glm::inverse(MV));

		glUniformMatrix4fv(program->M,   1, GL_FALSE, &(M[0][0]));
		glUniformMatrix4fv(program->MV,  1, GL_FALSE, &(MV[0][0]));
		glUniformMatrix4fv(program->MVP, 1, GL_FALSE, &(MVP[0][0]));
		glUniformMatrix4fv(program->normalMatrix, 1, GL_FALSE, &(normalMatrix[0][0]));

		uploadCameraMatrices(program, camera);
	}

#define tMats M,V,P,MV,VP,MVP,normalMatrix
#define mkpr(x) std::make_pair(&x, #x)
#define tMatDef mkpr(M),mkpr(V),mkpr(P),mkpr(MV),mkpr(VP),mkpr(MVP),mkpr(normalMatrix)

#define WSUniforms normalLighting, normalVectors, voxelColorTex, voxelLightTex

	struct ChunkShaderProgram: public Program {
		GLint tMats;
		GLint WSUniforms;
		GLint worldTime;
		GLint occlusionCurve, ditherSampler;
		GLint lightData, tileData, sun, depthBiasMVP, fogColor, fogDensity;
		ChunkShaderProgram(const std::string& fileName, bool geometryShader = false, std::vector<std::string> macros = {},
				std::vector<std::string> libs = {}, std::vector<std::string> inject = {}, bool deferredRender = false);
	};

	struct EntityShaderProgram: public Program {
		GLint tMats;
		GLint WSUniforms;
		GLint worldTime;
		GLint cornerLights, lightLevel; // the light intensity of the 8 corners, or at the center
		GLint size;
		GLint occlusionCurve, ditherSampler;
		GLint lightData, tileData, sun, depthBiasMVP, fogColor, fogDensity;
		EntityShaderProgram(const std::string& fileName, bool geometryShader = false, std::vector<std::string> macros = {},
				std::vector<std::string> libs = {}, std::vector<std::string> inject = {}, bool deferredRender = false);
	};

	struct DeferredShaderProgram: public Program {
		GLint diffuseTex, gBufferTex, normalTex, lightTex, bloomTex, skyTex, cameraPos, lightData, tileData, sun, skyBox, shadowMap, fogColor, fogDensity, ssaoTex;
		GLint IV, IP, IVP, depthBiasMVP;
		DeferredShaderProgram(const std::string& fileName, bool geometryShader = false, std::vector<std::string> macros = {}, std::vector<std::string> libs = {}, std::vector<std::string> inject = {});
	};

	struct ShadowShaderProgram: public Program {
		GLint tMats, sunVPs, cameraPos;
		ShadowShaderProgram(const std::string& fileName, bool geometryShader = false, std::vector<std::string> macros = {}, std::vector<std::string> libs = {}, std::vector<std::string> inject = {});
	};

	struct RenderTextureProgram: public Program {
		GLint texture, size, position, depth, rot, color, screenSize, UVstart, UVsize;
		RenderTextureProgram(const std::string& fileName, bool geometryShader = false, std::vector<std::string> macros = {}, std::vector<std::string> libs = {}, std::vector<std::string> inject = {});
	};

	struct RenderSkyShaderProgram: public Program {
		GLint tex, tex2, MVP, worldTimeDiff, depth, color;
		RenderSkyShaderProgram(const std::string& fileName, bool geometryShader = false, std::vector<std::string> macros = {}, std::vector<std::string> libs = {}, std::vector<std::string> inject = {});
	};

	struct RenderToSkyShaderProgram: public Program { // For rendering to the sky
		GLint sunPos, faceDirection, layer, model, cameraHeight, rayleighBrightness,
		mieBrightness, sunBrightness, intensity, rayleighStrength, mieStrength,
		rayleighCollectionPower, mieCollectionPower, scatterStrength, mieDistribution,
		surfaceHeight;
		RenderToSkyShaderProgram(const std::string& fileName, bool geometryShader = false, std::vector<std::string> macros = {}, std::vector<std::string> libs = {}, std::vector<std::string> inject = {});
	};

	struct CloudShaderProgram: public Program { // rendering clouds to the sky
		GLint face, tMats, worldTime, sundir;
		GLint phaseMod, phaseMul, phaseAdd, brightnessMul, maxAlpha;
		CloudShaderProgram(const std::string& fileName, bool geometryShader = false, std::vector<std::string> macros = {}, std::vector<std::string> libs = {}, std::vector<std::string> inject = {});
	};

	// Used for rendering things high in the sky, like the moon, or the sun's lens flare
	struct SkyObjectShaderProgram: public Program {
		GLint tex, face, intensity, tMats;
		SkyObjectShaderProgram(const std::string& fileName, bool geometryShader = false, std::vector<std::string> macros = {}, std::vector<std::string> libs = {}, std::vector<std::string> inject = {});
	};

	struct GUIShaderProgram: public Program {
		GLint textureAtlas, offset, textureAtlasSize, screenSize, stretch, hue, layer;
		GUIShaderProgram(const std::string& fileName, bool geometryShader = false, std::vector<std::string> macros = {}, std::vector<std::string> libs = {}, std::vector<std::string> inject = {});
	};

	struct ChunkLoDShaderProgram: public Program {
		GLint tMats, worldTime, occlusionCurve, fogColor, fogDensity;
		ChunkLoDShaderProgram(const std::string& fileName, bool geometryShader = false, std::vector<std::string> macros = {}, std::vector<std::string> libs = {}, std::vector<std::string> inject = {});
	};

	struct TextShaderProgram: public Program {
		GLint textureAtlas, color, screenSize, textureSize, pos, size, layer;
		TextShaderProgram(const std::string& fileName, bool geometryShader = false, std::vector<std::string> macros = {}, std::vector<std::string> libs = {}, std::vector<std::string> inject = {});
	};

	struct TexturedMeshShaderProgram: public Program {
		GLint tex, tMats, depth, fogColor, fogDensity, color;
		TexturedMeshShaderProgram(const std::string& fileName, bool geometryShader = false, std::vector<std::string> macros = {}, std::vector<std::string> libs = {}, std::vector<std::string> inject = {});
	};

	struct ColoredShaderProgram: public Program {
		GLint tMats, color;
		ColoredShaderProgram(const std::string& fileName, bool geometryShader = false, std::vector<std::string> macros = {}, std::vector<std::string> libs = {}, std::vector<std::string> inject = {});
	};

	struct LiquidShaderProgram: public Program {
		GLint tMats, timeDiff, tex1, tex2, occlusionCurve, ditherSampler, fogColor, fogDensity, camPos; // 2 textures needed for smoothing between 1 frame and the next
		LiquidShaderProgram(const std::string& fileName, bool geometryShader = false, std::vector<std::string> macros = {}, std::vector<std::string> libs = {}, std::vector<std::string> inject = {});
	};

	struct RenderToLiquidShaderProgram: public Program {
		GLint _time_, UVstart, UVsize;
		RenderToLiquidShaderProgram(const std::string& fileName, bool geometryShader = false, std::vector<std::string> macros = {}, std::vector<std::string> libs = {}, std::vector<std::string> inject = {});
	};

	struct InstancedParticleShaderProgram: public Program {
		GLint tex, V, P, VP;

		InstancedParticleShaderProgram(const std::string& fileName, bool geometryShader = false, std::vector<std::string> macros = {}, std::vector<std::string> libs = {}, std::vector<std::string> inject = {});
	};

	struct InstancedCloudParticleShaderProgram: public Program {
		GLint tex, time, color, UVsize, UVgap, scale, V, P, VP, camPos, camLookXZRotationMatrix, aspectRatio, globalColor;

		InstancedCloudParticleShaderProgram(const std::string& fileName, bool geometryShader = false, std::vector<std::string> macros = {}, std::vector<std::string> libs = {}, std::vector<std::string> inject = {});
	};
}



#endif /* PROGRAM_H_ */
