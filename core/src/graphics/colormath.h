/*
 * colormath.h
 *
 *  Created on: May 3, 2015
 *      Author: Dani
 */

#ifndef COLORMATH_H_
#define COLORMATH_H_

#include "util.h"

#include <vector>
#include <glm/glm.hpp>

namespace vox {
	// This is stored in the Hunter LAB color space for ease of calculation
	// Note that the float constructors take inputs in [0, 1], while u8 constructors take [0, 255]
	class Color {
	private:
		/*
		 * Luminosity, red-green opposition, yellow-blue opposition
		 *
		 * luminosity: high = bright, low = dark
		 * alpha: high = red,    low = green
		 * beta:  high = yellow, low = blue
		 */
		glm::vec3 _lab;

		float _alpha;
		Color(glm::u8vec4 color);

		glm::vec3 _rgbToLAB(glm::u8vec3 rgb) const;
		glm::u8vec3 _labToRGB(glm::vec3 lab) const;
		glm::u8vec3 _fastLABToRGB(glm::vec3 lab) const;

	public:
		typedef glm::u8vec4 GPUColor; // The color to give to the gpu, stored in rgba [0, 255]
		typedef glm::vec4 HighpColor; // high precision

		/*
		 * Creates an empty (white) color.
		 */
		Color();

		Color(const Color& c);
		~Color();

		/*
		 * These constructors take the rgb or rgba with bound of [0, 1]
		 */
		Color(float r, float b, float g, float a = 1.f);
		Color(glm::vec3 _color);
		Color(glm::vec4 _color);

		/*
		 * Adds two colors together using the average of their LAB values.
		 * This is equivalent to calling Color::mix(lhs, rhs, 0.5f);
		 */
		friend Color operator + (const Color& lhs, const Color& rhs);

		/*
		 * Scales a color by the amount specified. Since LAB is a linear space configured to our eyes,
		 * this will make the color more dull or more vibrant depending on the weight specified. A
		 * weight less than 1 would make it more dull, while a weight greater than 1 would make it brighter.
		 */
		friend Color operator * (float lhs, const Color& rhs);
		friend Color operator * (const Color& lhs, float rhs);

		static glm::vec3 RGBFromTemperature(float kelvin);

		/*
		 * Gets the color as a u8vec4, as preferred by the GPU.
		 */
		GPUColor toGPUColor() const;

		/*
		 * Creates a color using rgba values in the range [0, 255]
		 * Be careful, as this does need to call _rngToLAB which is quite expensive
		 */
		static Color u8Color(unsigned char r, unsigned char g, unsigned char b, unsigned char a = 255);

		/*
		 * Creates a color using the LAB color space.
		 * This is not very computationally expensive.
		 */
		static Color labColor(float l, float a, float b, float alpha = 1.f);

		/*
		 * Adding two colors assumes that they are added in equal quantities.
		 * Mix adds colors in unequal quantities as determined by the weights, whose
		 * values <i>should</i> conform to [0, 1], but they don't have to.
		 */
		static Color mix(const Color& c1, float w1, const Color& c2, float w2);
		/*
		 * Mixes the colors using w1 as w1 and w2 as 1 - w1. This means that
		 * w1 should be normalized. It is the weight of the first color.
		 */
		static Color mix(const Color& c1, const Color& c2, float w1);
		/*
		 * Mixes all the colors into one kind of congealed blob.
		 */
		static Color mix(const std::vector<Color>& colors);
	} const BLACK (0, 0, 0),
			WHITE (1, 1, 1),
			RED   (1, 0, 0),
			GREEN (0, 1, 0),
			BLUE  (0, 0, 1),
			YELLOW(1, 1, 0),
			MAGENTA(1, 0, 1),
			CYAN  (0, 1, 1),
			BROWN (0.38f, 0.2f, 0.09f),
			GRAY  (169 / 255.f, 161 / 255.f, 140 / 255.f),
			PINK  (1.000, 0.412, 0.706);

}

#endif /* COLORMATH_H_ */
