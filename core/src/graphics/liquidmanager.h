/*
 * LiquidHandler.h
 *
 *  Created on: Sep 5, 2015
 *      Author: Dylan
 */

#ifndef SRC_GRAPHICS_LIQUIDMANAGER_H_
#define SRC_GRAPHICS_LIQUIDMANAGER_H_

#include "noise.h"
#include "coords.h"
#include "colormath.h"
#include "glbuffer.h"
#include "shapes.h"

#include <unordered_map>

namespace vox {

	// the guide to end them all:

	// solid liquid rendering: one color
	// textured liquid rendering: animated, preset textures
	// procedural liquid rendering: animated, tiled, procedural noise-based textures

	class FrameBuffer;
	class ChunkMeshBuffer;
	class LiquidShaderProgram;
	class RenderToLiquidShaderProgram;
	class Texture;

	typedef int LiquidID;

	struct Liquid {
		LiquidID ID;
		std::string name;

		// SOLID
		Color::GPUColor color;

		// PROCEDURAL + TEXTURED
		glm::vec2 UV; // where to render this in the texture atlas

		// PROCEDURAL
		std::vector<std::string> glslFunction;   // function that writes to the texture atlas
		RenderToLiquidShaderProgram* glslShader; // glsl shader containing glslFunction

		// TEXTURED
		int numTextureFrames;    // number frames in texture
		int curTextureFrame = 0; // current frame in texture
		Texture* tex;		     // liquid texture

		Liquid(LiquidID ID, std::string name, Color::GPUColor color, std::vector<std::string> glslFunction, RenderToLiquidShaderProgram* glslShader, glm::vec2 UV, Texture* tex, int numTextureFrames):
			ID(ID), name(name), color(color), UV(UV), glslFunction(glslFunction), glslShader(glslShader), numTextureFrames(numTextureFrames), tex(tex) {}

		Liquid(const Liquid& l): Liquid(l.ID,l.name,l.color,l.glslFunction,l.glslShader,l.UV,l.tex,l.numTextureFrames) {}

		virtual ~Liquid();
	};

	enum LiquidRenderingType {
		SOLID, TEXTURED, PROCEDURAL
	};

	extern std::unordered_map<std::string, int> liquidRenderingTypeMap;

	extern std::string liquidRenderingTypeDefines[3];

	struct LiquidFrameSnapshot {
		float time;
	};

	// Manages the liquid textures and shaders only! Does not manage the geometry, that's another guy's job! (TODO: Add that guy)
	class LiquidManager {
	private:
		std::unordered_map<std::string, Liquid*> nameMap; // look up liquidID by name
		std::unordered_map<LiquidID,    Liquid*> idTable; // look up liquid by ID
	public:
		const int renderType;

		int noiseIndividualTextureSize = 64; // individual size of each liquid's noise, for procedural liquids
		int noiseTextureSize = 2; // n x n texture for storing all liquids, so a 2x2 one would store 4 liquids

		FrameBuffer* frames[3]; // last frame, current frame, next frame, this is what we render to
		Texture* liquidTextures[3]; // Just like the above, but this is what we render
		LiquidFrameSnapshot snapshots[3]; // hold onto some useful information about when liquids were rendered, here

		LiquidManager(int renderType = PROCEDURAL);
		~LiquidManager();

		void beginRendering(size_t bind1, size_t bind2, LiquidShaderProgram* program); // do this at the start of rendering, binds the textures

		void renderLiquidToFrameBuffer(Liquid* l);

		void tick(float delta);

		// render to the framebuffer (only renders some of it every tick, for performance reasons)
		// if we use solid/textured liquid rendering, this does nothing
		void renderToFrameBuffer();

		// get vertex data for a liquid, so we can actually render said liquid
		Quad<glm::u16vec2> getLiquidVertexData(LiquidID l, glm::vec2 UVstart, glm::vec2 UVsize);

		Liquid* getLiquid(LiquidID ID) { return idTable[ID]; }
		Liquid* getLiquid(std::string name) { return nameMap[name]; }

		Liquid* addLiquid(int ID, std::string name, Color::GPUColor color, std::vector<std::string> glslFunction);

		void removeLiquid(LiquidID ID);
		void removeLiquid(std::string name) { removeLiquid(nameMap[name]->ID); }

		GLBuffer<glm::vec2>* liquidVertexBuffer; // I live a pointless and futile existence
	};

}


#endif /* SRC_GRAPHICS_LIQUIDMANAGER_H_ */
