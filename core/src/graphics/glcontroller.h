/*
 * glcontroller.h
 *
 *  Created on: Mar 22, 2015
 *      Author: Dylan
 */

#ifndef GLCONTROLLER_H_
#define GLCONTROLLER_H_

#include "gameloop.h"
#include "console.h"
#include "vglm.h"
#include "renderutils.h"
#include "shapes.h"
#include "glanalytics.h"

#include <unordered_map>
#include <initializer_list>

namespace vox {

	class ChunkMeshBuffer; class EntityMeshBuffer; class LoDBuffer; class Asset; class ChunkLoDShaderProgram;
	struct CameraBase; class FrameBuffer; class Program; class FontRenderObject; class TextShaderProgram;
	template<typename T> class StackBuffer; class ColorBuffer;
	class RenderTextureProgram; class EntityShaderProgram;
	class ShadowShaderProgram; class ChunkShaderProgram;

	// Using viewport size for now until we make a setting for this
	//const glm::vec2 SCREENSHOT_SIZE = {1024, 768};

	class VoxelMesh;

	struct TextureBinding {
		Texture* texture;
		int channel;
		GLint uniform;
	};

	class GLController {
	private:
		GLuint vAID; // Vertex Array ID

		// Current Bindings
		FrameBuffer* currentFrameBuffer;
		Program* currentProgram;

		std::unordered_map<GLenum, GLuint> boundBuffers;
	public:
		bool CUBEMAP_FIX;

		StackBuffer<glm::vec3>* screenBuffer;
		StackBuffer<glm::vec3>* skyBoxBuffer;
		StackBuffer<float>* skyBoxTexLayerBuffer;

		StackBuffer<Line<glm::u8vec3>>* cubeWireframe;

		RenderTextureProgram* screenProgram;
		FrameBuffer* windowFBO;
		GLController();
		void init();
		virtual ~GLController();

		void bindBuffer(GLenum buffer, GLuint buf);
		void bindFrameBuffer(FrameBuffer* fbo);
		void unbindFrameBuffer(); // Render to the screen
		void bindTexture(Texture* t, int channel);
		void unbindTexture(Texture* t, int channel);

		void bindTexture(Texture* t, int channel, GLint uniform); // bind texture, set uniform
		void bindTextures(std::vector<TextureBinding> textures);

		void bindProgram(Program* p);
		void useScreenProgram();

		/* When rendering, we assume the program is bound and vertex attribs have been enabled */
		void render(ChunkMeshBuffer* v, ChunkShaderProgram* p, const CameraBase* viewCam, glm::mat4 model);
		void render(LoDMeshBuffer* v, ChunkLoDShaderProgram* p, const CameraBase* viewCam, glm::mat4 model);
		void render(EntityMeshBuffer* v, EntityShaderProgram* p, const CameraBase* viewCam, glm::mat4 model);

		// positions and sizes are [0, 1]
		void render(RenderTextureProgram* p, Texture* t, glm::vec2 pos = glm::vec2(0,0), glm::vec2 size = glm::vec2(1,1),
				float depth = 0.f, float rot = 0.f, glm::vec4 color = glm::vec4(1), glm::vec2 UVstart = glm::vec2(0), glm::vec2 UVsize = glm::vec2(1));

		void renderText(TextShaderProgram* p, FontRenderObject* f, glm::ivec2 pos, glm::vec2 size, glm::vec4 color, int layer);

		// writes source to target, used for downsampling, and possibly other things
		void sampleFramebuffer(FrameBuffer* source, FrameBuffer* target, GLenum mask = GL_COLOR_BUFFER_BIT, GLenum interpolate = GL_LINEAR, GLenum read = GL_COLOR_ATTACHMENT0, GLenum draw = GL_COLOR_ATTACHMENT0);

		void renderShadows(ChunkMeshBuffer* v, ShadowShaderProgram* p, const CameraBase* cam, glm::mat4 model);
		void renderShadows(EntityMeshBuffer* v, ShadowShaderProgram* p, const CameraBase* cam, glm::mat4 model);

		void renderGameStates(FrameBuffer* target);
		void takeScreenShot();
		void saveFrameBufferOutput(FrameBuffer* buf, const std::string& path);

		bool handle_error(GLenum error, const char * func, int line);

		GLAnalytics analytics;

		std::unordered_map<GLenum, int> colorFormatSizes;

		int getColorFormatSize(GLenum colorFormat);
	};

#define handleGLError() mGL->handle_error(glGetError(), __FILE__, __LINE__);

	extern GLController * mGL;
}

#endif /* GLCONTROLLER_H_ */
