/*
 * framebuffer.cpp
 *
 *  Created on: Jul 16, 2015
 *      Author: Dylan
 */

#include "framebuffer.h"
#include "glcontroller.h"
#include "sdlcontroller.h"

namespace vox {

	void FrameBuffer::clear(glm::vec4 color) {
		glClearColor(color.x,color.y,color.z,color.w);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}

	void DepthBuffer::clear() {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glDrawBuffer(GL_NONE); // No color buffer is drawn to.
	}

	void CascadedShadowMap::clear() {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glDrawBuffer(GL_NONE); // No color buffer is drawn to.
	}

	GLenum arr[] = {DeferredRenderer::COLOR_ATTACH, DeferredRenderer::G_BUFFER_ATTACH, DeferredRenderer::NORMAL_ATTACH, DeferredRenderer::LIGHT_ATTACH};
	void DeferredRenderer::setDrawBuffers(std::vector<int> buf) {
		GLenum drawBuffers[buf.size()];
		for(unsigned int i = 0; i < buf.size(); i++) {
			drawBuffers[i] = arr[buf[i]];
		}
		glDrawBuffers(buf.size(), drawBuffers);
	}

	void DeferredRenderer::clear() {
		setDrawBuffers({COLOR, NORMALS, LIGHT, G_BUFFER});
		glClearColor(0.f, 0.f, 0.f, 0.f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}

	FrameBuffer::FrameBuffer(glm::ivec2 size, int outputs): size(size), outputs(outputs), viewportStart(0,0), viewportSize(size) {
		glGenFramebuffers(1, &name);
		glBindFramebuffer(GL_FRAMEBUFFER, name);
		target = nullptr;
	}

	void FrameBuffer::initTextures() {
		for(int i = 0; i < outputs; i++)
			target[i].size = size;
	}

	FrameBuffer::FrameBuffer() {
		size = {mSDL->viewportWidth, mSDL->viewportHeight};
		name = 0;
		outputs = 1;
		target = new Texture(0, size);
	}

	FrameBuffer* FrameBuffer::getWindow() {
		return new FrameBuffer();
	}

	int texture2DHelper(glm::ivec2 size, bool multisample, int MSAALevel, Texture* tex, GLenum internalformat, bool border, GLenum externalformat, GLenum type, void* data) {
		glGenTextures(1, &(tex->tex));
		glBindTexture(tex->DEFAULT_BINDING_LOCATION, tex->tex);
		if(multisample) {
			glTexImage2DMultisample(tex->DEFAULT_BINDING_LOCATION, MSAALevel, internalformat, size.x, size.y, true);
			return MSAALevel * mGL->getColorFormatSize(internalformat) * size.x * size.y;
		}
		else {
			glTexImage2D(tex->DEFAULT_BINDING_LOCATION, 0, internalformat, size.x, size.y, border, externalformat, type, data);
		    glTexParameteri(tex->DEFAULT_BINDING_LOCATION, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		    glTexParameteri(tex->DEFAULT_BINDING_LOCATION, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(tex->DEFAULT_BINDING_LOCATION, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(tex->DEFAULT_BINDING_LOCATION, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			return mGL->getColorFormatSize(internalformat) * size.x * size.y;
		}
	}

	ColorBuffer::ColorBuffer(glm::ivec2 size, GLenum colorFormat, bool enableDepthBuffer, GLenum minFilter, GLenum magFilter, bool multisample, int MSAALevel) : FrameBuffer(size, 1), enableDepthBuffer(enableDepthBuffer) {
		target = new Texture[outputs];
		initTextures();

		if(multisample) target[0].DEFAULT_BINDING_LOCATION = GL_TEXTURE_2D_MULTISAMPLE;

		glGenTextures(1, &target[0].tex);
		glBindTexture(target[0].DEFAULT_BINDING_LOCATION, target[0].tex);
		memUsage += texture2DHelper(size, multisample, MSAALevel, target, colorFormat, false, GL_RGBA, GL_UNSIGNED_BYTE, NULL);

		// Poor filtering. Needed !
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, magFilter);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, minFilter);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

		if(enableDepthBuffer) {
			// Depth buffer
			glGenRenderbuffers(1, &depthBuffer);
			glBindRenderbuffer(GL_RENDERBUFFER, depthBuffer);
			if(!multisample)
				glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, size.x, size.y);
			else
				glRenderbufferStorageMultisample(GL_RENDERBUFFER, MSAALevel, GL_DEPTH_COMPONENT, size.x, size.y);
			glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthBuffer);
		}

		// Set "renderedTexture" as our colour attachement #0
		glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, target[0].tex, 0);

		// Set the list of draw buffers.
		GLenum DrawBuffers[1] = {GL_COLOR_ATTACHMENT0};
		glDrawBuffers(1, DrawBuffers); // "1" is the size of DrawBuffers
		// Always check that our framebuffer is ok
		if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
			println(GLERROR, "Framebuffer Initialized Improperly!");

		mGL->analytics.registerMemory(memUsage);
	}

	CubeMapColorBuffer::CubeMapColorBuffer(glm::ivec2 size): FrameBuffer(size, 1) {
		if(mGL->CUBEMAP_FIX) { // This computer does not support cubemaps, so we'll use a texture2DArray to emulate one
			target = new Texture2DArray(0,size,6);
			initTextures();

			glGenTextures(1, &target->tex);
			glBindTexture(GL_TEXTURE_2D_ARRAY, target->tex);

			glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_RGBA16F,
						 size.x,size.y,6,
						 GL_FALSE, GL_RGBA, GL_FLOAT, NULL );

			memUsage += 6 * mGL->getColorFormatSize(GL_RGBA16F) * size.x * size.y;

			glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
			glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
			glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

			glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, target->tex, 0);

			glDrawBuffer(GL_COLOR_ATTACHMENT0);

		} else {
			target = new CubeMap();
			initTextures();
			glGenTextures(1, &target->tex);
			glBindTexture(GL_TEXTURE_CUBE_MAP, target->tex);

			glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

			for(int i = 0; i < 6; i++)
				glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGBA16F, size.x, size.y, 0, GL_RGBA, GL_HALF_FLOAT, NULL);

			memUsage += 6 * mGL->getColorFormatSize(GL_RGBA16F) * size.x * size.y;

			mGL->unbindTexture(target, 0);

			for(int i = 0; i < 6; i++)
				glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, target->tex, 0);
		}

		// Always check that our framebuffer is ok
		if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
			printlnf(GLERROR, "Framebuffer Initialized Improperly! %i", glCheckFramebufferStatus(GL_FRAMEBUFFER));

		mGL->analytics.registerMemory(memUsage);
	}

	void CubeMapColorBuffer::clear() {
		glClearColor(0.0,0.0,0.0,0.0);
		glClear(GL_COLOR_BUFFER_BIT);
	}

	void CubeMapColorBuffer::selectFace(int face) {
		if(mGL->CUBEMAP_FIX) {
			glFramebufferTextureLayer(
				GL_FRAMEBUFFER,
				GL_COLOR_ATTACHMENT0,
				target->tex, 0, face
			);
		} else {
			glDrawBuffer(GL_COLOR_ATTACHMENT0 + face);
		}
	}

	CubeMapColorBuffer::~CubeMapColorBuffer() {}

	DepthBuffer::DepthBuffer(glm::ivec2 size): FrameBuffer(size, 1) {
		target = new Texture[outputs];
		initTextures();
		glGenTextures(1, &target[0].tex);
		glBindTexture(GL_TEXTURE_2D, target[0].tex);
		// Give an empty image to OpenGL ( the last "0" )
		glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT16, size.x, size.y, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, 0);

		memUsage += mGL->getColorFormatSize(GL_DEPTH_COMPONENT16) * size.x * size.y;

		// Poor filtering. Needed !
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
		glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, target[0].tex, 0);
		glDrawBuffer(GL_NONE);

		// Always check that our framebuffer is ok
		if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
			printlnf(GLERROR, "Framebuffer Initialized Improperly! %i", glCheckFramebufferStatus(GL_FRAMEBUFFER));

		mGL->analytics.registerMemory(memUsage);
	}

	CascadedShadowMap::CascadedShadowMap(glm::ivec2 size, int cascades): FrameBuffer(size, 1), cascades(cascades) {
		target = new Texture2DArray(0, size, cascades);
		initTextures();
		glGenTextures(1, &target[0].tex);
		glBindTexture(GL_TEXTURE_2D_ARRAY, target[0].tex);
		// Give an empty image to OpenGL ( the last "0" )
		glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_DEPTH_COMPONENT16,
					 size.x,size.y,cascades,
					 GL_FALSE, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, 0 );

		memUsage += cascades * mGL->getColorFormatSize(GL_DEPTH_COMPONENT16) * size.x * size.y;

		// Poor filtering. Needed !
		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_BORDER);
		glm::vec4 borderDepth = {1,0,0,0};
		glTexParameterfv(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_BORDER_COLOR, &borderDepth[0]);
		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
		glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, target[0].tex, 0);
		glDrawBuffer(GL_NONE);

		GLenum fboStatus = glCheckFramebufferStatus(GL_FRAMEBUFFER);
		if (fboStatus != GL_FRAMEBUFFER_COMPLETE) {
			printlnf(GLERROR, "CascadedShadowMap: FrameBuffer incomplete: 0x%x", fboStatus);
			exit(1);
		}

		mGL->analytics.registerMemory(memUsage);
	}

	void CascadedShadowMap::selectCascade(int cascade) {
		glFramebufferTextureLayer(
			GL_FRAMEBUFFER,
			GL_DEPTH_ATTACHMENT,
			target[0].tex, 0, cascade
		);
	}

	CascadedShadowMap::~CascadedShadowMap() {

	}

	DeferredRenderer::DeferredRenderer(glm::ivec2 size, bool multisample, int MSAALevel): FrameBuffer(size, 4) {
		target = new Texture[outputs];

		if(multisample) {
			for(int i = 0; i < 4; i++) {
				target[i].DEFAULT_BINDING_LOCATION = GL_TEXTURE_2D_MULTISAMPLE;
			}
		}

		memUsage += texture2DHelper(size, multisample, MSAALevel, &(target[COLOR]),    GL_RGBA8,   false, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
		memUsage += texture2DHelper(size, multisample, MSAALevel, &(target[NORMALS]),  GL_RGBA8,   false, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
		memUsage += texture2DHelper(size, multisample, MSAALevel, &(target[LIGHT]),    GL_RGBA16F, false, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
		memUsage += texture2DHelper(size, multisample, MSAALevel, &(target[G_BUFFER]), GL_RGBA16F, false, GL_RGBA, GL_UNSIGNED_BYTE, NULL);

		// Generate and bind the texture for depth rendering
		// Depth buffer
		glGenRenderbuffers(1, &depthBuffer);
		glBindRenderbuffer(GL_RENDERBUFFER, depthBuffer);
		if(!multisample)
			glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, size.x, size.y);
		else
			glRenderbufferStorageMultisample(GL_RENDERBUFFER, MSAALevel, GL_DEPTH_COMPONENT, size.x, size.y);

		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthBuffer);

		// Attach the textures to the FBO
		glFramebufferTexture2D(GL_FRAMEBUFFER, COLOR_ATTACH,        target[COLOR].DEFAULT_BINDING_LOCATION,   target[COLOR].tex, 0);
		glFramebufferTexture2D(GL_FRAMEBUFFER, NORMAL_ATTACH,       target[NORMALS].DEFAULT_BINDING_LOCATION, target[NORMALS].tex, 0);
		glFramebufferTexture2D(GL_FRAMEBUFFER, LIGHT_ATTACH,        target[LIGHT].DEFAULT_BINDING_LOCATION,   target[LIGHT].tex, 0);
		glFramebufferTexture2D(GL_FRAMEBUFFER, G_BUFFER_ATTACH,     target[G_BUFFER].DEFAULT_BINDING_LOCATION,target[G_BUFFER].tex, 0);

		GLenum windowBuffClear[] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2, GL_COLOR_ATTACHMENT3 };
		glDrawBuffers(4, windowBuffClear); // Select all buffers

		GLenum fboStatus = glCheckFramebufferStatus(GL_FRAMEBUFFER);
		if (fboStatus != GL_FRAMEBUFFER_COMPLETE) {
			printlnf(GLERROR, "DeferredRenderer: FrameBuffer incomplete: 0x%x", fboStatus);
			exit(1);
		}

		mGL->analytics.registerMemory(memUsage);
	}

	FrameBuffer::~FrameBuffer() {
	    glDeleteFramebuffers(1, &name);
	    GLuint* texes = new GLuint[outputs];
	    for(int i = 0; i < outputs; i++) texes[i] = target[i].tex;
	    glDeleteTextures(outputs, texes);
	    delete[] texes;

	    mGL->analytics.freeMemory(memUsage);
	}

	ColorBuffer::~ColorBuffer() {
		if(enableDepthBuffer)
			glDeleteRenderbuffers(1, &depthBuffer);
	}

	DepthBuffer::~DepthBuffer() {}


}

