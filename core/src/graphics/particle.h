/*
 * particle.h
 *
 *  Created on: Sep 7, 2015
 *      Author: Dylan
 */

#ifndef SRC_GRAPHICS_PARTICLE_H_
#define SRC_GRAPHICS_PARTICLE_H_

#include "vglm.h"
#include "voxelmesh.h"
#include "miscmesh.h"
#include "renderutils.h"
#include "program.h"

namespace vox {

	template<typename Billboard_T> struct BillboardSort {
		WorldCoordinate cameraPos;
		bool operator ()(Billboard_T a, Billboard_T b) {
			return glm::dot(cameraPos - a.pos, cameraPos - a.pos) > glm::dot(cameraPos - b.pos, cameraPos - b.pos); // choose the farther one, we want to render that first.
		}
	};

	// High memory usage, high versatility (25 bytes)
	struct InstancedParticleVertex {
		glm::vec3 pos; // center of the particle
		glm::u16vec2 UVstart; // bottomleft of the UV, normalized
		glm::u16vec2 UVsize; // topright of the UV, normalized
		glm::u8vec4 color; // normalized
		glm::vec2 scale;
	};

	// Low memory == maximum fun! (13 bytes)
	struct InstancedCloudParticleVertex {
		glm::vec3 pos; // center of particle in worldspace
		uint8_t textureIndex; // which cloud texture texture to use
	};

	class ParticleManager {
	public:
		std::vector<BillboardDisplayObject> billboards;
		std::vector<EntityMeshDisplayObject> meshes;

		int meshCount = 0, billboardCount = 0;

		Texture* billboardTexture;

		void storeForRender(BillboardDisplayObject b) { billboards.push_back(b); }
		void storeForRender(EntityMeshDisplayObject e) { meshes.push_back(e); }

		ParticleManager(Texture* billboardTexture);
		virtual ~ParticleManager() { delete billboardTexture; }

		virtual void prepareForRender(WorldCoordinate cameraPos) = 0; // Get ready to render, put everything into vertex buffers and stream the data (can render multiple times after preparing a single time)

		// billboards are streamed, but meshes are not
		virtual void renderBillboards(const Camera& cam) = 0; // we have our own specialized render for this
		void renderMeshes(EntityMeshRenderer* render); // we use someone else's renderer for this
	};

	class InstancedParticleManager: public ParticleManager {
	public:
		const int MAX_PARTICLES = 1600;

		GLBuffer<glm::vec2>* quadBuffer;
		InstancedParticleShaderProgram* billboardShader;
		ArrayBuffer<InstancedParticleVertex>* streamedBillboardBuffer;
		void prepareForRender(WorldCoordinate cameraPos); // depth sort billboards, put into instanced buffer
		void renderBillboards(const Camera& cam);

		InstancedParticleManager(Texture* billboardTexture);
		~InstancedParticleManager() { delete quadBuffer; }
	};


	class CloudParticleManager {
	public:
		std::vector<CloudParticleDisplayObject> cloudParticles;
		Texture* billboardTexture;

		CloudParticleManager(Texture* billboardTexture): billboardTexture(billboardTexture) {}
		virtual ~CloudParticleManager() { delete billboardTexture; }

		void storeForRender(CloudParticleDisplayObject p) { cloudParticles.push_back(p); }

		virtual void prepareForRender(WorldCoordinate cameraPos) = 0;
		virtual void render(const Camera& cam) = 0;
	};

	class InstancedCloudParticleManager: public CloudParticleManager {
	public:
		const int MAX_PARTICLES = 16000;

		GLBuffer<glm::vec2>* quadBuffer;
		InstancedCloudParticleShaderProgram* billboardShader;
		ArrayBuffer<InstancedCloudParticleVertex>* streamedBillboardBuffer;
		void prepareForRender(WorldCoordinate cameraPos);
		void render(const Camera& cam);

		InstancedCloudParticleManager(Texture* billboardTexture);
		~InstancedCloudParticleManager() { delete quadBuffer; }
	};

	// TODO: do
	/*
	class NonInstancedParticleManager {

	};

	class NonInstancedCloudParticleManager {

	};
	*/


}

#endif /* SRC_GRAPHICS_PARTICLE_H_ */
