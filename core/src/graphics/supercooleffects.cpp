/*
 * supercooleffects.cpp
 *
 *  Created on: Jul 25, 2016
 *      Author: Dylan
 */

#include "supercooleffects.h"
#include "distortscreen.h"
#include "framebuffer.h"
#include "program.h"
#include "glcontroller.h"
#include "glbuffer.h"
#include "noise.h"
#include "gpunoise.h"
namespace vox {

	RollingWaveDistortionEffectHandler::RollingWaveDistortionEffectHandler() {
		Rand tempRand;
		noise::Noise* xNoise = noise::Noise::readFile("terrifying distortion mode", tempRand, "noise_x");
		noise::Noise* yNoise = noise::Noise::readFile("terrifying distortion mode", tempRand, "noise_y");

		shader = new RenderTextureProgram("distort/noise", false, {"ACK"}, {"noise_lib"}, {noise::getGPUNoiseString("noise_x", 3, xNoise), noise::getGPUNoiseString("noise_y", 3, yNoise)} );
	}

	void RollingWaveDistortionEffectHandler::tick(float delta) {

	}

	void RollingWaveDistortionEffectHandler::render(ScreenDistortionHandler* handler, DeferredRenderer* renderer, float worldTime) {

		handler->beginDistortionTextureRender();

		glDisable(GL_DEPTH_TEST);

		mGL->bindProgram(shader);
		shader->beginRender();

		mGL->bindTextures({
			TextureBinding{&renderer->target[DeferredRenderer::COLOR],   0,shader->getUniformLocation("diffuseTex")},
			TextureBinding{&renderer->target[DeferredRenderer::G_BUFFER],1,shader->getUniformLocation("gBufferTex")},
			TextureBinding{&renderer->target[DeferredRenderer::NORMALS], 2,shader->getUniformLocation("normalTex")}
		});

		glUniform1f(shader->getUniformLocation("worldTime"), worldTime);

		mGL->screenBuffer->bindBuffer();
		glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE,0,(void*) 0);
		glDrawArrays(GL_TRIANGLES, 0, 6);

		shader->endRender();
	}

}


