/*
 * program.cpp
 *
 *  Created on: Jul 16, 2015
 *      Author: Dylan
 */

#include "program.h"
#include "io.h"

namespace vox {

	Program::Program(std::vector<std::string> vertexAttribs, const std::string& fileName,
					 std::initializer_list<std::pair<GLint*, std::string>> uniformNames,std::initializer_list<std::pair<GLint*, std::string>> uniformBuffers, std::vector<std::string> fragShaderOutputs,
					 bool geometryShader, std::vector<std::string> macros, std::vector<std::string> libs, std::vector<std::string> inject): geometryShader(geometryShader) {
		this->vertexAttribs = vertexAttribs.size();
		createMap(uniformNames, uniformBuffers);
		setShader(fileName, fragShaderOutputs, geometryShader, macros, libs, inject, vertexAttribs);
	}

	void Program::createMap(std::initializer_list<std::pair<GLint*, std::string>> uniformNames, std::initializer_list<std::pair<GLint*, std::string>> uniformBuffers) {
		for(auto it = uniformNames.begin(); it != uniformNames.end(); it++) {
			*(it->first) = 0;
			this->uniformNames.insert(*it);
		}
		for(auto it = uniformBuffers.begin(); it != uniformBuffers.end(); it++) {
			*(it->first) = 0;
			this->uniformBuffers.insert(*it);
		}
	}

	void Program::beginRender() { for(int i = 0; i < vertexAttribs; i++) glEnableVertexAttribArray(i); }
	void Program::endRender() 	{ for(int i = 0; i < vertexAttribs; i++) glDisableVertexAttribArray(i); }

	void Program::setShader(const std::string& fileName, std::vector<std::string> fragShaderOutputs, bool geometryShader,
			std::vector<std::string> macros, std::vector<std::string> libs, std::vector<std::string> inject, std::vector<std::string> vAttribs) {
		// If the shaders are the same as our old ones, then there's no need to
		if(this->fileName != fileName) {
			this->fileName = fileName;

			Asset geom;
			if(geometryShader)
				geom = mFileManager->fetchAsset(fileName, Asset::GeometryShader);

			std::vector<Asset> assetLibs;
			for(auto s:libs) {
				assetLibs.push_back(mFileManager->fetchAsset(s, Asset::ShaderLibrary));
			}

			name = loadShaders(mFileManager->fetchAsset(fileName, Asset::VertexShader), mFileManager->fetchAsset(fileName, Asset::FragmentShader), geom, fragShaderOutputs, vAttribs, macros, assetLibs, inject);
			for(auto it = uniformNames.begin(); it != uniformNames.end(); it++) {
				const char* uniformName = it->second.c_str();
				GLint uniform = glGetUniformLocation(name, uniformName);
				*it->first = uniform;

			}
			for(auto it = uniformBuffers.begin(); it != uniformBuffers.end(); it++) {
				const char* uniformName = it->second.c_str();
				GLint uniform = glGetUniformBlockIndex(name, uniformName);
				*it->first = uniform;
			}
		}
	}

	Program::~Program() {
		glDeleteProgram(name);
	}

	std::vector<std::string> aHelperFunction(std::vector<std::string> ifTrue, std::vector<std::string> ifFalse, bool decider) {
		return decider? ifTrue:ifFalse;
	}

	ChunkShaderProgram::ChunkShaderProgram(const std::string& fileName, bool geometryShader, std::vector<std::string> macros, std::vector<std::string> libs, std::vector<std::string> inject, bool deferredRender):
			Program({"vertexPosition_modelspace", "vertexUV"}, fileName,
					{
						tMatDef,
						mkpr(worldTime),
						mkpr(occlusionCurve),
						mkpr(ditherSampler),
						mkpr(fogColor),
						mkpr(fogDensity),
						mkpr(normalLighting),
						mkpr(normalVectors),
						mkpr(voxelColorTex),
						mkpr(voxelLightTex)

					},
					{
						{&lightData, "LightData"},
						{&tileData, "TileData"},
						{&sun, "Sun"},
						{&depthBiasMVP, "depthBias"},
					}, aHelperFunction({"color", "normal", "light", "g_buffer"}, {}, deferredRender), geometryShader, macros, libs, inject ) {}

	EntityShaderProgram::EntityShaderProgram(const std::string& fileName, bool geometryShader, std::vector<std::string> macros, std::vector<std::string> libs, std::vector<std::string> inject, bool deferredRender):
			Program({"vertexPosition_modelspace", "vertexUV"}, fileName,
					{
						tMatDef,
						mkpr(cornerLights),
						mkpr(size),
						mkpr(worldTime),
						mkpr(occlusionCurve),
						mkpr(ditherSampler),
						mkpr(fogColor),
						mkpr(fogDensity),
						mkpr(lightLevel),
						mkpr(normalLighting),
						mkpr(normalVectors),
						mkpr(voxelColorTex),
						mkpr(voxelLightTex)

					},
					{
						{&lightData, "LightData"},
						{&tileData, "TileData"},
						{&sun, "Sun"},
						{&depthBiasMVP, "depthBias"},
					}, aHelperFunction({"color", "normal", "light", "g_buffer"}, {}, deferredRender), geometryShader, macros, libs, inject ) {}


	RenderTextureProgram::RenderTextureProgram(const std::string& fileName, bool geometryShader, std::vector<std::string> macros, std::vector<std::string> libs, std::vector<std::string> inject):
			Program({"vertexPosition_modelspace"}, fileName,
					{
						{&texture,	"tex"},
						mkpr(position),
						mkpr(size),
						mkpr(depth),
						mkpr(rot),
						mkpr(color),
						mkpr(screenSize),
						mkpr(UVstart),
						mkpr(UVsize)

					}, {}, {}, geometryShader, macros, libs, inject ) {}

	DeferredShaderProgram::DeferredShaderProgram(const std::string& fileName, bool geometryShader, std::vector<std::string> macros, std::vector<std::string> libs, std::vector<std::string> inject):
			Program({"vertexPosition", "tileIndex"}, fileName,
					{
						mkpr(diffuseTex),
						mkpr(gBufferTex),
						mkpr(normalTex),
						mkpr(lightTex),
						mkpr(shadowMap),
						mkpr(cameraPos),
						mkpr(IV),
						mkpr(IP),
						mkpr(IVP),
						mkpr(skyBox),
						mkpr(fogColor),
						mkpr(fogDensity),
						mkpr(bloomTex),
						mkpr(skyTex),
						mkpr(ssaoTex)
					},
					{
						{&lightData, "LightData"},
						{&tileData, "TileData"},
						{&sun, "Sun"},
						{&depthBiasMVP, "depthBias"},
					}, {}, geometryShader, macros, libs, inject ) {}

	ShadowShaderProgram::ShadowShaderProgram(const std::string& fileName, bool geometryShader, std::vector<std::string> macros, std::vector<std::string> libs, std::vector<std::string> inject):
			Program({"vertexPosition_modelspace"}, fileName,
					{
						tMatDef,
						mkpr(cameraPos)

					}, {mkpr(sunVPs)}, {}, geometryShader, macros, libs, inject ) {}

	RenderSkyShaderProgram::RenderSkyShaderProgram(const std::string& fileName, bool geometryShader, std::vector<std::string> macros, std::vector<std::string> libs, std::vector<std::string> inject):
			Program({"vertexPosition_modelspace", "texlayer"}, fileName,
					{
						mkpr(tex),
						mkpr(tex2),
						mkpr(MVP),
						mkpr(worldTimeDiff),
						mkpr(depth),
						mkpr(color)

					}, {}, {}, geometryShader, macros, libs, inject ) {}

	RenderToSkyShaderProgram::RenderToSkyShaderProgram(const std::string& fileName, bool geometryShader, std::vector<std::string> macros, std::vector<std::string> libs, std::vector<std::string> inject):
			Program({"vertexPosition_modelspace"}, fileName,
					{
						mkpr(sunPos),
						mkpr(layer),
						mkpr(faceDirection),
						mkpr(model),
						mkpr(cameraHeight),
						mkpr(rayleighBrightness),
						mkpr(mieBrightness),
						mkpr(sunBrightness),
						mkpr(intensity),
						mkpr(rayleighStrength),
						mkpr(mieStrength),
						mkpr(rayleighCollectionPower),
						mkpr(mieCollectionPower),
						mkpr(scatterStrength),
						mkpr(mieDistribution),
						mkpr(surfaceHeight)

					}, {}, {}, geometryShader, macros, libs, inject ) {}

	GUIShaderProgram::GUIShaderProgram(const std::string& fileName, bool geometryShader, std::vector<std::string> macros, std::vector<std::string> libs, std::vector<std::string> inject):
			Program({"pos","UV","bottomleft","ref","ref2","rep","repeatSize"}, fileName,
					{
						mkpr(textureAtlas),
						mkpr(offset),
						mkpr(screenSize),
						mkpr(textureAtlasSize),
						mkpr(stretch),
						mkpr(hue),
						mkpr(layer)
					}, {}, {}, geometryShader, macros, libs, inject ) {}

	CloudShaderProgram::CloudShaderProgram(const std::string& fileName, bool geometryShader, std::vector<std::string> macros, std::vector<std::string> libs, std::vector<std::string> inject):
			Program({"vertexPosition_modelspace"}, fileName,
					{
						mkpr(face),
						tMatDef,
						mkpr(worldTime),
						mkpr(sundir),
						mkpr(phaseMod),
						mkpr(phaseMul),
						mkpr(phaseAdd),
						mkpr(brightnessMul),
						mkpr(maxAlpha)
					}, {}, {}, geometryShader, macros, libs, inject ) {}

	SkyObjectShaderProgram::SkyObjectShaderProgram(const std::string& fileName, bool geometryShader, std::vector<std::string> macros, std::vector<std::string> libs, std::vector<std::string> inject):
			Program({"vertexPosition_modelspace"}, fileName,
					{
						mkpr(tex),
						mkpr(face),
						mkpr(intensity),
						tMatDef,
					}, {}, {}, geometryShader, macros, libs, inject ) {}

	ChunkLoDShaderProgram::ChunkLoDShaderProgram(const std::string& fileName, bool geometryShader, std::vector<std::string> macros, std::vector<std::string> libs, std::vector<std::string> inject):
			Program({"vertexPosition_modelspace", "vertexColor", "occlusionLevel", "vertexNormal", "voxelLighting"}, fileName,
					{
						tMatDef,
						mkpr(worldTime),
						mkpr(occlusionCurve),
						mkpr(fogColor),
						mkpr(fogDensity)

					}, {}, {"color", "normal", "light"}, geometryShader, macros, libs, inject ) {}

	TextShaderProgram::TextShaderProgram(const std::string& fileName, bool geometryShader, std::vector<std::string> macros, std::vector<std::string> libs, std::vector<std::string> inject):
			Program({"vertexPosition_absolute", "texCoord", "vertexColor"}, fileName,
					{
						mkpr(textureAtlas),
						mkpr(color),
						mkpr(textureSize),
						mkpr(screenSize),
						mkpr(pos),
						mkpr(size),
						mkpr(layer)
					}, {}, {}, geometryShader, macros, libs, inject ) {}

	TexturedMeshShaderProgram::TexturedMeshShaderProgram(const std::string& fileName, bool geometryShader, std::vector<std::string> macros, std::vector<std::string> libs, std::vector<std::string> inject):
			Program({"vertexPosition_modelspace", "vertexUV"}, fileName,
					{
						tMatDef,
						mkpr(tex),
						mkpr(depth),
						mkpr(fogColor),
						mkpr(fogDensity),
						mkpr(color)

					}, {}, {}, geometryShader, macros, libs, inject ) {}

	ColoredShaderProgram::ColoredShaderProgram(const std::string& fileName, bool geometryShader, std::vector<std::string> macros, std::vector<std::string> libs, std::vector<std::string> inject):
			Program({"vertexPosition_modelspace"}, fileName,
					{
						tMatDef,
						mkpr(color)

					}, {}, {}, geometryShader, macros, libs, inject ) {}

	LiquidShaderProgram::LiquidShaderProgram(const std::string& fileName, bool geometryShader, std::vector<std::string> macros, std::vector<std::string> libs, std::vector<std::string> inject):
			Program({"position", "normal", "UV", "color", "height", "occlusion", "tangent", "bitangent", "light", "voxelLighting"}, fileName,
					{
							tMatDef,
							mkpr(normalMatrix),
							mkpr(occlusionCurve),
							mkpr(ditherSampler),
							mkpr(tex1),
							mkpr(tex2),
							mkpr(fogColor),
							mkpr(fogDensity),
							mkpr(timeDiff),
							mkpr(camPos)

					}, {}, {}, geometryShader, macros, libs, inject ) {}

	RenderToLiquidShaderProgram::RenderToLiquidShaderProgram(const std::string& fileName, bool geometryShader, std::vector<std::string> macros, std::vector<std::string> libs, std::vector<std::string> inject):
			Program({"UV"}, fileName,
					{
							mkpr(_time_),
							mkpr(UVstart),
							mkpr(UVsize)

					}, {}, {}, geometryShader, macros, libs, inject ) {}


	InstancedParticleShaderProgram::InstancedParticleShaderProgram(const std::string& fileName, bool geometryShader, std::vector<std::string> macros, std::vector<std::string> libs, std::vector<std::string> inject):
			Program({"quadPositionOffset", "position", "UVstart", "UVsize", "color", "scale"}, fileName,
					{
							mkpr(tex),
							mkpr(V),
							mkpr(P),
							mkpr(VP)
					}, {}, {}, geometryShader, macros, libs, inject) {}

	InstancedCloudParticleShaderProgram::InstancedCloudParticleShaderProgram(const std::string& fileName, bool geometryShader, std::vector<std::string> macros, std::vector<std::string> libs, std::vector<std::string> inject):
			Program({"quadPositionOffset", "position", "textureIndex"}, fileName,
					{
							mkpr(tex),
							mkpr(time),
							mkpr(color),
							mkpr(UVsize),
							mkpr(UVgap),
							mkpr(V),
							mkpr(P),
							mkpr(VP),
							mkpr(scale),
							mkpr(camPos),
							mkpr(camLookXZRotationMatrix),
							mkpr(aspectRatio),
							mkpr(globalColor)

					}, {}, {}, geometryShader, macros, libs, inject) {}
}


