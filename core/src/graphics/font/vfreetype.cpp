/*
 * vfreetype.cpp
 *
 *  Created on: Jul 21, 2015
 *      Author: Dylan
 */
#include "vfreetype.h"
#include "console.h"
#include "filesystem.h"
#include "renderutils.h"
#include "framebuffer.h"
#include "glcontroller.h"
#include "fontrender.h"
#include "glbuffer.h"
#include "program.h"
#include "fontrender.h"
#include "util.h"

#include <sstream>
#include <string>
#include FT_ERRORS_H

namespace vox {

	FontManager* mFontManager;

	void handleFreeTypeError(int error, const char* file, const char* function, int line, std::string errMsg) {
		if(error)
				printlnf(FONTERROR, "%s! Occurred in %s, on function %s, in line %i. Error code: %i",
						errMsg.c_str(), file, function, line, error);
	}

	void FontManager::init() {
		int error = FT_Init_FreeType(&lib);
		handleFontErrorMSG(error, "Error initializing FreeType");
	}

	int tconstrain(int n, int lowerBound, int upperBound) {
		if (n < lowerBound) n = lowerBound;
		else if (n > upperBound) n = upperBound;
		return n;
	}

	float mix(float a, float b, float n) {
		n = constrain(n, 0, 1);
		return n * b + (1-n) * a;
	}

	void shadowText(FT_GlyphSlot* inGlyph, glm::ivec2 offset, float alpha, float color = 0.f) {

		FT_Bitmap* in = &(*inGlyph)->bitmap;

		// > 2015
		// > malloc
		unsigned char* nData = (unsigned char*)malloc((in->width + offset.x) * (in->rows + offset.y) * sizeof(unsigned char));

		unsigned char* oldData = in->buffer;

		for(unsigned int i = 0; i < (in->width + offset.x) * (in->rows + offset.y); i++) nData[i] = 0; // fill array with 0's

		int scolor = ((unsigned char) (color * 7.f)) << 5;

		// draw shadows
		for(unsigned int x = 0; x < in->width; x++) for(unsigned int y = 0; y < in->rows; y++) {
			int val = oldData[x + y * (in->width)];
			int salpha = (int)((val & 31) * alpha);
			if((val & 31) > 0) {
				nData[(x + offset.x) + (y + offset.y) * (in->width + offset.x)] = salpha + scolor;
			}
		}

		// draw old data on top of shadows
		for(unsigned int x = 0; x < in->width; x++) for(unsigned int y = 0; y < in->rows; y++) {
			int val = oldData[x + y * (in->width)];
			int calpha = (val & 31);
			int salpha = nData[x + y * (in->width + offset.x)] & 31;
			if(calpha == 0) continue;
			else if(calpha == 31 || salpha == 0) nData[x + y * (in->width + offset.x)] = val;
			else { // do voodo anti-aliasing magicks
				float fcharalpha = (calpha) / 31.f;
				float shadowalpha = salpha / 31.f;
				float shadowcolor = (nData[x + y * (in->width + offset.x)] & 224) / 224.f;
				float color = 1.f;

				float nAlpha = fcharalpha + shadowalpha * (1 - fcharalpha);
				float nColor = mix(shadowcolor * shadowalpha, color, fcharalpha) / nAlpha;
				unsigned char finalValue = (tconstrain(((unsigned char) (nColor * 7.001f)),0,7) << 5) +
											tconstrain(((unsigned char) (nAlpha * 31.001f)),0,31);
				nData[x + y * (in->width + offset.x)] = finalValue;
			}
		}

		// what is this, 1992?
		free(in->buffer);

		in->width += offset.x;
		in->rows += offset.y;
		in->buffer = nData;
		(*inGlyph)->metrics.height += 64 * offset.y;
	}

	void Font::drawGlyph(ftchar ch, glm::ivec2 fontSize) {
		uint32_t glyphIndex = FT_Get_Char_Index(fontFace, ch);

		int error = FT_Set_Pixel_Sizes(fontFace, fontSize.x, fontSize.y);
		handleFontError(error);

		error = FT_Load_Glyph(fontFace, glyphIndex, FT_LOAD_DEFAULT);
		handleFontErrorMSG(error, "Error loading char \"" + toString(ch) + "\"");
		error = FT_Render_Glyph(fontFace->glyph, FT_RENDER_MODE_NORMAL);
		handleFontErrorMSG(error, "Error loading char \"" + toString(ch) + "\"");
		FT_Bitmap* bmap = &fontFace->glyph->bitmap;
		unsigned char* data = bmap->buffer;

		glm::ivec2 size = {bmap->width, bmap->rows};

		for(int x = 0; x < size.x; x++) for(int y = 0; y < size.y; y++) {
			data[x + y * size.x] = data[x + y * size.x] / 8 + 224; // divide by two to shift the alpha from 8 to 5 bits, and add 224 to fill the color channel
		}

		if(textShadow) shadowText(&fontFace->glyph, shadowOffset, shadowAlpha, shadowColor);
	}

	Font::Font() {
		// More initialization is needed, but that is done inside the FontManager Font constructor
		fontFace = FT_Face();
		Tbaseline = 0;
	}

	CharAtlas::CharAtlas(FontManager* parent, int size) {
		this->parent = parent;
		this->framebuffer = new ColorBuffer({size, size}, GL_R8, false, GL_NEAREST, GL_NEAREST);
		mGL->bindFrameBuffer(framebuffer);
		framebuffer->clear();
	}

	void CharAtlas::loadCharsInQueue() {
		glDisable(GL_CULL_FACE);
		mGL->bindFrameBuffer(framebuffer);
		mGL->bindProgram(mGL->screenProgram);
		mGL->screenProgram->beginRender();
		glm::vec2 fssize(framebuffer->size);
		while(!charsToLoad.empty()) {
			FontChar c = charsToLoad.front().first;
			glm::ivec2 pos = charsToLoad.front().second;
			charsToLoad.pop();
			Texture* tex = parent->getCharTexture(c);
			mGL->render(mGL->screenProgram, tex, glm::vec2(pos) / glm::vec2(framebuffer->size), glm::vec2(tex->size) / glm::vec2(framebuffer->size));
			delete tex;
		}
		mGL->screenProgram->endRender();
		glEnable(GL_CULL_FACE);
	}

	void CharAtlas::loadChar(FontChar c) {
		FT_Bitmap* bmap = parent->getCharBitmap(c);

		CharAtlasEntry* toLoad = new CharAtlasEntry();

		toLoad->size = glm::ivec2(bmap->width, bmap->rows);
		if(curTexturePos.x + toLoad->size.x + 2 >= framebuffer->size.x) {
			curTexturePos.x = 0;
			curTexturePos.y += texAtlasRowHeight + 1;
			texAtlasRowHeight = 0;
		}
		texAtlasRowHeight = std::max(texAtlasRowHeight, toLoad->size.y);
		toLoad->pos = curTexturePos + 1;
		toLoad->character = c.character;
		toLoad->font = c.font;
		toLoad->fontSize = c.fontSize;
		toLoad->yMin = c.font->fontFace->glyph->metrics.height - c.font->fontFace->glyph->metrics.horiBearingY;
		toLoad->yMax = c.font->fontFace->glyph->metrics.horiBearingY;
		toLoad->yAdvance = c.fontSize.y;
		toLoad->yBearing = c.font->fontFace->glyph->metrics.horiBearingY;
		toLoad->xBearing = c.font->fontFace->glyph->metrics.horiBearingX / 64;
		toLoad->descent = (c.font->fontFace->glyph->metrics.height >> 6) - c.font->fontFace->glyph->bitmap_top;
		toLoad->advance = c.font->fontFace->glyph->metrics.horiAdvance;

		entries.emplace(c, toLoad);

		curTexturePos.x += toLoad->size.x + 1;

		charsToLoad.emplace(c, toLoad->pos);
	}

	CharAtlasEntry* CharAtlas::getChar(FontChar c) {
		auto ret = entries.find(c);
		if(ret == entries.end()) { // This entry does not exist, so load it!
			loadChar(c);
			ret = entries.find(c);
		}
		return ret->second;
	}

	Font* FontManager::makeFont(std::string filename) {
		Asset a = mFileManager->fetchAsset(filename, Asset::Font);
		Font* ret = new Font();
		int error = FT_New_Face(lib, a.path.c_str(), 0, &ret->fontFace);
		if(error)
			printlnf(FONTERROR, "Error creating font %s!", a.path.c_str());
		else
			printlnf(FONTINFO, "Successfully created font: %s", a.name.c_str());

		getCharBitmap(FontChar{ret, 'T', {128,128}});
		ret->Tbaseline = ret->fontFace->glyph->bitmap_top / 128.f;

		return ret;
	}

	int nextPow2(int n) {
		return 1 << int(ceil(log2(n)));
	}

	bool isPow2(int n) {
	    return (n != 0) && ((n & (n - 1)) == 0);
	}

	FT_Bitmap* FontManager::getCharBitmap(FontChar ch) {
		ch.font->drawGlyph(ch.character, ch.fontSize);
		return &ch.font->fontFace->glyph->bitmap;
	}

	Texture* FontManager::getCharTexture(FontChar ch) {
		FT_Bitmap* bmap = getCharBitmap(ch);
		unsigned char* data = bmap->buffer;

		glm::ivec2 size = {bmap->width, bmap->rows};

		bool resized = false;
		if(!isPow2(size.x) || !isPow2(size.y)) { // resize texture to be a power-of-two
			resized = true;
			glm::ivec2 p2 = {nextPow2(size.x), nextPow2(size.y)};
			unsigned char* data2 = new unsigned char[p2.x * p2.y];
			for(int x = 0; x < p2.x; x++) for(int y = 0; y < p2.y; y++)
				data2[x + y * p2.x] = 0;
			for(int x = 0; x < size.x; x++) for(int y = 0; y < size.y; y++)
				data2[x + y * p2.x] = data[x + y * size.x];
			data = data2;
			size = p2;
		}

		Texture* ret = createTexture2D(size, data, GL_R8, GL_NEAREST, GL_CLAMP_TO_EDGE, GL_RED, GL_UNSIGNED_BYTE);

		if(resized) delete[] data;
		return ret;
	}

	void FontManager::deleteFontRenderObject(FontRenderObject* f) {
		for(auto ch: f->characters) {
			CharAtlasEntry* entry = loadedChars->entries.find(ch.fontChar)->second;
			entry->references--;
		}
	}

	void updateRowHeights(std::vector<int>& maxHeights, std::vector<int>& minHeights, int& curMax, int& curMin) {
		maxHeights.push_back(curMax);
		curMax = 0;
		minHeights.push_back(curMin);
		curMin = 0;
	}

	void addRow_stage1(float& maxWidth, glm::i16vec2& penPos, std::vector<int>& rowMaxHeights, std::vector<int>& rowMinHeights, int& curRowMaxHeight, int& curRowMinHeight, int& row,
			std::vector<int>& rowWidths, int curRowWidth) {
		maxWidth = fmax(maxWidth, penPos.x);
		penPos.x = 0;
		updateRowHeights(rowMaxHeights, rowMinHeights, curRowMaxHeight, curRowMinHeight);
		rowWidths.push_back(curRowWidth);
		curRowWidth = 0;
		row++;
	}

	void resetPenPos_stage2(int alignment, int maxWidth, std::vector<int> rowWidths, int row, glm::i16vec2& penPos) {
		if(alignment == LEFT) {
			penPos.x = 0;
		} else if(alignment == RIGHT) {
			penPos.x = maxWidth - rowWidths[row]-4;
		} else {
			penPos.x = maxWidth/2 - rowWidths[row]/2;
		}
	}

	void addRow_stage2(glm::i16vec2& penPos, std::vector<int>& rowMaxHeights, std::vector<int>& rowMinHeights, int& row, std::vector<int>& rowWidths, int alignment, int maxWidth) {
		penPos.y -= rowMaxHeights[row+1] + rowMinHeights[row];
		row++;
		resetPenPos_stage2(alignment, maxWidth, rowWidths, row, penPos);

	}

	FontRenderObject* FontManager::makeStringRenderObject(std::vector<FormattedChar> text, int width, int alignment) {
		StackBuffer<Quad<FontVertex>>* buf = new StackBuffer<Quad<FontVertex>>(ARRAY, DYNAMIC);

		// create the return object
		FontRenderObject* ret = new FontRenderObject();
		ret->tex = loadedChars->framebuffer->target;
		ret->vertexBuffer = buf;
		ret->parent = this;
		ret->characters = text;

		fontRenderObjects.push_back(ret);

		float maxWidth = 0;
		std::vector<int> rowMaxHeights;
		std::vector<int> rowMinHeights;
		std::vector<int> rowWidths;
		int curRowMaxHeight = 0;
		int curRowMinHeight = 0;

		int row = 0;

		int row1Baseline = 0;

		// load the data into the vertex buffer
		glm::i16vec2 penPos(0,0); // the y goes down, because we start from the top left (it's easier that way)
		for(auto ch: text) { // do initial count to find the height of the text box
			CharAtlasEntry* aEntry = loadedChars->getChar(ch.fontChar);
			if(ch.fontChar.character == ' ') {
				if(penPos.x + aEntry->fontSize.x / 2 > width) {
					addRow_stage1(maxWidth, penPos, rowMaxHeights, rowMinHeights, curRowMaxHeight, curRowMinHeight, row, rowWidths, penPos.x);
				} else penPos.x += aEntry->fontSize.x / 2;
			} else if(ch.fontChar.character == '\n') {
				addRow_stage1(maxWidth, penPos, rowMaxHeights, rowMinHeights, curRowMaxHeight, curRowMinHeight, row, rowWidths, penPos.x);
			} else {
				glm::i16vec2 size(aEntry->size);
				if(penPos.x + size.x > width) {
					addRow_stage1(maxWidth, penPos, rowMaxHeights, rowMinHeights, curRowMaxHeight, curRowMinHeight, row, rowWidths, penPos.x);
				}
				penPos.x += aEntry->advance / 64;
				curRowMaxHeight = std::max(curRowMaxHeight, aEntry->yAdvance);
				curRowMinHeight = std::max(curRowMinHeight, 0);

				if(row == 0) {
					row1Baseline = std::max(row1Baseline, (int) (aEntry->font->Tbaseline * 2 * aEntry->fontSize.y));
				}
			}
		}
		updateRowHeights(rowMaxHeights, rowMinHeights, curRowMaxHeight, curRowMinHeight);
		maxWidth = fmax(maxWidth, penPos.x); // This plus the two in the rollover checks guarantee that maxWidth
											 //  is indeed the maximum width of the string.

		if(alignment != LEFT) maxWidth = width;

		for(unsigned int i = 0; i < rowMaxHeights.size(); i++) {
			penPos.y += rowMaxHeights[i] + rowMinHeights[i];
		}

		ret->size = {maxWidth, penPos.y};

		ret->originOffsetY = penPos.y - row1Baseline;

		penPos = {0, penPos.y};
		penPos.y -= rowMaxHeights[0] + rowMinHeights[0];
		row = 0;
		resetPenPos_stage2(alignment, maxWidth, rowWidths, row, penPos);
		for(auto ch: text) {
			CharAtlasEntry* aEntry = loadedChars->getChar(ch.fontChar);
			if(ch.fontChar.character == ' ') {
				if(penPos.x + aEntry->fontSize.x / 2 > width+5) {
					addRow_stage2(penPos, rowMaxHeights, rowMinHeights, row, rowWidths, alignment, width);
				} else penPos.x += aEntry->fontSize.x / 2;
			} else if(ch.fontChar.character == '\n') {
				addRow_stage2(penPos, rowMaxHeights, rowMinHeights, row, rowWidths, alignment, width);
			} else {
				glm::u8vec4 color = ch.color;
				aEntry->references++;
				glm::i16vec2 size(aEntry->size);
				glm::i16vec2 texPos(aEntry->pos);

				if(penPos.x + size.x > width) {
					addRow_stage2(penPos, rowMaxHeights, rowMinHeights, row, rowWidths, alignment, width);
				}

				glm::i16vec2 ppos2 = penPos - glm::i16vec2(0,aEntry->yMin / 64) + glm::i16vec2(aEntry->xBearing, 0);
				buf->push(Quad<FontVertex>(
					{ppos2, {texPos.x, texPos.y + size.y}, color}, {{ppos2.x + size.x, ppos2.y}, texPos + size, color},
					{ppos2 + size, {texPos.x + size.x, texPos.y}, color}, {{ppos2.x, ppos2.y + size.y}, texPos, color}
				));
				penPos.x += aEntry->advance / 64;
			}
		}
		buf->generateBuffer();
		return ret;
	}

	FontRenderObject* FontManager::makeStringRenderObject(Font* font, std::string text, glm::ivec2 fontSize, glm::vec4 color, int width, int alignment) {
		std::vector<FormattedChar> chars;
		for(char c:text) {
			chars.push_back(FormattedChar(FontChar{font, ftchar(c), fontSize}, color));
		}
		return makeStringRenderObject(chars, width, alignment);
	}

	bool operator == (FontChar a, FontChar b) {
		return a.font == b.font && a.character == b.character && a.fontSize == b.fontSize;
	}



	std::hash<ftchar> hasha;
	size_t FontCharHash::operator ()(FontChar i) const {
		return hasha(i.character) & hasha(reinterpret_cast<ftchar>(i.font)); // I'm not sure if this is a very good hashing function, but w/e
	}
}

