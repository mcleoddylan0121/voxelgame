/*
 * vfreetype.h
 *
 *  Created on: Jul 21, 2015
 *      Author: Dylan
 */

#ifndef SRC_GRAPHICS_FONT_VFREETYPE_H_
#define SRC_GRAPHICS_FONT_VFREETYPE_H_

#include "vglm.h"
#include "shapes.h"

#include <ft2build.h>
#include FT_FREETYPE_H

#include <string>
#include <unordered_map>
#include <queue>
#include <set>

namespace vox {

	struct Texture; struct FontRenderObject; class FontVertex;
	class Font; class CharAtlas; class ColorBuffer; class FontManager; class TextShaderProgram;
	template<typename T> struct ArrayBuffer;

	void handleFreeTypeError(int error, const char* file, const char* function, int line, std::string errString = "A FreeType Error has occurred!");

	typedef uint64_t ftchar; // character code for freetype

	struct Font {
	public:
		FT_Face fontFace;
		bool textShadow = false;
		glm::ivec2 shadowOffset = {3,3};
		float shadowAlpha = 0.75;
		float shadowColor = 0.15; // 0 = black, 1 = same color as text, in [0,1]

		~Font() { FT_Done_Face(fontFace); }

		friend class FontManager;
	private:

		void drawGlyph(ftchar ch, glm::ivec2 fontSize);
		Font();

		float Tbaseline;
	};

	enum Alignment {
		LEFT, CENTER, RIGHT
	};

	struct CharAtlasEntry {
		glm::ivec2 pos;
		glm::ivec2 size;
		glm::ivec2 fontSize;
		int yMin;
		int yMax;
		int yAdvance;
		int yBearing;
		int xBearing;
		int advance;
		int descent;
		ftchar character;
		Font* font;
		int references = 0; // If this is 0, then it is marked for deletion
	};

	struct FontChar {
		Font* font;
		ftchar character;
		glm::ivec2 fontSize;

		friend bool operator == (FontChar a, FontChar b);
	};

	struct FontCharHash {
		size_t operator ()(FontChar i) const;
	};

	class CharAtlas {
	private:
		std::queue<std::pair<FontChar, glm::ivec2>> charsToLoad;
		void loadCharsInQueue();
	public:
		glm::ivec2 curTexturePos = glm::ivec2(0,0); // used for (crappy) packing
		int texAtlasRowHeight = 0;
		ColorBuffer* framebuffer; // Used for loading in new characters, as this is dynamic
		FontManager* parent;
		std::unordered_map<FontChar, CharAtlasEntry*, FontCharHash> entries;
		std::set<CharAtlasEntry*> atlasEntries;

		CharAtlas(FontManager* parent, int size);
		void loadChar(FontChar c);
		CharAtlasEntry* getChar(FontChar c);

		void tick() { loadCharsInQueue(); }

		// Repack the entire texture atlas, and update EVERY FontRenderObject's vertex buffer...
		// This isn't gonna be pretty
		void repack();

		// Resize the entire texture atlas, then repack it. This is a last resort.
		// You've done quite a bit to get yourself in this situation, haven't you?
		void resize(int nSize);
	};

	// These are parsed from UTF-8 and some markup stuff
	struct FormattedChar {
		FormattedChar(FontChar fontChar, glm::vec4 color):
			fontChar(fontChar), color(glm::u8vec4(color * 255.f)) {}
		FontChar fontChar;
		glm::u8vec4 color;
	};

	class FontManager {
	public:
		std::vector<FontRenderObject*> fontRenderObjects;
		void deleteFontRenderObject(FontRenderObject* f);

		FontManager() { lib = FT_Library(); loadedChars = new CharAtlas(this, 1536); }
		void init();

		FT_Library lib;
		Font* makeFont(std::string filename);
		FT_Bitmap* getCharBitmap(FontChar ch);
		Texture* getCharTexture(FontChar ch);
		// The vector of FormattedChars allows mixing and matching of fonts and colors within a single string
		// This allows for formatting like markup and such
		FontRenderObject* makeStringRenderObject(std::vector<FormattedChar> chars, int width, int alignment = LEFT);
		FontRenderObject* makeStringRenderObject(Font* font, std::string text, glm::ivec2 fontSize, glm::vec4 color, int width, int alignment = LEFT);
		CharAtlas* loadedChars; // This memoizes the characters requested for loading

		~FontManager() { FT_Done_FreeType(lib); }

		void tick() { loadedChars->tick(); }
	};

	extern FontManager* mFontManager;
}
#define handleFontError(error) handleFreeTypeError(error, __FILE__, __FUNCTION__, __LINE__)
#define handleFontErrorMSG(error, msg) handleFreeTypeError(error, __FILE__, __FUNCTION__, __LINE__, msg)


#endif /* SRC_GRAPHICS_FONT_VFREETYPE_H_ */
