/*
 * fontrender.cpp
 *
 *  Created on: Jul 23, 2015
 *      Author: Dylan
 */


#include "fontrender.h"
#include "glbuffer.h"
#include "vfreetype.h"
#include "glcontroller.h"
#include "program.h"

namespace vox {

	FontRenderObject::~FontRenderObject() {
		parent->deleteFontRenderObject(this);
		delete vertexBuffer;
	}

	void TextField::render(TextRenderer* render) {
		if(renderObject)
			render->render(TextDisplayObject(renderObject, pos - glm::vec2(0,renderObject->size.y), scale, textColor));
	}

	void TextField::setWidth(int nWidth) {
		if(nWidth == width) return; // the new width isn't different, don't waste our time
		width = nWidth;
		if(renderObject) {
			std::vector<FormattedChar> chars = renderObject->characters;
			setText(chars); // update the text with the new width
		}
	}

	TextField::TextField(int width, glm::vec2 bottomleft, glm::vec2 scale) {
		setWidth(width);
		setBottomLeft(bottomleft);
		this->scale = scale;
	}

	void TextField::setText(std::string text, Font* f, glm::ivec2 textSize, glm::vec4 color, int alignment) {
		if(renderObject) delete renderObject;
		renderObject = mFontManager->makeStringRenderObject(f, text, textSize, color, width, alignment);
	}

	void TextField::setText(std::vector<FormattedChar> nText, int alignment) {
		if(renderObject) delete renderObject;
		if(nText.size() < 1) return;
		renderObject = mFontManager->makeStringRenderObject(nText, width, alignment);
	}

	TextField::~TextField() {
		if(renderObject) delete renderObject;
	}

	void TextField::append(std::vector<FormattedChar> toAppend) { /*renderObject->append(toAppend);*/ }
	void TextField::append(std::string text, Font* f) { /*renderObject->append(text, f);*/ }
	void TextField::deleteText(int start, int end) { /*renderObject->deleteText(start, end);*/ } // remove the characters from start->end (inclusive)
	void TextField::backspace(int amt) { /*renderObject->backspace(amt);*/ } // take amt of chars off the end
}

