/*
 * fontrender.h
 *
 *  Created on: Jul 23, 2015
 *      Author: Dylan
 */

#ifndef SRC_GRAPHICS_FONT_FONTRENDER_H_
#define SRC_GRAPHICS_FONT_FONTRENDER_H_

#include "shapes.h"
#include "renderutils.h"
#include "guirender.h"
#include "vfreetype.h"

#include <vector>
namespace vox {

	struct Texture;
	template<typename T> struct GLBuffer;
	struct FormattedChar;
	class FontManager;
	class Font;
	class TextShaderProgram;

	struct FontVertex {
		glm::i16vec2 pos;
		glm::i16vec2 texCoord;
		glm::u8vec4 color;
	};

	struct FontRenderObject {
		FontManager* parent;
		std::vector<FormattedChar> characters;

		glm::vec2 size;
		float originOffsetY;

		int alignment;

		Texture* tex;
		GLBuffer<Quad<FontVertex>>* vertexBuffer;

		/* UNIMPLEMENTED: */
		// void append(std::vector<FormattedChar> toAppend);
		// void append(std::string text, Font* f);
		// void deleteText(int start, int end);
		// void backspace(int amt); // take amt of chars off the end

		virtual ~FontRenderObject();
	};

	namespace gui {
		struct GUILibrary;
	}

	/* Provides an interface to text rendering */
	class TextField {
		friend struct gui::GUILibrary;

	private:
		FontRenderObject* renderObject = nullptr;
		glm::vec2 pos; // in absolute coordinates
		int width;
	public:
		bool visible = true;
		glm::vec2 scale;
		glm::vec4 textColor = {1,1,1,1}; // "helpful" for making flashing text or variable text/window opacity

		void setText(std::string text, Font* f, glm::ivec2 textSize, glm::vec4 color, int alignment = LEFT);
		void setText(std::vector<FormattedChar> nText, int alignment = LEFT);

		TextField(int width, glm::vec2 bottomleft = {0,0}, glm::vec2 scale = {1,1});

		void setWidth(int nWidth);
		void setTopLeft(glm::vec2 nPos) { if(renderObject) pos = nPos; else pos = nPos; }
		void setBottomLeft(glm::vec2 nPos) { if(renderObject) pos = glm::vec2(nPos.x, nPos.y + renderObject->size.y); else pos = nPos; }
		void setFirstRowBaseline(glm::vec2 nPos) { if(renderObject) pos = glm::vec2(nPos.x, nPos.y + renderObject->size.y - renderObject->originOffsetY); else pos = nPos; }

		void append(std::vector<FormattedChar> toAppend);
		void append(std::string text, Font* f);
		void deleteText(int start, int end); // remove the characters from start->end (inclusive)
		void backspace(int amt); // take amt of chars off the end

		void render(TextRenderer* render);

		virtual ~TextField();
	};

}



#endif /* SRC_GRAPHICS_FONT_FONTRENDER_H_ */
