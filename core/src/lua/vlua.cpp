#include "vlua.h"
#include "filesystem.h"
#include "luacontext.h"
#include "io.h"

#include <sstream>
#include <iostream>

namespace vox {

	std::vector<std::tuple<std::string, lua_CFunction>> LuaScript::_funcs;

	LuaScript::LuaScript() {
		_state = 0;
		_arg = 0;
	}

	LuaScript::LuaScript(const std::string& filename) : LuaScript() {
		init(filename);
	}

	LuaScript::LuaScript(LuaScript * s, const std::string& g) : LuaScript(s->_state) {
		_globalTable = g;
	}

	LuaScript::LuaScript(lua_State * state) : LuaScript() {
		_state = state;
		_real = false;
	}

	LuaScript::~LuaScript() {
		if (isLoaded() && _real) lua_close(_state);
	}

	LuaScript* LuaScript::allocate(const Asset& file) {
		mLua->loadFile(file);
		return mLua->makeScript(file);
	}

	void LuaScript::init(const std::string& filename) {
		if (isLoaded()) return;
		if (filename.empty()) {
			_state = 0;
			return;
		}

		_state = luaL_newstate();
		luaL_openlibs(_state);
		for (std::tuple<std::string, lua_CFunction>& t : _funcs) {
			lua_register(_state, std::get<0>(t).c_str(), std::get<1>(t));
		}

		if (luaL_loadfile(_state, filename.c_str())) {
			printlnf(FATALERROR, "Could not load Lua script %s: %s.", filename.c_str(), lua_tostring(_state, -1));
			_state = 0;
		} else if (lua_pcall(_state, 0, 0, 0)) {
			printlnf(FATALERROR, "Could not load Lua script %s: %s.", filename.c_str(), lua_tostring(_state, -1));
			_state = 0;
		}
	}

	std::string stackToString(std::vector<std::string> s) {
		std::stringstream ss;
		for (std::string& str : s)
			ss << str;
		return ss.str();
	}

	void LuaScript::_handleVariableError(const std::string& varName, const std::string& reason) {
		printlnf(LUAERROR, "Error loading variable %s: %s (%s)", varName.c_str(), reason.c_str(), stackToString(_nameStack).c_str());
	}
	void LuaScript::_handleFunctionError(const std::string& funcName, const std::string& reason) {
		printlnf(LUAERROR, "Error calling function %s: %s (%s)", funcName.c_str(), reason.c_str(), stackToString(_nameStack).c_str());
	}

	bool LuaScript::isLoaded() {
		return _state;
	}

	bool LuaScript::variableExists(const std::string& var) {
		bool ret = _elevate(var, false);
		_clearStack();
		return ret;
	}

	bool LuaScript::errorFlagSet() {
		bool t = _errorFlag;
		_errorFlag = false;
		return t;
	}

	void LuaScript::_clearStack() {
		if (_levels.size() == 0) {
			println(LUAERROR, "Tried to clear with an empty levels stack.");
		} else {
			lua_pop(_state, _levels.top());
			_levels.pop();
			_nameStack.pop_back();
		}
	}

	bool LuaScript::_elevate(const std::string& t, bool printErrors) {
		std::string var = "";
		std::string varName = t;
		if (lua_gettop(_state) == 0) varName = _globalTable + "." + varName;
		_levels.push(0);
		_nameStack.push_back("");

		_errorFlag = true;

		// Split the name into its parts deligated by '.'
		for (unsigned int i = 0; i < varName.size(); i++) {
			if (varName.at(i) == '.') {
				if (var.empty()) continue;
				if (lua_gettop(_state) == 0)
					lua_getglobal(_state, var.c_str());
				else
					lua_getfield(_state, -1, var.c_str());

				_levels.top()++;
				_nameStack.back().append(var + ".");

				if (lua_isnil(_state, -1)) {
					if (printErrors) _handleVariableError(varName, var + " is not defined.");
					return false;
				} else {
					var = "";
				}
			} else var += varName.at(i);
		}

		// Account for the last variable past the '.'
		if (lua_gettop(_state) == 0) // No '.' found
			lua_getglobal(_state, var.c_str());
		else
			lua_getfield(_state, -1, var.c_str());

		_levels.top()++;
		_nameStack.back().append(t);
		_nameStack.back().append(".");

		if (lua_isnil(_state, -1)) {
			if (printErrors) _handleVariableError(varName, var + " is not defined.");
			return false;
		}

		_errorFlag = false;
		return true;
	}

	void LuaScript::_pushTop(const LuaObject& obj) {
		lua_newtable(_state);
		for (const std::shared_ptr<LuaPair>& p : obj._map)
			p->pushValue(*this);
	}

	void LuaScript::_populateTemporaryLuaObject(LuaObject* o) {
		o->init(*this, "");
	}

	LuaObjectPair::LuaObjectPair(LuaObject * out, const std::string& name) : _ptr(out), _name(name) {}

	void LuaObjectPair::populate(LuaScript& ls, const std::string& rootName) {
		_ptr->init(ls, rootName + "." + _name);
	}

	LuaObject::LuaObject(std::vector<std::shared_ptr<LuaPair>> map) : _map(map) {
		_created = false;
	}

	void LuaObject::init(LuaScript& ls, const std::string& varName) {
		for (std::shared_ptr<LuaPair>& p : _map)
			p->populate(ls, varName);
		_created = true;
	}

	bool LuaObject::isCreated() {
		return _created;
	}
}
