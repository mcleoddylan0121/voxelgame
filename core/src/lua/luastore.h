/*
 * luastore.h
 *
 *  Created on: Jun 23, 2015
 *      Author: Daniel
 */

#ifndef LUASTORE_H_
#define LUASTORE_H_

#include <string>
#include <unordered_map>
#include <memory>

namespace vox {

	struct LuaStoreObject;
	struct LuaScript;

	typedef std::shared_ptr<LuaStoreObject> LuaStorePtr;

	struct LuaStore {
		void set(const std::string& name, LuaStorePtr&);
		const LuaStorePtr get(const std::string& name);

		bool exists(const std::string& name);

		std::unordered_map<std::string, LuaStorePtr> _val;
	};

	class LuaStoreObject {
		friend class LuaScript;

	public:
		virtual ~LuaStoreObject() {}

	private:
		virtual void _pushVal(LuaScript* s) = 0;

		static LuaStorePtr _createStoreObjectFromTop(LuaScript* s);
	};
}

#endif /* LUASTORE_H_ */
