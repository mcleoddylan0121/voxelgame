/*
 * luacontext.cpp
 *
 *  Created on: Aug 17, 2015
 *      Author: Daniel
 */

#include "luacontext.h"
#include "filesystem.h"
#include "util.h"

namespace vox {
	LuaContext * mLua;

	LuaContext::LuaContext() {
		_context = new LuaScript(mFileManager->fetchAsset("context", Asset::Lua, true).path);
	}

	LuaContext::~LuaContext() {
		delete _context;
	}

	std::string makeName(const Asset& luaFile) {
		std::string name = luaFile.name;
		for (unsigned int i = 0; i < name.size(); i++) // In case the name has dots in it for whatever reason
			if (name[i] == '.') name[i] = '_';

		return name;
	}

	void LuaContext::loadFile(const Asset& luaFile, std::string moduleName) {
		if (!luaFile.exists)
			return;

		std::string name = makeName(luaFile);
		_context->callVoidFunction("vox_doluafile", luaFile.path, moduleName, name);
	}

	LuaScript * LuaContext::makeScript(const Asset& luaFile, std::string moduleName) {
		if (!luaFile.exists)
			return new LuaScript();

		std::string name = makeName(luaFile);
		name = "vox_global_table." + moduleName + "." + name;

		return new LuaScript(_context, name);
	}

	LuaScript * LuaContext::getContext() { return _context; }

}
