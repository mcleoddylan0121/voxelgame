/*
 * settingsmanager.h
 *
 *  Created on: Jun 21, 2015
 *      Author: Daniel
 */

#ifndef SETTINGSMANAGER_H_
#define SETTINGSMANAGER_H_

#include "vluautil.h"
#include "filesystem.h"

#include <string>
#include <vector>

namespace vox {

	class SettingsManager {
	public:
		SettingsManager(const Asset& settingsFile);

		int getBinding(std::string name);

		void saveSettings();


		static void set_save(std::string str);

		static void set_key(std::string name, int key);
		static void set_number(std::string name, float number);
		static void set_res(std::string name, lvec2 resolution);
		static void set_bool(std::string name, bool b);

		float mouseSensitivity = 0.5, placeRadius = 1;
		int resWidth = 800, resHeight = 600;
		bool fullscreen = false, vsync = false;

		// These are here until Dylan implements the actual functions for these
		int antiAliasing = 1;
		bool cubemapFix = true, bloom = true, downsampleClouds = true, enableVR = false;

	private:
		std::string settingsFile;
		std::unordered_map<std::string, int> _keyBindings;

	};

	extern SettingsManager * mSettings;
}

#endif /* SETTINGSMANAGER_H_ */
