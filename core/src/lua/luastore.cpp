/*
 * luastore.cpp
 *
 *  Created on: Jun 23, 2015
 *      Author: Daniel
 */

#include "luastore.h"
#include "vlua.h"

namespace vox {

	//------------LuaStore------------//
	void LuaStore::set(const std::string& name, LuaStorePtr& o) {
		_val[name] = o;
	}

	const LuaStorePtr LuaStore::get(const std::string& name) {
		if (exists(name))
			return _val[name];
		return _getDefault<LuaStorePtr>();
	}

	bool LuaStore::exists(const std::string& name) {
		return _val.count(name) != 0;
	}

	//------------LuaStoreObject------------//
	LuaStorePtr LuaStoreObject::_createStoreObjectFromTop(LuaScript * s) {
		if (lua_isboolean(s->_state, -1))
			return std::make_shared<LuaStoreType<bool>>(s->_getTop<bool>("Store Object"));
		else if (lua_isnumber(s->_state, -1))
			return std::make_shared<LuaStoreType<float>>(s->_getTop<float>("Store Object"));
		else if (lua_isstring(s->_state, -1))
			return std::make_shared<LuaStoreType<std::string>>(s->_getTop<std::string>("Store Object"));
		else if (lua_istable(s->_state, -1))
			println(LUAERROR, "Could not create store object: storing a table is unsupported!");// what do
		else if (lua_isnil(s->_state, -1))
			println(LUAERROR, "Could not create store object: top is null!");
		else
			println(LUAERROR, "Could not create store object: unidentified type");

		return _getDefault<LuaStorePtr>();
	}
}
