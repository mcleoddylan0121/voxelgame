/*
 * settingsmanager.cpp
 *
 *  Created on: Jun 21, 2015
 *      Author: Daniel
 */

#include "settingsmanager.h"
#include "console.h"
#include "resourcemanager.h"
#include "filesystem.h"
#include "luacontext.h"
#include "io.h"
#include "entitymanager.h"
#include "sdlcontroller.h"

namespace vox {
	SettingsManager * mSettings;

	SettingsManager::SettingsManager(const Asset& file) : settingsFile(file.path) {
		mLua->loadFile(file, "_default");
	}

	int SettingsManager::getBinding(std::string name) {
		return _keyBindings[name];
	}


	void SettingsManager::saveSettings() {
		mLua->getContext()->callVoidFunction("vox_save_settings");
	}


	void SettingsManager::set_save(std::string str) {
		saveTextFile(mSettings->settingsFile, str);
	}

	void SettingsManager::set_key(std::string name, int key) {
		mSettings->_keyBindings[name] = key;
	}

	void SettingsManager::set_number(std::string name, float num) {
		if (name == "Mouse Sensitivity") mSettings->mouseSensitivity = num;
		else if (name == "Place Radius") mSettings->placeRadius = num;
		else if (name == "Anti-Aliasing") mSettings->antiAliasing = num; // TODO
	}

	void SettingsManager::set_res(std::string name, lvec2 resolution) {
		mSettings->resWidth = resolution.x;
		mSettings->resHeight = resolution.y;
		if (mSDL->isWindowMade()) mSDL->setResolution(mSettings->resWidth, mSettings->resHeight);
	}

	void SettingsManager::set_bool(std::string name, bool b) {
		if (name == "Fullscreen") {
			mSettings->fullscreen = b;
			if (mSDL->isWindowMade()) mSDL->setFullscreen(b);
		} else if (name == "VSync") {
			mSettings->vsync = b;
			if (mSDL->isWindowMade()) mSDL->setVSync(b);
		} else if (name == "Cubemap Fix") mSettings->cubemapFix = b; // TODO
		else if (name == "Bloom") mSettings->bloom = b; // TODO
		else if (name == "Downsample Clouds") mSettings->downsampleClouds = b; // TODO
		else if (name == "Enable VR") mSettings->enableVR = b; // TODO
		else if (name == "Lua") mMessageStateHandler.setMessageOn(LUAERROR, b);
		else if (name == "OpenGL") mMessageStateHandler.setMessageOn(GLINFO, b);
	}
}
