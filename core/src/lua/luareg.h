/*
 * luareg.h
 *
 *  Created on: Jun 4, 2015
 *      Author: Dani
 */

#ifndef LUAREG_H_
#define LUAREG_H_

namespace vox {
	void registerLuaFunctions();
}

#endif /* LUAREG_H_ */
