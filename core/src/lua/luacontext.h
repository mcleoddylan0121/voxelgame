/*
 * luacontext.h
 *
 *  Created on: Aug 17, 2015
 *      Author: Daniel
 */

#ifndef LUACONTEXT_H_
#define LUACONTEXT_H_

#include "vlua.h"

namespace vox {
	class LuaContext {
	public:
		LuaContext();
		virtual ~LuaContext();

		// Call load file then get a reference to the file with makeScript
		void loadFile(const Asset& luaFile, std::string moduleName = "_default");
		LuaScript * makeScript(const Asset& luaFile, std::string moduleName = "_default");

		// TODO: this
		void deleteFile(std::string name);

		LuaScript * getContext();

	private:
		LuaScript * _context;
	};

	extern LuaContext * mLua;
}

#endif /* LUACONTEXT_H_ */
