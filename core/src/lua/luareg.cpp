/*
 * luareg.cpp
 *
 *  Created on: Jun 4, 2015
 *      Author: Dani
 */

#include "effectlua.h"
#include "luareg.h"
#include "vlua.h"
#include "vluautil.h"
#include "sdlcontroller.h"
#include "filesystem.h"
#include "settingsmanager.h"
#include "guirenderlua.h"
#include "guilua.h"
#include "entitylua.h"
#include "iolua.h"
#include "sky.h"
#include "sdlcontroller.h"

#include "patterns.h"

#include <string>

namespace vox {

	float getSeconds() {
		return mSDL->getTime<seconds>();
	}

	void vox_show_message_box(std::string title, std::string message) {
		mSDL->showErrorMessageBox(title, message);
	}

	void registerLuaFunctions() {
		LuaScript::reg("getSeconds", getSeconds);

		LuaScript::reg("makeQuad", 			gui::luaMakeQuad);
		LuaScript::reg("makeQuadFromAtlas", gui::luaMakeQuadFromAtlas);
		LuaScript::reg("makeTiledQuad", 	gui::luaMakeTiledQuad);

		LuaScript::reg("gui_createComponent",    gui::GUILibrary::createComponent);
		LuaScript::reg("gui_makeWindow",	gui::GUILibrary::makeWindow);
		LuaScript::reg("gui_setVisible",	gui::GUILibrary::setVisible);
		LuaScript::reg("gui_setSize",		gui::GUILibrary::setSize);
		LuaScript::reg("gui_addListener",	gui::GUILibrary::addListener);
		LuaScript::reg("gui_bringToFront",	gui::GUILibrary::bringToFront);
		LuaScript::reg("gui_updateSkin", 	gui::GUILibrary::updateSkin);
		LuaScript::reg("gui_setPos",	 	gui::GUILibrary::setPos);
		LuaScript::reg("gui_setParent",		gui::GUILibrary::setParent);
		LuaScript::reg("gui_setHue",		gui::GUILibrary::setHue);
		LuaScript::reg("gui_setText",		gui::GUILibrary::setText);
		LuaScript::reg("gui_setTextPos",	gui::GUILibrary::setTextPos);
		LuaScript::reg("gui_getTextSize",	gui::GUILibrary::getTextSize);
		LuaScript::reg("gui_setInteractable",	 gui::GUILibrary::setInteractable);
		LuaScript::reg("gui_setTextMode",	 gui::GUILibrary::setTextMode);
		LuaScript::reg("exitGame",          gui::GUILibrary::exitGame);

		LuaScript::reg("global_addListener",gui::GUILibrary::addGlobalListener);
		LuaScript::reg("setMouseGrabbed",	gui::GUILibrary::setMouseGrabbed);

		LuaScript::reg("ent_setPlayer", 	EntityLibrary::setPlayer);
		LuaScript::reg("ent_createEntity", 	EntityLibrary::createEntity);
		LuaScript::reg("ent_remove",		EntityLibrary::remove);
		LuaScript::reg("ent_getPos", 		EntityLibrary::getPos);
		LuaScript::reg("ent_getVel", 		EntityLibrary::getVel);
		LuaScript::reg("ent_getSize", 		EntityLibrary::getSize);
		LuaScript::reg("ent_setStat", 		EntityLibrary::setStat);
		LuaScript::reg("ent_setMesh", 		EntityLibrary::setMesh);
		LuaScript::reg("ent_setSkeleton", 	EntityLibrary::setSkeleton);
		LuaScript::reg("ent_setAnimation",	EntityLibrary::setAnimation);
		LuaScript::reg("ent_setAnimSpeed",	EntityLibrary::setAnimSpeed);
		LuaScript::reg("ent_setPos",		EntityLibrary::setPos);
		LuaScript::reg("ent_setVel", 		EntityLibrary::setVel);
		LuaScript::reg("ent_setAcc", 		EntityLibrary::setAcc);
		LuaScript::reg("ent_setNoClip",		EntityLibrary::setNoClip);
		LuaScript::reg("ent_setSolid", 		EntityLibrary::setSolid);
		LuaScript::reg("ent_setUntouchable",EntityLibrary::setUntouchable);
		LuaScript::reg("ent_setTerrainVolume",	EntityLibrary::setTerrainVolume);

		LuaScript::reg("eff_create",		EffectLibrary::create);
		LuaScript::reg("eff_startStop",		EffectLibrary::startStop);
		LuaScript::reg("eff_setDurations", 	EffectLibrary::setDurations);
		LuaScript::reg("eff_setTimeLeft", 	EffectLibrary::setTimeLeft);
		LuaScript::reg("eff_getTimeLeft", 	EffectLibrary::getTimeLeft);
		LuaScript::reg("eff_getPos",		EffectLibrary::getPos);
		LuaScript::reg("eff_remove",		EffectLibrary::remove);
		LuaScript::reg("eff_addLightComp", 	EffectLibrary::addLightComp);
		LuaScript::reg("eff_addSpriteComp",	EffectLibrary::addSpriteComp);
		LuaScript::reg("eff_addParticleEmitterComp",	EffectLibrary::addParticleEmitterComp);
		LuaScript::reg("eff_setPos",		EffectLibrary::setPos);

		LuaScript::reg("set_save", 			SettingsManager::set_save);
		LuaScript::reg("set_bool", 			SettingsManager::set_bool);
		LuaScript::reg("set_key", 			SettingsManager::set_key);
		LuaScript::reg("set_res", 			SettingsManager::set_res);
		LuaScript::reg("set_number", 		SettingsManager::set_number);

		LuaScript::reg("vox_renderDebug", 	EntityLibrary::renderDebug);
		LuaScript::reg("vox_getLuaPath",	getLuaPath);
		LuaScript::reg("vox_show_message_box", vox_show_message_box);

		LuaScript::reg("setAtmosVal",		Sky::setAtmosValue);
		LuaScript::reg("mulTimeRate",		Sky::mulTimeRate);

		// Hecka long names... Good thing this will have a wrapper, I guess
		LuaScript::reg("pat_createBoxPattern", pat::PATTERN_CREATE_BOX);
		LuaScript::reg("pat_createBoxPattern_startEnd", pat::PATTERN_CREATE_BOX_START_END);
		LuaScript::reg("pat_createRoofFrameDiagonal", pat::PATTERN_CREATE_ROOF_FRAME_DIAGONAL);
		LuaScript::reg("pat_createConstantVoxelIDFunctor", pat::VOXEL_ID_FUNCTOR_CREATE_CONSTANT);
		LuaScript::reg("pat_createCheckerboardVoxelIDFunctor", pat::VOXEL_ID_FUNCTOR_CREATE_CHECKERBOARD);
		LuaScript::reg("pat_assignFinalPattern", pat::PATTERN_ASSIGN_STRUCTURE_FINAL_PATTERN);
		LuaScript::reg("pat_mergePatterns", pat::MERGE_PATTERNS);
	}

}
