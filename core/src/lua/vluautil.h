/*
 * vluautil.h
 *
 *  Created on: May 24, 2015
 *      Author: Dani
 */

#ifndef VLUAUTIL_H_
#define VLUAUTIL_H_

#include "vlua.h"
#include "vglm.h"

namespace vox {

	struct lvec2 : public LuaObject {
		float x, y;

		lvec2(const glm::vec2& defVal = {0, 0});
		void init(const glm::vec2& v);

		lvec2& operator=(const glm::vec2& v);
		operator glm::vec2();
		operator glm::ivec2();
	};

	struct lvec3 : public LuaObject {
		float x, y, z;

		lvec3(const glm::vec3& defVal = {0, 0, 0});
		void init(const glm::vec3& v);

		lvec3& operator=(const glm::vec3& v);
		operator glm::vec3();
	};

	struct lvec4: public LuaObject {
		float x, y, z, w;

		lvec4(const glm::vec4& defVal = {1, 0, 0, 0});
		void init(const glm::vec4& v);

		lvec4& operator=(const glm::vec4& v);
		operator glm::vec4();
	};

}

#endif /* VLUAUTIL_H_ */
