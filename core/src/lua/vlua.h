#ifndef VLUA_H_
#define VLUA_H_

#include "console.h"
#include "luastore.h"

#ifdef __custom__
#include <lua.hpp>
#else
#include <luajit-2.0/lua.hpp>
#endif

#include <string>
#include <vector>
#include <type_traits>
#include <utility>
#include <initializer_list>
#include <memory>
#include <functional>
#include <tuple>
#include <stack>

namespace vox {

	struct LuaPair;
	template<typename T> struct LuaPrimitivePair;
	struct LuaArrayBase {};
	template<typename T> struct LuaArray;
	template<typename T> struct LuaArrayPair;
	struct LuaObject;
	struct LuaObjectPair;
	template<typename T> struct LuaFunction;
	template<typename T, typename ... ArgTypes> struct LuaFunction<T(ArgTypes...)>;
	template<typename ... ArgTypes> struct LuaFunction<void(ArgTypes...)>;
	template<typename T, typename ... ArgTypes> struct LuaFunctionPair;

	// Required by Vox
	struct Asset;

	// Generic default value
	template<typename T>
	static T _getDefault() { return T(); }

	class LuaScript {
	template<typename T> friend struct LuaArray;
	template<typename T> friend struct LuaArrayPair;
	template<typename T> friend struct LuaPrimitivePair;
	friend struct LuaObjectPair;
	template<typename R> friend struct LuaFunction;
	friend struct LuaStoreObject;
	template<typename T> friend struct LuaStoreType;

	public:
		// Required by vox
		static LuaScript * allocate(const Asset&);

		// Calls init
		LuaScript(const std::string& filename);

		// Initialize a luascript with g as the global table
		LuaScript(LuaScript * s, const std::string& g);

		LuaScript();
		virtual ~LuaScript();

		// Loads and reads the script using the Lua API
		// Does not do anything if the script is already loaded
		void init(const std::string& filename);
		bool isLoaded();

		// Gets a variable defined in the script or in a table in the script
		// To access a value in a table, use "table.variable"
		// This function uses the private function _getTop for template specification
		template<typename T>
		T get(const std::string& varName, const T& defVal = _getDefault<T>()) {
			if (!isLoaded()) {
				_handleVariableError(varName, "No script is loaded");
				return defVal;
			}

			T ret; // Return value

			if (_elevate(varName))
				ret = _getTop<T>(varName);
			else
				ret = defVal;

			_clearStack();
			return ret;
		}

		// Gets an array defined in the script or in a table in the script
		// To access an array in a table, use "table.array"
		// Uses the private function _getTop for template specification
		template<typename T>
		std::vector<T> getArray(const std::string& varName, const std::vector<T>& defVal = std::vector<T>(0)) {
			if (!isLoaded()) {
				_handleVariableError(varName, "No script is loaded");
				return defVal;
			}

			std::vector<T> v;

			if (_elevate(varName) && lua_istable(_state, -1)) {
				lua_pushnil(_state);
				while (lua_next(_state, -2)) {
					v.push_back(_getTop<T>(varName));
					lua_pop(_state, 1);
				}
				v.shrink_to_fit();
			} else
			v = defVal;
			_clearStack();
			return v;
		}

		// Calls a function with return type R and arguments ArgTypes
		// To be used in conjunction with LuaFunction
		template<typename R, typename ... ArgTypes>
		R callFunction(const std::string& funcName, ArgTypes& ... args) {
			if (!isLoaded()) {
				_handleVariableError(funcName, "No script is loaded");
				return _getDefault<R>();
			}
			R ret;
			_arg = 0;
			if (_elevate(funcName)) {
				_pushRecursive(args...);
				if (lua_pcall(_state, _arg, 1, 0))
					println(ERROR, _getPop<std::string>("Error Message").c_str());
				ret = _getTop<R>("Return Value");
			} else ret = _getDefault<R>();

			_clearStack();
			return ret;
		}

		template<typename ... ArgTypes>
		void callVoidFunction(const std::string& funcName, ArgTypes& ... args) {
			if (!isLoaded()) {
				_handleVariableError(funcName, "No script is loaded");
				return;
			}

			_arg = 0;
			if (_elevate(funcName)) {
				_pushRecursive(args...);
				if (lua_pcall(_state, _arg, 1, 0))
					println(ERROR, _getTop<std::string>("Error Message").c_str());
			}

			_clearStack();
		}

		bool variableExists(const std::string& var);
		bool errorFlagSet();

		// For member functions
		template<unsigned int T, typename R, typename C, typename ... ArgTypes>
		static void reg(const std::string& name, R(C::*f)(ArgTypes...)) {
			_funcs.push_back(std::tuple<std::string, lua_CFunction>(name, _createRegisterFunc<T, R, ArgTypes...>(std::function<R(ArgTypes...)>(f))));
		}
		// For non-member functions
		template<unsigned int T, typename R, typename ... ArgTypes>
		static void reg(const std::string& name, R(*f)(ArgTypes...)) {
			_funcs.push_back(std::tuple<std::string, lua_CFunction>(name, _createRegisterFunc<T, R, ArgTypes...>(std::function<R(ArgTypes...)>(f))));
		}

#define reg(x, y) LuaScript::reg<__COUNTER__>(x, y)

	private:
		lua_State * _state;
		int _arg;
		std::stack<int> _levels;
		std::vector<std::string> _nameStack;
		bool _real = true; // Defines whether or not this should be destructed
		std::string _globalTable = "";
		bool _errorFlag = false;

		// Private constructor for use with the static functions only
		LuaScript(lua_State * state);

		// Generic default push
		// Pushes a variable to the top of the stack
		template<typename T>
		typename std::enable_if<!std::is_base_of<LuaObject, T>::value && !std::is_base_of<LuaArrayBase, T>::value, void>::type
		_pushTop(const T& var) { lua_pushnil(_state); }
		// Hacky specialization for classes that extend LuaObject (see the is_base_of's)
		void _pushTop(const LuaObject& obj);
		// Hacky specialization for classes that extend LuaArrayType (see the is_base_of's)
		template<typename T>
		void _pushTop(const LuaArray<T>& arr);

		template<typename T>
		void _pushArgument(T& arg) { _arg++; _pushTop(arg); }

		// Expands a variadic template back via recursion
		template<typename T, typename ... Tn>
		void _pushRecursive(T& a, Tn& ... b) {
			_pushArgument(a);
			_pushRecursive(b...);
		}
		void _pushRecursive() {}

		// Generic default get
		// Gets the value of the variable at the top of the stack
		template<typename T>
		typename std::enable_if<!std::is_base_of<LuaObject, T>::value && !std::is_base_of<LuaArrayBase, T>::value, T>::type
		_getTop(const std::string& varName) { return _getDefault<T>(); }

		template<typename T>
		typename std::enable_if<std::is_base_of<LuaObject, T>::value, T>::type
		_getTop(const std::string& varName) {
			T pass = T();
			_populateTemporaryLuaObject(&pass);
			return pass;
		}
		void _populateTemporaryLuaObject(LuaObject * o);

		template<typename T>
		typename std::enable_if<std::is_base_of<LuaArrayBase, T>::value, T>::type
		_getTop(const std::string& varName) {
			T ret = T();
			ret._getFromTop(this, varName);
			return ret;
		}

		template<typename T>
		T _getPop(const std::string& varName) {
			if (lua_gettop(_state) > 0) {
				T t = _getTop<T>(varName);
				lua_pop(_state, 1);
				return t;
			} else
				return _getDefault<T>();
		}

		// Raises the variable to the top of the variable stack
		// Return false if the variable couldn't be found
		bool _elevate(const std::string& varName, bool printErrors = true);

		// Clears all the elements this class touched from the stack
		void _clearStack();

		// Prints out some nice little error messages
		void _handleVariableError(const std::string& varName, const std::string& reason);
		void _handleFunctionError(const std::string& funcName, const std::string& reason);

		template<typename R, unsigned int ID>
		struct _register : public _register<R(), ID> {};

		// Template to register a function
		template<typename R, typename ... ArgTypes, unsigned int ID>
		struct _register<R(ArgTypes...), ID> {
			static void define(std::function<R(ArgTypes...)> f) {
				_f = f;
			}

			static int call(lua_State * state) {
				LuaScript wrapper = LuaScript(state);
				R ret = _f(wrapper._getPop<ArgTypes>("Expanded Function")...);
				wrapper._pushTop(ret);
				return 1;
			}

			static std::function<R(ArgTypes...)> _f;
		};

		template<typename ... ArgTypes, unsigned int ID>
		struct _register<void(ArgTypes...), ID> {
			static void define(std::function<void(ArgTypes...)> f) {
				_f = f;
			}

			static int call(lua_State * state) {
				LuaScript wrapper = LuaScript(state);
				_f(wrapper._getPop<ArgTypes>("Expanded Function")...);
				return 0;
			}

			static std::function<void(ArgTypes...)> _f;
		};

		template<typename ... ArgTypes, unsigned int ID>
		struct _register<void(LuaScript*, ArgTypes...), ID> {
			static void define(std::function<void(LuaScript*, ArgTypes...)> f) {
				_f = f;
			}

			static int call(lua_State * state) {
				LuaScript wrapper = LuaScript(state);
				_f(&wrapper, wrapper._getPop<ArgTypes>("Expanded Function")...);
				return 0;
			}

			static std::function<void(LuaScript*, ArgTypes...)> _f;
		};

		template<typename R, typename ... ArgTypes, unsigned int ID>
		struct _register<R(LuaScript*, ArgTypes...), ID> {
			static void define(std::function<R(LuaScript*, ArgTypes...)> f) {
				_f = f;
			}

			static int call(lua_State * state) {
				LuaScript wrapper = LuaScript(state);
				R ret = _f(&wrapper, wrapper._getPop<ArgTypes>("Expanded Function")...);
				wrapper._pushTop(ret);
				return 1;
			}

			static std::function<R(LuaScript*, ArgTypes...)> _f;
		};

		// Creates a wrapper function to the function you give it
		template<unsigned int T, typename R, typename ... ArgTypes>
		static int(*_createRegisterFunc(std::function<R(ArgTypes...)> f))(lua_State*) {
			LuaScript::_register<R(ArgTypes...), T>::define(f);
			return LuaScript::_register<R(ArgTypes...), T>::call;
		}

		static std::vector<std::tuple<std::string, lua_CFunction>> _funcs;
	};

	// I stopped bothering to comment the magicks a long time ago
	template<typename R, typename ... ArgTypes, unsigned int ID>
	typename std::function<R(ArgTypes...)> LuaScript::_register<R(ArgTypes...), ID>::_f;
	template<typename ... ArgTypes, unsigned int ID>
	typename std::function<void(ArgTypes...)> LuaScript::_register<void(ArgTypes...), ID>::_f;
	template<typename R, typename ... ArgTypes, unsigned int ID>
	typename std::function<R(LuaScript*, ArgTypes...)> LuaScript::_register<R(LuaScript*, ArgTypes...), ID>::_f;
	template<typename ... ArgTypes, unsigned int ID>
	typename std::function<void(LuaScript*, ArgTypes...)> LuaScript::_register<void(LuaScript*, ArgTypes...), ID>::_f;

	// Template magic, _getDefault<std::string>() will redirect here
	template<> inline std::string _getDefault() { return "null"; }
	template<> inline LuaStorePtr _getDefault() { return 0; }

	// Template specializations for _pushTop
	template<> inline void LuaScript::_pushTop(const std::string& s) { lua_pushstring(_state, s.c_str()); }
	template<> inline void LuaScript::_pushTop(const int& i) { lua_pushnumber(_state, i); }
	template<> inline void LuaScript::_pushTop(const float& f) { lua_pushnumber(_state, f); }
	template<> inline void LuaScript::_pushTop(const bool& b) { lua_pushboolean(_state, b); }
	template<> inline void LuaScript::_pushTop(const LuaStorePtr& o) { o->_pushVal(this); }

	// Template specializations for _getTop
	template<> inline bool LuaScript::_getTop<bool>(const std::string& varName) { return (bool)lua_toboolean(_state, -1); }
	template<> inline float LuaScript::_getTop<float>(const std::string& varName) {
		if (!lua_isnumber(_state, -1))
			_handleVariableError(varName, "Not a number.");
		return (float)lua_tonumber(_state, -1);
	}
	template<> inline int LuaScript::_getTop<int>(const std::string& varName) {
		if (!lua_isnumber(_state, -1))
			_handleVariableError(varName, "Not a number.");
		return (int)lua_tonumber(_state, -1);
	}
	template<> inline std::string LuaScript::_getTop<std::string>(const std::string& varName) {
		if (!lua_isstring(_state, -1))
			_handleVariableError(varName, "Not a string.");
		else
			return std::string(lua_tostring(_state, -1));
		return _getDefault<std::string>();
	}
	template<> inline LuaStorePtr LuaScript::_getTop<LuaStorePtr>(const std::string& varname) {
		return LuaStoreObject::_createStoreObjectFromTop(this);
	}

	//-----------------------Lua Array-----------------------//
	template<typename T>
	struct LuaArray : public LuaArrayBase {
		friend struct LuaScript;

		LuaArray() {};
		LuaArray(const std::vector<T>& v) { init(v); }
		LuaArray(std::initializer_list<T> l) { init(std::vector<T>(l)); }

		void init(LuaScript& ls, const std::string& varName) { _val = ls.getArray<T>(varName); }
		void init(const std::vector<T>& v) { _val = v; _val.shrink_to_fit(); }

		// Returns the "i"th element in the array
		const T& operator[](const size_t& i) const { return _val[i]; }
		const T& at(const size_t& i) const {
			if (i < size() && i >= 0)
				return (*this)[i];
			printlnf(LUAERROR, "Array index out of bounds: %d", (int)i);
			return _getDefault<T>();
		}

		const T& front() const { return at(0); }
		const T& back() const { return at(size() - 1); }
		size_t size() const { return _val.size(); }

		typename std::vector<T>::const_iterator begin() const { return _val.begin(); }
		typename std::vector<T>::const_iterator end() const { return _val.end(); }
		typename std::vector<T>::const_iterator rbegin() const { return _val.rbegin(); }
		typename std::vector<T>::const_iterator rend() const { return _val.rend(); }

		const std::vector<T>& toVector() const { return _val; }

	private:
		void _getFromTop(LuaScript * ls, const std::string& varName) {
			if (lua_istable(ls->_state, -1)) {
				lua_pushnil(ls->_state);
				while (lua_next(ls->_state, -2)) {
					_val.push_back(ls->_getTop<T>(varName));
					lua_pop(ls->_state, 1);
				}
				_val.shrink_to_fit();
			} else ls->_handleVariableError(varName, "Not an array.");
		}

		std::vector<T> _val;
	};

	//-----------------------Lua Object-----------------------//
	struct LuaObject {
		friend class LuaScript;

		LuaObject(std::vector<std::shared_ptr<LuaPair>> map);
		virtual ~LuaObject() {}

		virtual void init(LuaScript& ls, const std::string& varName);
		bool isCreated();

	protected:
		bool _created;
		std::vector<std::shared_ptr<LuaPair>> _map;
	};

	//-----------------------Lua Function-----------------------//
	template<typename R> struct LuaFunction : public LuaFunction<R()> {};

	template<typename R, typename ... ArgTypes>
	struct LuaFunction<R(ArgTypes ...)> {
		R operator()(ArgTypes& ... args) {
			if (!_created) {
				println(LUAERROR, "Tried to call an uninitialized function.");
				return _getDefault<R>();
			}
			return _ls->callFunction<R, ArgTypes...>(name, args...);
		}

		void init(LuaScript& ls, const std::string& funcName) {
			_ls = &ls;
			name = funcName;

			_created = ls._elevate(funcName);
			ls._clearStack();
		}

		bool isCreated() { return _created; }

	private:
		bool _created = false;
		LuaScript * _ls;
		std::string name;
	};

	template<typename ... ArgTypes>
	struct LuaFunction<void(ArgTypes...)> {
		void operator()(ArgTypes& ... args) {
			if (!_created) {
				println(LUAERROR, "Tried to call an uninitialized function.");
			}
			_ls->callVoidFunction<ArgTypes...>(name, args...);
		}

		void init(LuaScript& ls, const std::string& funcName) {
			_ls = &ls;
			name = funcName;

			_created = ls._elevate(funcName);
			ls._clearStack();
		}

		bool isCreated() { return _created; }

	private:
		bool _created = false;
		LuaScript * _ls;
		std::string name;
	};

	//-----------------------Lua Pair-----------------------//
	struct LuaPair {
		virtual void populate(LuaScript& ls, const std::string& rootName) = 0;
		virtual void pushValue(LuaScript& ls) = 0;

		virtual ~LuaPair() {}
	};

	template<typename T>
	struct LuaPrimitivePair : public LuaPair {
		LuaPrimitivePair(T * out, const std::string& name, const T& defVal) : _ptr(out), _name(name), _defVal(defVal) {}

		void populate(LuaScript& ls, const std::string& rootName) { (*_ptr) = ls.get<T>(rootName + "." + _name, _defVal); }
		void pushValue(LuaScript& ls) { ls._pushTop(_name); ls._pushTop(*_ptr); lua_settable(ls._state, -3); }

	private:
		T * _ptr;
		std::string _name;
		T _defVal;
	};

	template<typename T>
	struct LuaArrayPair : public LuaPair {
		LuaArrayPair(LuaArray<T> * out, const std::string& name) : _ptr(out), _name(name) {}
		void populate(LuaScript& ls, const std::string& rootName) { _ptr->init(ls, rootName + "." + _name); }
		void pushValue(LuaScript& ls) { ls._pushTop(_name); ls._pushTop(*_ptr); lua_settable(ls._state, -3); }

	private:
		LuaArray<T> * _ptr;
		std::string _name;
	};

	struct LuaObjectPair : public LuaPair {
		LuaObjectPair(LuaObject * out, const std::string& name);
		void populate(LuaScript& ls, const std::string& rootName);
		void pushValue(LuaScript& ls) { ls._pushTop(_name); ls._pushTop(*_ptr); lua_settable(ls._state, -3); }

	private:
		LuaObject * _ptr;
		std::string _name;
	};

	template<typename R, typename ... ArgTypes>
	struct LuaFunctionPair : public LuaPair {
		LuaFunctionPair(LuaFunction<R(ArgTypes...)> * out, const std::string& name) : _ptr(out), _name(name) {}
		void populate(LuaScript& ls, const std::string& rootName) { _ptr->init(ls, rootName + "." + _name); }
		void pushValue(LuaScript& ls) { printlnf(LUA, "Warning: Attempted to push function %s", _name.c_str()); }

	private:
		LuaFunction<R(ArgTypes...)> * _ptr;
		std::string _name;
	};

	template<typename T>
	void LuaScript::_pushTop(const LuaArray<T>& arr) {
		lua_newtable(_state);
		for (int i = 0; i < arr.size(); i++) {
			_pushTop(i);
			_pushTop(arr[i]);
			lua_settable(_state, -3);
		}
	}

	// This is a template so it goes here :3
	template<typename T>
	struct LuaStoreType : public LuaStoreObject {
		friend class LuaScript;

		virtual ~LuaStoreType() {}
		LuaStoreType(T t) { _val = t; }

	private:
		void _pushVal(LuaScript* s) { s->_pushTop(_val); }
		T _val;
	};


	// Ignore Voodoo c++11 magicks
	template<typename T>
	inline typename std::enable_if<!std::is_base_of<LuaObject, T>::value && !std::is_base_of<LuaArrayBase, T>::value, std::shared_ptr<LuaPair>>::type
	lmake(T& t, const std::string& s, const T& defVal = _getDefault<T>()) { return std::make_shared<LuaPrimitivePair<T>>(&t, s, defVal); }

	template<typename T>
	inline std::shared_ptr<LuaPair> lmake(LuaArray<T>& t, const std::string& s) { return std::make_shared<LuaArrayPair<T>>(&t, s); }

	inline std::shared_ptr<LuaPair> lmake(LuaObject& t, const std::string& s) { return std::make_shared<LuaObjectPair>(&t, s); }

	template<typename R, typename ... ArgTypes>
	inline std::shared_ptr<LuaPair> lmake(LuaFunction<R(ArgTypes...)>& func, const std::string& s) { return std::make_shared<LuaFunctionPair<R, ArgTypes...>>(&func, s); }
	template<typename R>
	inline std::shared_ptr<LuaPair> lmake(LuaFunction<R>& func, const std::string& s) { return std::make_shared<LuaFunctionPair<R>>(&func, s); }

#define lmakem(x) lmake(x, #x)

#define lmakevec_loop_concat(arg1, arg2)   lmakevec_loop_concat1(arg1, arg2)
#define lmakevec_loop_concat1(arg1, arg2)  lmakevec_loop_concat2(arg1, arg2)
#define lmakevec_loop_concat2(arg1, arg2)  arg1##arg2

#define lmakevec_loop_16(x, ...) lmakem(x), lmakevec_loop_15(__VA_ARGS__)
#define lmakevec_loop_15(x, ...) lmakem(x), lmakevec_loop_14(__VA_ARGS__)
#define lmakevec_loop_14(x, ...) lmakem(x), lmakevec_loop_13(__VA_ARGS__)
#define lmakevec_loop_13(x, ...) lmakem(x), lmakevec_loop_12(__VA_ARGS__)
#define lmakevec_loop_12(x, ...) lmakem(x), lmakevec_loop_11(__VA_ARGS__)
#define lmakevec_loop_11(x, ...) lmakem(x), lmakevec_loop_10(__VA_ARGS__)
#define lmakevec_loop_10(x, ...) lmakem(x), lmakevec_loop_9(__VA_ARGS__)
#define lmakevec_loop_9(x, ...) lmakem(x), lmakevec_loop_8(__VA_ARGS__)
#define lmakevec_loop_8(x, ...) lmakem(x), lmakevec_loop_7(__VA_ARGS__)
#define lmakevec_loop_7(x, ...) lmakem(x), lmakevec_loop_6(__VA_ARGS__)
#define lmakevec_loop_6(x, ...) lmakem(x), lmakevec_loop_5(__VA_ARGS__)
#define lmakevec_loop_5(x, ...) lmakem(x), lmakevec_loop_4(__VA_ARGS__)
#define lmakevec_loop_4(x, ...) lmakem(x), lmakevec_loop_3(__VA_ARGS__)
#define lmakevec_loop_3(x, ...) lmakem(x), lmakevec_loop_2(__VA_ARGS__)
#define lmakevec_loop_2(x, ...) lmakem(x), lmakevec_loop_1(__VA_ARGS__)
#define lmakevec_loop_1(x, ...) lmakem(x)

#define lmakevec_loop_narg(...) lmakevec_loop_narg_(__VA_ARGS__, lmakevec_loop_rseq_n())
#define lmakevec_loop_narg_(...) lmakevec_loop_arg_n(__VA_ARGS__)
#define lmakevec_loop_arg_n(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16, n, ...) n
#define lmakevec_loop_rseq_n() 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0

#define lmakevec_loop_(n, x, ...) lmakevec_loop_concat(lmakevec_loop_, n)(x, __VA_ARGS__)
#define lmakevec(...) lmakevec_loop_(lmakevec_loop_narg(__VA_ARGS__), __VA_ARGS__)
}

#endif
