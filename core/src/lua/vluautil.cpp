/*
 * vluautil.cpp
 *
 *  Created on: May 24, 2015
 *      Author: Dani
 */

#include "vluautil.h"

namespace vox {

	lvec2::lvec2(const glm::vec2& defVal) : LuaObject ({
			lmake(x, "x", defVal.x),
			lmake(y, "y", defVal.y)
		}) {}

	void lvec2::init(const glm::vec2& v) {
		this->x = v.x;
		this->y = v.y;
	}

	lvec2& lvec2::operator=(const glm::vec2& v) {
		init(v);
		return *this;
	}

	lvec2::operator glm::vec2() {
		return {x, y};
	}

	lvec2::operator glm::ivec2() {
		return {x, y};
	}


	lvec3::lvec3(const glm::vec3& defVal) : LuaObject ({
			lmake(x, "x", defVal.x),
			lmake(y, "y", defVal.y),
			lmake(z, "z", defVal.z)
		}) {}

	void lvec3::init(const glm::vec3& v) {
		this->x = v.x;
		this->y = v.y;
		this->z = v.z;
	}

	lvec3& lvec3::operator=(const glm::vec3& v) {
		init(v);
		return *this;
	}

	lvec3::operator glm::vec3() {
		return {x, y, z};
	}


	lvec4::lvec4(const glm::vec4& defVal) : LuaObject ({
			lmake(x, "x", defVal.x),
			lmake(y, "y", defVal.y),
			lmake(z, "z", defVal.z),
			lmake(w, "w", defVal.w)
		}) {}

	void lvec4::init(const glm::vec4& v) {
		this->x = v.x;
		this->y = v.y;
		this->z = v.z;
		this->w = v.w;
	}

	lvec4& lvec4::operator=(const glm::vec4& v) {
		init(v);
		return *this;
	}

	lvec4::operator glm::vec4() {
		return {x, y, z, w};
	}
}
