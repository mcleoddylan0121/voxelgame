#version 140

// Input vertex data, different for all executions of this shader.
in vec3 vertexPosition_modelspace;

uniform vec2 size;
uniform vec2 position;

// Output data ; will be interpolated for each fragment.
out vec2 UV;

void main(){
	vec3 adjustedPos = vertexPosition_modelspace;
	adjustedPos.xy *= size;
	adjustedPos.xy += position;
	adjustedPos = adjustedPos * 2.0 - 1.0;
	gl_Position = vec4(adjustedPos,1);
	UV = vertexPosition_modelspace.xy;
}