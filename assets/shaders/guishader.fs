#version 140

uniform sampler2D textureAtlas;

in vec2 localUV;
flat in vec2 atlasRef;
flat in vec2 textureSize;

uniform vec4 hue = vec4(1,1,1,0.8);

out vec4 color;

void main() {
	color = hue * texture(textureAtlas, atlasRef + fract(localUV)*textureSize);
}