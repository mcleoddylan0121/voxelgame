#version 140

in vec2 fUV;
in vec4 fColor;

uniform sampler2D tex;

out vec4 outColor;


#ifdef CLOUD

in float dist;

float haze(float dist) {
	float c = 1.0;
	float haziness = 0.9;
	float first = 1.5;
	dist = max(dist-first, 0);
	float p = (dist*dist)/c;
	return pow(haziness,p);
}


#endif

void main() {
	outColor = texture(tex, fUV) * fColor;

#ifdef CLOUD

	// outColor.a *= haze( dist/10000.0 );

#endif	

}