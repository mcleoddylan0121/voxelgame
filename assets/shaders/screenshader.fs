#version 140

#ifdef BLUR
in vec2 v_blurTexCoords[6];
#endif
in vec2 UV;

out vec4 outColor;

uniform vec4 color;

uniform sampler2D tex;

uniform vec2 screenSize = vec2(1440,900)/2.0;

#ifdef BLUR
vec4 blur13(sampler2D image) {
	vec4 o = vec4(0.0);
	
	mat4 samples = mat4(
		texture(image, v_blurTexCoords[0]) + texture(image, v_blurTexCoords[5]),
		texture(image, v_blurTexCoords[1]) + texture(image, v_blurTexCoords[4]),
		texture(image, v_blurTexCoords[2]) + texture(image, v_blurTexCoords[3]),
		texture(image, UV                )
	);
	vec4 intensities = vec4(
		0.010381362401148057, 0.09447039785044732, 0.2969069646728344, 0.1964825501511404
	);
	
	return samples * intensities;
}
#endif


#ifdef DISTORT
uniform sampler2D distortionTexture; // contains the UV coordinates of all the coordinates on the screen
#endif

void main() {

#if defined(BLUR_HORIZONTAL)
	outColor = blur13(tex) * color;
	
#elif defined(BLUR_VERTICAL)
	outColor = blur13(tex) * color;

#elif defined(CLOUDS_TO_SCREEN)

	vec4 im = texture(tex, UV);
	outColor.rgb = im.rgb * color.rgb;
	outColor.a = im.r; // TODO: use intensity lmao

#elif defined(DISTORT)
	
	outColor = texture( tex, UV + texture(distortionTexture,UV).xy ) * color;
	
#else

	outColor = texture(tex, UV) * color;

#endif
}