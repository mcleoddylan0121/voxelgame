float hash(vec2 co){
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

vec3 expose(vec3 rgb) {
	return vec3(1)-exp(-rgb);
}

uniform vec4 fogColor = vec4(135.0/255.0, 206.0/255.0, 235.0/255.0, 1.0);
uniform float fogDensity = 0.01;

vec4 fog(vec4 color, vec4 fcolor, float depth, float density) {
    float f = exp(-depth*density*depth*density);
    return mix(fcolor, color, f);
}

uniform float clip_far = 1000000.0;
uniform float clip_near = 0.1;

uniform float PI = 3.1415926535;

float getLinearDepth(float depth) {
	float z_n = 2.0 * depth - 1.0;
	return 2.0 * clip_near * clip_far / (clip_far + clip_near - z_n * (clip_far - clip_near));
}

vec3 packNormal(vec3 unpackd) {
	return (unpackd+1)/2;
}

vec3 unpackNormal(vec3 packd) {
	return packd*2-1;
}

vec4 packNormal(vec4 unpackd) {
	return vec4(packNormal(unpackd.xyz),unpackd.w);
}

vec4 unpackNormal(vec4 packd) {
	return vec4(unpackNormal(packd.xyz),packd.w);
}

#ifdef INCLUDE_DYNAMIC_LIGHT_HANDLING
const int TILES = 24;
struct Light {
	vec3 position;
	float radius;
	vec3 color;
	float compensation;
	float brightness;
	float pad1;
	float pad2;
	float pad3;
};

const int MAX_LIGHTS = 256;

layout(std140) uniform LightData {
	Light lights[MAX_LIGHTS];
};

struct Tile {
	uvec4 data[3];
};

layout(std140) uniform TileData {
	Tile tiles[TILES * TILES];
};

uint getTileData(int tileIndex, int i) {
	return (tiles[tileIndex].data[i/16][(i/4)%4] & (255u << (8*(i%4)))) >> (8*(i%4));
}

layout(std140) uniform Sun {
	vec3 sun_invDirection;
	float sun_power;
	vec3 sun_color;
	float sun_compensation;
};

uniform lowp vec2 poissonDisk[16] = vec2[](
	vec2( -0.94201624, -0.39906216 ),
	vec2( 0.94558609, -0.76890725 ),
	vec2( -0.094184101, -0.92938870 ),
	vec2( 0.34495938, 0.29387760 ),
	vec2( -0.91588581, 0.45771432 ),
	vec2( -0.81544232, -0.87912464 ),
	vec2( -0.38277543, 0.27676845 ),
	vec2( 0.97484398, 0.75648379 ),
	vec2( 0.44323325, -0.97511554 ),
	vec2( 0.53742981, -0.47373420 ),
	vec2( -0.26496911, -0.41893023 ),
	vec2( 0.79197514, 0.19090188 ),
	vec2( -0.24188840, 0.99706507 ),
	vec2( -0.81409955, 0.91437590 ),
	vec2( 0.19984126, 0.78641367 ),
	vec2( 0.14383161, -0.14100790 )
);

const int shadowCascades = 3;
uniform vec3 shadowCascadeSizes = vec3(8, 16, 32);
uniform vec3 totalCascadeSizes = vec3(0, 16, 48);

layout(std140) uniform depthBias {
	mat4 depthBiasMVP[shadowCascades];
};

#ifndef NO_SHADOWMAP
	uniform sampler2DArrayShadow shadowMap;
#endif

vec3 getFragmentLighting(vec3 position, vec3 normal, float linDepth, vec3 llev, int tileIndex) {
	vec3 combinedLight = vec3(0);
	/*
	 * Handle directional light
	 */
	 
	int lightsThisTile = int(getTileData(tileIndex, 0));
	for(int i = 0; i < lightsThisTile; i++) {
		Light l = lights[getTileData(tileIndex, i+1)];
	    vec3 lightDir = l.position - position.xyz;
	    
	    float distsqr = dot(lightDir,lightDir);
	    
	    float atten = clamp(1.0 - distsqr/(l.radius*l.radius), 0.0, 1.0);
	    combinedLight += 1.5 * l.brightness * l.color * atten * atten *
	    	mix(l.compensation, 1, max(dot(normal.xyz,normalize(lightDir)),0.0));
	}
	
	/*
	 * Handle sunlight
	 */	
#ifdef NO_SHADOWMAP
	combinedLight += 1;
#else
		
	// Extract the position's respective cascade from the depth buffer value
	int cascade = int(linDepth > totalCascadeSizes[1]) + int(linDepth > totalCascadeSizes[2]);
	
	vec4 ShadowCoord;
	if(cascade == 0) {
		ShadowCoord = (depthBiasMVP[0]) * (vec4(position + normal.xyz * 0.03,1));
	} else if(cascade == 1) {
		ShadowCoord = (depthBiasMVP[1]) * (vec4(position + normal.xyz * 0.03,1));
	} else {
		ShadowCoord = (depthBiasMVP[2]) * (vec4(position + normal.xyz * 0.03,1));
	}
	
	float bias = (cascade+1) * 0.005;
	float depthBias = 0.997;
	float visibility = 1.0;
	for (int i=0;i<4;i++)
		visibility -= 0.25*(1.0-texture( shadowMap, vec4(ShadowCoord.xy + poissonDisk[i] / 1536.0, cascade, (ShadowCoord.z*depthBias - bias)/ShadowCoord.w)));
	
	combinedLight += mix(sun_compensation, 1, (min(visibility, cosTheta))) * sun_color * sun_power;
	// + visibility * sun_power * sun_color * pow(cosAlpha,5);
#endif

	/*
	 * Handle static lights
	 */
	
#ifdef USE_BLOOM
	combinedLight += vec3(2.0) * llev;
#else
	combinedLight += vec3(1.6) * llev;
#endif	
	return combinedLight;
}

vec3 scaleSaturation(vec3 c, float scale) {
	vec3 c2 = c * vec3(0.299, 0.587, 0.114);
	float p = (sqrt(dot(c,c2)));
	// return mix(scale,c,p);
	return (c - p) * scale + p;
}

vec4 colorCorrect(vec3 combinedLight, vec4 image) {
	vec3 c = combinedLight * image.rgb; // apply light to the diffuse image

	// c = (atan(3*c - PI/2.0)+1)/2.0;
	// c = scaleSaturation(c, 1.5);
	c = pow(c,vec3(1.2));
	return vec4(c, image.a);
}

#endif