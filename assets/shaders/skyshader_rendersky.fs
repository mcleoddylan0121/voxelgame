#version 140

out vec4 outcolor;
in vec3 UVposition;

const float PI = 3.1415926;

uniform float worldTimeDiff = 0.5;

#ifdef CUBEMAP_FIX
uniform sampler2DArray tex;
uniform sampler2DArray tex2;
#else
uniform samplerCube tex;
uniform samplerCube tex2;
#endif

// only apply exposure if we're resolving an HDR buffer
#ifdef RESOLVE_HDR
vec3 expose(vec3 rgb) {
	return vec3(1)-exp(-rgb);
}
#else
vec3 expose(vec3 rgb) {
	return rgb;
}
#endif

uniform vec4 color;

void main() {
	#ifdef STARS
	outcolor = color * texture(tex, UVposition);
	#else 
	outcolor = color * mix(texture(tex,UVposition), texture(tex2,UVposition), clamp(worldTimeDiff,0.0,1.0));
	#endif
	outcolor.rgb = expose(outcolor.rgb);
}