#version 140 // This corresponds to OpenGL 3.1

in vec3 vertexPosition;
in int tileIndex;

uniform mat4 skyMVP;
uniform mat4 IVP;
uniform vec3 cameraPos;

out vec2 fpos;
out vec3 lookDir;
flat out int index;
void main()
{
	gl_Position = vec4(2*vertexPosition-1,1);
	gl_Position.z = 0.99;
	// Copy position to the fragment shader. Only x and y is needed.
	fpos = vertexPosition.xy;
	index = tileIndex;
}