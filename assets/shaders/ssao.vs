#version 140 // This corresponds to OpenGL 3.1

in vec3 vertexPosition;

out vec2 TexCoords;
void main()
{
	gl_Position = vec4(2*vertexPosition.xy-1,0,1);
	
	TexCoords = vertexPosition.xy;
}