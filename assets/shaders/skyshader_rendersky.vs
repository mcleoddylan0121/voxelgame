#version 140

in vec3 vertexPosition_modelspace;
in float texLayer;
out vec3 UVposition;

uniform float depth = 1.0;

uniform mat4 MVP;

void main() {
	gl_Position = MVP * vec4(vertexPosition_modelspace.xyz, 1); // The w as the z coordinate puts the skybox in the background, as w == zfar clip distance
	gl_Position = vec4(gl_Position.xy, depth * gl_Position.w, gl_Position.w);


#ifdef CUBEMAP_FIX
	int maxC = int(texLayer)/2;
	int sgn = (int(vertexPosition_modelspace[maxC])+1)/2;
	vec2 UV = vec2(
		vertexPosition_modelspace[(maxC+(1+sgn))%3],
		vertexPosition_modelspace[(maxC+(2-sgn))%3]
	);
	UV = (UV+1)/2;
	UVposition = vec3(UV, float(texLayer));
#else
	UVposition = vertexPosition_modelspace; // for looking up from the cubemap
#endif


}