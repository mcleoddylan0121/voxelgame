#version 140
 
in vec2 UV;
 
out vec4 color;
 
uniform float time;

// This is a surprisingly easy converison. Maybe we could make use of HSV color space?
vec3 hsv2rgb(vec3 c) {
	vec4 K = vec4(1, 2.0/3.0, 1.0/3.0, 3.0);
	vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
	return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

vec4 mapColor(float color) {
	return vec4(hsv2rgb(vec3(0.999-color,1,1)),1);
}

vec4 mapColorGrayscale(float color) {
	return vec4(color,color,color,1);
}

vec3 getNoisePos(vec2 pos) {
	return vec3(pos.x + time / 10.0, time / 10.0, pos.y + time / 10.0);
}

void main() {
	color = mapColor(getNoise(getNoisePos(UV)));
}