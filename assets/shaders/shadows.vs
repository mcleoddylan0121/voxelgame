#version 140
 
// Input vertex data, different for all executions of this shader.
in vec3 vertexPosition_modelspace;

// Values that stay constant for the whole mesh.
uniform mat4 M;
uniform mat4 MVP;

void main() {
#ifdef LAYERED
	gl_Position =  M * vec4(vertexPosition_modelspace,1);
#else
    gl_Position =  MVP * vec4(vertexPosition_modelspace,1);
    gl_Position.z = max(gl_Position.z, -1);
#endif
}