#version 140

#ifndef returnType
#define returnType vec4
#endif

// Input vertex data, different for all executions of this shader.
in vec3 vertexPosition_modelspace;

uniform vec2 size;
uniform vec2 position;
uniform float depth;
uniform float rot = 0;
uniform vec2 UVstart = vec2(0);
uniform vec2 UVsize = vec2(1);

uniform vec2 screenSize = vec2(1440,900)/2.0;

#ifdef BLUR
out vec2 v_blurTexCoords[6];
#endif
out vec2 UV;

void main(){
	vec2 adjustedPos = vertexPosition_modelspace.xy;
	
	adjustedPos = adjustedPos * 2 - 1;
	float s = sin(rot);
	float c = cos(rot);
	adjustedPos = vec2(adjustedPos.x * c - adjustedPos.y * s, adjustedPos.x * s + adjustedPos.y * c);
	adjustedPos = (adjustedPos + 1) / 2;
	
	adjustedPos *= size;
	adjustedPos += position;
	adjustedPos = adjustedPos * 2.0 - 1.0;
	
	gl_Position = vec4(adjustedPos, depth, 1);
	UV = UVstart + UVsize * vertexPosition_modelspace.xy;
	
#ifdef BLUR_HORIZONTAL
	vec2 direction = vec2(0.5,0);
#endif
#ifdef BLUR_VERTICAL
	vec2 direction = vec2(0,1);
#endif
#ifdef BLUR
	vec2 off1 = vec2(1.411764705882353) * direction;
	vec2 off2 = vec2(3.2941176470588234) * direction;
	vec2 off3 = vec2(5.176470588235294) * direction;
	
    vec2 v_texCoord = vertexPosition_modelspace.xy;
    vec2 sz = 1 / screenSize;
    v_blurTexCoords[0] = v_texCoord + sz*(-off3);
    v_blurTexCoords[1] = v_texCoord + sz*(-off2);
    v_blurTexCoords[2] = v_texCoord + sz*(-off1);
    v_blurTexCoords[3] = v_texCoord + sz*( off1);
    v_blurTexCoords[4] = v_texCoord + sz*( off2);
    v_blurTexCoords[5] = v_texCoord + sz*( off3);
    
#endif

}

