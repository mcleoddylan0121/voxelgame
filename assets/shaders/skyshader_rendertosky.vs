#version 140

// Input vertex data, different for all executions of this shader.
in vec3 vertexPosition_modelspace;

// Output data ; will be interpolated for each fragment.
out vec3 fragPosition;
uniform vec3 faceDirection;
uniform float layer;
uniform mat4 model;

void main() {
	gl_Position = vec4(vertexPosition_modelspace * 2.0 - 1.0 ,1);
	// rotate from positive z to whatever orientation this is in
	fragPosition = (model * vec4(vertexPosition_modelspace.xy * 2.0 - 1.0, 1.0, 1.0)).xyz;
}