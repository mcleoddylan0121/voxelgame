#version 140

uniform float worldTime = 0;

vec3 getNoisePos(vec2 pos) {
	return vec3(pos.x + worldTime / 250.0, worldTime / 250.0, pos.y + worldTime / 250.0);
}

in vec2 UV;
in vec3 worldPosition;
in vec3 fnorm;
out vec4 color;

uniform vec3 sundir;

float haze(float dist) {
	float c = 1.0;
	float haziness = 0.9;
	float first = 0.7;
	dist = max(dist-first, 0);
	float p = (dist*dist)/c;
	return pow(haziness,p);
}

float phase(float alpha, float g){
    float a = 3.0*(1.0-g*g);
    float b = 2.0*(2.0+g*g);
    float c = 1.0+alpha*alpha;
    float d = pow(1.0+g*g-2.0*g*alpha, 1.5);
    return (a/b)*(c/d);
}

// Project the surface gradient (dhdx, dhdy) onto the surface (n, dpdx, dpdy)
vec3 CalculateSurfaceGradient(vec3 n, vec3 dpdx, vec3 dpdy, float dhdx, float dhdy)
{
    vec3 r1 = cross(dpdy, n);
    vec3 r2 = cross(n, dpdx);
 
    return (r1 * dhdx + r2 * dhdy) / dot(dpdx, r1);
}
 
// Move the normal away from the surface normal in the opposite surface gradient direction
vec3 PerturbNormal(vec3 n, vec3 dpdx, vec3 dpdy, float dhdx, float dhdy)
{
    return normalize(n - CalculateSurfaceGradient(n, dpdx, dpdy, dhdx, dhdy));
}

// Calculate the surface normal using screen-space partial derivatives of the height field
vec3 CalculateSurfaceNormal(vec3 position, vec3 normal, float height)
{
    vec3 dpdx = dFdx(position);
    vec3 dpdy = dFdy(position);
 
    float dhdx = dFdx(height);
    float dhdy = dFdy(height);
 
    return PerturbNormal(normal, dpdx, dpdy, dhdx, dhdy);
}

vec3 expose(vec3 k) {
	return 1-exp(-k);
}

uniform float phaseMod = 0.66;
uniform float phaseMul = 1.0;
uniform float phaseAdd = 0.1;
uniform vec3 brightnessMul = vec3(1);
uniform float maxAlpha = 1.0;
uniform float cutoff = 0.1;
uniform float alphaIncreaseMul = 5.0;

void main() {
	float n = noise(getNoisePos(UV));
	if(n < cutoff) discard;
	
	float alpha = dot(normalize(worldPosition), sundir);

#ifdef BUMPMAP // EXPERIMENTAL
	float bump = bumpmap(getNoisePos(UV));
	vec3 grad = CalculateSurfaceNormal(worldPosition, fnorm, bump);
	float height = worldPosition.y;
	vec3 projectedsunpos = sundir * height / sundir.y;
	vec3 dir = normalize(projectedsunpos - worldPosition);
	float cosTheta = dot(grad.xyz,normalize(dir))+1;
#else
	float cosTheta = 1.0;
#endif

	// TODO: make this uniform-based, for smooth transitions and such
	
	float brightness = clamp(phase(alpha, phaseMod), 0.0, 1.0) * phaseMul + phaseAdd;
	brightness = pow(brightness / (n + 0.15), 0.7);
	brightness *= cosTheta;
	color = vec4(brightnessMul*brightness, clamp(alphaIncreaseMul*(n - cutoff), 0, maxAlpha) * haze(length(worldPosition)));
	
	//color.xyz = pow(expose(color.xyz),vec3(2.2));
	color.a = pow(color.a,1/2.2);
	
	color.rgb /= 4.0;
}