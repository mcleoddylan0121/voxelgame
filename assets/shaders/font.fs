#version 140

in vec2 UV;
in vec4 fragBaseColor;

uniform vec4 color;
uniform sampler2D textureAtlas;

out vec4 fragColor;

void main() {
	float tex = texture(textureAtlas, UV).r;
	int itex = int(round(tex*255.0));
	float texColor = float(itex & 224) / 224.0; // extract the topmost 3 bits
	float texAlpha = float(itex & 31)  / 32.0;  // extract the bottommost 5 bits
	fragColor = vec4(fragBaseColor.xyz * color.xyz * texColor, fragBaseColor.a * color.a * texAlpha);
}