#version 140 // This corresponds to OpenGL 3.1

in vec3 vertexPosition;

out vec2 fpos;
void main()
{
	gl_Position = vec4(2*vertexPosition-1,1);
	gl_Position.z = 0.99;
	// Copy position to the fragment shader. Only x and y is needed.
	fpos = vertexPosition.xy;
}