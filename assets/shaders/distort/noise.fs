#version 140 // This corresponds to OpenGL 3.3

precision mediump float;
uniform sampler2D diffuseTex; // The color information
uniform sampler2D gBufferTex; // World position
uniform sampler2D normalTex;  // Normals
uniform vec3 cameraPos;       // The coordinate of the camera

in vec2 fpos;

out vec4 outColor;

uniform float worldTime;
uniform vec3 cameraOffs = vec3(0);

vec3 getNoisePos(vec2 pos) {
	return vec3(pos.x, worldTime/10.0, pos.y);
}

vec2 slightlyLessCoolDistortFunction(vec2 pos) {
	float xxx = noise_x(getNoisePos(pos));
	float yyy = noise_y(getNoisePos(pos) + vec3(0,1000,0));
	return vec2(xxx,yyy);
}

vec2 coolDistortFunction(float distance) {
	float intensity = sin( ( distance - worldTime * 10 ) /10);
	
	float checkem = intensity * 0.05 - 0.025;
	
	return vec2(0, 0) / distance;
}

void main() {
	vec4 pos_vec4 = texture(gBufferTex,fpos);
	//if(pos_vec4.w < 0.5) return;
	vec3 pos = pos_vec4.rgb;
	float dist = distance(cameraOffs,pos);

	//vec4 c = vec4(slightlyLessCoolDistortFunction(fpos),0,1);
	vec4 c = vec4(0);
	outColor = c;
}