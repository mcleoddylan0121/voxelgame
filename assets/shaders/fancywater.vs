#version 140

in vec3 normal;
in vec3 position;
in vec2 UV;
in vec4 color;
in float height;
in float occlusion;
in vec3 tangent;
in vec3 bitangent;
in vec3 light;
in vec3 voxelLighting;

out float R_baseColor;
out vec3 fNorm;

uniform vec3 camPos = vec3(0,0,0);

uniform mat4 MVP;
uniform mat4 M;
uniform mat4 normalMatrix;

void calculateBaseColorFresnel(vec3 wNormal, vec3 wPos) {

	vec3 I = normalize(wPos - camPos);
	
	float _Bias = 0.0;
	float _Scale = 1.0;
	float _Power = 0.4;
	
	R_baseColor = _Bias + _Scale * pow(1.0 + dot(I, wNormal), _Power);
}

uniform float J = 3.1415926 * 2.0 / 8.0;

void main() {
	vec3 p = position;
	vec4 camPos = MVP * vec4(p, 1);
	vec4 worldPos = M * vec4(p, 1);
	vec3 n = normal;
	n.x += cos(J*p.x)/4.0;
	n.y += sin(J*p.y)/4.0;
	n = normalize(n);
	vec3 worldNormal = normalize( (normalMatrix * vec4(n, 1)).xyz );
	
	calculateBaseColorFresnel(worldNormal.xyz, worldPos.xyz/worldPos.w);
	gl_Position = camPos;
	fNorm = worldNormal;
}