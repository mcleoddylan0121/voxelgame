#version 140

in vec2 fragUV;

uniform sampler2D tex;

out vec4 outColor;

uniform vec4 color = vec4(1,1,1,1);

void main() { 
	outColor = texture(tex, fragUV) * color;
}