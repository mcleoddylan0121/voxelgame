#version 140

uniform vec4 dark = vec4(0, 0.85, 0.0371, 1);
uniform vec4 light = vec4(0.17, 0.629, 1, 1); 

in float R_baseColor;

out vec4 outColor;

void main() {
	vec4 baseColor = mix(dark,light, R_baseColor);
	baseColor.a = 0.5;
	outColor = baseColor;
}

