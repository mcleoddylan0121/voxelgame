#version 140

#ifndef SOLID
in vec2 fUV;
#else
in vec4 fcolor;
#endif

in vec3 fposition;
in float focclusion;
in vec2 tileCoord;
in vec3 fnorm;
in vec3 lighting;
in float linDepth;

uniform sampler1D occlusionCurve;
uniform sampler2D ditherSampler;

uniform sampler2D tex1;
uniform sampler2D tex2;

out vec4 outColor;

uniform float timeDiff;

float occlude(float val) {
	return texture(occlusionCurve, val).r;
}
vec3 getDitherValue() {
	return vec3(texture2D(ditherSampler, gl_FragCoord.xy / 8.0).r) / 256.0; // faster, a little worse.
}

void main() {

#ifdef SOLID
	outColor = vec4(fcolor.xyz * occlude(focclusion), fcolor.a);
#else
	outColor = mix(texture(tex1, fUV), texture(tex2, fUV), timeDiff);
#endif
	
	//int tileIndex = int(tileCoord.x)+TILES*int(tileCoord.y);
	//vec3 combinedLight = getFragmentLighting(fposition, fnorm, linDepth, lighting, tileIndex);
	//outColor = colorCorrect2(combinedLight, outColor);
	
	outColor = fog(outColor, fogColor, linDepth, fogDensity);
	outColor += vec4(getDitherValue(), 0);
}