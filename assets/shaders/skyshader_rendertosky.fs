#version 140

uniform vec3 faceDirection;
uniform vec3 sunPos;

uniform float cameraHeight = 0.9;
uniform float rayleighBrightness = 4.0;
uniform float mieBrightness = 0.1;
uniform float sunBrightness = 8.0;
uniform float intensity = 2.0;

uniform float rayleighStrength = 1.0;
uniform float mieStrength = 1.0;
uniform float rayleighCollectionPower = 0.8;
uniform float mieCollectionPower = 1.0;
uniform float scatterStrength = 1.0;
uniform float mieDistribution = 0.5;

uniform float surfaceHeight = 0.8;
uniform vec3 Kr = vec3(0.18867780436772762, 0.4978442963618773, 0.6616065586417131);
uniform int stepCount = 5;

in vec3 fragPosition;
out vec4 color;

float phase(float alpha, float g){
    float a = 3.0*(1.0-g*g);
    float b = 2.0*(2.0+g*g);
    float c = 1.0+alpha*alpha;
    float d = pow(1.0+g*g-2.0*g*alpha, 1.5);
    return (a/b)*(c/d);
}

float atmoDepth(vec3 position, vec3 dir){
    float a = dot(dir, dir);
    float b = 2.0*dot(dir, position);
    float c = dot(position, position)-1.0;
    float det = b*b-4.0*a*c;
    float detSqrt = sqrt(det);
    float q = (-b - detSqrt)/2.0;
    float t1 = c/q;
    return t1;
}

float horizonExtinction(vec3 position, vec3 dir, float radius){
    float u = dot(dir, -position);
    if(u<=0.0){
        return 1.0;
    }
    vec3 near = position + u*dir;
    if(length(near) <= radius){
        return 0.0;
    }
    else{
        vec3 v2 = normalize(near)*radius - position;
        float diff = acos(dot(normalize(v2), dir));
        return smoothstep(0.0, 1.0, pow(diff*2.0, 3.0));
    }
}

vec3 absorb(float dist, vec3 color, float factor){
    return color-color*pow(Kr, vec3(factor/dist));
}

vec3 expose(vec3 k) {
	return 1-exp(-k);
}

void main() {
	vec3 lightDir = sunPos;
	vec3 eyeDir = normalize(fragPosition);
	float alpha = dot(eyeDir, lightDir);
	float rayF = phase(alpha, -0.01)*rayleighBrightness;
	float mieF = phase(alpha, mieDistribution)*mieBrightness;
	float spot = smoothstep(0, 15, phase(alpha, 0.9995))*sunBrightness;
	
	vec3 eyePosition = vec3(0, cameraHeight, 0);
	float eyeDepth = atmoDepth(eyePosition, eyeDir);
	float stepLength = eyeDepth/float(stepCount);
	float eyeExtinction = horizonExtinction(vec3(0,cameraHeight,0), eyeDir, surfaceHeight-0.15 );
	
	vec3 rayleighCollected = vec3(0.0, 0.0, 0.0);
	vec3 mieCollected = vec3(0.0, 0.0, 0.0);
	
	for(int i=0; i < stepCount; i++) {
	    float sample_distance = stepLength*float(i);
	    vec3 position = eyePosition + eyeDir*sample_distance;
	    float extinction = horizonExtinction(
	        position, lightDir, surfaceHeight-0.35
	    );
	    float sample_depth = atmoDepth(position, lightDir);
	    
	    vec3 influx = absorb(sample_depth, vec3(intensity), scatterStrength)*extinction;
	    
	    rayleighCollected += absorb(
		    sample_distance, Kr*influx, rayleighStrength
		);
		mieCollected += absorb(
		    sample_distance, influx, mieStrength
		);
	}
	
	rayleighCollected = (
		    rayleighCollected *
		    eyeExtinction *
		    pow(eyeDepth, rayleighCollectionPower)
		)/float(stepCount);
	mieCollected = (
		    mieCollected *
		    eyeExtinction * 
		    pow(eyeDepth, mieCollectionPower)
		)/float(stepCount);
	
	color = vec4(
	    spot*mieCollected +
	    mieF*mieCollected +
	    rayF*rayleighCollected,
	    1
	);
	
	color.rgb /= 2.0;
	
	//color = vec4(expose(color.xyz), color.a);
}