#version 140

in vec2 UV; // noise position, [0,1]

uniform vec2 UVstart;
uniform vec2 UVsize;

out vec2 _fUV_;
void main() {
	vec2 vUV = UV*UVsize + UVstart;
	gl_Position = vec4(vUV*2-1, 0, 1);
	_fUV_ = UV;
}