#version 140

// Interpolated values from the vertex shaders
in vec3 fnorm_packed;
in vec3 fpos;
centroid in vec2 fUV;
in vec3 sunlightExposure;

// Ouput data
out vec4 color;

#ifndef FORWARD
out vec4 normal;
out vec4 light;
out vec4 g_buffer; // position in viewspace
#endif

uniform float a = 0.1400122886866666; // 8.0*(PI-3.0)/(3.0*PI*(4.0-PI))

uniform sampler1D occlusionCurve;
uniform sampler2D ditherSampler; // We're writing to an 8 bit color channel here (deferred rendering or not), so we need to dither
uniform sampler2D voxelColorTex;
uniform sampler2D voxelLightTex;

float erf_guts(float x) {
   float x2 = x*x;
   return exp(-x2 * (4.0/PI + a*x2) / (1.0+a*x2));
}

float occlude(float val) {
	return texture(occlusionCurve, val).r;
}

// "error function": integral of exp(-x*x)
float erf(float x) {
   float sign=1.0;
   if (x<0.0) sign=-1.0;
   return sign*sqrt(1.0-erf_guts(x));
}

vec3 getDitherValue() {
	//return hash(fpos.xz) / 256.0; // slower, a little better
	return texture2D(ditherSampler, gl_FragCoord.xy / 8.0).rrr / 256.0; // faster, a little worse.
}

vec4 colorCorrect(vec3 combinedLight, vec4 image) {
	vec3 c = combinedLight * image.rgb; // apply light to the diffuse image

	// c = (atan(3*c - PI/2.0)+1)/2.0;
	// c = scaleSaturation(c, 1.5);
	c = pow(c,vec3(1.2));
	return vec4(c, image.a);
}

void main() {
#ifndef FORWARD
	color = texture(voxelColorTex, fUV);
	color.xyz *= sunlightExposure;
	color.xyz += getDitherValue();
	
	light = texture(voxelLightTex, fUV);
	
	normal = vec4(fnorm_packed,1);
	g_buffer = vec4(fpos.xyz,1);
#else
	float linDepth = -fpos.z;

	vec4 c = texture(voxelColorTex, fUV);
	c.xyz *= sunlightExposure;
	c.xyz += getDitherValue();
	
	vec3 combinedLight = texture(voxelLightTex, fUV).rgb + 1;
	
	c = colorCorrect(combinedLight,c);
	c = fog(c, fogColor, linDepth / 2, fogDensity); // I don't actually know why i need to divide by 2 here.
	c = c + 3*vec4(0,0,0,0);
	c = max(c, vec4(0,0,0,1));
	c.rgb = expose(c.rgb*1.5);
	color = c;
	
#endif
}