#version 140

// Input vertex data, different for all executions of this shader.
in vec4 vertexPosition_modelspace;
in vec4 vertexUV;

// Output data ; will be interpolated for each fragment.
out vec3 fnorm_packed;
out vec3 fpos;
out vec2 fUV;
out vec3 sunlightExposure;

// Values that stay constant for the whole mesh.
uniform mat4 MVP;
uniform mat4 MV;
uniform mat4 M;
uniform mat4 IM;
uniform mat4 V;
uniform mat4 normalMatrix; // inverse transpose of the model matrix
uniform vec3 normalLighting[6];
uniform vec3 normalVectors[6];

vec3 makeNormalFromID(int ID) { return normalVectors[ID]; }

vec4 distort(vec4 p) {
	vec2 v = p.xy / p.w;
	// Convert to polar coords:
	float theta = atan(v.y,v.x);
	float radius = length(v);
	// Distort:
	radius = pow(radius, 2);
	// Convert back to Cartesian:
	v.x = radius * cos(theta);
	v.y = radius * sin(theta);
	p.xy = v.xy * p.w;
	return p;
}

uniform float worldTime = 0;
uniform vec3 playerPos = vec3(0);

vec4 coolDistortFunction(vec4 vPosVec4) {
	vec4 worldspace = M*vPosVec4;
	worldspace.xyz /= worldspace.w;
	float dist = distance(worldspace.xyz,playerPos);
	float intensity = sin( ( dist - worldTime * 20 ) / 2)*1+1;
	
	vec3 direction = (worldspace.xyz-playerPos)/dist;
	
	worldspace.xyz += direction * intensity;
	worldspace.xyz *= worldspace.w;
	
	
	vPosVec4 = IM * worldspace;
	
	
	
	//vPosVec4.y += intensity / max(dist,16);
	
	return vPosVec4;
}

void main() {
	int vertexNormalID = int(vertexPosition_modelspace.w + vertexUV.w*4);

	sunlightExposure = normalLighting[vertexNormalID];
	
	vec4 vPosVec4 = vec4(vertexPosition_modelspace.xyz,1);
	
	// vPosVec4 = coolDistortFunction(vPosVec4);
	
	// Output position of the vertex, in clip space : MVP * position
	gl_Position = MVP * vPosVec4;
	vec4 fpos_vec4 = MV * vPosVec4;
	fnorm_packed = packNormal(makeNormalFromID(vertexNormalID));
	
	fpos = fpos_vec4.xyz / fpos_vec4.w;
	
	fUV = vertexUV.xy / 512.0;
}

