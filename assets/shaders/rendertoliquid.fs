#version 140

uniform float _time_ = 0;

out vec4 _outcolor_; // weird name to avoid collisions
in vec2 _fUV_;

// called in order of colorFunction, normalFunction

void main() {
	_outcolor_ = colorFunction(_time_, _fUV_);
}