#version 140

in vec2 v_blurTexCoords[9];
in vec2 UV;

out float outColor;

uniform float color;

uniform sampler2D tex;

#define T0 0.39894228
#define T1 0.24197072
#define T2 0.05399096
#define T3 0.00443184
#define T4 0.00013383
#define GC 1

float blur7(sampler2D image) {
	float o = 0.0;
	//o += texture(image, v_blurTexCoords[0]).r*(T4/GC);
    //o += texture(image, v_blurTexCoords[1]).r*(T3/GC);
    o += texture(image, v_blurTexCoords[2]).r*0.2;
    o += texture(image, v_blurTexCoords[3]).r*0.2;
	o += texture(image, v_blurTexCoords[4]).r*0.2;
	o += texture(image, v_blurTexCoords[5]).r*0.2;
    o += texture(image, v_blurTexCoords[6]).r*0.2;
    //o += texture(image, v_blurTexCoords[7]).r*(T3/GC);
    //o += texture(image, v_blurTexCoords[8]).r*(T4/GC);
	return o;
}

void main() {
	outColor = blur7(tex);
}