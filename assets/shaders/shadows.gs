#version 140

layout(triangles) in;
layout(triangle_strip, max_vertices = 9) out;

layout(std140) uniform sunVPs {
	mat4 vps[3];
};

void main() {
	gl_Layer = 0;
	gl_Position = vps[0]*gl_in[0].gl_Position;
	gl_Position.z = max(gl_Position.z, -1);
	EmitVertex();
	gl_Layer = 0;
	gl_Position = vps[0]*gl_in[1].gl_Position;
	gl_Position.z = max(gl_Position.z, -1);
	EmitVertex();
	gl_Layer = 0;
	gl_Position = vps[0]*gl_in[2].gl_Position;
	gl_Position.z = max(gl_Position.z, -1);
	EmitVertex();
	EndPrimitive();
	
	gl_Layer = 1;
	gl_Position = vps[1]*gl_in[0].gl_Position;
	gl_Position.z = max(gl_Position.z, -1);
	EmitVertex();
	gl_Layer = 1;
	gl_Position = vps[1]*gl_in[1].gl_Position;
	gl_Position.z = max(gl_Position.z, -1);
	EmitVertex();
	gl_Layer = 1;
	gl_Position = vps[1]*gl_in[2].gl_Position;
	gl_Position.z = max(gl_Position.z, -1);
	EmitVertex();
	EndPrimitive();
	
	gl_Layer = 2;
	gl_Position = vps[2]*gl_in[0].gl_Position;
	gl_Position.z = max(gl_Position.z, -1);
	EmitVertex();
	gl_Layer = 2;
	gl_Position = vps[2]*gl_in[1].gl_Position;
	gl_Position.z = max(gl_Position.z, -1);
	EmitVertex();
	gl_Layer = 2;
	gl_Position = vps[2]*gl_in[2].gl_Position;
	gl_Position.z = max(gl_Position.z, -1);
	EmitVertex();
	EndPrimitive();
}