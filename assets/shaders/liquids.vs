#version 140

in vec3 position;
in vec3 normal;
in vec2 UV;
in vec4 color;
in float height; // a little extra Z precision, so we can have puddles and stuff
in float occlusion;
in vec3 tangent;
in vec3 bitangent;
in vec3 light;
in vec3 voxelLighting;

uniform mat4 MVP;
uniform mat4 M;
uniform mat4 normalMatrix;

#ifdef SOLID
out vec4 fcolor;
#else
out vec2 fUV;
#endif

out vec3 fposition;
out float focclusion;
out vec2 tileCoord;
out vec3 fnorm;
out vec3 lighting;
out float linDepth;

void main() {
	vec4 vposition = vec4(position.xyz, 1);
	vposition.y -= 1-height;
	
	gl_Position = MVP * vposition;
	fposition = (M * vposition).xyz;
	focclusion = occlusion * 50.0;
	tileCoord = (gl_Position.xy/gl_Position.w + 1.0) / 2.0 * TILES;
	fnorm = (normalMatrix * vec4(normal, 1)).xyz;
	lighting = light + voxelLighting*4.0;
	
	linDepth = getLinearDepth(gl_Position.z/gl_Position.w);
	
#ifdef SOLID // solid color
	
	fcolor = color;
	
#else // read from a texture
	
	fUV = UV;
	
#endif

}