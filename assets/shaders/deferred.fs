#version 140 // This corresponds to OpenGL 3.3

precision mediump float;
uniform sampler2D diffuseTex; // The color information
uniform sampler2D gBufferTex; // World position
uniform sampler2D normalTex;  // Normals
uniform sampler2D lightTex;
uniform sampler2D bloomTex;
uniform sampler2D skyTex;
uniform sampler2D ssaoTex;
uniform vec3 cameraPos;       // The coordinate of the camera

uniform mat4 IVP;

in vec2 fpos;
flat in int index;

out vec4 outColor;

void main() {
	vec4 c = vec4(0,0,0,1);
	vec4 normal = texture( normalTex, fpos.xy ).xyzw;
	
	vec3 llev = texture( lightTex, fpos.xy).rgb;
	vec3 blm = texture(bloomTex, fpos).rgb;
	
	blm = max(llev,blm); // Perhaps there's a nicer looking operation that can be done here, as this was made up on the spot
	
	if(normal.w > 0.5) { // The normal.w acts as a crappy stencil buffer. Maybe i should migrate to a true stencil format?
		
		float ambientOcclusion = 0.2+0.8*texture(ssaoTex, fpos.xy).r;
	
		normal.xyz = unpackNormal(normal.xyz);
		vec4 image = texture(diffuseTex, fpos);
		vec3 position = texture(gBufferTex, fpos.xy ).xyz;
		float linDepth = -position.z;
		gl_FragDepth = 0;
		
		vec3 combinedLight = getFragmentLighting(position, normal.xyz, linDepth, llev, index) * ambientOcclusion;
		c = colorCorrect(combinedLight,image);
		c = fog(c, fogColor, linDepth / 2, fogDensity); // I don't actually know why i need to divide by 2 here.
	} else {
		vec3 skyColor = texture(skyTex, fpos).rgb;
		c.rgb += skyColor * (1-normal.w);
		gl_FragDepth = 1;
	}
	
	c = c + 3*vec4(blm,0);
	c = max(c, vec4(0,0,0,1));
	c.rgb = expose(c.rgb*1.5);
	outColor = c;
}