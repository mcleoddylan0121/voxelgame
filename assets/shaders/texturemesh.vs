#version 140

in vec3 vertexPosition_modelspace;
in vec2 vertexUV;

out vec2 fragUV;

uniform mat4 MVP;

uniform float depth = 1.0;

void main() {
#ifdef SKY
	gl_Position = MVP * vec4(vertexPosition_modelspace.xyz, 1); // The w as the z coordinate puts the skybox in the background, as w == zfar clip distance
	gl_Position = vec4(gl_Position.xy, depth * gl_Position.w, gl_Position.w);
#else
	gl_Position = MVP * vec4(vertexPosition_modelspace, 1);
#endif
	fragUV = vertexUV;
}