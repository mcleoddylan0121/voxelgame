#version 140

in vec2 vertexPosition_absolute;
in vec2 texCoord;
in vec4 vertexColor;

out vec2 UV;
out vec4 fragBaseColor;

uniform vec2 screenSize = vec2(1024, 768);
uniform vec2 textureSize = vec2(1024, 1024);

uniform vec2 pos = vec2(0,0);
uniform vec2 size = vec2(1,1);

uniform float layer = 0;

void main() {
	vec2 glPos = vertexPosition_absolute * size + pos;
	glPos /= screenSize;
	glPos = glPos*2-1;
	gl_Position = vec4(glPos, layer / 10000.0, 1.0);
	UV = texCoord / textureSize;
	fragBaseColor = vertexColor;
}