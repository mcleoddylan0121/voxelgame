#version 140

out vec4 outcolor;

uniform vec4 color;

void main() {
	outcolor = color;
}