#version 140

out float FragColor;

uniform sampler2D gBufferTexture;
uniform sampler2D normalTexture;
uniform sampler2D texNoise;
uniform sampler2D randomSamples;

#define numSamples 8

#define totalSamples 23
#define sampleConst 1
#define scaleConst 11

uniform vec3 samples[totalSamples];
uniform float scales[totalSamples];

uniform mat4 P;

// tile noise texture over screen based on screen dimensions divided by noise size

#define noiseScale1Const 5.0
#define noiseScale2Const 5.0
#define screenRes vec2(1440, 900)

const vec2 noiseScale = screenRes/noiseScale1Const;
const vec2 noiseScale2 = screenRes/noiseScale2Const;
in vec2 TexCoords;


// TODO: find a better way to do this, possibly?
vec3 getSample(int seed, int index) {
	int sPi = seed + index;
	return samples[(sPi * sampleConst) % totalSamples] * scales[(sPi * scaleConst) % totalSamples];
}

void main() {
	vec3 fragPos = texture(gBufferTexture,TexCoords).xyz;

	vec4 normal_prep = texture(normalTexture,TexCoords);
	
	if(normal_prep.w < 0.01) return; // early exit should significantly improve performance here, as nearby regions are stenciled similarly
	
	vec3 normal = unpackNormal(normal_prep.rgb);
	vec3 randomVec = texture(texNoise, TexCoords * noiseScale).xyz;
	vec3 tangent = normalize(randomVec - normal * dot(randomVec, normal));
	vec3 bitangent = cross(normal, tangent);
	
	int seed = int(256.0*texture(randomSamples, TexCoords * noiseScale2).r);
	
	mat3 TBN = mat3(tangent, bitangent, normal); 
	
	float occlusion = 0.0;
	
	for(int i = 0; i < numSamples; ++i) {
	    // get sample position
	    vec3 sample = TBN * getSample(seed,i); // From tangent to view-space
	    sample = fragPos + sample; 
	    
	    vec4 offset = vec4(sample, 1.0);
		offset = P * offset; // from view to clip-space
		offset.xy /= offset.w; // perspective divide
		offset.xy = offset.xy * 0.5 + 0.5; // transform to range 0.0 - 1.0  
		float sampleDepth = texture(gBufferTexture, offset.xy).z;
		float rangeCheck = smoothstep(0.0, 1.0, 0.8 / abs(fragPos.z - sampleDepth));
		occlusion += step(sample.z,sampleDepth) * rangeCheck;    
	}  
	occlusion = 1.0 - (occlusion / numSamples);
	FragColor = occlusion;
}

// ALCHEMY AO: (currently looks awful with low (< 40) sample count)

/*
#version 140
 
// Occlusion Output
out float fragColor;

in vec2 TexCoords;
 
// Note: Keep in mind that the proper values for the ambient occlusion
//       shader should change depending on the units in which your game world
//       works within. It may require dynamic modification of these terms.
uniform float SampleRadius = 0.5;
uniform float ShadowScalar = 1.3;
uniform float DepthThreshold = 0.0025;
uniform float ShadowContrast = 0.5;
uniform int NumSamples = 32;
 
uniform sampler2D gBufferTexture;
uniform sampler2D normalTexture;
 
float radicalInverse_VdC(uint bits) {
	bits = (bits << 16u) | (bits >> 16u);
	bits = ((bits & 0x55555555u) << 1u) | ((bits & 0xAAAAAAAAu) >> 1u);
	bits = ((bits & 0x33333333u) << 2u) | ((bits & 0xCCCCCCCCu) >> 2u);
	bits = ((bits & 0x0F0F0F0Fu) << 4u) | ((bits & 0xF0F0F0F0u) >> 4u);
	bits = ((bits & 0x00FF00FFu) << 8u) | ((bits & 0xFF00FF00u) >> 8u);
	return float(bits) * 2.3283064365386963e-10; // / 0x100000000
}
 
vec2 hammersley2d(int i, int N) {
	return vec2(float(i)/float(N), radicalInverse_VdC(uint(i)));
}

// Generate a random angle based on the XY XOR hash
float randAngle() {
	uint x = uint(gl_FragCoord.x);
	uint y = uint(gl_FragCoord.y);
	return (30u * x ^ y + 10u * x * y);
}

uniform vec2 screenSize = vec2(1440,900);

vec3 getViewPosition(vec2 clipSpace) {
	return texture(gBufferTexture,clipSpace).rgb;
}

void main()
{
	float visibility = 0.0;
	vec3 P = texture(gBufferTexture,TexCoords).rgb;
	vec4 normal_prep = texture(normalTexture,TexCoords);
	vec3 N = unpackNormal(normal_prep.rgb);
	float PerspectiveRadius = (SampleRadius / P.z);
 
	// Main sample loop, this is where we will preform our random
	// sampling and estimate the ambient occlusion for the current fragment.
	for (int i = 0; i < NumSamples; ++i) {
		// Generate Sample Position
		vec2 E = hammersley2d(i, NumSamples) * vec2(3.1415926, 6.2831852);
		E.y += randAngle(); // Apply random angle rotation
		vec2 sE= vec2(cos(E.y), sin(E.y)) * PerspectiveRadius * cos(E.x);
		vec2 Sample = gl_FragCoord.xy / screenSize + sE;
 
		
 
		// Create Alchemy helper variables
		vec3 Pi         = getViewPosition(Sample);
		vec3 V          = Pi - P;
		float sqrLen    = dot(V, V);
		float Heaveside = step(sqrt(sqrLen), SampleRadius);
		float dD        = DepthThreshold * P.z;
 
		// For arithmetically removing edge-bleeding error
		// introduced by clamping the ambient occlusion map.
		float EdgeError = step(0.0, Sample.x) * step(0.0, 1.0 - Sample.x) *
		                  step(0.0, Sample.y) * step(0.0, 1.0 - Sample.y);
 
		// Summation of Obscurance Factor
		visibility += (max(0.0, dot(N, V) + dD) * Heaveside * EdgeError) / (sqrLen + 0.0001);
	}
 
	// Final scalar multiplications for averaging and intensifying shadows
	visibility *= (2 * ShadowScalar) / NumSamples;
	visibility = max(0.0, 1.0 - pow(visibility, ShadowContrast));
	fragColor = visibility;
}*/