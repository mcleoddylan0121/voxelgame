#version 140

in vec2 pos; // from 0 to screen size
in vec2 UV; // textureinate on the atlas
in vec2 bottomleft; // bottom-left in screen space
in vec2 ref; // bottom-left of the texture in texel space
in vec2 ref2; // size of the texture in texel space
in vec2 rep;
in vec2 repeatSize; // size of a repeated portion on the screen. if rep is false, then ignore this

uniform vec2 offset = vec2(0,0); // the  position of the bottom left
uniform vec2 textureAtlasSize; 
uniform vec2 screenSize;
uniform vec2 stretch = vec2(1, 1);

out vec2 localUV;
flat out vec2 atlasRef;
flat out vec2 textureSize;

uniform float layer = 0;

void main() {
	vec2 pixelDist = pos * stretch - bottomleft;
	vec2 UVdist = UV - ref;
	localUV = vec2(0);
	
	if(rep.x > 0.5) { // the reason i use > 0.5 is becasue repeat is acting as a vec2 of bools
		localUV.x = pixelDist.x / repeatSize.x;
	} else {
		localUV.x = (sign(UVdist.x - 0.5) + 1.0) / 2.0;
	}
	if(rep.y > 0.5) {
		localUV.y = pixelDist.y / repeatSize.y;
	} else {
		localUV.y = (sign(UVdist.y - 0.5) + 1.0) / 2.0;
	}
	
	vec2 rescale = ((pos * stretch + offset) / screenSize) * 2.0 - 1.0;
	gl_Position = vec4(rescale, layer / 10000.0, 1.0);
	
	atlasRef = ref / textureAtlasSize;
	textureSize = ref2 / textureAtlasSize;
}