#version 140

in vec2 quadPositionOffset;
in vec3 position;

#ifndef CLOUD

in vec2 UVstart;
in vec2 UVsize;
in vec4 color;
in vec2 scale;

#else

in float textureIndex;
uniform vec2 UVsize;
uniform vec2 UVgap;
uniform vec4 color;
uniform vec2 scale;

uniform vec3 camPos = vec3(0,0,0);

uniform mat2 camLookXZRotationMatrix = mat2( vec2(1,0), vec2(0,1) ); // XZ lookdir of camera, represented as rotation matrix
uniform float aspectRatio = 16/9;

out float dist;

#endif

out vec2 fUV;
out vec4 fColor;

uniform vec4 globalColor = vec4(1);

uniform mat4 VP;

void main() {

	vec4 screenPos = VP * vec4(position, 1);
	fColor = color * globalColor;
	
#ifndef CLOUD
	fUV = UVstart + quadPositionOffset * UVsize;
	
	screenPos.xy += (quadPositionOffset-0.5)*scale;
	
#else

	vec2 offs = camLookXZRotationMatrix * (quadPositionOffset-0.5)*scale;
	offs.y *= aspectRatio;
	screenPos.xy += offs;
	
	fUV = UVgap * textureIndex + quadPositionOffset * UVsize;
	
	dist = distance(position, camPos);
#endif

	gl_Position = screenPos;

}