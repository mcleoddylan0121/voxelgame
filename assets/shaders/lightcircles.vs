
// Used to draw circles of light, in the case of bloom or dynamic lighting.
// For bloom, we draw circles around dynamic lights, and later blur the result onto the window.
// For dynamic lighting, we draw circles around light sources to find out where the affected region is.

#version 140

in vec2 screenPosition; // [0,1]
in vec2 UV; // [0,1]

#ifdef BLOOM
in float depth; // only needed in the case of bloom, ignored for dynamic lighting
#else
const float depth = 0.f; // the value doesn't actually matter
#endif

out vec2 fUV;

void main() {
	gl_Position = vec4(screenPosition * 2.0 - 1.0, depth, 1.0);
	fUV = UV;
}

