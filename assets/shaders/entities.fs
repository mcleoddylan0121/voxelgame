#version 140

// Interpolated values from the vertex shaders
in vec3 fnorm;
in vec3 fpos;
centroid in vec2 fUV;
in vec3 sunlightExposure;

// Ouput data
out vec4 color;
out vec4 normal;
out vec4 light;
out vec4 g_buffer; // position in viewspace

uniform float a = 0.1400122886866666; // 8.0*(PI-3.0)/(3.0*PI*(4.0-PI))

uniform sampler1D occlusionCurve;
uniform sampler2D ditherSampler; // We're writing to an 8 bit color channel here (deferred rendering or not), so we need to dither
uniform sampler2D voxelColorTex;
uniform sampler2D voxelLightTex;

float erf_guts(float x) {
   float x2 = x*x;
   return exp(-x2 * (4.0/PI + a*x2) / (1.0+a*x2));
}

float occlude(float val) {
	return texture(occlusionCurve,val).r;
}

// "error function": integral of exp(-x*x)
float erf(float x) {
   float sign=1.0;
   if (x<0.0) sign=-1.0;
   return sign*sqrt(1.0-erf_guts(x));
}

vec3 getDitherValue() {
	return vec3(texture(ditherSampler, gl_FragCoord.xy / 8.0).r) / 256.0;
}

void main() {
	color = texture(voxelColorTex, fUV);
	color.xyz *= sunlightExposure;
	
	light = texture(voxelLightTex, fUV);
	
	normal = packNormal(vec4(normalize(fnorm),1));
	g_buffer = vec4(fpos.xyz,1);
	
	color += vec4(getDitherValue(), 0);
}