#version 140

in vec3 vertexPosition_modelspace;

uniform int face;
uniform mat4 M;
uniform mat4 VP;
out vec2 UV;
out vec3 worldPosition;
out vec3 fnorm;
uniform float clip_far = 1000;
uniform float clip_near = 0.01;
void main() {
	gl_Position = VP * M * vec4(vertexPosition_modelspace, 1);
	worldPosition = (M * vec4(vertexPosition_modelspace, 1)).xyz;
	fnorm = vec3(0,-1,0);
	UV = vertexPosition_modelspace.xz;
}