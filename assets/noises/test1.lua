-- test file for defining a noise

noise = {
	type = "combined_noise",
	range = {0, 100},
	noises = {
		{
			type = "octave",
			largestfeature = 100,
			persistence = 0.2,
			range = {0, 10},
			base = "simplex"
		},
		{
			type = "output_shifter",
			range = {0, 100},
			noise = {
				type = "selector",
				first = {
					type = "octave",
					largestfeature = 1000,
					persistence = 0.4,
					range = {-1, 0},
					base = "opensimplex"
				},
				second = {
					type = "combined_noise",
					range = {0, 3},
					noises = {
						{
							type = "octave",
							largestfeature = 100,
							persistence = 0.1,
							base = "simplex"
						},
						{
							type = "octave",
							largestfeature = 100,
							persistence = 0.4,
							range = {0, 0.25}
						}
					}
				},
				selector = {
					type = "fit_curve",
					curve = "cubic_easeinout",
					noise = {
						type = "gaussianator",
						sigma = 0.2,
						shift = 0.1,
						range = {0, 1},
						noise = {
							type = "octave",
							largestfeature = 250,
							persistence = 0.4,
							range = {0, 1},
							base = "opensimplex"
						}
					}
				}
			}
		}
	}
}