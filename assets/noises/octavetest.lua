-- Just a test
noise = CombinedNoise( {-550, 550},
		OctaveNoise(9, defaultNoiseFactory, {0.0,100}, 0.425, 512),
		OctaveNoise(8, ridgedNoiseFactory, {0.0,1000}, 0.54, 12000)
	)