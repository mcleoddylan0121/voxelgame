documentation for various noise types:

generators:

	high level noises, do not take another noise as an input
	
	generators:
	
		conformant bases that only take range arguments 
		simplex, opensimplex, ridged, perlin (TODO: cellular)
		(only simplex and ridged are implemented for GLSL noise)
		arguments: range (2 floats)
		
	octave:
	
		takes any generator, and gives an octaved version of it
		arguments: 
			persistence (float),
			range (2 floats),
			largest feature (float),
			max_octaves (int, default 1,000,000),
			base (generator typename)			
		
	empty:
		takes 1 argument, returns that argument exclusively.
		arguments: value (float)
		
modifiers:

	takes 1 noise as input, modifies the output/input of that noise
	all modifiers take in an input noise as well as the arguments listed
	
	output modifiers:
	
		modifies the output of the given noise
		
		biselector: 
			clamps given noise to min/max, depending on whether it's above or below the boundary
			arguments:
				boundary (float), 
				range (2 floats)
		
		constrainer:
			clamps output to min/max
			also can be used to extend the output to values that it won't reach, in case that's something you want
			arguments: range (2 floats)
				
		discrete_fit:
			floors output into discrete steps
			arguments: steps (int)
				
		output_shifter:
			fits output to match given range
			arguments: range (2 floats)
			
		gaussianator:
			fits the output to a guassian curve, constrained to min/max
			arguments:
				sigma (float),
				shift (float),
				range (2 floats)
				
		fit_curve:
			fits the output to the given curve, and re-weights the input to [0,1], but the output is the noise's [min,max]
			arguments:
				curve (function name)
			
	input modifiers:
		
		modifies the input of the given noise
		
		input_scalar:
			scales the given noise's features by scale
			arguments: scale (1 to 5 floats, depending on the number of dimensions you will use)

miscellaneous:
	
	these are the outcasts of the group. these often depend on more than one noise.
	
	selector:
		linearly mixes the two given noise functions, via the output of the third
		the output of the third is clamped between 0 and 1, so you can specify a value outside of this range to get uneven selection samples
		to specify a mix function, wrap a fitcurve around the selector noise.
		for more complicated mix functions, and for ones that require arguments, use an output modifier instead.
		arguments:
			first (noise)
			second (noise)
			selector (noise)
			
	combined_noise:
		adds the two given noises together. the mins of the given noises must be 0.
		also shifts the output of the noises to given range.
		arguments:
			noises (array of noises)
			range (2 floats)
			
	multiply:
		multiplies the two given noises
		arguments:
			first (noise)
			second (noise)
	
functions:
	
	takes 1 float as an argument, can be used for fit_curve modifiers
	linear, cubic_easeinout, invert
	
	