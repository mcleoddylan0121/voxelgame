-- for cloud generation

-- Noise is in 3D. x axis: x direction, y axis: time, z axis: z direction
-- "wispy"

--[[

noise = 
	CloudCurve(0.46, 0.001,
		Multiplier(
			OctaveNoise(8, defaultNoiseFactory, {0,1}, 0.7, 2),
			Constrainer({0,1}, Generator("simplex",{-1,2},12)) -- this controls if it's clear or cloudy out
		)
	)
	
--]]


importAll "base::Noise"

noise = 
  -- GLSL noise has different properties from non-GLSL noise, lol sry
	CloudCurve(0.45, 0.001,
			OctaveNoise(2, defaultNoiseFactory, {0,1}, 0.7, 2)
	)
	
-- EXPERIMENTAL, uses the gradient of this to shade the clouds
bumpmap = OctaveNoise(2, defaultNoiseFactory, {0,0.9}, 0.5, 2)
