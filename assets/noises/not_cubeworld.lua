importAll "base::Noise"

-- The number of steps in the largest octave, and the multiplier of the num steps per octave. a positive multiplier means more steps for the smaller scales
function discreteNoiseFactoryFactory(steps_largestoctave, step_persistence, basefactory)
	return 
		function(octave, octaves, scale, amp)
			local steps = math.max(math.floor(steps_largestoctave * math.pow(step_persistence,octave) + 0.5),1)
			return DiscreteFit(steps, basefactory(octave, octaves, scale, amp))
		end
end


mySharedNoise = SharedNoise("is this string even necessary?", OctaveNoise(3, defaultNoiseFactory, {-25,25}, 0.3, 1250))

height = 
        OutputShifter( {0,80}, InputWarper(OctaveNoise(4, defaultNoiseFactory, {0,40}, 0.4, 100),  SimplexNoise(60,16.12) + SimplexNoise(30,7.91))) + -- This is the terrain
        FitCurve( "invert", DimensionalNoise(1, {-1000,1000} ) ) -- This is the threshold (less terrain as y dimension increases)
        
--DiscreteFit(50,mySharedNoise) + OutputShifter({-32, 32}, DiscreteFit(16, mySharedNoise)) + OutputShifter({0, 8}, DiscreteFit(4, mySharedNoise))
	
moisture = OctaveNoise(8, defaultNoiseFactory, {0,1}, 0.3, 200)

biome = FitCurve("quadratic_easeinout", OctaveNoise(5, defaultNoiseFactory, {-0.2, 0.2}, 0.3, 240))