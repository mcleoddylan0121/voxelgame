
noise = {
	type = "combined_noise",
	range = {-7600, 17600},
	noises = {
		{ 	-- tiny detail
			type = "octave",
			persistence = 0.4,
			range = {0, 10},
			largestfeature = 100,
			base = "simplex"
		},
		{	-- small detail
			type = "octave",
			persistence = 0.35,
			max_octaves = 3,
			range = {0, 40},
			largestfeature = 400,
			base = "simplex"
		},
		{ -- medium detail (e.g. hills)
			type = "octave",
			persistence = 0.325,
			max_octaves = 5,
			range = {0, 150},
			largestfeature = 1000,
			base = "simplex"
		},
		{ -- large detail (huge stuff)
			type = "octave",
			persistence = 0.3,
			max_octaves = 11,
			range = {0, 5000},
			largestfeature = 75000,
			base = "simplex"
		},
		{ -- gigantic detail (why not)
			type = "octave",
			persistence = 0.31,
			range = {0, 10000},
			largestfeature = 60000,
			base = "simplex"
		},
		{ -- mountains (really tall)
			type = "fit_curve",
			curve = "quintic_easeinout",
			noise = {
				type = "constrainer",
				range = {0, 10000},
				noise = {
					type = "octave",
					persistence = 0.4,
					range = {-10000, 10000},
					largestfeature = 60000,
					base = "simplex" -- TODO: add ridged, or cellular noise to make better looking mountains
				}
			}
		}
	}
}