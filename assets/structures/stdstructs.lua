randMetaTable = {}

function Random(seed)
  local obj = {}
  obj.seed = seed
  setmetatable(obj, randMetaTable)
  return obj
end

structureMetaTable = {}

function Structure(seed)
  local obj = {}
  obj.rand = Random(seed)
  setmetatable(obj, structureMetaTable)
  return obj
end

function Box(seed, size, filled)
  local obj = Structure(seed)
  obj.type = "box"
  obj.size = size or vec(5,5,5)
  obj.filled = filled or false
  if filled then
    
  else
    
  end
  return obj
end