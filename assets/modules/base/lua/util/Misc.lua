function vox_make_index_in_table(tab, obj, parent)
	local _tab = parent or vox_global_table._G
	if not _tab[tab] then
		_tab[tab] = {}
	end
	
	local tempIndex = _tab[tab].ind or 0
	local retVal = "i" .. tempIndex
	_tab[tab].ind = tempIndex + 1

	_tab[tab][retVal] = obj

	return tab .. "." .. retVal
end

function vox_round_places(x, digits)
	local shift = math.pow(10, digits)
	return math.floor( x*shift + 0.5 ) / shift;
end

function vox_copy_table(obj, seen)
	if type(obj) ~= 'table' then return obj end
	if seen and seen[obj] then return seen[obj] end
	local s = seen or {}
	local res = setmetatable({}, getmetatable(obj))
	s[obj] = res
	for k, v in pairs(obj) do res[vox_copy_table(k, s)] = vox_copy_table(v, s) end
	return res
end

function vox_loop_through_index_table(tab, functor, notG)
	local _tab = notG or vox_global_table._G
	for i = 1, _tab[tab].ind do
		functor(_tab[tab]["i" .. (i-1)])
	end
end

function vox_search(k, plist)
	for i=1, #plist do
		local v = plist[i][k]     -- try `i'-th superclass
		if v then return v end
	end
end