--[[

Usage:

Settings.group("mySettingsGroup")
	:addDropdown("myDropdown", onMyDropdownUpdate, "optionA", {"optionA", "optionB"})
	:addCustom("myCustomSetting", onMyCustomUpdate, 0, myCustomComponent)
	:addSlider("mySlider", onMySliderUpdate, 0.5, 0, 1, 0.1)

Check above each function for explanations on each variable.
Note: Do not set a setting to nil (that would cause undefined behavior). Instead, use false.

]]

import "util.Serialize"
import "util.Misc"

Settings = {__settingsTable = {}}
Group = {}
Group_mt = {__index = Group}

local vox_settings_table = vox_global_table._default.settings or {}



-- Get a group so you can add settings to it. Note updateFunc's are called on each add function.
function Settings.group(groupName)
	if Settings.__settingsTable[groupName] == nil then Settings.__settingsTable[groupName] = {} end
	return setmetatable({groupName = groupName}, Group_mt)
end

-- The use of this function is discouraged (Use updateFunc's instead).
function Group.get(self, name, default)
	local setting = Settings.__settingsTable[self.groupName][name]

	if setting == nil then
		error('Tried to read undefined setting "' .. name .. "'.")
		return default
	end

	if setting.value ~= nil then return setting.value
	elseif vox_settings_table[self.groupName] and vox_settings_table[self.groupName][name] ~= nil then
		return vox_settings_table[self.groupName][name]
	else return default end
end

-- For setting settings through the console. Will call the function's updateFunc.
function Group.set(self, name, value)
	local setting = Settings.__settingsTable[self.groupName][name]

	if setting == nil then
		error('Tried to set undefined setting "' .. name .. "'.")
		return
	end

	setting.value = value

	if setting.updateFunc then setting.updateFunc(name, value) end
end



--[[
	Each of these functions return the group used, making them chainable.
	
	name          The setting's name.
	updateFunc    Called when setting is updated and once when the setting is loaded for the first time.
	              Can be nil. Takes two arguments: The name and value of the setting.
	defaultVal    If no value can be loaded, this value is saved.
	description   The tool-tip text for the setting.
]]

-- This setting will return the component's getValue()
function Group.addCustom(self, name, updateFunc, defaultVal, description, component)
	Settings.__settingsTable[self.groupName][name] = {
		value = nil,
		component = component,
		updateFunc = updateFunc,
		description = description or ""
	}

	self:set(name, self:get(name, defaultVal))

	return self
end

-- ValueList is a list of strings
function Group.addDropdown(self, name, updateFunc, defaultVal, description, valueList)
	return self:addCustom(name, updateFunc, defaultVal, description, nil)
end

function Group.addSlider(self, name, updateFunc, defaultVal, description, minVal, maxVal)
	return self:addCustom(name, updateFunc, defaultVal, description, nil)
end

-- For true, false values
function Group.addSwitch(self, name, updateFunc, defaultVal, description)
	return self:addCustom(name, updateFunc, defaultVal, description, nil)
end

-- For keyboard / mouse / joypad controls
function Group.addInput(self, name, updateFunc, defaultVal, description)
	return self:addCustom(name, updateFunc, defaultVal, description, nil)
end



-- Saves the current settings to a file. Call this after you've changed things!
function Settings.save()
	local str = ""
	local valueTable = Misc.vox_copy_table(vox_settings_table)

	-- Some clean-up from the way scripts are defined
	valueTable._G = nil
	valueTable._M = nil

	for groupName, settings in pairs(Settings.__settingsTable) do
		valueTable[groupName] = valueTable[groupName] or {}

		for name, obj in pairs(settings) do
			valueTable[groupName][name] = obj.value
		end
	end

	for groupName, group in pairs(valueTable) do
		if group ~= nil and type(group) == "table" then
			str = str .. "_G[" .. string.format("%q", groupName) .. "] = "
				.. Serialize.tostring(group) .. "\n"
		end
	end

	set_save(str)
end

-- So we can save from C++
vox_global_table._G.vox_save_settings = Settings.save


return Settings