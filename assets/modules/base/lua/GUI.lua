import "math.Matrix"
import "math.Quaternion"
import "const.KEY"
import "const.MOUSE"
importAll "util.Misc"

-- Define _indexTabs first tho
function vox_multInherit(t, k)
	local v = vox_search(k, t._indexTabs)
	t[k] = v       -- save for next access
	return v
end

-- Global functions
function addGlobalListener(listener)
	local address = vox_make_index_in_table("vox_global_event_listener_array", listener)
	global_addListener(address)
end

-- GUI Table
GUI = {
	-- GUI Components
	Component = {},
	Window = {},

	-- Utilities
	BoundingRectangle = {},
	Rectangle = {},

	-- GUI Skins
	skins = {
		window = {},
		buttonPressed = {},
		buttonReleased = {},
		horizontalSlider = {},
	},

	-- The Offset Enum (kinda)
	BOTTOM = 0,
	TOP = 1,
	RIGHT = 1,
	LEFT = 0,
	CENTER = 0.5,

	NO_FRAME = {},
}

-- Utilities
function BorderWeight(...)
	local n = select("#", ...)
	
	local obj = {}

	if n == 1 then
		local amnt = select(1, ...)
		obj.top = amnt
		obj.right = amnt
		obj.bottom = amnt
		obj.left = amnt
	elseif n == 2 then
		local topBottom = select(1, ...)
		local rightLeft = select(2, ...)
		obj.top = topBottom
		obj.right = rightLeft
		obj.bottom = topBottom
		obj.left = rightLeft
	elseif n == 4 then
		obj.top = select(1, ...)
		obj.right = select(2, ...)
		obj.bottom = select(3, ...)
		obj.left = select(4, ...)
	end

	return obj
end

function CornerWeight(...)
	local n = select("#", ...)

	local obj = {}

	if n == 1 then
		local amnt = select(1, ...)
		obj.topLeft = amnt
		obj.topRight = amnt
		obj.bottomRight = amnt
		obj.bottomLeft = amnt
	elseif n == 2 then
		local top = select(1, ...)
		local bottom = select(2, ...)
		obj.topLeft = top
		obj.topRight = top
		obj.bottomRight = bottom
		obj.bottomLeft = bottom
	elseif n == 4 then
		obj.topLeft = select(1, ...)
		obj.topRight = select(2, ...)
		obj.bottomRight = select(3, ...)
		obj.bottomLeft = select(4, ...)
	end

	return obj
end

function AtlasEntry(pos, size)
	local obj = {}
	obj.size = size
	obj.pos = pos
	return obj
end

function FrameAtlas(texName, topLeft, top, topRight, right, bottomRight, bottom, bottomLeft, left)
	local obj = {}
	obj.texName = texName
	obj.topLeft = topLeft
	obj.top = top
	obj.topRight = topRight
	obj.right = right
	obj.bottomRight = bottomRight
	obj.bottom = bottom
	obj.bottomLeft = bottomLeft
	obj.left = left
	return obj
end

function StandardFrameAtlas(texName, pos, size)
	return FrameAtlas(
		texName,
		AtlasEntry(pos + vec2(0, size.y * 2), size), AtlasEntry(pos + vec2(size.x, size.y * 2), size), AtlasEntry(pos + vec2(size.x * 2, size.y * 2), size), AtlasEntry(pos + vec2(size.x * 2, size.y), size),
		AtlasEntry(pos + vec2(size.x * 2, 0), size), AtlasEntry(pos + vec2(size.x, 0),          size), AtlasEntry(pos,                               size), AtlasEntry(pos + vec2(0, size.y),          size)
	)
end

function FrameFactory(...)
	local n = select("#", ...)

	if n == 1 then
		local atlas = select(1, ...)
		return FrameFactory(atlas, BorderWeight(atlas.top.size.y, atlas.right.size.x, atlas.bottom.size.y, atlas.left.size.x))
	elseif n == 2 then
		local atlas = select(1, ...)
		local borders = select(2, ...)
		return FrameFactory(atlas, borders, CornerWeight(vec2(borders.left, borders.top), vec2(borders.right, borders.top), vec2(borders.right, borders.bottom), vec2(borders.left, borders.bottom)))
	elseif n == 3 then
		local obj = {}
		obj.atlas = select(1, ...)
		obj.borders = select(2, ...)
		obj.corners = select(3, ...)
		return obj
	end
end

GUI.NO_FRAME = FrameFactory(StandardFrameAtlas("testframe", vec2(0,0), vec2(0,0)), BorderWeight(0))
GUI.NO_SKIN = {
	frameFactory = GUI.NO_FRAME,
	backTex = makeQuadFromAtlas("testframe", AtlasEntry(vec2(0, 0), vec2(0, 0)), vec2(0, 0), vec2(1, 1))
}

function Rectangle(...)
	return GUI.Rectangle.new(...)
end

GUI.Rectangle.new = function(...)
	local n = select("#", ...)
	
	if n == 2 then
		local pos = select(1, ...)
		local size = select(2, ...)
		return Rectangle(pos.x, pos.y, size.x, size.y)
	elseif n == 4 then
		local obj = {}
		obj.x = select(1,...)
		obj.y = select(2,...)
		obj.w = select(3,...)
		obj.h = select(4,...)
		setmetatable(obj, {__index = GUI.Rectangle})
		return obj
	end
end

GUI.Rectangle.intersects = function(t, other)
	return
		not(t.x + t.w <= other.x	   or
			t.x >= other.x + other.w   or
			t.y + t.h <= other.y 	   or
			t.y >= other.y + other.h)
end

GUI.Rectangle.contains = function(t, p)
	return not(p.x >= t.x + t.w 	or
			   p.x <= t.x 			or
			   p.y >= t.y + t.h		or
			   p.y <= t.y)
end

GUI.Rectangle.getPos = function(self)
	return vec2(self.x, self.y)
end

GUI.Rectangle.getSize = function(self)
	return vec2(self.w, self.h)
end

function BoundingRectangle(...)
	return GUI.BoundingRectangle.new(...)
end

function RelativeCoord(x, y, rel_x, rel_y, offs_x, offs_y)
	local obj = {}
	obj.x = x
	obj.y = y
	obj.rel_x = rel_x
	obj.rel_y = rel_y
	obj.offs_x = offs_x or 0
	obj.offs_y = offs_y or 0
	return obj
end

-- All of these are vec2's
GUI.BoundingRectangle.new = function(anchor, offs, size)
	local obj = {}
	obj.anchor = anchor
	obj.offs = offs
	obj.size = size

	setmetatable(obj, {__index = GUI.BoundingRectangle})

	return obj
end

local function rel_sub(rel, sub_true, sub_false)
	local x = (not rel.columns and rel.rel_x) and (sub_true.x + rel.offs_x) or sub_false.x
	local y = (not rel.columns and rel.rel_y) and (sub_true.y + rel.offs_y) or sub_false.y
	local a = vec2(x, y)
	return a;
end

GUI.BoundingRectangle.makeRectangle = function(self, pos, size)
	local flooredAnchor = vec2(math.floor(self.anchor.x), math.floor(self.anchor.y))
	local s1 = vec2(self.size.x, self.size.y) * size;
	local s = rel_sub(self.size, s1, self.size)
	local p = vec2(pos.x, pos.y) + vec2(self.anchor.x, self.anchor.y) * (size - s) + ((flooredAnchor * -2) + 1) * vec2(self.offs.x, self.offs.y) * rel_sub(self.offs, s, vec2(1, 1))
	return Rectangle(p, s)
end

function RGBA_B(r, g, b, a) return RGBA_F(r / 255, g / 255, b / 255, a / 255) end
function RGBA_F(r, g, b, a) return vec4(r, g, b, a) end
function RGB_B(r, g, b) return RGB_F(r / 255, g / 255, b / 255) end
function RGB_F(r, g, b) return vec4(r, g, b, 1) end



-- The default frame skin
GUI.skins.window.backTex = makeQuadFromAtlas("testframe", AtlasEntry(vec2(118, 97), vec2(1, 1)), vec2(8, 8), vec2(1, 1))
GUI.skins.window.titleBarRectangle = BoundingRectangle(vec2(GUI.LEFT, GUI.TOP), vec2(0, 0), RelativeCoord(1, 27, true, false))
GUI.skins.window.frameFactory = FrameFactory(
	FrameAtlas("testframe",
		AtlasEntry(vec2(109,99),  vec2(8,29)), AtlasEntry(vec2(118,99), vec2(1,29)), AtlasEntry(vec2(120,99), vec2(8,29)), AtlasEntry(vec2(120,97), vec2(8,1)),
		AtlasEntry(vec2(120,88),  vec2(8, 8)), AtlasEntry(vec2(118,88), vec2(1, 8)), AtlasEntry(vec2(109,88), vec2(8, 8)), AtlasEntry(vec2(109,97), vec2(8,1))
	),
	BorderWeight(29, 8, 8, 8),
	CornerWeight(vec2(8, 29), vec2(8, 8))
)

-- The button skin
GUI.skins.buttonPressed.backTex = makeQuadFromAtlas("testframe", AtlasEntry(vec2(3, 12), vec2(3, 3)), vec2(8, 8), vec2(1, 1))
GUI.skins.buttonPressed.frameFactory = FrameFactory(StandardFrameAtlas("testframe", vec2(0, 9), vec2(3, 3)), BorderWeight(3))
GUI.skins.buttonReleased.backTex = makeQuadFromAtlas("testframe", AtlasEntry(vec2(3, 3), vec2(3, 3)), vec2(8, 8), vec2(1, 1))
GUI.skins.buttonReleased.frameFactory = FrameFactory(StandardFrameAtlas("testframe", vec2(0, 0), vec2(3, 3)), BorderWeight(3))

local _globList = false

-- Inner Component Functions
function Component(...)
	return GUI.Component.new(...)
end

local _grabbedPos = vec2(0, 0)
local _grabbedInitPos = vec2(0, 0)
local _grabbedWindow = {}
local _grabbed = false

local function makeGlobalListener()
	return {
		mouseMoved = function(event)
			if _grabbed then
				_grabbedWindow:setPos(vec2(event.absolutePos.x - _grabbedPos.x + _grabbedInitPos.x, event.absolutePos.y - _grabbedPos.y + _grabbedInitPos.y))
			end
		end,
		mouseReleased = function(event)
			_grabbed = false
		end
	}
end

GUI.Component.new = function(size, skin)
	local obj = {}

	obj.size = size
	obj.visible = true
	obj.skin = skin
	obj.textBounds = BoundingRectangle(vec2(GUI.CENTER, GUI.CENTER), vec2(0, 0), vec2(0, 0))
	obj.hue = RGB_F(1, 1, 1)
	obj.pos = vec2(0, 0)
	obj.text = ""

	obj._address = vox_make_index_in_table("vox_component_address_array", obj)
	obj._id = gui_createComponent(obj._address, size)
	obj._children = {}
	obj._parent = -2

	setmetatable(obj, {
		__index = GUI.Component
	})

	obj:setInteractable(true)

	-- Static init
	if not _globList then
		_globList = makeGlobalListener()
		addGlobalListener(_globList)
	end

	return obj;
end

GUI.Component.setInteractable = function(self, b)
	self.interactable = b
	gui_setInteractable(b)
end

GUI.Component.addListener = function(self, listener)
	local address = vox_make_index_in_table("_eventListeners", listener, self)
	gui_addListener(self._id, self._address .. "." .. address)
end

GUI.Component.add = function(self, child, pos)
	if child._parent == self then return end

	gui_setParent(child._id, self._id);
	child._parent = self

	if not pos.columns and pos.anchor then
		child:setPos(pos:makeRectangle(vec2(0, 0), self.size):getPos())
		child:setSize(pos:makeRectangle(vec2(0, 0), self.size):getSize())
	else
		child:setPos(vec2(pos.x, pos.y))
	end

	self._children[#self._children+1] = {
		bb = vox_copy_table(pos),
		comp = child
	}
end

GUI.Component.setVisible = function(self, b)
	gui_setVisible(self._id, b)
	self.visible = b
end

GUI.Component.setSize = function(self, size)
	self.size = size
	gui_setSize(self._id, size)
	self:setText(self.text)

	for i = 1, #self._children do
		if not self._children[i].bb.columns and self._children[i].bb.anchor and self._children[i].comp._parent == self then
			local rect = self._children[i].bb:makeRectangle(vec2(0, 0), self.size)
			self._children[i].comp:setPos(rect:getPos())
			self._children[i].comp:setSize(rect:getSize())
		end
	end
end

GUI.Component.setSkin = function(self, skin)
	self.skin = skin
	gui_updateSkin(self._id)
end

GUI.Component.setPos = function(self, pos)
	self.pos = pos
	gui_setPos(self._id, pos)
end

GUI.Component.getRectangle = function(self)
	return Rectangle(self.pos, self.size)
end

GUI.Component.getRelativeRectangle = function(self)
	return Rectangle(vec2(0, 0), self.size)
end

GUI.Component.setHue = function(self, hue)
	self.hue = hue
	gui_setHue(self._id, hue)
end

GUI.Component.setText = function(self, text)
	gui_setText(self._id, text)
	local s = gui_getTextSize(self._id)
	self.textBounds.size = vec2(s.x, s.y)
	self.text = text
	gui_setTextPos(self._id, self.textBounds:makeRectangle(vec2(0, 0), self.size):getPos())
end

local function callActivatedEvent(obj)
	if obj.activated then
		obj.activated()
	end
end

GUI.Component.activated = function(self)
	vox_loop_through_index_table("_eventListeners", callActivatedEvent, self)
end

-- Window Component
GUI.skins.windowXPressed = vox_copy_table(GUI.skins.buttonPressed);
GUI.skins.windowXPressed.backTex = makeQuadFromAtlas("testframe", AtlasEntry(vec2(0, 28), vec2(18, 9)), vec2(18, 9), vec2(0, 0))
GUI.skins.windowXReleased = vox_copy_table(GUI.skins.buttonReleased);
GUI.skins.windowXReleased.backTex = GUI.skins.windowXPressed.backTex


function Window(...)
	return GUI.Window.new(...)
end

GUI.Window.new = function(title, size, skin)
	local obj = Component(size, skin or GUI.skins.window)

	local titleLabel = Component(vec2(0, 0), GUI.NO_SKIN)
	titleLabel.textBounds = BoundingRectangle(vec2(GUI.LEFT, GUI.CENTER), vec2(12, 0), RelativeCoord(1, 1, true, true))
	titleLabel:setText(title)
	obj:add(titleLabel, obj.skin.titleBarRectangle)

	obj.title = title
	obj.pos = vec2(20, 20)

	obj._indexTabs = {GUI.Window, GUI.Component}
	setmetatable(obj, {
		__index = vox_multInherit,
	})

	gui_makeWindow(obj._id)
	obj:setPos(obj.pos)

	titleLabel:addListener({
		mousePressed = function(event)
			_grabbed = true
			_grabbedPos = event.absolutePos
			_grabbedInitPos = obj.pos
			_grabbedWindow = obj
		end
	})

	local closeButton = Button("", vec2(24, 15), GUI.skins.windowXPressed, GUI.skins.windowXReleased)
	closeButton:addListener({
		activated = function(event)
			obj:setVisible(false)
		end
	})
	obj:add(closeButton, BoundingRectangle(vec2(GUI.RIGHT, GUI.TOP), vec2(0, 0), closeButton.size))
	closeButton:setHue(RGB_F(1, 0, 0))

	return obj;
end

GUI.Window.bringToFront = function(self)
	gui_bringToFront(self._id)
end

-- Button Component
function Button(text, size, buttonPressed, buttonReleased)
	local obj = Component(size, buttonReleased or GUI.skins.buttonReleased)
	obj.buttonReleasedSkin = buttonReleased or GUI.skins.buttonReleased
	obj.buttonPressedSkin = buttonPressed or GUI.skins.buttonPressed

	obj.text = text

	obj:addListener({
		mousePressed = function(event)
			obj:setSkin(obj.buttonPressedSkin)
		end,
		mouseReleased = function(event)
			obj:setSkin(obj.buttonReleasedSkin)
			if obj:getRelativeRectangle():contains(event.relativePos) then
				obj:activated()
			end
		end,
		mouseEntered = function(event)
			obj:setHue(obj.hue * RGB_F(0.75, 0.75, 1))
		end,
		mouseExited = function(event)
			obj:setHue(obj.hue * RGB_F(1/0.75, 1/0.75, 1))
		end
	})
	obj:setText(text)

	return obj
end


-- The slider skin
GUI.skins.horizontalSlider.backTex = makeQuadFromAtlas("testframe", AtlasEntry(vec2(2, 21), vec2(2, 2)), vec2(2, 2), vec2(1, 1))
GUI.skins.horizontalSlider.frameFactory = FrameFactory(StandardFrameAtlas("testframe", vec2(0, 19), vec2(2, 2)), BorderWeight(2))
GUI.HorizontalSlider = {}
function HorizontalSlider(length, minVal, maxVal, skin)
	local obj = Component(vec2(length, 10), skin or GUI.skins.horizontalSlider)
	local sliderComp = Button("", vec2(12, 20))
	obj._filledComp = Component(vec2(0, 10), skin or GUI.skins.horizontalSlider)
	obj._filledComp:setHue(RGB_F(0.25, 0.5, 1))
	sliderComp.textBounds = BoundingRectangle(vec2(GUI.CENTER, GUI.TOP), vec2(0, sliderComp.size.y+2), vec2(0, 0))

	local grabbed = false
	local grabbedX = 0
	local initX = 0

	local offs = vec2(-sliderComp.size.x / 2, -sliderComp.size.y / 2 + obj.size.y / 2)
	obj._offs = offs
	obj.minVal = minVal
	obj.maxVal = maxVal

	obj._indexTabs = {GUI.HorizontalSlider, GUI.Component}
	setmetatable(obj, {
		__index = vox_multInherit,
	})

	sliderComp:addListener({
		mousePressed = function(event)
			grabbed = true
			grabbedX = event.absolutePos.x
			initX = sliderComp.pos.x
		end,
		mouseMoved = function(event)
			if grabbed then
				local newPos = sliderComp.pos
				newPos.x = initX + event.absolutePos.x - grabbedX
				obj:setValue(((newPos.x - offs.x) / obj.size.x) * (maxVal - minVal) + minVal)
			end
		end,
		mouseReleased = function(event)
			grabbed = false
		end
	})

	obj._sliderComp = sliderComp

	obj:add(obj._filledComp, vec2(0, 0))
	obj:add(sliderComp, vec2(offs))
	return obj
end

GUI.HorizontalSlider.setValue = function(self, value)
	local p = (value - self.minVal) / (self.maxVal - self.minVal)
	if p < 0 then p = 0
	elseif p > 1 then p = 1 end

	local newPos = vec2(self.size.x * p + self._offs.x, self._offs.y)
	self._sliderComp:setPos(newPos)
	self._filledComp:setSize(vec2(newPos.x - self._offs.x, self._filledComp.size.y))

	local lastValue = self.value
	self.value = p * (self.maxVal - self.minVal) + self.minVal
	if lastValue ~= self.value then self:activated() end

	local rounded = vox_round_places(self.value, 3)
	self._sliderComp:setText(vox_round_places(self.value, 3) .. "")
end

-- Scroll Bar
GUI.skins.scrollBar = {}
GUI.skins.scrollBar.backTex = makeQuadFromAtlas("testframe", AtlasEntry(vec2(0, 26), vec2(15, 1)), vec2(15, 1), vec2(0, 1))
GUI.skins.scrollBar.frameFactory = GUI.NO_FRAME

GUI.skins.scrollBarUpPressed = vox_copy_table(GUI.skins.buttonPressed);
GUI.skins.scrollBarUpPressed.backTex = makeQuadFromAtlas("testframe", AtlasEntry(vec2(10, 9), vec2(9, 9)), vec2(9, 9), vec2(0, 0))
GUI.skins.scrollBarUpReleased = vox_copy_table(GUI.skins.buttonReleased);
GUI.skins.scrollBarUpReleased.backTex = GUI.skins.scrollBarUpPressed.backTex

GUI.skins.scrollBarDownPressed = vox_copy_table(GUI.skins.buttonPressed);
GUI.skins.scrollBarDownPressed.backTex = makeQuadFromAtlas("testframe", AtlasEntry(vec2(20, 9), vec2(9, 9)), vec2(9, 9), vec2(0, 0))
GUI.skins.scrollBarDownReleased = vox_copy_table(GUI.skins.buttonReleased);
GUI.skins.scrollBarDownReleased.backTex = GUI.skins.scrollBarDownPressed.backTex

function ScrollBar(height, minValue, maxValue)
	local _s = 15;

	local max = height - _s * 2

	local obj = Component(vec2(_s, height), GUI.skins.scrollBar)
	local bar = Button("", vec2(_s - 2, max / (maxValue - minValue)))

	local upButton = Button("", vec2(_s, _s), GUI.skins.scrollBarUpPressed, GUI.skins.scrollBarUpReleased)
	local downButton = Button("", vec2(_s, _s), GUI.skins.scrollBarDownPressed, GUI.skins.scrollBarDownReleased)

	local grabbed = false
	local grabbedY

	local function setBarPos(pos)
		local newPos = vec2(pos.x, pos.y)
		if newPos.y < downButton.pos.y + downButton.size.y then newPos.y = downButton.pos.y + downButton.size.y
		elseif newPos.y > (upButton.pos.y - bar.size.y+1) then newPos.y = (upButton.pos.y - bar.size.y+1) end
		bar:setPos(newPos)
	end

	bar:addListener({
		mousePressed = function(event)
			grabbed = true
			grabbedY = event.relativePos.y
		end,
		mouseMoved = function(event)
			if grabbed then
				local newPos = bar.pos
				newPos.y = newPos.y + event.relativePos.y - grabbedY
				setBarPos(newPos)
				--local lastValue = obj.value
				--obj.value = (newPos.x / (height + offs.x)) * (maxVal - minVal) + minVal
				--if lastValue ~= obj.value then obj:activated() end
			end
		end,
		mouseReleased = function(event)
			grabbed = false
		end
	})

	upButton:addListener({
		activated = function(event)
			setBarPos(vec2(bar.pos.x, bar.pos.y + 5))
		end
	})
	downButton:addListener({
		activated = function(event)
			setBarPos(vec2(bar.pos.x, bar.pos.y - 5))
		end
	})

	obj:add(downButton, vec2(0, - _s + 5))
	obj:add(upButton, vec2(0, height - 5))
	obj:add(bar, vec2(1, downButton.pos.y + downButton.size.y))
	return obj;
end

GUI.skins.textBox = {}
GUI.skins.textBox.backTex = makeQuadFromAtlas("testframe", AtlasEntry(vec2(11, 2), vec2(2, 2)), vec2(2, 2), vec2(1, 1))
GUI.skins.textBox.frameFactory = FrameFactory(StandardFrameAtlas("testframe", vec2(9, 0), vec2(2, 2)), BorderWeight(2))

GUI.codeContext = {}

function TextBox(size)
	local obj = Component(size, GUI.skins.textBox)
	obj.textBounds = BoundingRectangle(vec2(GUI.LEFT, GUI.CENTER), vec2(12, 0), RelativeCoord(1, 1, true, true))
	obj:addListener({
		keyDown = function(event)
			if event.keyCode == KEY.BACKSPACE and string.len(obj.text) > 0 then
				obj:setText(string.sub(obj.text, 1, -2))
			end
		end,
		textTyped = function(event)
			obj:setText(obj.text .. event.text)
		end,
		focusGained = function(event)
			gui_setTextMode(true)
		end,
		focusLost = function(event)
			gui_setTextMode(false)
		end
	})
	return obj
end