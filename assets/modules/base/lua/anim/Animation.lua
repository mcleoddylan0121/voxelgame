Animation = {}

setmetatable(Animation, {
	__index = Animation,
	__call = function(self, frames, rep)
		return {
			frames = frames,
			rep = rep
		}
	end
})

return Animation