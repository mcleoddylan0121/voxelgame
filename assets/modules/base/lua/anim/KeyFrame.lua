KeyFrame = {}

setmetatable(KeyFrame, {
	__index = KeyFrame,
	__call = function(self, jointMotionList, time)
		return {
			jointMotionList = jointMotionList,
			time = time
		}
	end
})

return KeyFrame