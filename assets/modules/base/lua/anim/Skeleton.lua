importAll "util.Misc"
import "anim.KeyFrame"
import "anim.Animation"

Skeleton = {}

function parseJointMotionObject(arr, default)
	return {
		jointName = arr[1],
		posOnParent = arr[2] or default[2], 
		rotAnchor = arr[3] or default[3], 
		offset = arr[4] or default[4], 
		rotation = arr[5] or default[5],
	}
end

setmetatable(Skeleton, {
	__index = Skeleton,

	__call = function(self, jointList, animations)
		local obj = {}

		-- Load the joints
		obj.jointNames = {}  -- Corresponds 1:1 with jointMeshes and jointParents
		obj.jointMeshes = {}
		obj.jointParents = {}

		for i=1, #jointList do
			table.insert(obj.jointNames, jointList[i][1])
			table.insert(obj.jointMeshes, jointList[i][2])
			table.insert(obj.jointParents, jointList[i][3] or "_root")
		end

		-- Load the default joint orintation
		local base = {} -- Format will be jointName = JointMotionObject
		for i=1, #animations.base do
			base[animations.base[i][1]] = animations.base[i]
		end

		obj.animNames = {} -- Corresponds 1:1 with animData
		obj.animData = {}  -- An array of arrays of KeyFrames

		-- Load the animations
		for k, v in pairs(animations) do
			if k ~= "base" then
				table.insert(obj.animNames, k)
				local fs = v.frames
				local anim = Animation({}, v.rep)

				for i=1, #fs do -- For each KeyFrame
					-- Copy the base and modify this table to retain default
					-- We will change it back to an array after we're done
					local parseTable = vox_copy_table(base)
					local frame = fs[i]
					local newFrame = KeyFrame({}, frame.time)
					for j=1, #frame.jointMotionList do
						local jointMotionObject = frame.jointMotionList[j]
						parseTable[jointMotionObject[1]] = jointMotionObject
					end
					-- Now return parsTable to an array so we can fetch it in c++
					for jk, jv in pairs(parseTable) do
						local jointMotionObject = parseJointMotionObject(jv, base[jv[1]])
						table.insert(newFrame.jointMotionList, jointMotionObject)
					end

					table.insert(anim.frames, newFrame)
				end

				table.insert(obj.animData, anim)
			else
				-- Add a base animation
				local parseTable = vox_copy_table(base)
				newFrame = KeyFrame({}, 0.1)
				for jk, jv in pairs(parseTable) do
					parseTable[jk] = parseJointMotionObject(jv, jv)
				end
				-- If it looks redundant, it's because I'm lazy
				for jk, jv in pairs(parseTable) do
					table.insert(newFrame.jointMotionList, jv)
				end
				table.insert(obj.animNames, "base")
				table.insert(obj.animData, Animation({newFrame}, true))
			end
		end

		return obj
	end
})

return Skeleton