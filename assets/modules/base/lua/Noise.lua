
Noise_mt = {}

Noise_mt.__add = function(a,b)
	if getmetatable(a) ~= Noise_mt or getmetatable(b) ~= Noise_mt then
		error("attempt to `add' a noise with a non-noise value", 2)
	end
	return CombinedNoise(a,b)
end

Noise_mt.__mul = function(a,b)
	if getmetatable(a) ~= Noise_mt or getmetatable(b) ~= Noise_mt then
		error("attempt to multiply a noise with a non-noise value", 2)
	end
	return Multiplier(a,b)
end
	

function Noise(type)
	local obj = {}
	setmetatable(obj, Noise_mt)
	obj.type = type
	return obj
end

function EmptyNoise(value)
	local noise = Noise("empty")
	noise.value = value
	return noise
end

function CombinedNoise(...)
	local noise = Noise("combined_noise")
	noise.noises = {}
	local n = select("#", ...)
	for i = 1,n do
		noise.noises[i] = select(i, ...)
	end
	return noise
end

function DiscreteFit(steps, noise)
	local obj = Noise("discrete_fit")
	obj.steps = steps
	obj.noise = noise
	return obj
end

function Generator(type, range, scale)
	local obj = Noise(type)
	range = range or {0,1}
	if scale == nil then
		obj = Noise(type)
		obj.range = range
	else
		-- expert recursion
		local noise = Noise(type)
		noise.range = range
		obj = InputScalar(scale, noise)
	end
	return obj
end

function RidgedNoise(noise)
	local obj = Noise("ridged")
	obj.noise = noise
	return obj
end

function SimplexNoise(scale, amplitude)
    return Generator("simplex", {0,amplitude}, scale)  
end

function defaultNoiseFactory(octave, octaves, scale, amplitude)
	return Generator("simplex", {0, amplitude}, scale)
end

function ridgedNoiseFactory(octave, octaves, scale, amplitude)
	return RidgedNoise(Generator("simplex", {0, amplitude}, scale))
end

function worleyNoiseFactory(octave, octaves, scale, amplitude)
	return Generator("worley", {0, amplitude}, scale)
end

function OctaveNoise(octaves, factory, range, persistence, largestfeature)
	local obj = Noise("combined_noise")
	obj.noises = {}
	local min = range[1]
	local max = range[2]
	local cMax = 0
	local diff = max - min
	octaves = octaves or 10
	factory = factory or defaultNoiseFactory
	persistence = persistence or 0.5
	largestfeature = largestfeature or 1.0
	
	for octave = 1,octaves do
		local amp = math.pow(persistence,octave)
		cMax = cMax + amp
	end
	
	for octave = 1,octaves do
		local amplitude = (math.pow(persistence,octave) / cMax) * diff * ( 0.96 + math.random() * 0.8 )
		local scale = largestfeature / math.pow(2,octave-1)
		obj.noises[octave] = factory(octave, octaves, scale, amplitude)
	end
	
	if min ~= 0 then
		obj.noises[#obj.noises+1] = EmptyNoise(min)
	end
	
	return obj
end

function Selector(first, second, select)
	local noise = Noise("selector")
	noise.first = first
	noise.second = second
	noise.selector = select
	return noise
end

function FitCurve(curve, noise)
	local obj = Noise("fitcurve")
	obj.curve = curve
	obj.noise = noise
	return obj
end

function Constrainer(range, noise)
	local obj = Noise("constrainer")
	obj.range = range
	obj.noise = noise
	return obj
end

function InputScalar(scale, noise)
	local obj = Noise("inputscalar")
	obj.scale = scale
	obj.noise = noise
	return obj
end

function Multiplier(first, second)
	local obj = Noise("multiply")
	obj.first = first
	obj.second = second
	return obj
end

function CloudCurve(density, sharpness, noise)
	local obj = Noise("cloudcurve")
	obj.density = density
	obj.sharpness = sharpness
	obj.noise = noise
	return obj
end

function SharedNoise(name, noise)
	local obj = Noise("sharednoise")
	obj.name = name
	obj.noise = noise
	return obj
end

function OutputShifter(range, noise)
	local obj = Noise("outputshifter")
	obj.range = range
	obj.noise = noise
	return obj
end

function InputWarper(noise, warp)
    local obj = Noise("inputwarper")
    obj.noise = noise
    obj.warp = warp
    return obj
end

function DimensionalNoise(dim, range)
    local obj = Noise("dimensional")
    obj.dim = dim
    obj.range = range
    return obj
end


