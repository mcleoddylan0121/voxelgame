-- These create primitive variables in the global scope and thus must be defined first
run "math.Matrix"
run "math.Quaternion"

-- These settings are required for the game to even run lol
import "util.Settings"
import "const.KEY"
import "const.MOUSE"

Settings.group("Control")
	:addSlider("Mouse Sensitivity", set_number, 0.5, "Fine-tune the feelings of your mouse.", 0.1, 1)
	:addSlider("Place Radius", set_number, 1, "Why is this even here?", 1, 20)

Settings.group("Video")
	:addSwitch("Fullscreen", set_bool, false, "For those who value immersion.")
	:addSwitch("VSync", set_bool, false, "Locks the framerate to your monitor's refresh rate.")
	:addCustom("Resolution", set_res, {x=1440,y=900}, "May result in higher quality pixels.", nil)

Settings.group("Graphics")
	:addSwitch("Cubemap Fix", set_bool, true, "Not even we know why this needs to be enabled.")
	:addSwitch("Bloom", set_bool, true, "Makes your game look like an AAA title without all of the FPS drops.")
	:addDropdown("Anti-Aliasing", set_number, 1, "Smooths out those jaggies.", {1, 2, 4, 8, 16})
	:addSwitch("Downsample Clouds", set_bool, true, "Increases performance with no downside. Keep this on.")
	:addSwitch("Enable VR", set_bool, false, "It's all the rage these days.")

Settings.group("Debug Output")
	:addSwitch("Lua", set_bool, false, "For debugging C++ side Lua Errors.")
	:addSwitch("OpenGL", set_bool, false, "For debugging OpenGL Errors.")

Settings.group("Input")
	:addInput("Move Forward", set_key, KEY.W, "Toward the enemy.")
	:addInput("Move Backward", set_key, KEY.S, "For cowards.")
	:addInput("Move Left", set_key, KEY.A, "Port.")
	:addInput("Move Right", set_key, KEY.D, "Starboard.")
	:addInput("Jump", set_key, KEY.SPACE, "Witty Mario reference.")
	:addInput("Cycle Block", set_key, KEY.C, "Swaps which block you're placing.")
	:addInput("Place Block", set_key, KEY.E, "Places a block where you're looking.")
	:addInput("Remove Block", set_key, KEY.Q, "Removes the block you're looking at.")
	:addInput("Primary Fire", set_key, MOUSE.LEFT, "Always shoots flames.")
	:addInput("Secondary Fire", set_key, MOUSE.RIGHT, "Doesn't always shoot flames.")
	:addInput("Take Screenshot", set_key, KEY.F1, "Saves a screenshot to the screenshots folder.")
	:addInput("FOV Modifier", set_key, KEY.LSHIFT, "Hold and scroll to modify your FOV. Why isn't this a setting?")