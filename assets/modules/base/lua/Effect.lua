importAll "GUI"
import "Entity"

Effect = {}

effect_mt = { __index = Effect }

-- Constructor function
effect_construct_mt = {
	__call = function(this, duration, lingeringDuration)
		local obj = {}
		obj.duration = duration
		obj.lingeringDuration = lingeringDuration or 0

		obj._id = eff_create()
		eff_setDurations(obj._id, obj.duration, obj.lingeringDuration)

		setmetatable(obj, effect_mt)

		return obj
	end
}

setmetatable(Effect, effect_construct_mt)

function Effect.add(this, ec)
	if ec.type == "light" then
		eff_addLightComp(this._id, ec.color or RGB_F(1, 1, 1), ec.intensity or 0.5, ec.radius or 1, ec.compensation or -1)
	elseif ec.type == "sprite" then
		eff_addSpriteComp(this._id,
			ec.texName,
			ec.texOffs or vec2(0, 0),
			ec.texSize or vec2(-1, -1),
			ec.rot or 0,
			ec.scale or vec2(1, 1),
			ec.color or RGB_F(1, 1, 1)
			)
	elseif ec.type == "animatedSprite" then

	elseif ec.type == "particleEmitter" then
		local temp_effect = Effect(1);
		for i=1, #ec.particle do 
			temp_effect:add(ec.particle[i])
		end
		
		-- This function deletes temp_effect
		eff_addParticleEmitterComp( -- NOTE, THIS USES 8/20 PARAMATERS, BE CAREFUL WITH THE STACK SIZE
			this._id, temp_effect._id,
			ec.scale or vec4(1, 1, 1, 1), ec.particleCD or vec2(1, 1), ec.particleLife or vec2(1, 1),
			ec.rotate or false, ec.theta or vec2(0, math.pi * 2), ec.phi or vec2(0, math.pi),
			ec.rho or vec2(1, 1), ec.acceleration or Entity.GRAVITY
			)
	end
end


function Effect.start(this)
	eff_startStop(this._id, true)
end

function Effect.stop(this)
	eff_startStop(this._id, false)
end

function Effect.remove(this)
	eff_remove(this._id)
end


function Effect.getTimeLeft(this)
	return eff_getTimeLeft(this._id)
end

function Effect.setTimeLeft(this, newTime)
	eff_setTimeLeft(this._id, newTime)
end


function Effect.getPos(this)
	return vox_tovec3(eff_getPos(this._id))
end

function Effect.setPos(this, newPos)
	eff_setPos(this._id, newPos)
end

return Effect