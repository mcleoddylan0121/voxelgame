importAll "util.Misc"

Entity = {

	registeredEntities = {},
	playerEntityPointer = {},
	loadedEntities = {},

	GRAVITY = vec3(0, -20, 0),
}

function Entity.register(name_string, constructor_function)
	Entity.registeredEntities[name_string] = constructor_function
end

function Entity.spawn(name_string, pos_vec3, ...)
	local ent = Entity(pos_vec3)
	ent:setPos(pos_vec3)
	ent:makeType(name_string, ...)
	return ent
end

function Entity.spawnHere(name, ...)
	return Entity.spawn(name, Entity.getPlayer():getPos() + vec3(0, 2, 0), ...)
end

function Entity.setPlayer(player_Entity)
	Entity.playerEntityPointer[0] = player_Entity
	ent_setPlayer(player_Entity._id)
end

function Entity.getPlayer()
	-- TODO: base case for value = nil
	return Entity.playerEntityPointer[0]
end

function Entity.getEntity(id)
	return Entity.loadedEntities[id]
end

function vox_entity_pathify(func, id, delta)
	local obj = Entity.getEntity(id)
	return obj[func](obj, delta)
end

-- ENTITIES --

-- Stats
local stats_mt = {
	__newindex = function(this, key, value)
		if type(value) == "boolean" then
			ent_setStat(this._id, key, value and 1 or 0)
		elseif type(value) == "number" then
			ent_setStat(this._id, key, value)
		elseif value == nil then
			ent_setStat(this._id, key, 0)
		end

		rawset(this._store, key, value)

		return this
	end,

	__index = function(this, key)
		return rawget(this._store, key)
	end,
}

-- Constructor
setmetatable(Entity, {__call = 
function(this)
	local obj = {};

	setmetatable(obj, {__index = Entity});

	-- Register the entity
	obj._address = vox_make_index_in_table("vox_entity_address_array", obj)
	obj._id = ent_createEntity(obj._address)
	
	-- Set up the stats object
	obj.stats = {_id = obj._id, _store = {}}
	obj.stats.dump = function(this)
		-- Dump out all current stats and their values
		print("Dumping stats for Entity (id = " .. this._id .. ")")
		for k,v in pairs(this._store) do
			print(k, v)
		end
	end
	setmetatable(obj.stats, stats_mt)

	obj:setPos(vec3(0, 0, 0))
	obj:setStats({
		-- Utility --
		moveSpeed = 16,
		jumpHeight = 3,

		-- Offensive --
		attackPower = 0,
		spellPower = 0,

		-- Defensive --
		physicalDefense = 0,
		magicDefense = 0,

		-- Health --
		healthMax = 1,
		healthRegen = 1,

		-- Mana --
		manaMax = 1,
		manaRegen = 1,
	});

	obj:setNoClip(false)
	obj:setSolid(false)
	obj:setUntouchable(false)

	obj.eventListeners = {}

	Entity.loadedEntities[obj._id] = obj

	return obj;
end})

function Entity.setNoClip(obj, bool)
	obj.noClip = bool
	ent_setNoClip(obj._id, bool)

	if obj.noClip then
		obj:setAccleration(vec3(0, 0, 0))
	else
		obj:setAccleration(Entity.GRAVITY)
	end
end

function Entity.setSolid(obj, bool)
	obj.solid = bool
	ent_setSolid(obj._id, bool)
end

function Entity.setUntouchable(this, bool)
	this.untouchable = bool
	ent_setUntouchable(this._id, bool)
end

function Entity.stop(obj)
	obj:setVel(vec3(0, 0, 0))
end

function Entity.kill(this)
	--TODO make this delayed till the start of the next tick
	Entity.loadedEntities[this._id] = nil
	ent_remove(this._id)
end

function Entity.setStats(this, newStats)
	for k,v in pairs(newStats) do
		this:setStat(k, v)
	end
end

function Entity.setStat(this, name_string, value_float)
	this.stats[name_string] = value_float
end

function Entity._pathify(func, id, delta)

end

Entity.setPos = function(this, pos)
	if type(pos) == "function" then
		this._posPath = pos
		ent_setPosPath(this._id, vec3(), true)
	else
		this._posPath = nil
		ent_setPos(this._id, pos, false)
	end
end
Entity.setPosition = Entity.setPos

Entity.setVel = function(this, vel)
	if type(vel) == "function" then
		this._velPath = vel
		ent_setVel(this._id, vec3(), true)
	else
		this._velPath = nil
		ent_setVel(this._id, vel, false)
	end
end
Entity.setVelocity = Entity.setVel

Entity.setAcc = function(this, acc)
	if type(acc) == "function" then
		this._accPath = acc
		ent_setAcc(this._id, vec3(), true)
	else
		this._accPath = nil
		ent_setAcc(this._id, acc, false)
	end
end
Entity.setAccleration = Entity.setAcc

function Entity.setMesh(this, meshName, voxelSize)
	ent_setMesh(this._id, meshName, voxelSize or 1/16)
	local a = ent_getSize(this._id)
	this.size = vec3(a.x, a.y, a.z)
end

function Entity.setSkeleton(this, s, voxelSize)
	ent_setSkeleton(this._id, s, voxelSize or 1/16)
	local a = ent_getSize(this._id)
	this.size = vec3(a.x, a.y, a.z)
end

function Entity.setAnimation(this, anim)
	ent_setAnimation(this._id, anim, false)
end

function Entity.queueAnimation(this, anim)
	ent_setAnimation(this._id, anim, true)
end

function Entity.setTerrainVolume(this, size_v3)
	ent_setTerrainVolume(this._id, vec3(0, 0, 0), size_v3)
end

Entity.getPos = function(this)
	local a = ent_getPos(this._id)
	return vec3(a.x, a.y, a.z)
end
Entity.getPosition = Entity.getPos

Entity.getVel = function(this)
	local a = ent_getVel(this._id)
	return vec3(a.x, a.y, a.z)
end
Entity.getVelocity = Entity.getVel

Entity.getAcc = function(this)
	local a = ent_getAcc(this._id)
	return vec3(a.x, a.y, a.z)
end
Entity.getAccleration = Entity.getAcc

function Entity.getSize(this)
	return this.size
end

function Entity.makeType(entity, name_string, ...)
	Entity.registeredEntities[name_string](entity, ...)
end

function Entity.dispatchEvent(this, eventName, event)
	if not this.eventListeners[eventName] then return end
	
	local temp = {} -- Store the crap in here so you can remove it while calling it
	for i = 1, #this.eventListeners[eventName] do table.insert(temp, this.eventListeners[eventName][i]) end
	for i = 1, #temp do
		local e = vox_copy_table(event or {})
		e.this = this
		e.listener = temp[i]
		temp[i].onCall(e)
	end
end

local function _addEventListener3(this, eventName, onCall)
	if not this.eventListeners[eventName] then this.eventListeners[eventName] = {} end
	local l = {}
	l.onCall = onCall
	l.eventName = eventName;
	table.insert(this.eventListeners[eventName], l)
end

local function _addEventListener2(this, listener)
	if not this.eventListeners[listener.eventName] then this.eventListeners[listener.eventName] = {} end
	table.insert(this.eventListeners[listener.eventName], listener)
end

function Entity.addEventListener(...)
	local n = select("#", ...)
	if n == 2 then _addEventListener2(...)
	elseif n == 3 then _addEventListener3(...) end
end

function Entity.removeEventListener(this, listener)
	if not this.eventListeners[listener.eventName] then return end
	for i = 1, #this.eventListeners[listener.eventName] do
		if this.eventListeners[listener.eventName][i] == listener then
			table.remove(this.eventListeners[listener.eventName], i)
			return
		end
	end
end

-- Converts an address from c++ into the lua object corresponding to that address
function vox_convert_reference(address)
	return loadstring("return " .. address)()
end

function Entity._collide(thisAddress, otherEntityAddress)
	local o = vox_convert_reference(thisAddress)
	local this = vox_convert_reference(otherEntityAddress)
	this:dispatchEvent("collide", {other=o})
end

local debugRender = false
function toggleDebug()
	renderDebug(not debugRender)
end
function renderDebug(b)
	debugRender = b
	vox_renderDebug(b)
end

return Entity