local quat = {}
local quat_mt = {}

setmetatable(quat, {__call = function(self, ...) return quat.new(...) end})


-- Constructors:
-- quat(), blank quaternion quat(1, 0, 0, 0)
-- quat(x, y, z), quat.fromEulerAngles(x, y, z)
-- quat(w, x, y, z), sets each component
function quat.new(...)
    local n = select("#", ...)

    if n == 3 then
        return quat.fromEulerAngles(vec3(select(1, ...), select(2, ...), select(3, ...)))
    elseif n == 0 then
        return quat(1, 0, 0, 0)
    else
        local obj = {}
        obj.w = select(1, ...) or 0
        obj.x = select(2, ...) or 0
        obj.y = select(3, ...) or 0
        obj.z = select(4, ...) or 0
        setmetatable(obj, quat_mt)
        return obj
    end
end

-- rotates a vector
-- adapted from wikipedia pseudo-code http://en.wikipedia.org/wiki/quat_rotation
function quat_mt.__mul(self, a)
    if (type(a) == "number") then
        return self:scalar_mult(a)
    else
        if (not a.rows and a.w) then
            return self:quat_mult(a)
        end
        return self:vec_mult(a)
    end
end

function quat_mt.__add(self, other)
    local result = quat.new()
    result.x = self.x + other.x 
    result.y = self.y + other.y 
    result.z = self.z + other.z 
    result.w = self.w + other.w 
    return result
end

function quat_mt.__tostring(self)
    return self.x..", "..self.y..", "..self.z..", "..self.w
end

quat_mt.__index = quat

function quat.scalar_mult(self, a)
    return quat.new(self.x*a, self.y*a, self.z*a, self.w*a)
end

function quat.vec_mult(self, v)
    local t2  =  self.w * self.x
    local t3  =  self.w * self.y
    local t4  =  self.w * self.z
    local t5  = -self.x * self.x
    local t6  =  self.x * self.y
    local t7  =  self.x * self.z
    local t8  = -self.y * self.y
    local t9  =  self.y * self.z
    local t10 = -self.z * self.z
    local result = vec3()
    result.x = 2*( (t8 + t10) * v.x + (t6 -  t4) * v.y + (t3 + t7) * v.z) + v.x
    result.y = 2*( (t4 +  t6) * v.x + (t5 + t10) * v.y + (t9 - t2) * v.z) + v.y
    result.z = 2*( (t7 -  t3) * v.x + (t2 +  t9) * v.y + (t5 + t8) * v.z) + v.z
    return result
end

-- rotate another quat by this one and return the result
function quat.quat_mult(self, other)
    result = quat.new()
--   w * rkQ.w - x * rkQ.x - y * rkQ.y - z * rkQ.z,
--   w * rkQ.x + x * rkQ.w + y * rkQ.z - z * rkQ.y,
--   w * rkQ.y + y * rkQ.w + z * rkQ.x - x * rkQ.z,
--   w * rkQ.z + z * rkQ.w + x * rkQ.y - y * rkQ.x
    result.w = self.w * other.w - self.x * other.x - self.y * other.y - self.z * other.z
    result.x = self.w * other.x + self.x * other.w + self.y * other.z - self.z * other.y
    result.y = self.w * other.y + self.y * other.w + self.z * other.x - self.x * other.z
    result.z = self.w * other.z + self.z * other.w + self.x * other.y - self.y * other.x
    return result
end

-- it's slerp!
-- fT = alpha -- 0 returns rkP, 1 returns rkQ
function slerp (fT, rkP, rkQ)

    local fCos = rkP:dot(rkQ)
    local fAngle = math.acos(fCos)

    if ( math.abs(fAngle) < .001 ) then
        return rkP
    end

    local fSin = math.sin(fAngle)
    local fInvSin = 1.0/fSin
    local fCoeff0 = math.sin((1.0-fT)*fAngle)*fInvSin
    local fCoeff1 = math.sin(fT*fAngle)*fInvSin
    return (rkP * fCoeff0) + (rkQ * fCoeff1)
end

function quat.dot(self, other)
    return self.x * other.x + self.y * other.y + self.z * other.z + self.w * other.w
end

-- v is a vec3
function quat.fromEulerAngles(v)
    local c = vec3(math.cos(v.x * 0.5), math.cos(v.y * 0.5), math.cos(v.z * 0.5))
    local s = vec3(math.sin(v.x * 0.5), math.sin(v.y * 0.5), math.sin(v.z * 0.5))
    local res = quat()
    res.w = c.x * c.y * c.z + s.x * s.y * s.z;
    res.x = s.x * c.y * c.z - c.x * s.y * s.z;
    res.y = c.x * s.y * c.z + s.x * c.y * s.z;
    res.z = c.x * c.y * s.z - s.x * s.y * c.z;
    return res;
end

function quat.toAngleAxis(self) 
    local sqr_length = self.x * self.x + self.y * self.y + self.z * self.z
    local angle = 0
    local axis = vec3(0, 0, 0)
    if (sqr_length > 0) then
        angle = 2 * math.acos(self.w)
        local inv_length = 1 / math.sqrt(sqr_length)
        axis.x = self.x * inv_length
        axis.y = self.y * inv_length
        axis.z = self.z * inv_length
    else
        axis = vec3(1,0,0)
    end
    return angle, axis
end

function quat.fromAngleAxis(angle, vector)
    local half_angle = 0.5*angle
    local f_sin = math.sin(half_angle)
    return quat(math.cos(half_angle), f_sin*vector.x, f_sin*vector.y, f_sin*vector.z)
end

function quat.fromRotationMatrix(kRot)
    -- Algorithm in Ken Shoemake's article in 1987 SIGGRAPH course notes
    -- article "quat Calculus and Fast Animation".

    local fTrace = kRot[0][0]+kRot[1][1]+kRot[2][2]
    local fRoot
    local self = quat()

    if ( fTrace > 0.0 ) then
        -- |w| > 1/2, may as well choose w > 1/2
        fRoot = math.sqrt(fTrace + 1.0) -- 2w
        self.w = 0.5*fRoot
        fRoot = 0.5/fRoot -- 1/(4w)
        self.x = (kRot[2][1]-kRot[1][2])*fRoot
        self.y = (kRot[0][2]-kRot[2][0])*fRoot
        self.z = (kRot[1][0]-kRot[0][1])*fRoot
    else
        -- |w| <= 1/2
        local s_iNext = { 2, 3, 1 }
        local conv = {"x", "y", "z", "w"}
        local i = 1
        if ( kRot[1][1] > kRot[0][0] ) then
            i = 2
        end
        if ( kRot[2][2] > kRot[i][i] ) then
            i = 3
        end
        local j = conv[s_iNext[i]]
        local k = conv[s_iNext[j]]

        fRoot = math.sqrt(kRot[i][i]-kRot[j][j]-kRot[k][k] + 1.0)
        self[i] = 0.5*fRoot
        fRoot = 0.5/fRoot
        self.w = (kRot[k][j]-kRot[j][k])*fRoot
        self[j] = (kRot[j][i]+kRot[i][j])*fRoot
        self[k] = (kRot[k][i]+kRot[i][k])*fRoot
    end

    return self
end

function quat.toRotationMatrix(self)
    -- return value
    local kRot = mat3()

    local fTx  = 2.0*self.x
    local fTy  = 2.0*self.y
    local fTz  = 2.0*self.z
    local fTwx = fTx*self.w
    local fTwy = fTy*self.w
    local fTwz = fTz*self.w
    local fTxx = fTx*self.x
    local fTxy = fTy*self.x
    local fTxz = fTz*self.x
    local fTyy = fTy*self.y
    local fTyz = fTz*self.y
    local fTzz = fTz*self.z

    kRot[0][0] = 1.0-(fTyy+fTzz)
    kRot[0][1] = fTxy-fTwz
    kRot[0][2] = fTxz+fTwy
    kRot[1][0] = fTxy+fTwz
    kRot[1][1] = 1.0-(fTxx+fTzz)
    kRot[1][2] = fTyz-fTwx
    kRot[2][0] = fTxz-fTwy
    kRot[2][1] = fTyz+fTwx
    kRot[2][2] = 1.0-(fTxx+fTyy)

    return kRot
end

function quat.fromAxes(xAxis, yAxis, zAxis)
    local kRot = mat3()
 
    kRot[0][0] = xAxis.x;
    kRot[1][0] = xAxis.y;
    kRot[2][0] = xAxis.z;
 
    kRot[0][1] = yAxis.x;
    kRot[1][1] = yAxis.y;
    kRot[2][1] = yAxis.z;
 
    kRot[0][2] = zAxis.x;
    kRot[1][2] = zAxis.y;
    kRot[2][2] = zAxis.z;
 
    return quat.fromRotationMatrix(kRot);
end

function quat.toAxes(self)
    kRot = self:toRotationMatrix(kRot);

    local x_axis = vec3()
    local y_axis = vec3()
    local z_axis = vec3()

    x_axis.x = kRot[0][0]
    x_axis.y = kRot[1][0]
    x_axis.z = kRot[2][0]

    y_axis.x = kRot[0][1]
    y_axis.y = kRot[1][1]
    y_axis.z = kRot[2][1]

    z_axis.x = kRot[0][2]
    z_axis.y = kRot[1][2]
    z_axis.z = kRot[2][2]
    return x_axis, y_axis, z_axis
end

vox_global_table._G.quat = quat