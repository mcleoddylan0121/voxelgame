// Setting this to true will render bounding volumes
void renderDebug(bool b);
void toggleDebug();

void setPlayer(Entity player);
Entity getPlayer();
Entity getEntity(EntityID i);

/* Entity Functions */

struct Entity {
	
	// Register an entity creation function with the specified name
	// The creation function is passed a base entity and the paramaters of spawn function
	// The base entity will be an instance of the superclass
	static void register(string name, function<void(Entity, args...)> constructor);

	// Call a registered entity creation function with the specified arguments
	static Entity spawn(string name, vec3 pos, args...);
	static Entity spawnHere(string name, args...);

	// A blank entity with the default stats
	Entity(vec3 pos);

	// Passes the entity to the constructor of the given entity type
	void makeType(string name, args...);
	void kill(); // RIP Entity

	void setStats(Table newStats); // Basically a batch-setStat
	void setStat(string name, float value);

	// Event system, see entityevents_lib.h for all reserved events
	void dispatchEvent(string eventName, Table args); // These args will be added inside the Event table, NOTE: this can be omitted
	void addEventListener(string eventName, function<void(Table)> onCall); 	// See entityevents_lib.h for the Table
	void addEventListener(EntityEventListener e); 							// See entityevents_lib.h
	void removeEventListener(EntityEventListener e);  // Note it is 100% safe to call this inside of a listener's onCall function

	// Sets the bounding box with the terrain
	void setTerrainVolume(vec3 size);

	// These functions are defaulted to false
	void setNoClip(bool noClip); // Allows the entity to move through terrain.
								 // False sets the acceleration to gravity.
								 // True sets the acceleration to 0
	void setSolid(bool solid);	 // Determines whether other entities can pass through this entity.
	void setUntouchable(bool b); // Determines whether or not other entities can trigger collide events with this entity

	void setPos(vec3 newPos); // Union: Entity::setPosition
	void setVel(vec3 newVel); // Union: Entity::setVelocity
	void setAcc(vec3 newAcc); // Union: Entity::setAcceleration

	// These function have the same unions as the previous ones. See below for the path struct.
	// Note: These functions overwrite eachother. For instance, calling the path version of setPos and then the vector version
	//       will overwrite the path version. Furthermore, if you set an acceleration path after setting a velocity path that
	//       sets the object's velocity every tick, the acceleration path will not do much because its change to the velocity
	//       is being continuously overwritten, so keep that in mind.
	void setPos(Path p);
	void setVel(Path p);
	void setAcc(Path p);

	void stop(); // Sets the velocity to 0

	// Note that these functions will overwrite the work of itself and the other.
	void setMesh(string meshName, float voxelSize = 1/8.f);
	void setSkeleton(Skeleton s, float voxelSize = 1/8.f); // See anim_lib for skeletons

	// These functions only work if the entity has a skeleton
	void queueAnimation(string animation);	// Sets the next animation. Note: this will overwrite any previously queued animations.
	void setAnimation(string animation);	// Will interrupt the current animation and play the new one.

	vec3 getPos(); // Union: Entity::getPosition
	vec3 getVel(); // Union: Entity::getVelocity
	vec3 getAcc(); // Union: Entity::getAcceleration

	vec3 getSize(); // This is meshSize * voxelSize

private:
	void _collide(string otherEntityAddress); // C++ uses this to dispatch a collide event
	
	string _address; // Used in c++ to get access to the entity
	EntityID _id; // Used by lua to access the c++ side entity
	
	Table _stats;
	Table _spells;
	vec3 size; // Updated every time setMesh is called

	// See the relevant set function
	bool noClip;
	bool solid;
	bool untouchable;
};