
// Required to register an item
struct ItemDefinition {
	ItemDefinition();

	void setMesh(string meshName);				// If no mesh is set, the mesh of the object attached
												//  to this item will be used
	void setDescription(string description);
	void setDisplayName(string name);			// If no display name is set, the name the object
												//  attached to this item will be used


	string meshName, description, displayName;
};