struct Skeleton {
	Skeleton(Array<JointObject> jointList, AnimationSet animations);
};

// Note: Not an actual variable, just a template to pass into KeyFrame
// Note 2: These vectors are all relative
Array JointMotionObject = {string jointName, vec3 posOnParent, vec3 rotAnchor, vec3 offset, quat rotation}

struct KeyFrame {
	KeyFrame(Array<JointMotionObject> jointMotionList, float time);
};

struct Animation {
	Animation(Array<KeyFrame> frames, bool rep); // rep = repeat (repeat is claimed by lua)
};

// Note: Not an actual variable, just a template to pass into Skeleton
Array JointObject = {string jointName, string meshName, string parentJointName = "_root"}

// Note: Not an actual variable, just a template to pass into Skeleton
Table AnimationSet = {
	base = Array<JointMotionObject>, // See KeyFrame. If any parameter is nil or a
	                                 //  joint not specified, it will default to this
	anim_name = Animation(...),      // Specify any number of animations like this
	(...)
}


/** Notes about standardized joint names (used in code):

head		- This will move this around when the entity looks at something.
leftHand	- This holds items and in animations control the orientation of items.
rightHand	- Same as leftHand just a different hand.

_leftHand	- Internal joint attached to leftHand. DO NOT use this name (It will be overwritten).
_rightHand	- Internal joint attached to rightHand. DO NOT use this name (It will be overwritten).

*/

/** List of triggers with some notes:

WALK		- Only triggers when the entity is on the ground
IDLE		- This is the default trigger played when no animation is running.
INAIR		- This triggers whenever the player falls for more than one units.

*/