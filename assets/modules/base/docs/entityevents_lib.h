
// Event pseudo-code. Note that all events also contain listener and entity
void collide(Entity other);	// Note, this event will be called once per collision


struct Event {
	EntityEventListener listener; // If not created yourself, it was created internally.
	Entity this; // The entity to which this event was dispatched
	(...) // Whatever extra information passed into dispatchEvent (see above Event pseudo-code)
};


struct EntityEventListener {
	string eventName;
	function<void(Table)> onCall;
}