struct Effect {
	// Note: all times are in seconds.

	// Duration is the amount of time the effect travels
	// LingeringDuration is time between the end of the effect and when
	//  the effect is removed. A negative value will prevent the effect
	//  from being removed until the remove() function is called.
	Effect(float duration, float lingeringDuration = 0);

	// TODO: Something something motion

	void add(EffectComponent e); // See below

	void start();	// Nothing will happen before you call this function,
					//  so set up the effect then call start
	void stop();	// Pauses the effect
	void remove();	// Removes the effect completely. Note, you only need
					//  to call this if you want to prematurely end the
					//  effect or if you set the lingering duration to -1

	float getTimeLeft();
	void setTimeLeft(float newTime);	// A time over duration will cut
										//  into lingeringDuration.

	vec3 getPos();				// Returns the center of the effect
	void setPos(vec3 newPos);	// Temporary until I create paths

	// Variables
	float duration;
	float lingeringDuration;
};


// Effect Components
// Note: to use, create a table with the variables listed. If the variable
//       is optional, it will have its default value listed.

struct Sprite {
	string type = "sprite";
	
	string texName;
	vec2 texSize = vec2(-1, -1);	// A size of {-1, -1} will use the entire texture
	vec2 texOffs = vec2(0, 0);		// From the bottom-left

	float rot = 0;					// In radians, CW
	vec2 scale = vec2(1, 1);
	vec4 color = RGBA_F(1, 1, 1, 1);
};

struct AnimatedSprite {
	string type = "animatedSprite";
	// TODO
};

struct Light {
	string type = "light";
	float intensity = 0.5f;			// Domain [0, ∞)
	float radius = 1.f;
	float compensation = -1.f;		// Simulates scattering, -1 will generate a best-fit value
	vec4 color = RGB_F(1, 1, 1);	// Use the rgb functions (see gui library). Alpha is discarded: use intensity instead
};

// Note, each vec2 is in the format (min, max)
struct ParticleEmitter {
	string type = "particleEmitter";
	vec4 scale = vec4(1, 1, 1, 1);		// Particle Scale. Note, format is (minX, minY, maxX, maxY)
	vec2 particleCD = vec2(1, 1);		// Particle Cooldown: Time between particle spawns
	vec2 particleLife = vec2(1, 1);		// IE particle effect duration

	bool rotate = false;				// Rotate particles in the direction of travel

	// See spherical coordinates
	vec2 theta = vec2(0, 2π);			// θ: Angle on the xz plane [0, 2π]
	vec2 phi = vec2(0, π);				// φ: Tilt starting at +y and ending at -y [0, π]
	vec2 rho = vec2(1, 1);				// ρ: Radius of resulting range (ie speed)

	vec3 acceleration = Entity.GRAVITY;

	EffectComponent[] particle;			// This array defines the emitter's particles
};