import "base::Entity"
import "base::Effect"
importAll "base::GUI"
import "base::const.KEY"

run "gui.test"
run "structures.test"


import "anim"

-- fox
Entity.register("fox",
	function(obj)
		obj:setMesh("chr_fox")
		obj:setStat("moveSpeed", 10)
		obj:setStat("jumpHeight", 6)
		obj:setTerrainVolume(vec3(0.5, obj.size.y, 0.5))
	end
)

-- chickman
Entity.register("chicken",
	function(obj)
		obj:setSkeleton(anim.chicken_skel)
		obj:setAnimation("walk")
		obj:setStat("moveSpeed", 10)
		obj:setStat("jumpHeight", 6)
		obj:setTerrainVolume(vec3(0.5, obj.size.y + 1/16 * 8, 0.5))
	end
)

Entity.register("shark",
	function(obj)
		obj:setSkeleton(anim.shark_skel)
		obj:setAnimation("walk")
		obj:setStat("moveSpeed", 10)
		obj:setStat("jumpHeight", 6)
		obj:setTerrainVolume(vec3(10, 10, 10))
	end
)

Entity.register("oblong",
	function(obj)
		obj:setMesh("oblong", 0.5)
	end
)

Entity.register("testplayer",
	function(obj)
		obj:setSkeleton(anim.chad_skel)
		obj:setAnimation("walk")
		obj:setStat("moveSpeed", 10)
		obj:setStat("jumpHeight", 6)
		obj:setTerrainVolume(vec3(0.5, obj.size.y, 0.5))
	end
)

e = Entity.spawn("testplayer", vec3(10, 100, 10))
Entity.setPlayer(e)

--e:addEventListener("collide", function(event) event.other:setVel(vec3(math.random() * 40 - 20, 50, math.random() * 40 - 20)) event.other:kill() end)

addGlobalListener({
	keyDown = function(event)
		if event.keyCode == KEY.LEFT then
			e:setPos(e:getPos() + vec3(0, 10, 0))
		elseif event.keyCode == KEY.UP then
			local e = Effect(2)
			--[[e:add({
				type = "sprite",
				texName = "testgametiles",
				texSize = vec2(24, 24),
				rot = 0,
				scale = 1,
			})]]
			e:add({
				type = "particleEmitter",
				particleLife = vec2(10, 10),
				particleCD = vec2(0.1, 0.2),
				rho = vec2(15, 20),
				phi = vec2(0, math.pi / 4),
				scale = vec4(1, 1, 10, 1),
				particle = {{type = "sprite", texName = "particles", texSize = vec2(17, 16), texOffs = vec2(0, 64), rot = 0, scale = vec2(0.1, 0.1)}}
			})


			--[[,
					{
						type = "light",
						intensity = 0.4,
						radius = 10,
						color = RGB_F(math.random(), math.random(), math.random())
					}]]
			e:setPos(Entity.getPlayer():getPos())
			e:start()

		end
	end

})