import "base::util.Misc"



NULL_VOXELID = 65535 -- This means that no change will be made here

Pattern = {};

function Pattern.assignFinalPattern(self, pattern)
	pat_assignFinalPattern(self.ID, pattern);
end

function Pattern.mergePatterns(self, patterns)
	return pat_mergePatterns(self.ID, patterns);
end

function Pattern.createConstVoxelIDFunctor(self, material)
	return pat_createConstantVoxelIDFunctor(self.ID, material);
end

function Pattern.createBoxPattern(self, functor, position, size)
	return pat_createBoxPattern(self.ID, functor, position, size);
end

function Pattern.createBoxPattern_startEnd(self, functor, position1, position2)
	return pat_createBoxPattern_startEnd(self.ID, functor, position1, position2);
end

function Pattern.createRoofFrameDiagonal(self, functors, position, size, cycleDim, lengthDim, invSlope, flipDir)
	return pat_createRoofFrameDiagonal(self.ID, functors, position, size, cycleDim, lengthDim, invSlope, flipDir);
end

-- Makes the walls to a house
function Pattern.makeWalls(self, functor, position1, position2)
	local boxes = {
		self:createBoxPattern_startEnd(functor, position1, vec3(position2.x, position2.y, position1.z) ),
		self:createBoxPattern_startEnd(functor, position1, vec3(position1.x, position2.y, position2.z) ),
		self:createBoxPattern_startEnd(functor, position2, vec3(position1.x, position1.y, position2.z) ),
		self:createBoxPattern_startEnd(functor, position2, vec3(position2.x, position1.y, position1.z) )
	};
	
	return self:mergePatterns(boxes);
end

-- Makes the frame to a house (4 walls, roof)
function Pattern.makeFrame(self, functor, position1, position2)
	local y1 = position1.y;
	local y2 = position2.y;
	
	local yTop = math.max(y1,y2);
	
	local boxes = {
		-- 4 walls
		self:createBoxPattern_startEnd(functor, vec3(position1.x,y1,position1.z), vec3(position1.x,y2,position1.z)),
		self:createBoxPattern_startEnd(functor, vec3(position2.x,y1,position1.z), vec3(position2.x,y2,position1.z)),
		self:createBoxPattern_startEnd(functor, vec3(position2.x,y1,position2.z), vec3(position2.x,y2,position2.z)),
		self:createBoxPattern_startEnd(functor, vec3(position1.x,y1,position2.z), vec3(position1.x,y2,position2.z))
	};
	
	local roofFrame = self:makeWalls(functor, vec3(position1.x, yTop, position1.z), vec3(position2.x, yTop, position2.z));
	
	return self:mergePatterns( { self:mergePatterns(boxes), --[[roofFrame]] } );
end

structureEval_mt = {
	__index = Pattern
};

function structureEval(ID, position)
	local obj = {};
	obj.ID = ID;
	obj.position = position; -- position attribute is used if we need to know the world position for sampling, but not for placing voxels.
	setmetatable(obj, structureEval_mt);
	return obj;
end

big_ol_square = {

	size = vec3(5,5,5),
	
	height = 5,
	
	evaluate = function(ID, position)
		local eval = structureEval(ID,position);
		
		local p1 = vec3(1,1);
		local p2 = vec3(10,5,14);
		local height = 5;
		local material = 2; -- dirt
		local functor = eval:createConstVoxelIDFunctor(material);
		
		local frameMaterial = 3; --idk what this one is
		local frameFunctor = eval:createConstVoxelIDFunctor(frameMaterial);
		
		local walls = eval:makeWalls(functor, p1, p2);
		local flr = eval:createBoxPattern_startEnd(functor, p1, vec3(p2.x,p1.y,p2.z));
		local frame = eval:makeFrame(frameFunctor, p1 - vec3(1,0,1), p2 + vec3(1,1,1));
		
		local roofFunctors = {functor,frameFunctor};
		
		local roofHeight = (p1.x + p2.x)/2/2 + 1;
		
		local roofStart = vec3(p1.x,p2.y+2,p1.z-1);
		local roofEnd = vec3((p1.x + p2.x)/2+1, p2.y+2 + roofHeight, p2.z+2);
		
		local roof = eval:createRoofFrameDiagonal(roofFunctors, roofStart, roofEnd-roofStart, 0, 2, 2, false);
		
		local roofStart2 = vec3(p2.x, p2.y+2, p1.z-1);
		local roofEnd2 = vec3((p1.x + p2.x)/2+1 + (p2.x-p1.x), p2.y+2 + roofHeight, p2.z+2);
		
		local roof_other = eval:createRoofFrameDiagonal(roofFunctors, roofStart2, roofEnd2-roofStart2, 0, 2, 2, true);
		
		local roofFrameBorder1 = eval:createBoxPattern_startEnd(frameFunctor, vec3(p1.x-1,p2.y+1,p1.z), vec3(p1.x-1,p2.y+1,p2.z));
		local roofFrameBorder2 = eval:createBoxPattern_startEnd(frameFunctor, vec3(p2.x+1,p2.y+1,p1.z), vec3(p2.x+1,p2.y+1,p2.z));
		
		
		local merged = eval:mergePatterns({walls, flr, frame, roof, roof_other, roofFrameBorder1, roofFrameBorder2});
		
		eval:assignFinalPattern(merged);
		
	end

}



Misc.vox_make_index_in_table("vox_structure_table", big_ol_square)

























































		--[[
		-- ID, voxelID = 1, relative position = {0,0,0}, size = this.size
		-- functor2 = VOXEL_ID_FUNCTOR_CREATE_CONSTANT(ID,2);
		-- functor3 = VOXEL_ID_FUNCTOR_CREATE_REPLACE(ID,1,functor2); -- Wow-wee! a functor within a functor!
		
		-- local box = pat_createBoxPattern(ID,functor1,vec3(0),big_ol_square.size) --position not used as we already know position in c++
	
		-- PATTERN_APPLY_FUNCTOR(ID,box,functor2) -- demonstrating voxelIDFunctors
		
		-- pat_assignFinalPattern(ID,box)
		
		local wallLength = 6;
		local material = 2; -- That's dirt!
		local functor1 = pat_createConstantVoxelIDFunctor(ID,material);
		local frameMaterial = 3; -- I don't know what that is!
		
		local snake = { -- Or is it a hydra?
			points = {}, -- List of points that we're using
		};
		
		local startingPoint = {
			position = vec2(0,0), -- origin
			connections = {} -- empty
		};
		
		table.insert(snake.points, startingPoint); -- start at the origin
		
		for o = 1,2 do 
			for pt = 1,#snake.points do
				
				local connectionsOnBranch = 2;
				
				local connects = {vec2(0,1),vec2(1,0),vec2(0,-1),vec2(-1,0)}; -- Pull random amount of cards from this deck
				
				for c = 1,connectionsOnBranch do
					-- Get next element in deck
					local nConnect = table.remove(connects, math.random(#connects)); -- remove element from connects, use it as our next point on the base
				
					local dist = math.random(5,10);
					
					nConnect = vec2(nConnect.x*dist,nConnect.y*dist);
					
					local nPoint = {
						position = snake.points[pt].position + nConnect,
						connections = {pt}
					};
					
					table.insert(snake.points,nPoint);
					table.insert(snake.points[pt].connections,#snake.points);
				
				end
			end
		end
		
		local boxes = {};
		
		for pt = 1,#snake.points do
			
			local position = snake.points[pt].position;
			local pos3d = vec3(position.x,0,position.y) + vec3(16,16,16);
			for c = 1,#snake.points[pt].connections do 
				local cposition = snake.points[ snake.points[pt].connections[c] ].position;
				local cpos3d = vec3(cposition.x,0,cposition.y) + vec3(16,16,16);
			
				table.insert(boxes, pat_createBoxPattern_startEnd( ID,functor1,pos3d,cpos3d ) )
			end
			
		end
		
		local final = pat_mergePatterns(ID,boxes);
		pat_assignFinalPattern(ID,final);
		
		--]]