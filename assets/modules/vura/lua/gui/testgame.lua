local grid_size = vec2(24, 16)

local pixel_size = 2
local atlas_tile_size = vec2(16, 16)
local tile_size = atlas_tile_size * pixel_size

local atlas_char_size = vec2(16, 25)
local char_size = atlas_char_size * pixel_size

local tiles_file = "testgametiles"
local char_file = "testgame"

testgame_win = Window("Get yo' game on", grid_size * tile_size + vec2(2, 28))
testgame_win:setVisible(false)
testgame_win:setPos(vec2(100, 100))
testgame_win:addListener({
	keyPressed = function(event)
		if escPressed and event.keyCode == KEY.ESCAPE then
			testgame_win:setVisible(false)
		end
	end
})

local function makeSkin(file, size, tile_offs)
	local skin = {}
	skin.frameFactory = GUI.NO_FRAME
	skin.backTex = makeQuadFromAtlas(file, AtlasEntry(size * tile_offs, size), size * pixel_size, vec2(0, 0))
	return skin
end

testgame_pane = Component(vec2(0, 0), makeSkin(tiles_file, atlas_tile_size, vec2(0, 0)))
testgame_pane:setInteractable(false)
testgame_win:add(testgame_pane, BoundingRectangle(vec2(GUI.LEFT, GUI.BOTTOM), vec2(1, 1), Relativec2oord(1, 1, true, true, -1, -27)))

local function makeTile(x, y, solid)
	local obj = vec2(x, y)
	obj.solid = solid
	return obj
end

local TILES = {
	GRASS = makeTile(4, 5, false),
	BLACK = makeTile(0, 0, true),
	ROAD_VERT = makeTile(1, 1, false),
	ROAD_HOR = makeTile(2, 0, false),
	ROAD_BOTTOMLEFT = makeTile(1, 0, false)
}

local UNNAMED_TILES = {}
UNNAMED_TILES["1"] = makeTile(12, 3, true)
UNNAMED_TILES["2"] = makeTile(12, 4, true)
UNNAMED_TILES["3"] = makeTile(12, 5, true)
UNNAMED_TILES["4"] = makeTile(13, 5, true)
UNNAMED_TILES["5"] = makeTile(14, 5, true)
UNNAMED_TILES["6"] = makeTile(14, 4, true)
UNNAMED_TILES["7"] = makeTile(14, 3, true)

local tile_comps = {}

for y = 1, grid_size.y do
for x = 1, grid_size.x do
	local tile = Component(tile_size, makeSkin(tiles_file, atlas_tile_size, TILES.BLACK))
	tile.id = TILES.BLACK
	tile:setInteractable(false)
	local pos = vec2(x - 1, y - 1)
	local derp = pos * tile_size
	testgame_pane:add(tile, derp)
	tile_comps[#tile_comps + 1] = tile
end
end

local function getTile(x, y)
	local t = tile_comps[x + y * grid_size.x + 1]
	return t or TILES.BLACK
end

local function setTile(x, y, id)
	local t = getTile(x, y)
	t.id = id
	t:setSkin(makeSkin(tiles_file, atlas_tile_size, id))
end

maps_file = vox_dofile("gui/testgame_maps", this_file)
local MAPS = maps_file.MAPS

local function trim(s)
	local str = s:match'^()%s*$' and '' or s:match'^%s*(.*%S)'
	return string.gsub(str, "\n", "")
end

local function setMap(map)
	local s = trim(map)

	local i = 1

	for y = grid_size.y, 1, -1 do
	for x = 1, grid_size.x do
		local c = s:sub(i,i)
		i = i + 1

		local t_id = TILES.GRASS
		if c == '#' then t_id = TILES.BLACK
		elseif c == ' ' then t_id = TILES.GRASS
		elseif c == '-' then t_id = TILES.ROAD_HOR
		elseif c == '|' then t_id = TILES.ROAD_VERT
		elseif c == '\\' then t_id = TILES.ROAD_BOTTOMLEFT
		elseif c == 's' then this_file["spawnSlime"](x - 1, y - 1)
		else t_id = UNNAMED_TILES[c] or TILES.GRASS
		end

		setTile(x-1, y-1, t_id)
	end
	end
end

setMap(MAPS.EMPTY)

local function ab_sprite(p_x, p_y, s_x, s_y, a_x, a_y)
	local obj =  {
		x = p_x,
		y = p_y,
		w = s_x,
		h = s_y,
		a_x = a_x,
		a_y = a_y
	}
	return obj
end

local ACTION = {
	MOVE = 1,
	ATTACK = 2,
	IDLE = 3,
}

local OFFSETS = {vec2(0, 1), vec2(1, 0), vec2(0, -1), vec2(-1, 0)}
local entities = {}

local function Entity(sprites, size, health)
	local ent = {}
	ent.sprites = sprites
	ent.comp = Component(size * pixel_size, makeSkin(ent.sprites.FILE, size, vec2(0, 0)))
	ent.offs = vec2(0, 0)
	ent.dir = 3
	ent.health = health
	ent.action = ACTION.IDLE
	ent.setPos = function(x, y)
		if not y then x,y = x.x, x.y end
		if getTile(x, y).id.solid then return end
		ent.comp:setPos(vec2(x, y) * tile_size + ent.offs)
		ent.pos = vec2(x, y)
	end
	ent.setSprite = function(sprite)
		if sprite.a_x then
			ent.comp:setSkin(makeSkin(ent.sprites.FILE, vec2(sprite.w, sprite.h), vec2(sprite.x / sprite.w, sprite.y / sprite.h)))
			ent.comp:setSize(vec2(sprite.w, sprite.h) * pixel_size)
			ent.offs.x = (size.x - sprite.w) * (1 - sprite.a_x)
			ent.offs.y = (size.y - sprite.h) * (1 - sprite.a_y)
			ent.offs = ent.offs * pixel_size
		else
			ent.comp:setSkin(makeSkin(ent.sprites.FILE, size, sprite))
			ent.comp:setSize(size * pixel_size)
			ent.offs.x = 0
			ent.offs.y = 0
		end

		ent.comp:setPos(ent.pos * tile_size + ent.offs)
	end
	ent.isInFrontOfMe = function(other)
		local checkPos = (ent.pos + OFFSETS[ent.dir])
		return other.pos.x == checkPos.x and other.pos.y == checkPos.y
	end
	ent.setPos(4, 4)

	testgame_pane:add(ent.comp, ent.comp.pos)

	ent.tick = function()
		if ent.action == ACTION.MOVE then
			ent.setPos(ent.pos + OFFSETS[ent.dir])
		elseif ent.action == ACTION.ATTACK then
			ent.setSprite(ent.sprites.ATTACKS[ent.dir])
			local ent_attack = nil
			
			for i = 1, #entities do
				if ent.isInFrontOfMe(entities[i]) then
					ent_attack = entities[i]
					break
				end
			end
			if ent_attack then
				ent_attack.comp:setHue(RGB_F(1, 0.2, 0.2))
				ent_attack.health = ent_attack.health - 1
				if ent_attack.health <= 0 then
					ent_attack.comp:setVisible(false)
				end
			end
		end
		
		ent.action = ACTION.IDLE
	end

	ent.setDir = function(dir)
		ent.dir = dir
		ent.setSprite(ent.sprites.MOVEMENTS[ent.dir])
	end

	entities[#entities + 1] = ent

	return ent
end

local player = Entity({
	FILE = char_file,
	MOVEMENTS = {vec2(0, 1), vec2(0, 2), vec2(0, 3), vec2(0, 0)},
	ATTACKS = {ab_sprite(31, 100, 16, 31, 1, 1), ab_sprite(62, 100, 30, 25, 1, 1), ab_sprite(47, 100, 16, 31, 1, 0), ab_sprite(0, 100, 31, 25, 0, 1)}
}, atlas_char_size, 10)

function spawnSlime(x, y)
	local a = Entity({
		FILE = "testgame_slime",
		MOVEMENTS = {vec2(0, 0), vec2(0, 1), vec2(0, 2), vec2(0, 3)}
	}, vec2(17, 16), 4)
	a.setPos(x, y)
end

local function e_sort(a, b)
	return a.pos.y < b.pos.y
end

testgame_win:addListener({
	keyDown = function(event)
		for i = 1, #entities do
			entities[i].comp:setHue(RGB_F(1, 1, 1))
		end

		local key = event.keyCode
		local lastDir = player.dir
		if key == KEY.UP then
			player.setDir(1)
			if lastDir == player.dir then player.action = ACTION.MOVE else player.action = ACTION.IDLE return end
		elseif key == KEY.RIGHT then
			player.setDir(2)
			player.action = ACTION.MOVE
			if lastDir == player.dir then player.action = ACTION.MOVE else player.action = ACTION.IDLE return end
		elseif key == KEY.DOWN then
			player.setDir(3)
			player.action = ACTION.MOVE
			if lastDir == player.dir then player.action = ACTION.MOVE else player.action = ACTION.IDLE return end
		elseif key == KEY.LEFT then
			player.setDir(4)
			player.action = ACTION.MOVE
			if lastDir == player.dir then player.action = ACTION.MOVE else player.action = ACTION.IDLE return end
		elseif key == KEY.SPACE then
			player.action = ACTION.ATTACK
		end

		for i = 1, #entities do
			local ent = entities[i]
			if ent ~= player then
				ent.setDir(math.random(4))
				ent.action = math.random(10)
				if ent.action > 3 then ent.action = 1 end
				if ent.isInFrontOfMe(player) then
					ent.action = ACTION.ATTACK
				end
			end
		end

		for i = 1, #entities do
			local ent = entities[i]
			if ent.action == ACTION.MOVE then ent.tick() end
		end
		for i = 1, #entities do
			local ent = entities[i]
			if ent.action == ACTION.ATTACK then ent.tick() end
		end	
		for i = 1, #entities do
			local ent = entities[i]
			if ent.action ~= ACTION.IDLE then ent.tick() end
		end

		local iterated = false
		while not iterated do
			iterated = true
			for i = 1, #entities do
				local ent = entities[i]
				if ent ~= player and not ent.comp.visible then
					table.remove(entities, i)
					iterated = false
					break
				end
			end
		end
	end
})


setMap(MAPS.TEST)