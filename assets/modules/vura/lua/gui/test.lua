importAll "base::GUI"
import "base::const.KEY"

escPressed = false

local win = Window("The Sky's the Limit", vec2(420 --[[Blaze it]], 300))
win:setVisible(false)

local skyRanges = {
	{0,1}, {0,20}, {0,1}, {0,2000}, {0,5}, {0,3}, 
	{0,3}, {0,3},  {0,3}, {0,3},  {-1,1}, {0,1}
}

local skyDefaults = {
	0.99,15.0,0.1,1000.0,1.8,0.139,
	0.0256,0.81,0.39,0.028,0.63,0.99,
}

local skyNames = {
	"camera height", "rayleigh brightness", "mie brightness", "sun brightness", "intensity", "rayleigh strength", 
	"mie strength", "rayleigh collection power", "mie collection power", "scatter strength",  "mie distribution", "surface height"
}

local sliders = {}

for y = 0, 6-1 do
for x = 0, 3-1 do
	local i = x+y*3+1
	if i == 13 then
		local button = Button("Exit Game", vec2(100, 20))

		button:addListener({
			activated = function(event)
				exitGame();
			end
		})

		win:add(button, vec2(20 + (100 + 30) * x + 5, 10 + (10 + 30) * (y + 1)) - 5)
		break
	end
	if i > 13 then break end
	local slider = HorizontalSlider(100, skyRanges[i][1], skyRanges[i][2])
	slider:addListener({
		activated = function(event)
			setAtmosVal(i-1, slider.value)
		end
	})
	slider:setValue(skyDefaults[i])
	win:add(slider, vec2(20 + (slider.size.x + 30) * x, 10 + (slider.size.y + 30) * (y + 1)))
	sliders[#sliders+1] = slider
end
end

local lowerTimeRate = Button("<", vec2(20,20))
lowerTimeRate:addListener({
	activated = function(event)
		mulTimeRate(0.5)
	end
})
win:add(lowerTimeRate, vec2(20, 235))

local raiseTimeRate = Button(">", vec2(20,20))
raiseTimeRate:addListener({
	activated = function(event)
		mulTimeRate(2.0)
	end
})
win:add(raiseTimeRate, vec2(50, 235))

win:add(ScrollBar(win.size.y - 34 - 15*2, 0, 5), vec2(win.size.x - 15 - 4, 18))

local cons = Window("Console", vec2(420, 300))

local conshistory = ""

local _consbox = TextBox(vec2(0, 0))
_consbox:setHue(RGB_F(0, 0, 0))
_consbox:addListener({
	keyReleased = function(event)
		_consbox:setText(conshistory)
	end
})
_consbox.textBounds = BoundingRectangle(vec2(GUI.LEFT, GUI.TOP), vec2(12, 0), RelativeCoord(1, 1, true, true))

local _tb = TextBox(vec2(1, 1));
_tb:setHue(RGB_F(0, 0, 0))
_tb:addListener({
	keyDown = function(event)
		if event.keyCode == KEY.RETURN and string.len(_tb.text) > 2 then
			vox_dostring(_tb.text:sub(3), GUI.codeContext)
			conshistory = conshistory .. _tb.text .. "\n"
			_consbox:setText(conshistory)
			_tb:setText("> ")
		elseif event.keyCode == KEY.BACKSPACE and string.len(_tb.text) < 2 then
			_tb:setText("> ")
		end
	end
})
_tb:setText("> ")
cons:add(_tb, BoundingRectangle(vec2(GUI.LEFT, GUI.BOTTOM), vec2(0, 0), RelativeCoord(1, 18, true, false)))
cons:add(_consbox, BoundingRectangle(vec2(GUI.LEFT, GUI.TOP), vec2(0, 26), RelativeCoord(1, 1, true, true, 0, -18 - 26)))
cons:setVisible(false)
cons:setPos(vec2(500, 100))

function good2go()
	return not (win.visible and cons.visible)
end

addGlobalListener({
	keyPressed = function(event)
		if good2go() and event.keyCode == KEY.ESCAPE then
			escPressed = not escPressed
			setMouseGrabbed(not escPressed)
			if escPressed then
				win:setVisible(true)
				cons:setVisible(true)
			end
		end
	end
})

