import "base::anim.Skeleton"
import "base::anim.KeyFrame"
import "base::anim.Animation"

chicken_anim = {
	base = {
		{"body", vec3(), vec3(), vec3(), quat(0, 0, 0)},
		{"head" , vec3(0.75,5.99/9.0,0.5), vec3(0.5,0,0.25), vec3(), quat.fromAngleAxis(-math.pi / 2, vec3(0, 1, 0)) * quat.fromAngleAxis(0, vec3(1,0,0)) * quat(0, math.pi, 0)},
		{"left wing" , vec3(0.5,0.75,0), vec3(0,1,0.5), vec3(), quat.fromAngleAxis(math.pi / 2, vec3(0,1,0))},
		{"right wing" , vec3(0.5,0.75,1), vec3(1,1,0.5), vec3(), quat.fromAngleAxis(math.pi / 2, vec3(0,1,0))},
		{"tail" , vec3(0.05,0.5,0.5), vec3(0.05,0.5,0.55), vec3(), quat.fromAngleAxis(-math.pi / 2, vec3(0,1,0)) * quat.fromAngleAxis(-math.pi/6, vec3(0,1,0)) * quat(0,-math.pi/2,0)},
		{"left leg" , vec3(0.4,0,0.65), vec3(0,0.66,0.5), vec3(), quat.fromAngleAxis(-math.pi / 2, vec3(0,1,0)) * quat.fromAngleAxis(-math.pi/6, vec3(1,0,0)) * quat(0,math.pi/2,0)},
		{"right leg" , vec3(0.6,0,0.35), vec3(0,0.66,0.5), vec3(), quat.fromAngleAxis(-math.pi / 2, vec3(0,1,0)) * quat.fromAngleAxis(math.pi/6, vec3(1,0,0)) * quat(0,math.pi/2,0)},
	},

	walk = Animation({
		KeyFrame({}, 1),
		KeyFrame({
			{"left wing" , nil, nil, nil, quat.fromAngleAxis(math.pi / 2, vec3(0,1,0)) * quat(0,0,3*math.pi/4)},
			{"right wing" , nil, nil, nil, quat.fromAngleAxis(math.pi / 2, vec3(0,1,0)) * quat(0,0,-3*math.pi/4)},
			{"tail" , nil, nil, nil, quat.fromAngleAxis(-math.pi / 2, vec3(0,1,0)) * quat.fromAngleAxis(math.pi/6, vec3(0,1,0)) * quat(0,-math.pi/2,0)},
			{"left leg" , vec3(0.6,0,0.65), nil, nil, quat.fromAngleAxis(-math.pi / 2, vec3(0,1,0)) * quat.fromAngleAxis(math.pi/6, vec3(1,0,0)) * quat(0,math.pi/2,0)},
			{"right leg" , vec3(0.4,0,0.35), nil, nil, quat.fromAngleAxis(-math.pi / 2, vec3(0,1,0)) * quat.fromAngleAxis(-math.pi/6, vec3(1,0,0)) * quat(0,math.pi/2,0)},
		}, 1)},
  true)
}


chicken_skel = Skeleton({
	{"body", "chicken/body"},
	{"head", "chicken/head", "body"},
	{"left wing", "chicken/left wing", "body"},
	{"right wing", "chicken/right wing", "body"},
	{"tail", "chicken/tail1", "body"},
	{"left leg", "chicken/leg", "body"},
	{"right leg", "chicken/leg", "body"}
}, chicken_anim)

shark_anim = {
	base = {
		{"body", vec3(), vec3(), vec3(), quat(0, 0, 0)},
		{"head" , vec3(0.75,5.99/9.0,0.5), vec3(0.5,0,0.25), vec3(), quat.fromAngleAxis(-math.pi / 2, vec3(0, 1, 0)) * quat.fromAngleAxis(0, vec3(1,0,0)) * quat(0, math.pi, 0)},
		{"left wing" , vec3(0.6,0.8,0.9), vec3(0,1,0.5), vec3(), quat.fromAngleAxis(-math.pi / 2, vec3(0,1,0))},
		{"right wing" , vec3(0.6,0.8,0.1), vec3(1,1,0.5), vec3(), quat.fromAngleAxis(-math.pi / 2, vec3(0,1,0))},
		{"tail" , vec3(0.05,0.5,0.5), vec3(0.05,0.5,0.55), vec3(), quat.fromAngleAxis(-math.pi / 2, vec3(0,1,0)) * quat.fromAngleAxis(-math.pi/6, vec3(0,1,0)) * quat(0,-math.pi/2,0)},
		{"left leg" , vec3(0.4,0,0.65), vec3(0,0.66,0.5), vec3(), quat.fromAngleAxis(-math.pi / 2, vec3(0,1,0)) * quat.fromAngleAxis(-math.pi/6, vec3(1,0,0)) * quat(0,math.pi/2,0)},
		{"right leg" , vec3(0.6,0,0.35), vec3(0,0.66,0.5), vec3(), quat.fromAngleAxis(-math.pi / 2, vec3(0,1,0)) * quat.fromAngleAxis(math.pi/6, vec3(1,0,0)) * quat(0,math.pi/2,0)},
		{"dorsal fin", vec3(0.575,1.0,0.5), vec3(0.5,0,0.5), vec3(), quat.fromAngleAxis(math.pi / 2, vec3(0,1,0))}
	},

	walk = Animation({
		KeyFrame({
			{"left wing" , nil, nil, nil, quat.fromAngleAxis(-math.pi / 2, vec3(0,1,0)) * quat(0,0,-1*math.pi/8)},
			{"right wing" , nil, nil, nil, quat.fromAngleAxis(-math.pi / 2, vec3(0,1,0)) * quat(0,0,1*math.pi/8)},
			}, 2),
		KeyFrame({
			{"left wing" , nil, nil, nil, quat.fromAngleAxis(-math.pi / 2, vec3(0,1,0)) * quat(0,0,1*math.pi/8)},
			{"right wing" , nil, nil, nil, quat.fromAngleAxis(-math.pi / 2, vec3(0,1,0)) * quat(0,0,-1*math.pi/8)},
			{"tail" , nil, nil, nil, quat.fromAngleAxis(-math.pi / 2, vec3(0,1,0)) * quat.fromAngleAxis(math.pi/6, vec3(0,1,0)) * quat(0,-math.pi/2,0)},
			{"left leg" , vec3(0.6,0,0.65), nil, nil, quat.fromAngleAxis(-math.pi / 2, vec3(0,1,0)) * quat.fromAngleAxis(math.pi/6, vec3(1,0,0)) * quat(0,math.pi/2,0)},
			{"right leg" , vec3(0.4,0,0.35), nil, nil, quat.fromAngleAxis(-math.pi / 2, vec3(0,1,0)) * quat.fromAngleAxis(-math.pi/6, vec3(1,0,0)) * quat(0,math.pi/2,0)},
		}, 2)},
  true)
}

shark_skel = Skeleton({
	{"body", "shark/body"},
	{"head", "shark/pelvic fin", "body"},
	{"left wing", "shark/left fin", "body"},
	{"right wing", "shark/right fin", "body"},
	{"tail", "shark/tail", "body"},
	{"left leg", "chicken/leg", "body"},
	{"right leg", "chicken/leg", "body"},
	{"dorsal fin", "shark/first dorsal fin", "body"}
}, shark_anim)


testplayer_anim = {
	base = {
		{"body", vec3(), vec3(), vec3(), quat(0, 0, 0)},
		{"head" , vec3(0.5, 1, 0.5), vec3(0.5, 0, 0.5), vec3(), quat.fromAngleAxis(0, vec3(0,1,0))},
		{"rightArm1" , vec3(3/5,2/5,6/6 + 0.00001), vec3(0.5, 0.75, 0.5), vec3(-0.5, 1.5, -0.5), quat.fromAngleAxis(-math.pi / 2, vec3(0,1,0))},
		{"rightArm2", vec3(0+ 0.0001, -1, 0), vec3(0.5, 1, 0.5), vec3(0.5, 2, 0.5), quat.fromAngleAxis(math.pi / 2, vec3(0,1,0))},
		{"leftArm1" , vec3(3/5,2/5,1/6 + 0.00001), vec3(0.5, 0.75, 0.5), vec3(-0.5, 1.5, -0.5), quat.fromAngleAxis(-math.pi / 2, vec3(0,1,0))},
		{"leftArm2", vec3(0 - 0.0001, -1, 0), vec3(0.5, 1, 0.5), vec3(0.5, 2, 0.5), quat.fromAngleAxis(math.pi / 2, vec3(0,1,0))},
		{"leftLeg" , vec3(3/5, 0, 1/6 + 0.00001), vec3(0.5, 1, 0.5), vec3(-0.5, 0, 0.5), quat.fromAngleAxis(-math.pi / 2, vec3(0,1,0))},
		{"rightLeg" , vec3(3/5, 0, 4/6 + 0.00001), vec3(0.5, 1, 0.5), vec3(-0.5, 0, 0.5), quat.fromAngleAxis(-math.pi / 2, vec3(0,1,0))},
		{"leftHand", vec3(0.5+ 0.0002, 0, 0.5 - 0.0002), vec3(0.5, 0.5, 0.5), vec3(), quat()},
		{"rightHand", vec3(0.5+ 0.0002, 0, 0.5 + 0.0002), vec3(0.5, 0.5, 0.5), vec3(), quat()},
	},

	walk = Animation({
		KeyFrame({
			{"leftLeg", nil, nil, nil, quat.fromAngleAxis(-math.pi / 2, vec3(0,1,0)) * quat.fromAngleAxis(math.pi/4, vec3(1,0,0))},
			{"rightLeg", nil, nil, nil, quat.fromAngleAxis(-math.pi / 2, vec3(0,1,0)) * quat.fromAngleAxis(-math.pi/4, vec3(1,0,0))},
			{"rightArm1" , nil, nil, nil, quat.fromAngleAxis(-math.pi / 2, vec3(0,1,0)) * quat.fromAngleAxis(math.pi/12, vec3(1,0,0))},
			{"rightArm2", nil, nil, nil, quat.fromAngleAxis(math.pi / 2, vec3(0,1,0)) * quat.fromAngleAxis(math.pi/4, vec3(0,0,1))},
			{"leftArm1" , nil, nil, nil, quat.fromAngleAxis(-math.pi / 2, vec3(0,1,0)) * quat.fromAngleAxis(-math.pi/6, vec3(1,0,0))},
			{"leftArm2", nil, nil, nil, quat.fromAngleAxis(math.pi / 2, vec3(0,1,0)) * quat.fromAngleAxis(math.pi/12, vec3(0,0,1))},
			{"body", nil, nil, nil, quat.fromAngleAxis(-math.pi / 12, vec3(0, 0, 1))}
		}, 0.5),

		KeyFrame({
			{"leftLeg", nil, nil, nil, quat.fromAngleAxis(-math.pi / 2, vec3(0,1,0)) * quat.fromAngleAxis(-math.pi/4, vec3(1,0,0))},
			{"rightLeg", nil, nil, nil, quat.fromAngleAxis(-math.pi / 2, vec3(0,1,0)) * quat.fromAngleAxis(math.pi/4, vec3(1,0,0))},
			{"rightArm1" , nil, nil, nil, quat.fromAngleAxis(-math.pi / 2, vec3(0,1,0)) * quat.fromAngleAxis(-math.pi/6, vec3(1,0,0))},
			{"rightArm2", nil, nil, nil, quat.fromAngleAxis(math.pi / 2, vec3(0,1,0)) * quat.fromAngleAxis(math.pi/12, vec3(0,0,1))},
			{"leftArm1" , nil, nil, nil, quat.fromAngleAxis(-math.pi / 2, vec3(0,1,0)) * quat.fromAngleAxis(math.pi/12, vec3(1,0,0))},
			{"leftArm2", nil, nil, nil, quat.fromAngleAxis(math.pi / 2, vec3(0,1,0)) * quat.fromAngleAxis(math.pi/4, vec3(0,0,1))},
			{"body", nil, nil, nil, quat.fromAngleAxis(-math.pi / 12, vec3(0, 0, 1))}
		}, 0.5),			
	}, true),

	idle = Animation({
		KeyFrame({{"body", nil, nil, nil, nil}}, 0.1)
	}, true),

	inAir = Animation({
		KeyFrame({
			{"body", nil, nil, nil, quat.fromAngleAxis(-math.pi / 12, vec3(0, 0, 1))},
			{"rightArm1" , nil, nil, nil, quat.fromAngleAxis(-math.pi / 2, vec3(0,1,0)) * quat.fromAngleAxis(-math.pi/6, vec3(1,0,0))},
			{"rightArm2", nil, nil, nil, quat.fromAngleAxis(math.pi / 2, vec3(0,1,0)) * quat.fromAngleAxis(math.pi/12, vec3(0,0,1))},
			{"leftArm1" , nil, nil, nil, quat.fromAngleAxis(-math.pi / 2, vec3(0,1,0)) * quat.fromAngleAxis(math.pi/12, vec3(1,0,0))},
			{"leftArm2", nil, nil, nil, quat.fromAngleAxis(math.pi / 2, vec3(0,1,0)) * quat.fromAngleAxis(math.pi/4, vec3(0,0,1))},
			{"rightLeg", vec3(4/5, 0, 4/6 + 0.00001), nil, nil, quat.fromAngleAxis(-math.pi / 2, vec3(0,1,0)) * quat.fromAngleAxis(math.pi/3, vec3(1,0,0))},
			{"leftLeg", vec3(2/5, 0, 1/6 + 0.00001), nil, nil, quat.fromAngleAxis(-math.pi / 2, vec3(0,1,0)) * quat.fromAngleAxis(-math.pi/3, vec3(1,0,0))}
		}, 0.2)
	}, true)
}

testplayer_skel = Skeleton({
	{"body", "testplayer/body"},
	{"head", "testplayer/head", "body"},
	{"leftArm1", "testplayer/arm1", "body"},
	{"rightArm1", "testplayer/arm1", "body"},
	{"leftArm2", "testplayer/arm2", "leftArm1"},
	{"rightArm2", "testplayer/arm2", "rightArm1"},
	{"leftHand", "empty", "leftArm2"},
	{"rightHand", "empty", "rightArm2"},
	{"leftLeg", "testplayer/leg", "body"},
	{"rightLeg", "testplayer/leg", "body"},
}, testplayer_anim)



chad_skel = Skeleton({
	{"body", "grango/body"},
	{"head", "grango/face", "body"},
	{"leftArm1", "grango/left arm", "body"},
	{"rightArm1", "grango/right arm", "body"},
	{"leftArm2", "empty", "leftArm1"},
	{"rightArm2", "empty", "rightArm1"},
	{"leftHand", "empty", "leftArm2"},
	{"rightHand", "empty", "rightArm2"},
	{"leftLeg", "grango/left foot", "body"},
	{"rightLeg", "grango/right foot", "body"},
}, testplayer_anim)