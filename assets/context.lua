local function search(k, plist)
	for key,value in pairs(plist) do
		local v = rawget(value, k)     -- try `i'-th superclass
		if v then return v end
	end
end

function vox_multInherit(t, k)
	local v = search(k, getmetatable(t).__indexes)
	t[k] = v       -- save for next access
	return v
end

_G["vox_global_table"] = {_G = _G, _default = {_name = "_default"}}

function _findGlobal(k)
	return search(k, vox_global_table)
end

function _vox_dofile(file, moduleObj, fileObj)
	local func, err = loadfile(file)
	fileObj._G = fileObj
	fileObj._M = moduleObj
	setmetatable(fileObj, {__index = _G, __indexes = {_G}})

	if func == nil then print(err) return obj end

	setfenv(func, fileObj)
	fileObj._R = func()
	return fileObj
end

function vox_doluafile(file, moduleName, name)
	if not vox_global_table[moduleName] then vox_global_table[moduleName] = {_name = moduleName} end
	vox_global_table[moduleName][name] = {}
	_vox_dofile(file, vox_global_table[moduleName], vox_global_table[moduleName][name])
end

function vox_dostring(text, doObject, name)
	local func, err = loadstring(text)
	local obj = doObject or {}
	obj._G = obj
	if getmetatable(obj) == nil then setmetatable(obj, {__index = _G, __indexes = {_G}}) end

	if func == nil then print(err) return obj end

	setfenv(func, obj)
	func()
	return obj
end

function vox_copy_table_to(a, b, seen)
	if type(a) ~= 'table' then return a end
	if seen and seen[a] then return seen[a] end
	local s = seen or {}
	local res = (b == nil and setmetatable({}, getmetatable(obj)) or b)
	s[a] = res
	for k, v in pairs(a) do res[vox_copy_table_to(k, nil, s)] = vox_copy_table_to(v, nil, s) end
	return res
end

-- Some expanation:
--   path is the relative path to the file within modules/moduleName/lua/, folder1/file
--   name is the hidden full name of the file, eg folder1.file
--   humanName is the name of the variable we create to return without the folders, eg file
function vox_parse_import_string(s, m)
	-- Check s for a module declaration
	local i, j = string.find(s, ".+::")

	local moduleName = m ~= nil and m._name or "_default"

	-- If a module declaration was found
	if i ~= nil then
		moduleName = string.sub(s, i, j-2)
		s = string.sub(s, j+1, s.length)
	end

	-- We use '.' in the import declaration instead of '/' because it looks nicer but
	-- they're essentially the same thing.
	local path = string.gsub(s, "%.", "/")
	local name = string.gsub(path, "/", "%.") -- Reconvert so if someone used '/'s the name still has '.'s

	local i = string.match(name, ".*%.()")
	if i == nil then i = 1 end
	local humanName = string.sub(name, i, name.length)

	return moduleName, path, name, humanName
end



-- Note the above functions are for internal use only, so please use import instead.



-- Note: files are only ever run once. Importing a file from two different files will not run it twice.
--
-- Input takes the form of "module::folder1.folder2.file" and will import into the "file" object the script
-- in the module "module" and in the subfolders lua/folder1/folder2 etc. You can omit the "module::" as a
-- shortcut to import things from the current module.
--
-- The difference between import and importAs is that import creates a file object in the current scope, while
-- importAs does not and instead returns an object so you can make it yourself. This is useful when you have
-- a script named "test" in both folder1 and folder2. "import 'folder1.test' and import 'folder2.test'" would
-- both write to the same test object, so you can use "test1 = importAs 'folder1.test'" instead.
--
-- If the file returns something, that object will become what import manages instead of the file itself.

function import(s)
	-- The getfenv(2) gets the environment (the global variable) of the function that
	-- called this one. As such, we can access what the file sees using this.
	local global = getfenv(2)
	local moduleName, path, name, humanName = vox_parse_import_string(s, global._M) 

	if not vox_global_table[moduleName] or not vox_global_table[moduleName][name] then
		vox_doluafile(vox_getLuaPath(moduleName, path), moduleName, name)
	end

	-- Check if the file returned a value and use that instead if it did
	local _R = vox_global_table[moduleName][name]._R
	global[humanName] = _R == nil and vox_global_table[moduleName][name] or _R
end

function importAs(s)
	-- The getfenv(2) gets the environment (the global variable) of the function that
	-- called this one. As such, we can access what the file sees using this.
	local global = getfenv(2)
	local moduleName, path, name, humanName = vox_parse_import_string(s, global._M) 

	if not vox_global_table[moduleName] or not vox_global_table[moduleName][name] then
		vox_doluafile(vox_getLuaPath(moduleName, path), moduleName, name)
	end

	-- Check if the file returned a value and use that instead if it did
	local _R = vox_global_table[moduleName][name]._R
	return _R == nil and vox_global_table[moduleName][name] or _R
end

-- Adds the specified file to the list of indexes of the current file, meaning if you access
-- something not defined in the current environment, it will search there as well.
-- Useful if you want to create a file with a bunch of functions but not have to call
-- file.function for each of them.
function importAll(s)
	-- The getfenv(2) gets the environment (the global variable) of the function that
	-- called this one. As such, we can access what the file sees using this.
	local global = getfenv(2)
	local moduleName, path, name, humanName = vox_parse_import_string(s, global._M) 

	if not vox_global_table[moduleName] or not vox_global_table[moduleName][name] then
		vox_doluafile(vox_getLuaPath(moduleName, path), moduleName, name)
	end

	-- Check if the file returned a value and use that instead if it did
	
	getmetatable(global).__index = vox_multInherit
	table.insert(getmetatable(global).__indexes, 1, vox_global_table[moduleName][name])
end

-- If the object is a table, it will list all the key-value pairs. If it's not, it will print it.
function list(o)
	if type(o) == "table" then
		sorted = {}
		for key in pairs(o) do sorted[#sorted+1] = key end
		table.sort(sorted)

		for i=1,#sorted do
			print(sorted[i], o[sorted[i]])
		end

	else
		print(o)
	end
end

-- Runs the specified script (see import for how to specify scripts). Same as calling importAs.
run = importAs