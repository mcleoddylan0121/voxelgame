importAll "base::GUI"


--[[
	FLAGS:
		transparent (default false): 
			Whether or not this is transparent.
			If you're a developer of this game (daniel), then this should be false (Because of the art style and stuff).
			If you're making a mod, then go crazy.
		
		solid (default true):
			Solid voxels can be collided with.
			Even if this isn't transparent, you should raise the transparent flag if it's not solid.
			
		imaginary (default false):
			Imaginary voxels are not rendered, at all.
			If a voxel is imaginary, it should probably not be solid, and it should definitely be transparent.
			However, it's all up to you.	
--]]


function orEquals(a1, a2)
  if a1 == nil then
    return a2
  else
    return a1
  end
end

-- If you're making a transparent voxel, then be sure to raise the transparent flag.
function Voxel(name, color, voxelLight, flags)
	local obj = {}
	obj.name = name
	obj.color = color
	obj.flags = flags or {}
	
	obj.flags.transparent = orEquals(obj.flags.transparent, false)
	obj.flags.solid = orEquals(obj.flags.solid, true)
	obj.flags.imaginary = orEquals(obj.flags.imaginary, false)
	
	-- The proper constructors will set these to true
	obj.flags.liquid = false
	obj.flags.lightSource = false
	obj.lightcolor = RGB_B(0,0,0)
	
	--[[
	
	          QUICK NOTE ABOUT VOXEL LIGHTS:
	          unlike light levels, these do not spread.
	          however, they have a higher dynamic range (and can even be negative!)
	          and they work with entity palettes as well
	  
	          the allowed range is [-4, 4], unlike light levels, which are [0, 1].
	          
	--]]
	
	obj.voxellight = voxelLight;
	
	return obj
end

-- For each channel in lightcolor, the intensity indicates the light level. 1 is the highest, 0 is the lowest. Most lights will be around 0.5. The alpha bit is ignored.
function LightSource(name, color, lightcolor, voxelLight, flags)
	local obj = Voxel(name, color, voxelLight, flags)
	
	obj.flags.lightSource = true -- There's no way im letting you set this to false
	obj.lightcolor = lightcolor
	
	return obj
end

local vlightCol = 0.5;
local vlightCol2 = 0.2;

local lightCol = 255;
local lightCol2 = 200;

local lightPow = 0.5;
local lightPow2 = 0.2;

-- the 0-indexed array isn't standard lua, but it's how voxel IDs work, so deal with it
voxels = {
	[0] = Voxel("air", RGB_B(255,255,255), RGB_F(0,0,0), {imaginary = true, solid = false, transparent = true}),
	[1] = Voxel("stone", RGB_B(169,161,140)),
	[2] = Voxel("grass", RGB_B(100,160,75)),
	[3] = Voxel("dirt", RGB_B(100,90,30)),
	[4] = Voxel("???", RGB_F(0,0,0), RGB_F(-4,-4,-4), {transparent = true, solid = false}), -- ???
	[5] = Voxel("mahogany", RGB_B(170,1,20), RGB_F(0,0,0), {transparent = true, solid = false}), -- an elegant weapon for a more civilized age
	[6] = Voxel("i should use an actual file format", RGB_B(170,1,20), RGB_F(0,0,0), {transparent = true, solid = false}), -- an elegant weapon for a more civilized age
	[7] = Voxel("i should use an actual file format", RGB_B(170,1,20), RGB_F(0,0,0), {transparent = true, solid = false}), -- an elegant weapon for a more civilized age
	[8] = Voxel("i should use an actual file format", RGB_B(170,1,20), RGB_F(0,0,0), {transparent = true, solid = false}), -- an elegant weapon for a more civilized age
	[9] = Voxel("i should use an actual file format", RGB_B(170,1,20), RGB_F(0,0,0), {transparent = true, solid = false}), -- an elegant weapon for a more civilized age
	--[[
	[6] = LightSource("red light",      RGB_B(lightCol, lightCol2,lightCol2), RGB_F(lightPow, lightPow2,lightPow2), RGB_F(vlightCol, vlightCol2,vlightCol2)),
	[7] = LightSource("green light",    RGB_B(lightCol2,lightCol, lightCol2), RGB_F(lightPow2,lightPow, lightPow2), RGB_F(vlightCol2,vlightCol, vlightCol2)),
	[8] = LightSource("blue light",     RGB_B(lightCol2,lightCol2,lightCol),  RGB_F(lightPow2,lightPow2,lightPow),  RGB_F(vlightCol2,vlightCol2,vlightCol)),
	[9] = LightSource("yellow light",   RGB_B(lightCol, lightCol, lightCol2), RGB_F(lightPow, lightPow, lightPow2), RGB_F(vlightCol, vlightCol, vlightCol2)),
	[10] = LightSource("magenta light", RGB_B(lightCol, lightCol2,lightCol),  RGB_F(lightPow, lightPow2,lightPow),  RGB_F(vlightCol, vlightCol2,vlightCol)),
	[11] = LightSource("cyan light",    RGB_B(lightCol2,lightCol, lightCol),  RGB_F(lightPow2,lightPow, lightPow),  RGB_F(vlightCol2,vlightCol, vlightCol)),
	[12] = LightSource("white light",   RGB_B(lightCol, lightCol, lightCol),  RGB_F(lightPow, lightPow, lightPow),  RGB_F(vlightCol, vlightCol, vlightCol)),
	[13] = LightSource("wood", RGB_B(149,69,53))
	]]
}

-- This can have a transparent color.
-- solid is defaulted to false
-- transparent is defaulted to true (but you may change it in the case of tar, lava, etc)
function Liquid(name, color, voxelLight, flags)
  flags = flags or {}
  flags.solid = orEquals(flags.solid, false)
  flags.transparent = orEquals(flags.transparent, true)
  
  local obj = Voxel(name, color, voxelLight, flags)
  
  obj.flags.liquid = true -- don't even try setting this to false
  
  return obj
end

-- Lava, some glowy goo stuff, and that's about it
function LitLiquid(name, color, lightcolor, voxelLight, flags)
  local obj = Liquid(name, color, voxelLight, flags)
  
  obj.flags.lightSource = true
  obj.lightcolor = lightcolor
  
  return obj
end

-- these are based on liquid IDs, which are seperate from voxel IDs, because reasons
liquids = {
	[0] = Liquid("water", RGBA_B(0,150,200,100)),
	[1] = Liquid("lava", RGB_B(207,16,32), RGB_F(0,0,0), {transparent = false}) -- not yet used, be patient
}

masterlist = {} -- voxelID->voxel
voxelhash = {} -- name->voxel
cpplist = {} -- voxel

for i=0,#voxels do
	voxels[i].ID = i
	voxels[i].meta = 0
	masterlist[i] = voxels[i]
	voxelhash[voxels[i].name] = voxels[i]
	cpplist[#cpplist+1] = voxels[i]
end

function deepcopy(orig)
    local orig_type = type(orig)
    local copy
    if orig_type == 'table' then
        copy = {}
        for orig_key, orig_value in next, orig, nil do
            copy[deepcopy(orig_key)] = deepcopy(orig_value)
        end
        setmetatable(copy, deepcopy(getmetatable(orig)))
    else -- number, string, boolean, etc
        copy = orig
    end
    return copy
end

for i=0,#liquids do
	for m=0,15 do
		local t = deepcopy(liquids[i])
		t.meta = m*16+13 --this 13 _should_ be 15, but that leads to issues with z-fighting
		t.ID = i*16+8192+m
		masterlist[i*16 + 8192 + m] = t
		cpplist[#cpplist+1] = t
		if(m==0) then
			voxelhash[t.name] = t
		end
	end
end


